EFFECTIVE PERIOD:
24-JUL-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 0-3   B  -  B  B  -  -
03 0-4   C  -  C  -  C  C
04 0-4   D  D  D  D  -  D
05 0-4   -  -  E  -  E  E
06 0-4   F  -  F  -  -  F
07 0-4   -  G  -  -  -  -
08 0-5   H  H  -  H  H  -
09 0-5   -  I  -  I  I  I
10 0-5   -  -  J  -  J  -
11 0-5   K  -  K  K  K  K
12 0-5   L  L  L  -  -  -
13 0-5   -  -  -  -  M  -
14 0-5   -  -  -  -  N  -
15 0-5   O  -  -  -  O  -
16 0-5   -  P  P  P  P  P
17 0-6   -  -  Q  Q  -  Q
18 0-6   R  R  -  R  -   
19 0-6   -  -  -  -  S   
20 0-6   -  T  T  T      
21 0-6   U  U  -  -      
22 1-2   V  V  -         
23 1-4   W  -  X         
24 4-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

INOVS QMZBQ XLZCD STZOJ MOURO I
-------------------------------
