EFFECTIVE PERIOD:
27-JUL-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  B  -  -  -
03 1-0   -  C  -  C  C  C
04 1-0   -  D  -  D  D  -
05 1-0   -  -  E  -  -  E
06 1-0   F  -  F  -  F  F
07 2-0   G  G  G  -  G  G
08 2-0   H  -  -  -  -  -
09 2-0   -  I  -  I  -  -
10 0-3   J  -  -  -  -  J
11 0-6   K  -  K  -  -  -
12 0-6   -  -  -  L  L  L
13 0-6   -  -  -  M  -  M
14 0-6   -  N  -  N  -  N
15 0-6   -  -  O  O  -  O
16 1-2   P  -  P  P  P  -
17 1-3   Q  Q  Q  Q  Q  -
18 2-6   R  R  -  -  -   
19 2-6   -  S  S  S  -   
20 2-6   T  -  -  -      
21 2-6   U  U  U  -      
22 3-5   V  V  -         
23 3-6   W  X  X         
24 3-6   -  -            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KXAUZ OPZKQ KANSP FWPUS XVACU G
-------------------------------
