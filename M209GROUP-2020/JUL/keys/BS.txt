EFFECTIVE PERIOD:
01-JUL-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  -  B  B  -
03 1-0   C  C  -  -  C  C
04 2-0   -  -  D  -  -  -
05 0-3   E  -  -  E  E  E
06 0-4   F  F  -  -  -  -
07 0-4   -  -  -  G  G  -
08 0-4   H  -  H  H  H  H
09 0-4   I  I  -  -  -  -
10 0-4   J  -  -  -  -  -
11 0-4   -  K  K  -  -  K
12 0-4   L  -  L  L  -  L
13 0-4   M  -  M  -  M  M
14 0-4   -  -  N  N  N  -
15 0-4   -  O  -  -  -  -
16 0-4   P  P  P  -  P  -
17 0-5   Q  -  -  -  -  Q
18 0-5   R  R  R  R  R   
19 0-5   -  -  S  -  -   
20 0-5   -  T  T  T      
21 0-5   -  -  U  U      
22 0-5   V  -  -         
23 0-5   W  -  -         
24 0-5   -  Y            
25 0-6   -  Z            
26 1-6   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

HZOBI NOPSW UCNRK WQDFG XKPOZ K
-------------------------------
