EFFECTIVE PERIOD:
17-OCT-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  -  B  -  B  B
03 1-0   C  -  -  -  -  C
04 1-0   -  D  -  -  D  D
05 2-0   E  -  E  E  -  -
06 0-3   -  -  F  F  F  F
07 0-3   -  G  -  G  -  G
08 0-3   -  H  H  H  H  -
09 0-3   I  -  -  -  I  I
10 0-3   -  J  J  -  -  -
11 0-3   -  K  K  K  -  K
12 0-4   L  L  L  L  -  L
13 0-4   -  M  M  M  -  -
14 0-4   -  N  N  N  N  -
15 0-4   O  -  -  -  O  -
16 1-2   -  P  P  P  P  P
17 1-3   -  -  -  -  Q  -
18 2-4   R  -  -  R  -   
19 2-4   S  S  S  -  -   
20 2-4   -  T  -  T      
21 2-5   U  -  -  -      
22 3-4   V  V  -         
23 3-4   W  X  X         
24 3-4   X  -            
25 3-4   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TPRTG AFULS LMATW ZNRGY WWARP Q
-------------------------------
