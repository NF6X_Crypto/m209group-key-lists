EFFECTIVE PERIOD:
25-OCT-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  -
02 2-0   -  -  B  B  -  -
03 2-0   C  -  -  -  -  C
04 2-0   -  D  -  D  -  D
05 2-0   E  E  E  -  -  E
06 0-4   -  F  -  F  F  -
07 0-4   -  G  G  G  -  G
08 0-5   H  H  -  H  H  -
09 0-5   -  I  I  -  -  -
10 0-5   -  -  -  -  J  -
11 0-5   -  K  -  K  -  K
12 0-6   L  L  L  -  -  -
13 0-6   -  -  M  M  M  -
14 0-6   -  N  -  N  -  -
15 0-6   O  -  O  O  O  O
16 0-6   -  P  -  P  P  P
17 0-6   -  Q  Q  Q  Q  -
18 1-5   R  R  -  R  -   
19 2-5   S  -  S  -  S   
20 2-5   T  -  -  -      
21 2-5   U  U  -  -      
22 2-5   -  -  -         
23 2-6   W  X  -         
24 3-4   -  Y            
25 4-5   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MUQUV VJKQR MIFSP UPXLP KVAOU G
-------------------------------
