EFFECTIVE PERIOD:
13-OCT-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  -  -  -  -  -
03 2-0   -  C  C  -  C  C
04 0-3   D  D  D  D  -  D
05 0-3   E  E  -  -  E  E
06 0-3   F  -  -  -  -  -
07 0-3   -  -  -  G  -  G
08 0-3   -  H  -  H  -  H
09 0-3   -  -  I  -  I  I
10 0-3   J  J  J  J  -  J
11 0-3   -  K  K  K  -  -
12 0-4   L  -  L  -  -  -
13 0-4   -  M  -  -  M  -
14 0-4   N  -  -  -  N  -
15 0-4   -  O  -  -  -  O
16 0-4   P  -  -  P  P  -
17 0-5   -  Q  Q  -  -  -
18 0-6   -  -  R  R  R   
19 0-6   -  S  S  S  S   
20 0-6   -  -  T  T      
21 0-6   -  U  U  U      
22 1-2   V  V  -         
23 1-6   W  -  X         
24 3-4   X  -            
25 3-4   Y  Z            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

EACQT QWANY TFRSJ RRZSQ QTDCQ C
-------------------------------
