EFFECTIVE PERIOD:
07-OCT-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 2-0   B  -  -  -  B  B
03 0-3   C  -  -  C  C  C
04 0-4   -  D  -  -  D  D
05 0-4   -  E  E  -  -  -
06 0-4   F  F  -  F  -  -
07 0-4   G  -  G  G  -  -
08 0-4   H  H  -  H  -  -
09 0-5   -  -  I  I  -  I
10 0-5   J  -  -  -  -  J
11 0-5   K  K  -  -  K  K
12 0-5   -  L  L  -  L  -
13 0-5   M  -  -  M  -  -
14 0-5   -  -  N  N  N  -
15 0-5   O  -  -  O  -  O
16 0-5   -  P  -  P  P  -
17 0-6   -  -  Q  -  Q  -
18 0-6   -  R  R  R  R   
19 0-6   S  -  S  S  -   
20 1-3   T  T  T  -      
21 2-3   -  U  -  -      
22 2-6   V  -  V         
23 4-5   W  X  -         
24 4-5   X  Y            
25 4-5   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

YNRPT XXAJT VWAAP QSORL AANUR R
-------------------------------
