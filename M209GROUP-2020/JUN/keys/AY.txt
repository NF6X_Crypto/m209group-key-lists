EFFECTIVE PERIOD:
11-JUN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 2-0   B  -  -  -  -  -
03 0-3   C  C  C  -  C  C
04 0-4   -  D  -  D  -  -
05 0-4   -  E  E  E  E  E
06 0-4   -  F  F  F  F  F
07 0-5   G  -  G  G  -  G
08 0-5   H  -  H  H  -  H
09 0-5   -  I  -  I  -  I
10 0-5   -  -  -  -  -  -
11 0-5   -  -  K  -  K  K
12 0-5   L  L  L  L  -  -
13 0-5   -  M  -  -  -  -
14 0-5   -  -  -  N  N  N
15 0-6   O  O  -  O  O  O
16 0-6   P  -  P  -  -  P
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  -  -  R   
19 0-6   -  -  S  -  -   
20 0-6   T  -  T  T      
21 0-6   U  U  -  -      
22 0-6   V  -  -         
23 0-6   -  X  X         
24 0-6   -  Y            
25 0-6   Y  Z            
26 1-3   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

NDOVQ WMDAP KNZZR RGRJA QVZEH P
-------------------------------
