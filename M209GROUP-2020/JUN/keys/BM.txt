EFFECTIVE PERIOD:
25-JUN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 2-0   B  B  B  -  -  B
03 0-3   C  -  C  C  C  C
04 0-3   -  D  -  D  D  -
05 0-3   E  -  -  E  E  -
06 0-3   -  -  F  F  -  -
07 0-3   G  G  G  -  -  -
08 0-3   H  H  -  -  H  H
09 0-3   -  -  -  -  -  I
10 0-3   -  J  J  -  -  J
11 0-3   K  K  -  -  -  -
12 0-4   -  -  -  L  -  -
13 0-4   -  -  M  M  -  -
14 0-4   -  -  N  N  -  N
15 0-5   -  O  O  -  O  -
16 0-6   -  P  -  -  P  P
17 0-6   Q  -  Q  -  -  -
18 0-6   R  -  -  -  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  -  -      
21 0-6   -  U  U  U      
22 0-6   V  -  -         
23 0-6   W  -  X         
24 0-6   -  -            
25 0-6   -  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

NPBWE KGMPW DOQFO WSCCI OGYLI K
-------------------------------
