EFFECTIVE PERIOD:
03-NOV-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  B  B  B  -
03 1-0   C  -  C  C  -  C
04 1-0   -  D  -  -  -  -
05 2-0   E  E  -  E  E  E
06 2-0   F  -  -  -  -  F
07 2-0   -  G  G  -  G  -
08 2-0   -  H  -  H  -  -
09 0-3   I  -  -  -  -  -
10 0-3   -  J  -  J  J  J
11 0-3   K  -  -  -  -  -
12 0-3   -  -  L  -  -  L
13 0-3   M  M  -  -  M  M
14 0-3   N  -  -  -  N  N
15 0-3   O  O  O  O  O  O
16 0-3   -  P  P  -  -  -
17 0-3   -  -  -  Q  Q  -
18 0-3   R  R  R  -  -   
19 0-5   S  S  S  S  S   
20 0-5   -  -  -  -      
21 0-6   -  -  U  U      
22 1-2   V  V  -         
23 1-3   W  X  X         
24 1-3   -  Y            
25 1-3   Y  -            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FWWQU QIAIA AOWII MTIHI JWDNW X
-------------------------------
