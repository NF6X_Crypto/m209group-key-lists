EFFECTIVE PERIOD:
24-NOV-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  B  -  B  -  B
03 1-0   C  -  C  C  C  C
04 2-0   -  D  D  D  D  D
05 2-0   E  E  -  E  -  E
06 2-0   F  -  -  -  -  -
07 0-4   -  -  -  -  G  G
08 0-5   H  -  H  -  -  H
09 0-5   I  I  -  -  -  -
10 0-5   -  J  J  -  J  -
11 0-5   K  K  -  K  K  -
12 0-5   L  -  -  -  -  L
13 0-5   -  M  M  M  -  M
14 0-5   N  -  -  -  -  N
15 0-6   O  -  O  -  O  -
16 0-6   -  -  P  P  -  P
17 0-6   -  Q  -  Q  Q  -
18 0-6   R  R  -  -  -   
19 0-6   S  S  S  -  -   
20 1-4   -  T  T  T      
21 1-6   U  U  U  -      
22 2-5   -  -  -         
23 2-5   -  X  -         
24 2-5   -  -            
25 2-5   -  -            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

BHSAJ ZICVI XAKMV MELWT NNWHR M
-------------------------------
