EFFECTIVE PERIOD:
11-NOV-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   -  B  -  -  -  B
03 2-0   -  -  C  C  C  -
04 2-0   -  -  D  D  D  -
05 2-0   -  E  -  E  E  E
06 2-0   F  F  F  -  -  F
07 0-3   -  -  -  -  -  G
08 0-4   H  -  H  H  H  -
09 0-4   -  I  -  I  I  -
10 0-5   J  -  -  J  -  J
11 0-5   -  -  K  K  K  -
12 0-5   -  L  L  -  -  -
13 0-5   -  -  -  M  M  -
14 0-6   -  N  N  -  N  -
15 0-6   O  -  O  O  -  O
16 0-6   P  -  -  -  P  P
17 0-6   -  -  Q  Q  -  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  -  S  S   
20 1-6   -  -  T  -      
21 2-4   -  -  -  -      
22 2-5   V  V  V         
23 3-4   W  X  -         
24 5-6   X  Y            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FKNKT KPQYI QZAPZ RZENK LUSUJ O
-------------------------------
