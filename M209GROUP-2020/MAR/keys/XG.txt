EFFECTIVE PERIOD:
07-MAR-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  -  -  -  B
03 1-0   C  C  C  -  C  -
04 1-0   -  -  -  D  D  D
05 2-0   E  -  -  -  E  -
06 2-0   F  F  F  F  F  -
07 2-0   G  -  -  -  -  G
08 2-0   H  -  H  -  H  -
09 0-5   -  I  I  -  I  -
10 0-5   -  J  J  -  -  -
11 0-5   -  -  K  -  -  -
12 0-6   L  -  L  L  L  L
13 0-6   -  M  -  M  -  -
14 0-6   N  -  -  N  -  N
15 0-6   -  -  O  O  O  O
16 0-6   P  -  -  P  P  P
17 0-6   -  -  Q  -  -  Q
18 1-5   R  R  -  R  R   
19 1-5   -  -  -  S  S   
20 1-5   -  T  T  T      
21 1-5   U  U  U  U      
22 1-6   V  V  -         
23 2-3   -  -  -         
24 2-5   X  Y            
25 2-6   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

OKJWR WAPQH APPAA PUKRP PKVWL R
-------------------------------
