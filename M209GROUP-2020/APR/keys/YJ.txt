EFFECTIVE PERIOD:
05-APR-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   B  -  B  -  B  -
03 2-0   -  -  C  C  C  C
04 2-0   -  D  D  D  -  D
05 2-0   -  -  E  E  -  -
06 2-0   F  F  F  F  -  -
07 2-0   -  G  -  G  G  -
08 0-3   H  H  H  H  -  H
09 0-3   -  -  -  -  -  I
10 0-3   J  J  -  J  -  -
11 0-3   -  K  -  K  K  -
12 0-3   L  -  -  L  -  -
13 0-3   -  -  -  -  -  M
14 0-5   N  N  N  -  N  -
15 0-5   O  O  -  O  -  -
16 0-5   P  P  -  -  -  -
17 0-5   Q  -  Q  Q  Q  Q
18 1-3   R  R  R  -  R   
19 1-6   S  -  -  -  S   
20 2-5   -  T  T  -      
21 2-5   -  U  -  U      
22 2-5   V  V  -         
23 2-5   W  -  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VOPVI PAUJP TLLIY LPSUD PPDLU U
-------------------------------
