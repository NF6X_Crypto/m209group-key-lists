EFFECTIVE PERIOD:
19-APR-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  A  A
02 0-3   B  B  -  B  -  B
03 0-3   -  -  C  -  -  -
04 0-3   -  D  D  D  -  -
05 0-3   E  E  E  -  -  -
06 0-3   -  -  F  F  F  F
07 0-4   -  G  -  -  G  -
08 0-4   -  -  H  -  H  H
09 0-5   I  -  I  -  -  I
10 0-5   -  J  -  J  -  J
11 0-5   -  K  K  -  K  K
12 0-5   -  -  -  -  -  -
13 0-5   M  -  -  -  -  -
14 0-6   -  N  -  N  -  N
15 0-6   -  O  O  -  -  -
16 0-6   P  P  P  P  P  -
17 0-6   Q  -  -  Q  Q  Q
18 0-6   -  R  -  R  R   
19 0-6   S  S  S  -  -   
20 1-3   T  -  -  -      
21 2-4   U  U  U  U      
22 3-5   -  -  V         
23 3-5   W  -  X         
24 3-5   X  -            
25 3-5   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JOKLO URSAO EAVQA YHTUA TADRA A
-------------------------------
