EFFECTIVE PERIOD:
17-APR-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 2-0   -  -  B  B  B  -
03 2-0   C  C  C  -  C  -
04 2-0   D  D  D  D  D  D
05 2-0   E  E  -  E  -  E
06 2-0   F  F  F  F  -  -
07 2-0   -  G  G  -  G  G
08 2-0   -  H  -  H  -  -
09 0-3   I  -  -  -  -  I
10 0-3   -  J  J  J  J  J
11 0-3   -  -  -  K  K  -
12 0-3   L  -  L  -  L  L
13 0-3   -  -  M  -  -  -
14 0-4   N  N  -  -  -  N
15 0-4   O  O  -  O  O  O
16 0-4   -  P  -  P  -  P
17 0-4   -  Q  Q  Q  -  -
18 0-4   R  -  R  -  R   
19 0-4   S  -  -  -  S   
20 0-5   T  T  -  T      
21 0-6   U  U  -  U      
22 1-4   -  -  -         
23 1-5   -  -  -         
24 2-3   -  -            
25 2-3   -  -            
26 2-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

LKTTS NLJJB KUPZU TZLUS KJNHT U
-------------------------------
