EFFECTIVE PERIOD:
01-APR-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 1-0   B  B  B  B  -  B
03 1-0   -  C  C  C  C  C
04 1-0   D  -  D  -  -  -
05 1-0   E  -  E  E  E  -
06 1-0   F  -  -  -  F  F
07 2-0   -  G  -  -  -  G
08 2-0   -  H  H  -  -  H
09 2-0   -  I  -  I  I  -
10 2-0   J  J  -  -  J  J
11 2-0   K  K  -  K  -  -
12 2-0   -  -  -  -  L  -
13 0-5   -  -  M  M  -  -
14 0-5   N  -  N  -  N  -
15 0-5   -  -  -  -  -  O
16 0-5   P  P  -  P  -  -
17 0-5   Q  Q  -  Q  Q  -
18 0-6   R  -  R  R  -   
19 0-6   S  S  -  -  -   
20 1-3   T  T  T  T      
21 1-5   -  U  -  -      
22 1-5   V  V  V         
23 1-5   -  -  -         
24 1-5   -  -            
25 2-5   -  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RSXLV RSKED LLAUO KUSHR SAAAU D
-------------------------------
