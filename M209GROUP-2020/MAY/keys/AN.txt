EFFECTIVE PERIOD:
31-MAY-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   B  B  -  -  B  -
03 1-0   C  C  -  -  -  C
04 1-0   -  D  D  D  D  -
05 1-0   E  -  -  E  -  E
06 2-0   -  -  F  F  -  F
07 2-0   G  G  -  G  -  G
08 2-0   H  H  H  -  H  H
09 2-0   -  I  I  I  I  I
10 2-0   -  J  -  J  J  J
11 2-0   K  -  -  -  -  -
12 2-0   -  L  -  -  -  -
13 2-0   M  M  M  M  -  -
14 2-0   N  N  N  -  N  -
15 2-0   -  -  O  -  O  -
16 0-3   -  -  -  P  -  P
17 0-5   Q  -  Q  -  -  Q
18 0-5   R  -  R  -  R   
19 0-5   -  S  S  S  S   
20 0-5   T  -  -  T      
21 0-5   U  U  U  -      
22 0-5   -  V  -         
23 0-5   W  -  X         
24 0-5   X  Y            
25 0-6   -  -            
26 1-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

CHNKR ITANV CORSL QIZRJ JOVSS H
-------------------------------
