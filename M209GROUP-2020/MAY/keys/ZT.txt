EFFECTIVE PERIOD:
11-MAY-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  -  B  -  -  -
03 1-0   -  C  C  -  C  -
04 1-0   -  D  -  D  D  D
05 2-0   E  -  E  E  E  E
06 2-0   F  -  F  -  -  F
07 2-0   -  G  -  -  G  G
08 2-0   -  -  -  H  -  -
09 2-0   I  I  -  I  I  I
10 2-0   -  -  J  J  -  J
11 2-0   -  -  -  K  K  -
12 0-5   L  L  L  -  -  -
13 0-5   M  M  -  M  M  M
14 0-5   -  N  -  -  N  N
15 0-5   -  -  O  O  -  -
16 0-5   P  P  -  P  P  P
17 0-6   Q  -  -  -  -  -
18 0-6   -  -  R  R  -   
19 0-6   S  -  S  S  -   
20 1-3   T  T  T  -      
21 1-5   -  -  -  -      
22 2-4   -  -  -         
23 2-6   W  -  X         
24 2-6   X  Y            
25 2-6   Y  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GXOOL QBFLR SSPPT NTLHV NIVXM Z
-------------------------------
