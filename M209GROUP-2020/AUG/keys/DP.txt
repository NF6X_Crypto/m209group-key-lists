EFFECTIVE PERIOD:
19-AUG-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  B  B  -  B  B
03 1-0   -  -  C  C  C  -
04 2-0   D  -  D  D  -  D
05 0-3   E  E  -  -  E  -
06 0-3   F  F  F  -  F  F
07 0-4   G  -  -  -  G  -
08 0-5   -  -  -  H  -  H
09 0-5   -  I  -  I  I  I
10 0-5   -  -  J  -  J  -
11 0-5   K  K  K  K  K  -
12 0-5   -  L  -  L  -  -
13 0-5   M  M  -  -  M  M
14 0-5   N  N  -  -  -  N
15 0-5   -  -  O  O  O  O
16 0-5   -  P  P  -  -  P
17 0-6   -  Q  -  Q  -  -
18 0-6   -  -  R  R  R   
19 0-6   S  -  -  S  -   
20 0-6   T  T  -  -      
21 0-6   U  -  U  U      
22 1-3   -  -  -         
23 1-6   -  -  X         
24 3-4   -  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZQOVQ PNMKU RIXPV QTSLO WNSHQ W
-------------------------------
