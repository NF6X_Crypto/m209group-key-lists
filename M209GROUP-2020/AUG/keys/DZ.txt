EFFECTIVE PERIOD:
29-AUG-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   B  B  B  -  -  -
03 2-0   -  -  C  -  -  -
04 2-0   D  -  -  D  D  D
05 0-3   -  -  -  -  -  E
06 0-3   -  -  -  F  F  F
07 0-3   -  G  -  -  -  -
08 0-3   H  H  -  -  H  -
09 0-6   I  -  I  I  I  I
10 0-6   -  J  J  -  J  J
11 0-6   -  K  K  -  -  K
12 0-6   L  -  L  L  -  -
13 0-6   -  -  M  -  -  -
14 0-6   N  N  -  -  N  N
15 0-6   -  -  -  O  O  O
16 1-2   -  P  P  -  -  -
17 2-3   Q  Q  Q  Q  -  -
18 2-3   -  R  R  R  R   
19 2-3   -  S  -  S  -   
20 2-3   T  T  -  T      
21 2-6   -  -  -  -      
22 2-6   V  -  V         
23 2-6   -  X  X         
24 2-6   X  -            
25 3-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

LMTAA NCATL UAABB NNWSK AALAM K
-------------------------------
