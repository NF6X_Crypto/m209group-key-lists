EFFECTIVE PERIOD:
19-DEC-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   -  -  -  -  B  -
03 2-0   C  C  C  C  -  C
04 2-0   D  -  D  D  -  -
05 0-3   E  E  E  -  -  E
06 0-3   -  F  F  -  F  -
07 0-4   -  G  G  G  G  -
08 0-4   H  -  -  -  -  -
09 0-4   I  -  I  -  I  I
10 0-4   J  J  -  J  J  -
11 0-4   -  K  K  K  -  K
12 0-4   -  L  -  L  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  N  N  -  N  -
15 0-5   -  -  -  O  O  O
16 0-5   -  P  -  -  P  P
17 0-5   Q  -  -  Q  -  Q
18 1-3   -  -  R  R  R   
19 1-4   -  -  -  -  -   
20 1-6   -  -  T  -      
21 2-3   U  U  U  -      
22 2-5   V  V  -         
23 3-4   W  -  -         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QIRJW NULMS ZTPUQ VSPQN VXTWI R
-------------------------------
