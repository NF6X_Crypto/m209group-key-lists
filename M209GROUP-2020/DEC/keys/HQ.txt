EFFECTIVE PERIOD:
02-DEC-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 2-0   B  -  -  B  B  B
03 2-0   -  -  -  -  -  C
04 2-0   D  -  D  D  D  -
05 2-0   E  -  E  E  -  -
06 2-0   -  -  F  -  F  F
07 0-4   -  G  -  G  -  -
08 0-4   -  -  -  -  -  -
09 0-4   -  I  -  -  I  I
10 0-5   -  J  -  -  -  -
11 0-5   K  K  -  K  -  -
12 0-5   L  L  L  -  -  L
13 0-6   M  M  M  M  M  -
14 0-6   -  -  -  N  -  N
15 0-6   O  O  -  O  -  O
16 0-6   P  -  -  P  P  -
17 0-6   Q  -  Q  Q  Q  Q
18 1-2   -  R  -  R  R   
19 1-3   -  -  -  -  S   
20 1-5   T  T  T  T      
21 2-4   U  U  U  -      
22 2-4   -  -  -         
23 2-4   -  -  X         
24 2-4   -  Y            
25 2-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RUHWU VXTLX QHQOC QFLGP WXRAS A
-------------------------------
