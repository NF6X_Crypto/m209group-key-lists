EFFECTIVE PERIOD:
30-DEC-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  -
02 2-0   B  -  B  -  -  -
03 2-0   C  -  -  C  -  C
04 0-5   -  D  -  -  D  D
05 0-5   -  E  E  E  -  E
06 0-5   F  F  F  F  -  F
07 0-5   -  G  G  -  G  -
08 0-5   -  -  -  H  -  -
09 0-5   -  -  I  I  -  -
10 0-6   J  J  -  -  J  J
11 0-6   K  -  K  -  K  -
12 0-6   -  -  L  -  L  L
13 0-6   M  -  M  M  M  M
14 0-6   N  N  -  -  -  N
15 0-6   -  O  -  -  -  O
16 1-3   -  P  P  P  P  -
17 2-3   Q  -  -  Q  Q  -
18 2-3   -  R  R  -  R   
19 2-3   -  -  S  S  -   
20 2-3   T  -  -  -      
21 2-4   -  -  -  U      
22 2-5   V  -  -         
23 2-5   W  X  -         
24 2-5   -  Y            
25 2-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SAMTX QMALU HAIBU NUSWR ANFLT U
-------------------------------
