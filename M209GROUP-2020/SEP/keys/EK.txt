EFFECTIVE PERIOD:
09-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  -
02 2-0   -  -  -  B  B  -
03 2-0   C  C  C  C  -  -
04 2-0   -  D  -  -  -  -
05 2-0   E  -  E  -  -  E
06 2-0   -  -  F  F  -  -
07 2-0   G  G  G  -  G  G
08 0-3   H  -  -  -  -  H
09 0-3   -  -  I  I  I  I
10 0-3   J  J  J  J  J  -
11 0-3   K  -  K  -  -  -
12 0-3   -  L  L  L  L  L
13 0-4   M  -  M  -  M  M
14 0-4   -  N  -  -  N  -
15 0-4   -  -  -  O  O  -
16 0-4   -  -  -  P  -  P
17 0-4   Q  Q  -  Q  Q  -
18 0-4   -  -  -  R  R   
19 0-4   S  S  -  S  S   
20 0-4   T  T  T  -      
21 0-4   -  -  -  -      
22 0-4   -  -  -         
23 0-4   -  -  -         
24 0-4   X  Y            
25 0-6   Y  -            
26 1-3   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

ACUNU USABU LMFJA HGUTH ZGONT M
-------------------------------
