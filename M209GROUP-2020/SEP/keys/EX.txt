EFFECTIVE PERIOD:
22-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  A
02 2-0   B  B  B  B  B  B
03 2-0   C  -  C  -  -  C
04 2-0   -  D  -  -  -  D
05 0-3   E  E  -  -  E  -
06 0-3   -  -  F  -  -  F
07 0-3   G  -  -  -  G  G
08 0-4   -  -  H  H  -  -
09 0-4   -  I  -  -  -  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  -  K  K  -
12 0-4   -  -  -  -  L  L
13 0-4   -  -  M  -  -  M
14 0-6   N  -  -  -  N  -
15 0-6   -  O  -  O  -  -
16 0-6   -  -  P  P  -  -
17 0-6   -  -  -  -  Q  -
18 1-3   -  -  R  R  -   
19 1-4   -  S  S  S  S   
20 1-5   T  T  T  -      
21 2-3   U  U  U  -      
22 2-6   -  V  -         
23 3-4   W  -  X         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

GIWLM WVUJA SHULG ULHRW IAFLS S
-------------------------------
