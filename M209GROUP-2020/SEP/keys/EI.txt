EFFECTIVE PERIOD:
07-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 2-0   B  B  B  B  -  B
03 2-0   -  -  C  C  C  -
04 2-0   D  -  -  D  -  D
05 2-0   -  -  E  E  E  -
06 2-0   -  F  F  -  -  -
07 2-0   -  G  -  -  G  -
08 2-0   -  H  -  -  -  -
09 0-3   I  -  I  -  -  I
10 0-3   -  J  J  -  -  J
11 0-3   -  -  K  K  K  -
12 0-3   -  L  -  -  L  L
13 0-3   -  M  -  M  M  M
14 0-4   N  N  N  N  -  -
15 0-4   O  -  -  -  O  O
16 0-4   P  -  -  -  P  P
17 0-4   -  -  -  Q  Q  -
18 0-4   R  R  R  R  -   
19 0-4   S  S  S  S  -   
20 0-5   -  T  -  T      
21 0-6   -  U  U  U      
22 1-5   V  -  V         
23 2-3   -  X  X         
24 2-3   X  Y            
25 2-3   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SKCNZ ZTPZR KYZHM JKJTT SIUSN K
-------------------------------
