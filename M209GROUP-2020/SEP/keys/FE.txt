EFFECTIVE PERIOD:
29-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  A
02 2-0   B  B  B  B  B  -
03 2-0   C  -  C  C  -  -
04 2-0   D  D  -  D  -  -
05 2-0   -  E  -  E  -  E
06 0-3   -  F  -  F  F  -
07 0-3   G  -  G  -  -  -
08 0-3   H  -  H  -  -  -
09 0-3   -  I  -  -  I  I
10 0-3   -  J  J  J  J  J
11 0-3   -  K  K  -  -  K
12 0-3   -  -  -  -  L  L
13 0-6   M  -  M  M  M  -
14 0-6   N  -  N  -  N  N
15 0-6   -  -  O  -  O  -
16 1-3   P  P  -  -  -  -
17 1-5   Q  -  Q  Q  Q  -
18 1-6   R  -  -  -  -   
19 1-6   -  S  S  S  -   
20 2-3   -  T  T  T      
21 2-6   U  -  -  -      
22 2-6   V  V  -         
23 2-6   W  X  -         
24 2-6   -  -            
25 3-6   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VAFVK XNTJF KVWFO QAVNV AMMMV A
-------------------------------
