EFFECTIVE PERIOD:
05-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  -
02 2-0   B  B  -  -  B  B
03 0-3   C  -  C  -  -  -
04 0-4   D  D  -  D  -  D
05 0-4   -  E  E  -  E  -
06 0-4   -  -  -  -  F  F
07 0-4   G  G  -  G  -  G
08 0-4   -  -  H  -  -  -
09 0-4   -  I  -  I  I  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  -  -  K  -
12 0-4   -  -  -  L  -  -
13 0-4   -  -  -  M  -  M
14 0-5   N  N  N  -  N  -
15 0-5   O  -  O  -  -  -
16 0-5   P  -  -  P  P  P
17 0-5   Q  -  -  Q  Q  Q
18 0-6   -  R  -  R  -   
19 0-6   S  S  S  S  -   
20 0-6   -  -  T  -      
21 0-6   U  -  -  -      
22 1-3   V  V  V         
23 2-3   -  -  X         
24 2-5   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OCLQU YTDOI MAAMF UFWRK QRGOW R
-------------------------------
