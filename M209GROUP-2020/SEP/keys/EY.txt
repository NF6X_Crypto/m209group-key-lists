EFFECTIVE PERIOD:
23-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  A
02 2-0   -  B  -  B  -  -
03 2-0   C  -  -  C  C  C
04 2-0   -  D  -  -  -  D
05 2-0   -  -  -  E  -  -
06 0-3   -  F  F  F  F  F
07 0-3   -  G  -  G  G  G
08 0-3   H  H  -  -  -  -
09 0-3   I  I  I  I  I  I
10 0-3   J  -  -  -  J  -
11 0-3   K  -  -  -  K  -
12 0-3   L  -  L  L  -  -
13 0-3   M  M  M  M  M  M
14 0-3   -  N  N  -  -  -
15 0-4   -  O  -  O  O  -
16 0-4   P  -  -  P  -  P
17 0-5   Q  Q  Q  -  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  -  -  -  S   
20 0-6   -  T  T  -      
21 0-6   U  U  U  U      
22 0-6   V  V  V         
23 0-6   W  -  -         
24 0-6   -  -            
25 0-6   -  -            
26 1-4   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

RBQNZ OPYML VDNMJ UGJQO EMSRQ F
-------------------------------
