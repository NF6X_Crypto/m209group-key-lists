EFFECTIVE PERIOD:
17-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ES
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  A
02 0-3   -  B  B  -  -  B
03 0-3   -  -  C  -  C  C
04 0-3   -  -  -  D  -  D
05 0-3   E  -  -  E  E  E
06 0-3   F  -  -  -  -  -
07 0-3   G  G  -  -  -  -
08 0-3   H  H  H  H  H  -
09 0-3   I  I  I  -  -  -
10 0-3   J  -  J  -  -  J
11 0-4   -  -  -  -  K  K
12 0-4   -  L  -  -  L  L
13 0-4   M  -  M  M  M  -
14 0-4   -  -  -  N  N  -
15 0-4   O  O  O  O  -  O
16 0-4   P  -  P  P  -  P
17 0-5   -  -  -  -  Q  -
18 0-5   -  -  R  R  -   
19 0-5   -  S  S  -  -   
20 0-5   -  T  -  -      
21 0-6   -  -  -  U      
22 1-2   V  -  -         
23 2-5   -  X  -         
24 3-4   -  -            
25 3-4   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

RYXJP AAANL UPZJZ GLPUT GPWKY P
-------------------------------
