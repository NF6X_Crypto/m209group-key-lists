EFFECTIVE PERIOD:
28-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  A
02 2-0   B  -  -  -  -  -
03 2-0   C  -  -  -  C  C
04 2-0   D  D  -  D  D  D
05 2-0   -  E  E  E  -  E
06 0-3   -  -  F  -  F  F
07 0-4   G  -  G  G  -  G
08 0-4   H  -  -  H  -  -
09 0-4   -  -  -  -  I  -
10 0-4   -  -  -  J  -  J
11 0-5   -  -  -  -  K  -
12 0-5   -  L  L  L  -  -
13 0-5   M  M  -  -  M  -
14 0-5   N  -  N  N  N  -
15 0-5   -  O  -  O  O  O
16 0-5   P  -  P  P  -  -
17 0-5   -  Q  Q  -  Q  -
18 0-5   -  R  -  -  -   
19 0-5   S  S  -  S  S   
20 0-5   T  -  -  T      
21 0-6   U  U  U  U      
22 0-6   -  -  V         
23 0-6   W  X  X         
24 1-3   X  -            
25 2-4   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IPXXL KKVXG QVKUI NVAQZ PQUNU I
-------------------------------
