EFFECTIVE PERIOD:
24-SEP-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   -  B  -  -  -  B
03 0-3   C  -  -  C  C  C
04 0-3   D  -  -  -  D  -
05 0-3   E  E  -  E  -  -
06 0-3   F  F  F  F  -  -
07 0-3   -  -  G  -  -  -
08 0-3   H  -  -  H  -  H
09 0-4   I  I  -  -  -  I
10 0-4   J  -  J  J  -  J
11 0-4   K  K  K  K  K  K
12 0-4   L  L  L  -  -  -
13 0-5   M  M  -  M  M  M
14 0-5   -  -  N  N  N  N
15 0-5   -  O  O  -  O  -
16 0-6   -  P  -  P  -  -
17 0-6   -  -  -  Q  -  -
18 0-6   -  R  R  -  -   
19 0-6   S  S  -  -  S   
20 0-6   -  T  T  -      
21 0-6   U  U  -  U      
22 2-5   V  -  -         
23 3-4   W  X  -         
24 3-5   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WTNUQ CKFTO MMBIO TXINX VUOKO P
-------------------------------
