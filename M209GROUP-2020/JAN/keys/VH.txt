EFFECTIVE PERIOD:
16-JAN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  -  -  B  B
03 1-0   C  -  C  C  C  -
04 1-0   D  -  -  -  -  -
05 1-0   E  E  -  -  E  E
06 0-3   F  F  F  -  F  -
07 0-4   G  -  -  G  -  -
08 0-4   H  H  H  -  -  -
09 0-4   -  -  I  I  -  I
10 0-4   -  -  J  -  J  -
11 0-4   -  -  K  -  -  -
12 0-4   -  -  L  -  -  -
13 0-5   M  -  M  -  M  M
14 0-5   N  N  -  -  -  N
15 0-5   O  O  -  O  O  O
16 0-5   -  -  -  -  P  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   R  R  R  R  -   
19 0-5   -  S  S  -  -   
20 0-5   -  -  -  T      
21 0-6   -  U  -  U      
22 1-4   V  -  -         
23 1-5   -  -  -         
24 1-5   X  Y            
25 1-5   -  -            
26 2-4   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

BKROV VLHZT LJNQI SKYLC NCNZS R
-------------------------------
