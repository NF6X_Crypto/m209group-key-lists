EFFECTIVE PERIOD:
24-JAN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  -  B  -  B  B
03 2-0   -  -  -  C  C  -
04 2-0   D  D  -  -  D  -
05 2-0   -  E  -  -  E  E
06 2-0   -  F  F  F  -  F
07 2-0   G  -  -  G  -  -
08 2-0   H  H  H  H  -  H
09 2-0   I  I  -  -  I  -
10 2-0   -  -  J  -  J  -
11 2-0   -  K  -  -  K  K
12 0-3   L  -  L  -  -  -
13 0-4   M  -  -  -  -  M
14 0-4   N  N  N  N  -  N
15 0-4   O  O  -  O  O  -
16 0-4   -  -  -  P  -  P
17 0-5   -  -  Q  -  -  Q
18 0-6   -  -  R  R  -   
19 0-6   S  -  S  S  -   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 0-6   -  -  V         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   -  Z            
26 1-3   -               
27 1-4                   
-------------------------------
26 LETTER CHECK

SEMRB QVLZJ JRIZY PONNV HKQQY U
-------------------------------
