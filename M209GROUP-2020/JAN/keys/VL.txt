EFFECTIVE PERIOD:
20-JAN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 0-3   -  B  -  -  B  B
03 0-3   -  C  C  -  C  C
04 0-3   -  D  D  D  -  -
05 0-3   E  -  E  E  -  -
06 0-4   F  F  -  F  -  -
07 0-4   G  -  G  -  -  -
08 0-4   H  H  -  H  -  H
09 0-4   I  I  I  I  I  -
10 0-4   -  J  J  -  J  J
11 0-5   K  -  K  K  K  K
12 0-5   -  -  -  L  L  L
13 0-5   M  M  -  -  M  M
14 0-5   -  N  -  N  -  -
15 0-6   -  -  O  -  -  -
16 0-6   P  P  P  -  P  P
17 0-6   Q  Q  Q  -  -  -
18 1-2   R  R  -  -  -   
19 1-3   S  -  -  S  S   
20 1-3   -  -  T  T      
21 1-5   -  U  -  U      
22 3-6   V  -  -         
23 3-6   -  -  -         
24 3-6   -  Y            
25 3-6   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RAQMF HTLSR JZRRU EAWVA WENJK N
-------------------------------
