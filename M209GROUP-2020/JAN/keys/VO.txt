EFFECTIVE PERIOD:
23-JAN-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  A
02 2-0   -  -  -  B  -  B
03 2-0   C  C  C  -  C  -
04 0-3   D  D  D  -  D  D
05 0-4   E  -  -  E  E  E
06 0-4   -  F  -  -  F  -
07 0-4   -  -  G  G  G  -
08 0-5   -  -  -  -  -  H
09 0-5   -  I  -  I  -  I
10 0-5   -  J  -  -  -  J
11 0-5   -  -  K  K  -  K
12 0-5   L  L  L  -  L  L
13 0-5   M  -  -  -  -  -
14 0-6   -  N  -  N  -  -
15 0-6   O  -  O  -  O  -
16 0-6   P  -  P  P  P  P
17 0-6   -  Q  -  Q  -  -
18 0-6   -  -  -  -  -   
19 0-6   -  -  S  S  S   
20 1-3   -  T  T  -      
21 2-5   -  -  U  U      
22 2-5   V  V  -         
23 2-5   W  X  X         
24 2-5   X  -            
25 2-6   Y  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SNKOO RGXTG AUMUV MRLHS KCZKU N
-------------------------------
