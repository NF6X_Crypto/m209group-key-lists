EFFECTIVE PERIOD:
15-FEB-2020 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 0-3   -  -  B  B  B  B
03 0-3   -  -  -  -  C  -
04 0-3   -  D  -  D  D  -
05 0-4   -  -  -  E  -  -
06 0-4   F  F  -  F  -  F
07 0-4   G  -  -  -  G  G
08 0-4   -  -  -  -  -  H
09 0-5   I  I  I  I  -  I
10 0-5   J  J  -  J  J  J
11 0-5   -  K  K  -  -  -
12 0-5   L  L  L  -  -  -
13 0-6   M  M  -  -  M  -
14 0-6   -  -  -  N  N  N
15 0-6   O  O  O  -  O  O
16 0-6   P  P  P  P  -  P
17 0-6   -  Q  -  Q  Q  -
18 0-6   -  R  R  -  -   
19 0-6   -  S  S  -  -   
20 0-6   T  T  T  -      
21 0-6   -  -  -  -      
22 2-3   -  -  V         
23 3-5   W  X  -         
24 4-5   X  Y            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

BQJWW VEARN QKQQW LNNMB WVXBQ W
-------------------------------
