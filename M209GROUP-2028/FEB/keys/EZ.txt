EFFECTIVE PERIOD:
19-FEB-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   B  B  B  -  B  -
03 1-0   -  -  -  C  C  C
04 1-0   D  -  -  -  D  -
05 1-0   -  E  -  -  -  E
06 1-0   -  F  F  F  -  F
07 1-0   -  G  G  G  G  -
08 1-0   -  -  H  H  -  H
09 1-0   -  I  I  -  -  I
10 0-3   J  -  J  -  J  -
11 0-5   K  K  -  -  -  -
12 0-5   -  L  -  L  -  -
13 0-5   M  -  -  -  -  -
14 0-5   -  -  N  -  -  N
15 0-5   O  -  -  -  -  -
16 0-5   -  P  P  -  P  -
17 0-6   Q  -  Q  Q  -  Q
18 0-6   -  -  -  R  R   
19 0-6   -  -  S  -  S   
20 0-6   -  T  -  T      
21 0-6   -  -  U  -      
22 1-5   V  V  -         
23 1-5   -  -  -         
24 1-5   X  Y            
25 2-4   Y  Z            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ANAHK CYMLZ VRTZZ ICMAC TNBZZ U
-------------------------------
