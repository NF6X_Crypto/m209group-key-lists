EFFECTIVE PERIOD:
16-FEB-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   -  B  -  B  B  B
03 2-0   C  C  C  C  -  -
04 2-0   -  -  D  -  -  D
05 2-0   -  -  E  -  -  -
06 2-0   -  -  -  -  F  -
07 0-3   G  -  -  G  G  -
08 0-4   H  H  -  H  H  H
09 0-4   -  I  I  I  -  I
10 0-4   -  J  -  J  -  J
11 0-4   K  K  -  -  K  -
12 0-5   -  -  -  L  L  -
13 0-5   M  M  -  M  -  M
14 0-5   N  -  N  -  -  -
15 0-5   -  O  O  O  O  -
16 0-5   -  -  P  -  P  -
17 0-5   Q  -  -  Q  Q  -
18 0-5   -  R  -  R  -   
19 0-5   S  S  S  S  S   
20 0-6   T  -  T  -      
21 0-6   U  U  -  -      
22 0-6   -  -  -         
23 0-6   W  X  -         
24 1-3   -  Y            
25 2-4   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WCMPU QOOUO SFJRP SKGWU LMHWP S
-------------------------------
