EFFECTIVE PERIOD:
07-FEB-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  B  B  B  B
03 1-0   C  -  -  -  -  C
04 1-0   -  -  -  -  -  -
05 1-0   E  E  E  E  E  -
06 1-0   -  F  -  -  F  F
07 2-0   G  -  -  -  G  -
08 0-3   -  -  -  H  H  -
09 0-4   I  -  -  -  -  I
10 0-4   -  J  J  J  -  -
11 0-5   -  -  K  K  K  -
12 0-5   L  L  L  -  -  L
13 0-5   M  M  M  M  -  -
14 0-5   -  -  -  N  N  N
15 0-5   O  -  -  -  O  O
16 0-6   P  -  P  -  -  -
17 0-6   -  -  -  -  -  -
18 0-6   -  -  -  R  -   
19 0-6   -  S  S  -  S   
20 0-6   T  -  T  -      
21 0-6   U  U  U  U      
22 1-5   V  V  -         
23 1-5   -  X  X         
24 1-5   X  Y            
25 3-4   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MKKPR NLLUS FBVSK WEISN KTUJZ S
-------------------------------
