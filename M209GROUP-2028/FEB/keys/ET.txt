EFFECTIVE PERIOD:
13-FEB-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ET
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   -  C  -  -  C  -
04 2-0   D  -  -  -  -  D
05 2-0   -  -  -  -  E  E
06 2-0   -  -  F  F  -  -
07 2-0   G  G  G  G  G  G
08 2-0   -  -  -  H  -  H
09 2-0   I  I  -  -  I  -
10 2-0   J  J  J  J  -  J
11 2-0   K  K  K  -  K  -
12 2-0   -  -  -  L  -  L
13 0-3   M  -  -  -  M  -
14 0-3   -  N  -  N  -  N
15 0-3   -  O  O  -  -  -
16 0-3   P  P  P  P  P  P
17 0-4   -  Q  -  Q  Q  Q
18 0-5   -  -  -  R  R   
19 0-5   -  -  S  S  -   
20 0-5   -  -  T  -      
21 0-6   U  -  U  -      
22 0-6   -  -  -         
23 0-6   W  X  -         
24 1-5   -  -            
25 2-3   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NZQFF POHQF ZURQK GONJS ANFJW W
-------------------------------
