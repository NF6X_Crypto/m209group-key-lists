EFFECTIVE PERIOD:
06-APR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  -  -  B  B  -
03 1-0   -  -  C  C  C  -
04 1-0   D  D  -  D  D  D
05 1-0   E  -  E  -  E  -
06 1-0   -  F  F  F  F  -
07 2-0   -  G  G  -  -  -
08 0-3   -  H  H  -  -  -
09 0-5   I  I  -  I  -  I
10 0-5   J  J  -  J  -  J
11 0-5   -  -  K  K  -  K
12 0-5   L  -  -  L  L  -
13 0-5   -  -  -  -  -  -
14 0-5   N  N  N  -  N  -
15 0-5   -  -  O  -  O  O
16 0-6   -  -  -  -  -  -
17 0-6   Q  -  -  -  -  Q
18 0-6   -  R  R  -  -   
19 0-6   -  S  -  S  S   
20 1-5   -  -  T  T      
21 1-5   U  -  U  U      
22 1-5   -  V  V         
23 1-5   W  X  X         
24 1-6   X  -            
25 2-3   Y  Z            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TMLOI KIAPU ZSNLP RIJDV PRSHS Z
-------------------------------
