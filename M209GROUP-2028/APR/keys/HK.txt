EFFECTIVE PERIOD:
22-APR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 2-0   B  -  B  B  -  B
03 2-0   C  C  -  -  -  C
04 0-3   D  -  D  -  D  -
05 0-4   -  -  -  -  -  -
06 0-4   -  F  -  F  F  F
07 0-4   G  -  -  G  -  -
08 0-4   H  H  -  H  -  -
09 0-5   -  -  -  I  -  -
10 0-5   J  J  J  -  -  -
11 0-5   K  K  K  -  K  -
12 0-5   -  L  L  L  -  L
13 0-5   -  M  -  M  -  -
14 0-5   -  -  -  N  -  N
15 0-5   O  O  O  -  O  -
16 0-5   P  -  P  -  P  -
17 0-5   Q  -  -  Q  Q  Q
18 0-5   R  -  -  -  R   
19 0-5   -  -  S  S  -   
20 0-6   -  T  T  T      
21 0-6   -  -  U  U      
22 0-6   -  V  -         
23 0-6   W  -  -         
24 0-6   X  Y            
25 0-6   Y  Z            
26 2-3   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

INCQP PXPWK JMGQF EHFVB PJSUS A
-------------------------------
