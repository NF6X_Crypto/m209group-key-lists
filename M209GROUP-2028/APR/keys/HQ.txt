EFFECTIVE PERIOD:
28-APR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   -  B  B  -  B  B
03 2-0   C  C  C  C  C  -
04 2-0   D  -  -  D  -  D
05 2-0   E  E  -  E  E  -
06 2-0   -  -  -  F  F  -
07 0-4   -  G  G  G  G  G
08 0-4   H  -  H  H  -  H
09 0-4   -  I  I  -  -  I
10 0-4   J  -  -  -  J  -
11 0-4   -  K  K  -  -  K
12 0-6   L  L  -  L  L  L
13 0-6   -  M  M  M  -  -
14 0-6   N  -  N  N  N  -
15 0-6   -  O  O  O  -  -
16 0-6   -  P  P  -  P  P
17 0-6   -  -  -  -  -  -
18 1-4   -  -  -  R  R   
19 2-4   S  -  S  -  S   
20 2-4   -  T  -  -      
21 2-4   U  U  -  -      
22 2-4   -  -  V         
23 2-6   W  -  -         
24 3-4   X  -            
25 3-5   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

AANAB AMKKB TNUAU ZNUKM NTAUK U
-------------------------------
