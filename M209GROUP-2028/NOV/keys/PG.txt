EFFECTIVE PERIOD:
12-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  -  A
02 0-3   -  B  B  B  -  -
03 0-3   -  -  -  C  -  -
04 0-3   D  -  D  -  D  D
05 0-3   -  E  E  -  -  E
06 0-3   -  -  -  -  -  -
07 0-3   G  -  G  G  G  -
08 0-3   -  H  H  -  -  -
09 0-3   -  -  -  I  I  I
10 0-3   J  J  J  -  J  -
11 0-3   -  -  K  K  -  K
12 0-3   -  L  -  L  L  L
13 0-4   -  M  M  M  -  -
14 0-4   N  N  -  -  -  -
15 0-4   -  -  O  O  O  O
16 0-4   P  -  -  -  P  -
17 0-4   -  Q  -  -  -  -
18 0-4   R  R  -  R  R   
19 0-4   S  -  S  -  S   
20 0-5   T  T  T  -      
21 0-6   U  -  U  -      
22 0-6   -  -  -         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   -  -            
26 1-2   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

ORICH UMSBL NABLN NMAGT UMOJS A
-------------------------------
