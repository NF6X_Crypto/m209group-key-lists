EFFECTIVE PERIOD:
17-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  -  -  B  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  -  D  -  D
05 1-0   E  -  -  E  E  E
06 0-3   F  -  F  -  -  F
07 0-3   G  G  -  -  -  -
08 0-3   H  -  H  -  H  -
09 0-3   I  -  -  I  I  I
10 0-3   -  J  J  -  -  -
11 0-3   K  -  K  -  -  -
12 0-6   L  L  -  L  -  -
13 0-6   M  -  M  M  M  M
14 0-6   N  N  -  N  N  -
15 0-6   -  -  -  -  O  -
16 0-6   -  P  -  P  P  P
17 0-6   Q  -  -  Q  -  -
18 1-3   R  R  R  -  -   
19 1-3   -  -  -  S  S   
20 1-3   -  T  T  T      
21 1-3   U  -  U  -      
22 1-6   -  V  V         
23 2-3   -  X  -         
24 3-4   -  Y            
25 3-4   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

DOREO ROVVB SRVNZ AKUJZ ONAKU K
-------------------------------
