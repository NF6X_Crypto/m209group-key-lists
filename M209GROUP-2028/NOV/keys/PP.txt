EFFECTIVE PERIOD:
21-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 2-0   -  B  -  -  -  B
03 2-0   -  C  -  -  C  -
04 0-3   D  -  D  D  D  D
05 0-3   E  -  E  E  E  E
06 0-3   F  -  F  -  -  -
07 0-3   -  G  -  G  -  -
08 0-3   -  H  H  -  -  H
09 0-5   I  -  I  -  I  -
10 0-5   -  -  -  J  -  -
11 0-5   K  K  K  -  K  K
12 0-5   -  -  -  L  -  -
13 0-5   -  -  M  -  -  -
14 0-6   -  -  -  N  N  -
15 0-6   O  O  O  O  O  -
16 0-6   -  -  -  P  -  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   S  S  S  -  S   
20 0-6   T  -  T  T      
21 0-6   -  U  U  -      
22 1-2   -  V  -         
23 1-4   W  -  -         
24 2-5   X  Y            
25 3-5   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

NIKLN VTSTE TTKMS KJTRC ILWKO P
-------------------------------
