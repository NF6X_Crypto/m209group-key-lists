EFFECTIVE PERIOD:
05-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   B  -  B  -  -  B
03 1-0   C  C  -  C  C  C
04 2-0   D  D  -  -  D  -
05 2-0   E  E  -  -  -  E
06 2-0   F  F  F  F  F  F
07 2-0   -  -  G  -  -  G
08 2-0   H  H  -  H  -  H
09 2-0   I  -  I  -  I  -
10 2-0   -  -  -  -  J  J
11 2-0   -  -  -  -  -  K
12 2-0   L  -  -  -  -  L
13 2-0   M  -  M  M  M  -
14 0-3   N  N  N  N  -  -
15 0-4   O  O  O  -  O  O
16 0-5   P  P  P  P  -  -
17 0-5   -  Q  Q  Q  Q  -
18 0-6   -  R  R  R  R   
19 0-6   S  -  -  -  -   
20 0-6   -  T  -  -      
21 0-6   -  U  -  U      
22 1-2   V  -  -         
23 1-2   W  X  -         
24 1-2   -  Y            
25 1-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YXNWI RUGUU RKLYX RAIWG DHMHW A
-------------------------------
