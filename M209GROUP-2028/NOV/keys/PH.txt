EFFECTIVE PERIOD:
13-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  -  B  -  B  B
03 1-0   C  C  -  C  -  -
04 1-0   -  D  -  -  -  -
05 2-0   -  E  -  -  -  -
06 2-0   F  F  -  F  F  F
07 0-3   -  G  G  -  -  G
08 0-3   H  -  -  H  H  -
09 0-3   -  I  I  -  I  I
10 0-3   J  -  J  J  J  J
11 0-3   K  K  K  -  -  K
12 0-5   L  -  L  -  L  -
13 0-5   M  -  -  -  M  -
14 0-5   -  N  N  N  N  -
15 0-5   O  -  -  -  -  O
16 1-2   -  P  -  P  P  -
17 1-2   -  -  -  Q  Q  Q
18 1-2   R  -  R  R  R   
19 1-2   S  S  S  -  -   
20 1-3   -  -  T  T      
21 2-4   U  -  U  -      
22 2-4   -  V  -         
23 2-4   W  X  X         
24 2-4   X  -            
25 3-5   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MQOOW YMWPY UMAVL QAQUV TAOUQ T
-------------------------------
