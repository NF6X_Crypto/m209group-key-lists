SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF NOV 2028

    01 NOV 2028  00:00-23:59 GMT:  USE KEY OV
    02 NOV 2028  00:00-23:59 GMT:  USE KEY OW
    03 NOV 2028  00:00-23:59 GMT:  USE KEY OX
    04 NOV 2028  00:00-23:59 GMT:  USE KEY OY
    05 NOV 2028  00:00-23:59 GMT:  USE KEY OZ
    06 NOV 2028  00:00-23:59 GMT:  USE KEY PA
    07 NOV 2028  00:00-23:59 GMT:  USE KEY PB
    08 NOV 2028  00:00-23:59 GMT:  USE KEY PC
    09 NOV 2028  00:00-23:59 GMT:  USE KEY PD
    10 NOV 2028  00:00-23:59 GMT:  USE KEY PE
    11 NOV 2028  00:00-23:59 GMT:  USE KEY PF
    12 NOV 2028  00:00-23:59 GMT:  USE KEY PG
    13 NOV 2028  00:00-23:59 GMT:  USE KEY PH
    14 NOV 2028  00:00-23:59 GMT:  USE KEY PI
    15 NOV 2028  00:00-23:59 GMT:  USE KEY PJ
    16 NOV 2028  00:00-23:59 GMT:  USE KEY PK
    17 NOV 2028  00:00-23:59 GMT:  USE KEY PL
    18 NOV 2028  00:00-23:59 GMT:  USE KEY PM
    19 NOV 2028  00:00-23:59 GMT:  USE KEY PN
    20 NOV 2028  00:00-23:59 GMT:  USE KEY PO
    21 NOV 2028  00:00-23:59 GMT:  USE KEY PP
    22 NOV 2028  00:00-23:59 GMT:  USE KEY PQ
    23 NOV 2028  00:00-23:59 GMT:  USE KEY PR
    24 NOV 2028  00:00-23:59 GMT:  USE KEY PS
    25 NOV 2028  00:00-23:59 GMT:  USE KEY PT
    26 NOV 2028  00:00-23:59 GMT:  USE KEY PU
    27 NOV 2028  00:00-23:59 GMT:  USE KEY PV
    28 NOV 2028  00:00-23:59 GMT:  USE KEY PW
    29 NOV 2028  00:00-23:59 GMT:  USE KEY PX
    30 NOV 2028  00:00-23:59 GMT:  USE KEY PY

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 2-0   -  B  B  -  B  -
03 2-0   -  -  C  -  -  C
04 0-3   D  -  D  -  -  -
05 0-3   E  E  -  E  E  -
06 0-3   -  -  -  F  F  -
07 0-3   -  G  G  -  -  G
08 0-3   -  H  H  H  H  H
09 0-3   I  -  -  -  I  I
10 0-3   -  -  -  J  -  -
11 0-3   -  K  K  -  K  -
12 0-3   -  L  L  L  -  -
13 0-4   -  M  M  -  M  M
14 0-4   N  -  -  N  N  N
15 0-4   -  -  O  O  O  -
16 0-4   P  -  -  P  -  -
17 0-4   Q  Q  -  -  Q  Q
18 0-6   -  -  -  -  -   
19 0-6   S  -  -  S  -   
20 0-6   T  T  T  -      
21 0-6   U  U  U  U      
22 0-6   V  -  -         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   -  -            
26 2-4   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

AQQPA EAQIO ANMXO MPMII XMRQY Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  -  C  C
04 1-0   -  -  D  -  -  -
05 1-0   E  E  -  -  E  -
06 2-0   F  -  -  -  -  -
07 2-0   -  G  -  -  -  -
08 2-0   -  H  H  H  -  -
09 2-0   I  I  I  -  I  I
10 2-0   J  J  -  J  -  J
11 2-0   -  K  K  K  -  -
12 0-4   -  -  -  -  L  -
13 0-4   M  M  M  M  -  -
14 0-4   -  -  -  N  N  N
15 0-4   -  O  -  -  -  -
16 0-4   -  -  P  P  -  -
17 0-4   Q  -  -  Q  Q  Q
18 1-2   R  -  -  R  -   
19 1-3   -  -  S  -  S   
20 1-4   T  T  T  T      
21 2-4   U  -  -  U      
22 2-4   V  V  V         
23 2-4   -  X  -         
24 2-4   X  -            
25 2-5   Y  Z            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

TRJMR AJUAT TOAUZ UUOVZ UTMUN V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  -  B  B  B
03 1-0   C  -  C  -  -  C
04 2-0   -  D  -  -  D  -
05 0-4   E  E  -  E  -  -
06 0-4   F  F  -  F  F  -
07 0-4   -  G  -  -  -  G
08 0-4   H  -  H  -  -  H
09 0-4   I  -  I  I  -  -
10 0-5   J  J  J  -  J  -
11 0-5   -  K  -  K  -  K
12 0-6   -  L  -  -  -  L
13 0-6   M  M  -  -  -  -
14 0-6   -  N  -  N  N  -
15 0-6   -  O  -  -  O  -
16 0-6   -  -  -  P  P  P
17 0-6   Q  -  Q  -  Q  -
18 0-6   R  R  R  R  -   
19 0-6   S  -  S  S  S   
20 1-4   T  -  T  T      
21 1-5   -  -  U  -      
22 2-5   -  -  V         
23 3-6   -  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OWHSI WKOMR KIAZU GGXRO GEXIO O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   -  B  B  B  -  B
03 1-0   C  C  C  C  -  C
04 1-0   D  D  -  D  -  D
05 2-0   E  -  E  E  E  E
06 0-3   F  -  -  -  F  F
07 0-3   -  G  G  -  G  -
08 0-4   -  -  -  H  -  H
09 0-5   I  I  -  -  -  -
10 0-5   -  -  J  -  J  -
11 0-5   -  -  -  K  K  -
12 0-5   L  L  L  L  L  L
13 0-5   M  -  -  M  -  -
14 0-5   -  -  N  -  N  -
15 0-5   -  -  O  O  O  O
16 0-5   -  P  -  -  P  -
17 0-5   Q  Q  Q  -  Q  Q
18 0-5   R  R  R  R  -   
19 0-6   S  -  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  -  -  U      
22 1-3   -  -  -         
23 1-6   -  -  -         
24 3-4   X  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RPVWU SGZXG LMWVZ FJNQX BVTZQ G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   B  -  B  -  -  B
03 1-0   C  C  -  C  C  C
04 2-0   D  D  -  -  D  -
05 2-0   E  E  -  -  -  E
06 2-0   F  F  F  F  F  F
07 2-0   -  -  G  -  -  G
08 2-0   H  H  -  H  -  H
09 2-0   I  -  I  -  I  -
10 2-0   -  -  -  -  J  J
11 2-0   -  -  -  -  -  K
12 2-0   L  -  -  -  -  L
13 2-0   M  -  M  M  M  -
14 0-3   N  N  N  N  -  -
15 0-4   O  O  O  -  O  O
16 0-5   P  P  P  P  -  -
17 0-5   -  Q  Q  Q  Q  -
18 0-6   -  R  R  R  R   
19 0-6   S  -  -  -  -   
20 0-6   -  T  -  -      
21 0-6   -  U  -  U      
22 1-2   V  -  -         
23 1-2   W  X  -         
24 1-2   -  Y            
25 1-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YXNWI RUGUU RKLYX RAIWG DHMHW A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   B  -  -  -  B  -
03 2-0   C  -  -  -  C  C
04 0-3   -  D  D  -  D  D
05 0-3   -  -  -  -  E  E
06 0-3   F  F  -  F  -  F
07 0-3   G  G  -  G  -  -
08 0-3   H  -  H  -  -  H
09 0-3   -  I  I  I  -  -
10 0-4   J  -  J  J  -  J
11 0-4   K  K  -  K  K  K
12 0-4   -  L  L  L  L  -
13 0-4   M  M  -  M  -  M
14 0-4   N  N  -  -  -  N
15 0-6   -  -  -  O  O  -
16 0-6   -  P  P  -  -  -
17 0-6   -  -  Q  Q  Q  -
18 1-2   -  -  -  -  -   
19 1-3   -  S  S  -  -   
20 1-3   T  -  -  T      
21 1-5   U  -  -  -      
22 2-4   -  -  V         
23 3-6   -  X  X         
24 3-6   -  -            
25 3-6   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OEVQN NZNRQ VILNR GPAUA RXFNP S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  -  -  B  B
03 1-0   C  C  C  C  C  C
04 1-0   -  D  -  -  D  -
05 1-0   E  E  -  E  -  -
06 0-3   -  -  -  F  -  F
07 0-5   -  -  G  -  G  -
08 0-5   -  -  H  H  H  -
09 0-5   -  I  -  -  I  -
10 0-5   J  J  -  J  J  J
11 0-5   K  -  -  -  -  K
12 0-6   -  -  L  -  -  L
13 0-6   -  -  M  M  -  M
14 0-6   N  -  -  -  -  -
15 0-6   O  O  -  -  O  O
16 0-6   -  -  P  P  -  -
17 0-6   Q  -  Q  -  -  Q
18 1-3   R  R  -  R  R   
19 1-6   -  -  -  S  S   
20 2-5   T  -  T  -      
21 3-4   U  -  U  U      
22 3-5   V  V  V         
23 4-5   W  X  -         
24 5-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NQTOQ IAUAY LMPJM SVVML HUOKI A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  -  -  -  -  B
03 1-0   C  -  -  C  -  C
04 1-0   -  D  D  -  -  D
05 1-0   E  E  -  E  E  -
06 1-0   F  -  -  F  F  F
07 1-0   G  G  G  G  G  -
08 0-3   -  -  H  H  H  H
09 0-4   I  -  -  -  I  I
10 0-4   -  J  -  -  -  J
11 0-6   K  -  K  K  -  K
12 0-6   -  -  -  L  L  -
13 0-6   M  M  M  -  -  -
14 0-6   -  N  -  -  -  -
15 0-6   O  O  O  -  O  -
16 1-3   P  -  -  P  P  -
17 1-6   Q  Q  -  -  -  Q
18 2-5   -  R  R  R  R   
19 3-4   -  -  S  S  S   
20 3-4   -  T  -  -      
21 3-5   U  U  U  -      
22 4-5   V  -  V         
23 4-5   -  X  X         
24 4-6   X  Y            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VMALH VZHYP PJNVR ZAWZU NIHVN V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 0-3   -  B  -  -  B  -
03 0-3   -  -  -  -  -  C
04 0-3   D  -  D  D  D  D
05 0-3   E  -  E  E  E  -
06 0-3   F  F  F  F  -  -
07 0-3   G  -  -  -  G  G
08 0-3   -  H  -  -  H  -
09 0-3   I  I  I  -  -  I
10 0-3   -  J  -  -  -  -
11 0-3   -  K  -  -  K  K
12 0-4   L  -  L  L  L  L
13 0-4   M  -  -  M  M  -
14 0-4   -  N  -  N  -  N
15 0-4   O  -  O  O  -  O
16 0-4   -  P  P  -  P  -
17 0-4   -  Q  -  -  -  Q
18 0-4   R  R  R  -  -   
19 0-4   -  S  -  S  -   
20 0-5   -  T  T  -      
21 0-5   U  -  -  U      
22 0-5   -  -  -         
23 0-5   -  -  X         
24 0-5   X  Y            
25 0-6   -  Z            
26 1-2   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

OAUDS CSZIA DHPSY RCBSH QVIPM K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  -  B  -  B  -
03 2-0   C  -  -  -  -  -
04 2-0   -  -  -  D  -  -
05 2-0   E  -  -  -  E  E
06 2-0   -  -  -  -  F  -
07 2-0   G  G  -  G  G  G
08 0-5   -  -  H  -  H  H
09 0-5   -  I  I  I  -  I
10 0-5   J  -  -  -  -  -
11 0-5   K  K  K  K  K  K
12 0-5   L  L  -  L  L  L
13 0-5   M  M  -  -  -  M
14 0-6   -  -  N  N  N  N
15 0-6   O  -  -  -  -  -
16 1-3   P  -  P  P  -  -
17 1-5   -  -  Q  Q  Q  -
18 1-6   R  R  R  -  -   
19 1-6   S  -  S  S  -   
20 1-6   -  T  T  -      
21 2-5   U  -  -  U      
22 2-6   V  V  -         
23 2-6   -  -  -         
24 2-6   X  Y            
25 2-6   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

VKVUC YIPHW ZRIUR VZUTX OWRSS I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 0-3   B  B  -  -  -  -
03 0-3   -  -  C  -  C  -
04 0-3   -  -  D  D  -  -
05 0-3   E  -  E  E  E  E
06 0-3   -  -  F  -  -  F
07 0-3   G  -  G  -  G  G
08 0-3   -  H  -  H  H  -
09 0-3   I  I  -  -  I  -
10 0-4   J  J  -  J  -  J
11 0-4   K  -  -  K  K  -
12 0-4   -  -  -  -  -  L
13 0-4   M  M  -  M  M  -
14 0-4   N  -  N  -  N  N
15 0-4   -  O  -  O  O  -
16 0-4   P  -  -  -  P  -
17 0-4   Q  -  -  -  Q  -
18 0-4   -  -  R  R  -   
19 0-5   -  -  S  -  -   
20 0-5   -  T  T  T      
21 0-6   -  U  U  U      
22 0-6   V  -  -         
23 0-6   W  -  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SJMIL UJXIR EGAWL MZARK JNDVJ A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  -  A
02 0-3   -  B  B  B  -  -
03 0-3   -  -  -  C  -  -
04 0-3   D  -  D  -  D  D
05 0-3   -  E  E  -  -  E
06 0-3   -  -  -  -  -  -
07 0-3   G  -  G  G  G  -
08 0-3   -  H  H  -  -  -
09 0-3   -  -  -  I  I  I
10 0-3   J  J  J  -  J  -
11 0-3   -  -  K  K  -  K
12 0-3   -  L  -  L  L  L
13 0-4   -  M  M  M  -  -
14 0-4   N  N  -  -  -  -
15 0-4   -  -  O  O  O  O
16 0-4   P  -  -  -  P  -
17 0-4   -  Q  -  -  -  -
18 0-4   R  R  -  R  R   
19 0-4   S  -  S  -  S   
20 0-5   T  T  T  -      
21 0-6   U  -  U  -      
22 0-6   -  -  -         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   -  -            
26 1-2   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

ORICH UMSBL NABLN NMAGT UMOJS A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  -  B  -  B  B
03 1-0   C  C  -  C  -  -
04 1-0   -  D  -  -  -  -
05 2-0   -  E  -  -  -  -
06 2-0   F  F  -  F  F  F
07 0-3   -  G  G  -  -  G
08 0-3   H  -  -  H  H  -
09 0-3   -  I  I  -  I  I
10 0-3   J  -  J  J  J  J
11 0-3   K  K  K  -  -  K
12 0-5   L  -  L  -  L  -
13 0-5   M  -  -  -  M  -
14 0-5   -  N  N  N  N  -
15 0-5   O  -  -  -  -  O
16 1-2   -  P  -  P  P  -
17 1-2   -  -  -  Q  Q  Q
18 1-2   R  -  R  R  R   
19 1-2   S  S  S  -  -   
20 1-3   -  -  T  T      
21 2-4   U  -  U  -      
22 2-4   -  V  -         
23 2-4   W  X  X         
24 2-4   X  -            
25 3-5   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MQOOW YMWPY UMAVL QAQUV TAOUQ T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  -  -  B  B  -
03 1-0   -  C  C  -  -  -
04 1-0   D  D  -  -  -  D
05 1-0   -  E  E  E  E  -
06 2-0   -  F  F  F  F  -
07 2-0   -  -  -  -  -  G
08 0-4   H  -  -  -  -  -
09 0-4   I  -  I  -  -  I
10 0-4   J  J  -  -  J  -
11 0-4   -  -  K  -  -  K
12 0-4   -  -  L  L  -  L
13 0-4   M  -  -  -  M  -
14 0-4   N  N  -  -  -  -
15 0-4   -  -  O  O  O  O
16 1-2   -  -  -  -  -  P
17 1-2   Q  -  -  -  Q  Q
18 1-2   R  R  R  R  -   
19 1-2   S  -  S  S  S   
20 1-4   -  -  -  T      
21 2-3   U  -  U  U      
22 2-5   V  V  -         
23 2-6   W  X  -         
24 2-6   -  Y            
25 2-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

EQSRF YWAUR VQMVZ MKFPJ AFFWR Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  B  -  -  B  B
03 1-0   C  -  -  C  C  -
04 1-0   -  D  -  -  -  -
05 2-0   -  -  E  E  -  -
06 2-0   -  F  F  -  -  F
07 2-0   -  -  G  -  -  G
08 2-0   -  H  -  H  -  H
09 2-0   -  I  I  -  I  -
10 2-0   J  -  J  J  J  -
11 2-0   -  K  -  -  -  -
12 2-0   -  L  -  -  -  -
13 0-3   M  -  M  -  M  -
14 0-3   -  N  -  N  N  N
15 0-3   O  O  -  O  O  O
16 0-4   P  -  -  P  P  P
17 0-5   -  Q  Q  Q  -  -
18 0-5   R  -  R  -  R   
19 0-5   -  S  S  S  S   
20 1-3   T  T  T  -      
21 1-5   U  -  -  U      
22 2-3   V  -  -         
23 2-3   W  -  -         
24 2-3   X  -            
25 2-3   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RGXOA VSJIE MISIQ HWESP KRWLO X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  -  -  -  -
02 0-3   B  B  -  B  B  B
03 0-3   -  C  -  C  C  C
04 0-3   -  -  D  -  -  -
05 0-3   E  -  -  E  -  -
06 0-3   -  -  F  -  F  -
07 0-3   G  G  G  -  -  -
08 0-4   H  -  H  H  H  H
09 0-4   -  -  I  I  I  I
10 0-4   J  -  J  -  J  -
11 0-4   -  K  K  K  K  -
12 0-4   L  L  -  L  -  L
13 0-4   -  -  -  M  -  -
14 0-4   -  N  N  N  -  -
15 0-4   O  -  -  -  O  -
16 0-6   -  -  -  P  P  P
17 0-6   -  Q  Q  -  Q  Q
18 0-6   -  -  -  R  -   
19 0-6   -  -  S  S  S   
20 1-5   T  T  T  -      
21 2-4   -  U  U  -      
22 3-4   V  -  -         
23 3-4   -  X  X         
24 3-4   -  Y            
25 3-4   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

USAWB AANGS ONBWF RWWRU VMTRO G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  -  -  B  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  -  D  -  D
05 1-0   E  -  -  E  E  E
06 0-3   F  -  F  -  -  F
07 0-3   G  G  -  -  -  -
08 0-3   H  -  H  -  H  -
09 0-3   I  -  -  I  I  I
10 0-3   -  J  J  -  -  -
11 0-3   K  -  K  -  -  -
12 0-6   L  L  -  L  -  -
13 0-6   M  -  M  M  M  M
14 0-6   N  N  -  N  N  -
15 0-6   -  -  -  -  O  -
16 0-6   -  P  -  P  P  P
17 0-6   Q  -  -  Q  -  -
18 1-3   R  R  R  -  -   
19 1-3   -  -  -  S  S   
20 1-3   -  T  T  T      
21 1-3   U  -  U  -      
22 1-6   -  V  V         
23 2-3   -  X  -         
24 3-4   -  Y            
25 3-4   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

DOREO ROVVB SRVNZ AKUJZ ONAKU K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 2-0   B  -  -  B  -  B
03 2-0   C  C  C  -  C  C
04 2-0   D  -  D  -  D  D
05 2-0   E  -  E  -  E  E
06 2-0   -  -  F  F  F  F
07 0-4   G  G  -  -  -  G
08 0-4   H  H  H  -  -  -
09 0-4   -  -  -  -  -  I
10 0-4   -  J  -  J  -  -
11 0-4   K  -  -  K  -  -
12 0-4   -  -  L  -  -  L
13 0-4   M  M  -  M  M  M
14 0-4   N  -  N  N  -  -
15 0-4   -  O  O  O  -  O
16 0-6   -  -  P  -  P  -
17 0-6   Q  Q  -  Q  Q  -
18 0-6   -  -  -  R  R   
19 0-6   S  S  -  -  S   
20 0-6   T  -  T  -      
21 0-6   -  U  -  -      
22 2-5   -  V  -         
23 2-6   -  -  -         
24 3-5   X  -            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

TVZAA NHHMJ VRMRN LZIZS HULUC Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  A
02 0-3   -  -  -  -  B  -
03 0-3   C  C  C  C  C  C
04 0-4   D  -  D  -  -  -
05 0-4   E  E  -  -  -  -
06 0-4   -  -  F  F  F  -
07 0-4   G  G  -  G  G  -
08 0-4   H  H  -  H  -  -
09 0-4   -  I  -  -  -  I
10 0-4   J  -  J  -  J  J
11 0-4   K  -  -  K  K  K
12 0-4   -  -  -  -  -  L
13 0-6   -  M  M  -  -  M
14 0-6   N  N  -  N  N  N
15 0-6   O  -  O  O  O  -
16 0-6   P  -  P  -  P  -
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  -  -  -   
19 0-6   -  -  S  -  S   
20 0-6   -  T  -  -      
21 0-6   -  -  U  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-3   -  -            
25 1-5   Y  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NYAQL FBRKN FKDFM AVOOZ NBOMR O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  -  -  -  B  B
03 1-0   -  -  -  -  -  C
04 1-0   -  D  D  D  -  -
05 1-0   E  -  -  -  -  -
06 1-0   F  F  F  F  F  -
07 1-0   -  -  G  -  G  -
08 1-0   -  H  -  -  H  -
09 1-0   -  I  -  -  I  -
10 1-0   J  J  -  J  J  J
11 2-0   -  K  -  -  -  -
12 2-0   -  L  -  L  L  -
13 2-0   M  M  M  M  -  M
14 2-0   -  -  -  N  N  -
15 2-0   -  -  -  -  O  O
16 2-0   P  P  -  -  -  -
17 2-0   -  -  Q  Q  Q  Q
18 2-0   -  -  R  -  -   
19 0-3   S  -  S  -  -   
20 0-6   -  T  T  -      
21 0-6   U  U  -  U      
22 0-6   V  V  V         
23 0-6   W  X  -         
24 1-2   X  -            
25 2-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MKGNH WZYAR LZLCL NABKP UPMYG G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 2-0   -  B  -  -  -  B
03 2-0   -  C  -  -  C  -
04 0-3   D  -  D  D  D  D
05 0-3   E  -  E  E  E  E
06 0-3   F  -  F  -  -  -
07 0-3   -  G  -  G  -  -
08 0-3   -  H  H  -  -  H
09 0-5   I  -  I  -  I  -
10 0-5   -  -  -  J  -  -
11 0-5   K  K  K  -  K  K
12 0-5   -  -  -  L  -  -
13 0-5   -  -  M  -  -  -
14 0-6   -  -  -  N  N  -
15 0-6   O  O  O  O  O  -
16 0-6   -  -  -  P  -  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   S  S  S  -  S   
20 0-6   T  -  T  T      
21 0-6   -  U  U  -      
22 1-2   -  V  -         
23 1-4   W  -  -         
24 2-5   X  Y            
25 3-5   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

NIKLN VTSTE TTKMS KJTRC ILWKO P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   C  -  C  -  -  C
04 2-0   D  -  -  -  -  D
05 2-0   E  E  -  E  -  -
06 2-0   -  -  F  -  F  F
07 2-0   G  -  -  -  G  -
08 2-0   H  -  H  H  -  -
09 0-3   -  -  -  I  I  I
10 0-3   J  -  -  -  J  -
11 0-3   -  K  -  -  K  -
12 0-3   -  -  L  -  L  L
13 0-3   -  -  -  M  M  -
14 0-3   -  N  -  N  N  N
15 0-5   -  -  O  -  -  -
16 0-5   P  P  -  P  P  -
17 0-5   -  -  -  -  -  Q
18 1-3   R  R  -  -  R   
19 1-6   S  S  S  S  -   
20 2-5   -  T  T  T      
21 2-5   U  -  U  U      
22 2-5   V  V  -         
23 2-5   W  -  -         
24 2-6   -  -            
25 2-6   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NXMQO WVXAP XSUNM XITPH PWALU H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 0-3   -  B  B  B  -  B
03 0-3   C  C  C  -  C  -
04 0-3   D  D  D  D  -  -
05 0-3   E  -  -  E  E  E
06 0-3   -  F  F  F  F  -
07 0-3   -  G  G  -  -  G
08 0-3   -  H  -  -  -  H
09 0-4   I  I  -  I  -  I
10 0-4   J  J  -  -  -  J
11 0-4   -  -  K  -  K  K
12 0-4   -  -  -  -  L  -
13 0-4   -  -  -  -  -  M
14 0-4   -  N  -  N  -  -
15 0-4   O  -  O  O  -  -
16 0-4   -  -  P  P  P  -
17 0-4   -  Q  Q  -  -  Q
18 0-4   -  R  -  R  R   
19 0-5   -  -  S  S  S   
20 0-6   -  -  T  -      
21 0-6   U  -  -  -      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 1-2   -  -            
25 1-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HRXSC UAJGM ONVAS PNQDJ KZJRX Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  B  -  B
03 1-0   -  -  C  C  C  -
04 1-0   D  D  D  -  -  D
05 1-0   E  E  -  E  E  -
06 2-0   F  -  -  F  -  F
07 2-0   G  -  -  -  G  G
08 0-3   H  -  -  -  -  -
09 0-3   -  -  I  -  I  -
10 0-3   J  J  J  J  J  J
11 0-3   -  K  K  -  K  -
12 0-3   L  -  -  L  -  -
13 0-3   M  M  -  -  M  M
14 0-4   -  N  N  -  -  N
15 0-5   O  -  O  O  O  O
16 0-5   P  P  -  -  -  -
17 0-5   -  -  Q  -  -  -
18 0-5   R  R  -  -  -   
19 0-5   -  -  S  S  S   
20 1-2   T  -  -  T      
21 1-5   -  -  U  U      
22 2-4   -  -  V         
23 3-5   W  X  -         
24 3-5   X  Y            
25 3-5   -  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QTJUH JCYAF SJQMU JQPYK AEPVO H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 2-0   -  -  B  -  B  -
03 2-0   -  C  C  -  -  C
04 2-0   -  -  D  D  D  -
05 0-4   -  E  E  -  E  -
06 0-4   F  F  -  F  F  F
07 0-5   -  -  -  -  -  -
08 0-5   -  -  -  -  H  H
09 0-5   -  I  -  I  -  -
10 0-5   J  J  J  -  -  J
11 0-5   K  -  -  -  -  K
12 0-6   -  -  -  -  L  L
13 0-6   M  M  -  M  M  -
14 0-6   -  N  -  N  -  N
15 0-6   -  -  O  -  O  O
16 0-6   P  -  -  -  -  -
17 0-6   Q  Q  Q  Q  -  Q
18 0-6   -  R  -  R  R   
19 0-6   S  S  S  -  -   
20 1-4   -  T  T  -      
21 2-5   U  U  U  U      
22 2-6   -  V  -         
23 2-6   W  -  X         
24 2-6   X  -            
25 2-6   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

XXHYU NWKEX AQWOQ NSQMO YRJSI J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   B  -  B  -  B  -
03 2-0   -  -  C  -  -  C
04 2-0   D  D  -  D  D  D
05 2-0   E  -  E  -  -  -
06 0-3   F  F  F  -  F  -
07 0-3   -  G  -  G  G  -
08 0-3   -  H  -  H  -  H
09 0-3   I  I  -  -  I  I
10 0-3   -  J  J  J  J  J
11 0-3   K  -  -  K  -  -
12 0-4   L  -  L  L  -  L
13 0-4   -  -  M  M  -  -
14 0-4   -  N  N  N  -  N
15 0-4   -  -  -  O  -  -
16 0-4   P  -  -  -  P  -
17 0-4   -  Q  Q  -  -  -
18 0-4   R  R  R  R  -   
19 0-4   -  -  -  S  S   
20 0-6   T  -  T  -      
21 0-6   U  -  U  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-2   X  -            
25 1-5   -  Z            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

RNPQJ KRSKG KNRAS WUGSW SRRPK Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   -  B  B  B  B  -
03 2-0   C  -  -  C  C  -
04 2-0   -  D  D  -  -  D
05 2-0   -  -  E  E  E  E
06 2-0   -  -  -  F  F  -
07 2-0   G  G  G  -  -  G
08 2-0   H  H  -  -  -  -
09 0-3   -  I  -  -  -  I
10 0-4   J  -  -  -  J  -
11 0-4   K  K  K  K  -  K
12 0-4   -  -  -  L  -  -
13 0-4   -  -  -  -  -  -
14 0-4   N  -  -  N  -  -
15 0-4   -  O  O  O  O  O
16 0-5   P  P  P  P  P  P
17 0-6   -  -  Q  -  -  Q
18 0-6   -  -  -  -  -   
19 0-6   -  S  S  -  -   
20 1-2   -  T  -  T      
21 2-6   U  U  U  -      
22 2-6   -  -  -         
23 2-6   -  -  -         
24 2-6   -  -            
25 3-4   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QWJKS TQQBK AZOSW HCSMU KQQQR P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 2-0   B  -  B  -  B  -
03 2-0   C  C  -  -  -  C
04 2-0   -  D  D  D  -  -
05 2-0   -  -  -  E  -  E
06 2-0   F  -  F  F  F  F
07 0-3   -  G  G  G  G  G
08 0-3   -  H  H  H  H  -
09 0-3   -  I  I  -  I  -
10 0-3   J  -  -  -  J  J
11 0-3   -  K  -  -  K  -
12 0-3   L  -  -  L  -  L
13 0-3   M  -  M  M  M  -
14 0-3   -  N  -  N  -  N
15 0-3   O  -  -  O  O  O
16 0-3   -  P  -  -  P  -
17 0-3   -  Q  Q  -  -  Q
18 0-4   R  R  -  R  -   
19 0-4   S  S  S  S  -   
20 0-5   -  -  T  -      
21 0-5   U  -  U  U      
22 0-5   -  V  -         
23 0-6   W  X  -         
24 1-4   -  -            
25 2-3   -  Z            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

DOLYL QQUNF GUFAN TVVWG JLUJH B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   -  B  B  -  -  B
03 2-0   -  C  -  -  C  C
04 2-0   -  D  -  D  D  -
05 0-3   E  -  -  E  -  -
06 0-3   -  F  F  -  F  F
07 0-3   -  -  -  -  G  -
08 0-3   H  -  -  H  -  -
09 0-3   -  -  -  I  -  I
10 0-3   J  -  J  -  J  -
11 0-3   K  -  K  -  K  -
12 0-3   L  L  L  L  -  L
13 0-3   -  -  -  M  -  M
14 0-3   -  N  N  N  N  N
15 0-3   O  -  -  O  O  -
16 0-3   -  -  -  -  -  P
17 0-4   Q  -  -  Q  Q  Q
18 0-6   R  -  R  -  R   
19 0-6   -  S  S  S  -   
20 0-6   T  T  -  -      
21 0-6   -  U  U  U      
22 0-6   -  V  V         
23 0-6   W  -  X         
24 1-5   -  Y            
25 2-5   -  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

NJKJU ZOTWZ BONHG UGCJT VPOUG K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-NOV-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  A
02 2-0   B  -  B  -  B  -
03 0-3   C  C  C  -  C  C
04 0-3   D  -  -  -  D  D
05 0-3   E  E  E  -  E  E
06 0-3   -  -  F  F  F  F
07 0-4   -  G  G  G  -  -
08 0-4   H  H  -  H  -  H
09 0-4   I  -  I  I  I  I
10 0-4   -  -  J  -  -  -
11 0-4   K  -  -  -  K  -
12 0-4   L  L  -  L  -  -
13 0-4   -  -  -  -  -  M
14 0-4   -  N  N  N  -  -
15 0-4   O  -  O  -  -  O
16 0-4   P  P  -  P  P  -
17 0-5   -  Q  Q  -  -  -
18 0-5   R  -  -  R  R   
19 0-5   S  S  -  -  -   
20 0-5   -  -  -  T      
21 0-6   -  -  U  U      
22 1-6   V  -  -         
23 2-5   -  X  -         
24 2-6   X  Y            
25 3-4   -  Z            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

HUFRK LGTQK KQOJT WVPPJ LCQLW G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
