EFFECTIVE PERIOD:
25-MAY-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  B  B  B  -  B
03 1-0   -  C  -  C  C  -
04 1-0   -  D  -  -  -  -
05 1-0   E  E  -  -  E  E
06 1-0   -  -  F  F  F  -
07 1-0   -  -  G  G  -  -
08 1-0   H  H  H  -  -  -
09 1-0   -  -  I  -  -  I
10 1-0   J  -  J  -  J  -
11 2-0   K  -  -  -  -  K
12 2-0   -  L  -  L  -  L
13 2-0   M  -  M  -  M  M
14 2-0   -  N  N  N  -  N
15 2-0   -  O  O  O  -  -
16 2-0   -  P  P  P  -  -
17 2-0   Q  Q  -  -  Q  -
18 2-0   -  R  R  -  -   
19 0-4   -  S  -  -  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 0-6   V  V  V         
23 0-6   W  -  X         
24 0-6   -  -            
25 0-6   -  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

PAQPJ KRZIB LSSII ABHZH SJKMQ P
-------------------------------
