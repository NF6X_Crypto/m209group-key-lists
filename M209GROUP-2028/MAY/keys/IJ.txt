EFFECTIVE PERIOD:
17-MAY-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  -  -  B  -  -
03 1-0   C  C  -  C  -  -
04 1-0   D  D  D  -  D  D
05 2-0   E  -  E  E  -  E
06 2-0   -  F  F  -  F  -
07 2-0   -  G  -  -  -  -
08 2-0   H  -  -  H  -  -
09 2-0   -  -  I  -  I  -
10 2-0   -  J  -  J  -  J
11 0-4   K  K  -  -  K  K
12 0-4   L  L  -  L  L  L
13 0-4   -  M  M  M  M  M
14 0-4   N  -  N  -  -  N
15 0-4   -  -  O  -  -  O
16 0-4   P  -  -  -  P  -
17 0-4   -  -  Q  -  Q  -
18 1-4   -  -  -  -  R   
19 1-5   S  -  S  S  -   
20 2-3   T  T  T  -      
21 2-4   U  U  U  U      
22 2-4   -  V  V         
23 2-4   W  -  -         
24 2-4   -  Y            
25 2-5   -  -            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UEAHI TWSQU MPFAS GWNUD WASOJ D
-------------------------------
