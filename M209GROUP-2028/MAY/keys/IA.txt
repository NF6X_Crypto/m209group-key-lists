EFFECTIVE PERIOD:
08-MAY-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 2-0   B  -  B  -  B  B
03 2-0   C  C  C  -  -  C
04 2-0   -  -  D  -  D  -
05 2-0   -  E  E  -  -  -
06 0-3   F  -  -  F  -  -
07 0-4   G  G  -  G  -  G
08 0-4   H  H  -  -  -  -
09 0-5   I  I  I  -  I  I
10 0-5   -  -  -  J  J  -
11 0-5   K  -  K  K  K  K
12 0-5   L  L  -  L  -  -
13 0-5   M  M  -  -  -  -
14 0-6   -  N  -  -  N  N
15 0-6   -  -  O  -  O  -
16 0-6   P  P  -  P  -  P
17 0-6   -  Q  Q  Q  -  -
18 0-6   -  -  R  -  R   
19 0-6   -  -  S  -  S   
20 0-6   T  -  -  T      
21 0-6   -  -  U  U      
22 1-4   V  -  V         
23 2-4   -  X  X         
24 2-5   -  Y            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MGNVF LKVUZ ZLCTE TKXSJ PKXHU U
-------------------------------
