EFFECTIVE PERIOD:
19-MAR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   B  B  B  -  B  -
03 1-0   C  -  -  -  -  -
04 1-0   D  -  D  D  D  D
05 1-0   E  E  E  -  -  E
06 1-0   -  -  -  F  F  F
07 1-0   G  -  G  G  G  -
08 1-0   -  H  -  H  H  -
09 1-0   I  -  I  -  -  I
10 2-0   -  J  -  -  J  J
11 0-3   K  K  -  K  K  K
12 0-4   -  -  -  L  -  -
13 0-4   M  -  M  -  -  M
14 0-4   N  -  -  N  N  -
15 0-4   -  O  O  -  O  O
16 0-4   P  P  P  -  -  -
17 0-4   -  -  Q  Q  -  Q
18 0-4   -  -  R  -  -   
19 0-5   -  -  S  S  -   
20 0-5   -  T  -  T      
21 0-5   U  U  -  U      
22 0-6   -  V  -         
23 0-6   W  -  X         
24 1-4   -  Y            
25 3-6   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OLIGN MNWMR FSWQX ZNRHV JSONO L
-------------------------------
