EFFECTIVE PERIOD:
04-MAR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  -  -  B  -  B
03 1-0   -  -  C  C  -  C
04 1-0   -  -  D  D  D  D
05 1-0   E  -  E  E  E  -
06 1-0   F  F  -  -  -  F
07 1-0   G  -  G  G  -  -
08 1-0   -  H  -  -  -  -
09 2-0   I  -  -  -  -  -
10 2-0   -  J  -  J  -  -
11 0-3   -  K  K  -  -  -
12 0-3   -  L  L  -  L  L
13 0-3   M  M  -  -  M  M
14 0-5   N  N  -  N  N  -
15 0-6   O  -  -  -  -  O
16 0-6   -  -  -  P  P  P
17 0-6   -  Q  Q  Q  Q  -
18 0-6   R  R  -  -  R   
19 0-6   S  -  S  S  -   
20 1-4   T  -  -  T      
21 1-6   U  U  U  U      
22 1-6   V  -  V         
23 1-6   -  X  -         
24 1-6   X  Y            
25 2-3   Y  -            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

BMXUU YROAW KRQFE MUMOU IRORX U
-------------------------------
