EFFECTIVE PERIOD:
07-MAR-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  -  -  C  -  -
04 1-0   -  D  -  D  D  D
05 1-0   -  -  E  E  -  E
06 1-0   -  F  -  -  F  F
07 1-0   G  G  G  G  G  -
08 1-0   -  -  H  -  -  -
09 1-0   I  -  I  -  -  I
10 1-0   -  J  J  J  J  -
11 2-0   K  -  -  -  -  K
12 2-0   L  -  -  -  -  L
13 2-0   M  -  M  -  M  M
14 2-0   -  N  -  N  N  -
15 2-0   O  -  O  O  O  -
16 2-0   -  P  P  -  P  P
17 0-3   Q  Q  Q  -  -  Q
18 0-5   -  R  -  -  R   
19 0-5   S  -  -  S  -   
20 0-5   -  T  -  -      
21 0-5   -  -  -  U      
22 0-6   V  V  V         
23 0-6   W  X  -         
24 2-5   X  -            
25 3-4   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

DAQYF KTUYZ KQVGO EJAFP LPUTM S
-------------------------------
