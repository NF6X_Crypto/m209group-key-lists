EFFECTIVE PERIOD:
31-JUL-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  B  -  B  -  -
03 1-0   C  -  C  -  -  -
04 1-0   -  D  -  D  D  -
05 2-0   E  E  -  E  -  -
06 2-0   -  -  -  -  -  -
07 2-0   G  -  G  -  G  G
08 2-0   -  -  H  -  H  H
09 2-0   I  I  I  I  I  I
10 0-4   J  J  -  J  -  J
11 0-4   K  K  -  -  -  K
12 0-4   L  L  L  L  L  L
13 0-4   M  M  M  -  -  -
14 0-4   -  N  N  N  N  -
15 0-5   O  -  O  O  -  O
16 0-5   -  P  -  P  -  -
17 0-5   -  -  Q  Q  Q  -
18 1-2   R  -  -  -  R   
19 1-5   -  -  S  -  S   
20 2-4   -  T  -  T      
21 2-4   -  -  -  U      
22 2-4   -  V  -         
23 2-4   W  X  -         
24 3-4   X  Y            
25 3-5   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

HSUML RFAZO XKSRQ UHSMT VVXXW U
-------------------------------
