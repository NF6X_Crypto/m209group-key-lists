EFFECTIVE PERIOD:
25-JUL-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  -  -  -  -
04 1-0   -  -  D  -  D  D
05 1-0   -  E  -  E  -  -
06 2-0   -  F  -  F  -  -
07 2-0   G  G  G  -  G  G
08 2-0   -  H  -  H  -  -
09 0-3   -  -  I  I  I  I
10 0-4   J  -  -  J  J  -
11 0-4   K  -  K  K  -  K
12 0-4   L  L  L  L  L  L
13 0-4   -  -  -  -  M  -
14 0-4   -  N  N  -  -  -
15 0-4   O  -  -  O  -  -
16 0-4   -  P  -  -  P  P
17 0-4   -  Q  -  Q  Q  Q
18 0-5   -  R  -  -  -   
19 0-5   S  -  -  -  S   
20 1-2   T  -  T  -      
21 1-4   -  U  U  U      
22 1-4   -  -  -         
23 1-4   W  -  -         
24 1-4   -  Y            
25 2-5   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

FIRNW EHPQQ QIEYQ FVZTQ XLORI S
-------------------------------
