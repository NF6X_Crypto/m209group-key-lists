EFFECTIVE PERIOD:
03-JUL-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   -  -  B  B  B  B
03 2-0   C  C  -  C  -  C
04 2-0   D  -  D  -  D  -
05 2-0   -  -  E  -  -  E
06 2-0   -  F  F  -  -  F
07 0-3   -  G  -  G  -  G
08 0-3   H  H  -  -  H  H
09 0-3   I  I  I  -  I  -
10 0-3   J  -  -  J  J  J
11 0-6   -  -  K  K  K  -
12 0-6   -  -  -  L  L  -
13 0-6   -  -  M  -  -  -
14 0-6   -  N  N  N  N  N
15 0-6   -  -  -  O  -  -
16 0-6   P  P  -  -  P  -
17 0-6   Q  Q  Q  -  -  Q
18 1-2   -  R  -  R  -   
19 1-4   -  S  S  -  S   
20 2-4   T  -  -  T      
21 2-5   U  U  -  -      
22 2-6   V  V  -         
23 2-6   W  X  -         
24 2-6   X  -            
25 2-6   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

IOOZN IVOVM OULAS IUMTO TAHTM L
-------------------------------
