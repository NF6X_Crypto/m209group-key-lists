EFFECTIVE PERIOD:
21-AUG-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  B  -  B  -  -
03 1-0   -  -  -  -  C  -
04 2-0   D  D  -  -  -  D
05 2-0   -  E  E  -  E  -
06 2-0   -  F  -  F  F  -
07 2-0   G  -  -  G  -  -
08 2-0   -  -  -  -  -  H
09 2-0   I  I  -  -  -  I
10 2-0   -  -  J  J  -  J
11 2-0   -  -  -  -  K  K
12 0-4   -  -  L  -  L  -
13 0-4   -  M  -  -  -  M
14 0-4   N  N  N  -  -  N
15 0-4   O  -  O  -  -  O
16 0-5   -  -  P  P  P  P
17 0-6   -  -  -  Q  Q  Q
18 0-6   R  -  -  R  -   
19 0-6   S  -  S  S  S   
20 1-2   T  T  -  -      
21 1-2   U  U  U  U      
22 1-2   -  V  V         
23 1-2   W  X  X         
24 1-4   X  Y            
25 3-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GANQS AVSVM AZOCZ VQMIW EFESX N
-------------------------------
