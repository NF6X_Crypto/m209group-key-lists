EFFECTIVE PERIOD:
18-AUG-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 0-3   B  -  B  B  B  -
03 0-3   -  -  C  -  -  -
04 0-3   -  D  D  D  -  -
05 0-3   -  -  E  -  -  E
06 0-3   F  F  F  -  -  F
07 0-3   -  -  G  G  -  -
08 0-3   H  -  -  H  H  H
09 0-3   I  I  I  -  I  -
10 0-3   -  J  J  -  -  -
11 0-3   -  -  -  K  K  K
12 0-3   L  -  -  -  -  L
13 0-3   M  -  M  M  -  M
14 0-5   -  -  -  N  -  -
15 0-5   -  O  -  O  O  O
16 0-5   P  P  -  P  -  P
17 0-5   Q  Q  -  -  -  Q
18 0-5   -  R  R  R  R   
19 0-5   S  S  -  -  S   
20 0-5   -  T  T  T      
21 0-5   U  -  -  -      
22 0-5   -  V  V         
23 0-5   W  -  -         
24 0-6   -  -            
25 0-6   -  -            
26 2-4   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

EEDMP EYMVJ BPQLP WYCZQ BDAEA L
-------------------------------
