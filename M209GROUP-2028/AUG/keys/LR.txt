EFFECTIVE PERIOD:
11-AUG-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  B  -  B  -  -
03 1-0   -  C  -  C  C  C
04 1-0   D  -  D  D  D  D
05 1-0   -  E  -  E  E  E
06 2-0   -  F  F  -  -  -
07 0-4   -  -  G  G  -  -
08 0-4   H  H  -  H  H  H
09 0-4   -  -  -  -  I  -
10 0-4   -  J  -  -  J  J
11 0-5   -  K  K  -  K  K
12 0-5   L  L  L  -  -  -
13 0-5   -  M  M  M  -  -
14 0-5   -  -  -  -  -  -
15 0-5   O  O  -  O  -  -
16 0-5   P  -  -  -  -  P
17 0-5   -  -  Q  Q  -  Q
18 0-5   R  -  R  -  R   
19 0-6   -  -  -  S  S   
20 1-4   T  -  T  -      
21 1-5   U  -  U  -      
22 1-5   V  V  V         
23 1-5   W  X  -         
24 1-5   X  Y            
25 2-3   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SZOAP MZWUS TYZIM UVOUZ CIOJC Z
-------------------------------
