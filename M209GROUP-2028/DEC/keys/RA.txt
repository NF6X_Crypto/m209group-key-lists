EFFECTIVE PERIOD:
28-DEC-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  -  -  B  -  -
03 1-0   C  -  C  -  -  C
04 1-0   -  -  D  D  D  -
05 1-0   -  E  E  E  -  E
06 2-0   -  -  F  F  F  F
07 0-3   -  -  -  -  -  -
08 0-3   -  -  -  H  H  -
09 0-3   I  -  -  I  I  -
10 0-3   -  J  -  -  -  J
11 0-3   K  K  K  -  K  K
12 0-3   -  L  -  L  -  L
13 0-3   M  -  -  -  M  M
14 0-4   N  -  -  -  -  -
15 0-5   -  -  -  O  -  -
16 0-5   P  P  P  -  P  -
17 0-6   Q  -  -  -  Q  -
18 0-6   R  -  R  -  R   
19 0-6   -  -  S  -  -   
20 0-6   T  T  T  T      
21 0-6   U  U  U  U      
22 1-3   V  V  -         
23 1-3   -  -  -         
24 1-3   X  Y            
25 1-6   -  Z            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OSWZT UUMNI LSWUK OKHJO PKBNM N
-------------------------------
