EFFECTIVE PERIOD:
05-DEC-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  -  -  B  -
03 1-0   C  C  -  -  -  C
04 1-0   D  D  -  -  D  -
05 1-0   E  E  -  E  -  E
06 1-0   F  F  F  F  -  -
07 2-0   -  -  -  G  G  -
08 2-0   H  -  -  -  H  H
09 2-0   -  -  -  -  -  I
10 2-0   -  J  -  -  J  J
11 0-3   K  -  -  K  K  -
12 0-3   -  -  L  L  L  -
13 0-4   -  M  M  M  -  -
14 0-4   -  N  -  N  -  N
15 0-4   O  -  O  -  O  O
16 1-2   -  P  P  -  -  -
17 1-4   -  -  -  Q  Q  -
18 2-3   -  -  R  -  R   
19 2-3   S  S  -  S  S   
20 2-3   -  -  T  T      
21 2-3   -  -  -  -      
22 3-4   -  -  V         
23 3-6   -  -  X         
24 3-6   X  -            
25 3-6   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GTJMU APJRL VEHUY AUAIG SWPIQ W
-------------------------------
