EFFECTIVE PERIOD:
11-DEC-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   B  -  B  B  -  B
03 1-0   -  C  -  C  C  -
04 1-0   -  -  D  -  -  -
05 1-0   -  E  E  -  -  E
06 2-0   F  F  F  F  F  -
07 2-0   -  -  G  -  G  G
08 2-0   H  H  H  -  -  H
09 2-0   -  -  I  I  I  I
10 2-0   J  -  -  -  J  -
11 0-4   K  K  -  K  -  K
12 0-4   L  L  L  -  L  L
13 0-4   -  -  M  -  -  M
14 0-5   N  -  -  -  N  -
15 0-5   -  O  -  -  -  -
16 0-5   P  -  -  -  -  P
17 0-5   Q  -  -  Q  -  -
18 1-2   -  -  R  -  -   
19 1-4   S  S  -  S  S   
20 2-5   T  -  -  T      
21 2-5   -  U  -  -      
22 2-5   V  -  V         
23 2-5   -  -  X         
24 3-6   X  -            
25 4-5   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XJLGV CUOJG URGRP MWPSP GWSZG R
-------------------------------
