EFFECTIVE PERIOD:
13-SEP-2028 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  -  C  -  -  -
04 2-0   D  -  D  D  -  D
05 2-0   E  -  -  E  E  -
06 2-0   -  -  F  F  F  F
07 2-0   G  G  G  G  -  -
08 2-0   H  H  -  -  H  H
09 0-3   -  I  I  -  I  -
10 0-3   J  J  J  -  J  -
11 0-3   -  K  K  K  -  -
12 0-3   L  L  L  L  -  L
13 0-3   M  -  -  -  -  -
14 0-3   N  N  N  -  N  N
15 0-6   -  O  -  O  O  O
16 0-6   P  -  P  P  P  P
17 0-6   -  -  -  -  Q  -
18 1-2   R  R  R  -  R   
19 1-3   S  -  -  -  -   
20 1-5   -  -  -  T      
21 2-6   U  -  U  -      
22 3-5   -  V  -         
23 3-6   -  X  -         
24 3-6   -  Y            
25 3-6   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

WHRUR PCWJH NSANO RNNZT AXURU U
-------------------------------
