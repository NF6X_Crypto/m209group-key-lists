EFFECTIVE PERIOD:
05-DEC-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 0-3   -  B  B  B  -  B
03 0-3   C  C  C  C  -  -
04 0-5   -  D  D  -  D  -
05 0-5   -  E  -  E  -  E
06 0-5   -  -  -  -  F  -
07 0-5   -  G  -  -  -  G
08 0-5   H  H  -  -  H  H
09 0-5   I  I  -  I  I  I
10 0-6   J  J  J  -  -  J
11 0-6   -  K  K  K  K  -
12 0-6   L  L  L  -  -  L
13 0-6   -  -  -  M  -  M
14 0-6   N  -  N  N  N  -
15 0-6   O  -  -  O  -  O
16 1-2   P  -  P  -  -  -
17 1-3   Q  Q  Q  -  -  Q
18 1-3   R  -  R  -  R   
19 1-3   -  -  S  S  S   
20 1-5   T  -  -  T      
21 2-3   U  -  U  -      
22 3-4   -  V  -         
23 3-6   -  X  X         
24 3-6   -  -            
25 3-6   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NRAKU NSTMN CZLAM TRCXU NZHRF A
-------------------------------
