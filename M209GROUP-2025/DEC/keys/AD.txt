EFFECTIVE PERIOD:
09-DEC-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  B  B  -  B  -
03 1-0   -  C  C  C  -  -
04 1-0   -  D  -  -  D  D
05 1-0   E  -  E  -  -  -
06 2-0   -  F  -  F  F  -
07 2-0   G  -  G  -  -  G
08 2-0   -  -  -  -  -  H
09 2-0   I  I  I  -  -  I
10 2-0   J  J  J  J  J  -
11 2-0   -  K  K  K  K  K
12 0-5   L  -  L  L  -  L
13 0-5   M  M  -  -  M  M
14 0-6   -  -  -  N  -  N
15 0-6   -  -  -  O  O  -
16 1-2   P  P  -  -  -  P
17 1-6   Q  -  Q  -  Q  -
18 1-6   -  R  -  R  R   
19 1-6   -  S  -  S  -   
20 1-6   T  -  -  T      
21 2-5   -  U  -  -      
22 3-4   V  -  V         
23 4-5   -  X  X         
24 4-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PALVQ OYCRR JSRUT PITOY AIUTG G
-------------------------------
