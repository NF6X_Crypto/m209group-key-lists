EFFECTIVE PERIOD:
18-DEC-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   -  -  -  B  -  B
03 2-0   -  -  C  -  C  C
04 2-0   D  D  -  -  -  -
05 2-0   E  E  E  E  E  -
06 2-0   -  F  -  F  -  -
07 2-0   G  -  -  G  G  -
08 0-3   H  -  -  -  -  H
09 0-3   I  I  I  -  I  I
10 0-3   J  J  -  -  -  J
11 0-3   -  K  -  K  -  K
12 0-5   L  L  -  L  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  N  -  -  -  N
15 0-5   -  -  O  O  O  -
16 1-3   P  P  P  -  P  -
17 2-3   -  -  Q  Q  -  Q
18 2-3   -  -  R  R  -   
19 2-3   -  -  S  -  S   
20 2-3   T  -  -  -      
21 2-5   -  U  U  U      
22 3-4   V  V  -         
23 3-4   W  X  X         
24 3-4   -  -            
25 3-6   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

GUOTT OOZVO VZSEA SSCMR SQZLU O
-------------------------------
