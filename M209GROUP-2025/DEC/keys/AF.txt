EFFECTIVE PERIOD:
11-DEC-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  B  -  -  -  B
03 1-0   C  C  C  C  -  -
04 1-0   -  D  D  D  -  D
05 1-0   -  -  -  E  -  -
06 0-3   F  -  F  F  F  F
07 0-4   -  G  G  -  G  G
08 0-4   H  H  H  H  -  H
09 0-4   I  -  -  -  I  -
10 0-4   -  -  -  -  J  -
11 0-4   -  K  K  K  -  -
12 0-4   L  L  -  L  -  L
13 0-4   M  M  -  M  -  M
14 0-4   -  N  -  -  N  -
15 0-4   -  O  O  O  O  O
16 0-5   P  -  P  P  -  -
17 0-6   -  -  -  Q  Q  Q
18 0-6   -  -  R  -  R   
19 0-6   S  S  -  -  -   
20 0-6   T  T  -  -      
21 0-6   U  -  -  U      
22 0-6   -  V  V         
23 0-6   W  -  -         
24 1-2   -  Y            
25 1-6   -  Z            
26 2-3   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RKURZ IJPTB PQUAU BTDPX LUIYV V
-------------------------------
