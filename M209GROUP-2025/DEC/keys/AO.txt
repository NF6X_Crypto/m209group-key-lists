EFFECTIVE PERIOD:
20-DEC-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   -  B  -  -  B  -
03 0-4   C  -  C  C  C  -
04 0-4   D  -  D  D  D  D
05 0-4   -  E  -  E  -  E
06 0-4   F  F  -  -  -  -
07 0-5   G  -  -  -  -  G
08 0-5   -  -  H  -  -  -
09 0-5   I  I  I  -  -  I
10 0-5   J  -  -  J  J  -
11 0-5   -  -  -  K  -  K
12 0-5   L  L  -  -  -  -
13 0-6   M  M  M  -  -  -
14 0-6   -  N  N  -  -  N
15 0-6   O  -  O  O  O  O
16 0-6   -  -  P  P  P  -
17 0-6   -  -  -  -  -  -
18 1-2   R  R  -  -  R   
19 1-3   S  -  -  S  S   
20 1-4   -  -  T  T      
21 2-4   -  -  -  U      
22 2-5   V  -  V         
23 4-6   W  X  X         
24 4-6   X  Y            
25 4-6   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KALAQ ZTTOM QYORC OGTKO MJNVO K
-------------------------------
