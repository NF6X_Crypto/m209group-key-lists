EFFECTIVE PERIOD:
23-MAR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 0-4   -  -  B  B  B  B
03 0-4   -  C  -  C  C  -
04 0-4   D  -  -  -  -  -
05 0-4   E  E  -  E  -  E
06 0-4   F  F  F  -  -  F
07 0-4   G  G  G  G  -  -
08 0-5   -  -  -  -  H  H
09 0-5   I  -  I  -  I  I
10 0-5   -  J  -  J  -  -
11 0-5   -  K  -  -  -  -
12 0-5   -  L  -  -  -  -
13 0-5   -  M  M  M  M  M
14 0-5   N  -  N  -  N  N
15 0-5   -  -  -  -  -  O
16 0-5   P  P  -  P  -  P
17 0-5   Q  Q  Q  Q  Q  -
18 0-6   -  -  R  R  -   
19 0-6   S  S  S  -  S   
20 0-6   -  T  T  T      
21 0-6   -  U  -  U      
22 0-6   -  -  V         
23 0-6   W  X  X         
24 0-6   -  -            
25 0-6   Y  -            
26 1-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

HLSPQ ACZAL QQHHI AMBHI SZKCZ O
-------------------------------
