EFFECTIVE PERIOD:
16-FEB-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  B  -  B  B  -
03 1-0   C  -  C  C  -  C
04 1-0   -  -  D  -  D  -
05 1-0   -  E  -  -  E  E
06 1-0   -  -  -  -  -  F
07 1-0   G  -  -  G  -  -
08 0-4   H  H  H  -  -  -
09 0-4   I  -  I  I  I  I
10 0-4   J  J  J  J  -  J
11 0-4   K  -  -  K  K  -
12 0-4   -  L  -  -  L  L
13 0-5   -  M  M  M  M  -
14 0-5   N  N  -  -  -  N
15 0-5   O  O  -  -  -  -
16 1-4   P  -  -  -  -  -
17 1-5   -  Q  -  Q  Q  -
18 1-5   -  R  -  R  R   
19 1-5   -  S  S  S  S   
20 1-5   T  T  -  T      
21 2-5   -  U  U  -      
22 2-6   V  -  V         
23 3-5   -  -  -         
24 4-6   -  -            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ANBFT TATLT STVMZ LNSAZ VRCBV A
-------------------------------
