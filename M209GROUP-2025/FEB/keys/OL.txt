EFFECTIVE PERIOD:
08-FEB-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  A
02 2-0   B  -  B  B  B  B
03 2-0   -  C  -  -  -  -
04 0-3   -  D  -  -  -  D
05 0-4   -  E  -  -  E  E
06 0-4   -  -  F  F  F  -
07 0-4   -  G  -  G  G  G
08 0-4   -  H  H  H  -  -
09 0-4   I  I  -  I  I  -
10 0-4   -  -  J  -  -  J
11 0-4   K  -  K  -  K  -
12 0-4   L  L  -  -  -  -
13 0-4   -  -  M  M  -  M
14 0-4   N  -  N  -  N  -
15 0-4   O  O  -  O  -  O
16 0-5   P  P  P  P  P  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   -  -  -  R  R   
19 0-5   S  -  -  S  S   
20 0-5   -  T  T  -      
21 0-5   U  U  -  -      
22 0-5   V  -  -         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-5   -  Z            
26 1-6   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

QEAMW PALEN QCKOY PWPNF AWFXM A
-------------------------------
