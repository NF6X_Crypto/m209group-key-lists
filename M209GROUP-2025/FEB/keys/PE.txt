EFFECTIVE PERIOD:
27-FEB-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  -  A  A
02 0-3   B  B  B  -  -  B
03 0-3   C  C  C  -  C  C
04 0-3   -  -  D  -  D  -
05 0-3   E  -  -  E  -  E
06 0-4   -  F  -  -  F  -
07 0-4   -  -  -  G  -  -
08 0-4   -  -  -  -  -  -
09 0-4   I  I  -  -  I  -
10 0-5   -  J  J  -  -  -
11 0-5   -  -  K  K  -  K
12 0-5   -  -  L  L  -  L
13 0-5   -  M  -  -  M  M
14 0-5   -  N  N  -  N  -
15 0-6   -  O  -  O  -  O
16 0-6   P  P  P  -  P  P
17 0-6   Q  -  -  -  Q  -
18 1-2   R  R  R  R  R   
19 2-4   S  S  -  S  S   
20 2-5   -  T  T  T      
21 2-5   U  U  U  U      
22 3-4   V  -  -         
23 3-6   W  X  -         
24 5-6   -  Y            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RQADW VXAVL LUKXZ GMHHV XUHXQ W
-------------------------------
