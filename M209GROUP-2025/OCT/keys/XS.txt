EFFECTIVE PERIOD:
07-OCT-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 2-0   -  -  -  -  -  -
03 2-0   -  -  C  C  C  -
04 2-0   D  -  -  D  -  D
05 2-0   E  E  -  E  E  E
06 2-0   -  F  -  F  -  F
07 2-0   G  -  G  -  G  -
08 0-5   -  H  H  -  -  H
09 0-5   I  I  I  -  I  -
10 0-5   -  -  J  -  J  J
11 0-5   -  -  -  -  -  K
12 0-5   L  L  -  L  L  L
13 0-5   M  M  M  -  M  M
14 0-6   -  N  N  N  -  -
15 0-6   -  -  -  -  -  O
16 0-6   P  P  -  -  P  -
17 0-6   Q  -  Q  -  -  Q
18 0-6   -  -  R  R  R   
19 0-6   -  S  -  S  S   
20 0-6   T  T  T  -      
21 0-6   U  U  -  -      
22 2-3   -  V  -         
23 2-5   -  X  -         
24 3-4   -  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UAKBA JZQHB ZTTAT IZUTA KKJAH J
-------------------------------
