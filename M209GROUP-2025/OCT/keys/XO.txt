EFFECTIVE PERIOD:
03-OCT-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   -  -  -  -  B  B
03 1-0   -  -  -  -  C  C
04 2-0   D  D  -  -  -  -
05 0-3   E  -  E  -  -  E
06 0-3   F  F  F  F  F  -
07 0-3   -  G  -  -  G  -
08 0-3   -  H  H  -  -  -
09 0-4   -  -  -  I  I  I
10 0-4   J  -  -  -  -  J
11 0-4   K  K  -  K  K  K
12 0-4   L  -  -  L  L  -
13 0-5   -  -  -  -  -  M
14 0-5   N  N  N  N  -  N
15 0-5   O  O  -  -  O  -
16 0-5   P  -  P  P  P  -
17 0-5   -  Q  Q  -  -  -
18 0-5   -  R  R  R  -   
19 0-5   -  -  S  S  S   
20 0-5   -  T  T  -      
21 0-6   -  -  -  U      
22 1-3   V  -  V         
23 1-6   W  X  -         
24 3-4   X  -            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

WHIXQ NQQSV PBZKZ KRPLR NFUQO L
-------------------------------
