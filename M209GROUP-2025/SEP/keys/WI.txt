EFFECTIVE PERIOD:
01-SEP-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   A  -  -  -  A  -
02 0-4   -  -  B  B  B  -
03 0-5   -  -  -  C  -  -
04 0-5   D  D  D  -  D  -
05 0-5   E  E  -  E  E  -
06 0-5   -  F  -  -  -  F
07 0-5   -  G  -  G  G  G
08 0-5   H  -  H  H  -  -
09 0-6   -  I  I  I  -  -
10 0-6   J  J  -  -  J  J
11 0-6   K  K  K  -  K  -
12 0-6   -  -  L  L  L  L
13 0-6   M  M  M  M  M  -
14 0-6   N  N  -  N  -  N
15 0-6   -  -  -  -  -  O
16 1-3   P  -  P  P  -  P
17 1-4   -  Q  -  -  -  Q
18 2-4   R  -  -  -  -   
19 3-4   S  S  -  -  S   
20 3-4   -  -  T  -      
21 3-6   -  -  -  -      
22 4-5   V  V  V         
23 4-5   W  X  -         
24 4-5   -  Y            
25 4-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SZFUK JNNPN LTTUN LTTXL AUUML D
-------------------------------
