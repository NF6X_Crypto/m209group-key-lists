EFFECTIVE PERIOD:
15-SEP-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  B  B  B  -  B
03 1-0   C  C  -  -  C  C
04 2-0   D  -  -  -  D  D
05 2-0   E  -  E  -  -  E
06 2-0   -  F  F  F  -  F
07 2-0   -  -  G  G  G  G
08 2-0   -  -  -  H  -  -
09 2-0   -  -  -  -  I  -
10 2-0   J  -  J  J  J  J
11 2-0   K  K  K  -  -  -
12 0-4   -  -  -  L  L  L
13 0-5   M  M  -  -  -  -
14 0-5   -  -  N  N  N  N
15 0-5   O  -  -  O  -  -
16 0-6   P  P  -  P  -  -
17 0-6   -  Q  Q  -  Q  -
18 0-6   -  -  R  -  -   
19 0-6   -  -  S  -  S   
20 1-4   T  -  T  -      
21 1-6   U  U  U  U      
22 2-5   -  V  -         
23 2-5   W  -  X         
24 2-5   X  Y            
25 2-5   -  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NUSIF NMVLU XNRVO GXHII LUCSK J
-------------------------------
