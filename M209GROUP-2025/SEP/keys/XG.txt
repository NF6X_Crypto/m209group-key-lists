EFFECTIVE PERIOD:
25-SEP-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  -  B  B  B  B
03 0-3   -  -  C  C  C  -
04 0-3   D  -  -  D  -  D
05 0-3   -  E  E  -  -  E
06 0-3   -  F  F  F  F  -
07 0-3   G  -  -  -  -  -
08 0-4   -  -  H  H  H  H
09 0-4   -  I  -  I  I  -
10 0-5   -  J  -  J  J  -
11 0-5   K  -  K  K  -  -
12 0-5   L  -  L  -  L  L
13 0-5   M  M  M  M  -  -
14 0-5   -  -  N  N  N  -
15 0-5   O  O  O  O  -  O
16 0-5   P  P  -  P  P  P
17 0-5   Q  Q  -  -  Q  -
18 0-5   R  -  -  -  -   
19 0-5   S  -  -  -  S   
20 0-5   T  -  -  -      
21 0-5   -  -  -  -      
22 0-5   V  -  V         
23 0-6   W  X  X         
24 1-3   -  Y            
25 1-4   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZNUGT PDTIW SNJMN TRYFR GANZY G
-------------------------------
