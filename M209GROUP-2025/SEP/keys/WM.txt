EFFECTIVE PERIOD:
05-SEP-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 2-0   -  -  -  B  -  B
03 2-0   -  C  -  C  -  C
04 2-0   -  -  D  -  -  -
05 2-0   -  -  -  E  E  -
06 2-0   F  -  F  -  F  -
07 2-0   -  -  -  G  -  G
08 0-3   -  H  H  -  H  H
09 0-3   I  I  -  I  I  -
10 0-3   J  -  -  -  -  -
11 0-3   -  K  K  K  -  K
12 0-5   -  L  L  L  L  -
13 0-5   M  -  -  M  M  -
14 0-6   N  -  N  -  -  N
15 0-6   O  -  O  O  O  O
16 0-6   -  P  -  P  P  -
17 0-6   -  Q  -  -  -  -
18 0-6   R  R  R  R  -   
19 0-6   -  -  -  -  S   
20 1-5   T  -  T  T      
21 2-3   -  -  U  -      
22 2-6   V  -  -         
23 2-6   W  X  X         
24 2-6   X  Y            
25 2-6   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TSNVU AHUTY GADZK JEQKQ UWOKS M
-------------------------------
