EFFECTIVE PERIOD:
19-SEP-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 2-0   -  -  -  -  -  -
03 2-0   C  C  -  C  C  -
04 2-0   D  D  D  D  D  -
05 2-0   -  E  E  E  -  E
06 2-0   -  F  -  F  F  -
07 0-4   -  -  G  -  -  G
08 0-4   H  -  -  -  H  -
09 0-4   -  I  I  -  -  -
10 0-4   -  J  -  J  -  -
11 0-4   K  -  K  -  -  K
12 0-4   -  L  -  -  L  L
13 0-4   M  -  M  M  M  -
14 0-4   -  N  -  N  -  -
15 0-6   -  -  -  O  -  O
16 0-6   -  -  P  P  -  P
17 0-6   Q  -  Q  -  Q  Q
18 0-6   -  R  -  -  R   
19 0-6   S  S  -  S  S   
20 1-5   -  T  T  T      
21 2-5   U  -  -  -      
22 2-6   V  V  -         
23 3-4   W  -  -         
24 4-6   X  Y            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UVMVP RVABV IZMAV HHJUH IUYUZ C
-------------------------------
