EFFECTIVE PERIOD:
24-JAN-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  -  -  -  -  B
03 1-0   C  -  -  -  -  C
04 1-0   -  D  -  D  D  D
05 1-0   -  -  -  E  E  E
06 1-0   F  F  F  -  -  -
07 2-0   -  -  G  -  -  -
08 2-0   H  -  -  H  H  -
09 2-0   I  I  I  I  -  I
10 2-0   J  J  J  -  J  -
11 2-0   -  K  K  -  -  -
12 2-0   -  L  -  -  L  L
13 0-4   M  -  -  M  -  M
14 0-4   -  -  -  N  N  N
15 0-4   -  O  O  -  O  -
16 0-4   -  P  -  P  -  -
17 0-6   -  -  Q  Q  Q  -
18 1-2   R  R  -  R  -   
19 1-2   -  -  S  -  S   
20 1-2   -  -  -  T      
21 1-2   -  -  U  U      
22 1-4   V  V  V         
23 2-3   -  -  -         
24 2-5   -  -            
25 2-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TUPUF IKUJO WERPM FYHPW WTTKK U
-------------------------------
