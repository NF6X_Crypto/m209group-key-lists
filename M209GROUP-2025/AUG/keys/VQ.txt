EFFECTIVE PERIOD:
14-AUG-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  B  B  B  B  B
03 1-0   C  -  -  C  C  -
04 1-0   D  -  D  D  -  -
05 0-3   -  E  -  -  -  E
06 0-3   F  F  -  -  -  -
07 0-3   G  G  G  -  -  G
08 0-4   H  -  -  H  H  -
09 0-4   I  -  I  -  I  I
10 0-4   -  J  J  -  J  -
11 0-4   -  -  K  -  K  K
12 0-4   L  L  -  -  L  L
13 0-4   -  -  -  -  -  -
14 0-5   -  -  -  N  N  N
15 0-6   O  O  O  -  -  -
16 0-6   -  P  -  -  -  P
17 0-6   -  Q  Q  Q  -  Q
18 1-4   R  R  R  -  -   
19 1-5   -  -  -  S  -   
20 2-5   -  T  T  T      
21 3-5   U  -  U  -      
22 3-5   -  -  -         
23 3-6   -  -  X         
24 3-6   X  -            
25 3-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MUJNQ OTTRX QTNSF MXRUQ NEUHM U
-------------------------------
