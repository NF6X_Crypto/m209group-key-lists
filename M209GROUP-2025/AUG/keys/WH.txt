EFFECTIVE PERIOD:
31-AUG-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  -  -  -  B
03 2-0   -  -  C  -  -  C
04 2-0   -  -  D  D  D  -
05 2-0   E  -  -  E  E  -
06 2-0   -  -  F  F  F  -
07 2-0   G  -  G  G  -  G
08 0-5   H  H  H  -  -  -
09 0-5   I  I  I  -  -  I
10 0-5   -  J  -  J  -  -
11 0-5   K  -  -  -  K  K
12 0-5   -  -  -  -  -  L
13 0-6   M  -  -  M  M  M
14 0-6   N  -  N  N  -  N
15 0-6   O  O  O  -  O  -
16 0-6   P  P  -  -  -  -
17 0-6   -  -  Q  -  Q  Q
18 1-2   -  R  R  R  -   
19 1-3   S  -  -  S  S   
20 1-6   -  T  -  -      
21 1-6   U  U  -  U      
22 2-5   V  -  V         
23 4-6   W  X  X         
24 5-6   -  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VYLAA VVVYM ZPQSA PAPAV XGKPV V
-------------------------------
