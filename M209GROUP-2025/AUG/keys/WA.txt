EFFECTIVE PERIOD:
24-AUG-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  B  -  B  -  -
03 1-0   C  -  C  C  C  C
04 1-0   -  D  -  -  D  D
05 1-0   -  -  E  E  E  -
06 1-0   -  -  F  F  F  F
07 1-0   G  -  G  -  -  G
08 1-0   -  H  H  -  H  H
09 2-0   I  I  -  -  I  -
10 2-0   -  J  J  J  -  J
11 2-0   K  K  K  -  -  K
12 0-5   -  -  -  L  -  -
13 0-6   -  -  -  -  -  -
14 0-6   N  N  N  N  -  -
15 0-6   -  O  -  O  O  -
16 0-6   P  P  -  P  P  -
17 0-6   -  Q  Q  -  Q  Q
18 0-6   R  -  -  R  -   
19 0-6   S  -  S  S  -   
20 0-6   T  -  -  -      
21 0-6   -  -  -  -      
22 0-6   -  -  -         
23 0-6   -  X  X         
24 0-6   X  -            
25 0-6   -  Z            
26 2-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

ZEMKI COZLO ACSXR BOVEM ELCPN D
-------------------------------
