EFFECTIVE PERIOD:
31-JUL-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  -
02 2-0   -  B  -  -  B  B
03 2-0   C  -  -  C  -  -
04 2-0   D  D  -  D  D  D
05 2-0   E  E  E  E  E  -
06 0-3   F  F  -  F  F  -
07 0-3   G  -  -  G  G  G
08 0-3   -  -  H  -  -  H
09 0-3   I  I  -  -  -  I
10 0-3   -  -  J  -  -  J
11 0-6   K  -  -  -  K  K
12 0-6   L  -  L  -  L  L
13 0-6   M  -  M  M  -  -
14 0-6   N  -  -  -  -  N
15 0-6   O  O  -  -  -  -
16 1-4   -  -  -  P  P  -
17 1-5   -  -  -  -  Q  -
18 1-6   -  R  -  -  R   
19 2-3   S  S  -  -  S   
20 2-6   -  T  T  T      
21 2-6   -  U  U  U      
22 2-6   -  -  V         
23 2-6   -  -  X         
24 3-4   X  Y            
25 3-6   Y  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

VMZVK KRLVV AEOPA VUVNT MMKCF U
-------------------------------
