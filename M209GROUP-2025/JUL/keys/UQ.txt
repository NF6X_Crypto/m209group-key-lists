EFFECTIVE PERIOD:
19-JUL-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  -  -  B  -
03 1-0   C  -  -  C  C  C
04 1-0   -  D  -  D  -  D
05 2-0   E  -  E  E  -  E
06 2-0   -  -  F  -  F  -
07 2-0   G  -  -  G  -  G
08 2-0   -  H  -  -  H  -
09 2-0   -  -  I  -  I  I
10 2-0   -  -  J  J  J  J
11 0-3   K  -  -  -  -  K
12 0-3   L  -  L  L  L  -
13 0-3   M  M  M  -  -  M
14 0-4   -  N  -  -  N  -
15 0-4   -  -  -  O  O  -
16 0-4   P  -  -  P  -  -
17 0-4   -  -  Q  Q  -  Q
18 0-4   -  R  R  R  -   
19 0-4   S  S  S  -  -   
20 0-4   T  T  -  T      
21 0-4   U  U  U  -      
22 0-4   -  -  V         
23 0-6   -  -  X         
24 1-2   X  -            
25 1-3   -  Z            
26 2-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

HZLZZ ZIPJF NKQRM UBHVF JJJWR J
-------------------------------
