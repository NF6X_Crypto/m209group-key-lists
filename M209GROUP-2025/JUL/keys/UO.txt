EFFECTIVE PERIOD:
17-JUL-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   -  B  B  -  B  B
03 2-0   -  C  -  C  C  C
04 2-0   -  -  D  D  -  D
05 2-0   -  E  -  -  E  -
06 2-0   F  F  -  -  -  F
07 2-0   -  G  -  G  G  -
08 2-0   H  -  H  -  -  -
09 2-0   -  -  -  I  -  -
10 2-0   -  -  J  -  -  J
11 2-0   -  K  K  -  K  -
12 2-0   -  L  L  L  -  -
13 2-0   -  -  -  -  -  M
14 2-0   N  -  -  N  N  N
15 0-3   -  -  O  -  -  -
16 0-3   P  -  -  P  P  P
17 0-5   Q  -  Q  -  Q  -
18 0-6   R  -  R  -  -   
19 0-6   -  S  S  S  S   
20 0-6   T  T  T  T      
21 0-6   U  -  U  U      
22 0-6   -  -  V         
23 0-6   -  X  -         
24 1-3   X  Y            
25 1-6   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

DHMLU AEFZY MHVPC MVQGK LRTCI G
-------------------------------
