EFFECTIVE PERIOD:
12-JUL-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  A
02 2-0   -  B  B  B  B  B
03 2-0   -  C  -  -  C  -
04 2-0   -  D  D  -  -  -
05 2-0   E  -  -  -  -  E
06 0-5   F  F  -  F  -  F
07 0-5   G  G  G  -  G  -
08 0-5   -  -  -  H  H  -
09 0-5   I  I  I  I  I  -
10 0-5   J  -  -  J  J  -
11 0-6   -  -  -  -  -  -
12 0-6   -  L  -  L  L  L
13 0-6   M  M  -  -  M  M
14 0-6   N  -  -  N  -  -
15 0-6   O  -  -  O  O  O
16 1-3   -  P  P  -  P  P
17 2-3   -  -  -  Q  -  Q
18 2-3   -  R  R  -  -   
19 2-4   -  S  S  S  -   
20 2-4   T  -  -  -      
21 2-6   U  U  U  U      
22 2-6   V  V  V         
23 2-6   W  -  X         
24 2-6   X  -            
25 3-4   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GZQTA NVUVK MUSOU HVVQZ KJZAK Q
-------------------------------
