EFFECTIVE PERIOD:
06-NOV-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  C  -  C
04 0-3   -  D  -  D  -  -
05 0-3   -  E  -  -  -  -
06 0-3   F  F  F  F  F  -
07 0-3   -  -  -  G  -  G
08 0-3   H  -  -  H  H  H
09 0-3   I  -  -  -  -  I
10 0-3   -  -  -  -  J  -
11 0-3   K  -  -  -  K  K
12 0-4   L  L  L  L  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  N  N  -  N  N
15 0-5   O  -  O  O  O  -
16 0-5   P  P  P  P  P  -
17 0-5   -  Q  -  -  -  Q
18 0-5   -  -  -  -  -   
19 0-5   -  -  S  S  -   
20 0-5   T  T  -  -      
21 0-5   U  U  U  -      
22 0-5   -  V  V         
23 0-6   W  -  X         
24 1-2   -  -            
25 1-3   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

PMWNP ODOJW FKMRD XGMRE CXQAM N
-------------------------------
