EFFECTIVE PERIOD:
15-NOV-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  B  -  B  -  B
03 1-0   -  C  -  C  -  C
04 1-0   -  -  -  -  -  -
05 1-0   -  E  -  -  E  E
06 1-0   F  F  F  F  -  F
07 1-0   G  -  -  -  G  -
08 1-0   H  H  -  -  -  H
09 1-0   I  -  -  -  -  -
10 1-0   -  J  J  J  J  -
11 1-0   K  K  -  K  -  -
12 1-0   -  L  L  -  -  L
13 1-0   -  M  M  M  M  -
14 2-0   N  N  -  -  -  N
15 2-0   O  -  O  O  O  -
16 2-0   -  -  P  -  -  -
17 2-0   -  -  Q  Q  Q  -
18 0-3   R  -  -  -  -   
19 0-4   -  -  S  S  S   
20 0-4   T  T  -  -      
21 0-4   U  -  U  U      
22 0-4   -  -  -         
23 0-4   W  X  -         
24 0-4   X  Y            
25 0-6   -  Z            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FDTDS TFPZM YHGEI FROZT GYGKT V
-------------------------------
