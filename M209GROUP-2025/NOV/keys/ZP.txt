EFFECTIVE PERIOD:
25-NOV-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  -  -  -  B  -
03 1-0   -  -  -  C  -  C
04 1-0   D  D  -  D  D  D
05 1-0   -  -  E  E  E  -
06 1-0   F  F  -  -  F  F
07 0-5   -  G  -  G  G  -
08 0-5   -  -  -  H  -  -
09 0-5   -  I  I  -  -  -
10 0-5   J  J  -  J  -  J
11 0-5   -  -  K  K  K  K
12 0-5   L  -  L  L  -  L
13 0-6   M  M  -  M  -  -
14 0-6   N  N  N  -  -  N
15 0-6   -  -  -  -  -  -
16 0-6   P  P  P  -  -  P
17 0-6   -  Q  -  Q  -  Q
18 1-3   -  -  -  -  R   
19 1-5   S  -  -  -  -   
20 2-6   -  -  -  T      
21 3-4   U  -  U  -      
22 3-6   V  V  V         
23 3-6   W  X  X         
24 5-6   -  -            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ULETA TZLPU NTALV AULUB UDTUI O
-------------------------------
