EFFECTIVE PERIOD:
02-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   -  -  B  -  -  B
03 1-0   -  C  -  -  -  C
04 1-0   D  D  -  -  D  D
05 1-0   -  -  -  -  E  -
06 2-0   F  F  -  F  -  F
07 2-0   G  G  -  -  -  -
08 0-4   H  -  H  -  -  H
09 0-4   -  -  I  -  I  I
10 0-4   -  J  J  J  -  J
11 0-4   K  K  -  -  K  -
12 0-4   -  L  L  L  -  L
13 0-6   M  -  M  M  M  M
14 0-6   -  -  -  N  -  -
15 0-6   O  O  -  O  O  -
16 0-6   P  -  -  -  P  -
17 0-6   -  Q  -  Q  -  Q
18 1-2   R  -  R  R  R   
19 1-4   S  S  -  -  -   
20 2-5   -  -  -  T      
21 3-5   -  U  U  U      
22 4-6   V  V  V         
23 4-6   -  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SSZKQ JVORU LKMDZ KVKYL QTQYZ A
-------------------------------
