EFFECTIVE PERIOD:
07-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  -  -  B  -  B
03 1-0   C  -  -  -  -  -
04 1-0   D  -  -  -  -  -
05 1-0   -  -  E  -  -  -
06 2-0   -  F  F  F  -  -
07 0-3   -  G  -  G  G  G
08 0-3   -  H  -  -  H  H
09 0-3   -  -  -  I  -  I
10 0-3   -  -  J  J  J  -
11 0-5   K  K  -  K  K  -
12 0-5   -  -  -  L  -  L
13 0-5   -  -  M  -  -  -
14 0-5   N  -  N  -  -  -
15 0-5   -  O  O  -  -  -
16 1-2   -  P  P  P  P  P
17 1-5   -  -  Q  Q  Q  Q
18 2-3   R  R  R  -  -   
19 2-3   -  -  -  -  S   
20 2-6   -  -  -  T      
21 3-5   U  -  -  U      
22 3-5   V  V  V         
23 3-5   W  X  X         
24 3-5   X  -            
25 3-6   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OPSPS TKUAT FKGKZ TEPRM AKNTY P
-------------------------------
