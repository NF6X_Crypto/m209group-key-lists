EFFECTIVE PERIOD:
19-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   -  B  -  B  B  -
03 2-0   -  C  C  C  C  C
04 2-0   -  -  -  -  -  D
05 2-0   E  -  E  E  -  -
06 2-0   F  -  -  -  F  F
07 2-0   G  G  G  G  G  G
08 2-0   H  -  H  H  -  -
09 2-0   -  -  -  I  I  I
10 0-3   -  -  -  -  -  J
11 0-4   -  K  K  -  K  K
12 0-5   L  L  -  L  L  -
13 0-5   -  -  M  -  -  M
14 0-5   N  N  -  N  N  -
15 0-5   -  O  O  -  -  O
16 0-5   -  -  -  -  P  -
17 0-5   Q  Q  Q  -  -  -
18 0-5   R  -  -  -  R   
19 0-5   -  S  -  -  -   
20 0-5   T  -  T  T      
21 0-6   -  -  U  -      
22 0-6   -  V  V         
23 0-6   -  X  X         
24 1-4   X  -            
25 2-5   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WEYAY ZKQMS QCOYK HSRLO DWOJQ S
-------------------------------
