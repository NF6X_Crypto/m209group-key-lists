EFFECTIVE PERIOD:
06-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 0-3   -  B  B  B  B  -
03 0-3   C  C  C  C  -  C
04 0-3   -  -  D  -  D  -
05 0-3   -  -  E  E  -  -
06 0-3   F  F  -  F  -  F
07 0-3   G  -  -  G  G  G
08 0-3   H  -  -  -  H  -
09 0-3   -  I  I  -  I  I
10 0-3   J  -  J  -  J  -
11 0-4   -  -  -  -  K  -
12 0-4   -  L  L  L  -  L
13 0-4   M  -  M  -  -  M
14 0-4   N  N  -  N  -  -
15 0-4   -  O  O  -  O  O
16 0-4   -  -  -  P  P  -
17 0-5   -  Q  Q  Q  -  -
18 0-5   R  -  -  -  R   
19 0-6   S  S  S  -  -   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 0-6   V  -  -         
23 0-6   W  -  -         
24 1-5   -  Y            
25 3-4   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWIUR KEHLK HVRYU QEDTR IQVDY M
-------------------------------
