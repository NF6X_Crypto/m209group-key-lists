EFFECTIVE PERIOD:
15-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   B  B  B  B  B  B
03 2-0   C  C  C  -  C  C
04 2-0   -  -  D  -  D  D
05 2-0   E  E  E  E  E  E
06 0-3   -  -  -  -  -  -
07 0-3   -  G  G  -  -  -
08 0-4   -  -  -  -  H  H
09 0-4   -  I  -  -  -  I
10 0-4   J  J  J  J  -  -
11 0-4   K  -  K  -  -  K
12 0-4   -  L  -  L  -  L
13 0-4   M  -  -  M  M  M
14 0-5   -  N  -  N  N  -
15 0-5   -  -  -  -  -  -
16 1-3   P  P  P  P  P  P
17 1-5   -  Q  Q  Q  -  -
18 1-6   R  R  -  R  -   
19 2-4   S  S  S  S  S   
20 2-5   T  -  -  -      
21 2-5   U  -  U  U      
22 2-5   -  V  -         
23 2-5   W  X  -         
24 3-4   X  -            
25 3-5   Y  Z            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

KXUYY PHUYT XOUYH RGKIR ATRLQ O
-------------------------------
