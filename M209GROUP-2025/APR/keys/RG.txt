EFFECTIVE PERIOD:
22-APR-2025 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 2-0   -  -  B  B  -  -
03 2-0   C  C  -  -  C  C
04 2-0   -  -  D  D  -  D
05 2-0   -  E  -  -  -  -
06 0-3   -  F  -  -  -  -
07 0-3   -  -  -  -  -  -
08 0-4   H  H  -  -  H  -
09 0-5   I  -  I  -  I  I
10 0-5   J  J  -  J  -  J
11 0-5   K  K  -  -  K  -
12 0-5   -  -  -  L  -  L
13 0-5   M  M  -  M  -  M
14 0-5   N  -  N  -  N  -
15 0-5   O  -  O  -  -  O
16 0-5   P  -  -  P  -  -
17 0-5   -  Q  Q  -  -  Q
18 0-5   -  -  -  -  R   
19 0-6   -  S  -  S  S   
20 0-6   T  -  T  T      
21 0-6   U  U  U  -      
22 0-6   -  V  V         
23 0-6   -  -  X         
24 2-3   X  -            
25 2-6   Y  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FKVEM AZZPM ZJBJZ VVZAH IYZPM L
-------------------------------
