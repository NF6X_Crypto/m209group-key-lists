EFFECTIVE PERIOD:
07-OCT-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  B  B  B  B  -
03 1-0   -  C  C  -  -  C
04 1-0   D  -  -  D  -  -
05 1-0   -  E  E  E  E  -
06 1-0   -  -  -  F  F  F
07 1-0   -  G  -  -  -  G
08 1-0   H  H  -  H  H  -
09 1-0   I  I  I  -  I  I
10 1-0   J  J  -  -  J  J
11 1-0   -  -  K  K  -  -
12 2-0   -  -  -  -  -  -
13 0-3   M  -  M  M  -  -
14 0-3   N  -  -  N  N  -
15 0-4   O  O  -  O  -  O
16 0-4   P  -  P  P  -  -
17 0-4   Q  -  -  -  Q  -
18 0-4   -  R  -  -  R   
19 0-4   S  -  S  -  -   
20 0-4   -  -  T  T      
21 0-4   -  U  U  -      
22 0-5   -  V  -         
23 0-6   -  -  X         
24 1-4   X  Y            
25 2-3   Y  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

WTMWB EGOMA WFPNW ELDRW RLLTW S
-------------------------------
