EFFECTIVE PERIOD:
03-OCT-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   B  B  -  B  B  -
03 2-0   C  C  C  -  -  -
04 2-0   -  -  -  D  -  -
05 0-4   E  E  E  E  -  -
06 0-4   F  F  -  F  F  F
07 0-5   -  -  -  G  -  -
08 0-5   H  -  H  -  H  H
09 0-5   -  -  I  -  -  I
10 0-5   J  -  -  -  J  -
11 0-5   -  K  K  K  -  -
12 0-6   -  L  L  -  L  L
13 0-6   M  -  -  -  -  -
14 0-6   -  -  N  N  -  N
15 0-6   -  -  O  -  O  O
16 0-6   P  -  -  P  P  P
17 0-6   -  Q  -  -  -  -
18 1-3   -  -  R  -  -   
19 2-3   -  S  S  S  -   
20 2-4   T  T  -  T      
21 2-5   U  -  -  -      
22 2-5   V  -  V         
23 2-5   -  X  -         
24 2-5   -  -            
25 3-4   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MNJUY TFERJ FAVUN AKMKO KTZMG Z
-------------------------------
