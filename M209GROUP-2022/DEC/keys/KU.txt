EFFECTIVE PERIOD:
30-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   -  -  C  C  -  -
04 2-0   D  D  -  D  D  D
05 2-0   E  E  -  E  -  E
06 2-0   F  F  F  F  -  -
07 2-0   -  -  -  -  -  G
08 2-0   H  -  -  -  H  -
09 2-0   I  -  I  I  -  I
10 2-0   -  -  J  -  J  -
11 2-0   -  -  -  K  -  -
12 0-3   -  -  L  -  L  -
13 0-3   -  M  M  -  M  -
14 0-3   N  -  -  -  N  N
15 0-3   O  O  -  -  O  O
16 0-4   -  -  P  -  -  P
17 0-5   Q  -  Q  Q  -  Q
18 0-5   -  R  R  R  -   
19 0-5   S  -  S  S  S   
20 0-5   -  T  -  -      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-6   X  -            
25 2-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PHKZK WRWIF VZTFN ZMGHN KOOPB T
-------------------------------
