EFFECTIVE PERIOD:
06-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  B  -  B  -  -
03 1-0   -  -  -  -  -  -
04 1-0   -  -  D  -  -  D
05 1-0   -  E  -  E  E  -
06 1-0   F  -  F  F  -  -
07 2-0   -  G  G  -  G  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  I  -  -  I
10 0-3   J  J  J  -  -  -
11 0-3   -  K  K  K  -  K
12 0-3   L  -  -  L  -  -
13 0-3   -  M  -  -  M  M
14 0-3   -  -  N  -  -  -
15 0-3   O  O  -  O  O  -
16 0-5   -  P  -  P  P  P
17 0-5   Q  -  Q  Q  Q  Q
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  -  -      
22 1-3   V  V  V         
23 1-3   -  -  -         
24 1-3   X  Y            
25 1-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPNSG ZRBWK OJGOI PSPSO YARUG Q
-------------------------------
