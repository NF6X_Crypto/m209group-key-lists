EFFECTIVE PERIOD:
20-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   -  B  -  B  B  B
03 0-3   -  -  C  -  C  C
04 0-3   -  D  D  D  -  D
05 0-3   E  E  -  -  -  -
06 0-3   F  F  F  -  F  -
07 0-3   G  -  -  -  -  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  -  -  I  -
10 0-3   -  -  J  J  J  J
11 0-3   -  K  -  -  -  K
12 0-3   L  L  -  -  L  -
13 0-3   -  -  M  M  -  -
14 0-3   -  -  -  -  N  N
15 0-4   O  O  -  -  O  -
16 0-4   -  P  -  P  -  P
17 0-4   Q  -  Q  -  Q  Q
18 0-4   -  R  R  R  -   
19 0-4   S  -  S  S  S   
20 0-4   -  -  T  -      
21 0-4   U  -  -  U      
22 0-5   V  V  -         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-6   Y  Z            
26 1-2   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

NDDAL ZVGOO AZVQD POSKP AMTAK E
-------------------------------
