EFFECTIVE PERIOD:
17-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  -  -  B  -
03 1-0   -  C  -  C  C  C
04 1-0   D  D  -  D  D  -
05 1-0   E  -  -  -  E  E
06 1-0   F  -  F  -  -  -
07 2-0   -  G  -  -  G  -
08 2-0   H  H  -  -  -  H
09 2-0   I  I  -  I  -  I
10 2-0   -  J  J  -  J  -
11 0-3   K  -  K  K  K  -
12 0-4   L  L  -  L  L  -
13 0-4   -  M  M  M  -  M
14 0-4   -  -  N  N  -  N
15 0-4   O  -  O  -  O  O
16 0-4   -  P  P  P  -  P
17 0-4   Q  Q  -  -  -  -
18 0-4   R  -  -  R  -   
19 0-4   -  -  S  S  S   
20 0-6   -  T  T  T      
21 0-6   -  -  U  U      
22 1-2   -  -  V         
23 1-4   -  X  X         
24 1-4   X  Y            
25 2-6   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MYSOD UMMYK QZKFR OIQOA UUAIM L
-------------------------------
