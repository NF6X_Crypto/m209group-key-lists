SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF DEC 2022

    01 DEC 2022  00:00-23:59 GMT:  USE KEY JR
    02 DEC 2022  00:00-23:59 GMT:  USE KEY JS
    03 DEC 2022  00:00-23:59 GMT:  USE KEY JT
    04 DEC 2022  00:00-23:59 GMT:  USE KEY JU
    05 DEC 2022  00:00-23:59 GMT:  USE KEY JV
    06 DEC 2022  00:00-23:59 GMT:  USE KEY JW
    07 DEC 2022  00:00-23:59 GMT:  USE KEY JX
    08 DEC 2022  00:00-23:59 GMT:  USE KEY JY
    09 DEC 2022  00:00-23:59 GMT:  USE KEY JZ
    10 DEC 2022  00:00-23:59 GMT:  USE KEY KA
    11 DEC 2022  00:00-23:59 GMT:  USE KEY KB
    12 DEC 2022  00:00-23:59 GMT:  USE KEY KC
    13 DEC 2022  00:00-23:59 GMT:  USE KEY KD
    14 DEC 2022  00:00-23:59 GMT:  USE KEY KE
    15 DEC 2022  00:00-23:59 GMT:  USE KEY KF
    16 DEC 2022  00:00-23:59 GMT:  USE KEY KG
    17 DEC 2022  00:00-23:59 GMT:  USE KEY KH
    18 DEC 2022  00:00-23:59 GMT:  USE KEY KI
    19 DEC 2022  00:00-23:59 GMT:  USE KEY KJ
    20 DEC 2022  00:00-23:59 GMT:  USE KEY KK
    21 DEC 2022  00:00-23:59 GMT:  USE KEY KL
    22 DEC 2022  00:00-23:59 GMT:  USE KEY KM
    23 DEC 2022  00:00-23:59 GMT:  USE KEY KN
    24 DEC 2022  00:00-23:59 GMT:  USE KEY KO
    25 DEC 2022  00:00-23:59 GMT:  USE KEY KP
    26 DEC 2022  00:00-23:59 GMT:  USE KEY KQ
    27 DEC 2022  00:00-23:59 GMT:  USE KEY KR
    28 DEC 2022  00:00-23:59 GMT:  USE KEY KS
    29 DEC 2022  00:00-23:59 GMT:  USE KEY KT
    30 DEC 2022  00:00-23:59 GMT:  USE KEY KU
    31 DEC 2022  00:00-23:59 GMT:  USE KEY KV

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   B  B  B  B  -  B
03 1-0   -  -  -  -  -  -
04 1-0   -  -  -  D  -  -
05 1-0   -  -  E  E  -  E
06 0-3   -  -  -  F  -  -
07 0-3   G  G  G  -  -  -
08 0-3   H  H  -  -  H  -
09 0-4   -  I  -  -  I  -
10 0-4   J  J  J  J  -  J
11 0-5   -  -  K  -  -  -
12 0-5   -  -  L  -  L  L
13 0-5   M  M  M  -  M  -
14 0-5   -  N  -  N  -  -
15 0-5   O  O  O  O  O  O
16 1-4   -  -  -  -  -  -
17 1-5   -  -  Q  Q  Q  Q
18 2-3   -  R  -  R  R   
19 2-3   S  -  S  -  S   
20 2-4   -  T  T  -      
21 2-6   U  U  -  -      
22 3-4   -  -  V         
23 3-4   W  -  -         
24 3-5   -  -            
25 3-5   -  -            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

NVFYG ASQRZ LTOFO JRKOS YLUOS X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  B  B  B  -  -
03 1-0   C  -  -  -  -  C
04 2-0   D  D  D  D  -  -
05 2-0   -  E  E  E  -  -
06 2-0   -  -  F  -  -  F
07 0-3   -  G  -  G  G  G
08 0-3   H  H  H  -  H  -
09 0-3   -  -  -  -  -  -
10 0-3   -  -  -  J  J  -
11 0-3   -  K  K  -  K  K
12 0-3   -  -  -  L  L  L
13 0-4   M  M  -  M  M  M
14 0-6   N  -  -  -  -  -
15 0-6   -  -  O  O  -  O
16 0-6   P  P  P  P  -  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  -  R  -  R   
19 0-6   -  S  -  -  S   
20 1-3   T  -  T  T      
21 1-3   U  -  -  U      
22 1-3   -  -  V         
23 1-3   -  X  -         
24 1-6   X  Y            
25 2-4   Y  -            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

RWUMZ ISKUT QKMXT QZWUI MMZTX K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  B  B  B  -
03 1-0   -  -  -  -  C  C
04 1-0   D  -  -  -  -  -
05 1-0   -  E  -  -  E  -
06 1-0   F  F  F  F  F  -
07 0-3   G  G  G  G  -  G
08 0-3   H  -  H  -  H  H
09 0-3   I  I  -  -  -  I
10 0-3   J  -  J  J  -  J
11 0-3   -  K  K  K  -  K
12 0-3   L  L  -  -  -  -
13 0-6   -  M  M  M  M  M
14 0-6   -  N  -  -  -  N
15 0-6   -  -  O  -  -  -
16 1-3   P  -  -  -  P  -
17 1-5   -  -  Q  Q  Q  Q
18 1-6   R  R  -  -  R   
19 2-6   S  S  S  -  -   
20 3-6   T  T  -  T      
21 3-6   U  -  -  -      
22 3-6   -  V  -         
23 3-6   -  X  X         
24 4-5   -  -            
25 4-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UOTZN WNCAU PLLNU UZZKA ACTNP E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  B  B  -  -  -
03 1-0   C  C  -  -  C  -
04 1-0   -  -  D  -  -  -
05 1-0   -  -  E  E  E  E
06 1-0   -  F  -  -  -  F
07 2-0   G  G  -  G  -  G
08 0-3   H  H  H  H  H  H
09 0-3   -  -  I  -  -  -
10 0-3   -  J  -  -  -  J
11 0-3   K  -  -  K  K  K
12 0-3   L  -  L  -  L  -
13 0-6   M  -  M  -  M  -
14 0-6   -  N  N  N  -  -
15 0-6   -  O  -  -  O  O
16 0-6   -  P  -  P  P  -
17 0-6   Q  -  Q  Q  -  -
18 0-6   R  R  -  R  -   
19 0-6   S  -  -  -  S   
20 1-3   T  -  T  -      
21 1-3   U  -  U  U      
22 1-3   V  -  -         
23 1-3   -  -  -         
24 1-5   X  Y            
25 2-4   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UNKTU MQKMC MAVAR CKASC CYSQC C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  -  -  B  -  -
03 2-0   -  C  -  C  C  -
04 0-3   D  D  D  D  -  D
05 0-3   -  -  E  E  E  -
06 0-3   F  F  F  -  F  -
07 0-3   G  G  G  -  -  -
08 0-3   -  H  H  -  H  H
09 0-3   -  I  I  I  -  I
10 0-4   -  J  -  -  J  -
11 0-4   -  K  -  -  -  K
12 0-4   L  -  -  L  L  -
13 0-4   M  -  -  -  M  M
14 0-4   -  N  N  -  N  N
15 0-4   -  -  O  O  -  O
16 0-5   P  -  P  P  -  P
17 0-5   -  -  Q  -  -  Q
18 0-5   R  -  -  R  R   
19 0-5   -  -  S  S  S   
20 1-2   T  -  -  T      
21 1-4   U  U  -  U      
22 2-6   V  -  V         
23 3-5   W  -  -         
24 3-5   -  Y            
25 3-5   Y  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

TOZSE VUSVF QMSYP PPAML SLEVU O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  B  -  B  -  -
03 1-0   -  -  -  -  -  -
04 1-0   -  -  D  -  -  D
05 1-0   -  E  -  E  E  -
06 1-0   F  -  F  F  -  -
07 2-0   -  G  G  -  G  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  I  -  -  I
10 0-3   J  J  J  -  -  -
11 0-3   -  K  K  K  -  K
12 0-3   L  -  -  L  -  -
13 0-3   -  M  -  -  M  M
14 0-3   -  -  N  -  -  -
15 0-3   O  O  -  O  O  -
16 0-5   -  P  -  P  P  P
17 0-5   Q  -  Q  Q  Q  Q
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  -  -      
22 1-3   V  V  V         
23 1-3   -  -  -         
24 1-3   X  Y            
25 1-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPNSG ZRBWK OJGOI PSPSO YARUG Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  -
02 2-0   B  B  -  -  B  B
03 0-3   C  C  C  -  C  C
04 0-3   D  -  D  -  D  D
05 0-3   -  -  -  E  E  E
06 0-3   F  -  F  -  -  F
07 0-3   G  -  -  -  -  -
08 0-3   -  -  H  -  H  H
09 0-4   I  I  I  I  I  I
10 0-4   -  -  J  -  -  J
11 0-4   -  K  K  K  K  K
12 0-4   -  -  -  L  -  L
13 0-4   -  M  M  M  -  -
14 0-4   -  N  -  -  -  -
15 0-5   -  O  O  O  -  -
16 0-6   P  P  -  -  -  -
17 0-6   -  Q  Q  -  Q  -
18 0-6   R  -  -  R  -   
19 0-6   S  S  S  S  S   
20 1-5   -  -  -  T      
21 2-4   -  -  U  -      
22 2-5   V  V  V         
23 3-6   W  -  -         
24 3-6   -  Y            
25 3-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

TSLKD UIWOU LLUVJ RLWRV RKJKP M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 2-0   -  -  -  -  B  B
03 2-0   C  C  C  -  C  -
04 2-0   -  D  -  -  D  D
05 2-0   -  E  E  E  -  E
06 2-0   -  F  F  -  F  F
07 0-4   G  -  -  G  G  G
08 0-4   -  -  -  H  -  H
09 0-5   -  I  I  I  I  -
10 0-5   -  -  -  J  -  -
11 0-5   K  -  K  K  -  K
12 0-5   -  -  L  -  L  -
13 0-5   M  -  -  -  -  M
14 0-6   N  -  -  -  -  N
15 0-6   -  -  O  O  -  -
16 0-6   -  P  -  P  P  -
17 0-6   Q  -  -  Q  Q  Q
18 1-3   -  -  R  R  R   
19 2-3   S  S  -  S  S   
20 2-3   T  -  T  -      
21 2-4   U  U  -  -      
22 2-4   V  -  V         
23 2-4   -  X  -         
24 2-4   -  Y            
25 3-6   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LNMNW NAGSV LXIAY QUNKZ IDZSQ R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  D  -  -  D  D
05 1-0   E  E  E  E  E  -
06 2-0   -  F  F  F  F  -
07 2-0   -  -  G  G  -  -
08 2-0   -  -  H  H  -  H
09 2-0   I  -  -  I  I  I
10 2-0   J  -  -  -  -  J
11 2-0   -  -  -  K  -  -
12 2-0   L  -  L  -  L  -
13 0-4   M  M  -  -  -  M
14 0-4   -  -  -  -  N  -
15 0-6   O  O  -  O  O  O
16 1-2   P  P  P  -  -  -
17 1-4   -  Q  Q  -  Q  -
18 1-4   R  R  -  R  R   
19 1-4   -  -  -  S  S   
20 1-4   -  T  -  T      
21 2-6   -  -  -  U      
22 3-4   V  V  V         
23 3-6   -  -  X         
24 4-5   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SIALF TQVRL PKLYV AVLZT JHKYG U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   B  B  B  B  -  B
03 2-0   C  -  C  -  C  C
04 0-3   D  D  D  -  D  D
05 0-4   -  -  E  E  -  -
06 0-4   F  F  -  F  F  F
07 0-4   -  G  -  G  G  -
08 0-5   -  -  -  -  H  -
09 0-5   -  -  I  I  -  I
10 0-5   -  -  J  -  J  -
11 0-5   K  -  -  K  -  K
12 0-5   L  -  -  -  L  L
13 0-5   -  M  -  M  M  M
14 0-5   -  N  N  -  -  -
15 0-6   -  O  O  O  O  O
16 0-6   -  P  -  -  -  -
17 0-6   Q  -  -  Q  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   S  -  S  -  -   
20 1-5   -  -  T  T      
21 2-3   -  U  -  -      
22 2-6   -  -  V         
23 4-5   W  -  -         
24 4-5   -  -            
25 4-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AXNQN NLIUN ZNMRV VFZIX LOLSR R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 0-3   -  -  B  -  B  -
03 0-3   -  -  -  C  -  -
04 0-3   -  -  -  D  D  D
05 0-3   E  E  E  -  -  -
06 0-3   F  -  F  -  -  F
07 0-3   G  G  G  G  -  G
08 0-3   -  H  -  -  -  -
09 0-3   I  I  -  I  -  I
10 0-4   -  -  -  -  -  -
11 0-4   K  -  -  -  K  -
12 0-4   L  -  -  L  L  -
13 0-4   M  M  -  M  -  M
14 0-4   -  -  N  -  -  N
15 0-4   -  O  O  -  -  -
16 0-4   -  -  P  P  P  P
17 0-4   -  -  -  Q  Q  Q
18 0-4   R  R  -  R  R   
19 0-4   -  S  -  -  S   
20 0-4   -  -  T  T      
21 0-5   U  U  U  -      
22 0-5   V  V  V         
23 0-5   W  -  -         
24 0-5   X  -            
25 0-5   -  Z            
26 2-5   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

ZHJMR SPAIF JALAH VINGH LLBPZ Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  B  B  -  -  B
03 1-0   C  C  C  C  C  -
04 1-0   -  D  D  -  -  -
05 1-0   -  -  -  E  E  -
06 1-0   F  -  -  F  F  F
07 1-0   -  G  G  G  -  G
08 2-0   H  -  H  H  H  -
09 2-0   -  I  I  -  I  -
10 2-0   J  -  J  J  J  J
11 2-0   -  -  -  -  K  -
12 2-0   L  L  -  L  -  -
13 2-0   M  -  -  M  -  M
14 0-3   -  -  N  -  N  N
15 0-5   -  -  -  -  O  O
16 0-5   -  P  -  P  P  -
17 0-5   Q  Q  Q  -  Q  -
18 0-5   R  -  -  -  -   
19 0-6   S  S  S  -  -   
20 1-2   -  T  T  -      
21 1-2   -  U  -  -      
22 1-2   -  -  -         
23 1-2   -  -  -         
24 1-4   X  Y            
25 2-5   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CSZUR RMZOS TNAAL GURTO OIXTG C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  C  C  -  C
04 1-0   -  -  D  -  D  D
05 0-4   -  E  -  E  -  -
06 0-4   -  -  -  F  F  F
07 0-4   -  -  -  -  G  -
08 0-4   H  -  H  H  -  -
09 0-4   I  I  I  -  I  -
10 0-4   J  -  -  -  -  J
11 0-4   K  -  -  K  K  K
12 0-5   L  -  -  -  -  -
13 0-5   -  -  M  -  M  M
14 0-5   -  -  -  -  N  -
15 0-5   -  -  -  -  -  -
16 1-2   P  P  P  -  -  -
17 1-4   Q  Q  -  Q  Q  -
18 1-5   -  R  -  -  R   
19 2-3   -  -  -  -  -   
20 2-5   -  T  T  T      
21 2-5   U  U  U  U      
22 3-5   V  -  -         
23 4-5   W  X  X         
24 4-5   X  -            
25 4-5   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

HTODL TTUVA OAFAU DUCTV SSIZM V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  A
02 2-0   -  B  B  B  B  B
03 2-0   -  C  -  C  -  C
04 2-0   D  -  -  -  D  -
05 2-0   -  E  -  -  E  -
06 0-3   -  -  -  -  F  F
07 0-4   -  -  G  -  -  -
08 0-4   H  H  -  -  H  -
09 0-4   -  I  I  I  -  I
10 0-4   -  -  J  -  -  J
11 0-4   K  K  K  -  -  -
12 0-4   -  -  -  L  L  -
13 0-4   M  M  -  -  M  M
14 0-4   N  N  -  -  N  -
15 0-5   O  O  O  O  O  -
16 0-5   -  -  P  -  -  -
17 0-6   -  -  Q  Q  -  Q
18 0-6   R  -  R  -  -   
19 0-6   -  -  S  S  S   
20 1-4   T  -  -  -      
21 2-4   U  U  U  U      
22 2-4   V  -  V         
23 2-4   -  X  -         
24 2-4   X  -            
25 2-6   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NEVEO CWIAU URQNA QGMMR RODWQ Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  B  B  -  B
03 1-0   C  C  -  C  -  C
04 2-0   D  D  -  -  D  -
05 2-0   E  E  E  -  E  E
06 2-0   F  -  F  F  -  -
07 2-0   G  -  G  G  G  -
08 0-3   -  -  -  -  H  -
09 0-4   -  I  I  -  I  -
10 0-4   J  J  J  -  -  J
11 0-4   -  K  -  -  K  -
12 0-4   -  L  -  L  -  -
13 0-4   -  -  -  -  -  M
14 0-4   -  -  -  N  -  N
15 0-6   O  O  O  -  -  -
16 0-6   P  -  P  P  -  -
17 0-6   -  -  Q  Q  Q  -
18 0-6   R  R  -  R  R   
19 0-6   -  -  -  -  S   
20 1-4   -  -  -  T      
21 1-4   -  -  U  -      
22 1-4   -  V  V         
23 1-4   W  X  -         
24 1-6   X  -            
25 2-3   -  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

NMQXJ KAAUG MTGNV VQHCN YFPZT A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 2-0   -  B  B  -  -  -
03 0-3   -  C  C  -  C  C
04 0-3   D  D  -  -  -  -
05 0-3   -  -  -  E  -  E
06 0-3   F  F  F  F  F  F
07 0-3   -  -  G  G  -  G
08 0-3   H  -  H  H  -  H
09 0-3   I  I  I  -  I  -
10 0-3   -  J  J  J  -  J
11 0-3   -  -  -  -  -  K
12 0-3   L  -  -  L  -  -
13 0-3   -  -  M  M  -  -
14 0-3   N  -  -  N  N  N
15 0-4   O  O  -  O  -  -
16 0-4   P  P  -  -  P  -
17 0-4   -  Q  -  -  Q  -
18 0-4   R  R  -  -  R   
19 0-4   -  -  S  -  S   
20 0-4   T  -  -  T      
21 0-4   -  U  U  U      
22 0-4   -  -  -         
23 0-5   W  -  X         
24 0-5   X  Y            
25 0-5   -  Z            
26 1-5   -               
27 1-6                   
-------------------------------
26 LETTER CHECK

LIOBF PEZKV QMNNL TAMWP JLWLF G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  -  -  B  -
03 1-0   -  C  -  C  C  C
04 1-0   D  D  -  D  D  -
05 1-0   E  -  -  -  E  E
06 1-0   F  -  F  -  -  -
07 2-0   -  G  -  -  G  -
08 2-0   H  H  -  -  -  H
09 2-0   I  I  -  I  -  I
10 2-0   -  J  J  -  J  -
11 0-3   K  -  K  K  K  -
12 0-4   L  L  -  L  L  -
13 0-4   -  M  M  M  -  M
14 0-4   -  -  N  N  -  N
15 0-4   O  -  O  -  O  O
16 0-4   -  P  P  P  -  P
17 0-4   Q  Q  -  -  -  -
18 0-4   R  -  -  R  -   
19 0-4   -  -  S  S  S   
20 0-6   -  T  T  T      
21 0-6   -  -  U  U      
22 1-2   -  -  V         
23 1-4   -  X  X         
24 1-4   X  Y            
25 2-6   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MYSOD UMMYK QZKFR OIQOA UUAIM L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  A
02 2-0   -  -  -  B  B  B
03 2-0   -  -  -  -  -  C
04 2-0   -  -  D  D  D  -
05 2-0   -  -  E  -  E  -
06 2-0   F  F  -  F  F  F
07 0-3   -  -  -  G  -  G
08 0-4   -  -  H  -  -  H
09 0-4   I  -  -  I  -  -
10 0-4   J  J  -  -  J  -
11 0-5   -  -  K  -  -  K
12 0-5   -  -  L  -  L  -
13 0-6   M  M  M  -  -  M
14 0-6   -  N  N  N  -  N
15 0-6   O  O  O  O  -  -
16 0-6   -  -  -  P  P  P
17 0-6   Q  Q  Q  -  Q  -
18 0-6   -  R  -  R  -   
19 0-6   S  -  S  -  -   
20 1-3   -  -  -  -      
21 2-4   U  -  -  -      
22 2-5   V  V  -         
23 3-5   W  X  X         
24 4-6   -  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XKVYD UATSA KLSQR HIUZP VCRTA T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  A
02 2-0   B  -  -  -  -  -
03 2-0   -  C  C  C  C  C
04 0-3   D  -  -  D  D  -
05 0-3   E  -  -  E  E  -
06 0-3   -  -  -  -  -  -
07 0-3   G  G  -  -  -  -
08 0-3   H  H  -  H  H  H
09 0-4   -  -  I  -  I  I
10 0-4   J  -  J  -  -  -
11 0-4   -  -  K  K  -  K
12 0-4   -  L  L  L  L  -
13 0-4   -  M  M  M  -  M
14 0-4   N  N  -  N  N  -
15 0-4   O  -  O  -  -  O
16 1-2   -  -  P  -  P  -
17 1-2   -  -  Q  Q  Q  Q
18 1-2   -  R  R  R  -   
19 1-3   S  -  S  -  S   
20 1-6   T  T  T  -      
21 2-4   -  U  -  U      
22 2-4   V  V  -         
23 2-4   -  X  -         
24 2-4   X  -            
25 2-5   -  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

ORJTX ZTMVB NMWTF GMNTU VUZMT V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   -  B  -  B  B  B
03 0-3   -  -  C  -  C  C
04 0-3   -  D  D  D  -  D
05 0-3   E  E  -  -  -  -
06 0-3   F  F  F  -  F  -
07 0-3   G  -  -  -  -  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  -  -  I  -
10 0-3   -  -  J  J  J  J
11 0-3   -  K  -  -  -  K
12 0-3   L  L  -  -  L  -
13 0-3   -  -  M  M  -  -
14 0-3   -  -  -  -  N  N
15 0-4   O  O  -  -  O  -
16 0-4   -  P  -  P  -  P
17 0-4   Q  -  Q  -  Q  Q
18 0-4   -  R  R  R  -   
19 0-4   S  -  S  S  S   
20 0-4   -  -  T  -      
21 0-4   U  -  -  U      
22 0-5   V  V  -         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-6   Y  Z            
26 1-2   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

NDDAL ZVGOO AZVQD POSKP AMTAK E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 0-3   -  -  B  B  -  B
03 0-3   C  C  C  -  C  -
04 0-3   -  D  -  -  -  D
05 0-3   -  E  E  E  -  -
06 0-3   F  -  F  F  -  -
07 0-3   G  -  G  G  -  G
08 0-3   H  H  H  -  -  -
09 0-3   -  -  I  -  -  I
10 0-3   J  J  -  -  J  -
11 0-3   -  -  -  -  K  -
12 0-5   L  L  L  -  L  L
13 0-5   -  -  -  M  M  M
14 0-5   -  N  -  -  N  -
15 0-6   -  -  -  -  O  O
16 0-6   -  P  -  P  -  -
17 0-6   Q  -  -  Q  -  -
18 0-6   -  -  R  R  -   
19 0-6   S  -  S  -  S   
20 0-6   -  T  T  T      
21 0-6   U  U  -  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 0-6   X  -            
25 0-6   -  Z            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

EQOFW MLPXZ AKJNL WKOAO FZFZM M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  -  B  -  -
03 1-0   C  C  C  C  C  C
04 2-0   -  -  D  -  D  -
05 0-3   -  -  -  E  E  E
06 0-3   -  F  F  F  -  -
07 0-3   G  -  -  -  G  -
08 0-4   -  -  -  H  H  -
09 0-4   -  I  -  -  -  I
10 0-4   J  J  -  -  -  -
11 0-4   K  -  -  K  K  K
12 0-4   -  L  L  -  L  L
13 0-4   M  M  M  -  M  M
14 0-4   -  -  -  -  N  N
15 0-4   -  O  O  O  -  O
16 0-4   P  -  P  -  -  -
17 0-4   -  -  Q  Q  -  Q
18 0-4   R  -  -  -  -   
19 0-5   -  S  S  -  -   
20 0-6   T  -  -  -      
21 0-6   U  U  -  U      
22 0-6   -  -  V         
23 0-6   -  X  -         
24 1-2   X  Y            
25 1-3   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MKZJW IHFWW RXFVM KNKGT MOGWJ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   -  -  B  B  -  B
03 0-3   -  C  C  -  -  C
04 0-3   D  -  D  -  -  -
05 0-3   E  E  -  E  E  -
06 0-3   F  F  -  F  -  F
07 0-3   -  G  -  -  -  -
08 0-3   -  H  -  -  H  -
09 0-5   I  -  I  I  -  -
10 0-5   -  -  -  -  J  -
11 0-5   -  K  K  K  K  -
12 0-5   -  L  L  L  L  -
13 0-6   M  M  -  -  -  M
14 0-6   -  -  -  N  -  N
15 0-6   O  -  -  O  O  -
16 1-2   -  -  -  -  -  -
17 1-2   Q  -  Q  -  -  Q
18 1-2   R  R  R  -  -   
19 1-4   S  S  -  -  S   
20 1-5   T  -  T  T      
21 2-5   U  U  U  -      
22 2-6   -  V  -         
23 2-6   -  X  X         
24 2-6   -  -            
25 2-6   Y  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

GEMAT EUATT IMUSE QLQWS PRQPH M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 0-3   B  -  B  -  -  -
03 0-3   C  -  C  C  -  -
04 0-3   -  -  D  D  D  D
05 0-3   -  E  E  E  E  -
06 0-3   F  F  -  -  -  F
07 0-4   G  G  G  -  -  G
08 0-4   -  H  -  H  -  -
09 0-4   -  -  I  I  -  -
10 0-4   -  J  J  -  -  J
11 0-4   K  -  K  -  K  K
12 0-4   -  L  L  L  L  -
13 0-4   -  -  M  -  -  M
14 0-4   N  N  -  -  -  -
15 0-5   O  -  -  O  O  O
16 0-5   P  -  -  P  P  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   R  R  R  R  R   
19 0-5   -  S  -  -  -   
20 0-5   T  -  -  T      
21 0-5   U  -  -  U      
22 0-5   -  V  V         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-2   Z               
27 1-6                   
-------------------------------
26 LETTER CHECK

NZNVA VLNPC FVASE ASNNQ NKRTV S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  -  -  -  B
03 1-0   -  C  -  C  -  -
04 1-0   -  D  -  -  D  D
05 1-0   E  E  E  -  E  E
06 1-0   -  -  -  -  -  F
07 1-0   -  G  G  -  -  -
08 0-3   -  -  -  H  H  -
09 0-3   I  -  -  -  I  -
10 0-3   -  J  -  -  -  -
11 0-3   K  -  K  K  K  K
12 0-4   -  -  -  -  L  -
13 0-4   -  M  M  M  -  -
14 0-4   N  N  N  N  N  N
15 0-4   O  O  O  O  -  O
16 0-4   -  -  -  P  P  -
17 0-4   Q  Q  Q  -  Q  Q
18 0-4   R  R  R  R  -   
19 0-4   -  -  S  -  S   
20 0-4   T  T  T  -      
21 0-4   -  U  U  U      
22 0-4   V  -  -         
23 0-5   W  X  -         
24 1-3   X  -            
25 1-4   -  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

TYVSP TOPVZ GWCAG KSBAU GWRTA K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   B  -  B  B  -  B
03 2-0   C  C  C  -  -  -
04 0-3   -  D  D  D  D  -
05 0-3   -  -  E  E  E  -
06 0-3   F  -  F  -  F  -
07 0-3   G  -  -  G  -  G
08 0-3   H  H  H  H  H  -
09 0-3   -  I  -  -  -  -
10 0-3   J  -  J  J  -  -
11 0-3   -  K  -  K  -  K
12 0-4   -  -  -  L  L  -
13 0-5   -  -  -  -  -  M
14 0-5   -  N  N  N  N  N
15 0-5   -  O  -  -  O  -
16 0-5   -  -  P  -  P  P
17 0-5   Q  Q  -  -  -  Q
18 0-5   R  -  R  -  -   
19 0-5   S  -  S  -  S   
20 1-3   -  T  T  -      
21 2-5   -  U  -  U      
22 2-6   V  -  -         
23 3-5   -  X  -         
24 3-5   X  Y            
25 3-5   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZCZOU SSLSF BUWAR WCOHA DPOWE S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  -  -  -  -  -
03 2-0   -  -  C  -  C  C
04 2-0   -  D  D  D  D  D
05 2-0   E  E  -  E  -  -
06 2-0   F  F  F  -  F  -
07 0-3   -  G  G  G  G  G
08 0-3   -  -  -  H  H  H
09 0-3   I  I  -  I  -  I
10 0-3   -  -  J  J  -  -
11 0-3   -  -  -  -  -  K
12 0-3   L  -  L  -  -  -
13 0-4   M  -  M  M  M  M
14 0-4   -  -  -  -  -  -
15 0-4   O  -  O  -  O  -
16 1-2   -  P  P  -  P  P
17 1-2   -  -  Q  -  Q  Q
18 1-2   -  -  R  R  R   
19 1-2   -  S  S  S  -   
20 1-4   T  T  -  T      
21 1-4   U  -  -  -      
22 1-4   V  V  V         
23 1-6   W  X  -         
24 2-3   X  -            
25 3-4   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JQKIW APUVJ QJVQL AZSHX AOTLK S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  A
02 2-0   -  B  -  -  B  -
03 2-0   -  -  C  -  -  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  E  E  -  -
06 0-3   -  -  F  F  F  F
07 0-3   G  -  -  -  -  G
08 0-3   -  -  H  H  H  H
09 0-3   I  I  -  I  -  -
10 0-3   -  -  J  J  -  -
11 0-6   -  -  -  -  -  -
12 0-6   L  L  -  L  L  -
13 0-6   M  M  M  -  M  -
14 0-6   -  N  N  -  N  N
15 0-6   O  -  O  O  O  -
16 0-6   P  P  P  P  P  P
17 0-6   Q  -  -  -  Q  -
18 1-2   R  R  R  R  -   
19 1-5   S  S  -  S  -   
20 2-3   -  -  -  -      
21 2-3   U  -  U  -      
22 2-3   V  -  -         
23 2-3   -  -  X         
24 2-4   X  Y            
25 2-5   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NTOVT MCRDA BLZLS NAMAT LCATA L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  -  -  B  B
03 1-0   -  C  -  C  C  C
04 1-0   -  -  -  -  -  D
05 2-0   E  -  E  -  E  -
06 2-0   -  -  -  F  -  F
07 2-0   G  G  G  G  -  G
08 2-0   -  H  H  H  -  H
09 2-0   -  -  I  -  I  -
10 2-0   -  J  -  J  -  -
11 0-6   K  -  K  K  K  -
12 0-6   L  -  -  -  -  -
13 0-6   M  M  -  M  -  M
14 0-6   N  -  -  N  N  -
15 0-6   O  -  O  -  -  -
16 1-2   P  P  P  -  P  -
17 1-2   -  -  -  -  Q  -
18 1-2   -  R  R  R  -   
19 1-2   S  S  S  -  -   
20 1-3   -  -  T  -      
21 1-5   U  -  -  -      
22 1-6   V  -  -         
23 1-6   -  X  X         
24 1-6   -  Y            
25 2-6   Y  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ONMTN ONJTA UOUTL ODUOK MOOTU U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   -  -  C  C  -  -
04 2-0   D  D  -  D  D  D
05 2-0   E  E  -  E  -  E
06 2-0   F  F  F  F  -  -
07 2-0   -  -  -  -  -  G
08 2-0   H  -  -  -  H  -
09 2-0   I  -  I  I  -  I
10 2-0   -  -  J  -  J  -
11 2-0   -  -  -  K  -  -
12 0-3   -  -  L  -  L  -
13 0-3   -  M  M  -  M  -
14 0-3   N  -  -  -  N  N
15 0-3   O  O  -  -  O  O
16 0-4   -  -  P  -  -  P
17 0-5   Q  -  Q  Q  -  Q
18 0-5   -  R  R  R  -   
19 0-5   S  -  S  S  S   
20 0-5   -  T  -  -      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-6   X  -            
25 2-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PHKZK WRWIF VZTFN ZMGHN KOOPB T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-DEC-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  A
02 2-0   B  B  -  -  -  -
03 2-0   -  -  C  C  C  C
04 2-0   D  D  -  D  -  D
05 2-0   E  E  -  -  -  E
06 2-0   F  F  -  F  F  -
07 0-3   -  -  G  G  -  -
08 0-3   H  H  -  H  -  H
09 0-3   -  -  I  I  I  -
10 0-3   -  -  J  J  -  -
11 0-3   -  K  K  K  K  -
12 0-3   L  L  L  -  -  -
13 0-4   -  M  -  -  -  M
14 0-4   N  -  N  -  N  N
15 0-4   -  O  O  -  O  -
16 1-3   P  -  -  -  -  P
17 1-4   Q  Q  Q  Q  Q  -
18 1-4   R  -  -  -  -   
19 1-5   S  -  S  -  S   
20 2-3   -  T  -  T      
21 2-4   -  U  U  -      
22 2-4   -  -  V         
23 2-4   W  X  -         
24 2-4   -  -            
25 3-4   Y  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WMPNL VKUUE NUAAU UALAT KUPTN S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
