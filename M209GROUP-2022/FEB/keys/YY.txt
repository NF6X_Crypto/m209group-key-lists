EFFECTIVE PERIOD:
25-FEB-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  -  -
02 0-3   B  B  B  -  B  B
03 0-3   C  C  -  C  C  C
04 0-4   -  -  D  -  -  -
05 0-4   -  E  E  E  E  -
06 0-4   F  -  F  F  -  -
07 0-4   G  G  -  G  G  G
08 0-4   H  -  -  -  H  -
09 0-4   -  -  -  -  I  -
10 0-4   -  J  -  J  -  -
11 0-5   K  -  K  -  -  K
12 0-5   -  -  -  -  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  -  N  -  N  N
15 0-5   -  -  O  O  -  O
16 1-3   P  P  P  P  -  -
17 1-3   -  Q  -  -  -  -
18 1-4   -  R  R  -  R   
19 1-6   -  -  -  S  -   
20 2-6   -  T  -  -      
21 3-4   U  U  -  U      
22 3-5   -  V  V         
23 3-5   -  -  X         
24 3-5   -  -            
25 3-5   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NMZMM ETAVS OUOVY ZNFOZ MPZVT S
-------------------------------
