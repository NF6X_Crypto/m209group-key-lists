EFFECTIVE PERIOD:
16-FEB-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 0-3   -  -  B  B  B  -
03 0-3   C  C  -  -  -  -
04 0-3   D  -  -  D  -  -
05 0-3   E  -  E  E  E  E
06 0-3   -  F  F  -  -  -
07 0-3   -  -  G  G  G  -
08 0-4   H  -  H  H  -  -
09 0-4   I  I  I  -  -  I
10 0-4   -  J  -  J  -  J
11 0-6   K  K  K  K  K  K
12 0-6   -  L  -  L  L  -
13 0-6   M  M  M  M  -  M
14 0-6   N  -  -  -  N  -
15 0-6   -  -  O  O  O  O
16 1-3   -  P  -  -  P  -
17 1-4   Q  Q  -  -  Q  Q
18 1-4   R  R  R  R  R   
19 1-4   -  -  -  -  -   
20 1-5   -  T  -  T      
21 2-5   U  U  U  -      
22 3-6   V  -  -         
23 4-5   -  -  X         
24 4-6   X  -            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SNUCT ORXFK NWMOO ZYQVU TUKUX C
-------------------------------
