EFFECTIVE PERIOD:
15-FEB-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  -  -  -  B  -
03 2-0   C  C  C  C  C  -
04 2-0   D  D  D  D  -  -
05 2-0   -  E  -  -  E  E
06 2-0   -  -  -  -  -  -
07 2-0   -  -  -  G  -  -
08 0-3   H  -  -  H  H  -
09 0-5   I  I  I  I  I  I
10 0-5   -  J  J  -  -  -
11 0-5   K  -  K  K  -  -
12 0-5   L  -  -  L  -  L
13 0-5   M  -  M  -  -  M
14 0-5   -  -  N  N  N  N
15 0-5   O  O  -  -  -  O
16 0-5   P  P  P  -  P  -
17 0-5   -  Q  -  Q  -  Q
18 0-5   R  R  R  R  -   
19 0-5   -  S  S  S  S   
20 0-6   T  T  -  -      
21 0-6   -  -  -  -      
22 1-2   -  V  V         
23 1-6   W  -  X         
24 2-5   -  Y            
25 2-5   -  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

NMALA KEYPV VETNK XNAIP NTGVD N
-------------------------------
