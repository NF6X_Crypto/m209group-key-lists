EFFECTIVE PERIOD:
15-MAY-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   -  B  -  -  B  B
03 2-0   C  -  C  C  -  -
04 2-0   -  -  -  D  D  D
05 2-0   -  -  -  -  -  -
06 0-4   F  F  -  F  -  -
07 0-4   G  G  -  G  G  -
08 0-4   -  -  H  H  H  H
09 0-4   I  -  -  -  I  -
10 0-4   -  -  J  -  J  J
11 0-5   K  K  K  K  -  -
12 0-6   -  -  L  L  L  L
13 0-6   -  M  -  -  -  -
14 0-6   -  N  N  -  -  N
15 0-6   -  -  O  -  -  -
16 1-3   P  -  P  -  P  -
17 1-5   Q  -  -  Q  -  Q
18 1-6   R  R  R  -  R   
19 1-6   S  S  -  S  S   
20 2-4   T  -  T  -      
21 2-5   -  -  -  -      
22 4-6   V  V  V         
23 4-6   W  -  -         
24 4-6   X  -            
25 4-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HNDUW ZVVPO NWQDU OANLR SKZVT V
-------------------------------
