EFFECTIVE PERIOD:
20-MAY-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  -
02 2-0   -  B  B  B  B  -
03 2-0   C  C  -  C  -  -
04 2-0   D  -  D  -  D  D
05 0-3   E  E  E  E  E  -
06 0-3   -  -  F  F  -  F
07 0-3   G  -  G  G  G  G
08 0-3   -  H  -  H  H  H
09 0-4   I  I  I  -  I  -
10 0-4   -  -  -  -  J  -
11 0-4   -  -  K  K  K  -
12 0-6   L  L  -  L  -  L
13 0-6   -  M  -  M  M  M
14 0-6   -  N  N  -  -  N
15 0-6   O  -  O  -  -  -
16 0-6   -  P  -  -  -  P
17 0-6   -  Q  -  -  -  Q
18 1-4   R  -  -  R  R   
19 2-4   -  -  -  -  S   
20 2-4   T  T  T  -      
21 2-4   U  -  -  -      
22 2-4   V  V  -         
23 2-6   -  -  -         
24 3-4   -  -            
25 3-5   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PWQHA VPKZB WPRAW UPCWV WJQVP C
-------------------------------
