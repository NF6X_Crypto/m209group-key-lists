EFFECTIVE PERIOD:
11-MAY-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 2-0   B  -  -  B  -  -
03 2-0   C  C  C  -  C  C
04 2-0   -  -  D  -  -  D
05 2-0   E  E  -  -  E  -
06 2-0   -  F  F  -  -  -
07 2-0   G  -  G  G  -  -
08 0-3   -  -  H  -  -  -
09 0-4   I  -  I  I  I  I
10 0-4   -  J  J  -  J  -
11 0-4   -  K  -  -  K  K
12 0-4   L  -  -  -  L  L
13 0-4   -  -  -  M  -  M
14 0-4   N  -  -  N  N  N
15 0-4   O  O  O  O  O  O
16 0-4   P  P  P  -  -  P
17 0-4   Q  Q  Q  Q  -  -
18 0-4   -  R  -  R  R   
19 0-4   -  S  -  -  -   
20 0-4   T  T  -  T      
21 0-5   U  -  U  -      
22 0-6   V  V  -         
23 0-6   -  X  -         
24 1-5   -  -            
25 2-4   Y  -            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZZZNY YDXHF TJEKZ ZLUGH KYTMG Y
-------------------------------
