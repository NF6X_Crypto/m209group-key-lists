EFFECTIVE PERIOD:
12-JAN-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  B  B  B  B  B
03 1-0   C  C  C  C  C  C
04 1-0   D  -  -  -  D  D
05 1-0   -  E  E  E  -  E
06 1-0   -  F  -  -  F  -
07 1-0   -  G  G  -  -  G
08 2-0   -  H  H  -  -  -
09 2-0   I  I  -  -  -  -
10 2-0   J  -  -  J  J  -
11 2-0   -  -  K  K  K  K
12 0-4   -  L  -  -  -  L
13 0-4   -  -  -  M  M  M
14 0-4   N  -  N  -  -  N
15 0-4   -  O  -  O  -  O
16 1-2   P  -  -  P  -  -
17 1-4   Q  -  Q  Q  -  -
18 1-4   R  R  R  -  R   
19 1-4   S  -  -  S  S   
20 1-4   -  -  T  -      
21 2-4   U  U  -  U      
22 2-4   V  V  -         
23 2-4   W  -  X         
24 2-4   -  Y            
25 2-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

AOWAO LAVOL TAVAM JKLTA OOJNU T
-------------------------------
