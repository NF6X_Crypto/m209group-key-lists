EFFECTIVE PERIOD:
25-JAN-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 0-3   B  -  -  B  -  -
03 0-3   -  C  C  C  -  C
04 0-3   -  -  D  -  D  -
05 0-3   E  E  E  -  E  E
06 0-3   -  F  -  F  F  F
07 0-3   G  G  G  G  -  -
08 0-3   H  H  H  -  H  H
09 0-3   I  I  -  -  I  I
10 0-3   J  -  J  J  J  J
11 0-3   -  -  -  K  -  K
12 0-3   -  L  L  L  -  -
13 0-5   M  -  -  M  -  M
14 0-5   N  N  N  N  N  -
15 0-5   O  -  O  -  -  -
16 0-5   -  -  -  -  P  P
17 0-5   -  Q  Q  -  -  -
18 0-6   -  -  -  -  R   
19 0-6   S  S  -  S  S   
20 0-6   -  -  T  T      
21 0-6   -  -  U  U      
22 0-6   V  -  V         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   -  -            
26 1-4   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

TLOMG HRZSH APZRT PZACV BRJRV G
-------------------------------
