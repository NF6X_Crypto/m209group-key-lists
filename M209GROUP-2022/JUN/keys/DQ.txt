EFFECTIVE PERIOD:
27-JUN-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  B  -  -  B  B
03 0-3   -  -  C  C  C  -
04 0-4   D  D  D  D  -  -
05 0-4   -  -  E  -  E  -
06 0-4   -  -  F  -  F  -
07 0-4   G  -  G  -  -  G
08 0-5   -  H  -  H  H  H
09 0-5   I  I  -  I  -  I
10 0-5   J  J  J  J  J  J
11 0-5   -  -  K  -  -  -
12 0-5   -  -  -  -  -  L
13 0-5   M  M  -  -  M  M
14 0-5   N  -  -  N  -  -
15 0-5   -  O  O  -  -  O
16 0-6   -  P  P  -  P  P
17 0-6   -  -  Q  -  -  -
18 0-6   R  -  R  -  R   
19 0-6   S  S  -  S  -   
20 1-3   T  T  T  T      
21 1-6   U  U  -  U      
22 2-3   V  -  -         
23 4-5   -  -  -         
24 4-5   X  -            
25 4-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AYKFW JWIYP PMTUK UQUOK RFNQF U
-------------------------------
