EFFECTIVE PERIOD:
22-JUN-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 0-3   B  -  -  -  -  -
03 0-3   C  C  -  -  -  -
04 0-3   -  -  D  -  D  D
05 0-3   E  E  E  E  E  E
06 0-3   -  F  -  -  -  F
07 0-3   G  -  -  G  G  -
08 0-3   -  -  H  -  -  -
09 0-3   I  -  -  -  I  -
10 0-3   -  -  J  J  -  J
11 0-4   -  K  -  -  K  -
12 0-4   L  L  L  -  -  L
13 0-4   -  M  -  M  -  M
14 0-5   N  -  N  -  N  N
15 0-5   -  -  O  O  O  -
16 0-5   -  P  P  P  P  P
17 0-5   Q  -  -  -  -  -
18 0-6   R  R  -  R  -   
19 0-6   S  S  -  S  -   
20 0-6   T  -  -  T      
21 0-6   -  U  U  -      
22 0-6   V  -  V         
23 0-6   -  -  -         
24 2-4   X  Y            
25 3-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OSARH MFVKB RMNPP FNKQR HTOTW J
-------------------------------
