EFFECTIVE PERIOD:
02-JUN-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  B  -  -  B
03 1-0   C  -  -  -  C  -
04 1-0   -  -  -  D  D  D
05 1-0   E  E  -  E  E  E
06 0-3   -  F  -  -  -  -
07 0-3   -  G  G  G  -  G
08 0-3   -  -  H  H  H  -
09 0-3   I  I  -  -  I  -
10 0-3   -  J  J  -  -  -
11 0-6   -  K  -  -  -  -
12 0-6   -  L  -  L  -  -
13 0-6   M  -  M  -  M  M
14 0-6   -  N  N  N  N  N
15 0-6   O  O  O  O  O  -
16 1-3   -  -  P  P  P  -
17 1-3   -  Q  Q  Q  -  Q
18 1-3   -  R  -  R  -   
19 1-3   S  -  -  -  -   
20 1-4   T  -  T  T      
21 1-5   -  -  -  U      
22 1-5   V  -  V         
23 1-6   W  X  X         
24 2-4   -  -            
25 3-6   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KPVAP CYVVJ UAFVZ PPKZZ MEPAT A
-------------------------------
