EFFECTIVE PERIOD:
17-NOV-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  B  B  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  -  D  -  -  D
05 1-0   -  -  -  -  -  E
06 0-3   F  F  F  -  F  F
07 0-4   G  G  -  -  -  -
08 0-4   -  -  -  H  -  -
09 0-4   I  -  -  I  -  I
10 0-4   -  -  -  J  J  -
11 0-4   K  -  K  K  -  K
12 0-5   L  L  -  -  L  -
13 0-5   M  -  M  -  M  M
14 0-5   -  N  N  N  -  N
15 0-5   O  O  O  -  -  O
16 0-5   P  -  P  -  -  P
17 0-5   Q  Q  -  Q  Q  -
18 1-2   -  R  -  -  R   
19 1-3   S  -  -  -  -   
20 1-5   T  -  -  T      
21 1-5   U  U  -  U      
22 1-5   -  V  V         
23 1-5   -  X  -         
24 1-6   -  Y            
25 3-4   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VAFMT TLHUH ZFOVZ ESZGZ PMUOP A
-------------------------------
