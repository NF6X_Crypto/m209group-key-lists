EFFECTIVE PERIOD:
05-NOV-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  -  B  B  B  -
03 1-0   -  -  -  -  C  C
04 1-0   -  D  D  D  D  -
05 1-0   -  E  -  -  E  -
06 1-0   -  -  -  -  -  -
07 1-0   G  G  G  -  G  -
08 0-3   H  -  -  -  -  -
09 0-5   -  I  I  I  -  -
10 0-5   -  -  J  J  -  J
11 0-6   K  -  -  -  K  K
12 0-6   L  -  -  -  -  -
13 0-6   -  M  M  M  M  M
14 0-6   N  N  -  N  -  N
15 0-6   O  -  -  O  O  O
16 1-3   P  P  -  P  P  P
17 1-6   Q  Q  -  Q  Q  -
18 2-4   R  -  R  -  -   
19 3-4   -  S  -  -  S   
20 3-5   -  T  T  -      
21 3-5   -  -  -  U      
22 4-5   -  -  V         
23 4-5   -  -  X         
24 5-6   X  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VUVRT QEPPT VMLEK AOVVP PVALZ M
-------------------------------
