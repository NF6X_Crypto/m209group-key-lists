EFFECTIVE PERIOD:
02-NOV-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  -  B  B  B  B
03 1-0   C  -  -  C  -  -
04 1-0   -  -  D  -  D  -
05 1-0   -  E  E  E  E  -
06 1-0   F  -  -  -  -  F
07 1-0   G  G  -  G  -  G
08 2-0   -  H  H  -  -  -
09 2-0   I  I  -  -  I  I
10 2-0   -  J  -  -  -  -
11 0-4   K  -  K  -  -  K
12 0-4   L  L  L  L  L  -
13 0-4   -  -  -  -  -  M
14 0-4   -  -  -  -  N  -
15 0-5   -  O  O  O  -  O
16 1-2   -  -  -  P  P  P
17 1-4   Q  -  Q  Q  -  Q
18 2-3   -  R  -  R  R   
19 2-5   S  S  -  -  S   
20 2-5   T  T  T  -      
21 2-5   -  -  -  -      
22 3-5   V  V  V         
23 4-5   -  -  -         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OKOYA RTOKS QTJTB RSRHW RTQOK T
-------------------------------
