EFFECTIVE PERIOD:
10-NOV-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  -
02 2-0   -  -  B  B  -  B
03 2-0   -  -  C  C  -  C
04 2-0   D  D  D  -  -  -
05 2-0   -  E  -  E  E  E
06 0-3   F  -  F  -  F  F
07 0-3   G  -  G  -  G  G
08 0-3   -  H  -  -  H  H
09 0-3   -  -  I  I  -  -
10 0-3   J  J  -  J  -  J
11 0-3   -  -  K  -  K  -
12 0-3   -  -  -  -  L  -
13 0-5   -  M  M  -  -  M
14 0-5   N  N  -  N  N  -
15 0-5   O  O  -  -  -  O
16 1-2   P  P  P  -  -  -
17 1-5   -  Q  -  -  -  Q
18 1-5   R  R  R  R  -   
19 1-5   -  -  -  S  -   
20 1-6   T  T  -  -      
21 2-3   -  U  -  U      
22 3-5   -  V  -         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-5   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

THAGR HJGNL WOUUT ZRFZM VMMIK W
-------------------------------
