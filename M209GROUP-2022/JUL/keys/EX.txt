EFFECTIVE PERIOD:
30-JUL-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 2-0   B  -  -  -  -  -
03 2-0   -  -  -  -  C  C
04 2-0   D  D  D  -  -  D
05 2-0   E  E  E  E  E  -
06 2-0   -  F  -  -  F  -
07 2-0   G  -  -  G  G  -
08 2-0   H  -  H  -  H  H
09 0-3   I  I  I  I  -  -
10 0-4   -  -  J  -  J  -
11 0-4   -  -  K  K  -  -
12 0-4   -  L  L  -  -  L
13 0-4   M  -  -  -  M  -
14 0-4   -  N  N  -  -  -
15 0-5   -  -  -  O  -  O
16 0-6   P  P  P  -  -  -
17 0-6   -  -  -  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   -  -  -  -  -   
20 0-6   -  T  -  T      
21 0-6   -  U  -  U      
22 1-4   -  -  -         
23 1-5   -  -  X         
24 2-6   X  Y            
25 2-6   Y  Z            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

DSJZP SSPIZ SJDJV TJKNT CKJSS J
-------------------------------
