EFFECTIVE PERIOD:
18-JUL-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  -
02 2-0   B  B  -  B  -  -
03 2-0   C  -  -  C  C  -
04 2-0   D  -  D  D  D  -
05 0-3   -  E  -  -  E  -
06 0-4   -  F  -  -  F  F
07 0-4   G  G  G  G  -  -
08 0-4   -  -  -  -  -  H
09 0-5   I  I  -  -  I  I
10 0-5   J  -  -  -  J  J
11 0-5   -  K  K  K  -  -
12 0-5   L  -  L  -  -  L
13 0-5   -  -  -  -  -  M
14 0-5   N  -  -  N  N  -
15 0-5   O  O  O  O  O  -
16 1-3   P  P  P  -  P  P
17 1-4   -  Q  Q  -  Q  -
18 2-3   -  -  -  R  -   
19 2-3   -  S  S  -  S   
20 2-3   T  -  T  T      
21 2-3   -  -  -  -      
22 2-5   -  -  V         
23 3-4   -  -  -         
24 3-4   -  Y            
25 3-4   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KTASS WJPJI WPZTC RWSAS KWRPT P
-------------------------------
