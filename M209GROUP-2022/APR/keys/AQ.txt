EFFECTIVE PERIOD:
10-APR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   B  -  -  -  B  -
03 1-0   -  -  -  -  -  C
04 1-0   D  D  D  D  D  -
05 1-0   E  E  E  -  E  E
06 2-0   -  F  F  F  -  F
07 2-0   -  -  G  -  -  -
08 0-3   H  -  -  -  H  -
09 0-3   -  I  -  I  -  I
10 0-5   J  J  J  J  -  J
11 0-5   K  -  K  K  -  -
12 0-5   -  -  -  -  -  -
13 0-5   -  M  M  M  M  M
14 0-5   N  N  N  -  N  N
15 0-5   O  O  O  -  -  O
16 1-2   -  -  -  P  P  P
17 1-2   -  -  Q  Q  Q  -
18 1-2   -  -  -  R  -   
19 1-2   -  -  -  S  -   
20 1-5   -  -  -  -      
21 2-3   U  U  U  U      
22 2-3   -  V  V         
23 2-3   -  -  -         
24 2-4   X  -            
25 2-6   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ONJIK TKORT KJTGU AOAKO ATHVA Q
-------------------------------
