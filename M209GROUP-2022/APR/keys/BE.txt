EFFECTIVE PERIOD:
24-APR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  -  B  B  B  B
03 1-0   C  C  C  C  -  C
04 1-0   D  D  D  D  -  -
05 2-0   E  -  E  E  E  E
06 2-0   F  -  F  -  F  -
07 2-0   -  G  -  G  G  G
08 2-0   -  -  -  -  -  -
09 2-0   I  I  -  -  -  I
10 2-0   -  J  J  -  J  -
11 0-3   -  K  -  K  K  K
12 0-4   L  -  L  -  -  -
13 0-4   -  M  M  M  -  -
14 0-4   N  N  N  -  N  N
15 0-4   -  -  O  -  -  -
16 0-4   -  P  -  -  P  P
17 0-4   Q  Q  -  -  -  -
18 0-6   R  -  R  -  R   
19 0-6   -  -  S  -  S   
20 1-2   T  T  -  T      
21 1-4   U  -  U  -      
22 1-4   -  -  -         
23 1-4   W  -  -         
24 1-4   -  -            
25 2-6   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PRQDL TAUET SARAS UILAY KUNKT E
-------------------------------
