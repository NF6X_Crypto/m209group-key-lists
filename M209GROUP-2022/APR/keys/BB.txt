EFFECTIVE PERIOD:
21-APR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   -  -  B  B  -  -
03 2-0   -  C  -  -  -  C
04 2-0   -  -  D  D  D  D
05 2-0   -  -  E  E  E  E
06 0-3   -  F  F  -  -  -
07 0-3   G  -  G  G  -  -
08 0-3   H  -  -  -  -  H
09 0-3   I  -  -  I  I  I
10 0-4   J  J  J  -  J  J
11 0-5   -  K  -  K  -  -
12 0-5   -  L  -  -  L  -
13 0-5   -  M  M  -  M  -
14 0-5   -  N  -  N  -  N
15 0-6   O  -  O  O  -  -
16 0-6   -  -  P  P  -  P
17 0-6   Q  Q  Q  Q  Q  Q
18 1-4   R  -  R  R  R   
19 2-4   -  S  -  S  -   
20 2-4   -  -  -  -      
21 2-6   U  U  U  -      
22 2-6   V  -  -         
23 2-6   W  X  -         
24 2-6   -  Y            
25 3-5   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FOTRT RWNKO RSJTM RYJAO VKWRA O
-------------------------------
