EFFECTIVE PERIOD:
29-AUG-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  B  -  B  -  B
03 1-0   C  C  -  -  -  -
04 1-0   -  -  D  -  -  D
05 0-4   -  -  -  -  E  -
06 0-4   F  F  F  F  F  F
07 0-4   G  -  G  -  G  -
08 0-5   -  H  -  -  -  H
09 0-5   -  -  I  -  I  I
10 0-5   J  -  -  J  J  J
11 0-5   -  -  -  K  K  K
12 0-6   -  L  -  L  -  -
13 0-6   M  M  M  M  M  -
14 0-6   -  N  -  -  N  -
15 0-6   O  O  -  O  -  O
16 1-4   P  P  P  -  -  P
17 1-6   Q  -  -  Q  -  -
18 2-3   -  -  -  R  R   
19 2-4   S  S  S  -  S   
20 2-5   -  -  T  T      
21 2-5   -  U  U  -      
22 2-5   -  V  -         
23 4-5   W  X  X         
24 5-6   X  Y            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WTRFS PNASJ PZXTV JXVRF VVNON A
-------------------------------
