EFFECTIVE PERIOD:
09-AUG-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  -  -  B  -  -
03 1-0   C  -  -  C  C  -
04 1-0   D  -  D  D  D  -
05 1-0   -  E  E  -  -  -
06 2-0   -  F  F  -  -  F
07 2-0   -  G  G  -  G  -
08 2-0   H  -  -  -  H  -
09 2-0   I  -  I  -  -  I
10 0-3   J  J  -  J  -  -
11 0-3   -  -  K  K  K  K
12 0-3   L  L  L  -  L  L
13 0-3   M  M  M  M  -  -
14 0-3   -  N  -  N  N  N
15 0-3   O  O  O  O  O  -
16 1-2   -  -  -  P  -  P
17 1-2   Q  Q  Q  Q  Q  -
18 1-2   -  R  -  R  -   
19 1-2   -  S  -  -  S   
20 1-3   -  -  T  -      
21 2-4   U  -  U  U      
22 2-4   -  V  V         
23 2-4   -  -  -         
24 2-6   -  -            
25 3-4   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OWOVN QHVLJ OUGQR VTNWP AMATV T
-------------------------------
