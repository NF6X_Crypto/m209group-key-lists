EFFECTIVE PERIOD:
14-AUG-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  B  -  B  -  B
03 1-0   C  -  -  -  -  -
04 1-0   -  -  -  D  D  D
05 2-0   -  -  -  E  -  E
06 2-0   -  F  F  F  -  -
07 2-0   G  G  -  G  G  -
08 2-0   -  H  H  H  H  H
09 0-5   -  I  -  -  I  I
10 0-5   -  -  J  -  J  -
11 0-5   K  K  K  -  -  K
12 0-5   L  L  -  -  L  -
13 0-5   -  M  M  M  -  -
14 0-5   N  -  -  N  -  N
15 0-6   O  -  O  -  -  -
16 0-6   -  -  -  -  P  P
17 0-6   Q  -  -  Q  -  -
18 1-3   -  R  R  R  R   
19 1-5   -  S  S  S  -   
20 1-6   T  T  -  T      
21 2-5   U  -  -  -      
22 2-6   V  -  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 2-6   -  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RIZOX UOAAK PWQHR SQAIR KQUPH W
-------------------------------
