EFFECTIVE PERIOD:
07-AUG-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  B  -  -  -
03 2-0   -  -  C  -  C  C
04 2-0   D  D  D  D  -  -
05 2-0   -  -  E  -  -  E
06 2-0   F  -  -  F  F  -
07 2-0   G  -  -  -  G  G
08 2-0   H  H  -  -  H  H
09 2-0   -  I  -  I  -  I
10 2-0   J  -  J  -  J  -
11 2-0   -  K  -  -  K  -
12 2-0   L  -  L  L  L  -
13 2-0   -  -  M  M  -  M
14 2-0   -  -  -  -  N  N
15 0-3   O  -  -  O  -  O
16 0-3   P  P  P  -  P  P
17 0-3   Q  Q  Q  -  Q  -
18 0-3   R  -  R  -  -   
19 0-4   -  S  S  S  S   
20 0-5   -  T  -  T      
21 0-5   U  U  U  U      
22 0-5   V  -  -         
23 0-6   -  X  -         
24 1-5   X  -            
25 1-6   -  Z            
26 2-3   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

IQAHX ZRKRB YWGLA GDIUX FYKFE J
-------------------------------
