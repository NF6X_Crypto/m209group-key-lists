EFFECTIVE PERIOD:
02-SEP-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   -  B  -  -  -  -
03 2-0   C  C  C  -  -  -
04 2-0   -  -  D  -  D  -
05 2-0   E  E  E  -  -  -
06 2-0   -  -  F  F  F  F
07 0-3   -  G  G  -  -  -
08 0-3   -  H  -  -  -  -
09 0-3   I  -  -  -  I  -
10 0-3   -  J  J  J  J  J
11 0-3   K  -  -  -  K  K
12 0-3   L  -  L  L  -  -
13 0-4   M  -  -  M  M  M
14 0-4   -  N  N  N  -  N
15 0-4   O  O  -  O  O  O
16 1-4   P  P  -  P  -  P
17 2-3   -  Q  -  -  -  Q
18 2-4   R  -  R  R  R   
19 2-4   S  S  S  -  -   
20 2-6   T  -  T  -      
21 3-4   -  -  -  U      
22 3-4   V  -  V         
23 3-4   W  X  X         
24 3-4   X  Y            
25 4-6   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AUUNX UPAAZ KAWVS UOMUP UUNUT L
-------------------------------
