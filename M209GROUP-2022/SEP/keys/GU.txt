EFFECTIVE PERIOD:
17-SEP-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  -  -  -  -  -
03 1-0   -  C  -  -  -  -
04 0-3   -  D  D  D  -  -
05 0-4   -  -  E  E  -  -
06 0-4   F  F  F  -  -  F
07 0-4   G  -  -  -  G  G
08 0-4   H  -  -  H  H  H
09 0-4   -  I  I  I  I  I
10 0-4   J  -  -  J  J  J
11 0-4   K  -  -  -  -  K
12 0-4   -  L  L  L  -  -
13 0-5   M  M  M  -  -  -
14 0-5   N  N  -  -  N  N
15 0-6   -  O  O  O  -  -
16 0-6   P  P  -  -  P  P
17 0-6   -  -  Q  -  Q  -
18 0-6   R  -  R  -  R   
19 0-6   -  S  S  S  S   
20 1-4   T  T  T  T      
21 1-4   U  -  U  U      
22 1-4   -  V  V         
23 1-4   -  X  -         
24 1-6   -  -            
25 2-4   Y  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OCUIN SRRRH IAWVL KZZSA WMJUN E
-------------------------------
