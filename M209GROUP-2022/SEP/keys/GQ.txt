EFFECTIVE PERIOD:
13-SEP-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  B  B  -  B  B
03 1-0   C  C  -  C  C  -
04 1-0   D  -  D  D  D  -
05 1-0   -  -  -  E  E  E
06 1-0   -  -  -  -  F  -
07 1-0   G  -  G  G  -  -
08 1-0   -  H  H  H  -  H
09 1-0   I  I  -  I  -  I
10 1-0   J  J  J  J  J  -
11 1-0   K  K  K  -  K  K
12 1-0   L  -  L  L  -  -
13 2-0   M  M  -  -  -  -
14 2-0   -  N  -  N  -  -
15 2-0   -  O  -  -  -  O
16 2-0   -  -  P  P  -  P
17 2-0   -  Q  -  -  -  -
18 2-0   R  R  R  -  R   
19 2-0   S  -  S  -  -   
20 0-3   T  T  -  T      
21 0-4   -  -  -  -      
22 0-4   -  -  -         
23 0-4   -  -  X         
24 0-5   -  -            
25 0-6   Y  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HDUWO JCDDP COMZM EFTMU YVQSK L
-------------------------------
