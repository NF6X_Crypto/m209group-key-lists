EFFECTIVE PERIOD:
13-MAR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  -  -  A  -
02 0-3   B  -  -  -  -  -
03 0-3   -  -  C  -  C  C
04 0-3   D  -  -  D  D  -
05 0-3   -  -  E  -  -  E
06 0-3   -  -  -  F  -  F
07 0-3   G  -  -  G  -  G
08 0-5   H  H  H  H  H  -
09 0-5   -  I  I  I  I  I
10 0-5   -  J  -  J  -  J
11 0-5   K  -  K  K  -  -
12 0-5   -  -  L  -  -  L
13 0-6   -  M  -  -  M  -
14 0-6   -  -  N  -  -  -
15 0-6   -  O  -  O  O  O
16 1-3   P  P  P  -  P  -
17 1-4   -  -  -  Q  Q  -
18 1-6   R  R  R  -  R   
19 1-6   S  S  S  S  S   
20 2-4   T  -  -  T      
21 3-5   U  U  -  -      
22 4-6   V  V  -         
23 4-6   -  -  X         
24 5-6   -  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MROOA UQNJT JTVKO SSOVD SUJVJ V
-------------------------------
