EFFECTIVE PERIOD:
24-MAR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  -  -  -  -
02 0-3   -  B  B  B  B  B
03 0-3   C  C  -  C  -  C
04 0-3   D  D  -  -  D  -
05 0-3   E  -  -  E  E  -
06 0-4   -  -  -  -  -  -
07 0-4   G  -  G  G  -  G
08 0-5   -  H  -  -  -  -
09 0-5   -  I  I  I  I  I
10 0-5   J  J  -  -  -  -
11 0-5   -  -  K  K  -  -
12 0-5   -  -  L  -  -  L
13 0-6   -  M  M  -  -  M
14 0-6   N  -  N  N  N  -
15 0-6   -  -  -  -  O  O
16 0-6   P  -  P  -  P  -
17 0-6   -  -  Q  -  -  -
18 1-3   R  -  -  -  R   
19 1-4   S  -  S  S  S   
20 2-3   T  T  -  T      
21 3-4   -  U  U  U      
22 3-6   V  -  V         
23 3-6   W  X  -         
24 3-6   -  -            
25 3-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MOYTR LMVVP EVHQC PSFRP MXVSB X
-------------------------------
