EFFECTIVE PERIOD:
23-MAR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  A
02 2-0   -  B  -  B  B  B
03 2-0   -  -  -  -  C  C
04 0-3   -  -  -  -  D  -
05 0-3   E  -  E  E  E  E
06 0-4   F  F  F  F  F  -
07 0-4   G  G  G  -  -  G
08 0-4   -  H  -  -  H  H
09 0-4   I  -  I  I  -  -
10 0-5   -  J  J  -  J  J
11 0-5   -  K  K  K  -  -
12 0-5   L  -  L  -  -  L
13 0-5   -  -  M  -  -  -
14 0-5   -  -  -  -  -  N
15 0-5   O  -  -  O  -  -
16 1-6   -  P  -  P  P  -
17 2-3   Q  -  -  -  -  -
18 2-3   R  R  R  R  R   
19 2-4   S  S  -  S  S   
20 2-4   -  -  -  T      
21 2-4   U  U  -  -      
22 2-4   -  -  V         
23 2-6   W  -  -         
24 2-6   -  Y            
25 3-5   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZMJRV JWTIY ALSTR NPXPP POGPM L
-------------------------------
