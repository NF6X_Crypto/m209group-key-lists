EFFECTIVE PERIOD:
02-MAR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  B  B  B  -  B
03 1-0   -  -  -  -  C  C
04 1-0   -  -  D  -  -  D
05 1-0   E  -  E  E  E  -
06 1-0   F  F  -  -  -  -
07 2-0   -  -  -  -  -  -
08 2-0   -  H  -  H  H  H
09 2-0   -  -  I  I  I  I
10 2-0   J  J  -  J  J  -
11 2-0   K  K  -  -  -  -
12 0-5   L  -  L  L  L  L
13 0-5   -  -  M  -  -  -
14 0-5   -  N  -  N  N  -
15 0-5   -  O  O  O  -  -
16 0-5   -  -  -  -  P  -
17 0-5   -  Q  -  Q  -  Q
18 1-3   R  R  R  R  -   
19 1-5   S  -  S  -  -   
20 2-3   T  -  T  -      
21 2-4   -  -  -  -      
22 2-5   -  -  -         
23 2-5   -  -  X         
24 2-5   X  Y            
25 2-5   -  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

SEATU DDZIN AJSDD SAMNK UAKNN M
-------------------------------
