EFFECTIVE PERIOD:
08-MAR-2022 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  -  -  -  -  B
03 2-0   C  -  -  C  -  C
04 2-0   D  D  -  -  -  -
05 0-3   E  E  -  E  E  -
06 0-4   F  -  F  F  F  F
07 0-5   G  -  G  G  -  G
08 0-5   -  H  H  H  -  -
09 0-5   -  I  -  -  I  -
10 0-5   -  -  J  -  -  J
11 0-5   -  K  -  K  -  K
12 0-5   L  L  L  L  L  -
13 0-5   -  -  -  -  M  -
14 0-5   N  N  N  -  -  -
15 0-5   -  -  -  O  -  -
16 0-5   -  P  -  -  P  P
17 0-6   Q  -  -  -  Q  -
18 0-6   -  R  R  R  R   
19 0-6   S  S  S  S  -   
20 0-6   T  -  T  T      
21 0-6   -  -  -  U      
22 0-6   V  -  -         
23 0-6   W  -  X         
24 1-2   X  Y            
25 1-6   -  -            
26 2-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TRBGS VRFNN DFSXH IYMDQ MEONR S
-------------------------------
