EFFECTIVE PERIOD:
13-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 2-0   B  B  -  B  B  B
03 2-0   -  C  -  -  -  -
04 2-0   -  D  -  D  D  D
05 2-0   E  E  -  -  E  E
06 2-0   F  -  F  F  -  F
07 2-0   -  -  -  G  G  -
08 0-3   -  -  H  -  -  -
09 0-3   -  I  I  I  I  -
10 0-3   J  J  -  -  -  -
11 0-3   K  -  -  -  K  K
12 0-3   -  L  -  L  -  L
13 0-3   M  M  M  -  M  -
14 0-3   N  -  N  N  N  -
15 0-3   O  O  O  -  O  O
16 0-3   -  P  P  P  -  P
17 0-5   -  -  -  -  -  Q
18 0-5   -  -  R  -  R   
19 0-5   -  S  S  -  -   
20 0-5   -  -  -  T      
21 0-6   -  -  U  -      
22 2-3   V  V  -         
23 2-3   W  -  X         
24 2-3   X  -            
25 2-5   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VZRGV OIUIT OQRJP UGOTJ QIZWN A
-------------------------------
