EFFECTIVE PERIOD:
11-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ES
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 2-0   -  B  -  B  B  B
03 2-0   C  C  -  -  -  C
04 2-0   -  -  -  -  D  -
05 2-0   -  E  E  E  E  -
06 0-3   F  F  F  -  F  F
07 0-3   G  -  G  G  G  -
08 0-3   -  H  -  H  H  H
09 0-3   -  -  -  -  I  -
10 0-3   -  J  -  J  -  J
11 0-3   K  K  -  -  K  -
12 0-3   -  L  L  L  L  -
13 0-6   -  M  -  -  -  -
14 0-6   -  N  N  -  -  -
15 0-6   O  -  O  -  -  O
16 0-6   P  -  -  -  -  -
17 0-6   Q  -  Q  -  -  -
18 1-2   R  R  R  R  R   
19 1-3   -  S  S  -  -   
20 1-5   -  -  -  -      
21 2-5   -  -  -  U      
22 2-6   V  -  -         
23 2-6   W  -  X         
24 2-6   X  -            
25 2-6   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NLDSX JFNOU NNRNU UADLC KZJSX P
-------------------------------
