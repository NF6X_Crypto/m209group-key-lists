EFFECTIVE PERIOD:
09-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   -  -  -  B  -  -
03 1-0   -  C  -  -  -  C
04 1-0   -  -  -  D  -  D
05 2-0   E  E  E  E  -  -
06 2-0   -  F  F  F  F  F
07 2-0   -  G  G  -  -  G
08 2-0   H  H  H  H  -  H
09 2-0   I  -  I  -  I  I
10 2-0   J  -  J  J  J  -
11 0-3   -  -  -  K  K  K
12 0-3   -  -  L  -  -  -
13 0-3   M  M  M  M  -  -
14 0-3   -  N  -  N  N  -
15 0-3   O  -  -  -  O  O
16 0-3   P  P  P  -  -  -
17 0-4   Q  Q  -  Q  Q  Q
18 1-2   -  -  -  -  R   
19 1-2   -  S  S  -  -   
20 1-2   -  -  -  -      
21 1-2   U  U  U  U      
22 1-3   -  V  -         
23 2-4   -  -  -         
24 2-5   -  Y            
25 2-6   Y  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NUVBP RNUDU NMEPL YWVAS IMLRN T
-------------------------------
