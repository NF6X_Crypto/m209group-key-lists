EFFECTIVE PERIOD:
29-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  A
02 2-0   -  -  -  B  -  -
03 2-0   -  -  -  -  C  -
04 2-0   D  D  D  D  D  D
05 0-3   E  E  -  -  E  -
06 0-3   -  -  -  F  -  F
07 0-3   G  G  G  -  -  G
08 0-4   H  -  -  H  -  -
09 0-5   I  I  I  -  I  I
10 0-5   J  J  J  J  J  J
11 0-5   -  -  -  K  K  -
12 0-5   -  -  L  -  -  -
13 0-5   M  -  -  M  -  M
14 0-6   -  -  N  N  N  N
15 0-6   O  -  -  -  -  -
16 0-6   P  -  P  P  -  P
17 0-6   Q  Q  Q  -  -  Q
18 0-6   -  -  R  -  R   
19 0-6   S  S  -  S  -   
20 1-6   T  T  T  T      
21 2-3   -  U  U  U      
22 2-5   V  V  V         
23 3-4   W  X  X         
24 5-6   -  Y            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GCZQK QAVTJ SOSPI QWPQV QPMPM P
-------------------------------
