EFFECTIVE PERIOD:
16-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  -  B  -  B  B
03 1-0   C  -  C  -  C  -
04 1-0   D  D  D  D  D  D
05 1-0   -  -  E  -  -  E
06 1-0   F  -  -  -  F  F
07 0-3   G  G  -  G  G  G
08 0-3   H  H  -  -  -  -
09 0-3   -  I  -  I  I  -
10 0-3   J  J  J  -  J  J
11 0-3   -  K  K  -  K  K
12 0-4   L  -  L  -  -  -
13 0-4   -  -  -  M  -  -
14 0-6   N  -  -  N  N  -
15 0-6   -  -  -  O  -  -
16 0-6   -  P  -  -  -  -
17 0-6   Q  Q  -  Q  -  Q
18 1-2   R  R  R  R  -   
19 1-2   S  S  S  S  -   
20 1-4   T  -  -  T      
21 1-4   U  U  U  -      
22 1-4   -  -  -         
23 1-4   -  -  X         
24 2-5   X  -            
25 2-6   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MQQNW UPRRN YMRYU GSVNK UQOMR M
-------------------------------
