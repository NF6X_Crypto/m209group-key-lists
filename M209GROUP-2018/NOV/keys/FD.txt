EFFECTIVE PERIOD:
22-NOV-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  -  B  -
03 1-0   -  -  C  C  -  -
04 1-0   D  -  D  -  D  -
05 0-3   E  E  E  E  -  -
06 0-3   -  -  -  -  -  F
07 0-3   -  G  G  G  G  G
08 0-3   -  -  -  H  H  H
09 0-3   I  -  -  I  I  -
10 0-3   -  J  J  -  J  -
11 0-4   K  -  -  K  -  -
12 0-5   L  L  -  L  -  L
13 0-5   -  M  -  M  -  M
14 0-5   -  -  N  N  N  -
15 0-5   -  -  O  -  -  -
16 0-5   P  -  -  -  P  P
17 0-5   -  -  -  Q  -  -
18 0-5   R  -  R  -  -   
19 0-5   -  S  S  -  -   
20 0-5   -  T  T  -      
21 0-6   -  -  U  -      
22 1-3   V  V  -         
23 1-6   W  X  -         
24 2-6   -  -            
25 3-5   Y  Z            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

PNTZU MOQMR PYMNZ TTNCR BSITM A
-------------------------------
