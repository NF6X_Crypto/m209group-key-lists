EFFECTIVE PERIOD:
10-APR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 2-0   B  -  B  B  -  -
03 2-0   -  C  -  C  -  C
04 2-0   D  -  -  -  D  D
05 2-0   -  E  E  E  E  -
06 0-5   -  -  -  -  -  F
07 0-5   G  -  G  G  G  G
08 0-5   H  H  H  H  H  H
09 0-5   -  -  -  -  I  -
10 0-6   J  J  -  -  -  -
11 0-6   -  -  -  -  K  -
12 0-6   -  -  L  -  L  -
13 0-6   -  -  M  -  -  M
14 0-6   -  -  N  N  -  N
15 0-6   -  O  O  -  O  -
16 1-2   -  P  -  P  -  -
17 1-2   Q  Q  Q  -  Q  -
18 1-2   R  R  -  R  -   
19 1-3   -  -  -  -  -   
20 1-6   T  -  -  -      
21 2-3   U  U  U  U      
22 2-4   -  -  -         
23 2-5   -  -  X         
24 2-5   X  Y            
25 2-5   Y  Z            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SPWZF PMJPW VQUUV RLGUP WBLBS M
-------------------------------
