EFFECTIVE PERIOD:
17-APR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   B  -  -  B  -  B
03 1-0   C  C  C  C  C  -
04 1-0   D  D  -  D  D  D
05 1-0   E  -  E  -  -  E
06 1-0   -  F  -  -  F  -
07 1-0   G  -  G  -  G  -
08 1-0   -  -  H  -  H  -
09 2-0   I  -  I  I  I  -
10 2-0   J  -  J  -  -  J
11 2-0   K  K  -  K  -  K
12 2-0   -  -  L  -  -  L
13 2-0   M  -  -  -  -  -
14 2-0   -  N  N  -  N  -
15 2-0   -  O  -  -  O  O
16 0-3   P  P  -  -  -  P
17 0-3   -  Q  -  Q  -  Q
18 0-4   -  R  -  R  R   
19 0-4   S  S  -  S  -   
20 0-4   -  -  T  T      
21 0-4   U  U  -  -      
22 0-4   -  -  -         
23 0-6   W  -  X         
24 2-4   -  -            
25 3-4   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VKIQQ RSOCR QYRQU INTFR HVCNH Q
-------------------------------
