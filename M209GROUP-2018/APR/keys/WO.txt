EFFECTIVE PERIOD:
13-APR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   B  -  -  -  B  B
03 1-0   -  C  -  -  C  -
04 2-0   -  D  D  -  -  D
05 2-0   -  -  E  -  -  E
06 2-0   -  F  F  F  -  F
07 2-0   -  -  -  G  -  G
08 2-0   H  -  H  -  -  H
09 0-3   -  -  I  -  I  -
10 0-4   -  J  -  -  -  -
11 0-4   K  K  -  -  K  -
12 0-4   L  -  L  L  -  -
13 0-4   M  -  -  M  M  M
14 0-4   N  -  N  N  N  N
15 0-4   -  O  -  O  O  -
16 0-4   -  -  P  P  P  P
17 0-4   Q  Q  Q  Q  -  -
18 0-5   -  -  R  -  R   
19 0-5   S  -  -  S  -   
20 0-5   -  T  -  T      
21 0-5   U  U  -  -      
22 0-5   -  -  V         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-6   Y  Z            
26 1-2   -               
27 1-3                   
-------------------------------
26 LETTER CHECK

PTXVQ KUNLQ PFFIQ JMBMA VZUUN X
-------------------------------
