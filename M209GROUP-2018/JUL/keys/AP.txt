EFFECTIVE PERIOD:
27-JUL-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  B  B  -  B
03 2-0   -  C  C  C  C  -
04 2-0   -  -  D  D  D  D
05 2-0   -  E  -  E  E  -
06 2-0   F  F  -  F  -  -
07 2-0   -  -  -  -  G  G
08 0-3   -  -  H  H  -  H
09 0-3   -  -  -  I  -  -
10 0-3   J  J  -  -  -  J
11 0-3   K  K  -  K  K  K
12 0-3   L  L  -  -  -  -
13 0-3   -  M  -  -  M  -
14 0-3   N  N  N  -  N  -
15 0-3   -  O  -  -  -  -
16 0-4   -  -  P  P  -  -
17 0-4   Q  -  -  Q  -  Q
18 0-4   -  R  R  -  R   
19 0-4   S  S  S  S  -   
20 0-4   T  -  T  T      
21 0-4   U  -  U  -      
22 0-4   -  -  -         
23 0-4   W  -  -         
24 0-4   -  Y            
25 0-6   Y  -            
26 1-2   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

ODIRZ LSVAI SZRXK FSRAI IIIOS Y
-------------------------------
