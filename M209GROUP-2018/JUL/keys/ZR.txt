EFFECTIVE PERIOD:
03-JUL-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  B  -  B  -  -
03 1-0   C  -  C  -  C  -
04 1-0   -  D  -  -  -  -
05 1-0   E  -  E  E  E  E
06 1-0   F  F  F  F  -  F
07 2-0   -  -  G  G  G  G
08 2-0   H  -  -  -  H  H
09 2-0   -  -  -  I  -  I
10 2-0   -  -  J  -  -  J
11 2-0   K  -  K  K  K  -
12 2-0   -  -  -  L  L  L
13 2-0   -  M  M  M  -  M
14 2-0   -  -  N  -  N  -
15 2-0   -  O  -  -  -  O
16 2-0   -  -  P  P  P  -
17 0-3   Q  Q  -  -  -  Q
18 0-5   -  -  R  R  R   
19 0-5   -  -  -  -  S   
20 0-5   T  T  -  T      
21 0-5   -  U  -  U      
22 0-5   -  -  -         
23 0-6   W  X  X         
24 1-2   X  Y            
25 1-5   Y  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TIUBQ MSOTT OAAKU QUBOJ CPBUB T
-------------------------------
