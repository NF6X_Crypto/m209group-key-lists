EFFECTIVE PERIOD:
11-MAR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 2-0   -  B  B  B  -  -
03 2-0   C  C  C  C  C  -
04 2-0   -  -  D  D  D  -
05 2-0   -  E  E  E  E  E
06 0-3   F  -  F  -  -  F
07 0-3   -  -  -  G  G  -
08 0-3   -  -  H  -  -  -
09 0-3   -  -  -  -  I  -
10 0-3   -  J  -  J  -  J
11 0-4   -  -  -  K  K  K
12 0-4   L  -  L  -  L  -
13 0-4   -  M  -  -  -  M
14 0-4   N  N  N  N  N  -
15 0-4   O  O  -  O  O  O
16 0-5   -  -  P  P  -  P
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  -  -  -  -   
19 0-5   -  S  S  -  -   
20 1-5   T  -  -  T      
21 2-3   U  U  U  -      
22 2-3   V  V  -         
23 2-3   -  X  X         
24 2-3   X  Y            
25 2-4   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UIZVR QLUKQ MZZWP UHJLM IPNUV W
-------------------------------
