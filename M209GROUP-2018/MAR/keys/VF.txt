EFFECTIVE PERIOD:
09-MAR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  -  -  -  -  -
03 1-0   C  -  -  C  -  C
04 1-0   D  D  -  D  -  D
05 2-0   E  -  -  -  -  E
06 2-0   F  F  F  -  F  F
07 2-0   -  G  -  -  -  G
08 2-0   -  -  H  H  H  -
09 2-0   -  I  -  I  -  -
10 0-3   J  J  -  J  J  -
11 0-3   -  K  K  -  -  K
12 0-3   L  L  L  L  -  L
13 0-4   -  M  M  M  -  -
14 0-4   N  -  -  -  N  -
15 0-4   O  -  O  -  O  -
16 0-4   -  P  P  P  P  -
17 0-4   Q  Q  -  -  -  -
18 0-4   R  R  -  R  R   
19 0-4   -  -  -  -  S   
20 1-2   -  -  T  T      
21 1-3   -  -  U  U      
22 2-4   -  -  V         
23 2-4   W  X  -         
24 2-4   -  -            
25 2-4   -  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LTSSA LEVWL PLRHO JWWLJ AWSKV J
-------------------------------
