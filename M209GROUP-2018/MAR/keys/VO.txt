EFFECTIVE PERIOD:
18-MAR-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 0-4   -  B  B  -  B  B
03 0-4   -  -  -  -  -  -
04 0-4   D  D  D  D  D  -
05 0-4   E  E  -  -  -  -
06 0-5   F  F  -  -  -  F
07 0-5   -  -  -  -  G  G
08 0-5   -  -  H  -  -  -
09 0-5   I  -  -  I  I  -
10 0-6   J  -  J  -  J  J
11 0-6   -  -  -  K  -  -
12 0-6   L  -  -  L  L  -
13 0-6   M  M  M  M  M  -
14 0-6   N  -  N  N  -  -
15 0-6   O  -  -  -  O  -
16 1-3   -  -  -  -  P  P
17 1-4   -  -  Q  Q  Q  Q
18 1-4   -  R  R  -  -   
19 1-4   -  S  -  S  -   
20 1-6   T  -  -  -      
21 2-4   U  -  U  -      
22 3-4   -  V  V         
23 4-5   W  -  -         
24 4-5   -  Y            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SRZLU UUOJR AKUJP GVSZU UMAWU W
-------------------------------
