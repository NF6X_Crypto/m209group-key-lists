EFFECTIVE PERIOD:
16-FEB-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   -  -  B  -  B  -
03 1-0   C  -  -  C  -  -
04 1-0   D  D  D  -  D  D
05 1-0   -  -  -  -  E  -
06 1-0   F  F  -  F  F  F
07 2-0   G  G  G  G  G  -
08 2-0   H  -  -  -  H  H
09 2-0   -  I  I  -  -  I
10 2-0   J  -  -  J  -  -
11 0-3   K  K  K  -  K  -
12 0-4   L  -  -  -  -  -
13 0-4   M  -  -  M  -  M
14 0-4   -  N  -  N  N  N
15 0-4   -  -  -  O  O  O
16 0-4   -  -  -  -  -  -
17 0-4   Q  Q  Q  Q  -  -
18 0-4   R  R  R  -  -   
19 0-4   -  S  S  -  S   
20 0-5   T  -  -  -      
21 0-6   -  U  -  U      
22 1-2   V  V  V         
23 1-6   -  X  X         
24 2-4   X  -            
25 2-4   -  -            
26 2-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KSZMZ AOLXN DSDKZ OPOVK KZUSK K
-------------------------------
