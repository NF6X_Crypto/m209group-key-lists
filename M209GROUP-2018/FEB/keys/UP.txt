EFFECTIVE PERIOD:
21-FEB-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  -
02 2-0   -  B  B  B  B  B
03 2-0   C  C  C  -  C  C
04 2-0   D  D  -  -  -  -
05 2-0   -  -  E  E  -  E
06 2-0   F  -  F  F  -  F
07 2-0   -  -  -  -  G  G
08 2-0   -  H  H  -  H  -
09 2-0   I  I  I  I  I  I
10 2-0   -  -  J  -  J  -
11 0-3   -  -  -  -  K  K
12 0-3   L  -  -  L  -  -
13 0-3   -  M  -  M  M  -
14 0-3   N  -  N  N  N  N
15 0-3   -  O  O  O  -  O
16 0-3   -  P  -  -  -  P
17 0-3   -  -  Q  Q  -  -
18 0-4   -  R  R  R  -   
19 0-4   S  -  -  -  S   
20 0-5   -  T  -  -      
21 0-5   U  -  -  U      
22 0-5   V  V  V         
23 0-6   W  X  -         
24 1-6   X  -            
25 3-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

XYHUN PAPXJ ZKUPZ OFQVS JJFVI P
-------------------------------
