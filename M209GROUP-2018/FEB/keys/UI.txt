EFFECTIVE PERIOD:
14-FEB-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   -  B  B  -  -  B
03 1-0   -  C  C  -  C  -
04 1-0   D  D  D  -  D  -
05 1-0   E  -  -  E  -  -
06 2-0   -  F  F  F  F  F
07 0-3   G  G  -  G  G  G
08 0-3   -  -  -  H  H  -
09 0-3   -  I  -  I  -  -
10 0-5   J  -  -  J  J  J
11 0-5   K  K  -  -  K  K
12 0-5   L  -  L  -  L  L
13 0-5   M  M  M  -  M  -
14 0-6   N  N  -  -  -  -
15 0-6   O  O  -  O  O  O
16 0-6   -  -  -  -  -  P
17 0-6   -  Q  Q  -  -  -
18 0-6   -  -  -  R  R   
19 0-6   S  -  S  S  -   
20 1-5   -  -  T  -      
21 1-6   U  U  U  -      
22 1-6   -  -  -         
23 1-6   W  -  X         
24 1-6   -  Y            
25 2-3   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SQCSK VLOKP HKQVR TMXQH OWZML X
-------------------------------
