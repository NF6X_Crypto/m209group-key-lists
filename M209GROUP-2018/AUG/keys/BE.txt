EFFECTIVE PERIOD:
11-AUG-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   B  B  -  B  B  B
03 2-0   -  C  -  -  C  -
04 2-0   -  -  D  D  -  D
05 2-0   -  -  E  -  E  E
06 2-0   -  -  F  -  -  -
07 2-0   -  G  G  G  -  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  I  -  I  -
10 0-3   -  -  -  J  -  -
11 0-3   K  K  K  K  -  K
12 0-3   L  L  -  -  -  L
13 0-4   -  -  M  -  M  M
14 0-4   -  N  -  N  N  -
15 0-4   -  -  -  -  O  O
16 1-4   P  -  -  P  P  -
17 1-5   -  Q  -  -  -  -
18 2-3   -  -  -  R  R   
19 2-5   S  S  S  -  -   
20 3-4   T  -  -  -      
21 3-4   -  U  U  -      
22 3-4   V  V  -         
23 3-4   W  X  X         
24 4-5   X  Y            
25 4-5   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

CNNNO RNTOA NLVSK RTSAL OOAEU F
-------------------------------
