EFFECTIVE PERIOD:
06-AUG-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  A
02 2-0   -  B  B  B  B  B
03 2-0   -  -  C  -  C  C
04 2-0   D  D  D  D  D  D
05 0-3   -  E  -  E  -  -
06 0-3   -  F  -  F  -  -
07 0-3   G  -  -  G  -  G
08 0-3   -  -  -  H  -  H
09 0-3   I  -  -  -  -  -
10 0-3   -  J  J  -  J  -
11 0-4   K  -  -  K  K  -
12 0-4   L  -  -  L  -  L
13 0-4   M  M  -  -  -  M
14 0-4   -  -  N  -  N  -
15 0-6   O  -  O  -  O  -
16 0-6   -  P  -  -  P  P
17 0-6   -  -  Q  -  Q  -
18 1-5   -  -  R  R  R   
19 2-4   S  S  -  S  -   
20 2-4   T  -  T  T      
21 2-4   -  -  -  -      
22 2-4   V  V  -         
23 2-5   W  X  -         
24 2-6   -  Y            
25 3-4   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HVHKT QSUOU ATVUU OOCHV OCHTO H
-------------------------------
