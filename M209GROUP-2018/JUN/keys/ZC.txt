EFFECTIVE PERIOD:
18-JUN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  B  B  -  B  -
03 1-0   -  C  -  C  C  -
04 2-0   D  D  D  D  -  D
05 2-0   E  E  -  E  E  E
06 2-0   -  -  F  F  -  -
07 2-0   -  -  G  -  -  G
08 2-0   -  -  -  H  -  H
09 0-3   -  -  I  I  -  I
10 0-3   -  J  -  J  J  J
11 0-4   -  K  -  K  K  -
12 0-4   L  -  -  L  L  -
13 0-4   -  -  -  -  -  M
14 0-4   N  N  N  -  N  -
15 0-4   O  O  O  -  -  -
16 0-4   P  P  P  -  -  P
17 0-4   Q  -  -  Q  -  Q
18 0-4   -  R  -  -  R   
19 0-4   S  -  S  S  -   
20 0-4   -  T  -  -      
21 0-4   U  U  U  -      
22 0-4   -  V  -         
23 0-6   -  X  -         
24 1-2   X  -            
25 1-3   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NMFJL OULKF JXWXY ORMQO RGHFO Z
-------------------------------
