EFFECTIVE PERIOD:
01-JUN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  B  B  B  B  -
03 1-0   -  -  -  C  C  C
04 1-0   D  D  D  -  D  -
05 1-0   E  -  E  -  E  -
06 1-0   -  -  F  -  -  F
07 1-0   G  G  -  -  G  G
08 0-3   -  -  -  -  -  H
09 0-4   -  -  I  I  -  -
10 0-4   J  -  -  J  J  -
11 0-4   -  -  -  -  -  K
12 0-4   -  -  L  -  -  -
13 0-5   M  M  M  -  M  -
14 0-5   N  N  -  N  N  N
15 0-5   O  -  O  O  O  O
16 0-5   -  P  P  -  -  P
17 0-6   Q  -  Q  Q  -  -
18 0-6   -  R  -  -  -   
19 0-6   S  -  S  S  S   
20 0-6   -  T  -  T      
21 0-6   -  U  U  U      
22 1-5   V  -  -         
23 1-5   W  X  -         
24 1-5   -  Y            
25 2-4   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GVOWL IPVNV UOOHW KZZGZ UOTPL Q
-------------------------------
