EFFECTIVE PERIOD:
09-JUN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  -  -  B  B  -
03 1-0   -  C  C  -  -  -
04 1-0   -  D  -  -  D  -
05 1-0   E  E  E  -  -  -
06 1-0   F  -  -  -  -  F
07 1-0   -  G  G  G  -  G
08 1-0   H  -  H  -  H  H
09 1-0   -  I  -  -  -  -
10 1-0   J  -  J  -  J  J
11 2-0   K  -  K  -  K  K
12 2-0   L  -  L  -  L  L
13 0-3   -  M  -  M  M  -
14 0-4   -  N  -  N  N  N
15 0-5   O  O  O  O  -  -
16 0-5   -  P  P  P  -  -
17 0-5   Q  Q  -  Q  Q  -
18 0-5   R  R  R  -  -   
19 0-5   -  -  -  -  S   
20 0-5   T  -  -  T      
21 0-5   -  U  U  -      
22 1-5   V  -  V         
23 1-5   -  -  -         
24 2-4   -  -            
25 2-5   Y  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

PAAGQ JPOCW QJOEG YOPUP WYOFQ T
-------------------------------
