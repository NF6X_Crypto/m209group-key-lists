EFFECTIVE PERIOD:
24-DEC-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  -  B  -  B  -
03 1-0   -  -  -  C  -  C
04 1-0   D  -  D  D  -  D
05 1-0   E  E  -  -  -  E
06 1-0   F  F  F  -  F  -
07 0-3   G  G  G  -  G  G
08 0-3   -  -  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-3   J  J  -  -  -  -
11 0-3   K  -  K  -  K  -
12 0-3   -  -  -  L  -  -
13 0-4   -  M  -  -  -  -
14 0-4   N  -  N  N  N  N
15 0-4   O  -  -  -  O  O
16 1-2   -  -  P  P  P  P
17 1-3   Q  Q  -  -  -  -
18 1-4   -  -  -  R  -   
19 2-4   -  -  -  S  S   
20 2-4   -  T  T  T      
21 2-6   U  U  U  -      
22 3-4   V  -  -         
23 3-4   -  X  -         
24 3-4   -  -            
25 3-4   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KTPMC UASDL BUTNL VWALD ALMPT S
-------------------------------
