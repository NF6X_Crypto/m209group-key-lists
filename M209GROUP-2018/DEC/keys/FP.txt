EFFECTIVE PERIOD:
04-DEC-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   B  -  -  -  -  -
03 0-3   C  C  -  -  C  -
04 0-3   D  -  D  D  -  D
05 0-3   -  E  -  E  -  E
06 0-3   F  -  F  F  -  -
07 0-3   G  -  G  G  -  G
08 0-3   H  H  H  -  H  -
09 0-3   -  -  -  I  I  -
10 0-3   -  J  J  -  -  J
11 0-4   -  K  K  -  -  -
12 0-4   -  -  L  -  L  L
13 0-4   M  -  M  M  M  -
14 0-4   -  -  N  -  -  -
15 0-4   -  O  -  O  O  O
16 0-4   P  -  -  -  P  P
17 0-5   -  Q  Q  -  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   -  -  -  -  -   
20 1-5   T  T  T  -      
21 1-6   U  U  -  U      
22 2-5   V  V  -         
23 3-4   -  -  X         
24 3-4   X  Y            
25 3-4   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

YGPOI STEQQ GHOZO VUWHO OWSAG A
-------------------------------
