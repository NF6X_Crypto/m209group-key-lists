EFFECTIVE PERIOD:
07-DEC-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   B  -  -  -  -  -
03 1-0   C  -  C  -  C  -
04 1-0   -  -  D  -  -  D
05 1-0   E  -  -  E  E  E
06 0-4   -  F  -  -  -  -
07 0-4   -  G  -  -  G  G
08 0-4   -  H  H  H  -  H
09 0-4   -  -  -  I  I  -
10 0-4   -  -  J  J  J  -
11 0-4   K  K  -  K  K  K
12 0-6   L  L  L  -  -  L
13 0-6   -  -  -  -  -  M
14 0-6   -  -  N  -  N  N
15 0-6   O  O  O  O  -  O
16 0-6   P  -  -  P  P  -
17 0-6   Q  Q  -  Q  -  -
18 1-3   R  R  R  R  R   
19 1-4   -  -  -  -  S   
20 1-4   -  T  T  T      
21 1-4   U  U  -  U      
22 1-4   -  V  -         
23 1-5   W  -  -         
24 1-5   X  -            
25 2-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UTASV TEUSZ LINZK LOVUS HUMPL U
-------------------------------
