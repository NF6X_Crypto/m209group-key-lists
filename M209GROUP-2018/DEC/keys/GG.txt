EFFECTIVE PERIOD:
21-DEC-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  -  -  B  B  B
03 1-0   C  C  C  C  -  C
04 1-0   D  D  D  D  D  -
05 2-0   -  E  -  -  E  -
06 0-3   -  F  -  -  -  F
07 0-4   -  -  -  -  G  -
08 0-4   -  H  -  -  -  -
09 0-4   I  -  I  -  -  -
10 0-4   -  -  -  J  J  J
11 0-5   -  K  K  K  K  K
12 0-6   L  L  L  L  L  -
13 0-6   -  M  M  -  -  M
14 0-6   -  -  -  N  -  -
15 0-6   -  O  O  -  O  -
16 0-6   P  -  P  -  -  -
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   -  S  -  S  -   
20 0-6   T  -  T  T      
21 0-6   U  -  U  U      
22 1-3   -  V  -         
23 1-4   -  -  X         
24 3-5   X  -            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

VOVEN OULYQ OJVII ANOMY KDQND V
-------------------------------
