EFFECTIVE PERIOD:
10-DEC-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   -  C  -  C  C  C
04 1-0   D  -  -  -  -  -
05 1-0   E  -  -  -  E  E
06 2-0   -  F  F  -  -  -
07 2-0   -  -  G  -  G  -
08 0-3   H  H  H  H  -  H
09 0-3   I  I  I  -  -  -
10 0-3   -  -  -  -  -  -
11 0-3   K  K  -  K  -  -
12 0-3   -  L  -  L  L  -
13 0-4   M  M  -  -  -  -
14 0-4   -  -  -  -  -  -
15 0-4   O  O  O  O  -  O
16 0-4   P  P  P  P  P  P
17 0-4   Q  -  Q  -  Q  Q
18 1-2   -  R  -  R  R   
19 1-3   -  -  -  S  -   
20 2-4   T  -  T  -      
21 2-6   -  U  -  U      
22 3-4   V  V  -         
23 3-4   -  -  X         
24 3-4   X  -            
25 3-4   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

LTRVJ MAFSS VVFEJ PVJTV LLFVS U
-------------------------------
