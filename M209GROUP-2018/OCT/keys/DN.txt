EFFECTIVE PERIOD:
11-OCT-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  -  B  B  B
03 1-0   -  C  C  C  -  C
04 1-0   -  -  D  D  -  -
05 1-0   -  E  E  -  E  E
06 1-0   F  F  -  F  -  -
07 2-0   -  -  G  -  -  -
08 2-0   H  -  -  -  H  -
09 2-0   I  I  -  -  -  I
10 2-0   -  -  J  J  J  J
11 0-6   K  -  -  K  -  K
12 0-6   L  -  L  -  -  L
13 0-6   -  -  M  -  M  -
14 0-6   -  -  N  -  N  -
15 0-6   O  O  -  -  O  -
16 1-2   -  P  P  P  P  P
17 1-4   -  Q  -  -  Q  -
18 1-6   R  R  -  R  -   
19 2-3   -  -  S  -  -   
20 2-4   T  T  -  T      
21 2-4   U  U  -  U      
22 2-5   -  V  V         
23 2-6   W  X  X         
24 2-6   X  -            
25 2-6   -  Z            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

STVOK AMAAM VNOME TZLUV BLUCO Z
-------------------------------
