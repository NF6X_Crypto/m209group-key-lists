EFFECTIVE PERIOD:
09-OCT-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  -  -  -  B
03 1-0   -  C  C  -  -  C
04 0-3   D  D  -  D  D  D
05 0-3   -  -  E  -  E  E
06 0-3   F  -  F  F  F  -
07 0-3   -  -  -  G  -  G
08 0-3   H  -  H  H  H  -
09 0-5   I  I  -  I  -  I
10 0-5   J  -  J  -  J  J
11 0-5   -  -  -  -  K  K
12 0-5   L  L  -  -  -  -
13 0-5   -  M  -  M  M  -
14 0-5   -  -  N  N  N  -
15 0-5   O  -  O  -  -  -
16 0-6   P  -  P  -  -  P
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   S  -  S  -  -   
20 1-3   -  T  T  T      
21 1-5   -  -  U  -      
22 1-5   V  -  V         
23 1-5   -  -  -         
24 1-5   X  Y            
25 2-5   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SWVOA XSHAG FRWRP XNXPO PMOMS S
-------------------------------
