EFFECTIVE PERIOD:
26-OCT-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  -  -  B  B  B
03 1-0   -  C  -  -  C  C
04 1-0   D  -  D  D  D  D
05 1-0   -  -  E  E  E  -
06 1-0   -  -  F  -  -  -
07 2-0   -  G  -  -  -  G
08 2-0   -  H  -  -  -  -
09 2-0   I  I  -  I  -  I
10 2-0   J  -  -  -  J  J
11 2-0   K  K  K  K  -  -
12 2-0   L  L  -  L  -  -
13 0-3   M  M  M  -  -  M
14 0-3   -  N  N  N  -  N
15 0-3   -  -  -  O  O  O
16 0-3   -  -  P  -  -  -
17 0-3   -  -  -  -  Q  -
18 0-3   R  -  R  -  -   
19 0-3   -  S  S  -  -   
20 0-3   T  T  -  T      
21 0-4   U  U  U  U      
22 0-5   V  V  -         
23 0-6   -  -  -         
24 1-2   X  -            
25 1-4   -  -            
26 2-3   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ICTCY XOTLC UIKTQ KBLHR TQZYK K
-------------------------------
