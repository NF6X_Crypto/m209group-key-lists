EFFECTIVE PERIOD:
19-MAY-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  B  B  -  -  -
03 1-0   -  -  C  C  C  C
04 1-0   D  D  D  -  -  D
05 1-0   -  E  E  E  E  E
06 1-0   -  F  -  F  -  -
07 1-0   -  -  G  G  G  -
08 1-0   -  -  -  H  H  -
09 2-0   I  -  -  I  -  -
10 0-3   J  J  J  J  -  -
11 0-3   K  -  K  -  -  K
12 0-3   -  -  -  -  -  L
13 0-3   -  -  M  -  M  M
14 0-3   N  N  N  -  N  N
15 0-3   O  O  -  -  O  O
16 0-3   P  P  P  -  -  -
17 0-3   Q  Q  -  Q  -  -
18 0-3   -  -  -  -  R   
19 0-3   S  -  -  -  S   
20 0-4   T  -  -  -      
21 0-4   U  -  -  U      
22 0-4   V  V  V         
23 0-4   W  X  X         
24 1-3   -  Y            
25 1-4   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VMNLP JLMRS MANQA HRGGU WLMKL A
-------------------------------
