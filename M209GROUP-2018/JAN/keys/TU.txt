EFFECTIVE PERIOD:
31-JAN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  B  -  -  B  -
03 1-0   -  -  -  C  C  -
04 1-0   D  D  D  -  D  D
05 1-0   -  E  -  -  -  E
06 1-0   F  -  -  -  F  -
07 1-0   G  -  G  -  -  G
08 2-0   -  H  -  -  H  -
09 2-0   -  -  I  I  I  -
10 2-0   -  -  J  -  -  -
11 2-0   -  -  -  K  -  K
12 2-0   L  L  -  L  -  -
13 2-0   -  -  M  M  M  M
14 0-3   -  N  -  N  N  -
15 0-3   -  O  -  O  -  O
16 0-3   -  -  P  P  P  -
17 0-3   -  Q  -  -  Q  Q
18 0-3   -  R  R  R  -   
19 0-4   S  -  S  -  -   
20 0-5   -  -  -  T      
21 0-6   U  -  U  U      
22 1-3   V  -  V         
23 1-3   W  X  -         
24 1-3   X  Y            
25 2-3   -  Z            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KCTTM SMRZK ZLRMR IQNPM CEYEK P
-------------------------------
