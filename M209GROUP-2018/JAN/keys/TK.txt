EFFECTIVE PERIOD:
21-JAN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  -  B  B  -  -
03 1-0   -  -  C  C  C  C
04 1-0   -  D  -  -  -  -
05 1-0   -  E  E  -  -  -
06 1-0   F  F  -  -  F  F
07 2-0   G  -  -  G  G  -
08 2-0   H  H  -  -  -  H
09 2-0   I  -  I  -  I  I
10 0-4   -  J  J  J  J  -
11 0-5   -  K  -  K  -  K
12 0-5   L  L  -  L  L  L
13 0-5   M  M  -  M  -  -
14 0-5   -  -  -  -  -  N
15 0-5   -  O  O  -  -  -
16 0-6   -  -  -  P  P  P
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  R  -  R  R   
19 0-6   -  S  S  -  -   
20 1-2   -  -  -  -      
21 1-5   -  -  U  -      
22 2-6   V  V  V         
23 2-6   W  -  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UQOOP QWTHS OCQGT OUWVP OJTZQ P
-------------------------------
