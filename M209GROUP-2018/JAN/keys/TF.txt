EFFECTIVE PERIOD:
16-JAN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  -  -  B  -  -
03 1-0   -  -  C  -  -  -
04 1-0   -  -  -  D  D  -
05 1-0   -  E  -  E  E  E
06 1-0   -  -  -  -  F  -
07 1-0   G  G  G  G  G  -
08 1-0   H  -  H  H  -  H
09 1-0   -  I  -  I  -  I
10 1-0   -  J  -  J  J  J
11 0-3   K  K  K  K  K  K
12 0-4   -  -  L  -  -  -
13 0-4   M  M  M  -  -  -
14 0-4   N  -  N  -  N  N
15 0-4   O  -  O  -  -  O
16 0-5   P  P  -  P  P  -
17 0-5   -  -  Q  -  Q  -
18 0-5   R  R  R  R  R   
19 0-5   S  S  -  -  -   
20 0-5   -  -  T  -      
21 0-5   U  U  -  -      
22 1-5   -  V  -         
23 1-5   W  -  -         
24 1-5   X  Y            
25 2-4   -  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

OCTUM YMOTG GLZOK GHAYG CWAWZ T
-------------------------------
