EFFECTIVE PERIOD:
18-JAN-2018 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   -  -  -  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   -  -  D  -  D  D
05 1-0   -  -  -  E  E  E
06 2-0   F  -  -  F  -  -
07 2-0   G  -  -  -  G  G
08 2-0   H  H  H  -  -  -
09 2-0   I  I  I  -  I  I
10 2-0   J  J  J  -  -  -
11 2-0   -  -  -  -  K  -
12 2-0   L  -  L  -  -  -
13 2-0   M  -  -  M  M  M
14 2-0   -  -  -  -  -  -
15 2-0   -  O  O  O  O  O
16 2-0   P  P  -  P  P  -
17 0-3   Q  -  Q  -  -  Q
18 0-6   R  R  R  R  R   
19 0-6   S  S  S  S  -   
20 0-6   -  -  T  -      
21 0-6   U  -  U  -      
22 0-6   -  -  V         
23 0-6   W  X  -         
24 1-5   X  -            
25 1-6   -  -            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TUOUV IKHZJ OUBUB HHHUZ TJHOB U
-------------------------------
