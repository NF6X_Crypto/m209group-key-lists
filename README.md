This project contains M-209 cipher machine key lists for use by the
M-209 Cipher Machine Group:

    http://www.nf6x.net/m209group

These key lists were created by Mark J. Blair <nf6x@nf6x.net>. They
are all placed in the public domain by their author.

In addition to the key lists, a script is provided to generate two
different distributions of a year's worth of key lists:

* A .zip archive in which the .txt files have DOS line endings.

* A .tar.gz archive in which the .txt files have UNIX line endings.

Other than line ending conversion of .txt files, the two archives
contain identical contents. Two distribution forms are provided for
convenience of users who may not be familiar with different line
ending styles and the means of translating them.

Key lists will primarily be distributed in these two archive formats
at the group web page. End users are not normally expected to obtain
the key lists from this repository, but they are welcome to do so if
they wish. The home for this repository is:

    https://gitlab.com/NF6X_Crypto/m209group-key-lists

Key lists are generated with the key list generator that is included
as part of the hagelin package:

    https://gitlab.com/NF6X_Crypto/hagelin

The primary net indicator shall be "M209GROUP", though others may also
be used if a need arises.

