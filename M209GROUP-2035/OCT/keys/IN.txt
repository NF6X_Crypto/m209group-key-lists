EFFECTIVE PERIOD:
16-OCT-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  B  B  -  -  B
03 1-0   C  -  C  C  C  C
04 1-0   -  D  -  D  -  D
05 1-0   -  E  -  E  -  E
06 1-0   F  -  F  -  -  F
07 1-0   G  -  G  -  G  -
08 1-0   H  -  H  H  -  H
09 2-0   -  I  -  I  -  -
10 0-3   J  J  J  -  J  -
11 0-3   K  K  K  K  -  -
12 0-3   -  -  -  L  -  -
13 0-3   -  -  M  M  M  -
14 0-3   N  N  N  N  -  -
15 0-4   O  -  -  O  O  O
16 0-4   -  -  P  -  P  P
17 0-4   -  -  -  Q  -  Q
18 0-4   R  R  -  -  R   
19 0-5   -  -  -  -  S   
20 0-5   -  T  -  T      
21 0-5   U  U  -  -      
22 1-3   V  V  -         
23 1-3   W  -  X         
24 2-5   X  -            
25 2-6   Y  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MJUGK MQVKQ OVPPG KUYJP ASRRM Z
-------------------------------
