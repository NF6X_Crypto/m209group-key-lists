EFFECTIVE PERIOD:
29-OCT-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   B  -  B  B  -  B
03 2-0   C  C  C  -  C  -
04 2-0   D  D  -  D  -  -
05 0-3   -  -  E  E  -  E
06 0-3   F  F  F  -  F  F
07 0-3   -  -  G  -  G  -
08 0-3   -  -  -  -  -  -
09 0-3   I  I  -  I  -  -
10 0-3   -  -  -  -  -  J
11 0-3   K  K  -  -  K  K
12 0-3   -  -  L  -  L  L
13 0-4   M  -  -  M  M  -
14 0-4   N  N  -  N  -  -
15 0-4   O  O  O  O  O  O
16 0-4   P  P  -  P  P  -
17 0-4   -  -  -  -  Q  Q
18 0-5   -  R  -  -  -   
19 0-6   S  S  -  -  S   
20 0-6   T  T  T  -      
21 0-6   U  U  -  U      
22 0-6   -  -  V         
23 0-6   -  -  X         
24 0-6   X  -            
25 0-6   -  -            
26 1-2   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

SZFLN GTAXH GOWLO HUMHG AZGPT M
-------------------------------
