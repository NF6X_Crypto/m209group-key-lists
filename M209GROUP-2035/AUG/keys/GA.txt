EFFECTIVE PERIOD:
12-AUG-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  B  B  B  B  B
03 1-0   C  -  -  C  -  -
04 1-0   -  D  -  D  -  D
05 1-0   E  -  E  E  E  -
06 1-0   -  -  -  -  -  F
07 1-0   G  -  G  G  G  -
08 2-0   H  -  -  -  H  -
09 2-0   I  -  -  I  -  -
10 2-0   -  -  J  -  -  -
11 2-0   -  K  -  -  -  K
12 0-3   L  L  L  -  L  L
13 0-3   M  M  M  M  M  -
14 0-3   -  N  N  N  -  N
15 0-3   -  O  O  -  O  -
16 0-3   -  -  -  P  P  -
17 0-3   Q  Q  -  -  Q  Q
18 0-3   -  R  R  -  R   
19 0-3   S  S  -  -  -   
20 1-2   T  -  T  T      
21 1-3   U  -  -  -      
22 1-3   -  V  -         
23 1-3   -  X  -         
24 1-3   X  Y            
25 2-4   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

TWFSA MNGRR OWHNT NMOMV BTAAT H
-------------------------------
