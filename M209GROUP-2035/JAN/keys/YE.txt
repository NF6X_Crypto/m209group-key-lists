EFFECTIVE PERIOD:
20-JAN-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  B  -  B  B  -
03 1-0   C  -  C  C  C  C
04 0-3   D  D  -  -  D  D
05 0-3   -  -  E  -  E  E
06 0-3   -  F  -  F  -  F
07 0-3   G  -  -  -  G  -
08 0-3   H  -  H  H  -  -
09 0-5   -  I  -  -  -  I
10 0-5   -  -  -  -  -  -
11 0-5   -  -  K  K  -  -
12 0-5   L  -  L  -  L  L
13 0-5   -  M  M  -  -  M
14 0-5   -  -  N  N  -  -
15 0-5   -  O  -  O  O  O
16 1-2   -  P  -  P  P  -
17 1-2   Q  -  -  -  Q  Q
18 1-2   R  -  R  -  -   
19 1-3   S  S  S  -  S   
20 1-3   -  T  T  T      
21 1-3   -  U  U  -      
22 1-3   -  V  -         
23 1-4   W  -  X         
24 1-6   -  Y            
25 2-4   -  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

VNMVW VPAZW VLALA AFRLO JKAXK E
-------------------------------
