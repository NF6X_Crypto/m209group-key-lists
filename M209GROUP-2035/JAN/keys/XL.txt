EFFECTIVE PERIOD:
01-JAN-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   C  -  -  -  -  -
04 1-0   -  -  -  D  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  F  F  -  F  F
07 1-0   -  G  -  -  -  -
08 1-0   H  -  -  -  -  H
09 1-0   -  I  -  I  -  I
10 1-0   J  -  -  J  -  -
11 1-0   -  -  K  -  K  -
12 2-0   L  -  L  L  L  -
13 0-3   M  -  M  M  -  -
14 0-3   N  -  N  N  N  N
15 0-3   -  O  O  O  -  -
16 0-3   P  -  -  -  -  P
17 0-3   -  Q  Q  -  -  -
18 0-4   R  R  -  -  R   
19 0-4   -  -  S  S  S   
20 0-5   -  T  T  T      
21 0-5   U  U  U  -      
22 0-5   V  -  -         
23 0-5   -  -  X         
24 1-3   X  Y            
25 3-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AKVIZ TAOLM VXJRA IMEUG VOVVS G
-------------------------------
