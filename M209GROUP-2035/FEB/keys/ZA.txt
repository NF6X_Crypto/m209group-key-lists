EFFECTIVE PERIOD:
11-FEB-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  -  -  -  B  -
03 1-0   -  -  C  C  -  C
04 1-0   -  -  -  D  -  D
05 1-0   E  E  E  E  -  E
06 1-0   F  F  -  -  F  F
07 1-0   G  G  G  G  -  -
08 1-0   H  H  H  -  H  -
09 2-0   -  I  I  -  I  -
10 2-0   J  -  J  -  -  J
11 2-0   -  K  -  -  K  -
12 2-0   L  -  L  L  L  -
13 0-3   M  -  M  M  -  -
14 0-4   N  -  N  N  -  N
15 0-4   -  O  -  O  O  O
16 0-5   P  P  -  P  P  P
17 0-5   Q  -  -  -  -  -
18 0-5   R  -  R  -  R   
19 0-5   S  S  -  -  -   
20 0-5   T  -  T  -      
21 0-6   -  U  U  U      
22 1-5   -  -  -         
23 1-5   -  -  -         
24 1-5   -  -            
25 2-4   -  -            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZTOVY BOCDN NJQMA YYRWO PKWVP N
-------------------------------
