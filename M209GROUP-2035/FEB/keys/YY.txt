EFFECTIVE PERIOD:
09-FEB-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  B  B  -  B  -
03 1-0   -  C  -  C  C  -
04 1-0   -  -  D  -  D  D
05 1-0   E  E  E  -  -  E
06 1-0   -  -  -  F  -  F
07 1-0   -  -  -  -  G  -
08 1-0   -  H  H  H  -  H
09 1-0   -  I  -  -  -  I
10 2-0   J  -  -  -  J  -
11 2-0   K  K  K  -  -  -
12 2-0   L  -  -  L  -  L
13 2-0   M  -  M  -  M  -
14 2-0   -  N  N  N  -  N
15 2-0   -  O  -  O  -  O
16 2-0   P  P  -  P  -  -
17 0-4   -  -  -  Q  Q  -
18 0-4   -  R  R  -  R   
19 0-5   S  -  S  S  -   
20 0-5   -  -  T  -      
21 0-5   U  -  U  -      
22 0-5   V  V  -         
23 0-6   -  -  -         
24 1-2   X  -            
25 2-5   -  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SRDWE QSJPF ORSJJ SDGJI TSNNQ Q
-------------------------------
