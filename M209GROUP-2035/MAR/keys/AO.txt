EFFECTIVE PERIOD:
23-MAR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   B  -  -  -  -  -
03 2-0   C  C  C  -  -  -
04 0-4   -  D  D  D  -  D
05 0-4   -  E  -  E  -  -
06 0-4   F  F  -  -  F  F
07 0-4   -  -  G  -  G  G
08 0-4   H  H  H  -  -  -
09 0-4   I  -  I  -  I  I
10 0-4   J  -  J  J  -  -
11 0-4   -  -  K  -  -  -
12 0-4   L  L  L  -  -  L
13 0-4   -  -  -  -  M  -
14 0-5   N  N  -  -  N  -
15 0-5   -  O  O  O  O  O
16 0-5   -  -  -  P  P  -
17 0-5   Q  -  -  -  Q  Q
18 0-5   -  -  -  R  R   
19 0-5   S  -  S  S  -   
20 0-5   -  T  T  T      
21 0-6   U  -  -  U      
22 1-2   V  V  V         
23 1-5   W  X  -         
24 2-6   -  -            
25 3-6   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

CPTWM ZZRPP CZSAM EPATE NMOWZ M
-------------------------------
