EFFECTIVE PERIOD:
17-MAR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  B  B  -  -  -
03 1-0   C  C  C  -  -  -
04 2-0   -  D  D  D  -  -
05 2-0   E  E  -  E  -  E
06 2-0   -  -  -  F  F  F
07 2-0   G  G  G  G  -  -
08 2-0   -  -  -  H  -  H
09 2-0   -  -  I  -  I  I
10 2-0   -  J  -  J  -  -
11 0-3   -  -  K  -  K  K
12 0-6   L  -  L  -  L  -
13 0-6   M  -  -  -  -  M
14 0-6   N  N  N  -  N  -
15 0-6   O  -  O  -  O  -
16 0-6   P  P  -  -  P  P
17 0-6   -  -  Q  Q  -  -
18 0-6   -  R  -  -  -   
19 0-6   -  -  S  S  -   
20 1-2   T  T  -  T      
21 1-4   U  -  U  -      
22 2-6   -  -  -         
23 2-6   -  X  X         
24 2-6   -  -            
25 2-6   -  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TNMOG ABPMC LTXSW ZDYDM NCRHA F
-------------------------------
