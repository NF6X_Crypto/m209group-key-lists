SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JUL 2035

    01 JUL 2035  00:00-23:59 GMT:  USE KEY EK
    02 JUL 2035  00:00-23:59 GMT:  USE KEY EL
    03 JUL 2035  00:00-23:59 GMT:  USE KEY EM
    04 JUL 2035  00:00-23:59 GMT:  USE KEY EN
    05 JUL 2035  00:00-23:59 GMT:  USE KEY EO
    06 JUL 2035  00:00-23:59 GMT:  USE KEY EP
    07 JUL 2035  00:00-23:59 GMT:  USE KEY EQ
    08 JUL 2035  00:00-23:59 GMT:  USE KEY ER
    09 JUL 2035  00:00-23:59 GMT:  USE KEY ES
    10 JUL 2035  00:00-23:59 GMT:  USE KEY ET
    11 JUL 2035  00:00-23:59 GMT:  USE KEY EU
    12 JUL 2035  00:00-23:59 GMT:  USE KEY EV
    13 JUL 2035  00:00-23:59 GMT:  USE KEY EW
    14 JUL 2035  00:00-23:59 GMT:  USE KEY EX
    15 JUL 2035  00:00-23:59 GMT:  USE KEY EY
    16 JUL 2035  00:00-23:59 GMT:  USE KEY EZ
    17 JUL 2035  00:00-23:59 GMT:  USE KEY FA
    18 JUL 2035  00:00-23:59 GMT:  USE KEY FB
    19 JUL 2035  00:00-23:59 GMT:  USE KEY FC
    20 JUL 2035  00:00-23:59 GMT:  USE KEY FD
    21 JUL 2035  00:00-23:59 GMT:  USE KEY FE
    22 JUL 2035  00:00-23:59 GMT:  USE KEY FF
    23 JUL 2035  00:00-23:59 GMT:  USE KEY FG
    24 JUL 2035  00:00-23:59 GMT:  USE KEY FH
    25 JUL 2035  00:00-23:59 GMT:  USE KEY FI
    26 JUL 2035  00:00-23:59 GMT:  USE KEY FJ
    27 JUL 2035  00:00-23:59 GMT:  USE KEY FK
    28 JUL 2035  00:00-23:59 GMT:  USE KEY FL
    29 JUL 2035  00:00-23:59 GMT:  USE KEY FM
    30 JUL 2035  00:00-23:59 GMT:  USE KEY FN
    31 JUL 2035  00:00-23:59 GMT:  USE KEY FO

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  -  B  B  B  -
03 2-0   -  C  C  C  C  -
04 2-0   -  -  D  -  -  D
05 2-0   -  E  -  -  E  -
06 2-0   F  F  -  F  -  F
07 2-0   -  -  G  -  G  G
08 2-0   -  -  H  H  -  H
09 2-0   I  I  I  -  I  -
10 2-0   -  J  -  -  J  -
11 0-3   -  K  -  K  K  K
12 0-4   -  -  L  -  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  -  -  -  N  N
15 0-5   -  O  -  O  -  O
16 0-5   P  -  -  P  -  -
17 0-5   -  -  Q  Q  -  -
18 0-5   R  R  -  -  -   
19 0-5   S  -  -  S  -   
20 0-5   T  -  -  -      
21 0-5   U  U  U  -      
22 0-5   V  V  V         
23 0-5   W  -  -         
24 0-5   -  -            
25 0-5   Y  -            
26 1-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MZRNE CMMAR QCPKP HAPNM JYAEE J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   A  A  A  A  A  -
02 0-4   -  -  -  -  B  B
03 0-4   C  C  C  C  C  -
04 0-4   -  D  -  D  D  -
05 0-4   -  -  E  E  E  -
06 0-4   F  F  F  -  -  -
07 0-4   G  G  -  -  -  G
08 0-5   H  H  -  H  -  -
09 0-5   I  -  -  I  -  -
10 0-5   -  J  J  -  -  -
11 0-5   K  -  -  K  K  K
12 0-6   L  L  L  -  L  L
13 0-6   -  -  -  M  -  M
14 0-6   -  N  N  N  -  N
15 0-6   -  -  O  -  -  -
16 0-6   P  -  -  P  -  -
17 0-6   Q  Q  -  -  Q  Q
18 1-3   R  -  -  R  -   
19 2-6   -  -  S  -  S   
20 3-5   -  T  -  -      
21 4-5   -  -  U  U      
22 4-6   -  V  V         
23 4-6   -  -  X         
24 4-6   X  Y            
25 4-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWNUZ UMJAU IUTVC BNVTA MNNOU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  -  -  -  B  -
03 1-0   C  C  C  -  C  -
04 1-0   -  D  -  D  -  D
05 2-0   -  E  E  E  E  -
06 0-5   -  F  -  -  F  F
07 0-5   G  G  -  -  -  -
08 0-5   -  -  H  -  -  -
09 0-5   -  -  -  -  I  I
10 0-5   J  J  -  J  J  -
11 0-5   K  K  K  -  K  K
12 0-5   L  -  L  L  -  L
13 0-6   -  M  -  -  M  -
14 0-6   -  -  -  -  -  -
15 0-6   -  O  O  O  -  -
16 0-6   P  -  -  P  -  P
17 0-6   -  -  Q  Q  -  Q
18 0-6   -  R  -  -  -   
19 0-6   S  S  -  S  S   
20 0-6   -  T  T  T      
21 0-6   U  -  U  U      
22 0-6   V  -  -         
23 0-6   -  X  -         
24 1-4   X  -            
25 1-5   -  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SUBHC SSKHI AGZPB MJAVZ PHROB C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   B  -  B  B  B  B
03 2-0   C  -  -  -  -  -
04 0-4   D  -  -  D  -  D
05 0-4   -  -  -  E  E  -
06 0-5   -  F  -  -  -  F
07 0-5   -  G  G  -  G  -
08 0-5   -  -  -  H  H  H
09 0-5   -  -  -  I  -  I
10 0-5   J  -  -  J  -  J
11 0-5   -  K  -  -  K  -
12 0-6   -  -  -  L  -  L
13 0-6   -  M  -  -  M  -
14 0-6   N  N  N  N  -  N
15 0-6   -  O  O  O  O  O
16 1-2   -  P  P  -  -  -
17 1-3   Q  -  -  -  Q  -
18 1-4   -  R  R  -  R   
19 1-4   -  -  S  S  -   
20 2-4   T  -  -  T      
21 2-4   U  U  U  -      
22 2-5   V  V  V         
23 4-6   W  X  X         
24 4-6   -  Y            
25 4-6   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HAKVM LWUQO UDXLT PUOVZ XPRPY W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 0-3   -  -  -  -  -  B
03 0-3   -  -  C  -  C  C
04 0-3   -  -  D  -  -  -
05 0-3   -  E  -  E  E  E
06 0-3   F  F  -  F  F  F
07 0-3   G  -  G  G  -  -
08 0-3   H  H  H  -  -  H
09 0-3   -  -  I  I  I  -
10 0-3   -  J  -  -  J  -
11 0-3   -  -  -  -  -  -
12 0-5   L  L  -  -  L  L
13 0-5   -  M  -  -  M  M
14 0-5   N  N  -  N  -  N
15 0-5   -  O  O  O  -  -
16 0-5   P  -  P  -  -  P
17 0-5   Q  -  -  -  -  -
18 0-6   R  R  R  R  -   
19 0-6   -  S  -  S  -   
20 0-6   -  -  T  T      
21 0-6   -  -  U  -      
22 2-4   -  V  -         
23 3-5   W  X  X         
24 3-5   -  -            
25 3-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LAUGN GOPPU LGYVG BVWUO PZHBO T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   B  B  B  -  -  B
03 2-0   C  -  -  -  C  C
04 2-0   -  -  -  -  -  D
05 2-0   E  E  -  E  E  -
06 0-3   F  -  -  F  -  -
07 0-3   G  G  -  -  G  G
08 0-3   H  -  -  H  -  H
09 0-3   -  -  I  -  I  I
10 0-4   J  -  J  J  -  -
11 0-4   K  K  K  K  K  -
12 0-6   -  -  -  L  L  -
13 0-6   -  M  -  M  -  -
14 0-6   -  N  -  -  N  N
15 0-6   -  O  -  O  -  -
16 1-5   -  -  P  P  -  P
17 2-4   Q  Q  -  -  Q  Q
18 2-4   -  R  -  R  -   
19 2-5   -  S  -  -  -   
20 2-5   T  T  T  -      
21 2-6   U  -  U  U      
22 2-6   -  V  V         
23 2-6   W  -  X         
24 2-6   -  -            
25 3-4   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

RAWVN WNXVS RGOPR SINNN TRIVW W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   -  B  -  B  -  B
03 2-0   C  C  -  -  -  -
04 2-0   D  D  -  -  D  -
05 0-3   -  -  -  E  -  -
06 0-3   -  -  -  -  F  -
07 0-3   -  -  -  G  G  G
08 0-5   -  H  H  H  H  H
09 0-5   -  -  I  -  I  -
10 0-5   -  J  -  J  -  J
11 0-5   K  -  -  K  -  -
12 0-5   L  L  L  L  L  -
13 0-5   -  -  M  -  -  M
14 0-6   -  -  N  N  N  -
15 0-6   O  -  -  -  -  O
16 0-6   -  -  P  P  P  P
17 0-6   -  Q  Q  Q  -  -
18 1-2   -  R  R  -  R   
19 1-2   S  S  S  -  S   
20 1-3   T  T  -  -      
21 1-4   U  -  -  U      
22 2-6   V  V  -         
23 2-6   W  -  -         
24 2-6   -  Y            
25 2-6   -  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LAZPA ULTJJ PRJXX IQEPQ KWULA W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ER
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  B  B  -
03 1-0   -  -  C  C  C  C
04 1-0   -  D  -  -  -  -
05 1-0   E  E  E  E  E  -
06 2-0   -  -  F  -  F  F
07 2-0   G  G  -  G  -  -
08 2-0   -  -  H  -  H  -
09 2-0   I  I  -  I  I  I
10 2-0   -  J  -  J  -  J
11 2-0   -  -  -  -  -  K
12 2-0   -  -  -  -  -  L
13 2-0   -  -  M  -  M  -
14 2-0   N  -  N  -  -  -
15 2-0   -  -  O  -  O  -
16 2-0   -  P  -  P  -  P
17 0-3   -  -  -  -  -  -
18 0-4   R  -  R  R  R   
19 0-4   S  -  S  S  S   
20 0-5   T  T  -  -      
21 0-5   U  U  -  U      
22 0-5   -  -  -         
23 0-5   -  X  X         
24 1-5   X  -            
25 3-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

CTFNI WTGYN VLIIN AGWEQ QZICL F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ES
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 2-0   -  B  B  -  B  -
03 0-3   C  -  C  -  -  -
04 0-3   D  -  -  -  -  D
05 0-4   E  -  E  -  -  -
06 0-5   -  -  -  F  -  -
07 0-5   G  G  G  G  -  -
08 0-5   -  H  H  -  H  H
09 0-5   I  I  I  I  I  I
10 0-5   J  -  -  J  -  J
11 0-5   -  -  -  -  K  -
12 0-5   L  -  -  L  -  L
13 0-5   -  M  -  M  M  -
14 0-5   N  N  N  -  -  -
15 0-5   -  -  O  -  O  O
16 0-6   -  -  P  P  -  P
17 0-6   -  Q  -  -  Q  -
18 0-6   -  R  R  -  R   
19 0-6   S  -  -  S  -   
20 0-6   -  T  -  -      
21 0-6   -  -  -  U      
22 1-4   V  -  -         
23 3-4   -  X  -         
24 3-6   X  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FWERM UQJRR JDEBP XWHZC QMVEA Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ET
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   B  B  -  B  B  -
03 2-0   C  -  -  -  -  C
04 2-0   D  D  -  -  -  D
05 2-0   -  E  E  -  E  -
06 0-4   F  F  F  F  -  -
07 0-4   -  G  -  -  -  G
08 0-4   H  H  H  H  H  -
09 0-4   I  -  I  -  I  I
10 0-4   -  J  -  -  -  J
11 0-5   -  K  K  K  -  K
12 0-6   -  -  L  -  L  L
13 0-6   M  M  -  -  M  M
14 0-6   -  -  -  N  -  N
15 0-6   O  O  O  O  -  -
16 0-6   -  -  P  P  -  -
17 0-6   -  -  -  Q  Q  -
18 0-6   -  R  R  -  R   
19 0-6   -  -  -  -  S   
20 1-6   T  -  -  T      
21 2-3   -  U  U  -      
22 2-4   V  V  V         
23 3-5   -  X  -         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VVUYM BJRIU YAJMR VAUUU VHJQM S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  A  A  A
02 0-3   B  B  B  -  -  B
03 0-3   C  -  C  C  -  C
04 0-3   D  D  D  -  D  D
05 0-3   -  E  E  E  -  -
06 0-3   -  -  -  -  F  -
07 0-4   -  -  G  G  -  -
08 0-4   -  H  H  -  H  H
09 0-4   -  I  I  -  I  -
10 0-4   -  J  -  -  J  J
11 0-5   K  -  -  -  -  -
12 0-5   -  -  L  -  L  L
13 0-5   -  M  -  -  M  -
14 0-5   N  -  -  N  -  N
15 0-6   O  -  -  O  -  O
16 0-6   -  P  -  -  P  P
17 0-6   Q  -  -  Q  Q  -
18 1-2   -  R  R  R  -   
19 1-4   -  -  -  -  -   
20 1-6   T  T  T  T      
21 3-5   -  -  -  -      
22 3-6   -  V  -         
23 4-5   W  -  X         
24 4-5   -  Y            
25 4-5   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

IUJGA UHPOW HVKXO PUPNJ POOPQ P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  B  -  B  -
03 1-0   C  C  -  C  C  -
04 1-0   -  -  D  -  -  D
05 1-0   E  -  E  -  E  E
06 1-0   F  F  F  F  -  -
07 1-0   -  G  G  G  -  G
08 2-0   -  H  -  -  H  -
09 2-0   I  I  -  -  I  I
10 2-0   -  -  J  J  J  J
11 2-0   -  -  -  -  -  -
12 0-5   L  -  L  -  -  -
13 0-5   -  M  M  M  M  -
14 0-5   N  -  -  -  N  N
15 0-5   -  -  O  O  O  -
16 1-2   -  P  P  P  -  P
17 1-2   -  -  Q  -  Q  -
18 1-2   -  -  -  -  -   
19 1-2   -  S  -  S  -   
20 1-5   T  -  -  T      
21 2-3   U  U  -  U      
22 2-4   V  V  V         
23 2-5   W  -  -         
24 2-5   X  Y            
25 2-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

BSLLU UVVOS KDTWC WASNU JLVNW A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   -  -  B  -  -  B
03 1-0   -  -  -  C  -  C
04 0-3   D  -  D  -  -  D
05 0-3   -  E  -  -  -  -
06 0-3   F  -  -  F  F  -
07 0-3   G  G  G  -  G  -
08 0-3   H  H  H  -  -  -
09 0-4   -  -  I  -  -  -
10 0-4   -  -  -  J  J  J
11 0-4   K  K  K  -  K  K
12 0-4   L  L  L  L  -  -
13 0-4   M  M  -  M  M  -
14 0-4   N  N  N  N  N  -
15 0-4   -  O  -  O  -  O
16 0-4   -  P  -  -  -  -
17 0-5   Q  -  -  Q  Q  -
18 0-5   R  -  -  -  -   
19 0-5   -  S  S  S  S   
20 0-5   T  -  T  T      
21 0-6   U  U  -  -      
22 1-5   -  V  V         
23 1-6   -  X  -         
24 2-6   X  -            
25 3-4   Y  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

SUTZQ QQSSQ CPZMT AOSZZ UAPVL M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  B  -  -  -  B
03 2-0   C  -  C  -  -  C
04 0-3   D  D  D  -  D  D
05 0-3   E  E  E  E  -  -
06 0-3   F  F  F  -  -  -
07 0-3   -  G  -  G  G  G
08 0-3   H  -  H  H  H  -
09 0-3   I  I  I  -  -  -
10 0-3   J  J  -  J  -  J
11 0-4   -  -  -  K  K  K
12 0-4   -  L  -  L  -  -
13 0-4   -  M  M  -  M  -
14 0-4   -  N  N  -  N  N
15 0-4   -  -  -  O  -  O
16 1-2   -  -  P  P  -  -
17 1-2   Q  -  -  -  -  -
18 1-2   R  -  R  -  R   
19 1-4   S  -  S  -  S   
20 1-4   -  T  -  T      
21 1-4   U  -  -  U      
22 1-4   -  -  -         
23 1-5   W  -  X         
24 1-6   X  Y            
25 2-3   Y  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

KKYRK VAQRV BGVZV ATOYT TCNWP L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  A
02 2-0   -  -  -  B  B  B
03 2-0   -  -  C  C  -  -
04 2-0   -  D  -  -  -  -
05 2-0   -  E  E  -  E  E
06 0-4   -  F  -  -  -  F
07 0-4   G  -  G  -  -  -
08 0-4   -  H  -  -  H  -
09 0-6   I  -  I  -  I  -
10 0-6   -  J  -  J  J  J
11 0-6   -  K  -  -  -  -
12 0-6   -  -  -  L  -  -
13 0-6   M  -  M  M  M  M
14 0-6   N  N  N  N  N  -
15 0-6   -  O  -  O  O  -
16 1-3   P  -  -  -  -  P
17 2-3   Q  -  -  Q  Q  -
18 2-4   -  R  R  R  -   
19 2-5   -  S  S  -  -   
20 2-5   T  T  T  -      
21 2-6   U  U  -  U      
22 2-6   -  -  V         
23 2-6   W  X  -         
24 2-6   X  Y            
25 3-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

TJTOT AOMAT GQRZO OXETM SSVIX Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   B  B  B  -  B  B
03 1-0   -  -  C  -  C  C
04 0-3   D  D  -  D  -  -
05 0-3   -  E  E  E  -  E
06 0-3   F  -  -  F  F  F
07 0-3   -  G  -  -  -  -
08 0-3   -  -  -  H  H  H
09 0-3   -  I  -  I  I  I
10 0-3   J  -  J  J  -  -
11 0-3   K  -  -  K  K  -
12 0-3   L  -  L  L  -  -
13 0-3   -  M  M  -  -  -
14 0-3   -  -  N  N  N  N
15 0-4   O  -  O  -  -  -
16 0-4   P  -  -  -  P  -
17 0-4   -  -  -  -  -  Q
18 0-4   -  R  -  R  -   
19 0-4   S  -  S  -  -   
20 0-4   T  T  T  -      
21 0-4   -  U  U  -      
22 0-5   -  -  V         
23 0-6   -  X  X         
24 1-2   X  Y            
25 1-4   Y  Z            
26 2-5   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

QTBGG VOMSA CSZKZ DOKKV NSXPM F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  B  -  -  -  -
03 1-0   C  -  C  -  C  -
04 0-3   -  D  -  -  -  D
05 0-3   -  E  -  E  E  E
06 0-3   F  F  F  F  F  -
07 0-3   G  -  G  G  G  -
08 0-3   -  -  H  -  H  -
09 0-4   -  -  -  -  I  I
10 0-4   J  J  J  -  J  J
11 0-4   -  K  -  -  -  K
12 0-5   -  L  -  L  L  L
13 0-5   M  M  M  -  -  -
14 0-5   N  -  N  N  -  -
15 0-5   -  O  -  -  -  O
16 0-5   -  -  P  P  -  -
17 0-5   Q  -  Q  Q  Q  Q
18 1-2   R  R  -  R  -   
19 1-3   S  -  S  S  -   
20 1-5   -  T  T  -      
21 2-5   U  -  -  -      
22 3-4   -  -  -         
23 4-5   W  X  -         
24 4-5   -  Y            
25 4-5   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UOLTI AOFNB TGUVR XXXAT OCPNV P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  B  B  -
03 1-0   C  -  C  C  C  C
04 2-0   -  D  -  -  -  D
05 0-3   E  E  E  E  E  -
06 0-4   F  -  F  F  -  -
07 0-4   G  G  -  -  -  G
08 0-4   -  H  H  -  -  -
09 0-4   I  -  I  -  I  -
10 0-4   J  J  J  J  -  J
11 0-5   K  K  K  K  -  -
12 0-5   -  L  -  L  L  L
13 0-5   -  -  -  M  M  -
14 0-5   N  N  -  -  -  N
15 0-5   -  -  -  O  -  -
16 0-5   P  -  -  -  -  P
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  -  R  -  R   
19 0-5   -  S  S  S  S   
20 0-5   -  -  -  T      
21 0-5   U  -  U  -      
22 0-6   V  V  -         
23 0-6   -  X  -         
24 1-4   -  -            
25 1-6   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VTHEJ UYXMP WMNNJ HTHMX ALEKU R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  B  -  B  -  B
03 2-0   -  -  C  -  C  C
04 2-0   -  -  D  D  -  -
05 2-0   -  -  -  -  -  E
06 2-0   F  -  F  -  F  F
07 2-0   G  G  G  -  G  G
08 2-0   H  H  H  -  -  -
09 2-0   I  -  I  I  -  I
10 0-4   J  -  J  -  -  J
11 0-4   K  K  -  K  K  -
12 0-4   -  L  -  -  -  -
13 0-4   M  -  -  M  M  M
14 0-4   -  N  -  N  -  -
15 0-6   O  O  O  O  O  O
16 0-6   -  -  -  -  P  P
17 0-6   -  Q  -  -  Q  -
18 1-3   R  R  -  R  -   
19 1-6   -  -  -  -  -   
20 2-3   T  T  T  T      
21 2-3   -  U  U  -      
22 2-4   V  V  V         
23 2-4   W  -  X         
24 2-4   -  Y            
25 2-4   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SNZNI XRXIA UHYIA IKXRT EZXOO U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  A
02 2-0   -  B  B  B  -  B
03 2-0   -  C  -  -  -  C
04 2-0   -  -  -  -  -  -
05 2-0   E  -  E  E  E  -
06 2-0   F  F  F  F  -  -
07 0-3   -  G  -  -  G  G
08 0-3   -  -  -  -  H  H
09 0-3   -  I  I  -  I  -
10 0-3   J  -  -  J  J  J
11 0-3   -  -  K  -  K  K
12 0-3   L  -  L  L  -  L
13 0-5   -  -  -  M  -  -
14 0-5   N  N  -  -  -  -
15 0-5   -  O  -  -  O  -
16 1-5   -  P  P  P  -  P
17 2-3   Q  -  Q  Q  Q  -
18 2-5   R  -  -  R  R   
19 2-5   S  S  S  -  S   
20 2-5   T  -  T  T      
21 2-5   -  U  U  U      
22 3-4   V  V  V         
23 4-5   -  -  X         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QMIUR UZAMJ GHSQU TTTUK NMMZU A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  -  -  B  -  B
03 1-0   -  C  -  -  C  -
04 1-0   D  D  D  -  D  D
05 1-0   E  -  E  E  -  -
06 1-0   -  -  -  -  -  -
07 0-3   G  -  -  G  G  G
08 0-3   H  H  -  H  -  H
09 0-3   -  I  I  I  I  I
10 0-6   J  -  -  J  J  -
11 0-6   K  K  K  -  K  -
12 0-6   L  -  L  L  L  L
13 0-6   -  -  M  -  -  M
14 0-6   N  N  N  N  N  -
15 0-6   O  O  -  -  O  -
16 1-2   -  -  P  -  P  P
17 1-3   -  -  Q  Q  -  Q
18 1-3   -  -  R  -  -   
19 1-6   -  -  -  -  S   
20 2-3   T  -  -  T      
21 2-5   -  U  -  -      
22 3-4   V  -  -         
23 3-5   W  -  X         
24 3-6   -  Y            
25 3-6   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

NLTMU KVWKN WMZUA UCMAO TUATV A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 2-0   B  B  -  -  -  -
03 2-0   C  -  -  C  -  -
04 2-0   -  D  D  -  -  -
05 2-0   E  -  E  -  -  E
06 2-0   -  -  -  F  F  F
07 2-0   G  G  G  G  G  G
08 0-3   H  -  H  -  H  -
09 0-3   -  I  -  -  -  -
10 0-4   J  J  -  -  J  J
11 0-4   -  -  -  -  K  K
12 0-4   -  -  -  L  -  -
13 0-4   M  -  -  M  M  M
14 0-4   -  N  -  -  N  N
15 0-4   -  O  O  -  -  O
16 0-4   P  P  P  -  -  -
17 0-4   Q  -  Q  -  -  Q
18 0-4   -  -  -  R  R   
19 0-4   S  -  -  S  -   
20 0-5   T  -  -  T      
21 0-6   -  U  U  U      
22 1-3   V  -  V         
23 1-5   W  -  -         
24 2-3   -  Y            
25 2-4   Y  Z            
26 2-4   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

XPGGD MYTQW MXNHY QEQDK YPZDN O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  B  B  B  B  B
03 1-0   -  -  C  -  C  -
04 1-0   D  D  -  -  D  D
05 1-0   E  E  E  E  -  -
06 1-0   F  -  F  -  F  F
07 1-0   -  -  -  G  -  -
08 1-0   -  -  -  -  -  -
09 1-0   -  -  I  I  -  I
10 1-0   J  -  -  -  -  -
11 2-0   K  K  -  -  K  K
12 2-0   -  -  L  L  -  L
13 2-0   M  -  -  M  -  M
14 2-0   -  -  -  -  -  N
15 0-3   -  O  O  -  O  -
16 0-4   -  P  P  -  -  -
17 0-4   Q  Q  -  -  -  Q
18 0-5   -  -  R  R  -   
19 0-5   -  -  -  S  S   
20 0-5   -  -  T  T      
21 0-6   -  U  U  -      
22 1-5   -  -  -         
23 1-5   -  X  -         
24 1-5   X  Y            
25 2-4   Y  Z            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

JVIIA DKLXV QWVKT XMEJB RWGOA I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 2-0   B  B  B  B  -  -
03 2-0   -  -  -  -  -  -
04 2-0   -  -  D  -  -  -
05 2-0   E  E  E  -  -  E
06 2-0   F  -  -  -  F  -
07 2-0   -  -  -  G  -  -
08 2-0   -  H  -  H  H  H
09 2-0   -  I  I  I  -  I
10 2-0   -  -  J  J  J  J
11 2-0   -  -  -  -  K  -
12 0-4   -  -  L  -  L  L
13 0-4   M  -  M  M  -  -
14 0-5   -  N  -  -  N  N
15 0-5   O  O  -  -  O  -
16 0-5   P  -  P  P  -  P
17 0-5   -  Q  Q  Q  Q  -
18 0-5   R  R  -  R  R   
19 0-5   S  S  -  -  S   
20 0-6   T  T  T  -      
21 0-6   U  -  -  -      
22 0-6   -  V  -         
23 0-6   W  -  -         
24 2-5   X  Y            
25 3-4   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WFTAP AISAL PHLIG AJLZS FLNAV W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  -  B  -  B  B
03 1-0   C  C  -  -  -  -
04 1-0   D  -  -  -  D  D
05 2-0   E  E  E  E  E  -
06 2-0   F  F  F  F  F  F
07 2-0   -  G  -  -  -  G
08 2-0   -  -  H  H  -  -
09 0-3   -  I  I  I  I  I
10 0-3   J  J  -  J  J  -
11 0-3   -  K  -  K  -  -
12 0-3   -  -  L  L  L  L
13 0-3   M  -  -  -  -  -
14 0-4   -  -  N  -  N  N
15 0-4   O  -  -  -  O  O
16 0-4   P  P  -  P  -  P
17 0-6   Q  Q  Q  -  Q  Q
18 1-2   -  R  R  R  -   
19 1-6   S  -  -  S  -   
20 2-4   -  -  -  -      
21 3-4   U  -  -  -      
22 3-4   -  V  -         
23 3-4   W  -  -         
24 3-4   X  -            
25 3-6   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ROGAN RMKJI NREVU XSURN VTESI J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 2-0   -  -  B  -  B  B
03 2-0   -  C  -  C  -  -
04 2-0   -  D  -  -  -  D
05 2-0   E  -  -  -  -  E
06 2-0   F  -  F  F  F  F
07 0-3   -  G  G  -  -  -
08 0-3   H  H  -  H  H  -
09 0-3   -  I  -  I  -  -
10 0-3   -  J  -  -  -  -
11 0-3   -  -  -  -  -  K
12 0-3   L  -  L  L  -  L
13 0-3   -  M  M  -  M  -
14 0-3   N  -  N  -  N  -
15 0-4   -  -  O  O  O  O
16 0-4   -  P  P  -  P  P
17 0-4   -  Q  -  Q  Q  Q
18 0-4   -  R  R  R  -   
19 0-4   S  S  -  S  -   
20 0-4   T  T  -  -      
21 0-4   U  -  U  -      
22 0-5   V  V  V         
23 0-5   -  -  X         
24 1-5   X  -            
25 1-6   -  Z            
26 2-4   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

ISYLA SKPKT RWAJN NSOJG QMNZN Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  -  B  -  -  B
03 1-0   -  -  C  -  -  -
04 1-0   D  D  -  D  -  D
05 1-0   -  E  -  -  E  E
06 1-0   -  -  -  -  F  F
07 0-3   G  G  G  -  -  -
08 0-4   -  -  -  H  -  -
09 0-4   I  -  -  I  I  -
10 0-4   J  -  -  -  J  J
11 0-5   -  -  K  -  K  K
12 0-5   L  L  L  L  L  -
13 0-5   M  M  M  -  M  -
14 0-5   -  N  N  N  -  N
15 0-5   -  -  O  O  -  -
16 0-5   -  -  -  P  P  P
17 0-5   Q  Q  Q  -  Q  -
18 0-6   -  -  -  R  -   
19 0-6   S  S  -  S  -   
20 1-4   T  T  T  T      
21 1-5   -  -  -  -      
22 1-5   V  V  -         
23 1-5   W  -  -         
24 1-5   X  Y            
25 2-5   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QYFSS TQPOA EPALL AUZLS ABTYS D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 2-0   -  -  B  -  B  B
03 0-3   -  -  C  C  -  -
04 0-3   D  -  D  -  -  -
05 0-3   -  E  -  -  E  E
06 0-3   F  -  -  F  -  -
07 0-3   G  G  G  G  -  G
08 0-4   -  H  -  -  H  H
09 0-4   I  I  I  I  I  -
10 0-4   J  -  J  J  -  -
11 0-4   -  K  -  K  K  -
12 0-4   L  L  L  L  -  -
13 0-4   -  -  -  -  M  -
14 0-5   -  -  N  N  N  N
15 0-6   -  O  O  O  -  O
16 0-6   P  P  P  -  P  P
17 0-6   Q  -  -  -  -  -
18 0-6   R  R  R  -  -   
19 0-6   -  S  -  -  S   
20 0-6   -  T  -  T      
21 0-6   U  -  U  U      
22 0-6   V  -  -         
23 0-6   W  X  X         
24 2-3   X  -            
25 2-5   -  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JRQMT NAQVI AYIQR LJLQQ TTUPG S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  -  B  B  -  -
03 1-0   C  -  -  C  C  C
04 1-0   D  -  -  -  D  -
05 1-0   -  E  -  -  E  E
06 1-0   F  -  F  F  -  -
07 0-3   G  G  G  G  G  G
08 0-3   H  H  -  -  -  H
09 0-3   -  I  I  -  I  I
10 0-3   J  J  -  -  -  J
11 0-3   -  -  K  -  K  -
12 0-3   -  -  -  -  -  L
13 0-3   -  M  -  M  -  M
14 0-4   N  -  N  -  -  -
15 0-4   -  -  -  O  O  -
16 0-5   P  -  -  P  -  P
17 0-5   -  Q  Q  -  Q  -
18 0-5   R  R  R  -  R   
19 0-6   -  S  S  S  S   
20 1-4   T  T  -  T      
21 1-5   U  -  -  -      
22 2-6   V  V  V         
23 3-5   -  X  X         
24 3-5   -  Y            
25 3-5   -  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VCMTT YRNPN MAHKT QYIYC TDKPT H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  -  B  B  -
03 1-0   C  C  C  -  -  C
04 1-0   D  D  D  -  -  D
05 1-0   -  E  -  -  E  -
06 0-3   F  -  -  F  F  -
07 0-3   G  -  G  -  -  G
08 0-3   -  H  H  H  -  H
09 0-4   -  -  I  I  I  -
10 0-4   J  J  J  J  -  -
11 0-4   -  -  -  K  -  -
12 0-5   L  -  L  -  -  L
13 0-5   -  M  M  -  M  -
14 0-5   N  -  -  N  N  N
15 0-5   O  O  -  O  O  O
16 0-5   -  P  P  -  P  P
17 0-5   -  -  -  -  -  Q
18 1-4   -  -  -  -  R   
19 1-5   -  -  -  -  S   
20 1-5   T  T  T  -      
21 1-5   -  U  U  -      
22 1-5   V  V  -         
23 2-3   W  -  -         
24 2-5   X  -            
25 2-5   Y  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

DSUMS DXXRW JNXXR DZRLI TRASK X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JUL-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  -  -  -  -  -
04 1-0   -  D  D  -  -  D
05 1-0   E  -  E  -  E  -
06 1-0   -  F  F  F  -  F
07 1-0   G  -  G  -  -  -
08 1-0   -  H  H  H  H  H
09 1-0   I  I  -  I  I  I
10 1-0   J  -  J  J  J  -
11 2-0   K  K  K  K  K  -
12 2-0   -  L  L  -  -  L
13 2-0   -  -  M  M  M  M
14 0-3   N  -  -  -  -  -
15 0-3   O  -  -  -  -  O
16 0-3   P  -  -  -  -  -
17 0-3   Q  Q  Q  Q  -  Q
18 0-3   R  R  R  R  R   
19 0-3   -  S  S  S  S   
20 0-3   -  T  -  -      
21 0-3   U  U  -  -      
22 0-3   V  -  -         
23 0-4   -  -  -         
24 0-5   -  -            
25 0-6   Y  Z            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

LNOMP YRDCL GSGSP WPWHZ VZMAK N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
