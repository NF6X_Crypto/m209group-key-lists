EFFECTIVE PERIOD:
25-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  -
02 2-0   B  B  -  -  -  B
03 2-0   C  -  -  C  C  C
04 2-0   D  D  D  D  -  D
05 2-0   -  E  -  -  E  E
06 2-0   -  -  F  -  F  F
07 2-0   -  -  -  G  G  -
08 0-3   H  H  -  -  H  H
09 0-3   I  I  -  -  -  I
10 0-3   -  J  J  -  J  -
11 0-3   K  K  K  K  -  -
12 0-3   -  L  L  L  -  -
13 0-6   -  M  -  M  M  M
14 0-6   -  -  -  N  -  -
15 0-6   O  -  O  O  -  O
16 1-2   -  -  P  P  P  P
17 1-5   -  -  Q  -  -  -
18 1-6   -  R  -  R  -   
19 1-6   S  -  S  -  -   
20 1-6   T  -  -  T      
21 1-6   -  U  U  -      
22 2-3   -  V  V         
23 3-6   -  X  -         
24 3-6   X  -            
25 3-6   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ALRXN RISAN MVVPV AZANA TAUKK T
-------------------------------
