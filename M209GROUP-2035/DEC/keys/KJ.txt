EFFECTIVE PERIOD:
03-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  A
02 2-0   -  -  B  -  B  B
03 2-0   -  -  -  C  -  -
04 2-0   D  D  D  D  D  -
05 2-0   -  -  E  E  -  -
06 2-0   -  -  -  F  -  -
07 2-0   G  G  -  -  -  G
08 0-3   -  -  -  H  H  -
09 0-3   I  I  I  -  -  -
10 0-3   J  J  -  -  J  -
11 0-3   K  -  K  K  K  K
12 0-3   -  L  -  -  -  L
13 0-4   -  M  -  M  M  M
14 0-4   N  -  N  -  N  N
15 0-4   -  -  -  -  O  -
16 1-4   P  P  -  P  P  -
17 2-3   -  Q  Q  -  -  -
18 2-6   -  -  R  -  R   
19 3-4   -  -  S  -  -   
20 3-4   T  -  -  -      
21 3-4   U  -  -  U      
22 3-4   V  -  -         
23 4-5   -  X  -         
24 4-6   -  -            
25 4-6   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TNNTM KCEVB SALTZ BLOPV XMKTN F
-------------------------------
