EFFECTIVE PERIOD:
24-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  -
02 2-0   B  -  -  B  B  B
03 2-0   -  -  -  C  C  -
04 0-3   D  D  D  -  -  -
05 0-3   -  -  E  E  -  E
06 0-3   F  -  -  -  F  -
07 0-3   G  -  -  -  G  G
08 0-4   -  H  -  -  -  H
09 0-5   -  I  I  -  -  -
10 0-5   -  J  -  J  -  J
11 0-5   -  K  K  -  -  K
12 0-5   -  L  L  L  L  L
13 0-5   -  -  M  -  M  M
14 0-5   N  -  N  -  N  -
15 0-5   O  -  -  O  O  -
16 0-5   P  P  -  -  -  P
17 0-6   -  Q  -  Q  -  -
18 0-6   R  R  -  -  R   
19 0-6   S  -  -  S  S   
20 1-4   -  -  -  -      
21 2-3   U  U  U  U      
22 2-5   -  -  V         
23 2-5   W  -  X         
24 2-5   X  -            
25 2-5   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WUWQC FSSLU SOAXZ ZFFSU XUYIA S
-------------------------------
