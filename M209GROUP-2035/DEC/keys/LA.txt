EFFECTIVE PERIOD:
20-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  -  -  -  B  B
03 1-0   -  C  C  -  -  C
04 1-0   D  D  D  -  D  -
05 1-0   E  E  E  E  -  -
06 1-0   -  F  F  -  -  -
07 1-0   G  -  G  G  G  -
08 1-0   -  H  -  -  H  H
09 2-0   I  I  I  -  -  -
10 2-0   -  J  J  J  -  -
11 2-0   -  -  K  -  K  K
12 2-0   L  L  -  -  -  L
13 0-3   -  M  -  -  M  M
14 0-3   -  N  N  N  N  -
15 0-3   O  O  O  O  O  O
16 0-3   -  -  P  -  -  P
17 0-3   -  Q  -  Q  -  Q
18 0-3   -  -  -  R  R   
19 0-3   S  -  S  -  S   
20 0-3   -  T  T  T      
21 0-3   U  -  -  -      
22 0-3   -  V  -         
23 0-5   -  -  -         
24 0-5   -  -            
25 0-6   Y  -            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

POSLB YOHEM ZPRPA ZOISX REQQO A
-------------------------------
