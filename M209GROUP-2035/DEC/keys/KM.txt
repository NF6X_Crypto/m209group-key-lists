EFFECTIVE PERIOD:
06-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  -  -  B  -  B
03 1-0   C  -  -  -  C  -
04 2-0   -  -  -  D  -  D
05 0-3   -  E  -  E  E  E
06 0-3   -  F  -  F  -  -
07 0-3   -  -  G  -  -  G
08 0-3   -  -  -  H  H  -
09 0-3   -  I  -  -  I  -
10 0-3   J  J  -  -  J  -
11 0-3   K  K  K  K  K  K
12 0-4   -  -  L  -  L  L
13 0-6   M  M  -  M  -  M
14 0-6   N  -  -  N  N  N
15 0-6   -  -  O  -  O  -
16 0-6   -  P  P  P  -  -
17 0-6   Q  -  Q  -  Q  Q
18 0-6   -  -  R  R  R   
19 0-6   S  -  S  -  -   
20 1-2   T  -  -  -      
21 1-6   U  -  -  U      
22 2-4   V  V  V         
23 3-5   -  -  -         
24 3-6   X  Y            
25 3-6   Y  Z            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

OATZQ SQOZG XLCRB DPMMQ ZTCRZ T
-------------------------------
