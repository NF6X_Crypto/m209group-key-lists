EFFECTIVE PERIOD:
27-DEC-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  B  -  B  -
03 1-0   -  C  C  C  C  C
04 1-0   D  D  D  -  D  -
05 1-0   E  -  -  E  -  -
06 1-0   F  -  F  -  -  -
07 1-0   -  -  -  G  G  G
08 1-0   H  H  H  -  H  H
09 1-0   -  -  -  I  I  -
10 2-0   -  -  -  J  J  -
11 0-4   K  -  -  K  -  K
12 0-4   L  -  -  -  -  L
13 0-4   -  -  -  -  M  -
14 0-4   N  -  -  -  -  N
15 0-4   O  O  O  O  -  O
16 0-4   -  P  P  -  P  -
17 0-4   -  -  Q  -  -  -
18 0-4   -  -  R  -  -   
19 0-4   S  S  -  -  -   
20 0-4   T  T  T  T      
21 0-5   U  U  U  U      
22 0-6   -  -  -         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

FHLOQ JCYVA CVBQQ VKQYJ GNILO L
-------------------------------
