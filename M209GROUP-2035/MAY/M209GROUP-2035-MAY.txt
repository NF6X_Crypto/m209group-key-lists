SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF MAY 2035

    01 MAY 2035  00:00-23:59 GMT:  USE KEY CB
    02 MAY 2035  00:00-23:59 GMT:  USE KEY CC
    03 MAY 2035  00:00-23:59 GMT:  USE KEY CD
    04 MAY 2035  00:00-23:59 GMT:  USE KEY CE
    05 MAY 2035  00:00-23:59 GMT:  USE KEY CF
    06 MAY 2035  00:00-23:59 GMT:  USE KEY CG
    07 MAY 2035  00:00-23:59 GMT:  USE KEY CH
    08 MAY 2035  00:00-23:59 GMT:  USE KEY CI
    09 MAY 2035  00:00-23:59 GMT:  USE KEY CJ
    10 MAY 2035  00:00-23:59 GMT:  USE KEY CK
    11 MAY 2035  00:00-23:59 GMT:  USE KEY CL
    12 MAY 2035  00:00-23:59 GMT:  USE KEY CM
    13 MAY 2035  00:00-23:59 GMT:  USE KEY CN
    14 MAY 2035  00:00-23:59 GMT:  USE KEY CO
    15 MAY 2035  00:00-23:59 GMT:  USE KEY CP
    16 MAY 2035  00:00-23:59 GMT:  USE KEY CQ
    17 MAY 2035  00:00-23:59 GMT:  USE KEY CR
    18 MAY 2035  00:00-23:59 GMT:  USE KEY CS
    19 MAY 2035  00:00-23:59 GMT:  USE KEY CT
    20 MAY 2035  00:00-23:59 GMT:  USE KEY CU
    21 MAY 2035  00:00-23:59 GMT:  USE KEY CV
    22 MAY 2035  00:00-23:59 GMT:  USE KEY CW
    23 MAY 2035  00:00-23:59 GMT:  USE KEY CX
    24 MAY 2035  00:00-23:59 GMT:  USE KEY CY
    25 MAY 2035  00:00-23:59 GMT:  USE KEY CZ
    26 MAY 2035  00:00-23:59 GMT:  USE KEY DA
    27 MAY 2035  00:00-23:59 GMT:  USE KEY DB
    28 MAY 2035  00:00-23:59 GMT:  USE KEY DC
    29 MAY 2035  00:00-23:59 GMT:  USE KEY DD
    30 MAY 2035  00:00-23:59 GMT:  USE KEY DE
    31 MAY 2035  00:00-23:59 GMT:  USE KEY DF

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   -  -  -  C  -  C
04 2-0   D  D  -  D  -  -
05 2-0   E  -  -  -  E  E
06 0-3   -  -  F  -  -  F
07 0-4   -  -  G  G  G  -
08 0-4   -  H  -  H  H  -
09 0-4   -  I  -  I  -  I
10 0-4   -  -  J  J  -  -
11 0-4   -  -  -  K  -  -
12 0-4   L  L  -  -  L  L
13 0-4   M  -  M  -  M  M
14 0-4   N  -  -  -  N  N
15 0-6   -  O  O  -  -  O
16 0-6   P  P  -  -  -  -
17 0-6   -  -  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   S  S  -  -  S   
20 1-4   -  -  T  -      
21 2-5   -  U  -  -      
22 2-6   V  -  V         
23 3-5   -  X  X         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

IRLQA MSBJM TUZZI VOMYA LLJKC T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  -  B  -  -  B
03 1-0   -  C  -  C  C  C
04 1-0   D  -  D  -  D  -
05 1-0   -  -  -  E  -  E
06 2-0   -  F  F  F  F  -
07 0-3   -  -  -  -  G  G
08 0-3   H  -  -  -  -  H
09 0-3   -  -  -  -  I  -
10 0-3   J  J  J  -  J  J
11 0-3   K  -  K  -  K  K
12 0-3   L  -  L  L  L  -
13 0-3   M  -  -  M  -  M
14 0-3   -  -  N  -  -  N
15 0-3   O  O  O  O  -  -
16 0-4   -  -  P  -  -  P
17 0-6   Q  Q  Q  Q  Q  -
18 0-6   -  R  R  -  -   
19 0-6   -  -  -  S  -   
20 0-6   -  T  -  T      
21 0-6   U  U  U  -      
22 0-6   -  V  V         
23 0-6   W  X  -         
24 1-5   X  -            
25 1-6   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VQUSL QCTYQ HZGVQ JLKHT ASOSL A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  -  B  B  -  -
03 1-0   C  -  C  -  -  C
04 1-0   -  D  D  D  -  D
05 1-0   -  E  E  E  -  E
06 1-0   F  F  F  -  F  -
07 2-0   -  -  -  G  -  -
08 2-0   H  H  H  -  -  H
09 2-0   I  I  -  -  -  I
10 0-3   -  -  -  -  J  J
11 0-3   K  -  K  K  K  K
12 0-3   -  L  L  L  -  -
13 0-3   M  M  M  -  -  -
14 0-3   -  -  -  -  -  -
15 0-4   O  O  -  -  -  O
16 1-3   -  P  -  -  P  P
17 1-4   Q  -  -  Q  Q  Q
18 2-3   -  -  -  R  R   
19 2-3   -  S  -  -  S   
20 2-3   T  -  T  T      
21 2-3   -  -  U  -      
22 2-4   -  V  -         
23 2-4   -  X  -         
24 2-6   -  -            
25 2-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MSZZV HNULL OSUVS UUNTU OUSUA V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  B  -  -  -  B
03 1-0   -  -  C  -  -  -
04 1-0   D  D  D  D  D  -
05 1-0   E  -  E  E  -  -
06 1-0   F  F  F  F  -  -
07 0-3   G  -  G  G  G  G
08 0-3   H  H  -  -  H  H
09 0-3   -  -  -  I  -  -
10 0-3   -  -  -  -  -  -
11 0-3   K  K  K  K  K  K
12 0-5   -  -  -  -  L  L
13 0-5   -  -  M  M  -  M
14 0-6   -  N  -  N  N  N
15 0-6   -  O  O  -  O  -
16 0-6   P  P  -  -  -  P
17 0-6   -  -  -  Q  Q  -
18 1-3   -  R  -  R  -   
19 1-5   -  -  -  -  S   
20 2-6   -  T  T  T      
21 3-6   U  U  U  U      
22 3-6   V  V  -         
23 3-6   -  X  -         
24 3-6   -  Y            
25 4-5   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VROOA VRUOI RMLRA VRTVO QNUZU L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  A  -  A
02 0-4   B  B  -  -  B  B
03 0-4   C  -  -  -  C  C
04 0-4   D  D  -  -  -  -
05 0-4   E  E  E  E  E  E
06 0-4   -  F  F  -  F  -
07 0-4   G  -  -  G  G  G
08 0-4   H  -  H  -  -  H
09 0-4   I  -  I  I  -  I
10 0-5   J  J  -  J  J  J
11 0-5   K  K  -  K  -  -
12 0-5   L  -  L  L  -  -
13 0-5   -  -  -  -  -  -
14 0-5   N  N  N  -  N  -
15 0-5   -  O  O  -  O  -
16 0-6   -  P  P  P  -  -
17 0-6   -  -  Q  -  -  Q
18 0-6   -  R  R  -  R   
19 0-6   -  S  -  -  S   
20 0-6   T  -  -  T      
21 0-6   -  U  -  -      
22 0-6   V  V  V         
23 0-6   -  -  X         
24 0-6   X  -            
25 0-6   Y  Z            
26 1-2   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

BBHQC ZRZMA HKHZS UKQKJ JIHKU S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  B  B  -  -  B
03 2-0   -  -  -  -  C  -
04 2-0   -  -  -  D  -  D
05 2-0   E  -  -  E  E  E
06 2-0   F  -  F  -  -  F
07 2-0   -  -  G  G  G  G
08 0-4   H  H  H  -  H  -
09 0-4   I  I  -  -  I  -
10 0-4   J  -  -  J  -  -
11 0-4   K  K  K  K  -  -
12 0-5   -  L  -  -  L  -
13 0-6   -  -  M  -  M  M
14 0-6   N  N  N  N  N  -
15 0-6   -  O  -  O  -  O
16 0-6   P  P  -  P  -  P
17 0-6   Q  Q  Q  -  Q  -
18 0-6   R  -  -  -  R   
19 0-6   -  -  -  S  -   
20 0-6   T  -  T  -      
21 0-6   -  U  -  -      
22 1-4   -  -  -         
23 1-5   W  X  X         
24 2-4   -  -            
25 2-6   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

NRAGO NPDTA WRAQC KFAPA FWWGR T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  B  B  -  -  B
03 1-0   -  C  -  -  C  C
04 1-0   D  D  -  D  -  -
05 1-0   -  -  E  E  E  -
06 1-0   -  -  F  -  F  -
07 1-0   G  -  G  -  G  G
08 2-0   H  -  H  -  -  H
09 2-0   -  I  I  I  I  I
10 2-0   J  J  -  -  J  J
11 2-0   K  K  K  K  K  K
12 2-0   L  L  -  -  L  -
13 2-0   -  -  M  M  -  -
14 0-3   N  -  N  -  -  -
15 0-3   -  O  -  O  O  O
16 0-3   P  -  -  P  P  P
17 0-3   -  -  -  Q  -  Q
18 0-3   -  R  R  -  -   
19 0-3   S  S  -  S  -   
20 0-3   T  T  T  T      
21 0-3   -  -  -  -      
22 0-4   V  V  V         
23 0-5   W  -  -         
24 0-5   -  Y            
25 0-5   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AZCSA GRPMN RNSLZ IMUSL UPZNJ I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  -  A  A
02 0-3   B  B  -  B  B  -
03 0-3   -  -  C  C  -  -
04 0-3   D  -  D  -  D  D
05 0-3   E  E  -  -  -  E
06 0-5   -  -  F  F  F  F
07 0-5   -  G  -  G  G  -
08 0-5   H  -  -  H  -  -
09 0-5   I  I  -  -  -  I
10 0-5   -  J  -  J  -  -
11 0-6   K  K  K  K  K  -
12 0-6   -  L  L  -  -  L
13 0-6   M  M  M  -  M  M
14 0-6   -  -  N  N  N  N
15 0-6   O  -  -  O  -  O
16 0-6   -  -  P  -  P  -
17 0-6   Q  Q  -  Q  -  Q
18 1-5   R  R  -  -  -   
19 2-4   S  -  -  -  -   
20 2-5   T  T  T  -      
21 2-6   -  U  U  U      
22 3-5   -  -  -         
23 3-5   -  X  X         
24 3-5   X  Y            
25 3-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JVALV CTRNT LVURM VBCVM AANAI M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   -  B  -  B  B  B
03 1-0   -  C  -  C  -  -
04 1-0   -  D  -  D  D  D
05 1-0   -  E  -  -  -  -
06 1-0   F  -  F  F  F  F
07 1-0   -  G  -  G  G  G
08 2-0   H  H  -  -  -  H
09 2-0   -  I  I  I  -  -
10 2-0   -  -  J  J  J  J
11 2-0   K  K  -  -  K  -
12 2-0   L  -  L  -  -  -
13 2-0   M  -  M  M  -  M
14 2-0   -  -  -  -  -  -
15 2-0   -  O  O  O  -  O
16 0-3   P  -  -  -  P  P
17 0-5   Q  -  -  -  Q  Q
18 0-6   R  R  R  -  -   
19 0-6   -  S  -  S  S   
20 1-2   -  -  T  T      
21 1-2   -  -  U  -      
22 1-2   -  V  V         
23 1-2   W  -  -         
24 1-6   X  Y            
25 2-4   Y  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UNPMT FGYRX PDQSS OOPYS ZZQRD Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   -  A  A  A  A  A
02 0-4   -  B  B  -  -  -
03 0-4   C  C  -  -  C  C
04 0-4   D  -  D  -  -  D
05 0-4   E  E  -  E  E  -
06 0-4   F  -  F  F  -  F
07 0-5   G  -  G  -  -  G
08 0-5   -  H  -  -  -  -
09 0-5   -  -  I  I  -  I
10 0-6   -  -  J  J  -  -
11 0-6   -  K  -  K  K  -
12 0-6   L  L  L  -  L  L
13 0-6   -  -  -  -  -  -
14 0-6   -  N  -  -  N  -
15 0-6   O  O  O  O  -  O
16 1-5   -  -  -  P  P  -
17 2-3   Q  Q  Q  -  Q  -
18 2-4   -  R  -  -  -   
19 2-5   S  S  -  S  -   
20 2-5   -  T  -  -      
21 3-5   U  -  -  U      
22 4-5   -  -  -         
23 4-6   -  -  X         
24 5-6   X  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NNUNU NIKTV UEUXJ QCJAU ZUETA E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  B  -  B  B  B
03 1-0   -  -  C  -  C  C
04 1-0   -  -  D  -  -  -
05 2-0   -  E  -  -  E  -
06 2-0   F  F  -  F  F  -
07 2-0   G  G  G  -  -  -
08 2-0   -  -  -  -  H  H
09 0-3   I  I  I  -  I  -
10 0-4   -  -  -  -  -  J
11 0-4   -  K  -  K  -  K
12 0-4   -  -  L  L  L  -
13 0-4   M  M  M  M  -  -
14 0-4   -  -  N  -  -  N
15 0-4   O  O  O  -  O  -
16 0-6   -  P  -  -  -  P
17 0-6   Q  -  -  Q  -  Q
18 0-6   R  -  -  R  R   
19 0-6   S  -  -  -  -   
20 1-2   -  -  -  T      
21 1-6   U  -  U  U      
22 2-3   -  V  V         
23 4-5   -  X  X         
24 4-6   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WLFOK UJWAW RKHCP GUSBV PRLTK Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  -  -  B  B
03 1-0   -  C  -  -  C  C
04 1-0   D  -  D  -  -  D
05 1-0   -  -  E  -  E  -
06 0-3   -  -  F  F  -  F
07 0-3   G  G  -  -  G  -
08 0-3   -  H  -  -  -  -
09 0-3   I  I  I  -  I  I
10 0-3   -  -  -  J  -  J
11 0-6   K  K  -  K  K  -
12 0-6   -  -  -  -  -  L
13 0-6   -  -  M  M  M  -
14 0-6   -  N  N  N  -  -
15 0-6   O  -  -  O  -  -
16 1-3   P  P  P  P  -  -
17 1-4   Q  -  -  Q  Q  Q
18 1-6   R  R  -  R  -   
19 2-5   -  S  S  -  S   
20 3-4   -  -  -  T      
21 3-4   -  -  U  U      
22 3-5   V  -  V         
23 3-6   W  X  -         
24 3-6   X  Y            
25 3-6   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UVUEE PQAAM TIMNV SMIVL CLOUP Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  A
02 2-0   -  -  B  B  -  B
03 0-3   -  C  -  -  C  C
04 0-4   -  D  D  -  -  D
05 0-4   -  E  E  -  E  E
06 0-4   -  -  -  F  F  F
07 0-4   -  -  -  G  G  -
08 0-4   H  H  -  -  H  -
09 0-4   I  -  -  I  -  -
10 0-4   -  -  J  J  J  -
11 0-4   -  K  -  K  -  -
12 0-5   L  -  -  L  L  L
13 0-5   M  -  -  M  -  M
14 0-5   -  N  N  -  -  -
15 0-6   O  -  O  -  O  -
16 0-6   P  -  P  -  P  P
17 0-6   Q  Q  Q  -  -  -
18 0-6   R  R  -  R  R   
19 0-6   S  S  S  -  -   
20 1-4   T  T  T  T      
21 2-3   -  U  -  -      
22 2-6   V  -  -         
23 4-5   W  -  X         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SJLNM XNTOI ZJNMV UZQWY MQOKK Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   -  -  -  B  B  -
03 1-0   -  C  -  -  C  -
04 2-0   -  -  D  D  -  -
05 0-3   -  -  E  -  E  -
06 0-5   F  F  F  F  F  F
07 0-5   G  -  G  -  -  G
08 0-5   H  H  -  H  -  -
09 0-5   -  -  I  I  -  I
10 0-5   -  J  J  -  J  -
11 0-5   K  K  K  -  K  K
12 0-5   L  L  -  L  L  -
13 0-5   -  M  -  M  -  M
14 0-5   N  -  -  N  -  -
15 0-5   O  O  -  -  O  O
16 0-6   P  P  -  -  -  -
17 0-6   -  -  Q  -  -  Q
18 0-6   R  -  R  R  -   
19 0-6   S  S  -  -  S   
20 0-6   T  -  T  T      
21 0-6   -  U  U  -      
22 1-4   -  V  -         
23 1-6   W  X  X         
24 3-4   X  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZCNTQ BMUST QFVWC LHYUV DPTKE F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 2-0   -  B  -  -  B  -
03 0-3   -  C  C  C  C  -
04 0-3   D  -  D  -  D  -
05 0-4   E  -  -  E  -  E
06 0-4   F  -  -  F  F  F
07 0-4   G  G  G  G  G  -
08 0-4   -  H  H  H  -  -
09 0-4   -  -  I  -  -  I
10 0-4   J  J  -  -  -  -
11 0-4   K  -  K  K  -  K
12 0-5   L  L  -  -  L  -
13 0-5   -  -  -  -  -  M
14 0-5   -  -  -  -  N  N
15 0-5   O  O  O  -  O  O
16 0-5   -  P  -  P  -  P
17 0-5   Q  Q  -  Q  -  Q
18 0-5   R  R  -  -  -   
19 0-5   -  -  S  -  -   
20 0-5   -  -  -  -      
21 0-5   -  U  -  U      
22 0-6   V  V  V         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   -  Z            
26 2-3   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

QFRXL HHGLN PVIVR MLJEV JPREO J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  -
02 0-3   -  B  B  -  -  -
03 0-4   -  C  -  C  C  -
04 0-4   D  -  D  D  -  D
05 0-4   E  E  -  E  E  -
06 0-4   F  -  -  F  -  -
07 0-5   G  G  G  -  G  G
08 0-5   -  H  H  -  -  H
09 0-5   -  -  -  I  I  I
10 0-5   J  -  J  -  J  -
11 0-5   K  K  -  -  K  -
12 0-5   -  -  -  -  L  -
13 0-5   M  -  M  -  -  M
14 0-5   -  N  -  N  N  N
15 0-5   -  -  O  O  -  O
16 0-5   P  P  P  P  P  -
17 0-6   -  -  Q  -  -  Q
18 0-6   R  R  -  -  R   
19 0-6   -  -  -  -  -   
20 0-6   T  -  T  T      
21 0-6   -  U  -  -      
22 1-3   -  V  V         
23 1-4   -  X  -         
24 4-6   X  Y            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OIZQU ZVGQG UHTQY LPVNU KVHYI O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 2-0   B  -  -  -  B  -
03 2-0   C  -  C  C  C  C
04 2-0   D  D  D  D  -  D
05 0-3   E  E  -  -  -  E
06 0-3   -  -  -  F  -  -
07 0-3   -  G  G  -  G  -
08 0-3   H  H  -  H  H  -
09 0-4   -  I  -  -  -  I
10 0-4   -  J  -  J  J  -
11 0-4   -  -  -  -  -  K
12 0-4   -  -  L  -  L  -
13 0-4   M  -  M  -  -  M
14 0-4   -  N  N  -  -  N
15 0-5   O  O  -  O  -  -
16 0-5   -  -  P  -  P  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   -  R  -  R  -   
19 0-5   S  -  -  S  S   
20 1-5   T  T  -  T      
21 2-3   -  U  U  -      
22 2-3   V  -  V         
23 2-3   W  X  X         
24 2-3   X  -            
25 2-4   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

TBTOO ORXOP HISKV QQPQP IIIXH V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  -  -  -  B  -
03 1-0   -  -  C  C  -  -
04 1-0   -  -  D  D  -  D
05 0-4   -  E  E  E  E  E
06 0-4   F  F  -  -  -  F
07 0-4   G  -  -  -  G  -
08 0-4   -  -  -  H  -  -
09 0-4   I  -  I  I  I  -
10 0-4   J  J  -  -  -  J
11 0-4   -  K  K  K  -  K
12 0-6   -  -  L  -  L  -
13 0-6   -  M  -  -  -  M
14 0-6   -  N  -  N  N  -
15 0-6   O  -  O  -  O  -
16 0-6   P  -  -  P  -  -
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  -  R   
19 0-6   S  S  -  -  -   
20 1-2   T  -  T  T      
21 1-4   U  -  -  -      
22 2-3   -  -  -         
23 4-6   -  -  X         
24 4-6   X  Y            
25 4-6   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SMTSR FBUGH MAVMB BAZAZ GCMBW R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  -  -  -  B  B
03 2-0   C  C  -  -  C  C
04 2-0   -  -  -  D  -  D
05 2-0   -  E  -  -  E  -
06 2-0   -  -  F  F  F  -
07 2-0   G  -  G  -  G  G
08 2-0   H  H  -  H  -  H
09 0-3   I  I  -  I  I  -
10 0-3   J  J  J  -  -  -
11 0-4   K  -  K  K  K  K
12 0-4   -  L  L  L  -  -
13 0-4   M  -  M  M  -  M
14 0-4   -  N  -  -  -  -
15 0-4   -  O  O  -  O  -
16 0-4   P  -  P  P  P  P
17 0-4   -  Q  -  Q  Q  -
18 0-4   -  R  R  R  -   
19 0-6   S  -  S  -  -   
20 1-2   T  T  -  T      
21 1-3   -  U  -  U      
22 2-4   -  -  -         
23 2-4   W  -  X         
24 2-4   X  -            
25 2-4   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YDQWT YKSSI NDQQO OQSOA YSQEY Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  -  B  B  -  B
03 1-0   C  -  C  -  -  -
04 1-0   D  -  -  D  D  D
05 1-0   -  E  E  E  E  -
06 1-0   -  -  F  F  -  F
07 1-0   -  G  -  -  G  G
08 1-0   H  -  -  -  H  H
09 2-0   -  I  I  -  I  -
10 2-0   -  -  -  J  -  -
11 2-0   -  K  K  -  -  K
12 2-0   -  -  -  L  -  L
13 2-0   M  M  -  M  -  -
14 2-0   -  N  -  N  -  -
15 2-0   O  -  -  O  -  -
16 0-3   -  -  P  -  P  -
17 0-3   Q  Q  Q  -  Q  Q
18 0-3   -  R  -  -  R   
19 0-5   S  -  -  -  -   
20 0-6   T  T  T  -      
21 0-6   U  -  -  U      
22 1-2   -  -  V         
23 1-2   W  -  -         
24 2-3   -  Y            
25 3-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

EOOVM UJNIR SHJOT QCNSJ FKAXP T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  -  -  -  B
03 0-3   -  C  -  -  C  C
04 0-4   -  -  -  D  -  -
05 0-4   -  E  E  -  E  -
06 0-4   -  -  F  -  -  -
07 0-4   G  G  G  G  -  G
08 0-4   H  -  -  -  H  -
09 0-4   I  I  I  I  -  I
10 0-5   J  -  -  -  -  J
11 0-5   -  -  -  -  K  -
12 0-5   L  -  -  L  L  -
13 0-5   M  M  M  M  -  M
14 0-5   -  -  N  -  N  -
15 0-5   -  O  -  -  -  O
16 0-5   -  -  P  -  P  -
17 0-6   -  -  Q  Q  Q  Q
18 0-6   R  -  -  R  -   
19 0-6   -  -  -  S  -   
20 0-6   -  -  -  -      
21 0-6   -  U  -  U      
22 1-3   -  V  V         
23 1-4   W  -  X         
24 2-3   X  -            
25 4-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TZUCV QPEAT JATIC YKLOT KVFUI R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 1-0   B  B  -  -  -  B
03 1-0   -  -  -  C  C  -
04 2-0   -  D  D  D  D  D
05 0-3   E  E  -  E  E  -
06 0-3   -  F  F  -  -  F
07 0-3   G  -  -  -  G  -
08 0-3   H  H  H  H  H  H
09 0-3   I  I  I  I  I  I
10 0-3   J  -  J  J  -  -
11 0-4   -  -  -  -  -  -
12 0-4   -  L  L  -  -  -
13 0-4   -  M  M  -  M  -
14 0-4   N  -  -  N  -  -
15 0-4   -  -  O  O  -  O
16 0-4   P  P  P  -  -  -
17 0-4   -  -  -  -  -  -
18 0-4   -  R  -  R  -   
19 0-4   -  -  S  -  S   
20 0-4   -  -  T  -      
21 0-4   U  -  -  U      
22 0-4   V  V  -         
23 0-4   W  -  X         
24 0-5   -  Y            
25 0-6   Y  Z            
26 1-6   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

DVSBC WJHPY NRUSE NPWYL EIUOL I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   -  B  B  -  -  B
03 2-0   C  C  C  -  C  -
04 2-0   D  D  -  -  -  -
05 2-0   -  -  E  E  E  E
06 2-0   F  F  -  F  -  F
07 2-0   -  -  -  -  -  -
08 2-0   H  H  -  -  H  -
09 2-0   I  I  I  I  -  I
10 2-0   J  -  -  -  J  J
11 0-3   K  K  K  K  K  K
12 0-4   -  -  -  L  L  L
13 0-4   -  -  M  M  M  M
14 0-4   -  -  N  N  -  -
15 0-4   O  -  O  O  -  O
16 0-4   -  P  P  -  P  P
17 0-4   -  Q  Q  -  -  -
18 0-4   R  R  R  -  R   
19 0-5   -  -  -  -  S   
20 0-6   T  T  -  -      
21 0-6   -  U  U  -      
22 0-6   -  V  -         
23 0-6   -  -  -         
24 1-3   X  -            
25 1-6   -  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QAZMI QLNHI NHTUV GROTZ MYPLB I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  -  -  B  B  B
03 1-0   -  C  -  C  -  -
04 1-0   -  -  -  -  D  D
05 2-0   E  E  -  E  -  E
06 2-0   -  -  F  -  -  F
07 2-0   G  G  -  -  G  G
08 2-0   H  H  H  H  H  -
09 0-4   -  I  -  I  -  -
10 0-4   -  J  J  J  -  -
11 0-4   -  K  K  K  K  -
12 0-4   L  -  -  -  -  -
13 0-4   -  -  -  M  M  -
14 0-4   -  N  N  -  N  N
15 0-4   -  -  O  O  -  O
16 0-4   P  P  -  P  P  P
17 0-5   -  -  Q  -  -  Q
18 0-5   R  -  R  -  -   
19 0-5   -  -  S  -  S   
20 0-5   T  -  T  -      
21 0-5   -  -  U  -      
22 0-5   V  -  -         
23 0-6   -  X  X         
24 1-2   X  -            
25 1-6   -  Z            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

OMUKO CHJMP UACQG RHTYO OWHRQ C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   -  -  B  -  -  -
03 1-0   C  -  C  C  C  C
04 1-0   D  D  D  -  D  D
05 2-0   E  E  -  -  -  E
06 2-0   -  F  F  F  -  -
07 2-0   G  G  -  -  G  -
08 2-0   -  H  -  H  H  -
09 0-3   I  -  I  I  I  -
10 0-3   J  -  J  -  -  J
11 0-3   K  -  K  K  -  K
12 0-3   -  -  -  -  L  -
13 0-5   M  -  M  M  M  M
14 0-5   -  N  -  -  N  N
15 0-5   -  O  O  O  -  O
16 0-5   -  -  -  P  -  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   -  -  R  -  R   
19 0-6   S  S  S  S  S   
20 1-3   T  T  T  -      
21 1-6   U  U  -  U      
22 2-3   -  -  -         
23 2-5   W  X  -         
24 2-5   X  Y            
25 2-5   Y  Z            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RURMU SWRVQ RWFKV YRQNW WRFHQ P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 2-0   B  -  B  -  B  -
03 2-0   C  C  -  C  -  C
04 2-0   D  D  D  -  -  -
05 2-0   E  -  -  -  E  -
06 2-0   -  F  F  F  -  -
07 0-3   -  -  -  G  G  -
08 0-3   H  H  H  -  -  -
09 0-3   -  I  I  I  I  I
10 0-3   -  -  -  -  -  J
11 0-3   K  K  K  -  K  -
12 0-3   -  -  -  L  -  -
13 0-3   M  -  -  M  -  -
14 0-3   -  -  N  -  N  N
15 0-5   -  -  O  -  -  O
16 0-5   -  P  P  -  -  P
17 0-5   Q  Q  -  Q  -  Q
18 0-5   R  R  R  -  R   
19 0-5   S  -  S  S  S   
20 0-5   -  T  -  T      
21 0-6   U  -  -  -      
22 1-4   -  -  V         
23 2-4   -  X  X         
24 2-5   X  Y            
25 3-5   Y  -            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

MTZJN UMMTA BSLTR OQMZG NSANL T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 2-0   B  -  B  B  -  -
03 2-0   C  -  C  C  C  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  E  E  E  E
06 2-0   F  -  -  -  F  F
07 2-0   G  G  -  G  -  -
08 2-0   -  H  -  H  H  H
09 2-0   I  -  -  -  I  -
10 2-0   J  J  -  -  -  -
11 0-3   -  K  K  K  -  -
12 0-3   L  -  L  -  -  -
13 0-3   -  -  M  M  M  M
14 0-3   -  N  N  N  N  N
15 0-4   -  -  -  -  -  -
16 0-4   -  P  -  -  P  P
17 0-5   -  -  -  -  Q  -
18 0-6   -  R  R  -  R   
19 0-6   S  -  S  S  -   
20 0-6   T  T  T  -      
21 0-6   -  -  U  U      
22 0-6   V  V  -         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

HNISH WJNFX EYRFM FFKIM YRQFJ A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  -  -  -  -  B
03 1-0   -  C  C  -  C  C
04 1-0   D  D  -  D  -  -
05 1-0   -  E  E  E  -  -
06 1-0   F  F  -  F  F  F
07 1-0   -  G  -  -  G  G
08 1-0   -  -  -  H  -  H
09 2-0   I  -  I  I  I  I
10 2-0   -  -  J  J  -  -
11 2-0   -  -  -  -  K  -
12 2-0   L  -  L  -  L  L
13 2-0   M  -  M  M  -  -
14 2-0   N  N  -  N  -  -
15 0-3   O  O  O  -  -  -
16 0-3   -  -  -  -  -  P
17 0-3   -  -  -  -  Q  Q
18 0-3   R  R  -  -  -   
19 0-5   -  -  S  S  S   
20 0-5   -  -  T  -      
21 0-6   U  -  U  U      
22 1-2   V  -  V         
23 1-2   W  -  -         
24 2-3   X  Y            
25 3-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NIJIZ USSSL QJSOA LVJKS ZGSMY T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  B  B  -  B  B
03 1-0   C  C  C  C  -  -
04 2-0   D  D  -  -  D  -
05 2-0   -  -  E  -  E  -
06 2-0   F  -  -  -  -  F
07 0-3   -  -  G  -  -  G
08 0-5   H  -  -  H  -  H
09 0-5   I  -  I  I  -  -
10 0-5   -  J  -  -  -  J
11 0-5   K  -  K  -  K  -
12 0-6   L  L  -  L  L  L
13 0-6   -  -  -  -  M  -
14 0-6   -  N  N  N  N  N
15 0-6   O  O  -  -  -  -
16 0-6   -  P  -  P  P  -
17 0-6   -  -  Q  Q  -  -
18 1-5   R  R  -  -  R   
19 1-6   S  -  -  S  S   
20 1-6   T  -  T  T      
21 1-6   U  U  U  -      
22 1-6   V  -  V         
23 2-3   -  -  -         
24 2-5   -  -            
25 3-4   -  -            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

NWMWN HUKUG ZUXUW ZSAXM XMNKF Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  -
02 2-0   B  -  -  -  B  -
03 2-0   -  C  -  -  -  C
04 2-0   D  D  -  D  -  -
05 2-0   -  E  -  -  -  -
06 2-0   -  F  F  F  F  F
07 0-3   G  G  G  -  -  G
08 0-4   H  H  -  -  H  -
09 0-4   -  -  I  I  I  I
10 0-4   -  -  -  J  -  J
11 0-4   K  -  K  -  -  -
12 0-4   L  -  L  L  L  L
13 0-6   -  M  -  M  M  -
14 0-6   N  -  -  N  -  -
15 0-6   -  O  -  -  -  -
16 0-6   -  -  P  P  -  P
17 0-6   Q  -  -  -  Q  Q
18 0-6   R  R  R  -  R   
19 0-6   -  -  S  S  S   
20 1-3   -  T  T  -      
21 1-6   U  U  U  -      
22 2-4   V  V  -         
23 2-4   -  -  X         
24 2-4   -  Y            
25 2-4   Y  -            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QTCQB VAMZA BZSTS VVSAA MMSTM M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  -  -  -  -  B
03 1-0   -  C  C  C  -  -
04 1-0   D  -  D  D  -  -
05 1-0   -  -  E  -  E  -
06 1-0   F  F  -  F  F  F
07 1-0   G  G  -  G  G  -
08 0-3   H  -  H  -  -  H
09 0-3   I  I  -  I  I  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  -  -  -  K
12 0-4   L  L  -  L  L  -
13 0-4   M  M  M  M  M  M
14 0-4   N  N  -  -  -  -
15 0-5   -  -  O  -  -  -
16 0-5   -  P  -  P  P  P
17 0-5   Q  Q  Q  -  Q  -
18 0-5   -  R  R  -  -   
19 0-5   S  -  -  S  -   
20 0-5   -  -  T  T      
21 0-6   -  U  U  U      
22 1-4   -  -  -         
23 1-4   -  -  X         
24 1-4   X  Y            
25 2-3   -  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QUBTK IMNXD NITKA JSAPT XIOSA A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
