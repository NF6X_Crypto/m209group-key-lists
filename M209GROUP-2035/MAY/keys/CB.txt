EFFECTIVE PERIOD:
01-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   -  -  -  C  -  C
04 2-0   D  D  -  D  -  -
05 2-0   E  -  -  -  E  E
06 0-3   -  -  F  -  -  F
07 0-4   -  -  G  G  G  -
08 0-4   -  H  -  H  H  -
09 0-4   -  I  -  I  -  I
10 0-4   -  -  J  J  -  -
11 0-4   -  -  -  K  -  -
12 0-4   L  L  -  -  L  L
13 0-4   M  -  M  -  M  M
14 0-4   N  -  -  -  N  N
15 0-6   -  O  O  -  -  O
16 0-6   P  P  -  -  -  -
17 0-6   -  -  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   S  S  -  -  S   
20 1-4   -  -  T  -      
21 2-5   -  U  -  -      
22 2-6   V  -  V         
23 3-5   -  X  X         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

IRLQA MSBJM TUZZI VOMYA LLJKC T
-------------------------------
