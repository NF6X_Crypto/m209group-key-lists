EFFECTIVE PERIOD:
27-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 2-0   B  -  B  B  -  -
03 2-0   C  -  C  C  C  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  E  E  E  E
06 2-0   F  -  -  -  F  F
07 2-0   G  G  -  G  -  -
08 2-0   -  H  -  H  H  H
09 2-0   I  -  -  -  I  -
10 2-0   J  J  -  -  -  -
11 0-3   -  K  K  K  -  -
12 0-3   L  -  L  -  -  -
13 0-3   -  -  M  M  M  M
14 0-3   -  N  N  N  N  N
15 0-4   -  -  -  -  -  -
16 0-4   -  P  -  -  P  P
17 0-5   -  -  -  -  Q  -
18 0-6   -  R  R  -  R   
19 0-6   S  -  S  S  -   
20 0-6   T  T  T  -      
21 0-6   -  -  U  U      
22 0-6   V  V  -         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

HNISH WJNFX EYRFM FFKIM YRQFJ A
-------------------------------
