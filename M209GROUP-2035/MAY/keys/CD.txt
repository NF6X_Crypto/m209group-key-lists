EFFECTIVE PERIOD:
03-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  -  B  B  -  -
03 1-0   C  -  C  -  -  C
04 1-0   -  D  D  D  -  D
05 1-0   -  E  E  E  -  E
06 1-0   F  F  F  -  F  -
07 2-0   -  -  -  G  -  -
08 2-0   H  H  H  -  -  H
09 2-0   I  I  -  -  -  I
10 0-3   -  -  -  -  J  J
11 0-3   K  -  K  K  K  K
12 0-3   -  L  L  L  -  -
13 0-3   M  M  M  -  -  -
14 0-3   -  -  -  -  -  -
15 0-4   O  O  -  -  -  O
16 1-3   -  P  -  -  P  P
17 1-4   Q  -  -  Q  Q  Q
18 2-3   -  -  -  R  R   
19 2-3   -  S  -  -  S   
20 2-3   T  -  T  T      
21 2-3   -  -  U  -      
22 2-4   -  V  -         
23 2-4   -  X  -         
24 2-6   -  -            
25 2-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MSZZV HNULL OSUVS UUNTU OUSUA V
-------------------------------
