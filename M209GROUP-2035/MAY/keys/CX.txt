EFFECTIVE PERIOD:
23-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   -  B  B  -  -  B
03 2-0   C  C  C  -  C  -
04 2-0   D  D  -  -  -  -
05 2-0   -  -  E  E  E  E
06 2-0   F  F  -  F  -  F
07 2-0   -  -  -  -  -  -
08 2-0   H  H  -  -  H  -
09 2-0   I  I  I  I  -  I
10 2-0   J  -  -  -  J  J
11 0-3   K  K  K  K  K  K
12 0-4   -  -  -  L  L  L
13 0-4   -  -  M  M  M  M
14 0-4   -  -  N  N  -  -
15 0-4   O  -  O  O  -  O
16 0-4   -  P  P  -  P  P
17 0-4   -  Q  Q  -  -  -
18 0-4   R  R  R  -  R   
19 0-5   -  -  -  -  S   
20 0-6   T  T  -  -      
21 0-6   -  U  U  -      
22 0-6   -  V  -         
23 0-6   -  -  -         
24 1-3   X  -            
25 1-6   -  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QAZMI QLNHI NHTUV GROTZ MYPLB I
-------------------------------
