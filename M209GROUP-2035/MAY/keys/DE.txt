EFFECTIVE PERIOD:
30-MAY-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  -
02 2-0   B  -  -  -  B  -
03 2-0   -  C  -  -  -  C
04 2-0   D  D  -  D  -  -
05 2-0   -  E  -  -  -  -
06 2-0   -  F  F  F  F  F
07 0-3   G  G  G  -  -  G
08 0-4   H  H  -  -  H  -
09 0-4   -  -  I  I  I  I
10 0-4   -  -  -  J  -  J
11 0-4   K  -  K  -  -  -
12 0-4   L  -  L  L  L  L
13 0-6   -  M  -  M  M  -
14 0-6   N  -  -  N  -  -
15 0-6   -  O  -  -  -  -
16 0-6   -  -  P  P  -  P
17 0-6   Q  -  -  -  Q  Q
18 0-6   R  R  R  -  R   
19 0-6   -  -  S  S  S   
20 1-3   -  T  T  -      
21 1-6   U  U  U  -      
22 2-4   V  V  -         
23 2-4   -  -  X         
24 2-4   -  Y            
25 2-4   Y  -            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QTCQB VAMZA BZSTS VVSAA MMSTM M
-------------------------------
