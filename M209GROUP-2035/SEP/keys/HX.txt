EFFECTIVE PERIOD:
30-SEP-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  B  B  -  B
03 1-0   -  C  -  -  C  -
04 1-0   D  -  -  D  -  D
05 0-4   -  E  E  E  -  -
06 0-5   F  -  F  F  -  -
07 0-5   G  G  G  G  G  G
08 0-5   H  -  -  H  H  -
09 0-5   -  -  -  -  -  I
10 0-5   -  -  J  -  J  -
11 0-5   -  -  K  -  K  -
12 0-6   L  L  -  -  -  -
13 0-6   M  M  -  -  -  M
14 0-6   N  -  N  -  -  -
15 0-6   O  O  O  O  -  O
16 0-6   -  -  P  P  P  P
17 0-6   -  Q  -  -  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   -  -  -  S  S   
20 1-5   T  T  T  T      
21 1-6   -  -  -  -      
22 1-6   -  V  V         
23 1-6   W  X  X         
24 1-6   -  -            
25 2-6   -  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

CBUCY WPAOB WBLOR WLQPO BMRAA T
-------------------------------
