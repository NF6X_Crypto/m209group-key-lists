EFFECTIVE PERIOD:
02-SEP-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 2-0   B  B  -  -  -  -
03 0-3   C  -  -  C  C  C
04 0-3   D  D  D  D  -  D
05 0-3   E  E  E  E  -  E
06 0-3   -  F  F  -  -  -
07 0-3   -  -  -  -  G  -
08 0-3   -  -  H  -  H  -
09 0-3   I  -  -  -  I  -
10 0-3   J  J  -  J  -  -
11 0-3   -  K  -  K  K  K
12 0-3   L  L  -  -  -  L
13 0-3   M  -  M  -  M  -
14 0-3   N  -  N  -  -  N
15 0-4   -  O  O  -  O  -
16 0-6   -  -  P  P  -  -
17 0-6   -  Q  -  -  -  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  -  -  S   
20 0-6   T  -  -  T      
21 0-6   -  -  U  U      
22 0-6   -  -  V         
23 0-6   -  X  X         
24 2-5   -  -            
25 2-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

XXPMD BLPRW ZLFPR RDVMC NCRWP Q
-------------------------------
