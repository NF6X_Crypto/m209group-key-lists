EFFECTIVE PERIOD:
27-JUN-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 2-0   B  -  B  B  -  -
03 0-3   C  -  -  C  C  C
04 0-3   -  D  D  -  D  -
05 0-3   -  E  E  -  E  E
06 0-3   F  -  F  F  -  -
07 0-4   -  G  G  -  G  -
08 0-4   -  H  -  H  H  -
09 0-4   -  I  I  -  I  I
10 0-4   -  -  -  J  -  -
11 0-4   -  K  -  K  -  K
12 0-4   L  L  -  -  -  L
13 0-4   -  -  M  -  M  M
14 0-4   -  N  N  -  N  N
15 0-4   O  O  -  O  -  -
16 0-4   P  P  P  P  -  -
17 0-4   -  Q  -  Q  Q  -
18 0-4   R  -  R  R  R   
19 0-5   S  S  -  -  -   
20 0-5   T  -  -  -      
21 0-6   U  U  -  U      
22 0-6   -  -  -         
23 0-6   -  X  X         
24 1-5   X  -            
25 3-4   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MIATU REEPV SZEHW NWFWS HEIRV X
-------------------------------
