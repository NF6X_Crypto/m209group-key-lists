EFFECTIVE PERIOD:
04-NOV-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   B  B  B  -  B  -
03 2-0   -  C  C  -  -  -
04 2-0   -  -  D  D  D  D
05 0-3   -  -  -  -  -  E
06 0-3   -  -  F  -  -  F
07 0-3   -  G  G  G  G  G
08 0-3   -  H  -  -  H  H
09 0-3   I  -  I  -  -  -
10 0-3   J  J  -  J  J  J
11 0-4   K  -  -  K  -  K
12 0-5   L  L  L  -  L  L
13 0-5   -  -  M  M  -  -
14 0-5   N  N  N  -  N  -
15 0-5   O  -  -  O  O  O
16 0-5   P  -  -  -  -  P
17 0-5   Q  Q  Q  -  Q  -
18 0-5   R  R  R  -  -   
19 0-5   -  -  -  S  S   
20 0-5   T  -  -  T      
21 0-5   U  U  -  U      
22 0-5   -  -  V         
23 0-6   W  -  X         
24 1-6   -  Y            
25 2-3   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

WCTXK OGNTY HVPOO TKTPN GWZIS A
-------------------------------
