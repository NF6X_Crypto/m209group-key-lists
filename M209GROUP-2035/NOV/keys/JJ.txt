EFFECTIVE PERIOD:
07-NOV-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  -
02 2-0   -  B  -  -  -  -
03 0-3   C  -  -  -  -  -
04 0-4   D  D  D  D  D  D
05 0-4   E  E  -  -  -  E
06 0-4   -  -  F  F  F  F
07 0-4   -  G  -  -  G  -
08 0-5   -  H  H  -  -  -
09 0-5   -  -  I  -  -  -
10 0-5   -  J  -  J  -  J
11 0-5   K  K  -  K  -  K
12 0-5   L  L  L  L  L  -
13 0-5   -  -  M  -  M  M
14 0-5   N  -  -  N  N  -
15 0-6   O  O  -  -  -  -
16 0-6   P  -  P  -  P  P
17 0-6   Q  -  -  Q  Q  -
18 0-6   -  -  -  R  -   
19 0-6   S  S  -  -  -   
20 1-5   -  T  -  -      
21 2-3   U  U  U  U      
22 2-6   -  V  V         
23 4-5   W  X  -         
24 4-5   X  Y            
25 4-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

VQUAU ZDFVD JSRNU FGKNW UQTLL O
-------------------------------
