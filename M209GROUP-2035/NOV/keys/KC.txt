EFFECTIVE PERIOD:
26-NOV-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   -  B  B  B  -  B
03 2-0   C  C  C  -  -  -
04 2-0   -  D  D  D  -  D
05 2-0   -  -  E  E  E  -
06 2-0   F  -  -  F  -  -
07 2-0   G  G  -  -  G  G
08 0-4   H  -  H  -  H  H
09 0-4   -  I  I  I  -  I
10 0-4   -  J  -  J  -  -
11 0-5   K  -  -  K  K  -
12 0-5   L  L  -  L  L  L
13 0-5   M  -  -  -  M  M
14 0-5   N  N  -  -  N  N
15 0-6   O  -  O  O  -  O
16 0-6   -  P  -  -  -  -
17 0-6   Q  -  -  -  Q  -
18 0-6   R  R  -  R  R   
19 0-6   S  S  S  S  S   
20 1-2   -  -  T  -      
21 2-4   U  -  -  U      
22 2-4   -  -  -         
23 2-4   W  -  -         
24 2-4   -  -            
25 3-5   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FXTSS ONGNA OMVWA LAQTA AMLIR M
-------------------------------
