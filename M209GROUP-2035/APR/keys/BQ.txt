EFFECTIVE PERIOD:
20-APR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  B  -  B  B
03 1-0   -  C  -  C  -  -
04 0-3   D  -  -  D  -  D
05 0-3   E  E  -  -  E  E
06 0-3   F  F  -  F  F  F
07 0-3   -  -  -  -  G  -
08 0-3   -  H  H  -  H  -
09 0-4   I  -  -  I  -  I
10 0-4   J  -  -  -  -  -
11 0-4   -  -  -  K  -  -
12 0-4   -  L  L  -  L  -
13 0-4   -  -  M  M  -  -
14 0-4   -  -  N  N  -  N
15 0-4   O  O  O  O  -  O
16 1-2   P  P  P  -  P  -
17 1-2   -  -  -  -  Q  Q
18 1-3   -  R  R  -  -   
19 1-3   -  S  S  S  S   
20 1-3   T  T  -  -      
21 1-3   U  U  -  U      
22 1-6   -  -  -         
23 1-6   W  -  X         
24 2-4   -  Y            
25 2-6   Y  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AURGT TOMMT QAOEU NMRDU MVUVN T
-------------------------------
