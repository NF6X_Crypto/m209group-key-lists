EFFECTIVE PERIOD:
25-APR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   B  B  -  B  -  -
03 1-0   -  -  C  -  C  C
04 1-0   D  -  -  D  D  -
05 2-0   E  -  E  -  E  E
06 2-0   -  F  F  F  -  -
07 0-3   G  G  G  G  -  -
08 0-3   H  -  -  H  H  -
09 0-3   -  I  I  -  I  -
10 0-3   J  -  J  -  J  J
11 0-3   -  K  -  K  -  K
12 0-5   L  L  -  L  -  L
13 0-5   -  -  M  -  -  -
14 0-5   -  N  -  -  N  N
15 0-5   O  O  -  O  -  O
16 0-5   P  -  P  -  P  P
17 0-5   Q  Q  -  Q  -  -
18 0-5   -  R  -  -  R   
19 0-6   S  -  -  S  -   
20 1-2   T  -  T  T      
21 1-3   U  -  -  -      
22 2-6   -  -  V         
23 3-5   -  -  X         
24 3-5   -  Y            
25 3-5   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KIVKT IRCQP WTZZP WWVRO IQTLF Z
-------------------------------
