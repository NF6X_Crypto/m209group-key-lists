EFFECTIVE PERIOD:
13-APR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  -  -  -  B  B
03 1-0   -  C  -  C  C  -
04 1-0   -  -  D  D  D  D
05 1-0   -  E  -  -  -  E
06 2-0   -  F  F  F  -  F
07 2-0   -  G  -  G  -  G
08 2-0   H  -  H  H  -  -
09 0-3   -  I  -  -  I  -
10 0-3   -  -  J  -  J  -
11 0-3   K  K  -  K  K  K
12 0-3   -  L  -  L  -  L
13 0-3   -  M  -  -  M  -
14 0-3   N  -  N  -  -  -
15 0-6   -  -  O  -  -  -
16 0-6   -  -  -  P  P  P
17 0-6   -  -  Q  -  -  -
18 1-2   R  R  -  -  R   
19 1-6   S  -  -  S  S   
20 2-3   -  T  T  T      
21 2-3   U  U  -  U      
22 2-3   -  -  V         
23 2-3   W  -  X         
24 3-4   X  -            
25 3-4   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QRGXA RRNXN SUNUD RLVLR DNMQX F
-------------------------------
