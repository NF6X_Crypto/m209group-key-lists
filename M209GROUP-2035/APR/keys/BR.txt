EFFECTIVE PERIOD:
21-APR-2035 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 2-0   B  -  B  -  -  B
03 0-3   -  C  C  C  -  C
04 0-3   D  -  D  -  D  D
05 0-3   -  E  -  E  -  E
06 0-3   F  -  -  F  F  -
07 0-3   G  G  -  -  G  G
08 0-3   H  H  H  H  -  -
09 0-3   I  -  I  -  -  -
10 0-3   -  J  J  -  J  -
11 0-3   -  K  K  K  -  -
12 0-3   L  -  L  L  L  L
13 0-3   M  -  M  M  -  -
14 0-3   -  -  -  N  N  N
15 0-4   -  O  O  -  -  O
16 0-5   -  P  -  P  P  P
17 0-5   -  -  Q  -  Q  Q
18 0-5   -  R  R  R  R   
19 0-5   S  S  S  S  -   
20 0-5   T  T  -  T      
21 0-5   -  U  -  -      
22 0-6   -  -  -         
23 0-6   -  -  -         
24 0-6   -  Y            
25 0-6   Y  Z            
26 2-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

JTFUZ ZNCKH GAMZJ BDUTV TZTLU M
-------------------------------
