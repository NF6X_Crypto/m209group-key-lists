EFFECTIVE PERIOD:
07-NOV-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   B  B  -  -  -  -
03 0-3   C  -  -  C  -  C
04 0-3   -  D  D  -  D  -
05 0-3   E  E  -  E  E  -
06 0-4   F  -  F  -  F  -
07 0-5   -  G  G  G  G  G
08 0-5   -  H  -  -  H  -
09 0-5   I  I  -  -  -  I
10 0-5   -  -  -  J  -  -
11 0-5   K  K  K  -  K  K
12 0-5   L  -  L  L  -  -
13 0-5   -  -  M  M  M  M
14 0-5   -  N  N  N  N  N
15 0-5   O  O  O  O  O  O
16 0-5   P  -  P  -  P  P
17 0-5   -  -  -  -  -  -
18 0-5   -  R  -  R  -   
19 0-6   S  -  -  -  -   
20 0-6   T  -  T  -      
21 0-6   U  U  -  -      
22 0-6   V  V  V         
23 0-6   W  X  -         
24 0-6   -  Y            
25 0-6   -  -            
26 1-2   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

OQXWU JGDRJ ZMPZO AFJPK XAZQF G
-------------------------------
