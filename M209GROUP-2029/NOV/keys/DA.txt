EFFECTIVE PERIOD:
05-NOV-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  A
02 2-0   -  B  -  B  -  -
03 2-0   -  C  C  -  -  -
04 2-0   D  -  D  -  D  -
05 0-3   E  -  -  E  E  -
06 0-3   -  F  -  F  -  F
07 0-3   G  G  G  G  -  -
08 0-5   -  H  -  H  H  -
09 0-5   -  I  -  I  I  -
10 0-5   -  -  J  -  J  -
11 0-5   K  -  -  -  K  K
12 0-5   -  -  -  L  -  -
13 0-5   M  M  M  M  M  M
14 0-6   -  -  N  N  -  N
15 0-6   O  O  O  -  O  O
16 1-3   P  P  -  P  -  -
17 1-6   -  -  Q  -  -  Q
18 2-3   -  -  -  -  -   
19 2-3   -  -  S  -  S   
20 2-3   T  -  T  T      
21 2-3   U  U  -  U      
22 2-5   -  -  -         
23 3-4   W  X  X         
24 3-6   X  -            
25 3-6   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UXGWH MUPSM LPAQC KXXPU OITUP K
-------------------------------
