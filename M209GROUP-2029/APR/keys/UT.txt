EFFECTIVE PERIOD:
04-APR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  -  B  B  -
03 1-0   C  C  C  C  C  -
04 1-0   D  D  -  -  -  D
05 1-0   -  E  -  -  -  E
06 1-0   -  -  -  F  -  F
07 1-0   -  -  -  G  G  G
08 1-0   -  -  -  -  H  H
09 1-0   -  I  I  -  -  -
10 2-0   J  -  J  -  -  J
11 0-3   K  -  K  -  K  -
12 0-3   L  L  L  L  -  -
13 0-3   -  -  -  -  -  -
14 0-3   N  -  N  -  -  -
15 0-3   O  O  -  O  O  O
16 0-3   P  -  P  P  P  -
17 0-5   -  -  Q  Q  Q  Q
18 0-5   R  R  -  -  R   
19 0-6   S  -  S  S  S   
20 0-6   -  -  T  -      
21 0-6   U  -  U  -      
22 0-6   -  V  V         
23 0-6   W  X  X         
24 1-3   X  -            
25 3-6   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RZRVJ UQARL UHXRU EGZLI RWOQT V
-------------------------------
