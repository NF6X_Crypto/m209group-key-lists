EFFECTIVE PERIOD:
20-APR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  -
02 2-0   B  -  -  -  B  -
03 2-0   C  C  -  -  C  C
04 0-3   -  -  -  D  D  D
05 0-3   -  E  E  E  -  E
06 0-3   F  F  -  F  F  -
07 0-3   -  G  -  -  -  G
08 0-4   -  -  -  H  -  H
09 0-4   -  I  -  -  I  I
10 0-4   J  J  J  -  J  -
11 0-4   K  -  -  -  -  -
12 0-4   L  L  L  -  L  L
13 0-4   -  -  M  -  M  -
14 0-6   -  N  -  N  -  -
15 0-6   O  -  O  O  -  -
16 0-6   -  -  -  P  -  P
17 0-6   -  -  -  Q  -  Q
18 1-2   -  R  R  -  R   
19 1-2   -  S  S  S  S   
20 1-3   -  -  T  T      
21 1-5   -  U  -  U      
22 2-6   V  -  V         
23 2-6   W  -  X         
24 2-6   X  -            
25 2-6   -  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AVHQV OALPV ROLWU NSJIP KIAPV L
-------------------------------
