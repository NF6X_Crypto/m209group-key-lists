EFFECTIVE PERIOD:
28-APR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  B  -  -  B  B
03 0-4   -  -  C  C  -  -
04 0-4   D  -  -  -  -  D
05 0-4   E  E  -  -  -  -
06 0-4   F  F  F  F  F  -
07 0-5   G  G  -  G  -  G
08 0-5   -  H  -  -  -  -
09 0-5   I  I  -  -  I  I
10 0-5   -  -  J  -  J  -
11 0-5   K  -  K  -  -  K
12 0-5   L  -  L  -  L  L
13 0-6   -  -  M  -  -  M
14 0-6   -  N  -  N  -  N
15 0-6   -  -  O  -  O  -
16 1-3   -  -  -  P  -  -
17 1-3   Q  Q  Q  -  Q  Q
18 1-3   -  -  R  R  -   
19 1-4   S  -  S  S  S   
20 1-4   -  T  -  T      
21 1-4   U  U  U  -      
22 1-4   -  -  -         
23 1-6   -  X  -         
24 2-3   X  -            
25 3-6   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MUGAE XPUPV GOLMO USWGW JLXFU F
-------------------------------
