EFFECTIVE PERIOD:
01-APR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   B  -  B  -  B  B
03 1-0   C  C  -  C  C  -
04 1-0   -  D  -  D  -  D
05 1-0   -  E  E  E  E  E
06 2-0   -  F  F  -  F  -
07 0-4   G  -  G  -  G  G
08 0-4   -  -  H  -  -  -
09 0-4   I  I  -  -  -  -
10 0-4   -  J  J  J  J  J
11 0-5   -  K  K  K  K  K
12 0-5   -  -  L  L  -  -
13 0-5   -  M  -  -  -  -
14 0-5   N  -  -  N  N  N
15 0-6   O  O  -  O  -  -
16 0-6   -  -  P  P  -  P
17 0-6   -  Q  -  Q  -  Q
18 1-4   -  R  R  R  R   
19 1-6   -  -  -  S  S   
20 2-3   T  T  -  -      
21 2-4   U  U  -  -      
22 2-5   -  V  V         
23 2-5   W  -  X         
24 5-6   -  -            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MFRRM WVWIK UTVAM EMUPP RMNRS A
-------------------------------
