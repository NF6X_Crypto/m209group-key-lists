EFFECTIVE PERIOD:
09-OCT-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  -  B  -  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  -  D  -  -
05 1-0   E  -  E  -  E  -
06 1-0   F  -  F  -  -  F
07 0-5   G  G  G  G  G  G
08 0-5   H  H  -  -  -  H
09 0-5   I  I  -  I  I  I
10 0-6   J  J  J  J  -  -
11 0-6   -  -  K  -  -  -
12 0-6   -  -  L  L  -  -
13 0-6   -  -  -  M  M  M
14 0-6   -  -  -  -  N  -
15 0-6   O  O  -  O  -  O
16 1-4   -  P  -  P  -  P
17 1-5   -  -  Q  Q  Q  Q
18 1-5   R  -  -  -  -   
19 1-6   -  -  S  -  S   
20 2-4   T  -  T  -      
21 2-5   -  U  -  U      
22 3-5   -  V  -         
23 4-5   W  X  X         
24 5-6   -  Y            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

DMADV SNUOE NBATS WTMMW UUKNN W
-------------------------------
