EFFECTIVE PERIOD:
22-OCT-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  -  -  -  B  -
03 1-0   C  C  C  -  -  C
04 1-0   D  -  D  -  -  D
05 1-0   E  -  E  E  -  -
06 1-0   F  F  -  F  -  F
07 1-0   -  -  -  G  G  -
08 1-0   -  -  H  -  H  -
09 0-3   I  -  I  -  I  -
10 0-3   -  J  J  J  J  J
11 0-3   K  K  K  K  -  K
12 0-4   -  -  -  -  L  -
13 0-5   -  M  M  M  M  M
14 0-5   -  N  -  -  -  -
15 0-5   -  O  O  O  O  O
16 0-5   P  -  P  P  P  P
17 0-6   -  -  -  Q  -  Q
18 0-6   -  -  -  -  R   
19 0-6   S  S  S  -  S   
20 0-6   T  -  -  T      
21 0-6   -  -  U  U      
22 1-6   V  V  -         
23 1-6   W  -  -         
24 2-4   -  -            
25 3-4   Y  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XJOSX MUVCU QZMQV SVQGA KSQPG L
-------------------------------
