EFFECTIVE PERIOD:
05-OCT-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  -  -  -  B  -
03 1-0   C  -  -  C  -  -
04 1-0   -  D  -  D  -  -
05 1-0   E  -  -  E  E  -
06 1-0   -  -  -  -  -  F
07 1-0   G  -  -  -  G  G
08 1-0   H  H  H  H  H  -
09 1-0   I  I  -  -  I  I
10 1-0   -  J  J  -  J  J
11 2-0   K  -  K  -  K  K
12 0-3   L  -  L  L  -  -
13 0-4   -  M  M  -  M  -
14 0-4   -  -  -  N  N  -
15 0-5   O  O  -  O  -  O
16 0-5   P  -  P  P  P  P
17 0-5   Q  -  -  Q  -  Q
18 0-5   R  -  R  -  -   
19 0-5   S  S  S  -  -   
20 0-5   T  T  -  T      
21 0-5   -  U  U  U      
22 0-5   V  -  V         
23 0-5   -  -  X         
24 0-5   -  -            
25 0-5   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MXZMA DMEEP BDYNL WOXVM QXOPD P
-------------------------------
