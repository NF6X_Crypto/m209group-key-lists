EFFECTIVE PERIOD:
24-OCT-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  -  B  -  B  -
03 1-0   -  C  -  C  C  C
04 1-0   D  D  -  -  D  -
05 1-0   E  -  -  E  -  E
06 1-0   F  F  F  F  F  -
07 2-0   G  G  G  -  -  -
08 0-3   H  H  -  H  -  -
09 0-3   I  I  I  -  I  -
10 0-3   -  J  -  J  J  J
11 0-3   -  -  K  -  -  K
12 0-4   L  -  -  L  L  -
13 0-4   -  M  M  -  -  M
14 0-4   -  -  -  -  N  N
15 0-4   -  O  -  O  O  O
16 0-5   -  P  P  P  -  P
17 0-5   Q  -  -  -  Q  Q
18 1-2   -  -  R  R  -   
19 1-2   -  S  -  -  -   
20 1-5   -  -  T  T      
21 1-5   U  -  U  U      
22 1-5   V  V  -         
23 1-5   W  -  -         
24 2-4   X  Y            
25 2-6   Y  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

LORNK VSYRW ORZQT RARYR OHUJM K
-------------------------------
