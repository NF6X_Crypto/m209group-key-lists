EFFECTIVE PERIOD:
14-MAY-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 2-0   B  B  B  -  -  B
03 2-0   -  -  C  -  -  C
04 0-3   -  D  D  -  D  -
05 0-3   E  E  E  E  -  -
06 0-3   F  F  F  F  F  -
07 0-3   -  G  -  -  -  G
08 0-3   H  H  H  H  H  H
09 0-3   I  -  -  -  I  -
10 0-4   -  -  -  -  J  J
11 0-4   K  -  -  -  -  K
12 0-4   L  L  -  L  -  -
13 0-4   -  M  M  -  M  -
14 0-4   N  N  -  -  N  -
15 0-6   -  O  -  O  -  O
16 0-6   P  P  -  -  -  -
17 0-6   -  -  Q  Q  -  Q
18 1-5   -  -  -  -  R   
19 2-3   S  -  -  S  -   
20 2-5   T  -  T  -      
21 3-6   -  U  U  U      
22 4-5   -  V  V         
23 4-5   -  X  -         
24 4-6   -  -            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NHMIP ZASZO ALMXZ OUVFA MLMTN U
-------------------------------
