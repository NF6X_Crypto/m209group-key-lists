EFFECTIVE PERIOD:
01-MAY-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   B  B  -  -  B  -
03 1-0   C  -  C  C  -  C
04 1-0   -  -  -  -  -  D
05 2-0   -  E  E  -  -  -
06 2-0   -  -  F  F  F  -
07 2-0   G  G  G  -  G  -
08 2-0   H  H  -  -  -  -
09 2-0   -  I  -  I  -  I
10 2-0   J  J  -  -  J  J
11 2-0   K  -  K  K  -  K
12 2-0   L  L  -  L  -  L
13 0-3   M  M  M  -  M  -
14 0-6   -  N  N  N  N  N
15 0-6   O  O  -  O  -  -
16 0-6   -  -  P  P  -  P
17 0-6   Q  -  -  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   S  S  -  -  S   
20 0-6   -  T  T  -      
21 0-6   -  -  -  U      
22 0-6   -  -  V         
23 0-6   W  X  X         
24 0-6   X  -            
25 0-6   -  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

URGRI VOFOO TINFC UMZYI AFAAJ O
-------------------------------
