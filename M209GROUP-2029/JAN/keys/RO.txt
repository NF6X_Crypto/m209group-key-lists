EFFECTIVE PERIOD:
11-JAN-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  -  -  A
02 0-3   -  B  B  -  B  B
03 0-3   -  -  -  -  C  C
04 0-3   D  D  -  -  -  -
05 0-3   -  -  -  -  -  -
06 0-3   -  -  F  F  -  F
07 0-3   -  G  G  -  -  G
08 0-3   H  H  -  -  H  H
09 0-3   I  -  I  I  I  I
10 0-3   J  J  J  J  -  -
11 0-3   K  -  -  K  -  K
12 0-4   L  L  L  -  -  -
13 0-4   -  M  M  M  M  M
14 0-4   N  -  N  N  -  -
15 0-4   O  O  -  -  -  -
16 0-4   -  -  -  P  -  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  -  R  R  R   
19 0-5   S  -  S  -  S   
20 0-5   T  -  T  T      
21 0-5   -  -  U  U      
22 0-5   -  -  -         
23 0-5   W  -  -         
24 0-5   -  Y            
25 0-6   Y  -            
26 1-2   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

SOCRR YTAGK HMCAH TSHRS JRRNP P
-------------------------------
