EFFECTIVE PERIOD:
19-JAN-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  B  -  -  -
03 1-0   C  C  C  -  -  C
04 1-0   -  D  D  D  D  -
05 2-0   -  -  -  -  E  -
06 2-0   -  -  F  F  -  -
07 2-0   G  -  G  -  -  G
08 2-0   H  H  -  -  H  H
09 2-0   -  -  -  -  I  I
10 2-0   -  -  -  J  -  -
11 0-3   -  K  -  -  K  K
12 0-3   -  -  L  L  -  L
13 0-3   -  M  M  M  M  M
14 0-4   -  N  -  -  -  -
15 0-4   O  O  O  -  -  -
16 0-4   P  -  -  P  P  -
17 0-4   -  -  Q  Q  -  Q
18 1-2   -  -  -  R  -   
19 1-3   -  S  -  -  S   
20 1-3   T  T  -  T      
21 1-3   U  U  -  U      
22 1-3   -  V  -         
23 2-4   W  -  -         
24 3-4   X  -            
25 3-5   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WKBPW VJXPB VOWWO KJKXB HWJKW V
-------------------------------
