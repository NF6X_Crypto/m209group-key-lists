EFFECTIVE PERIOD:
13-SEP-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  -  -  -  B  B
03 1-0   -  C  -  -  C  -
04 1-0   -  D  D  D  -  -
05 1-0   E  -  -  E  -  E
06 1-0   -  -  F  F  -  F
07 2-0   G  G  G  -  G  -
08 2-0   H  -  H  -  -  H
09 2-0   I  I  -  I  I  -
10 2-0   J  -  J  -  J  -
11 0-3   -  K  -  K  -  K
12 0-3   -  L  L  -  L  -
13 0-3   M  -  -  M  -  M
14 0-4   N  -  -  -  -  -
15 0-5   O  O  O  -  -  -
16 0-5   P  P  P  P  P  -
17 0-5   -  -  -  -  Q  -
18 0-5   -  R  R  R  -   
19 0-5   -  -  -  S  -   
20 1-3   -  T  -  -      
21 1-3   -  -  -  -      
22 1-3   -  V  -         
23 1-3   W  -  -         
24 2-4   X  -            
25 2-5   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XNIHN NCQRQ NAMPN NNAJA JPFQW U
-------------------------------
