EFFECTIVE PERIOD:
28-SEP-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  B  B  -  B
03 2-0   C  C  -  -  C  C
04 0-3   -  -  -  D  D  -
05 0-3   -  -  E  E  -  E
06 0-3   F  F  F  -  -  F
07 0-3   -  -  G  -  G  -
08 0-4   H  -  -  -  H  -
09 0-4   I  I  I  I  -  I
10 0-4   J  J  J  -  -  J
11 0-4   -  -  -  -  -  -
12 0-4   L  L  L  L  L  -
13 0-4   M  M  M  M  M  -
14 0-6   -  -  -  -  -  -
15 0-6   -  -  -  O  O  O
16 0-6   P  P  P  -  -  P
17 0-6   Q  -  -  Q  -  Q
18 0-6   -  -  R  R  -   
19 0-6   S  S  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  U  U  U      
22 0-6   V  V  -         
23 0-6   -  -  X         
24 1-3   -  -            
25 1-5   -  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LEMDX XPRSQ LRVMG RUHIJ MWUWK W
-------------------------------
