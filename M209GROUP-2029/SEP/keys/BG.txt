EFFECTIVE PERIOD:
20-SEP-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   -  -  B  -  -  B
03 2-0   C  C  -  -  -  -
04 2-0   D  D  D  D  -  -
05 2-0   -  -  -  -  -  -
06 0-4   -  -  -  F  F  F
07 0-4   -  -  G  -  -  -
08 0-5   -  -  H  H  -  H
09 0-5   I  -  -  I  I  -
10 0-5   J  J  -  J  -  J
11 0-5   K  K  -  -  K  -
12 0-6   -  -  L  -  -  L
13 0-6   -  M  M  M  M  M
14 0-6   N  -  -  -  -  -
15 0-6   -  -  O  -  O  O
16 0-6   P  P  -  -  P  P
17 0-6   Q  Q  -  Q  -  -
18 1-2   R  -  R  -  -   
19 2-3   S  -  -  S  S   
20 2-4   -  -  T  -      
21 2-6   U  U  -  U      
22 2-6   -  V  -         
23 2-6   -  -  X         
24 2-6   X  -            
25 3-4   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FYSGU LPRUG LCPSW ZUFTL RWRRC P
-------------------------------
