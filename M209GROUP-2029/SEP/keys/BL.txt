EFFECTIVE PERIOD:
25-SEP-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  -
02 2-0   B  -  -  B  -  -
03 2-0   C  C  C  C  C  -
04 2-0   D  D  -  D  D  -
05 0-4   -  E  -  -  E  E
06 0-4   F  F  F  F  -  F
07 0-4   -  G  G  -  -  -
08 0-4   -  H  H  H  -  H
09 0-4   -  -  I  -  I  -
10 0-4   -  -  J  J  J  J
11 0-5   K  K  -  K  K  -
12 0-5   -  L  L  L  L  L
13 0-6   -  -  -  M  M  M
14 0-6   N  N  N  -  -  N
15 0-6   O  -  -  -  -  O
16 0-6   -  P  P  -  P  P
17 0-6   Q  Q  -  Q  -  Q
18 1-5   -  R  -  R  R   
19 2-4   S  S  S  -  S   
20 2-6   T  T  -  T      
21 2-6   -  U  U  -      
22 2-6   -  -  -         
23 2-6   -  -  -         
24 3-6   X  -            
25 4-5   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NUQRA XUNAA NWWJY JAPAW WYQPJ N
-------------------------------
