EFFECTIVE PERIOD:
10-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   B  -  -  -  B  -
03 2-0   -  C  -  -  -  C
04 2-0   -  -  D  -  D  D
05 2-0   -  -  -  -  -  E
06 2-0   -  -  F  F  -  F
07 2-0   G  G  -  G  -  -
08 2-0   -  -  H  H  H  H
09 2-0   -  I  -  I  I  -
10 2-0   -  -  J  J  -  J
11 2-0   -  -  -  -  K  -
12 2-0   L  L  -  L  -  L
13 2-0   -  M  -  M  M  -
14 0-3   N  N  N  -  -  -
15 0-3   O  O  O  O  -  O
16 0-4   -  -  -  -  -  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  -  R  R  -   
19 0-5   S  -  -  -  -   
20 0-5   -  T  -  T      
21 0-5   -  -  U  U      
22 0-5   V  V  -         
23 0-5   W  -  -         
24 1-3   -  Y            
25 1-5   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QONGF VETKW EWEOC NKTKO KCOQZ R
-------------------------------
