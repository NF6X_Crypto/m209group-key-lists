EFFECTIVE PERIOD:
17-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  B  -  -  -  -
03 1-0   -  -  C  -  -  C
04 1-0   -  D  D  D  -  -
05 2-0   -  E  -  E  -  E
06 2-0   F  F  F  F  F  F
07 2-0   G  G  G  G  G  -
08 2-0   -  -  H  -  -  H
09 2-0   -  -  I  -  -  I
10 0-3   J  -  J  -  -  J
11 0-3   K  -  -  -  K  -
12 0-5   L  -  -  L  L  -
13 0-5   -  M  -  M  -  -
14 0-5   N  N  -  N  N  -
15 0-5   -  O  -  -  O  -
16 1-2   P  P  P  P  P  -
17 1-5   -  -  -  Q  Q  Q
18 1-5   -  -  -  -  R   
19 1-5   S  -  S  S  -   
20 1-5   -  T  -  -      
21 2-3   U  -  -  -      
22 3-5   V  V  V         
23 3-5   W  -  X         
24 3-6   -  -            
25 4-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

XHPRM ZSOPR LMOQU OWTPP JRRUX W
-------------------------------
