EFFECTIVE PERIOD:
07-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  -  -  -  -  B
03 1-0   C  -  -  C  -  -
04 1-0   -  D  -  D  -  -
05 1-0   -  E  E  -  E  -
06 2-0   F  F  -  -  -  F
07 2-0   -  G  -  G  G  G
08 0-3   -  H  H  -  -  -
09 0-3   -  I  -  -  I  I
10 0-3   J  -  J  -  -  -
11 0-3   K  -  K  -  -  -
12 0-4   L  -  L  L  L  L
13 0-4   M  M  -  -  M  M
14 0-4   -  -  N  -  N  -
15 0-4   -  O  -  -  -  -
16 0-4   -  -  -  -  P  -
17 0-4   Q  -  Q  -  -  Q
18 1-2   R  R  -  R  -   
19 1-3   S  S  -  S  -   
20 1-3   -  T  T  T      
21 1-3   -  U  U  U      
22 1-3   -  V  V         
23 1-5   -  -  X         
24 1-6   X  -            
25 2-4   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

OMMUF LUTYX NTKMO MPNFP XQLUZ F
-------------------------------
