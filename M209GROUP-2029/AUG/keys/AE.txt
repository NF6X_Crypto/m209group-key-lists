EFFECTIVE PERIOD:
23-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  -  -  -  -  -
03 1-0   -  C  -  C  -  C
04 1-0   D  -  -  -  D  -
05 2-0   -  -  -  -  E  E
06 0-4   -  F  F  F  F  F
07 0-4   G  -  -  G  -  G
08 0-4   H  -  -  H  H  -
09 0-4   I  -  I  -  I  -
10 0-4   -  J  -  -  -  J
11 0-5   K  K  K  -  -  K
12 0-5   L  L  -  L  -  -
13 0-5   -  M  M  M  M  M
14 0-5   N  N  N  N  N  -
15 0-5   -  -  O  O  O  O
16 1-2   P  P  P  P  -  -
17 1-5   Q  -  -  -  Q  -
18 2-4   -  R  R  R  -   
19 2-4   -  S  S  -  -   
20 2-6   -  T  -  -      
21 3-6   -  U  U  U      
22 4-5   -  -  -         
23 4-5   W  -  -         
24 4-5   -  -            
25 4-5   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UAUVH WEJSU AASRS TWJGJ WAAQA A
-------------------------------
