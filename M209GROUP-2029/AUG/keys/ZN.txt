EFFECTIVE PERIOD:
06-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   -  B  B  B  -  -
03 0-4   -  C  -  -  -  -
04 0-4   D  -  -  -  D  D
05 0-4   -  E  -  E  -  E
06 0-4   -  F  -  -  -  -
07 0-5   -  G  -  -  G  G
08 0-5   H  -  -  H  -  -
09 0-5   I  -  I  -  -  -
10 0-5   J  -  -  -  J  J
11 0-6   K  K  K  K  K  -
12 0-6   -  -  L  L  -  L
13 0-6   M  M  M  -  M  -
14 0-6   N  N  N  -  -  -
15 0-6   O  O  O  O  O  O
16 0-6   P  -  -  P  P  P
17 0-6   -  Q  Q  Q  -  Q
18 1-3   -  R  -  -  R   
19 1-5   -  S  S  -  S   
20 2-3   -  -  T  T      
21 3-6   -  U  -  -      
22 3-6   -  -  V         
23 4-5   W  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

GUJRX OUQJA WWASI ASGWM TRUAP R
-------------------------------
