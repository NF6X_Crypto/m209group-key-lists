EFFECTIVE PERIOD:
19-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  -  A
02 0-3   -  B  -  -  -  -
03 0-3   C  C  -  C  C  C
04 0-3   -  -  D  -  -  D
05 0-4   E  E  -  E  E  -
06 0-4   F  -  F  F  F  F
07 0-4   -  -  -  G  G  -
08 0-4   -  H  H  H  -  H
09 0-5   I  I  -  I  I  -
10 0-6   -  J  -  -  -  J
11 0-6   -  K  K  -  -  -
12 0-6   L  L  -  -  L  L
13 0-6   M  -  -  -  M  -
14 0-6   -  N  N  N  N  N
15 0-6   -  O  -  -  O  -
16 1-2   P  P  P  -  P  P
17 1-4   Q  Q  -  Q  -  Q
18 1-5   R  R  R  -  -   
19 3-4   -  -  S  S  -   
20 3-4   T  T  T  -      
21 3-4   U  -  U  U      
22 3-4   -  -  V         
23 3-6   -  -  -         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QWQNU SXPQK UNNUV SWQIH VGWWI R
-------------------------------
