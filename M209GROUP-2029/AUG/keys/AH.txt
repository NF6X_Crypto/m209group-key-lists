EFFECTIVE PERIOD:
26-AUG-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   A  -  -  -  A  A
02 0-4   B  B  B  -  -  -
03 0-4   -  C  -  C  C  C
04 0-4   -  -  -  D  -  -
05 0-5   -  E  E  -  -  E
06 0-5   F  F  F  -  F  F
07 0-5   G  -  -  -  G  G
08 0-5   H  -  H  H  H  -
09 0-5   -  I  -  -  -  -
10 0-6   -  J  J  J  -  J
11 0-6   -  -  -  K  -  -
12 0-6   -  -  -  L  L  L
13 0-6   -  -  M  M  -  M
14 0-6   N  N  N  N  -  -
15 0-6   O  -  O  O  O  O
16 1-2   -  P  -  -  -  -
17 1-3   Q  -  Q  -  Q  -
18 1-4   R  R  R  R  R   
19 2-4   -  -  -  S  -   
20 2-4   -  T  T  T      
21 2-5   -  U  -  -      
22 4-5   -  V  -         
23 4-6   W  X  -         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TVULL TUVKQ MUVUI LVCUA NSTMW K
-------------------------------
