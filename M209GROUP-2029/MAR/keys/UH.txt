EFFECTIVE PERIOD:
23-MAR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  B  B  B  -  -
03 1-0   C  C  C  -  -  -
04 1-0   -  -  -  -  -  D
05 1-0   E  -  E  -  E  E
06 1-0   F  -  -  F  -  F
07 1-0   -  -  -  -  G  -
08 2-0   H  -  -  H  -  -
09 0-3   -  I  I  I  I  -
10 0-3   J  -  -  J  -  -
11 0-3   K  K  -  -  -  K
12 0-3   -  -  L  -  L  -
13 0-3   -  -  M  M  -  M
14 0-5   -  N  -  -  N  N
15 0-5   O  O  O  O  O  -
16 0-5   P  -  P  P  -  -
17 0-5   -  Q  Q  Q  Q  -
18 0-6   -  R  -  -  R   
19 0-6   -  -  -  -  S   
20 0-6   -  T  T  T      
21 0-6   U  -  -  -      
22 1-3   -  -  V         
23 1-3   W  -  -         
24 2-4   -  -            
25 2-5   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VHTTA QNUHZ GWALO IPGPM OWNAT W
-------------------------------
