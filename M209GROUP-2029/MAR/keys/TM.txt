EFFECTIVE PERIOD:
02-MAR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   -  B  B  -  B  -
03 1-0   -  -  -  -  -  C
04 1-0   D  -  D  -  -  -
05 2-0   -  -  E  -  E  E
06 2-0   -  F  F  -  -  -
07 2-0   G  G  -  G  G  G
08 2-0   H  H  -  H  -  -
09 2-0   I  -  I  -  I  I
10 0-3   -  -  -  -  J  -
11 0-3   K  K  -  K  K  -
12 0-4   L  L  -  -  -  L
13 0-6   M  -  M  M  M  M
14 0-6   -  N  -  -  N  -
15 0-6   -  O  O  -  O  O
16 0-6   P  -  -  P  P  P
17 0-6   Q  -  -  Q  -  Q
18 0-6   -  R  -  -  -   
19 0-6   S  -  -  -  -   
20 1-2   -  -  T  T      
21 1-2   U  -  U  U      
22 1-2   V  -  V         
23 1-2   W  X  -         
24 1-6   -  Y            
25 2-5   -  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

QUJQL QTNOU MTJLN MLOZS AYZRO L
-------------------------------
