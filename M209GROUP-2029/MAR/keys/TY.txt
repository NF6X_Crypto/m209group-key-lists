EFFECTIVE PERIOD:
14-MAR-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  D  D  D  -
05 1-0   E  -  -  -  -  -
06 1-0   -  -  F  F  F  -
07 0-5   -  G  -  -  -  -
08 0-5   -  -  H  H  -  H
09 0-5   -  I  I  -  -  I
10 0-5   J  J  J  J  J  -
11 0-5   K  -  K  K  -  K
12 0-6   -  -  -  -  L  L
13 0-6   -  M  -  M  -  M
14 0-6   N  -  N  -  N  N
15 0-6   O  O  -  -  O  O
16 0-6   P  -  P  P  P  -
17 0-6   -  Q  Q  -  -  Q
18 1-3   R  -  -  R  R   
19 1-5   S  S  S  -  S   
20 1-5   T  -  -  -      
21 1-6   -  U  -  -      
22 2-5   V  -  V         
23 3-4   -  X  X         
24 5-6   X  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AKAAZ BZSVN UTCUL UAUMU VALUU A
-------------------------------
