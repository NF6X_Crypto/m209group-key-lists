EFFECTIVE PERIOD:
13-FEB-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 2-0   -  B  B  B  -  B
03 2-0   C  -  -  -  -  -
04 2-0   -  -  D  -  -  D
05 2-0   -  -  E  -  -  -
06 2-0   F  -  -  F  F  F
07 2-0   -  G  -  G  -  G
08 2-0   H  -  -  H  H  -
09 0-3   -  -  -  I  -  -
10 0-3   J  J  -  J  J  J
11 0-3   K  -  K  -  K  -
12 0-3   L  -  -  L  -  -
13 0-4   M  -  M  -  M  M
14 0-4   N  -  -  N  N  N
15 0-5   O  O  O  -  -  -
16 0-6   -  -  P  P  P  P
17 0-6   Q  -  -  -  Q  Q
18 0-6   R  R  -  -  R   
19 0-6   S  S  S  -  -   
20 0-6   -  T  T  -      
21 0-6   -  -  U  -      
22 0-6   V  V  V         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

GJRVI XRVXI VLQRH EOLFJ ACSMT Y
-------------------------------
