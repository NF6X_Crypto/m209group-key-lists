EFFECTIVE PERIOD:
09-FEB-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 2-0   -  B  -  B  B  -
03 2-0   C  -  C  -  -  C
04 2-0   D  -  -  -  D  -
05 0-3   E  E  -  -  -  E
06 0-3   -  -  F  F  -  F
07 0-3   -  G  -  -  -  -
08 0-3   H  H  -  H  -  H
09 0-3   I  -  I  -  I  -
10 0-3   J  -  J  J  -  -
11 0-3   K  K  -  K  -  K
12 0-3   L  -  L  -  L  -
13 0-3   -  -  M  M  -  -
14 0-4   -  N  N  -  -  N
15 0-4   -  O  O  O  O  O
16 0-4   -  P  P  P  P  P
17 0-4   -  Q  -  -  Q  Q
18 0-4   -  -  R  -  R   
19 0-4   S  S  S  S  -   
20 0-5   -  -  -  -      
21 0-6   -  U  -  -      
22 0-6   V  -  V         
23 0-6   -  -  X         
24 1-6   X  -            
25 2-4   -  -            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

ZOUOT PRELP JFSBN QIIWT FZQWZ P
-------------------------------
