EFFECTIVE PERIOD:
17-FEB-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  -  B  -  -  B
03 1-0   -  -  C  -  -  C
04 1-0   D  D  D  D  -  -
05 1-0   E  -  E  -  E  -
06 1-0   F  F  -  -  -  F
07 1-0   -  -  -  G  G  G
08 2-0   -  -  -  H  H  H
09 0-3   -  -  -  I  -  -
10 0-3   -  -  J  -  J  -
11 0-3   K  -  K  -  K  -
12 0-3   L  L  -  L  L  -
13 0-4   -  M  M  M  -  M
14 0-4   -  N  -  -  N  -
15 0-4   -  O  -  O  -  -
16 0-4   -  -  P  -  P  P
17 0-4   Q  Q  -  Q  -  -
18 0-5   R  R  -  R  R   
19 0-5   S  S  -  -  S   
20 0-5   -  T  T  T      
21 0-6   U  -  U  U      
22 1-5   -  -  V         
23 1-5   W  X  -         
24 1-5   -  -            
25 3-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

NJAKN TMYNL APSGV TRMGN LTRNA T
-------------------------------
