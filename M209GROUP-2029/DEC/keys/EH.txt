EFFECTIVE PERIOD:
08-DEC-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  B  B  B  B
03 1-0   C  C  -  -  -  C
04 1-0   -  -  D  D  D  -
05 1-0   -  -  E  -  E  E
06 1-0   -  -  -  -  F  F
07 1-0   -  G  G  -  -  G
08 1-0   H  -  -  H  H  H
09 0-3   I  I  -  -  I  I
10 0-3   -  -  -  J  -  -
11 0-3   K  -  -  -  -  -
12 0-3   L  -  -  -  -  L
13 0-3   -  -  M  M  M  -
14 0-3   -  -  N  -  -  N
15 0-4   O  O  O  O  -  -
16 0-4   P  -  -  P  P  -
17 0-4   Q  Q  -  Q  -  Q
18 0-4   R  R  -  R  R   
19 0-4   -  -  S  S  -   
20 0-4   -  -  -  T      
21 0-4   -  U  U  -      
22 0-4   V  V  -         
23 0-4   -  X  X         
24 0-5   X  Y            
25 0-6   Y  -            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

YRSLK PUQLR IJMRH CRBRJ CQOSP U
-------------------------------
