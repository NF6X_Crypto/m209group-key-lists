EFFECTIVE PERIOD:
05-DEC-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 2-0   B  -  -  B  -  -
03 2-0   C  C  C  -  C  -
04 2-0   -  D  D  D  D  -
05 2-0   E  -  E  E  -  -
06 2-0   -  F  -  -  -  -
07 2-0   G  -  -  -  -  G
08 2-0   H  -  H  H  -  -
09 2-0   I  -  I  -  -  -
10 2-0   -  J  -  J  J  J
11 2-0   -  K  -  -  -  K
12 0-3   L  L  -  L  -  L
13 0-3   -  -  M  -  M  -
14 0-4   -  N  N  N  N  N
15 0-4   -  -  O  -  O  -
16 0-4   -  -  P  -  P  -
17 0-4   Q  -  -  Q  Q  Q
18 0-5   -  -  -  R  -   
19 0-6   S  -  -  -  -   
20 0-6   T  -  -  -      
21 0-6   U  U  U  U      
22 1-3   -  V  -         
23 2-4   W  -  -         
24 2-4   X  Y            
25 2-4   Y  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

HQEHK KRVKO XNMZQ BVJPB WNXIV L
-------------------------------
