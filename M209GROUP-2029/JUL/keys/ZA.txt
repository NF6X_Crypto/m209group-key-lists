EFFECTIVE PERIOD:
24-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  B  B  -  B  B
03 1-0   -  -  -  -  C  C
04 2-0   D  -  -  -  D  D
05 2-0   E  E  E  -  E  -
06 2-0   -  -  F  F  -  -
07 2-0   -  -  G  G  -  -
08 0-4   H  H  H  -  -  -
09 0-4   -  -  -  -  -  -
10 0-4   J  -  -  J  -  -
11 0-4   K  -  -  K  K  K
12 0-5   L  L  -  L  L  -
13 0-6   M  M  M  M  M  M
14 0-6   -  -  -  -  -  -
15 0-6   O  -  O  O  -  O
16 0-6   -  P  P  P  -  P
17 0-6   -  Q  -  -  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   S  S  S  S  S   
20 0-6   -  -  -  T      
21 0-6   U  U  U  -      
22 1-3   -  V  V         
23 1-4   W  X  -         
24 2-4   -  -            
25 2-6   Y  Z            
26 2-6   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

SJWQR RVRRR WJNZJ QAMRS KVVZH Q
-------------------------------
