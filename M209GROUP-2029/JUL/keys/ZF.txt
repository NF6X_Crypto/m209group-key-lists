EFFECTIVE PERIOD:
29-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  -  B  B  -  B
03 1-0   C  -  -  C  C  C
04 2-0   D  D  D  D  D  D
05 0-3   E  E  -  E  E  -
06 0-3   -  -  F  -  F  -
07 0-3   -  G  -  G  -  -
08 0-3   H  -  -  -  -  H
09 0-4   I  I  I  I  I  I
10 0-4   J  -  -  -  -  -
11 0-4   -  -  K  -  K  K
12 0-4   -  L  L  L  -  -
13 0-4   M  -  -  M  -  M
14 0-6   -  N  -  N  N  -
15 0-6   -  O  -  -  -  O
16 0-6   -  -  -  -  -  -
17 0-6   -  -  Q  Q  -  Q
18 0-6   -  -  R  -  R   
19 0-6   -  -  -  -  -   
20 1-2   T  T  -  -      
21 1-6   -  U  U  U      
22 3-4   V  -  -         
23 3-4   -  X  X         
24 3-4   X  -            
25 3-4   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TJIIZ BNIWV ZUOTM AATOM TVLLB T
-------------------------------
