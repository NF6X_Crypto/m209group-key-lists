EFFECTIVE PERIOD:
01-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  -  -  -  -  B
03 1-0   C  -  C  C  -  C
04 1-0   -  -  D  D  -  D
05 1-0   -  E  E  -  E  E
06 1-0   -  F  F  F  F  -
07 1-0   -  -  G  G  G  -
08 1-0   H  -  H  H  H  -
09 1-0   I  -  -  -  I  I
10 2-0   J  J  -  J  -  J
11 0-3   -  K  -  K  -  K
12 0-4   L  -  L  -  L  L
13 0-5   M  M  -  -  M  -
14 0-5   -  N  N  N  N  N
15 0-5   O  -  -  -  -  O
16 0-5   P  -  P  P  -  -
17 0-5   -  -  -  -  Q  -
18 0-6   R  -  -  R  -   
19 0-6   S  -  S  -  -   
20 0-6   -  T  -  T      
21 0-6   -  U  U  -      
22 1-5   -  -  -         
23 1-5   W  X  X         
24 1-5   -  -            
25 2-3   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

YOUSF NBPPH RUMUM RRHSS YQMPL T
-------------------------------
