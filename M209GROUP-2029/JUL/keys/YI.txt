EFFECTIVE PERIOD:
06-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  -  -  B  -  B
03 1-0   C  -  C  -  C  -
04 1-0   D  D  D  -  -  -
05 1-0   E  -  -  -  E  E
06 0-3   -  F  -  -  -  F
07 0-3   -  G  -  -  -  -
08 0-3   H  H  H  H  H  H
09 0-5   -  -  -  I  -  -
10 0-5   J  -  -  -  -  J
11 0-5   -  K  K  K  K  -
12 0-5   L  -  -  L  -  L
13 0-5   -  M  -  -  M  M
14 0-5   -  N  N  N  N  -
15 0-5   -  -  O  -  O  -
16 0-6   -  -  -  P  P  P
17 0-6   Q  Q  -  -  Q  Q
18 0-6   R  -  -  -  -   
19 0-6   S  S  S  S  S   
20 1-5   T  -  T  T      
21 1-5   -  -  -  -      
22 1-5   -  V  V         
23 1-5   W  X  -         
24 1-6   -  Y            
25 2-3   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

EAXRS VKSVL WFVOP RHFXG WMWVQ F
-------------------------------
