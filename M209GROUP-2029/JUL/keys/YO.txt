EFFECTIVE PERIOD:
12-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 2-0   B  B  -  -  -  B
03 2-0   -  C  -  -  -  C
04 2-0   D  D  -  D  D  -
05 2-0   E  E  -  -  -  E
06 0-4   -  -  F  -  F  -
07 0-4   G  -  -  G  G  -
08 0-4   -  H  H  H  H  H
09 0-4   I  -  -  -  -  I
10 0-4   -  J  -  -  -  -
11 0-5   K  K  K  -  -  K
12 0-5   L  -  L  L  -  -
13 0-5   -  M  M  M  -  -
14 0-5   N  -  N  N  N  -
15 0-5   -  -  -  O  -  -
16 1-6   P  -  P  P  P  P
17 2-3   -  Q  -  -  -  -
18 2-3   R  R  -  -  R   
19 2-4   -  S  S  S  -   
20 2-5   T  -  T  -      
21 2-5   -  U  -  U      
22 2-5   -  -  V         
23 2-5   -  X  X         
24 2-6   -  -            
25 3-4   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MHATT FZZAF AMUSU AHFMA UAMZI P
-------------------------------
