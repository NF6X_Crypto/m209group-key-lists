EFFECTIVE PERIOD:
08-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 2-0   B  B  B  -  B  -
03 2-0   C  C  -  C  -  -
04 2-0   -  -  D  D  D  -
05 0-3   E  E  -  E  -  E
06 0-3   -  -  F  -  -  F
07 0-4   -  G  -  G  G  G
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  -  I  -
10 0-4   J  -  -  J  J  -
11 0-5   K  K  K  -  K  K
12 0-6   -  -  -  -  -  L
13 0-6   M  M  M  -  M  -
14 0-6   -  N  -  -  N  N
15 0-6   O  -  O  -  O  -
16 0-6   -  P  P  P  -  P
17 0-6   Q  Q  -  Q  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  -  S   
20 0-6   -  -  -  T      
21 0-6   U  U  U  U      
22 1-3   -  -  V         
23 2-4   -  X  -         
24 2-6   X  -            
25 2-6   Y  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

MWMTL RGWPC ZWXZA PGVWI SJRIY J
-------------------------------
