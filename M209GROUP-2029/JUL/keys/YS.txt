EFFECTIVE PERIOD:
16-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  B  -  -  B  -
03 1-0   C  -  C  C  -  -
04 1-0   D  -  D  -  D  D
05 1-0   -  -  -  -  -  -
06 1-0   -  -  F  -  F  F
07 2-0   G  G  -  -  G  -
08 0-3   -  -  -  H  -  H
09 0-3   -  I  I  I  -  -
10 0-4   -  J  -  J  -  -
11 0-4   K  -  K  K  K  K
12 0-4   -  -  -  -  L  -
13 0-5   -  M  -  -  -  M
14 0-5   N  -  N  -  -  -
15 0-5   -  -  -  O  -  O
16 0-5   -  -  P  -  -  P
17 0-5   Q  Q  Q  Q  -  -
18 0-5   R  -  R  R  R   
19 0-5   -  S  -  -  S   
20 0-5   T  -  T  -      
21 0-6   -  U  -  U      
22 1-4   -  -  V         
23 1-5   W  X  X         
24 1-5   X  Y            
25 1-5   Y  Z            
26 2-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

PJSDS FPRRW CXFVS DEQSS EKAIJ S
-------------------------------
