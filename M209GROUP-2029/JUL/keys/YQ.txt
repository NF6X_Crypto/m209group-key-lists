EFFECTIVE PERIOD:
14-JUL-2029 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 0-3   -  -  -  -  B  B
03 0-3   C  C  C  -  C  C
04 0-3   D  -  -  D  -  D
05 0-3   E  E  E  -  E  -
06 0-4   -  -  F  -  -  F
07 0-4   G  -  -  -  -  -
08 0-4   H  -  -  H  -  -
09 0-4   I  -  I  -  -  I
10 0-5   J  -  J  J  -  -
11 0-5   K  -  K  K  K  K
12 0-5   -  L  -  L  L  L
13 0-5   -  M  M  -  -  -
14 0-5   -  N  -  N  N  -
15 0-5   -  -  -  -  -  -
16 1-4   -  P  -  P  P  P
17 1-4   -  Q  Q  Q  -  -
18 1-4   R  R  -  -  -   
19 1-5   -  S  -  -  -   
20 1-6   T  T  -  -      
21 2-4   U  U  U  U      
22 3-4   -  -  V         
23 3-4   W  X  -         
24 3-4   -  Y            
25 3-4   Y  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JNNRW HAOZW VGZGO QMGAA UMSIM N
-------------------------------
