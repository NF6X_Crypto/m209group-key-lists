EFFECTIVE PERIOD:
22-NOV-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  B  -  -  B  -
03 1-0   -  -  -  C  -  -
04 2-0   -  D  D  D  D  D
05 0-4   E  -  -  -  -  E
06 0-4   F  -  -  F  F  F
07 0-4   G  G  G  -  G  -
08 0-4   -  -  -  -  -  H
09 0-4   -  -  -  I  -  I
10 0-4   -  -  -  -  -  J
11 0-4   -  K  -  K  K  -
12 0-5   -  L  L  L  -  L
13 0-5   -  -  -  -  -  -
14 0-5   N  N  N  N  N  N
15 0-6   O  -  O  -  -  O
16 0-6   -  P  -  -  -  -
17 0-6   Q  -  -  Q  -  -
18 0-6   R  -  -  R  R   
19 0-6   -  S  S  -  -   
20 1-4   T  T  T  T      
21 1-4   -  U  U  U      
22 1-4   V  V  V         
23 1-4   -  -  X         
24 1-6   X  Y            
25 2-5   Y  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AXMGX NONUV QWUFX NORKL SMNAK U
-------------------------------
