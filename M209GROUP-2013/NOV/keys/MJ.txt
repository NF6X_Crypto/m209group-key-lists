EFFECTIVE PERIOD:
08-NOV-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   B  -  B  -  -  -
03 1-0   -  C  -  C  -  -
04 1-0   D  D  -  -  -  -
05 1-0   -  E  -  E  -  E
06 1-0   F  F  -  -  F  -
07 1-0   G  -  -  G  G  G
08 1-0   H  -  H  H  -  -
09 1-0   -  I  -  I  -  I
10 1-0   -  J  -  J  -  J
11 2-0   -  -  -  -  K  -
12 0-3   -  -  -  L  L  -
13 0-4   -  -  M  M  M  M
14 0-5   -  N  -  N  -  -
15 0-5   O  -  O  O  O  O
16 0-5   P  -  P  P  -  P
17 0-5   Q  -  -  -  Q  Q
18 0-5   -  R  R  -  R   
19 0-5   -  S  S  -  -   
20 0-5   -  -  T  -      
21 0-5   U  -  U  -      
22 0-5   V  V  -         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   -  -            
26 2-4   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

UVGMG XXNJE ARJPV GQMUO MFPFN T
-------------------------------
