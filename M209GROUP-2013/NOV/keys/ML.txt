EFFECTIVE PERIOD:
10-NOV-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ML
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   -  -  -  B  -  B
03 0-3   C  -  -  C  C  C
04 0-3   D  -  D  -  D  -
05 0-3   -  -  -  -  -  E
06 0-3   F  F  F  F  -  F
07 0-3   G  -  -  G  -  G
08 0-3   H  -  -  -  -  -
09 0-3   I  -  I  I  I  I
10 0-3   J  -  J  J  -  -
11 0-3   -  K  -  -  -  K
12 0-3   L  -  L  L  L  -
13 0-4   M  M  -  M  -  -
14 0-4   -  N  -  -  -  -
15 0-4   -  O  -  O  O  -
16 0-4   -  P  -  -  P  P
17 0-5   -  Q  -  -  Q  -
18 0-6   R  R  R  -  R   
19 0-6   -  -  -  -  -   
20 0-6   -  T  T  T      
21 0-6   -  U  -  -      
22 0-6   -  -  V         
23 0-6   -  -  X         
24 1-5   X  -            
25 3-6   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

PAIDN LOFVT UPHTH WEOSO HRLJD L
-------------------------------
