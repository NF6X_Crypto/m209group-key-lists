EFFECTIVE PERIOD:
17-NOV-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 1-0   -  B  B  B  B  -
03 1-0   C  -  -  C  -  -
04 1-0   -  -  -  -  -  -
05 1-0   E  -  -  -  E  -
06 1-0   F  F  F  -  -  -
07 1-0   G  G  -  G  G  -
08 0-3   H  H  H  H  H  H
09 0-4   I  I  I  -  I  I
10 0-4   -  -  J  -  J  -
11 0-5   K  K  -  -  K  -
12 0-5   L  -  -  -  L  L
13 0-5   -  -  -  M  -  -
14 0-5   -  N  -  -  N  N
15 0-5   O  -  O  O  O  -
16 0-5   P  -  P  -  -  P
17 0-5   Q  -  Q  Q  -  Q
18 0-5   -  R  -  -  -   
19 0-5   -  S  -  S  S   
20 0-5   T  T  T  T      
21 0-6   U  -  -  U      
22 1-4   -  -  -         
23 1-5   -  -  X         
24 1-5   -  Y            
25 2-3   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WOSQN NHRDS PPMJO AXWYW CXWFE P
-------------------------------
