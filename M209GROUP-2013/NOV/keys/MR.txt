EFFECTIVE PERIOD:
16-NOV-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  B  -  B  -  B
03 1-0   -  C  C  -  C  C
04 1-0   -  D  D  D  -  D
05 1-0   E  E  -  E  E  E
06 1-0   -  -  -  -  F  -
07 2-0   -  G  -  -  -  G
08 0-3   H  -  H  -  -  H
09 0-3   -  I  I  I  -  -
10 0-3   J  -  -  J  -  -
11 0-3   -  -  K  -  K  -
12 0-3   L  L  -  -  L  L
13 0-4   -  -  -  -  -  M
14 0-5   N  -  -  N  N  -
15 0-5   O  -  O  -  -  -
16 0-5   -  -  -  -  P  -
17 0-5   Q  Q  Q  -  Q  -
18 0-5   -  R  R  R  -   
19 0-5   -  -  -  S  -   
20 0-5   T  -  -  T      
21 0-5   U  U  -  -      
22 0-6   -  -  V         
23 0-6   -  X  X         
24 1-3   -  -            
25 1-5   -  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

IRNHV RCETJ ULYUE IZJZN NIKIY L
-------------------------------
