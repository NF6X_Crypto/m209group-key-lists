EFFECTIVE PERIOD:
27-AUG-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  B  B  B  -  B
03 1-0   -  -  -  -  C  C
04 1-0   D  D  D  D  D  D
05 1-0   E  -  -  -  E  E
06 1-0   F  F  -  -  -  F
07 1-0   G  -  G  G  G  -
08 1-0   -  H  -  H  H  -
09 1-0   -  -  I  I  I  -
10 1-0   -  -  J  -  -  -
11 1-0   K  K  K  -  K  K
12 1-0   L  L  L  -  L  -
13 0-3   -  -  -  M  -  -
14 0-4   N  N  N  -  -  -
15 0-4   -  O  -  -  O  O
16 0-4   P  -  P  -  P  P
17 0-4   Q  Q  Q  -  -  -
18 0-4   R  R  R  R  -   
19 0-4   -  S  -  S  -   
20 0-4   -  -  -  T      
21 0-4   -  U  -  -      
22 0-5   -  V  V         
23 0-6   W  -  X         
24 1-4   -  -            
25 2-3   -  Z            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AMPDF LLXEE NFNBP WZXDC BZRCO E
-------------------------------
