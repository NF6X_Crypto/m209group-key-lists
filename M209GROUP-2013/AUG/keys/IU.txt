EFFECTIVE PERIOD:
07-AUG-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  B  B  -  B
03 1-0   -  C  -  -  -  C
04 1-0   -  -  D  -  D  -
05 1-0   -  -  -  E  E  -
06 1-0   -  F  -  F  F  -
07 1-0   G  -  -  G  G  -
08 1-0   H  H  H  H  H  H
09 2-0   -  I  -  -  -  I
10 0-3   -  J  -  -  J  J
11 0-3   K  K  -  K  K  K
12 0-3   L  -  L  -  -  -
13 0-3   -  -  -  M  M  -
14 0-3   N  N  -  -  -  -
15 0-4   -  O  O  O  O  O
16 0-4   P  -  P  P  P  P
17 0-4   Q  -  -  -  -  Q
18 0-4   -  R  -  R  R   
19 0-5   -  S  S  -  -   
20 0-6   T  T  T  -      
21 0-6   U  U  -  -      
22 1-4   -  V  V         
23 1-4   W  -  X         
24 1-4   X  Y            
25 3-4   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UTNKQ ZLLUZ NJMQM MMVNI CZPMW L
-------------------------------
