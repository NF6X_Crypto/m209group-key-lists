EFFECTIVE PERIOD:
31-AUG-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   -  B  -  -  -  -
03 1-0   -  -  -  -  -  -
04 1-0   D  D  D  D  -  D
05 2-0   E  E  E  -  E  -
06 2-0   -  F  -  F  -  F
07 2-0   G  -  G  -  G  G
08 2-0   H  -  H  H  H  H
09 2-0   -  I  -  -  -  I
10 0-3   -  -  J  -  J  J
11 0-3   -  -  K  -  K  -
12 0-3   L  L  -  -  -  -
13 0-3   M  -  -  -  -  -
14 0-3   -  -  N  N  -  N
15 0-3   O  O  -  O  O  O
16 0-3   P  -  P  P  P  P
17 0-5   Q  -  Q  Q  -  Q
18 0-5   R  R  -  -  R   
19 0-5   -  S  -  S  S   
20 1-2   T  -  -  T      
21 1-6   -  U  U  U      
22 2-5   -  -  -         
23 3-4   W  X  X         
24 3-5   -  -            
25 3-5   -  Z            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

XGRWT APVOL TGPMA STVNS MATVA A
-------------------------------
