EFFECTIVE PERIOD:
09-JAN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  -  -  -  -  -
04 1-0   -  -  -  -  D  -
05 1-0   -  E  E  E  E  E
06 2-0   F  F  -  F  -  F
07 0-3   G  G  -  G  G  G
08 0-4   H  H  H  H  -  -
09 0-4   -  I  -  I  I  -
10 0-4   J  -  J  -  -  J
11 0-4   K  -  K  K  K  K
12 0-4   L  L  -  L  -  L
13 0-5   -  -  -  M  M  M
14 0-5   -  -  -  N  -  -
15 0-5   -  O  -  O  -  O
16 0-5   -  -  P  P  P  P
17 0-5   -  -  Q  -  -  -
18 0-5   -  -  R  -  R   
19 0-5   S  S  S  -  -   
20 0-6   T  T  -  -      
21 0-6   -  U  -  -      
22 1-4   -  -  V         
23 1-6   -  X  X         
24 2-6   -  Y            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NTNPV FLDMJ OPORQ KZTUO WRSPM M
-------------------------------
