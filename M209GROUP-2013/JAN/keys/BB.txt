EFFECTIVE PERIOD:
18-JAN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   -  B  -  B  B  -
03 1-0   C  -  -  C  -  C
04 1-0   -  -  D  -  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  -  F  -  F  F
07 2-0   -  -  -  -  -  -
08 0-3   -  -  -  H  -  H
09 0-3   -  I  I  I  -  -
10 0-3   J  -  -  -  -  J
11 0-4   K  -  -  K  -  K
12 0-5   -  L  L  L  L  -
13 0-5   -  -  -  -  -  -
14 0-5   -  N  N  N  -  N
15 0-5   -  -  -  O  O  O
16 0-5   P  -  P  -  -  -
17 0-5   -  -  -  -  Q  -
18 0-5   R  R  R  -  -   
19 0-5   S  -  S  S  S   
20 0-5   T  -  T  -      
21 0-6   -  -  -  U      
22 1-3   V  V  V         
23 1-5   -  X  -         
24 1-5   X  Y            
25 1-5   -  -            
26 2-3   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

XBPWO QPOQP QPPWA GMCEP MOPGO E
-------------------------------
