EFFECTIVE PERIOD:
02-DEC-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   B  B  -  B  B  B
03 1-0   -  -  C  -  -  -
04 0-3   D  -  D  D  D  -
05 0-4   E  -  -  E  E  E
06 0-4   -  F  F  F  -  -
07 0-5   G  G  G  G  -  -
08 0-5   H  -  H  -  H  H
09 0-5   I  I  -  I  -  I
10 0-5   -  -  -  J  J  -
11 0-5   K  K  -  K  K  K
12 0-5   -  L  L  -  -  -
13 0-5   M  -  -  -  -  -
14 0-5   N  N  N  -  N  N
15 0-5   O  -  -  -  -  O
16 0-5   -  -  -  P  P  -
17 0-5   Q  Q  -  -  Q  -
18 0-5   -  -  -  -  R   
19 0-6   S  -  S  -  -   
20 0-6   T  -  -  -      
21 0-6   -  U  -  U      
22 0-6   V  -  V         
23 0-6   -  -  -         
24 1-4   -  Y            
25 1-6   -  Z            
26 2-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

IEMMV ORFLM FWVFM ACATI FYATZ V
-------------------------------
