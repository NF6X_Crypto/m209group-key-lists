EFFECTIVE PERIOD:
12-DEC-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  -  B  -  B
03 1-0   C  C  -  C  C  C
04 1-0   -  -  D  D  -  -
05 0-3   -  E  -  -  -  E
06 0-3   -  -  -  -  F  F
07 0-3   -  G  -  -  -  -
08 0-3   -  H  -  -  -  -
09 0-4   -  I  I  I  -  I
10 0-4   J  -  -  -  J  -
11 0-4   K  -  -  -  K  -
12 0-4   L  L  L  L  L  -
13 0-4   M  M  -  M  M  -
14 0-5   -  -  N  -  -  N
15 0-5   -  O  O  -  O  O
16 0-5   P  P  P  -  P  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   R  -  -  R  R   
19 0-6   -  S  S  S  -   
20 1-4   T  T  -  -      
21 1-5   U  U  -  U      
22 1-5   -  V  V         
23 1-5   W  -  -         
24 1-5   X  Y            
25 2-5   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZUPQW ZSJUL VIQUZ CZQPJ WUOVP W
-------------------------------
