EFFECTIVE PERIOD:
11-APR-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  B  B  B  B  -
03 2-0   -  C  -  C  C  -
04 0-4   -  -  -  -  -  -
05 0-4   -  E  -  E  E  E
06 0-4   F  F  -  -  -  -
07 0-4   -  -  G  G  G  G
08 0-4   H  H  -  -  -  H
09 0-4   -  I  -  -  I  -
10 0-4   -  J  -  -  J  -
11 0-4   -  -  -  K  -  K
12 0-4   L  L  L  L  -  L
13 0-4   -  -  -  M  M  -
14 0-5   N  N  N  -  N  N
15 0-5   -  -  O  O  O  O
16 0-5   P  P  P  -  -  -
17 0-5   Q  -  -  Q  Q  -
18 0-5   -  R  -  -  -   
19 0-5   S  -  S  -  S   
20 0-5   T  -  -  T      
21 0-5   U  -  U  U      
22 1-3   V  -  V         
23 1-5   W  X  X         
24 3-6   -  Y            
25 4-5   -  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

RQXQR PDZPO WSPOM NQMFN XXPOQ O
-------------------------------
