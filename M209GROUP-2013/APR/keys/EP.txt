EFFECTIVE PERIOD:
20-APR-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  -  -  -  B  B
03 1-0   -  C  C  C  -  C
04 1-0   -  D  -  D  D  -
05 1-0   E  E  -  E  -  E
06 1-0   -  -  -  -  F  -
07 1-0   G  G  G  G  G  G
08 0-3   H  H  -  -  -  H
09 0-3   I  -  -  I  -  I
10 0-3   -  J  J  J  J  -
11 0-3   K  -  K  -  -  -
12 0-3   -  -  -  L  -  L
13 0-6   -  -  -  -  -  -
14 0-6   N  -  N  N  N  N
15 0-6   -  O  O  -  -  -
16 1-3   -  -  P  -  -  -
17 1-3   -  -  Q  Q  Q  -
18 1-3   -  -  -  R  -   
19 1-3   -  -  -  -  S   
20 1-6   T  T  T  T      
21 2-3   -  -  U  -      
22 2-4   V  V  -         
23 2-5   W  X  X         
24 3-4   -  Y            
25 3-4   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

OSNRP FUXIT OJWKI VMRVU TMDMV T
-------------------------------
