EFFECTIVE PERIOD:
22-FEB-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  -  -  -  -  -
03 1-0   C  -  -  -  C  -
04 1-0   D  D  D  -  D  -
05 2-0   E  -  -  -  -  E
06 2-0   -  -  F  F  F  -
07 0-4   G  -  -  G  -  -
08 0-4   -  -  H  H  -  -
09 0-4   -  -  -  -  I  I
10 0-4   -  -  -  J  J  J
11 0-4   -  K  K  -  K  K
12 0-4   L  -  L  -  -  L
13 0-4   -  M  -  M  M  M
14 0-4   -  N  -  -  N  -
15 0-5   O  O  -  O  -  O
16 0-5   P  P  P  P  P  P
17 0-5   Q  -  -  -  -  -
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  S  -   
20 0-5   T  T  T  T      
21 0-6   -  U  U  U      
22 1-2   -  V  V         
23 1-5   W  X  -         
24 2-3   X  -            
25 4-5   Y  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

USYOP EVTTF NSFPT SSQSA KWGVO R
-------------------------------
