EFFECTIVE PERIOD:
11-FEB-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 2-0   B  B  -  B  B  -
03 0-3   C  -  C  -  -  C
04 0-3   D  -  D  -  D  -
05 0-3   -  -  -  -  E  E
06 0-3   -  F  F  -  F  F
07 0-3   G  G  -  G  -  -
08 0-3   H  -  H  H  H  H
09 0-3   I  I  I  I  -  I
10 0-3   -  J  -  J  J  J
11 0-3   K  K  K  -  -  -
12 0-3   -  L  -  -  L  -
13 0-3   M  -  M  -  -  M
14 0-3   N  -  -  -  -  N
15 0-3   O  O  -  -  -  O
16 0-4   P  P  -  -  -  P
17 0-5   -  Q  -  Q  Q  -
18 0-5   -  -  -  R  R   
19 0-5   S  -  S  S  S   
20 0-6   -  -  -  T      
21 0-6   -  U  U  U      
22 0-6   V  V  V         
23 0-6   W  X  X         
24 0-6   -  Y            
25 0-6   -  Z            
26 1-2   Z               
27 1-5                   
-------------------------------
26 LETTER CHECK

GBHIG XHUPY RJZSI XIUQM UJUCF H
-------------------------------
