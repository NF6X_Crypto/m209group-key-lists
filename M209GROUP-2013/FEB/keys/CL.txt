EFFECTIVE PERIOD:
23-FEB-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   B  -  B  -  -  B
03 2-0   C  C  -  -  -  -
04 2-0   D  D  -  -  D  -
05 2-0   -  E  E  -  -  E
06 2-0   -  F  F  F  F  -
07 2-0   G  -  -  G  G  G
08 0-3   -  H  -  H  -  H
09 0-3   I  I  I  -  -  I
10 0-3   -  -  J  -  J  -
11 0-3   -  K  K  K  K  K
12 0-3   L  -  -  -  -  -
13 0-6   M  M  -  M  -  M
14 0-6   N  -  -  N  N  N
15 0-6   -  O  -  -  O  O
16 0-6   -  -  P  P  -  -
17 0-6   -  -  Q  Q  -  -
18 1-2   R  R  -  R  -   
19 1-3   S  -  -  -  S   
20 1-5   -  -  T  -      
21 2-4   U  -  U  -      
22 2-5   V  V  -         
23 2-6   -  -  -         
24 2-6   -  -            
25 2-6   -  -            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

SATAK EJULA JRDVA YMOUA PTFPE Y
-------------------------------
