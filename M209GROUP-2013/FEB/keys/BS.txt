EFFECTIVE PERIOD:
04-FEB-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  -  -  -  -
03 1-0   C  C  C  -  -  -
04 1-0   -  -  -  D  -  -
05 1-0   -  -  E  -  E  E
06 1-0   F  -  -  -  -  -
07 2-0   -  -  -  G  -  G
08 2-0   H  -  H  H  -  H
09 2-0   -  -  -  -  I  I
10 0-3   -  J  -  J  J  J
11 0-3   -  K  K  K  K  K
12 0-3   -  -  L  -  -  -
13 0-3   M  M  M  -  M  M
14 0-3   N  N  -  -  N  -
15 0-5   O  -  O  O  O  O
16 0-5   P  -  -  P  P  P
17 0-5   Q  -  Q  -  Q  -
18 1-5   R  -  R  -  -   
19 1-5   -  -  -  S  S   
20 1-5   T  T  -  -      
21 1-5   U  U  U  U      
22 1-6   -  -  V         
23 1-6   W  X  X         
24 2-3   X  Y            
25 2-6   -  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

NVROX VFVXL KHWRK DRRQD RURVA L
-------------------------------
