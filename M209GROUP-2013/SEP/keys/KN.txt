EFFECTIVE PERIOD:
21-SEP-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   -  -  -  -  B  -
03 1-0   C  C  -  C  -  -
04 1-0   -  -  -  -  -  -
05 1-0   -  E  E  E  E  -
06 1-0   F  -  F  F  F  F
07 2-0   G  G  -  -  -  -
08 0-3   H  H  H  H  H  -
09 0-3   -  I  -  I  I  I
10 0-4   -  J  J  -  -  J
11 0-4   -  K  K  K  -  -
12 0-4   L  L  -  L  -  L
13 0-4   -  -  -  -  -  -
14 0-6   -  N  -  -  N  -
15 0-6   O  -  -  -  -  O
16 0-6   P  -  -  -  -  -
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   -  R  R  R  R   
19 0-6   S  -  S  -  S   
20 1-4   -  -  -  -      
21 1-4   U  U  U  -      
22 1-4   V  -  V         
23 1-4   W  -  X         
24 1-5   X  -            
25 2-3   -  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KHCPS TUVOE KIIRM MWUUN NKMHK P
-------------------------------
