EFFECTIVE PERIOD:
24-SEP-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  B  B  B  -  B
03 1-0   -  C  -  C  C  -
04 0-3   D  D  -  D  -  -
05 0-3   -  -  -  -  E  -
06 0-3   -  F  -  -  F  F
07 0-3   G  G  G  G  -  G
08 0-4   -  H  -  H  H  H
09 0-4   -  -  -  -  I  -
10 0-4   J  -  -  -  J  J
11 0-4   -  K  K  K  K  -
12 0-5   L  L  L  L  -  L
13 0-5   M  -  M  -  -  -
14 0-5   -  -  -  N  N  -
15 0-5   O  -  -  -  -  O
16 0-5   P  P  P  -  -  P
17 0-5   -  Q  -  Q  Q  -
18 1-2   -  -  R  -  R   
19 1-3   S  S  -  S  -   
20 1-5   T  -  T  T      
21 2-3   -  U  U  -      
22 2-6   V  V  V         
23 3-4   W  X  -         
24 3-4   X  -            
25 3-4   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FLTFU IAUWO VQWKH PXOKI TTUFH A
-------------------------------
