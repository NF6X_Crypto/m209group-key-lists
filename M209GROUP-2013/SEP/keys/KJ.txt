EFFECTIVE PERIOD:
17-SEP-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  A  -  A
02 0-3   -  B  -  B  B  -
03 0-3   C  C  -  -  -  -
04 0-3   -  D  -  -  D  D
05 0-3   E  -  E  E  E  -
06 0-3   -  F  F  -  F  -
07 0-3   G  G  -  G  -  G
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  -  I  I
10 0-4   -  J  J  J  J  J
11 0-4   -  K  K  K  K  K
12 0-4   -  -  -  L  L  L
13 0-6   M  -  M  -  -  -
14 0-6   -  N  N  N  -  N
15 0-6   -  -  O  -  -  O
16 0-6   P  P  -  -  P  P
17 0-6   Q  -  -  Q  -  -
18 1-4   R  -  -  R  R   
19 2-4   -  -  -  -  -   
20 2-5   T  -  T  -      
21 3-5   -  -  -  U      
22 3-6   V  -  V         
23 4-5   W  X  -         
24 4-6   X  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UAAJT RLKVN ZDDTK ZTVAV ALZLT N
-------------------------------
