EFFECTIVE PERIOD:
11-JUN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 2-0   -  -  B  B  -  B
03 2-0   -  -  -  -  C  C
04 2-0   -  -  D  -  D  -
05 2-0   E  E  -  E  E  -
06 0-4   F  -  -  F  -  F
07 0-4   -  G  G  G  -  G
08 0-4   -  H  -  -  H  H
09 0-4   I  -  -  I  -  I
10 0-4   J  -  J  -  J  -
11 0-4   K  K  K  K  K  K
12 0-4   -  L  -  -  L  L
13 0-4   M  M  -  M  -  -
14 0-4   -  N  N  -  N  -
15 0-4   -  O  -  O  O  -
16 0-5   P  -  P  P  -  P
17 0-5   -  -  Q  -  -  -
18 0-5   R  R  -  R  R   
19 0-5   S  -  -  -  S   
20 0-5   -  T  T  -      
21 0-5   -  -  -  U      
22 0-5   V  V  V         
23 0-5   W  -  -         
24 0-5   X  Y            
25 0-6   -  Z            
26 1-3   Z               
27 2-3                   
-------------------------------
26 LETTER CHECK

AAVZZ PLPNP ZKJMK ACYOV CZOQO P
-------------------------------
