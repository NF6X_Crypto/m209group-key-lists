EFFECTIVE PERIOD:
06-JUN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   B  -  -  B  B  B
03 2-0   C  C  -  -  C  -
04 2-0   D  D  D  D  -  D
05 2-0   E  -  E  -  E  -
06 2-0   -  -  F  F  F  -
07 0-5   G  G  -  -  -  G
08 0-5   -  -  H  H  -  H
09 0-5   -  -  I  I  I  I
10 0-5   J  -  J  -  J  -
11 0-6   -  -  -  K  -  K
12 0-6   -  L  L  -  L  -
13 0-6   -  M  M  -  -  -
14 0-6   N  -  N  -  -  N
15 0-6   -  O  O  O  -  -
16 1-2   -  P  -  -  -  -
17 1-4   Q  Q  -  Q  -  -
18 1-5   -  -  R  -  R   
19 1-5   S  -  S  S  -   
20 1-5   T  -  T  -      
21 2-6   U  U  -  U      
22 3-4   -  -  -         
23 4-5   W  -  -         
24 5-6   X  Y            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZMKOG FVEUV AZOAW PNAAN OAIKF A
-------------------------------
