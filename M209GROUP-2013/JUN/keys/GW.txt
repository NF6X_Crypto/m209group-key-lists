EFFECTIVE PERIOD:
18-JUN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   B  B  -  -  -  B
03 1-0   -  -  C  -  C  C
04 1-0   D  -  -  D  D  -
05 1-0   -  -  -  E  -  -
06 1-0   F  -  F  -  F  -
07 1-0   G  G  -  G  -  -
08 1-0   -  H  -  -  -  H
09 1-0   I  I  I  I  -  I
10 2-0   J  -  J  J  J  J
11 0-3   -  K  -  -  -  K
12 0-3   L  L  L  -  -  L
13 0-3   M  M  -  M  M  -
14 0-3   -  N  N  -  N  -
15 0-4   O  O  -  O  O  O
16 0-4   -  P  P  -  -  -
17 0-4   Q  -  -  -  Q  Q
18 0-5   -  R  -  R  -   
19 0-6   S  -  S  -  S   
20 0-6   -  T  T  -      
21 0-6   U  U  U  U      
22 1-4   -  V  -         
23 1-4   -  -  X         
24 1-4   X  -            
25 3-4   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QSNOE XKLUV BCVIQ PQWQS NROZG W
-------------------------------
