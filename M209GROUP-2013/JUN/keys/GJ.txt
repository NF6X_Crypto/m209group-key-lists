EFFECTIVE PERIOD:
05-JUN-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  B  B  -  -
03 1-0   C  C  C  -  -  -
04 2-0   -  D  -  -  D  -
05 2-0   -  -  E  -  -  -
06 2-0   -  -  -  F  -  F
07 2-0   -  -  -  G  G  G
08 2-0   H  H  H  -  H  -
09 2-0   I  -  I  I  I  I
10 2-0   J  J  J  -  -  -
11 2-0   K  K  -  -  -  K
12 2-0   -  L  L  L  L  L
13 2-0   -  -  M  -  -  -
14 2-0   -  -  -  -  -  -
15 2-0   -  -  O  -  O  -
16 0-3   P  -  -  -  -  P
17 0-3   Q  Q  -  Q  Q  -
18 0-3   -  -  R  R  R   
19 0-3   -  -  -  S  S   
20 0-3   T  -  T  T      
21 0-4   -  U  -  -      
22 0-4   -  V  V         
23 0-6   W  X  -         
24 1-3   X  -            
25 1-4   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

HMLAM FWVVE DWUTW FALYK TVUZI E
-------------------------------
