EFFECTIVE PERIOD:
29-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  -  C  C  -  -
04 1-0   -  D  D  -  D  D
05 1-0   E  E  E  E  -  E
06 1-0   -  F  F  -  -  F
07 1-0   -  -  G  -  -  G
08 1-0   H  -  -  H  H  -
09 1-0   I  I  I  -  -  I
10 2-0   -  J  -  -  J  -
11 0-3   -  -  K  K  -  K
12 0-3   -  -  -  -  L  -
13 0-3   -  -  M  -  M  -
14 0-3   N  N  -  N  N  -
15 0-3   O  O  -  O  -  O
16 0-3   P  P  -  -  P  P
17 0-3   Q  Q  -  -  -  -
18 0-3   R  -  R  R  -   
19 0-4   S  -  -  S  -   
20 0-4   -  T  -  -      
21 0-4   U  U  U  U      
22 0-4   V  V  -         
23 0-4   -  -  -         
24 0-6   -  Y            
25 0-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NQQIL SPMGP EZVQI VODQQ JDQGN J
-------------------------------
