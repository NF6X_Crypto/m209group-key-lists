EFFECTIVE PERIOD:
24-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  -  -  B  -  -
03 1-0   C  C  C  C  C  -
04 1-0   D  D  D  -  D  D
05 1-0   E  -  -  -  E  E
06 2-0   F  -  -  F  F  F
07 0-3   -  -  G  -  -  -
08 0-4   -  -  -  -  H  H
09 0-4   I  I  I  I  I  -
10 0-4   J  J  J  -  J  -
11 0-4   K  K  -  -  K  -
12 0-4   -  -  -  -  -  -
13 0-4   -  -  M  -  -  M
14 0-4   N  -  N  N  N  -
15 0-4   -  O  -  O  -  -
16 0-4   P  -  P  -  -  P
17 0-4   -  Q  -  -  -  Q
18 0-4   -  R  -  R  R   
19 0-5   -  S  S  S  -   
20 0-5   T  -  -  T      
21 0-5   -  -  U  U      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 0-5   -  -            
25 0-6   -  -            
26 1-3   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

IKVBH OROQZ UXOOT LNQIJ HNOOF J
-------------------------------
