EFFECTIVE PERIOD:
07-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  B  B  -  B  -
03 1-0   -  C  C  C  C  C
04 1-0   -  D  -  D  -  D
05 2-0   E  E  E  -  -  E
06 2-0   F  F  F  F  -  -
07 0-4   -  -  -  G  -  G
08 0-4   -  H  H  -  -  -
09 0-4   -  -  -  -  I  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  K  -  K  -
12 0-5   -  -  L  L  -  -
13 0-5   M  -  -  M  -  M
14 0-5   N  N  -  -  N  N
15 0-5   -  -  -  O  O  -
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  -  Q  -  -
18 1-2   R  -  -  -  -   
19 1-4   S  S  -  S  S   
20 2-3   -  T  T  -      
21 2-5   U  U  -  U      
22 3-5   -  V  V         
23 3-6   -  -  X         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JJYSO JQQYT MUQFQ UUNVA UTLOK M
-------------------------------
