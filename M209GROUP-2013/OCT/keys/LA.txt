EFFECTIVE PERIOD:
04-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 2-0   -  -  B  -  B  B
03 2-0   C  -  C  -  -  -
04 2-0   -  D  -  -  -  -
05 2-0   -  -  E  -  E  E
06 2-0   -  -  F  F  F  F
07 0-3   G  G  G  G  G  -
08 0-4   H  H  -  -  -  H
09 0-4   -  I  I  -  I  -
10 0-4   J  J  J  -  -  -
11 0-4   K  -  -  K  -  K
12 0-4   -  L  L  L  L  -
13 0-4   -  M  -  -  M  -
14 0-4   N  -  -  N  -  -
15 0-5   -  O  O  -  O  O
16 0-6   -  -  -  -  P  P
17 0-6   -  -  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   -  -  S  -  S   
20 0-6   T  T  T  T      
21 0-6   -  U  U  -      
22 0-6   V  -  -         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   -  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

RKZHV YSVQO ARNDZ IDQIP PMYRM W
-------------------------------
