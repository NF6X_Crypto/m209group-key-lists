EFFECTIVE PERIOD:
28-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 2-0   B  B  -  -  -  -
03 2-0   C  -  C  C  -  C
04 2-0   D  -  -  -  D  -
05 2-0   -  E  E  E  E  E
06 2-0   F  -  -  F  F  F
07 2-0   G  -  G  G  G  -
08 2-0   H  -  -  -  -  -
09 2-0   -  I  I  -  -  I
10 2-0   J  -  -  -  -  -
11 2-0   K  -  K  -  -  -
12 2-0   -  L  -  L  L  L
13 2-0   M  -  M  -  -  M
14 0-3   -  N  N  -  N  N
15 0-3   O  O  -  -  -  O
16 0-3   -  -  P  -  P  P
17 0-3   -  Q  Q  Q  -  -
18 0-3   -  R  -  R  -   
19 0-3   S  S  -  -  -   
20 0-3   T  -  T  T      
21 0-4   U  -  U  U      
22 0-5   -  -  -         
23 0-5   -  X  -         
24 0-5   X  -            
25 0-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WBYVT EGKSD SHZGN PHAJR LGULN W
-------------------------------
