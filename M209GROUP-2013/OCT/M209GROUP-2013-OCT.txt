SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF OCT 2013

    01 OCT 2013  00:00-23:59 GMT:  USE KEY KX
    02 OCT 2013  00:00-23:59 GMT:  USE KEY KY
    03 OCT 2013  00:00-23:59 GMT:  USE KEY KZ
    04 OCT 2013  00:00-23:59 GMT:  USE KEY LA
    05 OCT 2013  00:00-23:59 GMT:  USE KEY LB
    06 OCT 2013  00:00-23:59 GMT:  USE KEY LC
    07 OCT 2013  00:00-23:59 GMT:  USE KEY LD
    08 OCT 2013  00:00-23:59 GMT:  USE KEY LE
    09 OCT 2013  00:00-23:59 GMT:  USE KEY LF
    10 OCT 2013  00:00-23:59 GMT:  USE KEY LG
    11 OCT 2013  00:00-23:59 GMT:  USE KEY LH
    12 OCT 2013  00:00-23:59 GMT:  USE KEY LI
    13 OCT 2013  00:00-23:59 GMT:  USE KEY LJ
    14 OCT 2013  00:00-23:59 GMT:  USE KEY LK
    15 OCT 2013  00:00-23:59 GMT:  USE KEY LL
    16 OCT 2013  00:00-23:59 GMT:  USE KEY LM
    17 OCT 2013  00:00-23:59 GMT:  USE KEY LN
    18 OCT 2013  00:00-23:59 GMT:  USE KEY LO
    19 OCT 2013  00:00-23:59 GMT:  USE KEY LP
    20 OCT 2013  00:00-23:59 GMT:  USE KEY LQ
    21 OCT 2013  00:00-23:59 GMT:  USE KEY LR
    22 OCT 2013  00:00-23:59 GMT:  USE KEY LS
    23 OCT 2013  00:00-23:59 GMT:  USE KEY LT
    24 OCT 2013  00:00-23:59 GMT:  USE KEY LU
    25 OCT 2013  00:00-23:59 GMT:  USE KEY LV
    26 OCT 2013  00:00-23:59 GMT:  USE KEY LW
    27 OCT 2013  00:00-23:59 GMT:  USE KEY LX
    28 OCT 2013  00:00-23:59 GMT:  USE KEY LY
    29 OCT 2013  00:00-23:59 GMT:  USE KEY LZ
    30 OCT 2013  00:00-23:59 GMT:  USE KEY MA
    31 OCT 2013  00:00-23:59 GMT:  USE KEY MB

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  -
02 2-0   B  B  -  -  B  B
03 0-3   C  -  -  -  C  C
04 0-3   D  D  -  -  D  D
05 0-3   E  -  E  E  -  -
06 0-3   -  F  F  -  -  -
07 0-3   -  -  G  G  G  G
08 0-4   -  -  H  H  -  H
09 0-4   I  I  -  I  -  -
10 0-4   -  -  -  J  -  J
11 0-4   -  K  K  K  -  -
12 0-4   -  -  L  -  L  -
13 0-4   -  -  -  -  M  M
14 0-4   N  N  N  N  -  -
15 0-4   -  O  -  O  -  -
16 0-5   -  P  -  P  P  P
17 0-5   Q  -  -  Q  Q  Q
18 0-5   -  -  R  -  -   
19 0-5   S  S  -  -  -   
20 1-4   -  T  T  -      
21 2-5   -  -  -  U      
22 2-6   V  -  V         
23 3-4   W  X  X         
24 3-4   X  -            
25 3-4   -  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

RVNMA ILAWY GJENS SWSMV ONNYV Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  B  -  -  B
03 1-0   -  C  -  -  C  C
04 2-0   -  -  -  D  D  D
05 2-0   E  -  -  -  -  E
06 2-0   -  F  F  F  -  -
07 2-0   -  -  G  -  G  -
08 2-0   -  H  -  -  -  -
09 0-3   I  -  -  I  I  I
10 0-3   J  -  -  -  -  J
11 0-3   K  -  K  K  K  K
12 0-3   -  L  -  -  -  -
13 0-3   M  M  -  M  -  -
14 0-3   N  N  N  -  N  N
15 0-4   -  O  -  -  O  -
16 0-5   P  P  -  P  -  -
17 0-5   -  -  Q  Q  -  Q
18 0-5   R  -  R  R  -   
19 0-5   S  S  S  S  S   
20 1-2   T  -  -  -      
21 1-3   -  U  U  U      
22 1-3   V  -  V         
23 1-3   W  -  -         
24 1-3   -  Y            
25 2-5   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WGNVW MQUMO QWJGH OWCLK OPLUQ J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  B  -  -  B  -
03 1-0   C  C  -  C  C  C
04 1-0   D  D  -  D  -  -
05 1-0   E  E  -  E  E  -
06 2-0   F  -  F  -  F  F
07 2-0   -  G  -  G  -  G
08 2-0   -  -  H  -  -  H
09 2-0   I  I  -  -  I  -
10 2-0   -  -  J  -  J  -
11 2-0   K  K  K  K  -  -
12 2-0   L  -  L  L  L  -
13 2-0   -  M  -  M  -  -
14 0-4   N  N  N  N  -  N
15 0-5   -  O  O  O  -  O
16 0-5   P  P  -  -  P  P
17 0-5   -  -  Q  -  -  Q
18 0-5   -  -  R  -  -   
19 0-6   S  -  -  -  S   
20 1-2   -  -  -  T      
21 1-2   U  U  -  U      
22 1-2   V  V  -         
23 1-2   W  -  X         
24 1-5   X  -            
25 2-3   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZPCMI LUAMS SCVAU VOSZZ EWNVU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 2-0   -  -  B  -  B  B
03 2-0   C  -  C  -  -  -
04 2-0   -  D  -  -  -  -
05 2-0   -  -  E  -  E  E
06 2-0   -  -  F  F  F  F
07 0-3   G  G  G  G  G  -
08 0-4   H  H  -  -  -  H
09 0-4   -  I  I  -  I  -
10 0-4   J  J  J  -  -  -
11 0-4   K  -  -  K  -  K
12 0-4   -  L  L  L  L  -
13 0-4   -  M  -  -  M  -
14 0-4   N  -  -  N  -  -
15 0-5   -  O  O  -  O  O
16 0-6   -  -  -  -  P  P
17 0-6   -  -  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   -  -  S  -  S   
20 0-6   T  T  T  T      
21 0-6   -  U  U  -      
22 0-6   V  -  -         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   -  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

RKZHV YSVQO ARNDZ IDQIP PMYRM W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  -  B  -  -  B
03 1-0   C  -  -  -  C  C
04 1-0   D  -  D  -  D  D
05 2-0   E  E  -  E  -  -
06 2-0   F  -  -  F  F  F
07 2-0   -  G  -  G  G  -
08 2-0   H  H  H  -  -  -
09 2-0   -  I  -  -  -  -
10 2-0   -  J  -  -  -  J
11 2-0   -  K  -  K  -  K
12 0-3   L  L  L  -  L  -
13 0-5   M  -  M  -  M  -
14 0-5   -  -  -  -  N  N
15 0-5   O  O  -  -  -  -
16 1-2   -  -  -  P  -  P
17 1-3   Q  -  -  -  Q  -
18 1-3   -  -  R  R  R   
19 1-3   -  -  S  S  -   
20 1-3   T  T  -  -      
21 2-5   U  -  U  U      
22 3-4   V  V  V         
23 3-5   -  X  X         
24 3-5   -  Y            
25 3-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QQHCO ARSWX AABTR RKTQQ QWXII Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  -  -  A
02 0-3   -  -  B  B  B  B
03 0-3   C  C  C  C  C  -
04 0-3   -  -  -  -  -  -
05 0-4   E  -  E  E  -  -
06 0-4   F  F  F  -  -  -
07 0-4   -  G  -  -  -  -
08 0-4   -  -  H  -  H  H
09 0-4   I  -  -  I  I  -
10 0-4   -  J  -  J  -  J
11 0-4   K  K  -  -  -  K
12 0-6   -  -  -  -  L  L
13 0-6   M  -  M  M  -  -
14 0-6   -  N  N  -  -  N
15 0-6   O  -  O  -  O  O
16 1-3   P  P  -  P  -  -
17 1-5   -  -  Q  Q  Q  Q
18 1-6   R  R  -  R  -   
19 1-6   S  -  S  -  S   
20 1-6   -  T  -  -      
21 2-5   -  U  -  -      
22 3-4   V  -  V         
23 4-6   W  X  X         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ASEAT TOOSL SRFRR LOZTG MWISA V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  B  B  -  B  -
03 1-0   -  C  C  C  C  C
04 1-0   -  D  -  D  -  D
05 2-0   E  E  E  -  -  E
06 2-0   F  F  F  F  -  -
07 0-4   -  -  -  G  -  G
08 0-4   -  H  H  -  -  -
09 0-4   -  -  -  -  I  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  K  -  K  -
12 0-5   -  -  L  L  -  -
13 0-5   M  -  -  M  -  M
14 0-5   N  N  -  -  N  N
15 0-5   -  -  -  O  O  -
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  -  Q  -  -
18 1-2   R  -  -  -  -   
19 1-4   S  S  -  S  S   
20 2-3   -  T  T  -      
21 2-5   U  U  -  U      
22 3-5   -  V  V         
23 3-6   -  -  X         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JJYSO JQQYT MUQFQ UUNVA UTLOK M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   -  -  -  B  -  B
03 2-0   C  C  -  -  C  -
04 2-0   D  -  -  D  -  D
05 0-3   -  -  E  E  -  E
06 0-4   -  -  F  F  -  -
07 0-4   -  -  G  G  -  -
08 0-4   H  -  -  H  -  H
09 0-4   -  I  -  -  I  -
10 0-5   J  -  -  -  -  -
11 0-5   -  K  K  -  K  -
12 0-5   -  L  L  -  -  -
13 0-5   M  -  -  M  M  -
14 0-5   -  N  -  -  N  N
15 0-5   O  -  O  O  O  O
16 0-5   -  -  P  -  -  P
17 0-5   Q  Q  Q  -  -  -
18 0-5   R  R  R  -  R   
19 0-6   S  -  S  S  S   
20 0-6   T  -  -  T      
21 0-6   -  -  U  U      
22 2-3   V  V  -         
23 2-4   W  -  X         
24 4-6   -  Y            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FOXWJ ISAUK ZGFKO HTZME SZUGM S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  -  -  B  B  B
03 1-0   -  -  -  C  -  -
04 1-0   D  D  D  -  D  -
05 2-0   -  E  E  -  -  E
06 2-0   -  -  -  F  -  -
07 2-0   G  G  -  -  G  G
08 2-0   -  H  H  -  -  H
09 2-0   I  I  I  I  I  I
10 2-0   -  -  -  J  J  -
11 2-0   -  -  -  K  -  -
12 2-0   -  -  L  -  -  -
13 2-0   M  -  -  M  M  -
14 2-0   N  N  -  -  -  N
15 0-3   O  -  O  -  O  -
16 0-3   P  P  -  -  P  P
17 0-4   -  Q  Q  Q  Q  Q
18 0-5   -  -  R  -  R   
19 0-5   -  -  S  S  -   
20 0-5   T  -  -  -      
21 0-6   -  -  U  U      
22 1-2   V  V  V         
23 1-2   -  X  X         
24 1-2   X  Y            
25 1-5   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

IUMIP IOQXU SOZEA OEZRS GEGNY O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 0-4   -  -  B  -  -  B
03 0-4   -  C  -  C  -  -
04 0-4   -  D  -  -  D  D
05 0-4   -  -  E  E  E  E
06 0-4   F  F  F  -  F  -
07 0-4   G  -  -  -  -  -
08 0-4   -  H  -  -  -  H
09 0-4   I  -  I  -  I  I
10 0-4   -  J  J  J  J  J
11 0-5   K  K  K  K  K  K
12 0-5   -  L  L  L  L  -
13 0-5   -  M  -  M  M  M
14 0-5   -  -  N  -  -  N
15 0-5   O  O  O  -  -  -
16 0-5   -  -  -  P  P  P
17 0-5   Q  -  Q  -  -  -
18 0-6   R  R  R  R  R   
19 0-6   -  S  -  S  S   
20 0-6   -  T  -  T      
21 0-6   -  U  -  -      
22 2-3   V  V  -         
23 3-6   -  X  -         
24 4-5   X  -            
25 4-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZNGSM SVUHA HAKUG GRQVA ZOGMN R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  B  B  -  -  B
03 1-0   C  C  -  C  C  -
04 1-0   -  -  D  D  -  D
05 2-0   -  -  E  E  -  E
06 0-3   -  -  F  -  F  F
07 0-3   -  -  -  -  G  G
08 0-3   H  H  -  -  -  H
09 0-3   I  I  -  -  I  -
10 0-3   J  -  J  -  -  J
11 0-3   -  K  -  K  -  K
12 0-3   L  L  L  -  L  -
13 0-3   -  -  -  M  M  -
14 0-3   -  N  -  N  -  -
15 0-3   O  -  -  O  O  -
16 0-4   P  -  P  P  P  P
17 0-5   -  Q  -  -  -  -
18 0-5   -  -  R  -  -   
19 0-5   S  S  -  S  S   
20 0-5   T  T  -  -      
21 0-6   -  U  U  U      
22 0-6   -  -  -         
23 0-6   W  X  -         
24 1-5   -  Y            
25 1-6   -  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

PGNLW FCUGB UWGAU KKEHX KNQGC U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  -
02 2-0   -  -  -  B  -  B
03 2-0   C  -  C  -  C  C
04 2-0   -  -  D  D  -  -
05 2-0   E  E  E  E  -  -
06 0-4   F  F  -  -  F  F
07 0-4   -  G  -  -  -  G
08 0-4   H  H  -  -  H  -
09 0-4   -  I  -  -  I  -
10 0-4   -  J  J  J  -  J
11 0-6   K  -  -  -  -  K
12 0-6   L  L  -  -  -  L
13 0-6   M  M  M  M  M  M
14 0-6   -  N  -  -  N  -
15 0-6   O  -  -  -  -  -
16 1-3   P  -  P  -  P  -
17 1-4   Q  Q  -  Q  Q  -
18 1-4   -  -  -  R  R   
19 1-5   S  -  S  -  S   
20 2-5   T  -  T  -      
21 2-6   -  -  -  U      
22 4-5   V  V  V         
23 4-5   -  X  X         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UQTDP PGJZU VZTVA MVPAQ JPKMJ V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   -  -  B  -  B  B
03 1-0   -  -  C  C  -  C
04 1-0   -  D  -  D  D  D
05 2-0   E  -  -  -  -  -
06 0-4   -  -  F  F  F  -
07 0-4   -  G  -  -  -  G
08 0-4   H  H  -  -  -  H
09 0-5   I  -  I  I  -  I
10 0-5   J  J  J  J  -  -
11 0-5   -  -  -  -  -  K
12 0-5   -  -  -  L  L  -
13 0-5   M  -  M  -  -  M
14 0-5   N  -  N  N  N  -
15 0-5   O  O  -  O  O  O
16 0-5   -  P  P  P  P  -
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   S  S  -  -  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  -      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   -  -            
26 2-3   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

TFAUW OIORT GUEQL KZLJV JQPZS P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 2-0   -  -  -  B  -  B
03 2-0   -  C  C  C  -  C
04 2-0   -  D  -  D  D  D
05 0-4   E  E  -  -  E  -
06 0-4   -  F  F  -  -  -
07 0-4   G  G  G  G  G  G
08 0-4   -  H  H  H  H  H
09 0-4   I  -  I  -  -  -
10 0-5   -  -  -  -  J  -
11 0-5   -  -  K  K  -  K
12 0-5   -  L  -  -  L  -
13 0-5   -  M  M  M  M  -
14 0-6   N  N  -  -  -  N
15 0-6   O  -  -  O  O  O
16 0-6   -  P  P  P  -  -
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   R  R  -  -  R   
19 0-6   -  -  -  -  S   
20 0-6   T  T  T  T      
21 0-6   -  -  U  U      
22 1-2   V  -  -         
23 1-3   W  -  -         
24 2-5   -  Y            
25 4-5   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UJOZS JSXPO SSNOT SNVFP QPKUA X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 2-0   -  B  B  B  B  -
03 0-3   C  -  -  -  C  C
04 0-3   -  -  D  -  -  -
05 0-3   -  -  E  -  E  E
06 0-3   F  -  -  F  F  F
07 0-3   -  G  G  -  -  -
08 0-3   H  -  H  -  -  H
09 0-4   -  I  -  -  -  -
10 0-4   J  -  J  J  J  J
11 0-4   K  K  -  -  K  -
12 0-4   -  L  L  -  -  L
13 0-4   -  M  -  -  M  -
14 0-4   -  -  -  -  -  -
15 0-4   O  O  -  -  -  O
16 0-4   P  P  P  P  P  -
17 0-4   Q  -  -  Q  Q  Q
18 0-5   -  -  R  R  R   
19 0-5   -  -  S  S  -   
20 0-5   -  -  -  T      
21 0-5   U  -  U  -      
22 2-5   V  V  -         
23 2-6   -  X  X         
24 3-4   X  -            
25 3-4   -  Z            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

DIJLY YTVDY UKHPT ZLQTM GNCHR A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   -  -  B  B  B  B
03 2-0   -  -  C  -  -  C
04 2-0   -  -  -  D  D  -
05 2-0   -  -  E  -  -  E
06 2-0   F  -  -  F  -  F
07 2-0   -  -  G  G  G  G
08 0-3   H  H  H  H  H  H
09 0-3   I  I  I  -  -  I
10 0-4   J  -  -  -  -  -
11 0-4   -  K  -  -  K  -
12 0-4   -  -  -  -  L  -
13 0-4   -  -  -  M  -  -
14 0-5   -  N  N  -  -  -
15 0-5   -  O  O  -  -  O
16 0-5   P  -  -  -  P  P
17 0-5   -  Q  -  Q  -  -
18 0-5   R  -  R  R  R   
19 0-5   -  -  -  -  S   
20 0-5   T  -  T  -      
21 0-5   -  U  -  U      
22 0-5   V  V  V         
23 0-6   W  X  -         
24 1-6   X  -            
25 2-4   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZDIZT IVRRR QWKCT RJDVR APCML Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  B  -  B  B  B
03 1-0   -  -  -  C  -  -
04 1-0   D  D  -  D  -  -
05 1-0   E  -  E  -  -  -
06 1-0   F  F  F  -  F  -
07 1-0   -  -  G  G  G  G
08 1-0   -  -  -  -  -  -
09 0-3   I  I  I  -  -  -
10 0-3   -  -  -  J  J  -
11 0-3   K  -  K  -  K  K
12 0-3   L  L  -  -  L  L
13 0-4   -  -  -  -  M  M
14 0-4   N  N  -  N  -  N
15 0-4   -  -  O  O  -  -
16 0-4   P  -  P  P  -  -
17 0-5   Q  -  Q  -  Q  Q
18 0-5   -  -  R  -  -   
19 0-6   -  S  S  S  -   
20 1-2   T  -  -  T      
21 1-3   U  U  -  -      
22 1-3   V  V  V         
23 1-3   W  X  -         
24 1-3   -  -            
25 3-4   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RAPPR VUSLQ SFGEV WRERR NMSHZ K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  -  B  -  B  -
03 1-0   -  -  C  -  C  -
04 1-0   -  -  D  -  -  D
05 0-3   E  E  -  E  -  E
06 0-3   F  F  F  -  F  -
07 0-3   G  -  G  -  -  -
08 0-3   H  H  -  -  -  H
09 0-3   -  I  -  I  I  I
10 0-4   -  -  J  -  -  J
11 0-4   K  K  -  K  -  K
12 0-4   L  -  -  L  -  -
13 0-4   -  M  M  M  M  M
14 0-4   -  N  -  N  N  N
15 0-4   -  -  -  O  -  -
16 1-2   -  P  P  -  -  -
17 1-3   Q  -  -  Q  Q  -
18 1-3   -  -  R  -  -   
19 1-3   S  S  -  -  -   
20 1-4   -  -  T  T      
21 1-4   -  U  -  U      
22 1-4   -  -  V         
23 1-4   W  X  -         
24 1-6   X  -            
25 2-5   -  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

LVVMV BLTAM OUTUA UUTNW NVZMD T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 2-0   B  B  B  -  B  B
03 2-0   C  C  -  C  C  C
04 2-0   D  -  -  D  -  D
05 2-0   -  E  E  -  -  -
06 2-0   -  F  -  -  -  -
07 2-0   G  -  -  G  G  G
08 2-0   -  -  H  H  H  H
09 2-0   I  -  I  -  I  -
10 2-0   J  -  -  -  J  -
11 2-0   K  -  -  -  -  -
12 2-0   L  L  -  -  L  -
13 0-3   M  M  M  M  -  M
14 0-4   -  N  N  N  -  -
15 0-4   -  -  O  -  -  O
16 0-4   -  -  P  P  -  P
17 0-4   Q  -  Q  Q  Q  -
18 0-5   -  R  R  R  -   
19 0-5   -  -  -  -  S   
20 0-5   -  T  T  T      
21 0-5   -  U  U  U      
22 0-5   -  V  -         
23 0-5   W  X  X         
24 0-5   -  -            
25 0-6   -  -            
26 1-4   Z               
27 1-6                   
-------------------------------
26 LETTER CHECK

MGCRH ZMASO JFROK XXJAJ BGTOA H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  A
02 2-0   -  B  -  B  -  B
03 2-0   C  -  C  -  C  -
04 2-0   -  -  -  -  D  -
05 0-3   -  E  E  E  E  -
06 0-3   -  -  F  F  F  F
07 0-3   G  -  G  -  -  -
08 0-4   -  H  -  -  -  -
09 0-4   -  I  -  I  -  I
10 0-4   J  -  J  -  J  J
11 0-6   -  K  -  -  K  -
12 0-6   L  -  -  L  -  L
13 0-6   M  M  M  M  -  M
14 0-6   N  -  -  N  N  -
15 0-6   -  O  O  O  O  -
16 1-2   P  -  -  -  P  -
17 1-2   Q  Q  -  Q  -  -
18 1-4   R  R  -  R  R   
19 1-5   S  S  S  S  S   
20 2-3   -  -  T  -      
21 2-3   U  -  U  -      
22 2-3   V  V  -         
23 2-3   -  X  -         
24 2-4   X  Y            
25 2-4   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RWXNR RRWUL VNWJO VNJTV JRRRR X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  -  -  -  -  -
03 1-0   C  C  C  C  -  -
04 1-0   D  -  D  -  -  D
05 1-0   E  -  -  E  -  -
06 2-0   F  F  -  F  -  F
07 0-4   -  G  G  G  G  -
08 0-4   H  H  H  -  H  H
09 0-4   -  -  -  -  -  -
10 0-4   -  -  -  -  J  J
11 0-5   -  K  K  -  K  K
12 0-5   L  L  -  -  L  -
13 0-5   M  -  -  M  -  M
14 0-5   -  N  -  -  -  N
15 0-5   O  -  -  O  -  -
16 1-2   -  P  P  P  P  P
17 1-2   Q  -  -  -  Q  Q
18 1-3   R  R  -  R  R   
19 1-3   -  -  S  -  -   
20 1-5   T  -  T  T      
21 1-5   U  -  -  U      
22 1-5   V  V  -         
23 1-5   -  -  X         
24 2-3   -  -            
25 2-4   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PPUKS ZUJOD HFDVY XQRVK DJQYS U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  B  B  -  -  B
03 1-0   C  C  -  -  -  -
04 1-0   -  D  -  -  D  -
05 1-0   -  -  E  E  E  -
06 0-3   -  -  F  -  F  F
07 0-4   G  G  -  -  -  -
08 0-4   -  -  -  -  H  -
09 0-4   I  -  -  -  -  -
10 0-4   J  -  J  J  -  J
11 0-4   K  K  -  -  K  -
12 0-4   -  L  L  L  L  -
13 0-4   M  M  M  M  M  M
14 0-4   N  -  N  N  N  N
15 0-5   O  O  -  O  O  O
16 0-5   -  P  P  -  P  -
17 0-6   Q  -  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   -  S  S  -  -   
20 0-6   -  -  -  T      
21 0-6   U  U  U  U      
22 0-6   -  -  -         
23 0-6   -  -  -         
24 0-6   -  Y            
25 0-6   -  -            
26 1-5   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

MQVMV OJIUI AJMQB EIGLS ZAMAF Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  B  -  -  -  B
03 1-0   C  C  C  C  -  C
04 1-0   -  D  D  -  -  -
05 1-0   -  -  -  E  E  E
06 1-0   F  -  -  F  -  F
07 2-0   G  G  -  G  -  -
08 2-0   H  H  -  H  -  -
09 2-0   -  -  I  I  I  -
10 2-0   -  J  J  -  -  -
11 0-3   -  K  -  K  K  -
12 0-3   L  L  -  -  -  -
13 0-3   -  -  M  -  M  M
14 0-4   -  -  N  N  N  -
15 0-4   O  -  -  -  O  -
16 0-4   -  -  -  -  -  -
17 0-5   Q  Q  -  -  -  Q
18 1-2   -  -  -  R  R   
19 1-4   S  -  S  -  S   
20 2-5   T  -  T  T      
21 3-4   U  U  U  -      
22 3-4   -  -  -         
23 3-4   -  X  X         
24 3-4   X  Y            
25 3-5   -  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ITJMH TQPNT QOXYN HTTXN JJAJA M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  -  -  B  -  -
03 1-0   C  C  C  C  C  -
04 1-0   D  D  D  -  D  D
05 1-0   E  -  -  -  E  E
06 2-0   F  -  -  F  F  F
07 0-3   -  -  G  -  -  -
08 0-4   -  -  -  -  H  H
09 0-4   I  I  I  I  I  -
10 0-4   J  J  J  -  J  -
11 0-4   K  K  -  -  K  -
12 0-4   -  -  -  -  -  -
13 0-4   -  -  M  -  -  M
14 0-4   N  -  N  N  N  -
15 0-4   -  O  -  O  -  -
16 0-4   P  -  P  -  -  P
17 0-4   -  Q  -  -  -  Q
18 0-4   -  R  -  R  R   
19 0-5   -  S  S  S  -   
20 0-5   T  -  -  T      
21 0-5   -  -  U  U      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 0-5   -  -            
25 0-6   -  -            
26 1-3   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

IKVBH OROQZ UXOOT LNQIJ HNOOF J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  -
02 2-0   -  B  B  -  B  B
03 2-0   C  C  C  C  -  -
04 2-0   -  -  -  -  D  -
05 2-0   E  E  -  E  E  -
06 2-0   -  F  -  -  F  F
07 2-0   G  -  -  G  G  G
08 2-0   H  H  H  H  -  H
09 2-0   -  -  I  I  -  -
10 2-0   J  -  -  -  -  -
11 0-3   -  -  K  -  K  K
12 0-3   -  L  -  -  -  L
13 0-3   -  M  M  -  -  -
14 0-4   -  N  -  N  -  N
15 0-4   O  O  O  O  O  O
16 0-4   P  P  -  P  P  -
17 0-4   Q  Q  Q  -  -  -
18 0-5   R  -  R  R  R   
19 0-6   -  S  -  -  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 1-5   -  V  V         
23 2-6   -  -  X         
24 2-6   X  Y            
25 3-4   -  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QZSQW GOOPX ZLWXI SRPWZ LFXUV S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  -  B  B  B  B
03 1-0   -  -  C  C  C  -
04 1-0   D  D  D  D  D  -
05 1-0   -  E  -  E  -  E
06 1-0   F  -  -  -  F  F
07 1-0   G  -  -  -  -  G
08 1-0   -  H  H  -  H  -
09 1-0   -  I  I  I  I  I
10 1-0   J  J  J  -  -  -
11 2-0   K  -  -  K  -  K
12 2-0   L  -  -  -  L  L
13 0-3   M  M  M  -  -  -
14 0-5   N  N  -  -  N  -
15 0-5   O  -  O  O  O  -
16 0-5   -  -  -  P  -  -
17 0-5   Q  Q  -  Q  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   -  -  -  -  -   
20 0-6   T  -  -  T      
21 0-6   U  U  U  U      
22 1-6   -  -  V         
23 1-6   -  X  -         
24 2-3   -  Y            
25 2-5   Y  -            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LTJJM RKCQU IOTOP ORTMK PRUVW R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 2-0   -  -  B  -  -  B
03 2-0   -  C  -  C  -  -
04 2-0   D  -  D  -  -  -
05 2-0   E  E  -  E  -  E
06 0-3   F  -  -  -  F  -
07 0-3   -  -  G  G  -  G
08 0-3   -  -  H  H  H  -
09 0-3   -  -  I  -  I  -
10 0-3   J  J  J  J  -  J
11 0-3   -  -  K  -  -  K
12 0-4   L  -  -  -  L  -
13 0-5   -  M  -  M  -  M
14 0-5   -  -  N  N  N  -
15 0-5   O  O  -  -  O  -
16 0-5   -  P  P  -  P  -
17 0-5   -  Q  -  Q  -  -
18 0-5   -  R  R  R  R   
19 0-5   S  S  S  -  -   
20 0-5   T  -  -  T      
21 0-5   -  -  U  U      
22 0-5   V  V  -         
23 0-5   W  X  -         
24 0-5   X  -            
25 0-6   -  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZGYRZ IMOIZ LJZZA TWHIT LOPWB A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 2-0   B  B  -  -  -  -
03 2-0   C  -  C  C  -  C
04 2-0   D  -  -  -  D  -
05 2-0   -  E  E  E  E  E
06 2-0   F  -  -  F  F  F
07 2-0   G  -  G  G  G  -
08 2-0   H  -  -  -  -  -
09 2-0   -  I  I  -  -  I
10 2-0   J  -  -  -  -  -
11 2-0   K  -  K  -  -  -
12 2-0   -  L  -  L  L  L
13 2-0   M  -  M  -  -  M
14 0-3   -  N  N  -  N  N
15 0-3   O  O  -  -  -  O
16 0-3   -  -  P  -  P  P
17 0-3   -  Q  Q  Q  -  -
18 0-3   -  R  -  R  -   
19 0-3   S  S  -  -  -   
20 0-3   T  -  T  T      
21 0-4   U  -  U  U      
22 0-5   -  -  -         
23 0-5   -  X  -         
24 0-5   X  -            
25 0-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WBYVT EGKSD SHZGN PHAJR LGULN W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  -  C  C  -  -
04 1-0   -  D  D  -  D  D
05 1-0   E  E  E  E  -  E
06 1-0   -  F  F  -  -  F
07 1-0   -  -  G  -  -  G
08 1-0   H  -  -  H  H  -
09 1-0   I  I  I  -  -  I
10 2-0   -  J  -  -  J  -
11 0-3   -  -  K  K  -  K
12 0-3   -  -  -  -  L  -
13 0-3   -  -  M  -  M  -
14 0-3   N  N  -  N  N  -
15 0-3   O  O  -  O  -  O
16 0-3   P  P  -  -  P  P
17 0-3   Q  Q  -  -  -  -
18 0-3   R  -  R  R  -   
19 0-4   S  -  -  S  -   
20 0-4   -  T  -  -      
21 0-4   U  U  U  U      
22 0-4   V  V  -         
23 0-4   -  -  -         
24 0-6   -  Y            
25 0-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NQQIL SPMGP EZVQI VODQQ JDQGN J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  -  B  -  B  -
03 1-0   -  C  -  C  -  -
04 1-0   D  -  -  D  D  -
05 1-0   -  -  -  E  E  -
06 0-3   -  -  F  -  F  F
07 0-3   -  -  G  G  -  G
08 0-3   -  H  H  -  -  -
09 0-3   -  -  -  -  I  I
10 0-3   J  -  J  J  J  -
11 0-3   K  K  K  -  K  K
12 0-4   L  -  L  -  -  L
13 0-4   -  M  -  -  -  M
14 0-5   N  N  N  N  -  N
15 0-5   O  O  O  O  -  -
16 0-5   -  P  -  P  -  -
17 0-5   Q  Q  -  -  Q  Q
18 0-5   R  R  -  R  -   
19 0-5   S  -  -  -  -   
20 1-3   -  -  T  T      
21 1-5   -  U  U  -      
22 1-5   -  V  -         
23 1-5   -  -  -         
24 1-5   -  -            
25 2-5   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

JUOOZ OJLZQ ZVUOJ KSTOI RRNLA K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-OCT-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 2-0   B  -  -  -  B  B
03 2-0   C  -  -  -  -  -
04 2-0   -  D  -  -  D  D
05 2-0   E  E  -  E  E  -
06 0-3   -  F  -  F  F  F
07 0-3   G  G  -  G  -  -
08 0-5   H  -  H  -  H  -
09 0-5   -  I  -  I  I  I
10 0-5   J  -  J  -  -  -
11 0-6   -  K  K  K  K  K
12 0-6   L  -  L  -  -  L
13 0-6   -  -  -  M  -  M
14 0-6   -  N  N  N  N  N
15 0-6   O  O  O  O  -  -
16 0-6   -  -  P  -  -  -
17 0-6   -  -  Q  Q  -  -
18 0-6   -  -  -  -  R   
19 0-6   -  S  -  -  -   
20 1-3   -  -  -  T      
21 1-4   -  -  U  U      
22 2-3   V  V  -         
23 2-5   W  X  -         
24 5-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WRVQG ZKKVK QPTKG YMXTW SQCEI A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
