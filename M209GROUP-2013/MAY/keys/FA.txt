EFFECTIVE PERIOD:
01-MAY-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   -  -  B  -  B  -
03 2-0   C  C  -  -  C  -
04 2-0   D  -  D  D  D  -
05 2-0   -  -  -  -  -  -
06 0-5   F  -  F  F  -  -
07 0-5   -  G  -  G  G  G
08 0-5   H  H  H  H  H  H
09 0-5   -  I  I  I  -  -
10 0-5   -  J  J  J  J  -
11 0-5   K  K  -  -  -  K
12 0-6   L  -  -  L  -  -
13 0-6   M  -  -  -  -  M
14 0-6   N  -  N  -  -  N
15 0-6   O  -  O  -  O  O
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  -  -  Q  Q
18 1-2   R  R  R  R  R   
19 1-3   S  -  -  S  S   
20 2-5   -  T  T  T      
21 2-6   -  U  -  -      
22 3-6   V  V  V         
23 4-6   -  X  -         
24 5-6   -  Y            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NJUAU SASJS RNAUV NCAKU TUMVO N
-------------------------------
