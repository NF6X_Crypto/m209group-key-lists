EFFECTIVE PERIOD:
31-MAY-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 2-0   -  B  B  -  -  -
03 2-0   C  -  C  -  -  -
04 0-3   D  -  -  D  -  D
05 0-3   E  -  -  E  E  E
06 0-3   -  -  F  -  F  F
07 0-3   G  -  -  G  -  G
08 0-3   -  -  H  -  H  -
09 0-3   -  I  I  I  I  -
10 0-3   -  -  -  -  J  -
11 0-3   -  -  -  K  K  -
12 0-4   L  L  -  L  -  L
13 0-5   M  M  -  -  -  M
14 0-5   N  -  N  -  -  N
15 0-5   O  -  O  -  O  -
16 0-5   -  P  P  P  P  -
17 0-5   -  -  Q  -  -  Q
18 0-6   R  R  -  -  -   
19 0-6   -  S  -  -  S   
20 0-6   T  T  T  T      
21 0-6   U  U  U  U      
22 1-2   -  V  V         
23 2-6   -  X  -         
24 3-5   X  -            
25 3-5   Y  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KTSZN ETZQC HTZQB TITVJ GZNZT M
-------------------------------
