EFFECTIVE PERIOD:
03-MAY-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  -  B  -  -  B
03 1-0   -  -  -  C  -  -
04 1-0   -  D  D  -  D  -
05 1-0   -  E  E  E  -  E
06 1-0   -  F  F  F  F  -
07 0-3   G  -  G  -  G  G
08 0-4   -  -  -  H  -  H
09 0-4   -  -  -  -  I  I
10 0-5   J  -  -  J  -  -
11 0-5   -  -  -  -  K  K
12 0-5   -  L  -  -  L  -
13 0-6   -  M  M  -  -  -
14 0-6   N  N  N  N  N  -
15 0-6   O  -  O  O  O  -
16 0-6   -  -  P  -  -  -
17 0-6   Q  -  -  -  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  -  S  -   
20 0-6   -  -  -  T      
21 0-6   U  U  -  U      
22 0-6   -  V  V         
23 0-6   W  X  X         
24 1-5   X  Y            
25 1-6   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

RMPMZ LPLXB IUZUU NISHE HEIQN T
-------------------------------
