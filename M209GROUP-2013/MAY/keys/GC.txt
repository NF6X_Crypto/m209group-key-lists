EFFECTIVE PERIOD:
29-MAY-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 2-0   B  -  B  B  -  B
03 2-0   C  -  -  -  C  -
04 2-0   D  D  -  -  -  D
05 2-0   -  E  -  -  -  E
06 2-0   -  F  F  F  F  F
07 2-0   G  G  G  -  -  -
08 0-4   H  -  -  H  H  -
09 0-4   -  -  I  I  I  I
10 0-4   -  J  -  -  -  J
11 0-4   K  K  K  K  K  -
12 0-4   -  L  L  L  L  -
13 0-4   M  M  M  -  -  -
14 0-4   N  -  -  -  -  -
15 0-4   O  -  O  -  -  O
16 0-4   P  -  P  P  P  -
17 0-4   -  Q  Q  Q  -  Q
18 0-4   R  -  -  -  -   
19 0-4   -  -  -  S  S   
20 0-5   T  T  -  -      
21 0-5   U  -  U  -      
22 0-5   V  V  V         
23 0-5   W  -  X         
24 2-4   -  Y            
25 2-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

GBKOU IOOBJ ZJBGU NZUHS VKJOP P
-------------------------------
