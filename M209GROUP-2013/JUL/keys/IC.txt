EFFECTIVE PERIOD:
20-JUL-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 2-0   B  B  -  -  -  B
03 0-3   C  -  -  C  -  C
04 0-3   D  -  D  -  D  D
05 0-3   -  -  E  E  -  E
06 0-3   -  F  F  F  F  F
07 0-3   G  G  G  G  G  -
08 0-3   H  H  -  H  -  -
09 0-3   -  -  I  -  I  I
10 0-3   -  J  J  -  -  -
11 0-3   -  K  K  K  -  -
12 0-3   -  -  L  -  -  -
13 0-3   -  -  M  M  -  -
14 0-3   -  -  N  -  N  N
15 0-3   O  -  -  -  -  -
16 0-4   -  P  -  P  P  P
17 0-4   Q  Q  -  -  -  -
18 0-4   -  -  R  -  R   
19 0-4   S  S  -  S  S   
20 0-4   T  -  -  -      
21 0-4   U  U  U  -      
22 0-4   V  -  -         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   -  -            
26 1-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TCCMQ FKRFD FMGWW XWCPT RWQOK F
-------------------------------
