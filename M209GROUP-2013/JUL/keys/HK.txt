EFFECTIVE PERIOD:
02-JUL-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   B  B  -  B  -  -
03 1-0   -  -  C  C  C  -
04 1-0   D  -  D  -  -  -
05 1-0   -  E  E  -  E  E
06 2-0   F  -  -  F  F  F
07 2-0   -  -  -  G  G  G
08 0-3   H  -  H  H  -  H
09 0-3   I  I  -  I  -  -
10 0-3   -  -  -  J  J  J
11 0-3   K  K  K  -  -  -
12 0-3   L  -  L  -  -  L
13 0-6   M  -  M  M  M  -
14 0-6   -  N  N  -  -  -
15 0-6   -  -  -  -  -  O
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  -  -  -  Q
18 1-2   R  -  -  -  -   
19 1-3   S  -  -  S  S   
20 2-4   -  -  -  -      
21 2-6   U  U  U  U      
22 2-6   V  -  -         
23 3-6   -  X  -         
24 3-6   -  -            
25 3-6   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KVUHL GVRGZ MVARY FTLTL KAPXP J
-------------------------------
