EFFECTIVE PERIOD:
27-JUL-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  -  -  -  D
05 1-0   E  E  -  E  -  E
06 2-0   -  F  F  -  F  F
07 2-0   -  -  G  -  -  -
08 2-0   -  -  -  -  -  H
09 2-0   I  I  -  I  I  -
10 2-0   J  J  J  J  J  J
11 2-0   -  -  -  -  -  -
12 0-4   L  L  L  L  L  L
13 0-4   -  M  -  M  -  -
14 0-4   -  N  -  N  -  N
15 0-4   -  O  O  -  -  -
16 0-5   P  P  P  -  P  P
17 0-6   -  Q  -  Q  Q  -
18 0-6   -  -  -  -  -   
19 0-6   S  -  S  S  S   
20 1-2   -  T  T  -      
21 1-5   -  -  -  U      
22 2-6   -  V  V         
23 3-4   W  -  -         
24 4-6   X  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XTIOK RIOQU JIUQO ORKMP ZIZJK Q
-------------------------------
