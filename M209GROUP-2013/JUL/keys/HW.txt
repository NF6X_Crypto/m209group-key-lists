EFFECTIVE PERIOD:
14-JUL-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 2-0   B  B  B  -  -  B
03 2-0   -  -  -  -  -  -
04 2-0   -  -  D  -  D  D
05 2-0   E  E  E  E  E  -
06 2-0   F  F  -  F  -  F
07 2-0   G  -  G  G  -  G
08 2-0   H  -  H  -  -  -
09 0-3   I  I  -  -  I  -
10 0-4   -  J  J  -  -  -
11 0-4   -  K  -  -  K  K
12 0-4   -  L  L  L  -  -
13 0-4   -  -  M  -  -  M
14 0-4   N  N  -  -  N  -
15 0-6   -  O  O  O  -  O
16 0-6   -  -  -  -  -  P
17 0-6   -  Q  -  -  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  -  S  -   
20 1-3   T  -  T  T      
21 2-3   -  U  U  U      
22 2-4   V  V  -         
23 4-6   -  -  -         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SCXZU PAZPL UEUSM TMNCV RUUPV S
-------------------------------
