EFFECTIVE PERIOD:
09-JUL-2013 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 2-0   B  -  -  -  B  B
03 2-0   -  C  -  C  C  C
04 2-0   D  D  -  -  -  D
05 2-0   -  E  -  -  -  E
06 2-0   F  -  -  -  F  -
07 2-0   G  G  G  -  G  -
08 0-3   -  -  -  H  -  H
09 0-3   -  I  -  -  I  -
10 0-3   -  J  J  J  -  J
11 0-3   -  -  K  K  K  -
12 0-4   L  L  -  L  -  L
13 0-4   -  M  M  M  M  M
14 0-4   N  -  N  N  N  -
15 0-4   -  O  O  -  -  -
16 0-6   -  P  -  -  -  P
17 0-6   Q  Q  -  Q  -  -
18 1-2   R  R  R  -  -   
19 1-2   S  S  S  -  -   
20 1-5   -  -  -  -      
21 1-6   -  U  -  U      
22 2-3   -  -  V         
23 2-3   W  -  X         
24 2-3   -  -            
25 2-3   -  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WAROW MMGRN NLDNR MTOJZ XGARP V
-------------------------------
