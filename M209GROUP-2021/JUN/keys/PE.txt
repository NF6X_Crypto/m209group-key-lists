EFFECTIVE PERIOD:
16-JUN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 2-0   -  -  B  B  B  B
03 0-4   C  -  C  -  -  C
04 0-4   D  -  D  -  -  D
05 0-4   -  -  -  -  -  -
06 0-4   F  -  -  F  -  F
07 0-4   G  G  G  G  -  -
08 0-5   H  -  -  -  H  -
09 0-5   I  I  -  -  -  -
10 0-5   -  J  J  J  J  J
11 0-6   -  -  -  -  K  K
12 0-6   -  -  -  L  L  L
13 0-6   M  M  -  M  -  -
14 0-6   -  N  -  -  -  N
15 0-6   O  O  -  -  O  -
16 1-3   -  -  P  P  P  P
17 2-3   Q  -  -  Q  Q  -
18 2-4   -  R  R  -  -   
19 2-4   -  -  S  S  -   
20 2-6   -  T  T  -      
21 3-4   U  U  U  U      
22 3-4   -  -  V         
23 4-5   W  -  -         
24 4-5   -  Y            
25 4-5   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZHDZS VRDVS VIKDN QKTXQ WMNOU X
-------------------------------
