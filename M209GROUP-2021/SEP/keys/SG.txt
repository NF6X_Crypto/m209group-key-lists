EFFECTIVE PERIOD:
04-SEP-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  -  -  B  B  -
03 1-0   C  C  -  C  C  -
04 1-0   D  D  D  D  -  D
05 1-0   -  E  E  -  -  E
06 1-0   -  -  F  F  F  F
07 1-0   G  -  -  G  -  -
08 1-0   H  H  -  H  H  H
09 1-0   I  -  -  -  I  -
10 2-0   -  -  -  -  J  -
11 2-0   K  K  K  K  -  -
12 0-3   L  L  -  -  -  L
13 0-4   M  -  M  -  -  -
14 0-4   -  N  N  -  N  N
15 0-4   -  O  O  O  -  -
16 0-4   -  P  P  -  P  -
17 0-4   -  -  -  -  Q  Q
18 0-4   -  -  R  R  R   
19 0-4   S  S  -  S  -   
20 0-4   T  -  -  -      
21 0-4   -  U  -  U      
22 0-4   V  -  -         
23 0-4   W  -  X         
24 0-4   -  -            
25 0-6   -  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

EFOJN NZJLO XVMNA QNKMU CMNYF E
-------------------------------
