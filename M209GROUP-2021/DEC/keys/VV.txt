EFFECTIVE PERIOD:
06-DEC-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 2-0   B  -  -  -  -  -
03 2-0   -  -  C  -  -  -
04 2-0   D  -  D  D  D  -
05 2-0   E  E  E  E  E  -
06 2-0   F  -  F  F  -  F
07 0-4   -  G  -  G  G  -
08 0-4   -  H  -  -  -  H
09 0-4   I  -  I  -  I  -
10 0-4   -  -  -  J  J  J
11 0-4   K  -  K  K  -  -
12 0-4   L  L  -  -  L  -
13 0-4   M  M  -  M  -  -
14 0-5   N  N  N  N  N  N
15 0-5   -  -  O  -  O  O
16 0-5   -  P  -  P  -  P
17 0-5   Q  Q  -  -  -  Q
18 0-5   R  -  R  -  R   
19 0-6   -  S  S  S  S   
20 1-6   -  T  T  T      
21 2-5   -  U  -  -      
22 2-6   V  -  V         
23 3-4   W  X  -         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

RURUJ JAVSL JPVLV UNPST DVAKV J
-------------------------------
