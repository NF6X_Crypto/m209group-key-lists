EFFECTIVE PERIOD:
12-DEC-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  B  -  -  B  B
03 1-0   C  C  C  -  -  -
04 1-0   -  -  D  D  D  D
05 2-0   E  E  -  -  E  -
06 2-0   F  F  F  F  -  F
07 2-0   G  -  -  G  -  G
08 2-0   -  H  H  H  -  -
09 2-0   -  -  -  -  I  -
10 2-0   -  -  -  -  J  -
11 2-0   K  -  K  -  -  -
12 0-6   L  -  L  L  L  -
13 0-6   M  M  -  M  M  M
14 0-6   N  N  -  -  N  -
15 0-6   -  O  O  -  O  O
16 1-2   P  P  P  P  -  P
17 1-2   Q  Q  Q  Q  Q  -
18 1-2   -  R  -  R  -   
19 1-2   S  -  -  S  -   
20 1-3   T  T  T  -      
21 1-4   -  -  -  U      
22 1-5   V  V  V         
23 1-6   -  -  -         
24 1-6   -  Y            
25 2-6   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WWVWT VNSLP SCAZB VWOLJ ETTUA V
-------------------------------
