EFFECTIVE PERIOD:
03-DEC-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  -
02 2-0   B  B  -  B  B  -
03 2-0   C  -  C  -  C  -
04 2-0   -  -  D  -  -  D
05 2-0   -  E  E  E  E  E
06 2-0   F  F  F  -  -  F
07 0-4   -  G  -  G  G  -
08 0-4   -  H  H  H  -  H
09 0-4   I  -  I  I  I  -
10 0-5   J  -  -  -  J  -
11 0-5   K  K  K  K  K  -
12 0-6   -  L  -  -  L  L
13 0-6   M  -  M  -  -  M
14 0-6   -  N  -  N  N  N
15 0-6   -  -  O  -  -  -
16 0-6   P  P  -  -  P  P
17 0-6   -  -  Q  Q  -  -
18 1-3   -  R  R  R  R   
19 2-4   S  -  S  -  -   
20 2-6   -  T  -  T      
21 2-6   U  U  -  U      
22 2-6   -  V  -         
23 2-6   W  -  -         
24 3-5   -  Y            
25 3-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

EYPUK YOPEP AMXIQ SSUXU NPYEO H
-------------------------------
