EFFECTIVE PERIOD:
30-DEC-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   D  -  D  D  D  -
05 1-0   E  -  E  -  -  E
06 0-4   -  F  F  F  F  -
07 0-4   -  G  -  -  G  -
08 0-4   -  H  -  -  H  H
09 0-4   I  I  I  -  I  -
10 0-4   J  J  -  J  J  J
11 0-4   -  -  -  -  K  -
12 0-4   L  -  L  L  -  L
13 0-4   -  M  -  -  -  M
14 0-5   N  N  -  N  -  N
15 0-5   O  -  O  O  -  O
16 0-5   P  -  -  P  P  P
17 0-5   Q  Q  Q  -  -  -
18 0-5   -  -  R  -  -   
19 0-5   -  S  S  S  -   
20 1-4   T  T  -  T      
21 1-4   -  U  -  U      
22 1-4   -  V  V         
23 1-4   W  -  X         
24 1-5   -  -            
25 2-5   Y  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

JTONS COUUT SJORA RIIVL UBTVU J
-------------------------------
