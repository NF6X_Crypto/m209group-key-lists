EFFECTIVE PERIOD:
03-OCT-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  A
02 2-0   B  B  -  B  -  -
03 0-3   -  C  -  C  -  -
04 0-3   -  D  -  D  -  D
05 0-3   E  -  E  -  E  -
06 0-3   F  -  F  -  F  -
07 0-4   -  -  -  -  G  G
08 0-5   -  -  H  -  H  -
09 0-5   -  -  -  I  I  I
10 0-5   -  J  J  J  -  -
11 0-5   K  -  K  -  -  -
12 0-5   L  -  L  -  L  L
13 0-5   -  M  -  -  M  M
14 0-6   N  N  N  N  -  -
15 0-6   O  -  O  O  -  -
16 0-6   P  -  P  -  P  P
17 0-6   -  Q  -  Q  -  -
18 0-6   -  -  -  R  -   
19 0-6   S  S  S  -  S   
20 1-5   -  T  -  T      
21 2-3   -  -  U  U      
22 2-4   -  -  V         
23 3-6   W  X  X         
24 5-6   -  -            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QYTAP TZYJG QFPVT WKHMW TGNUT I
-------------------------------
