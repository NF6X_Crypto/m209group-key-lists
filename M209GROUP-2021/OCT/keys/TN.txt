EFFECTIVE PERIOD:
07-OCT-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  -
02 2-0   B  -  B  -  -  B
03 2-0   -  -  C  -  -  -
04 2-0   -  -  D  D  D  D
05 2-0   E  E  -  E  -  E
06 0-3   F  F  -  F  -  -
07 0-3   -  G  -  -  -  -
08 0-3   H  -  -  -  H  H
09 0-3   -  I  -  -  I  I
10 0-3   -  J  -  J  J  -
11 0-3   K  -  K  -  K  -
12 0-4   L  -  -  L  -  L
13 0-4   M  M  M  -  -  M
14 0-4   -  N  N  N  -  -
15 0-5   O  O  O  O  -  O
16 0-5   -  -  P  -  P  P
17 0-5   Q  -  Q  -  -  Q
18 1-6   -  R  -  R  R   
19 2-4   S  S  S  -  S   
20 2-5   T  -  -  T      
21 3-4   U  -  U  U      
22 3-4   V  -  -         
23 3-4   -  -  -         
24 3-4   X  Y            
25 3-5   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SRRTN RHUKN JXQPK NMLPJ VCRAS X
-------------------------------
