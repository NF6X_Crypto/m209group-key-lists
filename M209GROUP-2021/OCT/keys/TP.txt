EFFECTIVE PERIOD:
09-OCT-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  C  -  -  -
04 1-0   D  -  -  -  D  -
05 2-0   E  E  E  E  -  -
06 0-3   F  -  -  -  F  -
07 0-4   -  G  -  G  -  G
08 0-4   -  H  -  -  -  H
09 0-4   -  -  I  I  -  -
10 0-4   -  J  J  J  J  J
11 0-4   K  K  -  K  -  K
12 0-5   -  -  L  -  L  -
13 0-5   -  -  M  -  -  M
14 0-5   N  -  N  -  N  N
15 0-5   O  -  O  O  -  -
16 0-5   P  P  -  P  P  P
17 0-5   Q  Q  -  -  -  -
18 0-5   -  R  -  R  -   
19 0-5   -  -  S  -  S   
20 0-5   T  T  T  T      
21 0-6   U  -  -  U      
22 1-4   V  V  V         
23 1-6   -  -  X         
24 2-6   -  Y            
25 4-5   -  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UIVMR RGVVO PRCYI PHVLV DUDSQ H
-------------------------------
