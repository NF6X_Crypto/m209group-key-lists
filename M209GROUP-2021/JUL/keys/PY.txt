EFFECTIVE PERIOD:
06-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  A
02 2-0   -  -  -  B  B  -
03 2-0   -  C  C  -  -  -
04 2-0   D  D  -  D  -  D
05 0-3   E  -  E  E  -  E
06 0-3   -  -  F  -  -  F
07 0-3   -  G  G  -  -  G
08 0-3   H  -  H  -  H  -
09 0-4   I  -  I  I  I  -
10 0-4   -  J  -  J  J  -
11 0-4   -  -  -  -  -  K
12 0-5   -  -  -  L  L  -
13 0-6   -  -  -  -  -  -
14 0-6   N  N  N  -  -  -
15 0-6   O  O  -  -  O  O
16 0-6   P  -  P  -  P  P
17 0-6   -  -  -  Q  -  -
18 0-6   -  -  -  -  -   
19 0-6   -  S  -  -  S   
20 1-6   T  -  T  T      
21 2-3   -  -  U  U      
22 2-4   -  V  -         
23 3-6   W  X  -         
24 3-6   -  Y            
25 3-6   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

XZIBW LIAJI VLVVO LKAOC JLNKR S
-------------------------------
