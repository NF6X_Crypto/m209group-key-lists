EFFECTIVE PERIOD:
15-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  -  -  -  -  B
03 2-0   -  -  C  C  -  -
04 2-0   -  D  D  D  -  -
05 2-0   E  E  -  -  E  E
06 2-0   F  F  -  -  F  F
07 2-0   -  -  -  G  -  G
08 0-3   H  H  -  H  H  -
09 0-3   -  I  -  I  -  I
10 0-3   J  -  J  -  -  J
11 0-4   -  -  -  -  -  K
12 0-4   -  L  -  L  L  L
13 0-4   -  M  -  -  -  M
14 0-5   -  N  -  N  N  -
15 0-5   -  -  -  O  -  -
16 0-5   P  -  -  P  P  -
17 0-5   Q  -  Q  Q  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   S  S  -  S  -   
20 1-3   T  -  T  -      
21 2-4   -  U  U  U      
22 2-4   -  -  V         
23 2-4   -  X  X         
24 2-4   -  -            
25 2-6   Y  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KKRUA HTEZF QOUMK XQIMW JHOVV I
-------------------------------
