EFFECTIVE PERIOD:
09-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 0-3   B  -  B  B  B  -
03 0-3   C  C  C  -  -  C
04 0-3   -  -  D  -  D  D
05 0-3   -  E  E  -  -  -
06 0-4   -  -  -  -  F  -
07 0-4   G  G  G  G  -  G
08 0-5   H  -  -  -  H  H
09 0-5   -  I  -  I  -  I
10 0-5   J  -  -  J  J  -
11 0-5   -  K  -  -  -  -
12 0-5   -  -  -  L  L  -
13 0-6   M  M  M  -  M  M
14 0-6   -  N  -  N  N  N
15 0-6   -  O  O  -  -  O
16 0-6   -  P  -  -  -  -
17 0-6   -  -  Q  -  Q  -
18 0-6   R  -  -  R  -   
19 0-6   -  S  -  S  S   
20 0-6   T  -  -  T      
21 0-6   -  U  -  U      
22 1-4   -  V  V         
23 3-4   W  X  -         
24 3-5   -  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JRSRN MENJR VPGYR WFNDS VSIBO A
-------------------------------
