EFFECTIVE PERIOD:
26-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  -
02 2-0   -  -  B  -  -  -
03 2-0   -  -  C  C  -  C
04 2-0   -  D  -  -  -  D
05 2-0   -  -  -  -  E  -
06 2-0   F  F  F  F  F  F
07 2-0   -  -  -  G  -  G
08 2-0   -  H  -  H  H  H
09 2-0   -  -  I  -  I  I
10 0-4   -  J  -  J  -  J
11 0-4   K  K  -  K  -  -
12 0-4   L  L  L  L  L  -
13 0-4   M  M  -  -  -  -
14 0-4   -  N  N  -  N  -
15 0-4   -  O  -  -  O  O
16 0-4   P  -  P  -  -  -
17 0-5   -  -  Q  Q  -  -
18 0-5   R  -  R  -  R   
19 0-5   S  -  S  -  -   
20 0-5   T  T  T  T      
21 0-6   U  -  U  -      
22 1-3   V  -  -         
23 1-5   -  -  -         
24 2-4   -  Y            
25 2-4   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SZGGN NGLFM CUHGU RAZUP TTSNL Z
-------------------------------
