EFFECTIVE PERIOD:
30-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  -
02 2-0   B  B  -  -  B  -
03 2-0   C  -  -  C  -  C
04 2-0   -  D  D  -  D  -
05 2-0   E  E  E  E  -  E
06 2-0   -  -  -  -  -  F
07 2-0   G  -  G  G  G  G
08 2-0   -  H  H  -  -  -
09 0-3   -  -  I  I  -  I
10 0-3   J  J  J  -  -  J
11 0-3   -  K  -  -  K  K
12 0-3   L  -  L  -  L  -
13 0-3   -  -  -  M  M  M
14 0-3   -  -  -  -  N  -
15 0-4   O  -  -  O  -  O
16 0-5   -  P  -  P  P  -
17 0-5   -  -  -  -  -  Q
18 0-5   R  -  R  -  R   
19 0-5   -  S  -  S  S   
20 0-6   T  -  -  T      
21 0-6   U  -  U  U      
22 1-6   V  V  V         
23 2-3   W  X  -         
24 2-3   X  Y            
25 2-3   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JQJAZ AJYYT RJOOW PNOWR RAAJA G
-------------------------------
