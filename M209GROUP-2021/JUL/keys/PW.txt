EFFECTIVE PERIOD:
04-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   B  -  -  B  B  -
03 1-0   C  C  C  C  -  C
04 1-0   D  D  -  -  -  -
05 1-0   -  E  E  E  -  -
06 0-3   F  F  -  -  F  -
07 0-3   -  -  -  G  G  G
08 0-3   H  -  -  H  H  H
09 0-6   -  -  -  I  I  I
10 0-6   J  J  -  -  J  -
11 0-6   K  K  K  -  -  -
12 0-6   -  L  L  -  -  L
13 0-6   -  M  -  -  M  -
14 0-6   N  N  N  N  -  -
15 0-6   -  O  O  -  O  -
16 1-3   -  -  P  P  -  -
17 1-4   -  -  Q  Q  Q  Q
18 1-4   -  -  -  -  R   
19 1-5   S  S  -  -  -   
20 1-6   T  T  T  T      
21 1-6   U  -  -  -      
22 1-6   V  -  V         
23 1-6   W  -  -         
24 2-5   -  -            
25 3-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GVFTP OAWFW EERXA RPFUZ TSFRA K
-------------------------------
