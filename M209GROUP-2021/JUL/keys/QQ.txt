EFFECTIVE PERIOD:
24-JUL-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 2-0   B  B  B  -  -  B
03 2-0   -  C  -  -  C  -
04 2-0   -  -  D  D  -  -
05 2-0   -  -  E  -  E  E
06 0-3   F  F  F  -  F  F
07 0-4   G  -  G  G  G  G
08 0-4   -  H  -  -  H  -
09 0-4   -  I  I  -  I  I
10 0-5   J  J  -  -  -  J
11 0-5   -  -  -  -  -  -
12 0-5   -  -  -  L  L  -
13 0-5   -  -  -  -  -  M
14 0-5   -  N  N  N  -  N
15 0-6   O  O  O  -  O  O
16 0-6   P  P  P  -  -  -
17 0-6   Q  -  Q  Q  Q  -
18 0-6   -  -  -  R  R   
19 0-6   S  S  -  S  S   
20 1-2   T  T  -  T      
21 2-6   U  -  U  U      
22 2-6   -  -  -         
23 2-6   -  X  X         
24 2-6   -  Y            
25 3-4   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SPLSU UKUUP QRZXI UXALV UJKBX A
-------------------------------
