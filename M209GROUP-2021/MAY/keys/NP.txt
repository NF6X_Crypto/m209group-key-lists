EFFECTIVE PERIOD:
06-MAY-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 2-0   -  B  B  B  -  B
03 2-0   -  -  C  -  -  C
04 2-0   -  D  -  D  -  -
05 2-0   -  E  -  -  -  -
06 2-0   F  -  -  F  -  -
07 2-0   G  G  G  -  G  G
08 2-0   H  -  -  -  -  -
09 2-0   -  I  -  I  I  -
10 0-3   J  -  J  -  J  -
11 0-3   -  K  K  -  K  K
12 0-3   -  -  L  L  L  L
13 0-3   -  -  M  -  M  M
14 0-3   -  -  N  N  -  N
15 0-3   -  -  O  O  -  -
16 0-3   -  P  -  P  P  -
17 0-3   Q  -  -  -  Q  -
18 0-3   R  R  R  R  R   
19 0-3   -  S  S  S  S   
20 0-5   T  -  -  -      
21 0-5   U  U  U  -      
22 2-3   V  -  -         
23 2-3   W  -  X         
24 2-3   -  Y            
25 2-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RACNZ SQREY CMXAP LRQFZ SVORR E
-------------------------------
