EFFECTIVE PERIOD:
13-FEB-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  -
02 2-0   -  B  B  -  B  -
03 2-0   -  C  -  -  -  C
04 0-3   D  D  -  D  D  -
05 0-3   E  E  -  -  E  E
06 0-3   -  -  F  F  -  F
07 0-3   -  -  -  -  -  G
08 0-4   -  -  H  -  H  H
09 0-5   I  -  -  -  I  I
10 0-5   -  -  -  J  -  -
11 0-5   K  K  -  K  -  -
12 0-5   -  L  -  L  -  L
13 0-5   -  M  M  -  -  -
14 0-5   N  N  -  N  N  N
15 0-5   -  O  O  -  O  O
16 0-5   P  -  -  -  -  -
17 0-5   -  Q  Q  -  Q  Q
18 0-6   -  R  -  R  R   
19 0-6   S  -  S  S  -   
20 1-6   -  -  T  -      
21 2-3   -  -  -  -      
22 2-6   -  V  -         
23 3-4   W  X  X         
24 4-5   X  Y            
25 4-5   Y  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FORZD TXFFA JWWHN ZMFJG JUXHX R
-------------------------------
