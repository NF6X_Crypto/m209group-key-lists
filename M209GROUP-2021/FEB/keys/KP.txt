EFFECTIVE PERIOD:
17-FEB-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  A
02 0-3   -  -  -  -  B  -
03 0-3   C  -  C  -  C  C
04 0-3   D  D  D  D  D  D
05 0-3   E  -  E  -  -  -
06 0-4   -  F  -  F  F  F
07 0-4   G  G  G  -  -  G
08 0-4   H  H  -  H  H  -
09 0-4   -  I  -  I  I  -
10 0-5   J  -  -  J  -  J
11 0-5   K  K  K  K  -  -
12 0-6   L  -  -  -  L  L
13 0-6   -  -  M  M  -  -
14 0-6   -  N  N  -  N  N
15 0-6   O  -  O  -  -  -
16 0-6   P  P  -  -  P  P
17 0-6   -  -  -  Q  -  Q
18 0-6   -  R  -  R  R   
19 0-6   S  -  S  S  S   
20 1-2   T  T  -  -      
21 2-5   -  -  U  U      
22 3-4   -  V  -         
23 3-5   -  X  -         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RUDON YUWLP WDVVQ EALTO SOSUU N
-------------------------------
