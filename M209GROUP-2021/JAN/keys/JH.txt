EFFECTIVE PERIOD:
14-JAN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  -  B  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   D  -  D  D  -  D
05 1-0   E  E  -  E  -  -
06 1-0   -  -  F  -  -  F
07 1-0   -  -  -  -  G  G
08 1-0   H  H  H  H  H  -
09 1-0   I  I  -  -  I  I
10 1-0   J  -  J  J  -  J
11 2-0   K  -  K  K  K  K
12 2-0   L  L  L  L  -  -
13 2-0   M  M  -  -  M  -
14 2-0   -  N  N  -  -  -
15 2-0   -  -  O  -  -  -
16 2-0   P  -  -  P  -  -
17 2-0   -  Q  Q  -  Q  Q
18 0-3   -  R  -  R  R   
19 0-3   -  -  S  -  S   
20 0-3   -  -  -  -      
21 0-5   U  U  U  U      
22 0-5   -  V  V         
23 0-6   -  -  -         
24 2-3   X  -            
25 3-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RHHMZ SNQTP KWMON AFHWW SORYQ F
-------------------------------
