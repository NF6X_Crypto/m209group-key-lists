EFFECTIVE PERIOD:
05-JAN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  -  -  -  -  B
03 2-0   C  C  C  -  -  C
04 2-0   D  -  D  D  D  D
05 2-0   E  E  -  -  E  -
06 2-0   -  -  F  F  -  F
07 2-0   -  -  G  -  -  -
08 2-0   H  H  -  H  -  -
09 2-0   -  -  -  I  I  I
10 2-0   J  -  J  J  J  -
11 0-3   K  K  -  K  K  -
12 0-4   L  -  -  L  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  -  N  -  N  -
15 0-5   -  O  O  -  -  -
16 0-5   -  P  P  P  -  -
17 0-5   -  -  Q  -  Q  Q
18 0-5   -  R  -  R  -   
19 0-5   -  -  S  -  S   
20 0-5   T  -  -  -      
21 0-5   U  U  -  -      
22 0-5   -  V  V         
23 0-6   -  -  X         
24 1-2   -  Y            
25 1-6   -  -            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KNLMD RMWDN CWPNF PNMZC FWDPP N
-------------------------------
