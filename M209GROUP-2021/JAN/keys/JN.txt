EFFECTIVE PERIOD:
20-JAN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 2-0   B  -  B  B  B  B
03 0-3   C  -  C  -  -  -
04 0-3   -  D  -  D  D  D
05 0-3   E  E  -  E  -  -
06 0-3   -  -  -  F  F  F
07 0-3   -  G  G  -  G  G
08 0-3   H  H  H  H  -  H
09 0-3   I  -  -  I  I  -
10 0-3   -  J  J  J  J  -
11 0-3   -  -  -  -  -  -
12 0-3   L  L  L  L  -  -
13 0-3   M  M  -  -  M  -
14 0-3   -  N  -  N  -  N
15 0-4   O  -  O  -  -  O
16 0-4   -  -  -  -  -  P
17 0-5   Q  Q  Q  -  -  Q
18 0-5   -  -  -  R  -   
19 0-5   -  S  S  -  -   
20 0-5   -  -  -  -      
21 0-5   U  U  U  -      
22 0-5   V  -  -         
23 0-5   W  -  X         
24 1-2   X  Y            
25 1-6   -  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZZCMD SEOKY GWSNN EWYHO NMHEO C
-------------------------------
