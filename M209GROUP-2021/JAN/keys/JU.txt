EFFECTIVE PERIOD:
27-JAN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  -  B  B  B  -
03 1-0   -  C  -  C  C  -
04 1-0   D  -  D  D  D  D
05 1-0   -  E  E  -  E  E
06 1-0   F  F  -  -  -  -
07 2-0   G  G  G  G  G  -
08 2-0   H  H  -  H  -  -
09 2-0   -  -  I  I  -  I
10 0-3   -  -  -  -  J  -
11 0-3   -  -  K  -  -  K
12 0-3   L  -  L  -  L  L
13 0-3   M  -  M  M  -  -
14 0-3   -  -  -  N  N  -
15 0-6   -  O  -  O  O  O
16 0-6   P  -  -  -  -  P
17 0-6   -  Q  -  -  Q  -
18 1-2   R  -  R  -  -   
19 1-2   -  S  -  -  -   
20 1-2   T  T  T  -      
21 1-2   U  U  -  U      
22 1-4   V  V  -         
23 1-5   W  -  -         
24 1-6   X  -            
25 2-3   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VGRAA VWVNN PUCNT OXVAA GNXNG H
-------------------------------
