EFFECTIVE PERIOD:
26-JAN-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  B  -  -  B
03 1-0   -  C  C  C  C  C
04 1-0   D  -  D  D  -  D
05 1-0   -  E  -  -  E  E
06 1-0   F  -  F  -  F  -
07 2-0   G  -  G  -  -  -
08 0-5   -  -  H  H  -  -
09 0-5   I  I  -  I  I  I
10 0-5   -  -  -  J  -  -
11 0-5   K  -  -  -  -  K
12 0-6   -  L  L  L  L  L
13 0-6   -  -  M  M  M  -
14 0-6   N  -  -  N  N  -
15 0-6   -  O  -  O  O  -
16 1-5   P  P  -  -  P  P
17 1-6   -  Q  Q  -  -  -
18 1-6   -  R  R  -  -   
19 1-6   S  -  S  S  -   
20 1-6   T  -  T  T      
21 2-3   -  U  -  -      
22 2-5   V  V  -         
23 2-6   -  X  -         
24 2-6   -  -            
25 2-6   Y  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

AULWS NSPOS UPMMK PSUMU GSUGS O
-------------------------------
