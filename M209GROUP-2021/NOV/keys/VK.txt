EFFECTIVE PERIOD:
25-NOV-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  -  -  -  B
03 1-0   -  C  C  C  -  C
04 1-0   D  -  D  -  -  -
05 1-0   -  -  E  -  -  -
06 1-0   F  F  -  -  F  F
07 1-0   -  -  G  G  G  G
08 1-0   -  H  -  H  H  -
09 2-0   I  -  -  I  -  I
10 2-0   J  -  J  -  -  -
11 2-0   K  K  -  -  K  K
12 2-0   -  L  L  -  L  L
13 2-0   M  -  -  M  M  -
14 0-4   N  -  -  -  -  N
15 0-5   -  O  -  -  -  O
16 0-5   -  -  P  P  P  -
17 0-6   -  -  -  -  Q  -
18 0-6   R  R  R  R  R   
19 0-6   -  S  S  -  S   
20 0-6   -  -  T  T      
21 0-6   -  -  -  U      
22 1-6   V  V  -         
23 1-6   -  X  X         
24 2-5   X  -            
25 2-6   -  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SLKQR FOSYI KTIRO NTKQO TYGNV R
-------------------------------
