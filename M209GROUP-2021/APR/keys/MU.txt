EFFECTIVE PERIOD:
15-APR-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   D  D  -  -  D  D
05 1-0   -  -  -  E  -  -
06 1-0   F  F  F  F  F  F
07 1-0   G  -  G  -  G  G
08 1-0   H  H  -  -  -  -
09 1-0   -  -  -  I  -  I
10 1-0   -  J  J  -  J  -
11 2-0   -  K  K  -  -  -
12 0-3   -  L  -  -  -  -
13 0-3   M  M  M  M  M  -
14 0-4   N  -  -  -  -  N
15 0-4   -  O  O  O  -  -
16 0-4   -  -  -  -  P  -
17 0-4   -  -  -  Q  -  Q
18 0-6   -  R  -  -  R   
19 0-6   -  S  -  S  -   
20 0-6   -  -  T  T      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   -  -  -         
24 0-6   X  -            
25 0-6   -  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

FDEIG JAIWI PRSOQ LOPKD MFEYN O
-------------------------------
