EFFECTIVE PERIOD:
23-APR-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  B  -  B  -
03 1-0   -  C  C  -  C  -
04 1-0   D  -  D  -  -  -
05 1-0   -  E  -  -  -  -
06 1-0   F  -  -  F  F  F
07 1-0   -  G  -  -  -  -
08 2-0   H  -  H  -  -  -
09 2-0   -  -  I  -  I  I
10 2-0   J  J  -  J  -  J
11 2-0   -  K  -  K  -  K
12 2-0   -  L  -  L  -  -
13 0-3   M  M  -  M  M  -
14 0-3   N  N  N  -  N  -
15 0-4   O  -  -  O  -  -
16 0-5   P  -  P  -  P  P
17 0-5   Q  Q  -  Q  Q  Q
18 0-5   R  -  -  R  -   
19 0-5   -  S  S  S  S   
20 1-2   T  T  T  T      
21 1-2   U  -  -  -      
22 1-2   -  -  -         
23 1-2   W  -  X         
24 2-5   -  Y            
25 3-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QQAZY AFTZR TDORN KROQC YRKAW Q
-------------------------------
