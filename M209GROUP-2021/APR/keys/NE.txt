EFFECTIVE PERIOD:
25-APR-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  -  B  -  -  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  -  D  -  -
05 1-0   E  -  E  -  E  -
06 1-0   -  F  -  -  F  F
07 1-0   -  G  G  G  G  -
08 0-3   -  H  -  -  -  H
09 0-3   I  -  -  -  I  I
10 0-3   -  -  -  -  J  -
11 0-4   -  K  K  -  -  -
12 0-5   L  L  L  L  -  -
13 0-5   M  -  M  -  -  M
14 0-5   -  N  N  N  N  N
15 0-5   O  -  O  O  -  -
16 0-6   P  P  -  P  -  -
17 0-6   Q  -  Q  -  Q  Q
18 0-6   -  R  -  R  -   
19 0-6   -  -  -  S  -   
20 1-2   -  T  T  T      
21 1-6   -  -  U  -      
22 1-6   V  -  V         
23 1-6   -  -  -         
24 1-6   X  Y            
25 3-4   -  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QRPPZ IWFVA FQARR SKGAQ OKRWL Q
-------------------------------
