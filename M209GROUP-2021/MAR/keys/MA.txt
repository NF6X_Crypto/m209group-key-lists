EFFECTIVE PERIOD:
26-MAR-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  -  B  B  -  B
03 1-0   C  -  C  -  -  -
04 1-0   D  -  -  D  D  -
05 1-0   E  E  -  E  E  E
06 1-0   -  -  F  -  -  F
07 0-3   -  G  -  -  -  G
08 0-3   -  -  H  -  -  -
09 0-3   -  -  I  I  I  I
10 0-3   -  -  -  J  -  -
11 0-3   K  K  -  K  K  -
12 0-3   L  -  -  -  -  -
13 0-3   -  M  -  -  M  -
14 0-5   N  N  N  -  N  N
15 0-5   -  -  O  -  -  O
16 0-5   -  -  -  P  P  -
17 0-5   Q  Q  Q  Q  -  -
18 0-5   -  R  -  R  R   
19 0-5   -  S  S  -  S   
20 1-4   T  -  -  T      
21 1-5   -  -  U  U      
22 2-3   -  -  -         
23 3-5   W  X  -         
24 3-5   -  Y            
25 3-5   -  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LZLNT NKANC UTAIH UKNUB LTHSU M
-------------------------------
