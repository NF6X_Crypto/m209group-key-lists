EFFECTIVE PERIOD:
04-MAR-2021 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 2-0   -  -  -  -  -  -
03 2-0   -  -  C  C  C  C
04 2-0   D  D  D  D  D  -
05 2-0   E  E  -  -  -  -
06 2-0   -  -  F  F  F  F
07 0-3   G  G  -  -  -  G
08 0-3   H  -  H  -  H  -
09 0-3   I  I  -  I  -  I
10 0-3   -  -  J  J  J  J
11 0-3   -  K  K  -  -  -
12 0-3   L  -  L  -  -  L
13 0-3   M  M  M  M  M  -
14 0-3   -  N  -  -  N  -
15 0-3   -  O  -  -  -  O
16 0-4   P  P  P  P  P  P
17 0-5   -  -  Q  -  -  Q
18 0-5   -  -  R  R  R   
19 0-5   S  S  -  S  S   
20 0-5   T  -  -  -      
21 0-5   U  -  U  -      
22 1-6   -  -  -         
23 2-3   W  X  -         
24 2-3   X  -            
25 2-3   -  -            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LPSOZ DIUQB UHOTL ZBADT SZOQJ X
-------------------------------
