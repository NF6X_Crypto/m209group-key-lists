EFFECTIVE PERIOD:
16-AUG-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  B  B  -  B
03 1-0   -  -  C  C  -  -
04 1-0   D  D  D  -  -  -
05 1-0   -  -  -  -  E  -
06 1-0   -  -  F  F  -  -
07 1-0   -  G  G  G  -  G
08 2-0   -  H  -  H  -  H
09 2-0   I  I  -  I  I  I
10 0-3   J  J  -  -  J  J
11 0-4   -  -  -  -  -  -
12 0-5   L  L  -  -  L  -
13 0-5   M  M  M  M  -  M
14 0-5   -  -  N  N  N  N
15 0-5   O  O  O  O  O  -
16 0-6   -  P  P  P  -  P
17 0-6   Q  -  Q  -  -  -
18 0-6   R  R  -  R  R   
19 0-6   S  S  -  -  S   
20 0-6   T  -  T  T      
21 0-6   -  U  -  -      
22 0-6   V  -  V         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 2-4   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

JMYAK LPXWL EJIKV MRIDY UGAMM Z
-------------------------------
