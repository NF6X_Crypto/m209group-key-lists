EFFECTIVE PERIOD:
15-AUG-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  A
02 0-3   -  B  B  B  -  B
03 0-3   -  -  C  C  C  C
04 0-4   D  D  -  -  D  -
05 0-4   -  -  -  E  -  E
06 0-4   -  F  F  -  -  -
07 0-4   G  -  -  G  -  G
08 0-4   H  H  -  -  -  -
09 0-4   I  -  I  -  I  -
10 0-4   J  J  -  J  -  -
11 0-4   K  K  K  K  -  K
12 0-4   L  L  L  L  L  -
13 0-5   -  M  -  -  M  M
14 0-5   -  N  N  -  -  -
15 0-5   -  O  O  -  -  -
16 0-5   P  -  -  -  P  P
17 0-6   Q  -  -  -  -  Q
18 0-6   R  -  R  R  R   
19 0-6   -  -  -  -  -   
20 0-6   -  -  T  T      
21 0-6   -  U  -  -      
22 0-6   -  V  V         
23 0-6   -  X  X         
24 1-3   X  Y            
25 3-5   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RIJZI RRSRN SAERB NGJOX TEKRN O
-------------------------------
