EFFECTIVE PERIOD:
22-JAN-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  -  -  B  -  -
03 1-0   -  -  -  -  -  -
04 1-0   -  -  D  -  -  D
05 1-0   E  E  -  -  E  E
06 1-0   -  -  F  -  F  F
07 1-0   -  G  -  -  -  G
08 2-0   -  H  H  H  H  H
09 2-0   I  I  -  -  -  -
10 2-0   -  J  -  J  -  J
11 2-0   -  -  K  K  K  -
12 0-3   L  -  -  -  L  -
13 0-3   M  M  M  M  -  -
14 0-3   -  N  -  N  N  -
15 0-3   O  -  O  -  O  O
16 0-3   P  P  P  -  -  P
17 0-4   -  Q  Q  Q  Q  Q
18 0-4   R  R  -  -  R   
19 0-4   -  S  S  -  -   
20 0-5   -  -  T  T      
21 0-6   U  -  U  U      
22 1-2   V  V  -         
23 1-2   -  -  -         
24 1-2   -  Y            
25 2-3   -  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PRWPN TTRFH LKMJA WFNNU NGWPP V
-------------------------------
