EFFECTIVE PERIOD:
04-JAN-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 2-0   B  -  B  -  -  B
03 2-0   -  C  -  C  -  -
04 2-0   -  D  -  -  D  D
05 0-3   E  E  -  -  E  E
06 0-3   F  -  -  F  F  -
07 0-3   -  G  G  G  -  -
08 0-3   H  H  -  H  H  H
09 0-4   -  -  I  I  I  I
10 0-4   J  -  -  -  -  J
11 0-5   -  K  -  -  K  -
12 0-5   L  L  L  -  L  L
13 0-5   -  -  -  M  -  -
14 0-5   N  -  N  N  -  N
15 0-5   -  O  O  -  O  -
16 1-4   -  -  P  -  -  P
17 1-5   Q  Q  -  -  Q  -
18 1-5   -  -  -  -  R   
19 1-6   -  S  S  S  S   
20 2-3   -  -  -  T      
21 2-5   -  U  -  U      
22 2-5   V  -  V         
23 2-5   W  -  X         
24 2-5   X  Y            
25 3-4   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZNNNI TWRNV AAORP WWOYM DSWFU X
-------------------------------
