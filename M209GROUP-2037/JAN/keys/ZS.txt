EFFECTIVE PERIOD:
05-JAN-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 2-0   B  -  B  B  -  -
03 2-0   -  -  -  -  C  C
04 0-3   -  -  D  D  -  -
05 0-3   E  -  -  E  E  -
06 0-3   F  F  -  -  -  F
07 0-3   -  G  -  -  -  -
08 0-3   H  -  H  -  -  -
09 0-3   I  I  I  -  I  -
10 0-3   J  J  J  J  -  -
11 0-5   K  -  K  K  K  K
12 0-5   -  -  L  -  L  -
13 0-5   M  M  M  -  -  M
14 0-5   -  N  -  N  -  N
15 0-5   -  O  -  O  O  O
16 0-5   -  P  -  -  P  -
17 0-5   -  -  Q  -  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   -  -  S  S  -   
20 0-5   -  T  -  T      
21 0-6   -  -  -  -      
22 0-6   -  V  V         
23 0-6   -  X  -         
24 0-6   -  Y            
25 0-6   Y  Z            
26 2-4   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

SDHZW UGIKJ VHRHZ AOPSA QNRTS D
-------------------------------
