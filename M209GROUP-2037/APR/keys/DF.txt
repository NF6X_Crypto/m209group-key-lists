EFFECTIVE PERIOD:
06-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  -  B  -  -
03 1-0   C  C  -  -  -  -
04 1-0   -  -  -  -  -  -
05 1-0   -  E  E  E  E  -
06 1-0   F  -  F  F  F  -
07 1-0   -  -  -  -  -  G
08 1-0   H  -  -  -  -  H
09 1-0   I  -  -  -  -  -
10 0-3   J  J  J  J  -  -
11 0-3   K  K  K  -  K  -
12 0-3   L  -  -  L  -  L
13 0-4   -  M  M  M  M  M
14 0-5   N  -  -  N  N  N
15 0-5   -  O  O  -  O  -
16 0-5   -  -  P  P  P  P
17 0-5   Q  Q  -  Q  Q  -
18 0-6   -  R  -  -  -   
19 0-6   -  S  S  -  -   
20 1-4   -  T  T  -      
21 1-4   -  U  U  -      
22 1-4   V  V  V         
23 1-4   W  -  X         
24 2-6   -  -            
25 3-5   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GAMOL MSUER FNGXN FMZXU AUAGA L
-------------------------------
