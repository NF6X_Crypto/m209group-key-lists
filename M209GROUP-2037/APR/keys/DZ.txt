EFFECTIVE PERIOD:
26-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  -  -  B  -  B
03 1-0   C  C  C  -  -  -
04 1-0   -  D  -  -  -  D
05 2-0   -  E  E  E  -  -
06 2-0   F  -  -  F  -  F
07 2-0   -  -  -  -  G  G
08 2-0   H  H  H  -  H  H
09 2-0   I  I  I  I  I  -
10 2-0   J  -  J  -  J  J
11 0-3   K  K  K  K  K  -
12 0-3   -  L  L  L  -  -
13 0-3   -  -  M  -  M  M
14 0-4   -  -  -  N  N  -
15 0-4   -  O  -  -  O  O
16 1-2   -  P  P  -  -  -
17 1-4   Q  -  -  Q  -  -
18 1-4   R  -  -  R  R   
19 1-4   -  -  -  S  -   
20 1-4   T  -  T  -      
21 2-3   U  U  -  U      
22 3-4   -  -  V         
23 3-4   W  -  -         
24 3-4   -  Y            
25 3-6   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

HWSAQ PWIPS PHRXW ILPXA UYWZZ W
-------------------------------
