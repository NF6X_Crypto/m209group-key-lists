EFFECTIVE PERIOD:
01-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 2-0   B  B  B  B  -  -
03 2-0   -  C  C  -  C  C
04 2-0   D  D  -  -  D  -
05 0-3   -  E  -  -  -  -
06 0-3   F  -  F  F  -  F
07 0-3   G  -  -  G  -  -
08 0-3   -  -  -  -  H  -
09 0-3   -  I  I  I  I  I
10 0-3   J  J  -  -  -  J
11 0-4   K  -  -  K  -  K
12 0-4   L  -  L  -  -  -
13 0-5   M  M  M  M  -  M
14 0-6   -  N  -  -  -  N
15 0-6   O  -  O  -  O  -
16 0-6   -  P  P  P  -  -
17 0-6   -  Q  -  Q  Q  -
18 0-6   R  -  R  -  R   
19 0-6   S  S  S  S  S   
20 0-6   -  T  -  T      
21 0-6   -  U  U  -      
22 1-4   V  -  -         
23 2-3   W  X  X         
24 2-4   -  -            
25 3-6   Y  -            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ONXQQ OJPFW EKXEI ZNPJS ROMGR P
-------------------------------
