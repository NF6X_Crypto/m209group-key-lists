EFFECTIVE PERIOD:
11-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  A  A  -
02 0-3   -  -  -  B  -  B
03 0-3   -  -  -  C  C  -
04 0-3   -  D  D  D  -  D
05 0-3   -  E  -  E  E  -
06 0-3   F  -  -  -  -  F
07 0-4   -  G  G  G  -  -
08 0-4   H  -  H  H  H  -
09 0-4   I  I  -  -  I  I
10 0-4   -  -  -  -  -  J
11 0-4   -  -  K  K  K  -
12 0-4   -  L  -  L  -  L
13 0-5   -  -  -  M  M  M
14 0-6   N  -  N  N  -  N
15 0-6   -  O  O  -  -  -
16 0-6   P  P  -  -  P  P
17 0-6   -  -  Q  -  Q  -
18 1-2   R  -  R  -  -   
19 1-3   S  -  -  -  -   
20 1-5   T  -  -  T      
21 3-4   -  -  U  -      
22 3-4   V  V  -         
23 3-4   W  X  -         
24 3-4   X  Y            
25 3-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UAITP ICUKL ANSSA TAMKS ZKQOP G
-------------------------------
