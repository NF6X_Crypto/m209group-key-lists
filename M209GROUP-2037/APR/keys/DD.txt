EFFECTIVE PERIOD:
04-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  -
02 2-0   -  B  -  -  B  -
03 2-0   C  -  -  C  -  -
04 2-0   -  D  D  -  -  -
05 2-0   E  E  E  -  -  -
06 2-0   F  -  -  F  F  F
07 2-0   G  -  -  -  -  G
08 2-0   H  -  H  -  H  H
09 2-0   -  I  I  I  I  -
10 2-0   J  -  -  -  -  J
11 2-0   K  K  K  K  K  K
12 2-0   L  L  -  -  -  -
13 0-3   -  M  -  M  M  -
14 0-3   -  N  N  N  N  -
15 0-3   O  O  -  O  -  O
16 0-3   -  -  P  P  -  -
17 0-3   Q  Q  Q  Q  -  Q
18 0-3   -  R  -  R  R   
19 0-3   -  S  -  -  -   
20 0-3   -  -  T  T      
21 0-3   U  U  -  U      
22 0-3   V  -  V         
23 0-4   -  -  X         
24 0-4   -  -            
25 0-6   Y  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZDZYP DYCML LCMMZ NCOLO LQPZO Z
-------------------------------
