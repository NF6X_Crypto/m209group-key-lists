EFFECTIVE PERIOD:
19-APR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  B  -  -  B  B
03 1-0   C  -  C  -  C  C
04 1-0   -  D  D  D  -  D
05 1-0   -  -  E  E  E  -
06 2-0   F  -  F  -  -  -
07 2-0   G  -  -  G  -  G
08 2-0   -  -  -  H  H  -
09 2-0   -  -  -  -  -  I
10 2-0   J  J  J  J  -  -
11 2-0   K  -  -  -  K  K
12 0-5   L  L  L  L  L  -
13 0-5   M  -  M  -  M  M
14 0-5   N  N  -  -  N  -
15 0-5   -  -  O  -  -  O
16 0-6   -  P  P  P  -  P
17 0-6   Q  -  -  -  -  -
18 0-6   R  R  R  R  -   
19 0-6   -  S  S  -  -   
20 1-3   T  -  -  T      
21 1-6   -  U  -  U      
22 1-6   -  -  V         
23 1-6   -  X  -         
24 1-6   -  Y            
25 2-5   Y  -            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

OWUIV KHJVA URWPI PKOFP WUVJW J
-------------------------------
