EFFECTIVE PERIOD:
17-MAR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   -  B  B  B  -  B
03 1-0   C  -  -  -  C  -
04 1-0   D  D  -  D  D  D
05 1-0   -  E  E  E  E  E
06 2-0   F  -  -  F  -  F
07 0-4   G  G  G  G  G  G
08 0-4   -  H  -  -  H  -
09 0-4   I  -  -  -  -  -
10 0-4   -  J  J  -  -  J
11 0-4   -  -  K  -  -  K
12 0-5   -  -  L  L  -  -
13 0-6   -  M  -  M  M  M
14 0-6   -  -  N  N  -  -
15 0-6   O  -  -  O  O  -
16 0-6   P  -  P  -  -  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   -  -  -  -  R   
19 0-6   -  -  -  -  -   
20 0-6   -  T  T  -      
21 0-6   U  U  U  -      
22 1-3   V  V  V         
23 1-4   W  -  X         
24 3-5   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

YKUBI VVIUH VVUXR LTTAT MKTUB Z
-------------------------------
