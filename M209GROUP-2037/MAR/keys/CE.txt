EFFECTIVE PERIOD:
10-MAR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  -  B  B  -  -
03 1-0   -  -  C  C  -  C
04 1-0   -  -  -  -  -  D
05 1-0   E  E  E  -  E  E
06 1-0   F  F  F  -  F  F
07 2-0   -  -  G  -  G  G
08 0-3   H  -  H  H  -  -
09 0-5   I  -  -  I  -  I
10 0-5   J  J  -  -  J  -
11 0-5   -  K  K  -  K  -
12 0-5   -  -  L  L  L  -
13 0-6   -  M  M  -  -  -
14 0-6   -  N  -  N  N  -
15 0-6   -  -  O  O  O  -
16 0-6   -  P  -  -  P  P
17 0-6   Q  Q  Q  -  -  Q
18 0-6   -  R  R  -  -   
19 0-6   -  S  -  S  -   
20 0-6   -  -  -  T      
21 0-6   -  U  -  U      
22 1-5   V  -  -         
23 1-6   -  X  -         
24 1-6   X  -            
25 1-6   Y  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

FNHII LUMQT VWTRH WNHOO NITTP Z
-------------------------------
