EFFECTIVE PERIOD:
04-MAR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  B  -  B  B  -
03 1-0   -  C  C  C  C  C
04 1-0   -  D  D  D  -  D
05 1-0   -  -  E  -  -  E
06 1-0   F  -  F  -  F  -
07 1-0   -  -  G  -  -  G
08 1-0   -  H  -  -  -  H
09 1-0   I  I  -  -  -  I
10 2-0   J  J  J  J  J  -
11 2-0   -  -  -  K  -  K
12 2-0   -  -  -  -  -  -
13 2-0   M  -  -  -  M  M
14 0-3   N  -  N  N  N  -
15 0-3   O  -  O  O  -  O
16 0-4   -  P  P  P  -  -
17 0-6   Q  Q  Q  Q  Q  -
18 0-6   R  R  -  R  -   
19 0-6   S  S  -  S  S   
20 0-6   -  -  T  -      
21 0-6   U  U  -  U      
22 0-6   V  -  -         
23 0-6   W  X  -         
24 2-3   -  Y            
25 2-6   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

LTATP LTJIW NKTQP LMIIA PICOO O
-------------------------------
