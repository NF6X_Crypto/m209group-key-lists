EFFECTIVE PERIOD:
23-MAR-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   -  -  -  -  -  B
03 2-0   C  C  C  C  C  -
04 2-0   D  D  D  -  -  -
05 2-0   E  -  E  E  -  -
06 2-0   F  F  F  -  -  F
07 2-0   -  -  -  -  G  G
08 2-0   -  -  H  H  H  H
09 2-0   -  -  I  -  I  I
10 2-0   J  J  J  J  J  J
11 0-3   -  -  -  -  -  K
12 0-3   L  L  -  L  -  -
13 0-3   M  M  -  M  -  -
14 0-3   N  -  N  -  N  -
15 0-3   -  O  -  O  -  O
16 0-3   P  -  -  P  -  -
17 0-3   Q  -  Q  -  -  -
18 0-3   -  R  R  -  R   
19 0-4   -  S  -  S  S   
20 0-4   -  -  -  -      
21 0-5   -  U  -  U      
22 1-6   V  -  -         
23 2-3   W  -  -         
24 2-3   X  -            
25 2-3   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZAEYX ZSBEE MRNZY OSOOL RFNZE P
-------------------------------
