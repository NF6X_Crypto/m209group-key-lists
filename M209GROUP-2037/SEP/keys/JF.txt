EFFECTIVE PERIOD:
09-SEP-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  -  -  B  -  B
03 1-0   C  C  -  C  C  -
04 2-0   -  -  D  -  -  -
05 2-0   -  -  -  E  -  E
06 2-0   -  F  -  -  -  -
07 2-0   -  -  G  -  -  G
08 2-0   H  -  -  H  H  -
09 0-4   -  -  I  I  -  -
10 0-4   -  J  -  J  -  -
11 0-4   K  -  -  -  K  -
12 0-5   L  L  L  -  L  -
13 0-5   M  -  M  -  M  M
14 0-5   -  N  N  N  N  N
15 0-5   O  -  -  -  -  -
16 1-2   -  -  P  P  P  P
17 1-5   Q  Q  -  -  -  Q
18 1-5   R  R  R  R  -   
19 1-5   -  -  S  S  -   
20 1-5   -  T  T  -      
21 2-4   -  -  U  U      
22 3-4   -  V  -         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-5   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NUXMN PJLVS ZMZWZ LPFVM TRWRM M
-------------------------------
