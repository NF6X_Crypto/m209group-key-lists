EFFECTIVE PERIOD:
17-SEP-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   B  B  B  B  B  B
03 2-0   -  C  C  -  C  C
04 2-0   -  -  -  -  D  D
05 2-0   -  -  E  E  E  -
06 2-0   -  -  -  F  F  -
07 2-0   G  G  -  -  G  -
08 2-0   -  -  -  H  -  -
09 0-4   -  I  -  -  I  -
10 0-4   -  J  J  -  -  -
11 0-4   K  K  K  K  -  K
12 0-4   -  -  -  L  L  -
13 0-4   M  M  M  M  M  M
14 0-4   N  -  -  -  -  N
15 0-4   O  O  O  O  -  O
16 0-4   -  P  P  -  P  P
17 0-4   Q  Q  -  -  -  -
18 0-5   -  R  -  R  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  T  -      
21 0-6   U  -  U  -      
22 1-3   V  -  V         
23 1-6   W  -  -         
24 2-4   X  Y            
25 2-4   -  -            
26 2-4   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

XOXQR ZPIHO UOOTN ZMQZY YZFXH Q
-------------------------------
