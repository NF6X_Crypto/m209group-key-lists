EFFECTIVE PERIOD:
16-SEP-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  B  -  B  -  -
03 1-0   C  C  -  -  C  C
04 1-0   D  -  D  -  -  -
05 2-0   -  E  -  E  E  E
06 0-3   -  -  -  F  F  -
07 0-3   G  G  G  -  -  G
08 0-3   H  -  H  -  H  -
09 0-3   -  -  -  -  I  I
10 0-3   J  -  J  J  -  -
11 0-4   -  K  K  K  K  K
12 0-4   L  L  L  L  L  -
13 0-5   M  -  M  M  -  M
14 0-5   -  -  N  N  -  N
15 0-5   O  -  O  -  O  -
16 0-5   P  P  -  P  P  P
17 0-5   -  -  -  Q  -  -
18 0-5   R  -  -  R  -   
19 0-5   S  S  S  -  -   
20 0-5   -  T  T  -      
21 0-5   -  U  U  -      
22 1-3   -  -  V         
23 1-5   W  X  -         
24 1-5   X  -            
25 1-5   -  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZKIUQ KHOXA PIRAL NSPUV QKAWJ L
-------------------------------
