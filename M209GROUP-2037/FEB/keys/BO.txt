EFFECTIVE PERIOD:
22-FEB-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  B  B  -  B  B
03 1-0   C  -  -  -  C  -
04 1-0   D  D  D  D  -  D
05 1-0   E  -  -  E  -  -
06 1-0   -  -  F  F  -  -
07 1-0   G  G  G  -  G  G
08 2-0   -  H  H  -  -  H
09 2-0   I  -  I  I  I  -
10 0-3   J  -  -  J  J  -
11 0-4   K  -  K  -  K  -
12 0-4   -  -  L  L  -  -
13 0-4   M  M  -  -  -  M
14 0-4   -  N  -  N  -  N
15 0-5   O  -  O  O  O  O
16 0-5   -  -  P  P  P  -
17 0-5   -  Q  -  -  Q  -
18 0-5   R  -  -  R  R   
19 0-5   -  -  S  -  -   
20 0-5   -  -  T  T      
21 0-5   -  -  U  U      
22 0-5   -  -  -         
23 0-5   W  -  -         
24 0-5   X  Y            
25 0-6   Y  Z            
26 2-3   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

ZGQPK QLNWU OLOOZ ILZSH OJOYS T
-------------------------------
