EFFECTIVE PERIOD:
25-FEB-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  B  B  B  -  B
03 1-0   -  C  C  -  -  C
04 0-3   D  D  D  -  D  D
05 0-4   -  -  -  E  E  -
06 0-4   F  -  -  -  F  -
07 0-4   -  G  G  -  -  -
08 0-4   -  H  H  H  H  H
09 0-4   I  I  -  I  -  -
10 0-4   J  J  J  J  -  J
11 0-4   -  -  K  K  -  K
12 0-4   -  L  -  -  L  L
13 0-4   -  -  M  M  -  -
14 0-4   N  N  N  -  -  -
15 0-5   -  -  O  -  -  O
16 0-5   -  -  P  P  -  -
17 0-5   Q  -  -  -  Q  -
18 0-6   -  -  -  R  -   
19 0-6   S  -  S  S  S   
20 0-6   T  T  -  T      
21 0-6   -  U  -  U      
22 1-4   -  V  -         
23 1-4   W  -  -         
24 1-6   X  Y            
25 2-3   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

XNCPK SMVYG SGMSL EXUSW IRCCW I
-------------------------------
