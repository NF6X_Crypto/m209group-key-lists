EFFECTIVE PERIOD:
20-FEB-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 2-0   B  -  B  B  -  B
03 2-0   -  -  C  C  C  -
04 2-0   D  D  -  -  D  D
05 2-0   -  E  E  E  -  -
06 2-0   -  F  F  F  F  F
07 0-3   -  -  -  -  G  -
08 0-4   -  H  -  -  -  -
09 0-4   I  -  -  -  -  -
10 0-4   J  -  J  -  J  J
11 0-4   K  K  -  K  -  K
12 0-4   -  -  -  -  L  -
13 0-4   -  -  -  -  M  -
14 0-4   -  -  -  N  -  N
15 0-4   -  O  O  O  O  O
16 0-4   -  -  P  -  P  P
17 0-4   Q  Q  Q  -  -  -
18 0-5   -  -  -  -  R   
19 0-5   S  S  -  -  S   
20 0-5   -  T  T  T      
21 0-5   -  U  -  U      
22 0-5   -  -  -         
23 0-5   -  X  -         
24 0-5   X  -            
25 0-6   Y  Z            
26 1-6   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

OTQLO HNTSB ATEQZ GHJFT KITQD P
-------------------------------
