EFFECTIVE PERIOD:
01-OCT-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  -  B  B  -  -
03 1-0   C  -  -  C  C  C
04 1-0   D  D  D  D  -  D
05 1-0   -  E  E  -  E  E
06 2-0   F  -  -  F  F  F
07 2-0   -  -  -  -  -  G
08 2-0   -  -  -  H  -  H
09 0-3   -  I  I  I  I  -
10 0-3   -  J  -  -  -  J
11 0-3   -  -  K  K  K  K
12 0-3   -  L  L  -  -  -
13 0-3   M  -  M  M  -  -
14 0-6   N  N  -  -  -  N
15 0-6   -  O  -  O  -  -
16 0-6   P  P  -  -  P  -
17 0-6   Q  -  Q  Q  Q  Q
18 1-2   R  R  -  -  -   
19 1-3   S  S  S  -  -   
20 2-5   T  T  -  -      
21 2-6   -  -  -  -      
22 3-6   V  -  V         
23 3-6   -  X  -         
24 3-6   -  Y            
25 3-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VLHZN RQZRO RQLXA FSKMG UKPSN G
-------------------------------
