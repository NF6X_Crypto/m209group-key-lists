EFFECTIVE PERIOD:
10-OCT-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   -  B  B  -  B  B
03 1-0   C  -  C  -  -  C
04 1-0   D  D  D  -  D  D
05 2-0   E  -  -  -  E  -
06 2-0   F  F  -  -  -  -
07 2-0   G  G  -  G  G  -
08 2-0   -  H  H  -  H  -
09 2-0   -  -  I  I  I  I
10 0-4   -  J  -  -  -  -
11 0-4   -  K  K  K  -  K
12 0-4   L  -  L  L  L  L
13 0-6   -  -  -  M  M  -
14 0-6   N  -  -  N  N  N
15 0-6   -  -  -  O  -  -
16 0-6   P  P  P  P  P  P
17 0-6   -  -  Q  -  -  Q
18 1-3   R  -  -  R  R   
19 1-3   -  S  S  -  -   
20 1-4   -  -  -  -      
21 1-4   U  U  -  U      
22 1-4   V  -  -         
23 1-4   W  X  -         
24 2-3   X  -            
25 2-6   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UUPRH VXJZO QJJLR URAPI WRTON K
-------------------------------
