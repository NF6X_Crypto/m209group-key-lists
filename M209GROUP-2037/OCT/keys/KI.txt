EFFECTIVE PERIOD:
08-OCT-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  -  B  B  -  B
03 1-0   -  C  -  -  C  -
04 1-0   -  -  D  -  D  D
05 0-4   E  E  E  E  E  E
06 0-4   -  -  F  -  -  -
07 0-4   G  G  -  G  -  -
08 0-4   H  H  H  -  -  H
09 0-4   I  -  I  -  I  I
10 0-4   J  J  -  J  J  J
11 0-4   -  K  -  K  K  K
12 0-5   -  L  -  L  -  -
13 0-6   -  -  -  M  -  -
14 0-6   -  -  N  N  N  N
15 0-6   O  -  -  O  O  O
16 0-6   P  -  P  -  P  P
17 0-6   -  Q  -  -  -  -
18 0-6   R  -  -  -  R   
19 0-6   -  -  S  S  S   
20 0-6   -  -  -  T      
21 0-6   U  -  U  U      
22 1-2   V  -  V         
23 1-4   W  X  X         
24 2-3   X  Y            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZKRMM SQRZB WTGMM ZMTRP ZWWMZ T
-------------------------------
