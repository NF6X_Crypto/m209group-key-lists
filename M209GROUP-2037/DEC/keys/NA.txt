EFFECTIVE PERIOD:
17-DEC-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  C  -  C
04 1-0   D  D  D  D  D  D
05 1-0   -  -  E  E  -  -
06 1-0   -  F  F  -  F  -
07 1-0   G  G  -  -  G  G
08 1-0   H  -  H  H  -  H
09 1-0   -  I  I  I  I  I
10 1-0   -  -  -  -  J  -
11 1-0   K  K  -  -  K  -
12 2-0   L  -  -  -  -  -
13 2-0   -  -  M  -  -  M
14 2-0   -  N  -  N  -  N
15 2-0   O  -  O  -  -  -
16 2-0   -  -  P  P  -  -
17 2-0   Q  -  Q  Q  -  -
18 2-0   R  R  -  R  R   
19 2-0   S  S  -  S  -   
20 0-3   -  -  T  -      
21 0-3   -  -  -  U      
22 0-4   -  V  -         
23 0-5   -  -  -         
24 1-2   X  Y            
25 2-3   Y  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZRQZM CGKPY YAOAZ ENAQK AURZO S
-------------------------------
