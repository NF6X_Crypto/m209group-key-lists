EFFECTIVE PERIOD:
02-DEC-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ML
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  -
02 2-0   -  B  -  B  -  B
03 2-0   -  -  -  C  C  C
04 2-0   D  -  D  -  -  -
05 0-3   E  -  -  E  -  -
06 0-3   F  F  F  -  -  -
07 0-3   G  -  -  -  -  G
08 0-3   H  -  H  -  H  -
09 0-3   -  -  I  I  I  -
10 0-3   -  J  -  J  -  -
11 0-5   K  K  -  -  K  K
12 0-5   -  -  L  L  -  L
13 0-5   M  M  -  M  -  -
14 0-6   -  N  N  N  N  N
15 0-6   -  O  O  -  O  O
16 0-6   P  -  -  -  P  P
17 0-6   Q  -  -  -  Q  -
18 1-4   R  R  R  R  R   
19 2-3   -  S  S  S  S   
20 2-6   -  T  -  -      
21 2-6   U  -  U  U      
22 2-6   -  -  V         
23 2-6   -  X  -         
24 3-5   X  Y            
25 4-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XTGTZ AHMXP QLITA NPTWZ QAPVU T
-------------------------------
