EFFECTIVE PERIOD:
24-NOV-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 2-0   -  B  -  -  B  B
03 2-0   -  -  -  -  C  -
04 2-0   -  D  -  D  -  -
05 2-0   -  E  E  -  E  -
06 2-0   F  -  F  -  -  -
07 2-0   -  -  -  G  G  -
08 2-0   H  H  -  H  -  -
09 2-0   -  I  I  I  -  I
10 2-0   -  -  J  J  J  -
11 2-0   K  -  -  -  K  K
12 0-3   L  -  L  L  -  L
13 0-3   -  -  -  -  -  -
14 0-3   -  N  -  -  N  -
15 0-3   O  -  -  -  -  O
16 0-3   P  P  P  -  -  -
17 0-4   -  Q  -  Q  -  Q
18 0-4   R  R  R  -  R   
19 0-4   S  -  -  S  S   
20 0-6   -  T  T  -      
21 0-6   -  -  U  -      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 0-6   -  Y            
25 0-6   -  -            
26 1-4   Z               
27 1-5                   
-------------------------------
26 LETTER CHECK

IPRMO JQXPO HZOZP YFKAO DNCFP Q
-------------------------------
