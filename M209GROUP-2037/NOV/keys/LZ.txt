EFFECTIVE PERIOD:
20-NOV-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 0-4   -  B  B  B  -  -
03 0-4   -  C  -  C  C  C
04 0-4   -  -  -  -  -  -
05 0-4   -  -  -  -  -  E
06 0-4   -  -  F  -  F  -
07 0-4   G  -  G  -  -  -
08 0-4   H  H  -  H  H  H
09 0-4   -  -  -  I  -  -
10 0-4   J  J  -  J  J  J
11 0-4   K  -  -  -  -  -
12 0-4   -  L  L  L  L  L
13 0-5   M  -  -  M  -  -
14 0-5   -  N  N  N  -  N
15 0-5   -  O  O  -  O  O
16 0-5   P  P  -  P  P  P
17 0-5   Q  -  Q  Q  -  -
18 0-5   R  R  -  -  -   
19 0-5   -  S  -  S  S   
20 0-5   T  T  T  T      
21 0-6   U  U  U  -      
22 0-6   V  -  -         
23 0-6   -  X  X         
24 0-6   X  -            
25 0-6   -  -            
26 1-3   Z               
27 1-6                   
-------------------------------
26 LETTER CHECK

VSBAA HNZKG URAIF IRUSH SRZJS L
-------------------------------
