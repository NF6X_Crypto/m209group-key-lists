EFFECTIVE PERIOD:
14-JUN-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  B  -  -  -
03 1-0   C  -  -  C  -  C
04 1-0   D  -  D  D  -  -
05 1-0   -  -  E  -  E  E
06 2-0   F  F  -  F  -  -
07 2-0   G  -  -  G  G  G
08 2-0   H  -  H  H  -  -
09 2-0   -  -  I  I  I  I
10 0-3   J  J  -  -  -  J
11 0-4   K  K  -  K  -  -
12 0-4   -  L  L  -  L  -
13 0-4   -  -  M  M  -  M
14 0-4   N  N  N  N  -  -
15 0-5   -  -  O  -  O  -
16 0-5   -  -  -  -  P  -
17 0-5   Q  -  -  -  -  Q
18 1-4   R  -  R  R  R   
19 1-5   S  -  S  -  -   
20 2-3   -  -  -  -      
21 2-3   -  U  -  U      
22 2-5   -  V  -         
23 2-5   -  X  -         
24 2-5   -  -            
25 2-5   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

RPHNM JOKTM QXNLW GZPLO QXVMT H
-------------------------------
