EFFECTIVE PERIOD:
13-MAY-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  B  B  -  -
03 1-0   C  -  -  C  C  -
04 1-0   -  -  -  -  D  D
05 1-0   -  E  -  E  E  -
06 1-0   -  -  F  -  -  F
07 2-0   G  G  G  G  G  -
08 0-4   -  -  -  H  -  -
09 0-4   I  I  I  I  -  -
10 0-5   J  J  J  J  -  -
11 0-5   K  -  K  -  -  K
12 0-5   -  -  -  -  -  L
13 0-5   -  -  -  -  M  M
14 0-5   -  -  -  -  -  -
15 0-5   O  -  O  O  O  O
16 0-5   P  P  -  -  P  P
17 0-6   -  Q  -  Q  Q  -
18 0-6   R  -  R  R  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  T  T      
21 0-6   -  U  U  -      
22 1-4   V  V  V         
23 1-6   W  -  -         
24 3-4   -  -            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PRTRA ULVAH ANWKJ EDTMR TPPNK K
-------------------------------
