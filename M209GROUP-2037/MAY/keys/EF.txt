EFFECTIVE PERIOD:
02-MAY-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  -  B  -  -  B
03 1-0   -  C  -  -  -  C
04 1-0   D  -  D  D  D  -
05 1-0   E  -  E  -  -  -
06 1-0   -  F  -  F  F  F
07 0-3   -  G  -  G  -  -
08 0-3   -  H  H  -  H  H
09 0-3   I  I  I  I  -  I
10 0-3   J  -  -  J  J  J
11 0-3   -  K  K  -  K  -
12 0-3   -  L  L  -  L  -
13 0-3   -  M  M  -  M  M
14 0-3   N  -  -  N  -  N
15 0-4   -  O  O  O  -  O
16 0-5   P  -  -  P  -  -
17 0-5   -  -  Q  -  -  -
18 0-5   R  -  -  R  R   
19 0-5   S  S  -  S  S   
20 0-5   -  -  -  -      
21 0-6   -  U  U  -      
22 1-3   V  V  V         
23 1-3   W  X  -         
24 1-3   X  Y            
25 1-5   -  -            
26 2-5   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

QSMUC ARZUD KRMSU SCTTR ZNNCU I
-------------------------------
