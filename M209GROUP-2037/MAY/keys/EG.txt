EFFECTIVE PERIOD:
03-MAY-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 2-0   B  B  -  -  B  -
03 2-0   C  -  C  C  C  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  -  -  -  E
06 0-3   -  -  -  F  F  F
07 0-3   -  -  G  -  -  G
08 0-4   -  -  H  -  -  H
09 0-4   -  I  I  I  I  -
10 0-4   J  -  J  -  -  -
11 0-4   K  -  -  K  -  K
12 0-5   -  L  -  -  L  -
13 0-5   -  M  M  M  M  M
14 0-5   -  N  -  N  N  N
15 0-5   -  O  O  O  O  -
16 0-5   P  -  P  P  P  P
17 0-5   -  Q  Q  -  -  Q
18 1-4   -  R  -  R  -   
19 2-4   S  -  S  S  -   
20 2-4   T  T  T  -      
21 2-4   U  U  -  -      
22 2-4   V  V  V         
23 2-5   -  -  -         
24 3-4   -  Y            
25 3-5   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

YVAAI UULLQ RRUNR ALOEA KSAYU O
-------------------------------
