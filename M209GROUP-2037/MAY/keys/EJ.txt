EFFECTIVE PERIOD:
06-MAY-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   B  B  B  B  B  B
03 1-0   C  C  -  -  C  C
04 1-0   -  D  D  -  -  -
05 1-0   -  -  E  -  -  -
06 1-0   F  -  F  -  -  F
07 2-0   -  G  G  G  -  -
08 2-0   -  H  -  H  H  -
09 2-0   -  -  I  I  -  I
10 2-0   -  J  J  J  J  J
11 0-5   -  K  K  K  K  -
12 0-5   -  -  -  -  -  -
13 0-5   M  M  M  -  M  M
14 0-5   -  N  -  N  -  -
15 0-6   O  -  -  -  -  O
16 1-5   P  P  P  -  -  -
17 1-6   Q  -  -  -  -  Q
18 2-3   R  R  -  R  R   
19 2-3   -  -  -  S  S   
20 2-5   T  -  -  -      
21 2-5   -  -  U  U      
22 2-5   -  -  V         
23 2-5   W  X  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

HYOLL AOWUT MVVVN QORSH SODVO W
-------------------------------
