EFFECTIVE PERIOD:
29-MAY-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  -  -  A  A
02 0-3   -  B  -  B  -  B
03 0-3   C  -  C  -  C  -
04 0-3   -  -  -  -  D  -
05 0-4   -  E  E  E  E  E
06 0-4   F  F  -  F  -  F
07 0-4   G  -  -  -  -  -
08 0-4   -  H  H  H  H  H
09 0-4   I  I  I  -  -  -
10 0-4   -  J  J  -  -  J
11 0-5   -  -  -  -  -  -
12 0-5   L  L  -  -  L  L
13 0-5   M  M  -  M  -  M
14 0-5   N  -  -  -  -  -
15 0-6   O  -  -  O  -  O
16 0-6   P  P  P  P  -  -
17 0-6   -  -  -  Q  Q  -
18 1-5   R  R  -  R  -   
19 1-6   -  -  -  S  S   
20 2-6   T  T  T  T      
21 3-4   U  U  U  -      
22 3-6   -  -  V         
23 3-6   W  X  X         
24 3-6   X  -            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OJWIS PQAUI OPUJV HSKOU ARCKW W
-------------------------------
