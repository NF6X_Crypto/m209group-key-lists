EFFECTIVE PERIOD:
19-JUL-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  B  -  -
03 1-0   -  -  -  C  -  C
04 0-3   -  D  -  D  -  -
05 0-3   E  E  E  -  E  -
06 0-3   F  F  -  F  -  -
07 0-3   -  G  G  -  G  G
08 0-3   H  -  H  -  -  H
09 0-3   -  I  -  -  -  -
10 0-4   -  J  -  -  -  J
11 0-5   K  -  K  -  K  -
12 0-5   -  L  L  L  -  -
13 0-5   -  -  M  M  M  -
14 0-5   N  N  N  N  N  -
15 0-5   O  -  -  O  -  -
16 0-5   P  P  P  -  -  P
17 0-5   Q  -  Q  -  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   S  -  -  -  S   
20 0-6   -  -  -  T      
21 0-6   U  -  -  U      
22 0-6   -  -  V         
23 0-6   -  X  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 1-4   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

LOUKF SFRUR LFLFX EQGMA RUOOT J
-------------------------------
