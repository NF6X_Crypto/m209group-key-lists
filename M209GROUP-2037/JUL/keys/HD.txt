EFFECTIVE PERIOD:
17-JUL-2037 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  -  B  -  B
03 1-0   -  C  C  -  C  C
04 2-0   D  -  -  -  D  -
05 2-0   E  -  E  -  -  -
06 0-3   -  F  -  F  F  F
07 0-4   G  G  -  G  -  G
08 0-4   H  -  -  -  H  H
09 0-4   I  -  I  -  -  I
10 0-4   -  -  -  J  -  -
11 0-4   -  -  K  -  -  -
12 0-4   L  L  -  -  L  -
13 0-6   -  -  M  -  M  -
14 0-6   -  -  N  N  N  N
15 0-6   -  O  O  O  -  O
16 0-6   P  -  P  P  P  P
17 0-6   Q  -  Q  Q  -  Q
18 0-6   R  -  -  -  R   
19 0-6   S  -  S  -  -   
20 1-2   T  T  -  -      
21 1-4   -  U  -  U      
22 2-3   V  V  -         
23 4-6   W  -  X         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JRRYW QTHSA PESPY WDHQF NLAUM X
-------------------------------
