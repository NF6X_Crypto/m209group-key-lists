EFFECTIVE PERIOD:
17-FEB-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  -  -  -  B  -
03 1-0   C  C  C  C  -  -
04 1-0   -  -  -  D  D  D
05 1-0   -  E  -  -  -  E
06 0-4   F  F  -  F  F  F
07 0-5   -  -  G  -  -  -
08 0-5   -  -  H  H  -  H
09 0-5   -  -  I  -  I  I
10 0-5   J  J  J  J  J  -
11 0-5   -  -  K  -  -  -
12 0-6   -  -  -  -  -  -
13 0-6   M  M  -  -  M  M
14 0-6   N  -  -  N  -  N
15 0-6   -  -  -  O  -  -
16 0-6   -  -  -  -  P  P
17 0-6   Q  Q  Q  -  Q  -
18 1-3   -  R  R  R  R   
19 1-4   -  S  S  -  -   
20 1-4   -  -  -  -      
21 1-5   U  U  -  U      
22 1-5   -  V  V         
23 1-5   -  -  -         
24 1-5   -  Y            
25 2-4   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZULJL AKAPS TAVSL IUKFI ZZLUR J
-------------------------------
