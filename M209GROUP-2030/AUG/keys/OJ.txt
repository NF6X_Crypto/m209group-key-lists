EFFECTIVE PERIOD:
27-AUG-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   -  B  B  -  -  -
03 2-0   -  C  C  -  C  -
04 2-0   -  D  -  D  -  -
05 2-0   -  -  -  -  E  E
06 2-0   -  -  F  -  -  F
07 2-0   -  G  -  -  -  -
08 2-0   H  H  H  -  -  -
09 0-3   I  I  -  I  I  I
10 0-3   J  -  J  J  J  J
11 0-3   K  K  K  K  -  K
12 0-3   L  L  L  -  -  L
13 0-4   -  M  M  -  M  -
14 0-4   -  -  -  -  -  N
15 0-4   O  -  O  O  O  O
16 0-4   P  P  -  P  -  P
17 0-5   -  Q  -  -  Q  Q
18 0-5   R  -  R  R  -   
19 0-5   -  -  S  -  -   
20 0-5   -  -  -  -      
21 0-6   -  U  -  U      
22 1-6   V  V  -         
23 2-4   W  -  -         
24 2-4   -  -            
25 3-4   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

CWPOI SMKMH PSAJJ IHARU MSWRQ A
-------------------------------
