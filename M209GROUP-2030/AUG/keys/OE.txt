EFFECTIVE PERIOD:
22-AUG-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  -  -  B  -  -
03 1-0   -  C  C  -  C  -
04 1-0   -  D  D  -  D  D
05 1-0   -  E  -  E  -  -
06 1-0   -  -  -  F  F  -
07 2-0   -  -  -  G  -  G
08 2-0   -  H  -  H  -  -
09 0-4   I  -  -  I  -  I
10 0-4   -  J  J  J  -  -
11 0-4   -  -  K  -  K  -
12 0-4   L  L  L  -  -  -
13 0-4   -  M  M  -  M  M
14 0-4   -  N  -  -  -  N
15 0-6   -  -  -  -  O  O
16 0-6   P  P  P  P  P  -
17 0-6   Q  Q  Q  -  Q  Q
18 1-4   R  R  R  -  -   
19 1-4   S  -  -  S  -   
20 1-4   -  -  -  T      
21 1-4   U  U  U  U      
22 1-5   V  -  V         
23 1-5   W  X  -         
24 2-5   -  -            
25 2-6   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LQUAI NZTPZ YDMUO RKKRR KFQKR A
-------------------------------
