EFFECTIVE PERIOD:
14-MAR-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 0-3   B  B  B  B  -  -
03 0-3   -  C  C  -  -  C
04 0-3   -  -  -  D  D  D
05 0-3   -  E  E  -  E  -
06 0-3   -  -  -  F  -  F
07 0-4   -  G  G  -  -  -
08 0-4   H  H  -  H  H  H
09 0-5   I  -  I  I  I  -
10 0-5   -  J  -  -  J  J
11 0-5   K  K  K  K  -  -
12 0-5   -  -  -  L  -  L
13 0-5   M  -  M  -  -  -
14 0-5   N  N  N  -  -  -
15 0-5   O  -  O  O  O  -
16 0-5   P  -  P  -  -  -
17 0-5   -  -  -  Q  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  -  -  S   
20 0-6   T  -  T  -      
21 0-6   U  -  -  U      
22 0-6   V  -  -         
23 0-6   -  -  -         
24 1-4   -  Y            
25 3-4   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HNJJE NXQNM LSAAQ KZOPD KBQQY T
-------------------------------
