EFFECTIVE PERIOD:
28-MAR-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  -  B  B  B  -
03 1-0   -  C  C  C  -  -
04 1-0   -  -  -  -  -  -
05 1-0   -  E  E  -  -  E
06 0-4   -  F  -  -  -  -
07 0-4   G  -  -  -  -  -
08 0-4   -  -  H  H  -  -
09 0-5   I  -  I  -  I  -
10 0-5   J  -  J  J  -  J
11 0-5   -  K  K  K  K  K
12 0-5   -  -  L  L  -  L
13 0-5   M  -  -  -  M  M
14 0-6   -  N  -  N  N  N
15 0-6   O  O  -  -  O  O
16 0-6   -  P  P  P  -  -
17 0-6   -  -  -  -  -  -
18 1-4   -  R  R  -  R   
19 1-6   S  S  -  -  -   
20 2-5   -  T  T  T      
21 3-5   U  -  -  U      
22 3-6   V  V  -         
23 4-5   W  X  -         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HVRAI HTOVL QQIAB AVNVU ARPGI T
-------------------------------
