EFFECTIVE PERIOD:
13-MAR-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   -  B  B  -  -  B
03 2-0   -  -  C  -  C  C
04 2-0   -  -  D  D  -  -
05 2-0   E  -  -  E  -  E
06 2-0   -  F  -  F  -  -
07 2-0   -  -  -  G  -  G
08 2-0   H  H  H  -  H  -
09 2-0   -  I  -  -  I  I
10 2-0   -  -  -  J  J  -
11 2-0   K  K  K  K  K  -
12 2-0   -  -  L  -  L  L
13 2-0   M  -  M  -  M  M
14 0-3   N  N  -  -  -  -
15 0-4   -  O  -  -  O  O
16 0-4   -  P  P  P  -  P
17 0-4   Q  Q  Q  -  Q  -
18 0-4   R  R  R  R  -   
19 0-4   -  -  S  -  -   
20 0-4   T  T  -  -      
21 0-4   -  U  -  -      
22 0-5   V  V  -         
23 0-5   -  X  X         
24 0-5   X  -            
25 0-6   -  -            
26 1-5   -               
27 1-6                   
-------------------------------
26 LETTER CHECK

QSQYD AOMPJ EQUJG MQEZZ EQGBY P
-------------------------------
