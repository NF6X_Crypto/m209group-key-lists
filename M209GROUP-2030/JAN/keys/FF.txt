EFFECTIVE PERIOD:
01-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 2-0   -  -  -  B  B  B
03 2-0   -  C  C  -  C  C
04 2-0   -  D  -  D  D  -
05 2-0   -  -  -  -  -  -
06 2-0   F  -  F  F  -  -
07 2-0   G  G  G  G  G  G
08 0-3   H  H  -  -  H  -
09 0-4   I  -  I  I  -  I
10 0-4   -  J  J  -  -  J
11 0-4   -  K  K  K  -  -
12 0-4   L  L  L  -  -  L
13 0-4   M  M  -  -  M  M
14 0-4   -  -  N  N  -  -
15 0-4   O  O  -  O  O  O
16 0-4   -  -  -  -  P  -
17 0-4   Q  Q  Q  -  -  -
18 0-4   R  -  -  R  R   
19 0-4   -  -  S  -  -   
20 0-4   -  T  -  T      
21 0-5   -  U  -  U      
22 0-6   V  -  V         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   -  Z            
26 1-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GTYDG SJLUN VJMKU THTNT JLVWZ N
-------------------------------
