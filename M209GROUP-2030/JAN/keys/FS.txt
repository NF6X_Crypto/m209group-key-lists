EFFECTIVE PERIOD:
14-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  B  -  -
03 1-0   C  -  C  -  C  -
04 2-0   D  D  D  D  -  D
05 2-0   -  -  E  -  -  -
06 2-0   -  -  -  -  F  -
07 2-0   -  G  G  G  -  -
08 2-0   -  -  H  -  -  H
09 2-0   -  I  -  -  -  I
10 2-0   J  -  -  J  J  -
11 0-4   K  -  K  -  -  -
12 0-4   L  L  -  -  L  -
13 0-4   M  -  M  M  M  M
14 0-4   -  N  -  N  -  -
15 0-6   O  O  O  O  O  O
16 0-6   -  P  P  -  P  P
17 0-6   Q  -  -  Q  Q  Q
18 0-6   -  R  -  -  R   
19 0-6   -  -  S  S  -   
20 1-2   T  T  -  -      
21 1-2   -  -  -  U      
22 1-2   -  V  V         
23 1-2   W  -  X         
24 1-6   X  -            
25 2-3   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RVMMS TLVOG MHGJV LAMFT JSAST V
-------------------------------
