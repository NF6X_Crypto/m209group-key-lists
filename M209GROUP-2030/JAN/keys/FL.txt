EFFECTIVE PERIOD:
07-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  -  B  -  -  B
03 1-0   C  C  C  -  -  C
04 1-0   -  -  D  -  D  -
05 1-0   E  -  -  E  E  -
06 1-0   F  F  -  -  -  F
07 0-3   -  -  G  -  -  G
08 0-3   -  H  -  -  -  -
09 0-3   I  I  I  I  I  I
10 0-3   J  -  J  J  -  J
11 0-3   -  -  -  K  -  -
12 0-4   L  L  -  -  -  -
13 0-5   -  -  -  M  -  -
14 0-5   -  N  N  N  N  -
15 0-5   O  O  O  O  -  -
16 0-5   P  P  -  P  P  P
17 0-5   Q  -  Q  -  Q  -
18 1-3   -  -  R  -  -   
19 1-5   -  -  -  -  S   
20 1-5   T  T  T  -      
21 1-5   U  -  -  U      
22 1-5   -  -  -         
23 2-5   -  X  -         
24 3-4   -  Y            
25 4-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UUOJY TGLOF LYZVZ MZDKU KOQUU N
-------------------------------
