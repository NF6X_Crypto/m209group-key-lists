EFFECTIVE PERIOD:
05-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  B  -  B  -
03 2-0   -  -  -  -  -  C
04 2-0   -  D  -  D  D  -
05 2-0   E  E  -  E  E  -
06 2-0   F  -  F  F  F  -
07 2-0   -  -  -  G  -  -
08 2-0   -  H  H  H  -  -
09 2-0   -  -  I  I  -  I
10 2-0   J  -  J  -  J  J
11 0-3   -  K  K  -  K  -
12 0-3   L  L  L  -  L  -
13 0-3   -  M  M  -  -  M
14 0-3   N  N  -  -  -  -
15 0-6   O  O  O  O  -  O
16 0-6   P  -  P  -  -  -
17 0-6   Q  Q  -  -  -  Q
18 0-6   -  R  -  -  -   
19 0-6   -  S  -  S  S   
20 1-3   T  T  -  T      
21 1-5   U  U  -  -      
22 2-4   V  -  -         
23 2-6   W  X  X         
24 2-6   X  Y            
25 2-6   Y  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

QIXMW QWOQA ZGEBM AOHTP IXVSA V
-------------------------------
