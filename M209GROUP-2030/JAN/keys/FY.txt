EFFECTIVE PERIOD:
20-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  A
02 2-0   -  B  -  B  -  B
03 2-0   -  C  C  -  C  C
04 2-0   -  -  D  D  D  -
05 2-0   E  -  -  -  -  -
06 2-0   F  F  -  F  -  -
07 2-0   -  -  G  G  -  G
08 0-4   H  -  H  -  -  -
09 0-4   I  I  -  -  I  -
10 0-4   -  -  -  J  J  -
11 0-4   -  -  -  K  -  K
12 0-5   -  L  L  L  -  -
13 0-5   -  -  M  M  M  -
14 0-5   -  N  -  N  -  N
15 0-5   O  -  -  -  -  O
16 0-5   P  P  -  P  P  P
17 0-5   -  Q  -  -  Q  -
18 1-5   R  R  R  -  R   
19 2-4   S  S  S  S  -   
20 2-5   T  T  T  -      
21 2-5   U  -  -  -      
22 2-5   -  V  V         
23 2-5   -  -  X         
24 3-4   X  Y            
25 3-5   -  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

HZMSW WTMEP SMTPF GWOGR TITMA N
-------------------------------
