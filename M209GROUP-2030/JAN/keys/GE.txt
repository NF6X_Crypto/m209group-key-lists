EFFECTIVE PERIOD:
26-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  -  -  -  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  -  D  -  D  D
05 1-0   -  E  -  E  -  -
06 1-0   F  F  -  -  F  F
07 2-0   G  -  -  G  G  G
08 2-0   -  H  -  -  -  -
09 2-0   I  I  I  I  I  -
10 2-0   -  J  J  -  -  -
11 2-0   K  K  K  -  K  K
12 0-3   -  -  -  -  L  -
13 0-3   -  -  -  M  -  -
14 0-3   N  -  N  N  -  N
15 0-3   -  -  O  O  O  -
16 0-3   P  P  P  P  P  -
17 0-3   -  Q  Q  Q  Q  Q
18 0-3   -  -  -  R  R   
19 0-3   S  -  S  -  -   
20 0-3   T  -  -  T      
21 0-3   U  U  U  -      
22 0-4   V  -  V         
23 0-4   -  X  -         
24 0-4   -  Y            
25 0-5   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VSSVL VNOOE XHKAC INKRX POFPV P
-------------------------------
