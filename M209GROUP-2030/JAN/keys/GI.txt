EFFECTIVE PERIOD:
30-JAN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  -  B  B  B  B
03 1-0   -  C  -  C  -  -
04 1-0   D  D  D  -  D  D
05 1-0   -  -  E  -  E  -
06 0-3   -  F  F  F  -  -
07 0-3   G  -  -  -  -  G
08 0-3   H  H  -  H  -  -
09 0-3   -  I  I  -  -  I
10 0-3   -  -  J  -  -  -
11 0-3   K  K  -  -  K  -
12 0-5   L  -  -  -  L  -
13 0-5   M  M  M  -  M  -
14 0-5   -  -  -  N  -  N
15 0-5   -  -  -  -  O  O
16 1-3   -  P  -  P  P  P
17 1-5   Q  -  Q  Q  Q  -
18 1-5   R  -  -  -  R   
19 1-5   S  S  S  S  -   
20 1-5   -  T  -  -      
21 2-6   U  -  -  U      
22 3-4   V  V  V         
23 4-5   W  -  -         
24 4-5   -  Y            
25 4-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LUJAO ATMVM NURKV KFJZU NMASO O
-------------------------------
