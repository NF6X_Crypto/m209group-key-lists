EFFECTIVE PERIOD:
19-JUL-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  A
02 2-0   B  B  -  -  B  B
03 2-0   -  -  C  -  C  C
04 2-0   -  -  D  -  -  -
05 2-0   -  -  E  -  -  -
06 2-0   F  F  -  F  F  F
07 0-3   -  G  G  -  G  G
08 0-3   -  H  H  -  -  -
09 0-3   I  -  I  I  I  -
10 0-3   J  J  -  J  -  -
11 0-3   -  K  -  K  -  K
12 0-3   L  -  L  -  -  -
13 0-4   M  -  M  -  M  -
14 0-4   -  -  -  N  -  -
15 0-4   -  O  -  O  O  O
16 0-4   -  P  P  -  P  -
17 0-4   Q  -  -  Q  -  Q
18 1-3   -  -  -  -  R   
19 1-5   S  S  -  -  -   
20 2-4   T  T  -  T      
21 2-5   -  -  U  U      
22 3-4   -  -  -         
23 3-4   W  X  X         
24 3-4   -  Y            
25 3-4   Y  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

ITSDU HDTTV DZVOU ZVZZL KUUOA A
-------------------------------
