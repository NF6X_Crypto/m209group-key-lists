EFFECTIVE PERIOD:
15-JUL-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  B  B  B  B
03 1-0   C  -  -  C  -  -
04 1-0   -  D  -  -  D  D
05 2-0   E  -  E  E  -  E
06 0-3   F  -  F  -  -  -
07 0-3   -  -  -  G  G  G
08 0-3   -  -  H  -  -  -
09 0-3   I  I  -  I  -  -
10 0-5   J  -  -  -  J  -
11 0-5   K  -  K  -  K  K
12 0-5   -  L  -  L  -  L
13 0-5   M  M  -  M  -  M
14 0-6   -  N  -  N  N  N
15 0-6   O  -  O  O  O  -
16 0-6   -  P  -  -  -  -
17 0-6   Q  -  Q  Q  Q  Q
18 1-3   -  -  -  -  R   
19 1-5   S  S  -  -  S   
20 1-5   T  T  -  T      
21 1-5   U  U  U  U      
22 1-5   -  -  V         
23 2-4   W  X  X         
24 2-5   -  Y            
25 2-5   -  Z            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

GWQYN RRZOS SAEPS OIVPS TPMSW V
-------------------------------
