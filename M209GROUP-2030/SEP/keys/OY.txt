EFFECTIVE PERIOD:
11-SEP-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  -  -  B  -  -
03 1-0   -  -  C  -  C  -
04 1-0   D  D  D  D  D  D
05 1-0   -  -  E  -  -  E
06 1-0   -  -  -  F  F  -
07 2-0   -  G  -  G  -  -
08 2-0   H  H  -  -  -  H
09 2-0   -  -  -  -  I  -
10 2-0   -  J  J  J  -  -
11 2-0   K  -  -  K  K  K
12 2-0   L  -  -  -  L  -
13 2-0   M  M  M  -  -  M
14 2-0   -  N  N  -  N  N
15 2-0   O  -  -  O  O  -
16 2-0   P  -  -  P  -  -
17 2-0   -  -  Q  -  -  Q
18 0-3   R  -  R  R  R   
19 0-3   -  S  -  -  S   
20 0-3   -  T  -  -      
21 0-3   U  -  -  -      
22 0-3   V  -  V         
23 0-6   W  X  X         
24 1-2   X  -            
25 1-3   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

PZKIS TKJZH AVHHA ABMUI MJOAM J
-------------------------------
