EFFECTIVE PERIOD:
30-SEP-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   -  B  B  -  -  -
03 1-0   C  C  -  C  C  C
04 1-0   D  D  -  D  -  D
05 1-0   -  -  -  E  -  -
06 2-0   -  -  F  -  F  -
07 2-0   -  G  G  -  G  -
08 2-0   -  H  H  -  H  -
09 2-0   I  I  I  I  I  I
10 2-0   -  -  J  J  -  -
11 0-5   -  -  -  -  K  K
12 0-5   -  L  L  -  -  L
13 0-5   M  -  -  M  -  -
14 0-5   N  N  -  N  N  N
15 0-5   -  -  O  O  O  -
16 1-2   P  P  -  P  -  -
17 1-5   -  -  Q  Q  -  -
18 1-5   R  -  -  -  -   
19 1-5   -  S  S  -  -   
20 1-5   T  T  -  T      
21 2-3   -  U  -  -      
22 3-5   V  V  V         
23 3-5   W  -  X         
24 3-6   X  -            
25 4-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NMTPQ JPTUG VKHZY POMVS DPUKV A
-------------------------------
