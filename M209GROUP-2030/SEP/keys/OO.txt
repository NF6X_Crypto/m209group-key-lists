EFFECTIVE PERIOD:
01-SEP-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   B  -  -  B  B  B
03 2-0   -  -  C  -  C  -
04 2-0   D  -  D  -  D  -
05 0-3   -  E  E  -  -  -
06 0-3   -  F  -  -  -  -
07 0-3   -  G  G  -  -  -
08 0-3   -  -  -  -  -  H
09 0-3   I  I  I  I  I  I
10 0-3   J  J  -  -  J  -
11 0-4   K  -  K  K  K  K
12 0-4   L  -  -  L  -  L
13 0-4   M  -  -  M  M  -
14 0-5   -  N  N  -  -  N
15 0-5   -  -  -  O  O  O
16 0-5   -  P  -  -  -  P
17 0-5   -  Q  -  -  Q  Q
18 0-5   R  R  R  R  R   
19 0-5   S  -  S  S  -   
20 0-5   T  T  -  T      
21 0-5   -  U  U  -      
22 0-5   -  -  V         
23 0-6   -  X  X         
24 1-2   X  Y            
25 2-4   Y  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

MPJQK ZTMXW OOJHK FVTRJ AHMOP K
-------------------------------
