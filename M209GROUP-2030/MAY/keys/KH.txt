EFFECTIVE PERIOD:
13-MAY-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  B  B  B  -  -
03 1-0   -  C  -  -  C  C
04 1-0   -  -  -  -  -  D
05 1-0   E  -  -  -  -  -
06 0-4   F  F  -  -  F  -
07 0-4   -  -  G  -  G  G
08 0-4   H  H  -  -  -  -
09 0-4   I  I  I  I  -  -
10 0-4   J  J  -  J  J  J
11 0-5   -  -  -  K  K  -
12 0-5   -  L  L  L  L  L
13 0-5   -  M  M  M  -  M
14 0-5   N  N  -  -  N  N
15 0-5   O  O  -  -  O  O
16 0-5   P  -  P  P  -  P
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   R  R  -  R  R   
19 0-6   S  -  S  -  S   
20 1-2   -  T  T  T      
21 1-4   -  -  -  -      
22 2-6   -  V  -         
23 3-5   -  -  -         
24 4-5   X  Y            
25 4-5   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZIVAL NOUTR UHBPM GJSAA RUPCQ Z
-------------------------------
