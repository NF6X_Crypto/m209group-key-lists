EFFECTIVE PERIOD:
02-MAY-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 2-0   -  -  B  -  -  B
03 2-0   C  C  C  -  C  C
04 2-0   -  D  D  D  -  D
05 2-0   -  -  -  E  E  E
06 2-0   F  F  -  F  -  -
07 2-0   -  -  G  G  -  -
08 0-4   -  -  -  -  H  -
09 0-4   -  -  -  I  -  I
10 0-4   J  J  -  J  J  -
11 0-4   -  -  K  K  -  -
12 0-4   -  L  L  L  L  L
13 0-5   -  M  -  -  M  M
14 0-5   N  N  -  N  N  N
15 0-5   O  -  -  O  -  -
16 1-3   -  P  -  -  P  -
17 1-5   Q  -  -  Q  Q  Q
18 1-6   R  R  -  -  -   
19 2-4   S  S  S  -  S   
20 2-5   T  -  T  T      
21 2-6   U  -  -  -      
22 4-5   V  V  -         
23 4-5   -  -  X         
24 4-5   X  -            
25 4-5   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KATOA MFZNF NFRVS SMQNS ALSZV P
-------------------------------
