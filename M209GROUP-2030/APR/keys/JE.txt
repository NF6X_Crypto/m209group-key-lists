EFFECTIVE PERIOD:
14-APR-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  -  -  -  B  -
03 1-0   -  C  -  C  C  -
04 1-0   -  D  -  -  -  -
05 1-0   E  -  E  -  -  E
06 0-3   -  F  -  F  -  F
07 0-3   G  -  G  -  -  -
08 0-4   H  H  -  H  H  -
09 0-4   -  -  I  -  -  -
10 0-4   J  -  -  J  J  J
11 0-4   K  -  K  -  K  K
12 0-4   L  L  L  -  L  L
13 0-4   -  M  -  -  -  M
14 0-4   N  N  -  -  -  N
15 0-5   O  -  O  O  -  -
16 0-5   P  -  -  -  -  -
17 0-5   Q  -  Q  -  Q  Q
18 1-4   -  R  -  R  -   
19 1-4   -  -  S  S  -   
20 1-4   -  -  T  T      
21 1-4   -  U  U  U      
22 1-5   -  -  -         
23 2-6   W  X  X         
24 3-5   -  Y            
25 3-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RQOGZ URXSV FUNAR NRRAI QNKOT J
-------------------------------
