EFFECTIVE PERIOD:
04-APR-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  B  B  B  B  -
03 1-0   -  -  C  C  C  -
04 1-0   -  -  D  D  -  D
05 1-0   E  E  -  -  -  E
06 2-0   F  F  F  F  -  F
07 2-0   -  G  G  -  -  G
08 0-3   H  -  H  -  -  H
09 0-3   -  -  -  I  -  I
10 0-3   -  -  -  J  J  -
11 0-3   -  K  -  -  -  -
12 0-3   L  L  L  -  L  -
13 0-3   -  -  -  -  -  M
14 0-3   N  -  -  -  -  -
15 0-3   O  -  -  O  O  O
16 0-4   P  -  P  -  P  P
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  R  R  -  -   
19 0-6   -  S  S  -  S   
20 1-2   -  T  -  -      
21 1-6   U  -  U  U      
22 2-4   V  V  V         
23 3-6   W  X  -         
24 3-6   X  Y            
25 3-6   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HZAQV YGWXK KVVPK YVRJI ZTRCG V
-------------------------------
