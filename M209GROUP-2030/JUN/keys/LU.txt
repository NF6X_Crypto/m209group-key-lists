EFFECTIVE PERIOD:
21-JUN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  A
02 2-0   B  -  B  -  -  -
03 2-0   -  -  C  -  C  -
04 2-0   D  -  D  D  D  D
05 0-3   E  -  -  -  -  E
06 0-3   -  -  F  F  -  -
07 0-3   G  G  G  G  -  G
08 0-3   -  H  -  H  H  -
09 0-3   -  I  I  -  I  I
10 0-3   J  J  J  J  J  J
11 0-3   K  K  K  K  -  K
12 0-3   -  -  L  L  -  L
13 0-4   -  M  -  M  -  M
14 0-5   -  N  -  N  N  N
15 0-5   -  -  -  O  -  -
16 0-5   P  P  -  -  -  -
17 0-5   Q  Q  -  -  Q  -
18 0-6   -  -  R  -  R   
19 0-6   S  -  S  S  S   
20 1-3   -  T  -  -      
21 2-5   -  -  -  -      
22 2-6   V  V  V         
23 3-6   -  -  -         
24 3-6   X  -            
25 3-6   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JORHM XHQMA QVOPC WWRWW WYBHQ F
-------------------------------
