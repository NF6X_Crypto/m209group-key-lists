EFFECTIVE PERIOD:
23-JUN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  B  B  -  -  B
03 2-0   C  C  C  C  -  -
04 2-0   -  D  -  D  D  D
05 2-0   -  -  -  -  -  E
06 2-0   -  -  F  F  F  -
07 0-3   G  -  -  G  G  G
08 0-5   H  -  H  -  H  H
09 0-5   -  I  I  -  -  -
10 0-5   -  J  -  J  J  -
11 0-5   K  K  -  K  K  -
12 0-5   L  L  -  -  L  -
13 0-5   M  M  -  -  -  M
14 0-5   -  -  N  N  -  N
15 0-5   O  -  -  -  -  O
16 0-5   -  P  P  P  -  P
17 0-5   Q  Q  -  -  -  -
18 0-6   -  R  R  -  R   
19 0-6   S  S  -  -  S   
20 0-6   -  T  T  T      
21 0-6   -  -  -  U      
22 0-6   -  -  V         
23 0-6   -  -  X         
24 0-6   -  -            
25 0-6   Y  -            
26 1-3   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

MHOPO LMSUE KYIYW MZOOW LOPII W
-------------------------------
