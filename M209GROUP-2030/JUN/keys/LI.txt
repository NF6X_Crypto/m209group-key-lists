EFFECTIVE PERIOD:
09-JUN-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  B  -  -  B  -
03 1-0   C  -  C  -  -  -
04 1-0   D  D  D  -  -  -
05 0-3   -  -  E  -  E  -
06 0-3   -  F  -  F  -  F
07 0-3   -  G  G  -  -  G
08 0-3   H  -  -  H  -  H
09 0-5   -  I  -  I  -  I
10 0-5   J  -  -  J  J  J
11 0-5   K  -  K  -  K  -
12 0-5   L  -  -  L  L  -
13 0-5   -  -  -  -  M  M
14 0-5   -  N  N  -  -  N
15 0-6   -  O  -  -  O  -
16 0-6   P  -  P  P  -  P
17 0-6   Q  -  -  Q  -  -
18 1-5   -  -  R  -  R   
19 1-6   -  -  -  S  S   
20 1-6   T  -  -  T      
21 1-6   -  U  U  U      
22 1-6   -  V  V         
23 2-3   W  -  X         
24 2-4   -  -            
25 2-6   -  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

XPORP ILXGN ZMWTU IPLQS AIUFD P
-------------------------------
