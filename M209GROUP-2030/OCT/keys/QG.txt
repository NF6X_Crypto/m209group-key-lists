EFFECTIVE PERIOD:
15-OCT-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 2-0   B  -  B  B  -  -
03 2-0   C  -  C  -  -  -
04 2-0   -  -  D  D  -  -
05 2-0   -  E  -  -  E  -
06 0-3   F  -  F  -  F  -
07 0-3   -  G  -  G  -  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  I  I  I  I
10 0-3   J  -  -  J  J  J
11 0-4   K  -  K  -  -  K
12 0-4   -  -  L  L  -  -
13 0-4   M  -  -  M  -  M
14 0-4   N  N  -  -  N  -
15 0-4   -  -  -  -  O  -
16 0-4   -  P  P  -  P  -
17 0-4   -  -  -  -  -  -
18 0-4   -  -  R  R  R   
19 0-4   S  S  S  S  -   
20 0-5   T  -  -  -      
21 0-6   U  U  -  U      
22 1-2   V  V  V         
23 1-6   W  X  -         
24 2-3   -  -            
25 3-4   Y  Z            
26 3-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

NFLDQ UUIWC TIVNB YQZYB UVADI J
-------------------------------
