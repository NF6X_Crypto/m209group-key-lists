EFFECTIVE PERIOD:
28-OCT-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  -  B  -  -  B
03 1-0   C  C  -  C  -  C
04 1-0   D  -  D  -  -  -
05 1-0   E  E  E  E  E  -
06 1-0   -  F  F  -  F  F
07 0-4   -  G  -  -  -  G
08 0-4   -  -  H  H  H  H
09 0-4   I  -  -  -  I  -
10 0-4   -  J  -  J  J  -
11 0-5   -  -  -  K  -  K
12 0-5   L  L  L  -  -  L
13 0-5   M  M  -  M  M  -
14 0-5   -  -  N  -  -  N
15 0-6   -  -  -  O  -  -
16 0-6   P  P  -  P  P  -
17 0-6   Q  -  -  Q  -  -
18 1-2   -  R  R  R  R   
19 1-3   S  -  -  -  S   
20 1-4   T  -  -  T      
21 1-4   -  U  U  -      
22 1-4   -  V  -         
23 1-4   -  X  X         
24 1-6   -  Y            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WWIWX IGUGH ASWAR TOMOR LKMXO S
-------------------------------
