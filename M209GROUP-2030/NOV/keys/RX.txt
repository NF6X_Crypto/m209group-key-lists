EFFECTIVE PERIOD:
27-NOV-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  -  B  B  B  -
03 1-0   C  C  C  -  C  -
04 1-0   D  -  D  -  -  D
05 1-0   E  E  -  E  -  -
06 1-0   -  F  -  -  -  F
07 1-0   -  -  -  G  G  G
08 2-0   H  -  H  -  H  H
09 2-0   I  I  I  I  I  -
10 2-0   -  -  -  J  J  -
11 2-0   -  -  -  -  K  -
12 2-0   -  L  -  -  L  L
13 2-0   M  -  M  M  -  M
14 2-0   -  -  N  N  -  N
15 2-0   -  O  -  O  -  -
16 2-0   -  P  P  P  -  -
17 2-0   Q  Q  -  -  Q  -
18 2-0   R  -  R  R  -   
19 2-0   S  -  -  -  S   
20 0-3   -  T  -  -      
21 0-3   -  -  -  -      
22 0-3   V  V  -         
23 0-3   W  X  -         
24 0-5   X  -            
25 0-6   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SVZJE OGUSJ AXFZJ VNGOV ABRMC Q
-------------------------------
