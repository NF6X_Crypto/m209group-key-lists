EFFECTIVE PERIOD:
20-NOV-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  -  B  B  -  B
03 1-0   C  C  C  -  C  C
04 1-0   D  -  D  D  -  D
05 2-0   E  -  -  -  -  -
06 0-3   -  F  F  F  F  F
07 0-4   G  G  G  G  -  G
08 0-5   -  H  -  -  -  -
09 0-5   I  -  I  -  -  I
10 0-5   J  -  J  -  -  J
11 0-5   K  K  -  K  K  K
12 0-6   -  -  L  -  -  -
13 0-6   -  -  -  M  M  -
14 0-6   -  N  -  -  N  -
15 0-6   O  O  -  O  O  O
16 0-6   -  P  P  P  -  P
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  -  R   
19 0-6   -  S  -  S  S   
20 0-6   T  -  T  T      
21 0-6   U  -  U  U      
22 0-6   -  V  -         
23 0-6   W  X  -         
24 1-5   X  -            
25 1-6   -  -            
26 2-4   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

QHOIW RIOZM UJPZH UQUII KASBD A
-------------------------------
