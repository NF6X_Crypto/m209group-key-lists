EFFECTIVE PERIOD:
03-NOV-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  A
02 2-0   -  -  -  B  B  -
03 2-0   C  C  -  C  -  C
04 0-3   -  -  -  D  D  -
05 0-3   E  E  E  -  -  -
06 0-3   -  F  -  F  -  F
07 0-3   G  G  G  -  -  G
08 0-3   H  H  -  H  H  -
09 0-3   -  I  -  -  -  I
10 0-4   -  -  J  J  -  J
11 0-4   K  K  -  K  K  -
12 0-4   L  -  L  L  -  L
13 0-4   -  M  -  -  M  M
14 0-4   -  -  -  -  N  -
15 0-4   -  O  -  -  -  -
16 0-4   P  -  P  -  P  -
17 0-4   Q  Q  Q  -  -  Q
18 0-4   -  R  R  R  R   
19 0-5   -  -  S  -  -   
20 0-5   -  -  -  -      
21 0-5   -  U  U  U      
22 0-5   V  -  -         
23 0-5   -  -  -         
24 0-5   X  -            
25 0-6   Y  Z            
26 1-6   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

EIRJL OLIJI SOLFO LUEUU OQALC P
-------------------------------
