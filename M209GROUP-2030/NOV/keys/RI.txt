EFFECTIVE PERIOD:
12-NOV-2030 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  -  B  -  -  -
03 1-0   -  -  C  -  C  C
04 1-0   D  D  D  -  -  D
05 1-0   -  -  -  -  E  E
06 2-0   -  -  F  F  -  F
07 0-4   -  G  -  -  G  G
08 0-4   -  -  -  H  H  -
09 0-4   I  I  -  -  I  -
10 0-4   J  -  -  J  J  J
11 0-4   K  K  K  K  -  -
12 0-4   -  L  -  L  L  L
13 0-6   -  -  -  -  M  M
14 0-6   N  -  N  -  -  -
15 0-6   O  -  O  -  -  -
16 0-6   P  -  -  -  -  P
17 0-6   -  Q  -  -  Q  -
18 1-2   -  -  R  R  R   
19 1-6   S  -  S  S  -   
20 2-3   T  T  T  -      
21 2-4   -  -  -  U      
22 3-4   -  -  V         
23 4-5   -  -  X         
24 4-6   X  Y            
25 4-6   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JMNGI VNJUV UOSLV CSESA UJNBS H
-------------------------------
