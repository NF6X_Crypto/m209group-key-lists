EFFECTIVE PERIOD:
10-NOV-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 2-0   B  B  -  -  B  -
03 2-0   C  C  C  -  -  -
04 2-0   -  D  -  D  -  -
05 2-0   E  -  E  E  E  -
06 2-0   -  -  F  F  -  -
07 2-0   -  -  G  G  G  G
08 0-3   H  H  -  H  -  H
09 0-3   I  I  I  -  I  I
10 0-3   -  J  J  -  J  -
11 0-3   K  -  -  -  K  K
12 0-4   -  -  -  L  L  L
13 0-4   M  M  M  M  M  M
14 0-4   -  N  N  N  -  -
15 0-4   -  O  -  -  O  O
16 1-3   P  -  P  -  -  P
17 1-3   -  Q  -  -  Q  -
18 1-3   R  -  -  -  -   
19 1-4   -  -  -  -  -   
20 1-6   T  -  T  T      
21 2-3   U  U  -  U      
22 2-3   V  V  -         
23 2-3   -  -  X         
24 2-3   X  Y            
25 2-4   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AIWAH HVSLT ZQAWZ USCWW QMTVN S
-------------------------------
