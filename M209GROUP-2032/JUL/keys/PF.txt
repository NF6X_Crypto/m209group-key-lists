EFFECTIVE PERIOD:
25-JUL-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  B  B  B  B  B
03 1-0   C  C  C  C  -  -
04 1-0   -  D  D  D  D  -
05 1-0   E  -  -  -  E  -
06 1-0   -  -  F  F  F  F
07 1-0   G  -  G  G  G  G
08 2-0   H  -  -  -  H  H
09 0-4   I  I  I  I  I  I
10 0-4   J  J  J  -  -  J
11 0-4   -  K  -  K  -  -
12 0-4   L  -  -  L  -  L
13 0-4   M  M  M  -  M  M
14 0-4   -  N  N  N  N  -
15 0-4   O  O  O  O  -  -
16 0-4   -  P  -  P  -  -
17 0-4   -  Q  -  -  -  Q
18 0-4   -  R  -  -  -   
19 0-5   -  -  -  S  -   
20 0-5   -  -  T  -      
21 0-6   -  -  -  -      
22 1-4   V  -  -         
23 1-4   -  -  X         
24 1-4   X  -            
25 1-5   Y  Z            
26 2-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

CTTOZ ZXZOO PRTQT ZFUNY ZFWYP N
-------------------------------
