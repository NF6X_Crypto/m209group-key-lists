EFFECTIVE PERIOD:
29-JUL-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  B  -  B  -  B
03 1-0   -  -  C  -  C  C
04 1-0   -  D  D  D  D  -
05 1-0   -  -  E  -  E  -
06 2-0   -  -  -  -  -  F
07 2-0   -  G  G  -  -  -
08 2-0   H  H  -  H  H  -
09 0-4   I  I  I  -  -  I
10 0-4   J  J  -  J  J  -
11 0-4   -  K  K  -  K  -
12 0-5   L  -  -  L  L  L
13 0-5   M  -  -  -  M  M
14 0-5   -  -  N  N  -  N
15 0-5   O  -  -  -  O  O
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  -  -  -  -
18 1-2   -  R  -  -  R   
19 1-3   S  S  -  S  -   
20 1-4   T  -  -  T      
21 1-4   U  U  -  -      
22 1-4   -  V  V         
23 1-4   -  -  -         
24 2-3   X  -            
25 2-5   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

KACXQ KTNMI RWVCH IKMAX AGVMZ U
-------------------------------
