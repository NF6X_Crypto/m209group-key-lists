EFFECTIVE PERIOD:
09-JUL-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  -  -  B  -  -
03 1-0   -  -  -  C  -  -
04 1-0   D  -  D  -  D  D
05 1-0   E  -  E  E  -  -
06 1-0   -  F  -  F  -  -
07 1-0   G  G  G  G  G  G
08 0-3   H  -  H  H  H  H
09 0-3   I  -  I  I  -  -
10 0-3   -  -  -  -  J  J
11 0-4   K  -  -  -  -  K
12 0-4   -  L  L  -  L  L
13 0-4   M  M  -  M  M  -
14 0-4   -  N  -  -  -  N
15 0-6   -  O  -  -  -  O
16 0-6   P  -  P  -  P  -
17 0-6   Q  -  -  Q  Q  -
18 1-2   -  -  R  R  -   
19 1-2   -  -  -  -  -   
20 1-6   -  -  T  -      
21 1-6   -  U  -  -      
22 1-6   V  -  -         
23 1-6   -  X  -         
24 2-3   -  Y            
25 2-5   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XREHR GSHDO QUZQA TTAVN IZDOL M
-------------------------------
