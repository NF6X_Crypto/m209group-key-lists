EFFECTIVE PERIOD:
13-JUL-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  A
02 2-0   -  -  B  -  -  -
03 2-0   -  -  -  -  -  C
04 0-3   -  -  D  -  D  -
05 0-3   E  E  E  E  -  -
06 0-3   -  -  F  F  -  -
07 0-3   G  G  -  G  G  -
08 0-3   -  H  H  H  -  H
09 0-3   I  I  I  -  I  -
10 0-3   J  J  -  J  J  J
11 0-5   -  -  K  -  K  K
12 0-5   -  -  -  -  L  -
13 0-5   -  -  -  -  M  -
14 0-5   N  N  -  -  -  -
15 0-5   O  O  O  O  O  O
16 1-2   -  P  P  P  -  -
17 1-5   Q  -  -  Q  -  Q
18 1-5   -  -  R  -  R   
19 1-6   S  -  S  S  S   
20 2-3   T  -  -  -      
21 3-5   U  U  -  -      
22 3-5   -  -  -         
23 3-5   W  -  X         
24 3-5   X  Y            
25 4-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TZMPS PTPDV LZZMW TOAZS ZDMDV A
-------------------------------
