EFFECTIVE PERIOD:
29-DEC-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  B  -  -  B
03 1-0   -  C  C  -  -  C
04 1-0   -  D  -  D  -  -
05 2-0   -  -  -  E  -  E
06 0-3   -  F  F  -  F  -
07 0-3   G  -  -  -  G  -
08 0-5   H  H  H  H  H  -
09 0-5   I  I  -  -  I  I
10 0-5   -  J  -  J  J  J
11 0-5   -  K  K  K  -  -
12 0-6   L  L  -  -  -  L
13 0-6   M  -  M  M  -  M
14 0-6   N  -  -  N  N  N
15 0-6   -  O  -  O  -  O
16 0-6   -  -  -  -  P  -
17 0-6   -  -  Q  Q  Q  -
18 0-6   -  -  R  R  R   
19 0-6   S  S  S  -  -   
20 1-3   -  T  -  T      
21 1-5   -  U  U  -      
22 2-3   V  -  V         
23 2-4   W  -  -         
24 5-6   X  Y            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CMRUW OQJUO FWJLL UTQUU RTKON T
-------------------------------
