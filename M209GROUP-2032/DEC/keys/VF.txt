EFFECTIVE PERIOD:
28-DEC-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   -  -  -  B  B  -
03 1-0   -  C  -  C  -  -
04 1-0   D  D  -  -  -  -
05 1-0   E  E  -  E  -  E
06 1-0   -  F  F  F  -  F
07 1-0   G  -  G  -  -  -
08 2-0   H  H  H  H  H  H
09 2-0   -  -  -  I  I  I
10 2-0   J  -  -  -  -  J
11 2-0   K  -  K  -  K  -
12 0-3   -  -  L  -  -  L
13 0-3   -  -  -  -  M  M
14 0-3   -  N  N  -  N  -
15 0-4   O  O  O  O  -  -
16 0-4   P  -  P  P  P  -
17 0-4   Q  Q  -  -  Q  -
18 0-4   R  R  R  R  -   
19 0-4   -  S  -  -  -   
20 0-4   -  T  -  -      
21 0-4   -  -  U  -      
22 0-4   -  V  V         
23 0-5   -  X  -         
24 1-2   -  -            
25 2-3   Y  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QOANL QZKJS MOANV STUPA OOZOT Q
-------------------------------
