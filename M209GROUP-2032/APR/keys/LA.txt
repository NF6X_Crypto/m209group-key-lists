EFFECTIVE PERIOD:
07-APR-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  B  B  -  B  B
03 1-0   -  -  C  C  C  -
04 1-0   -  -  -  D  -  -
05 1-0   E  E  -  E  -  -
06 2-0   F  -  F  -  -  -
07 0-3   -  -  G  G  -  G
08 0-3   H  H  H  -  H  -
09 0-3   -  I  -  -  I  -
10 0-3   -  -  J  J  J  -
11 0-5   -  K  -  -  -  K
12 0-5   L  L  L  L  -  L
13 0-5   -  -  M  -  -  -
14 0-5   -  -  -  -  -  N
15 0-5   O  O  -  O  O  O
16 0-5   P  P  -  -  -  P
17 0-5   Q  Q  Q  -  Q  -
18 0-6   -  -  -  -  -   
19 0-6   S  S  S  S  S   
20 0-6   -  -  -  -      
21 0-6   -  U  U  -      
22 1-3   V  -  V         
23 1-6   -  -  -         
24 3-4   -  Y            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OOBZI UIUUS QFMIZ WVUPG IOAAO O
-------------------------------
