EFFECTIVE PERIOD:
27-APR-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  -  B  -  B  B
03 2-0   -  C  C  -  -  C
04 2-0   -  D  -  -  -  D
05 2-0   E  -  E  E  -  -
06 2-0   -  F  -  -  F  -
07 2-0   -  G  -  -  -  G
08 2-0   H  -  H  H  H  -
09 0-5   I  I  I  I  I  I
10 0-5   -  J  J  -  J  J
11 0-5   -  -  -  K  K  K
12 0-5   L  -  L  -  -  -
13 0-6   M  -  M  -  M  M
14 0-6   N  N  -  N  N  N
15 0-6   -  -  -  -  -  -
16 1-3   P  P  P  -  P  P
17 1-3   -  -  -  Q  -  -
18 1-5   -  R  R  R  R   
19 1-5   -  -  -  S  -   
20 1-5   T  T  -  T      
21 1-5   U  U  U  -      
22 1-6   V  V  V         
23 1-6   W  -  -         
24 2-5   -  Y            
25 2-6   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

OQSNQ WWTJA VLLWL TWYQW VMPOR S
-------------------------------
