EFFECTIVE PERIOD:
17-AUG-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 1-0   B  B  -  B  B  B
03 1-0   C  -  -  -  C  C
04 1-0   -  -  D  D  D  -
05 1-0   -  -  -  -  -  -
06 1-0   -  F  F  F  F  -
07 2-0   G  -  -  -  -  G
08 2-0   -  -  H  -  -  H
09 2-0   -  I  I  -  I  -
10 2-0   -  -  J  -  -  J
11 2-0   K  K  -  K  K  K
12 0-3   -  -  L  -  -  -
13 0-3   M  M  -  M  -  -
14 0-3   -  -  N  -  -  -
15 0-3   O  O  -  -  O  -
16 0-3   P  -  P  P  -  -
17 0-5   -  -  -  -  Q  -
18 1-2   -  R  -  R  -   
19 1-5   S  -  S  -  -   
20 2-3   -  -  T  -      
21 2-3   U  U  U  U      
22 2-3   V  V  -         
23 2-3   W  -  -         
24 3-5   -  -            
25 3-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZZLUZ VYYBM ULRVB LPTOS ZZEYF Y
-------------------------------
