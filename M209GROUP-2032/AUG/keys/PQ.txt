EFFECTIVE PERIOD:
05-AUG-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   B  B  B  -  -  B
03 1-0   -  -  C  C  C  C
04 2-0   -  -  D  D  -  -
05 2-0   E  -  -  -  E  E
06 2-0   F  F  -  F  -  -
07 2-0   G  G  -  G  G  G
08 2-0   -  -  H  H  H  -
09 0-3   I  -  -  -  I  -
10 0-4   -  J  -  -  J  -
11 0-5   K  -  -  K  -  K
12 0-5   L  -  -  -  -  L
13 0-5   -  M  M  M  M  M
14 0-5   -  -  N  -  -  N
15 0-5   -  -  O  -  O  -
16 0-5   P  P  -  P  P  P
17 0-6   -  Q  -  -  -  -
18 0-6   -  R  -  -  -   
19 0-6   S  -  -  S  S   
20 0-6   T  T  T  T      
21 0-6   -  U  -  -      
22 0-6   -  V  -         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   -  Z            
26 1-2   -               
27 1-4                   
-------------------------------
26 LETTER CHECK

PVOZK OIZIP MPRJH LQZIO UFUML I
-------------------------------
