EFFECTIVE PERIOD:
02-AUG-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  A
02 0-3   B  B  -  -  -  B
03 0-3   C  -  C  C  C  -
04 0-3   -  -  D  D  D  -
05 0-3   -  E  E  E  E  -
06 0-4   -  -  F  F  F  -
07 0-4   -  G  -  -  -  -
08 0-4   H  H  H  -  -  H
09 0-4   -  I  I  I  I  -
10 0-4   J  J  -  -  -  -
11 0-4   K  K  K  -  K  K
12 0-5   L  L  L  -  L  -
13 0-5   M  -  M  M  -  M
14 0-5   -  N  -  N  -  N
15 0-5   -  -  O  O  -  -
16 0-5   -  -  -  -  P  -
17 0-5   Q  Q  -  Q  Q  Q
18 1-5   R  R  -  -  R   
19 2-3   -  -  -  -  S   
20 2-5   T  -  -  T      
21 2-6   -  U  U  U      
22 3-4   V  -  -         
23 4-5   W  -  -         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UQQET MPZNZ PWAYR IMTUP KTTUU U
-------------------------------
