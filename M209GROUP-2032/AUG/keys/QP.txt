EFFECTIVE PERIOD:
30-AUG-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   B  B  -  -  B  -
03 1-0   -  C  -  -  -  -
04 1-0   D  D  D  D  -  -
05 2-0   -  E  E  E  E  E
06 2-0   F  -  F  -  F  F
07 2-0   G  G  -  -  G  -
08 0-3   -  -  H  H  H  -
09 0-3   -  I  -  -  -  I
10 0-3   J  J  J  -  J  -
11 0-4   K  -  -  -  K  K
12 0-4   L  L  L  L  L  L
13 0-4   -  M  -  M  -  -
14 0-4   N  -  N  N  -  -
15 0-4   O  -  -  O  -  O
16 0-4   P  -  P  P  -  P
17 0-4   -  -  Q  -  Q  Q
18 1-2   R  R  R  -  R   
19 1-2   S  -  S  S  -   
20 1-2   -  T  -  -      
21 1-2   -  U  -  -      
22 1-4   -  -  -         
23 2-3   W  X  X         
24 2-5   -  -            
25 2-6   Y  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

XJVAG OTIKW OAXWT OQTCO WWWWS P
-------------------------------
