EFFECTIVE PERIOD:
29-AUG-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   -  -  -  -  -  B
03 2-0   C  C  -  -  -  C
04 2-0   D  D  D  -  -  D
05 2-0   E  -  E  E  -  E
06 0-3   F  -  -  F  F  -
07 0-3   -  -  -  G  G  G
08 0-3   -  H  -  H  -  -
09 0-3   -  -  -  I  I  I
10 0-3   J  J  J  -  J  J
11 0-3   -  K  -  -  -  -
12 0-3   L  L  L  -  -  -
13 0-3   -  -  M  -  M  -
14 0-3   N  N  -  N  -  N
15 0-3   -  O  -  -  -  O
16 0-4   -  P  -  P  P  -
17 0-4   Q  Q  Q  Q  Q  -
18 0-4   R  R  R  R  R   
19 0-5   S  -  S  -  S   
20 0-6   T  T  T  -      
21 0-6   -  -  -  U      
22 0-6   -  -  V         
23 0-6   W  -  -         
24 2-3   -  Y            
25 2-6   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

FMGZW VMIVL RGZAW KLFFK NUVBQ P
-------------------------------
