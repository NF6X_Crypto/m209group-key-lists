EFFECTIVE PERIOD:
05-JAN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  A  -  A
02 0-4   B  -  B  -  -  -
03 0-4   -  C  C  -  C  -
04 0-4   -  -  -  -  D  D
05 0-4   E  -  -  E  -  E
06 0-4   F  F  F  -  F  F
07 0-4   G  -  -  G  -  -
08 0-4   H  H  -  H  -  -
09 0-4   I  -  I  -  I  I
10 0-4   -  -  -  -  J  -
11 0-5   K  -  K  K  -  K
12 0-5   -  -  L  L  -  -
13 0-5   M  -  -  M  M  M
14 0-5   -  N  N  -  -  -
15 0-5   -  O  O  -  -  O
16 0-5   P  P  -  P  -  P
17 0-5   -  -  -  Q  -  -
18 0-5   -  -  R  -  R   
19 0-5   -  S  S  -  S   
20 0-5   -  -  -  T      
21 0-6   U  -  U  -      
22 0-6   V  V  V         
23 0-6   W  X  -         
24 0-6   -  -            
25 0-6   -  Z            
26 1-2   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

QMFIQ HKACI QQHUZ QUQCP TQFBQ U
-------------------------------
