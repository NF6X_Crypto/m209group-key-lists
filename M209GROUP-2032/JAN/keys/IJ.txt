EFFECTIVE PERIOD:
29-JAN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  -
02 2-0   -  -  B  B  B  -
03 0-3   C  -  -  C  C  C
04 0-4   -  -  -  -  -  D
05 0-5   -  E  E  -  E  -
06 0-5   -  F  F  -  F  -
07 0-5   G  G  -  G  G  G
08 0-5   H  -  -  -  H  H
09 0-5   I  -  I  -  -  I
10 0-5   J  -  -  J  -  -
11 0-5   -  -  -  -  -  K
12 0-5   -  L  -  L  L  L
13 0-5   M  M  M  -  -  M
14 0-5   -  -  -  -  N  -
15 0-6   O  -  -  O  O  O
16 0-6   P  -  -  -  -  P
17 0-6   Q  Q  Q  Q  -  -
18 0-6   -  R  -  R  R   
19 0-6   S  -  -  -  -   
20 0-6   T  T  T  T      
21 0-6   U  U  -  U      
22 1-4   -  V  V         
23 2-3   -  -  X         
24 2-6   X  -            
25 3-4   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UKZSM PRGQX YHLUW ADNOU CTYGZ O
-------------------------------
