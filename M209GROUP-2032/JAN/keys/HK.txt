EFFECTIVE PERIOD:
04-JAN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   B  -  -  B  -  B
03 1-0   -  C  -  C  -  -
04 1-0   D  -  -  -  D  -
05 2-0   E  -  E  -  E  E
06 2-0   F  -  -  -  F  -
07 2-0   G  -  G  -  G  -
08 0-3   H  H  -  -  H  -
09 0-3   -  -  I  I  I  I
10 0-3   -  J  -  -  -  J
11 0-3   -  -  -  K  -  -
12 0-4   L  -  L  -  L  -
13 0-5   M  M  M  -  -  M
14 0-6   -  N  N  -  N  -
15 0-6   O  -  O  O  O  O
16 0-6   -  P  -  -  -  P
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  S  S  -   
20 0-6   T  T  T  T      
21 0-6   -  -  U  -      
22 0-6   -  V  -         
23 0-6   -  X  X         
24 1-2   X  -            
25 1-3   -  -            
26 2-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

GKRGV WSVUH HWGFL RVHON EQVZV T
-------------------------------
