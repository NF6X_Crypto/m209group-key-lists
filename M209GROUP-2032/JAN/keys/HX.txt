EFFECTIVE PERIOD:
17-JAN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  C  -  -  -  C
04 0-3   D  D  D  -  D  D
05 0-4   -  E  -  -  -  E
06 0-4   -  -  -  F  -  -
07 0-4   G  -  -  -  G  -
08 0-4   -  H  H  -  H  H
09 0-4   -  I  -  I  I  -
10 0-5   J  J  -  J  -  J
11 0-5   -  -  K  K  -  -
12 0-6   L  -  L  L  -  -
13 0-6   M  -  -  M  M  M
14 0-6   -  N  N  -  -  N
15 0-6   O  -  O  -  -  -
16 0-6   -  P  -  -  -  P
17 0-6   Q  Q  -  Q  Q  -
18 0-6   R  R  R  -  R   
19 0-6   S  -  S  -  -   
20 1-4   -  -  -  T      
21 1-5   -  -  U  -      
22 2-6   -  -  -         
23 3-5   W  -  X         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LJRRL RDYDU RVFCZ EQPKS ALRGR Q
-------------------------------
