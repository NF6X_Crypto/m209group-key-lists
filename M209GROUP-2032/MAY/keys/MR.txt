EFFECTIVE PERIOD:
20-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  -  -  B  B  -
03 1-0   C  C  -  C  C  -
04 1-0   D  D  D  -  -  -
05 1-0   -  -  -  -  -  -
06 1-0   F  F  -  -  -  F
07 1-0   G  -  G  G  -  G
08 1-0   -  -  H  -  H  -
09 2-0   -  -  I  -  -  -
10 0-3   -  -  -  -  J  J
11 0-3   -  -  -  -  K  K
12 0-3   L  -  -  -  -  -
13 0-3   M  M  M  M  M  -
14 0-4   N  -  N  N  -  -
15 0-4   O  -  -  O  O  -
16 0-6   -  P  P  P  -  P
17 0-6   Q  -  -  Q  -  Q
18 0-6   R  R  R  R  R   
19 0-6   -  S  S  -  -   
20 0-6   T  T  T  T      
21 0-6   -  -  U  -      
22 1-6   V  V  V         
23 1-6   -  -  -         
24 2-4   X  Y            
25 2-5   -  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

QPSFU SWSQF OFIYM EYWGZ ADZAU T
-------------------------------
