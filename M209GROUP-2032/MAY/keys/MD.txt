EFFECTIVE PERIOD:
06-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   B  -  B  -  -  -
03 1-0   -  C  -  C  -  C
04 1-0   D  -  -  D  -  D
05 1-0   -  -  E  -  -  E
06 0-3   F  -  F  F  -  F
07 0-3   -  G  -  -  G  -
08 0-3   -  H  -  -  -  -
09 0-3   -  I  I  I  -  -
10 0-3   -  J  J  J  J  -
11 0-3   -  -  -  K  K  -
12 0-5   -  L  -  -  L  L
13 0-5   M  M  -  -  -  M
14 0-5   -  -  -  N  N  N
15 0-5   O  O  O  O  -  -
16 0-5   -  -  -  P  P  -
17 0-5   Q  Q  Q  -  Q  -
18 1-2   -  -  R  -  R   
19 1-3   S  S  -  -  S   
20 1-3   T  T  T  -      
21 1-3   U  U  U  U      
22 1-3   -  -  V         
23 1-5   -  -  X         
24 1-5   X  Y            
25 3-5   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KULUZ TUUTN NNNAA KLLVV CJBUU T
-------------------------------
