EFFECTIVE PERIOD:
09-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  -  -
02 0-3   -  B  -  B  B  -
03 0-3   -  C  -  -  C  C
04 0-3   D  -  -  D  D  D
05 0-4   E  -  E  E  E  E
06 0-4   F  F  F  F  F  -
07 0-4   G  G  -  G  -  -
08 0-4   H  H  -  H  H  H
09 0-4   I  -  I  -  -  I
10 0-4   -  -  -  J  -  J
11 0-4   -  -  K  -  -  K
12 0-6   -  L  L  -  L  -
13 0-6   -  M  -  M  -  M
14 0-6   -  N  -  N  -  -
15 0-6   O  -  O  -  O  -
16 0-6   -  -  -  -  P  -
17 0-6   -  Q  -  -  Q  -
18 1-2   R  -  R  -  R   
19 2-3   -  -  -  S  -   
20 3-4   T  T  T  -      
21 3-6   U  -  U  -      
22 3-6   -  -  -         
23 4-6   W  -  X         
24 4-6   X  Y            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

USOAJ CTIAA WNOOA AWOTN HSOCJ B
-------------------------------
