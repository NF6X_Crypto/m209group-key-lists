EFFECTIVE PERIOD:
28-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  B  -  B  -  B
03 1-0   -  -  -  -  C  C
04 1-0   D  -  D  -  D  D
05 1-0   -  -  E  -  -  -
06 2-0   F  F  F  -  F  F
07 2-0   -  -  G  -  G  -
08 2-0   -  H  H  -  -  -
09 2-0   -  I  -  I  -  -
10 2-0   -  -  -  J  -  -
11 2-0   K  -  -  -  -  K
12 0-4   L  -  L  L  L  L
13 0-4   -  -  M  -  -  M
14 0-4   N  N  N  N  N  -
15 0-4   O  O  -  O  O  -
16 0-5   -  P  -  -  -  P
17 0-5   -  -  Q  Q  Q  Q
18 0-5   R  R  -  R  -   
19 0-6   -  -  -  S  -   
20 1-4   T  -  -  -      
21 1-5   -  U  U  -      
22 2-3   V  -  V         
23 2-5   W  -  X         
24 2-5   -  Y            
25 2-5   -  Z            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QRTOL OXJCR VNMIO ARHRQ FOUHW X
-------------------------------
