EFFECTIVE PERIOD:
01-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  -  -  B  B
03 2-0   -  C  C  C  -  C
04 0-4   -  D  -  D  D  D
05 0-4   -  E  E  -  -  E
06 0-4   F  -  F  F  F  -
07 0-5   -  -  -  -  G  -
08 0-5   H  -  -  H  -  -
09 0-5   -  -  I  I  -  -
10 0-5   J  -  -  -  J  -
11 0-5   K  -  K  K  -  K
12 0-5   L  L  L  -  L  -
13 0-5   -  -  M  M  -  M
14 0-5   N  -  N  -  N  N
15 0-5   -  -  -  O  O  O
16 0-5   P  P  -  P  -  -
17 0-6   Q  -  -  -  -  -
18 0-6   R  -  R  R  R   
19 0-6   -  S  -  S  -   
20 0-6   T  T  T  -      
21 0-6   -  -  -  -      
22 0-6   V  V  V         
23 0-6   W  X  X         
24 1-2   -  -            
25 1-4   -  -            
26 2-3   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZFYVN HSNKS MYHMR CHVEV MZPFO R
-------------------------------
