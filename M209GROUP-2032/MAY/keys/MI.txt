EFFECTIVE PERIOD:
11-MAY-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  -  -  B  -  B
03 1-0   -  C  C  C  -  -
04 1-0   -  D  D  D  -  -
05 1-0   -  E  E  E  -  E
06 1-0   -  -  F  F  F  -
07 2-0   G  G  G  -  G  G
08 2-0   H  -  -  -  H  H
09 2-0   -  I  I  I  I  I
10 2-0   J  -  -  -  -  -
11 0-3   K  K  K  K  K  -
12 0-3   L  -  -  -  -  -
13 0-3   -  -  -  M  M  M
14 0-3   N  -  -  -  -  N
15 0-3   O  O  O  O  O  -
16 1-3   -  -  P  -  -  -
17 1-6   Q  -  -  -  Q  Q
18 2-3   -  -  R  -  R   
19 2-3   S  -  -  S  S   
20 2-3   T  T  -  T      
21 2-3   U  U  U  -      
22 2-4   -  -  -         
23 2-5   W  -  X         
24 2-6   X  Y            
25 2-6   -  Z            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MSPMQ VOTMS SMKUU ULUSO VUJFS A
-------------------------------
