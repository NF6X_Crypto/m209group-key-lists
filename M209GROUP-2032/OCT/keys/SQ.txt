EFFECTIVE PERIOD:
22-OCT-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  -
02 2-0   B  -  B  -  B  -
03 2-0   C  -  C  -  -  C
04 2-0   -  D  D  -  D  D
05 2-0   -  -  -  -  E  -
06 2-0   -  F  -  F  F  -
07 2-0   G  G  G  G  G  -
08 0-3   -  H  -  -  -  H
09 0-3   I  -  I  I  -  I
10 0-3   -  J  -  -  -  -
11 0-3   K  K  K  K  K  K
12 0-3   L  -  L  -  -  L
13 0-5   -  -  -  M  -  M
14 0-5   N  N  N  -  N  N
15 0-5   -  O  -  O  O  -
16 0-5   P  -  P  P  -  P
17 0-5   Q  -  -  -  Q  Q
18 1-3   R  -  -  R  -   
19 2-5   S  S  S  -  -   
20 2-6   -  -  T  T      
21 3-4   -  U  U  -      
22 3-5   V  V  -         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-5   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VDTOI TVVAT NRRRU DUMAE VANNU U
-------------------------------
