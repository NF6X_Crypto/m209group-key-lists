EFFECTIVE PERIOD:
21-OCT-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  -  -  B  B  B
03 1-0   -  C  C  C  -  -
04 1-0   D  D  -  -  -  -
05 0-3   -  E  -  E  -  -
06 0-3   -  F  F  -  F  F
07 0-3   -  G  -  G  G  -
08 0-3   -  -  -  H  H  H
09 0-3   I  I  -  -  I  -
10 0-4   -  -  -  -  J  J
11 0-4   K  -  K  K  -  K
12 0-4   L  -  L  -  -  L
13 0-4   M  -  M  M  M  M
14 0-4   N  -  N  N  -  -
15 0-4   O  -  -  O  O  -
16 0-4   -  P  P  -  P  -
17 0-6   Q  Q  Q  -  -  Q
18 0-6   R  -  R  -  -   
19 0-6   S  -  S  -  -   
20 1-3   T  T  T  -      
21 1-5   -  U  -  U      
22 2-4   -  -  -         
23 3-6   W  -  X         
24 4-6   X  -            
25 4-6   Y  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

VVALL HONVL RLNMO TQJHV HXXAN S
-------------------------------
