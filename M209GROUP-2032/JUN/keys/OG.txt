EFFECTIVE PERIOD:
30-JUN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 2-0   -  -  B  -  B  B
03 2-0   -  C  C  C  C  -
04 0-3   D  -  D  -  -  -
05 0-3   -  E  -  E  -  -
06 0-3   -  -  -  F  F  F
07 0-3   G  G  -  G  -  G
08 0-3   -  -  H  H  -  -
09 0-3   I  -  -  -  I  I
10 0-3   -  J  J  -  -  -
11 0-3   -  K  K  K  K  K
12 0-4   -  -  -  L  -  L
13 0-4   -  -  -  M  -  M
14 0-4   N  -  -  -  -  -
15 0-4   -  O  O  -  O  -
16 0-4   P  -  -  -  P  -
17 0-4   -  Q  -  Q  Q  -
18 0-4   -  -  R  -  R   
19 0-5   S  -  -  -  -   
20 1-2   -  -  T  -      
21 1-5   -  U  U  U      
22 2-4   -  -  -         
23 3-4   W  X  X         
24 3-4   X  Y            
25 3-4   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

RMFCW BTORT TZMAN QNSCW YPBBP S
-------------------------------
