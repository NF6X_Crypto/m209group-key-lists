EFFECTIVE PERIOD:
16-JUN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  -  -  -  A
02 0-3   -  -  -  -  -  -
03 0-3   C  -  -  -  -  C
04 0-3   -  -  -  -  D  D
05 0-3   E  -  E  -  -  -
06 0-3   -  -  F  -  F  -
07 0-4   -  G  G  G  G  G
08 0-4   H  -  -  -  -  -
09 0-4   -  I  -  I  I  -
10 0-6   J  J  -  -  J  J
11 0-6   K  -  -  K  -  -
12 0-6   -  L  L  L  -  -
13 0-6   -  -  M  M  -  M
14 0-6   N  N  N  N  -  -
15 0-6   -  O  -  O  O  -
16 1-3   -  -  -  P  -  P
17 1-4   Q  Q  Q  -  Q  -
18 1-4   -  R  R  -  R   
19 1-4   S  S  -  -  -   
20 1-4   T  -  -  T      
21 1-5   U  -  -  U      
22 2-4   -  V  V         
23 3-6   W  X  X         
24 4-6   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TNSUN QAUUW LGZQI USMFS NUATU B
-------------------------------
