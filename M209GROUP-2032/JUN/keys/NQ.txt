EFFECTIVE PERIOD:
14-JUN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  -  -  -  -  B
03 1-0   C  -  C  C  -  C
04 1-0   D  -  -  -  D  D
05 1-0   -  E  E  -  -  E
06 2-0   F  F  -  F  F  F
07 2-0   G  -  G  G  -  -
08 2-0   -  -  -  -  -  -
09 2-0   -  I  I  I  -  -
10 2-0   -  -  J  -  -  J
11 0-5   K  K  K  -  K  -
12 0-5   L  L  -  L  L  -
13 0-5   -  -  M  M  M  -
14 0-5   -  N  N  N  -  -
15 0-6   -  O  -  -  O  -
16 1-2   -  -  -  P  -  -
17 1-5   Q  Q  -  Q  -  Q
18 1-5   R  -  -  -  R   
19 1-5   S  -  -  S  S   
20 1-5   T  T  T  -      
21 2-6   -  U  -  -      
22 3-4   V  V  V         
23 3-5   -  X  X         
24 3-6   -  Y            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

USPSK GASNW WVHRM HWKNU NQZHH V
-------------------------------
