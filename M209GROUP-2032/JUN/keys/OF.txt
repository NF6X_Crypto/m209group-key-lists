EFFECTIVE PERIOD:
29-JUN-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  -  B  -  B  -
03 1-0   -  C  -  C  C  -
04 1-0   -  D  -  -  -  D
05 2-0   E  E  E  E  E  E
06 2-0   F  -  F  -  -  F
07 2-0   G  -  -  G  G  -
08 0-3   -  H  H  H  -  H
09 0-3   -  I  I  -  -  I
10 0-3   J  -  -  J  J  -
11 0-3   K  K  K  K  -  -
12 0-6   L  -  -  -  L  -
13 0-6   M  -  -  -  M  M
14 0-6   N  -  N  N  -  N
15 0-6   -  O  O  O  O  O
16 0-6   P  P  -  P  -  -
17 0-6   -  Q  -  -  Q  -
18 1-3   -  -  -  R  -   
19 1-6   -  -  -  -  S   
20 1-6   T  T  -  T      
21 1-6   -  U  U  -      
22 1-6   -  -  -         
23 2-3   -  -  -         
24 2-5   X  -            
25 4-5   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UMRNQ HETMM WASFW AJRKR XVMOX E
-------------------------------
