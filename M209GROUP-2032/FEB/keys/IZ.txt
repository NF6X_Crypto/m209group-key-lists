EFFECTIVE PERIOD:
14-FEB-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  -
02 2-0   -  -  B  -  -  B
03 2-0   C  C  C  -  C  C
04 2-0   D  D  -  D  -  D
05 0-4   E  E  E  E  E  E
06 0-4   -  F  F  F  F  -
07 0-4   -  G  G  -  -  G
08 0-4   -  H  -  -  H  H
09 0-4   -  -  I  I  I  -
10 0-4   J  -  J  J  J  J
11 0-4   -  -  K  K  -  -
12 0-5   -  L  -  -  -  -
13 0-5   M  M  -  -  -  -
14 0-5   -  N  N  -  N  N
15 0-5   -  -  -  -  -  -
16 1-6   -  P  P  P  P  -
17 2-4   Q  -  -  Q  -  -
18 2-5   -  -  -  R  R   
19 2-5   S  -  -  -  -   
20 2-5   T  -  -  T      
21 2-5   U  U  -  U      
22 2-6   -  V  V         
23 3-5   W  -  X         
24 4-5   X  Y            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

AKMNV MSVOL OCUAW TTZVM CMJTU W
-------------------------------
