EFFECTIVE PERIOD:
21-MAR-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   B  B  -  B  -  B
03 1-0   C  C  -  -  C  -
04 1-0   -  -  D  -  D  D
05 2-0   E  E  -  -  E  E
06 0-5   F  -  -  F  -  F
07 0-5   G  -  G  G  -  -
08 0-5   -  H  -  H  H  H
09 0-5   I  I  I  -  I  I
10 0-5   J  -  J  -  J  -
11 0-6   -  -  K  K  K  -
12 0-6   -  L  L  L  L  L
13 0-6   M  M  M  -  -  M
14 0-6   -  -  -  N  N  -
15 0-6   -  -  O  O  -  -
16 1-2   -  P  -  -  -  -
17 1-2   -  -  Q  Q  -  -
18 1-2   R  -  -  -  R   
19 1-3   S  -  -  S  -   
20 1-4   -  T  -  T      
21 1-5   -  -  U  -      
22 1-5   V  V  -         
23 1-5   -  -  -         
24 1-5   -  Y            
25 2-3   -  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JNYNA FSHPF YYAPH ATSVA NLHPB Q
-------------------------------
