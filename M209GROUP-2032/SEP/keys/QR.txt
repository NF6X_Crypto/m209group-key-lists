EFFECTIVE PERIOD:
01-SEP-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 0-3   -  -  -  -  B  -
03 0-3   C  C  C  C  C  C
04 0-3   -  D  D  -  -  D
05 0-3   E  -  E  E  E  E
06 0-3   -  -  -  -  -  F
07 0-3   G  G  G  G  -  -
08 0-3   -  -  H  H  -  -
09 0-3   I  I  I  I  -  I
10 0-5   -  J  -  J  J  -
11 0-5   -  K  -  -  -  K
12 0-5   -  -  -  L  -  -
13 0-5   -  M  -  -  M  -
14 0-5   N  -  N  -  -  -
15 0-6   -  -  O  -  O  -
16 0-6   P  -  P  P  -  -
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  -  S  S   
20 1-3   T  -  T  -      
21 2-4   -  U  U  U      
22 3-6   V  V  V         
23 3-6   W  X  -         
24 3-6   -  Y            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

STOVC GVAPR RLYPO MOAOC IBGUZ O
-------------------------------
