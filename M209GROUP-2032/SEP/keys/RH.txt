EFFECTIVE PERIOD:
17-SEP-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   -  -  -  -  -  B
03 1-0   -  -  -  C  -  -
04 2-0   -  D  D  -  D  D
05 2-0   E  -  E  -  E  -
06 2-0   -  -  F  F  F  -
07 2-0   G  -  G  G  -  G
08 2-0   -  H  H  -  H  H
09 2-0   I  I  -  I  I  I
10 0-3   J  -  J  -  J  J
11 0-3   -  -  -  -  K  -
12 0-3   -  -  L  L  -  -
13 0-3   -  -  M  M  M  M
14 0-6   N  N  -  N  -  -
15 0-6   -  -  O  -  O  -
16 0-6   -  -  -  -  P  P
17 0-6   Q  -  -  -  -  -
18 1-2   R  R  -  -  -   
19 1-4   -  S  -  S  -   
20 1-6   -  T  -  -      
21 2-3   U  -  U  -      
22 3-6   -  V  -         
23 3-6   W  -  X         
24 3-6   X  Y            
25 3-6   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

CQOII VCUPW VCAPX UPQIZ HLQAU J
-------------------------------
