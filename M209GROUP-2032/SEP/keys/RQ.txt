EFFECTIVE PERIOD:
26-SEP-2032 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  -  C  -  -
04 1-0   -  -  -  D  D  D
05 1-0   E  E  -  E  -  -
06 1-0   F  F  -  -  -  -
07 1-0   -  -  G  -  G  G
08 1-0   H  -  -  H  -  H
09 1-0   I  -  -  -  I  -
10 2-0   -  J  -  -  J  -
11 0-3   K  K  K  -  -  -
12 0-3   -  -  L  L  L  -
13 0-4   M  M  -  M  M  M
14 0-5   -  N  N  -  N  N
15 0-5   O  O  -  -  O  -
16 0-5   -  -  P  -  P  P
17 0-5   Q  -  Q  Q  -  -
18 0-5   -  -  R  R  -   
19 0-5   -  S  S  -  S   
20 0-5   T  T  -  T      
21 0-6   -  U  U  -      
22 1-5   -  V  V         
23 1-5   -  X  -         
24 1-5   X  -            
25 2-6   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

QTPPZ HBRYL GLCQN NZKLW RZNYD S
-------------------------------
