EFFECTIVE PERIOD:
29-MAY-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  -  B  -  -  -
03 1-0   C  C  -  -  -  -
04 1-0   D  -  D  D  D  -
05 1-0   -  E  E  E  -  -
06 1-0   -  -  -  F  -  -
07 1-0   -  -  -  -  -  G
08 1-0   H  -  H  -  H  H
09 1-0   -  I  -  -  -  -
10 1-0   J  -  -  J  J  -
11 0-3   -  K  -  K  -  K
12 0-3   -  L  L  L  L  L
13 0-3   M  -  M  -  -  -
14 0-3   -  N  -  -  -  N
15 0-4   O  -  O  -  O  -
16 0-4   P  -  -  -  P  P
17 0-5   Q  Q  -  -  Q  -
18 0-5   -  -  R  R  R   
19 0-5   S  -  -  S  -   
20 0-5   -  -  T  -      
21 0-5   U  U  -  -      
22 0-5   -  V  -         
23 0-6   -  X  X         
24 1-5   X  Y            
25 2-4   -  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

SRASM WPBSI QNOUU IZNPL PGORO R
-------------------------------
