EFFECTIVE PERIOD:
04-AUG-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 2-0   -  B  -  B  B  -
03 2-0   C  -  -  C  C  C
04 2-0   -  D  -  -  -  D
05 2-0   E  E  E  -  E  -
06 2-0   -  F  F  -  F  -
07 0-3   G  -  -  G  -  G
08 0-3   -  -  H  H  -  -
09 0-3   I  -  -  -  -  I
10 0-3   -  -  -  -  -  -
11 0-3   -  K  K  K  K  -
12 0-3   -  L  -  L  -  L
13 0-3   -  -  M  -  -  M
14 0-3   -  -  N  -  N  -
15 0-3   O  -  -  O  O  -
16 0-4   P  P  P  -  -  P
17 0-4   Q  -  Q  -  Q  -
18 0-4   -  R  R  R  R   
19 0-4   S  -  -  S  S   
20 0-4   T  T  -  -      
21 0-4   -  -  U  U      
22 0-4   V  -  -         
23 0-4   W  X  X         
24 0-5   -  Y            
25 0-6   Y  -            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JIURJ QLNNS UHHEO ZNJDK QHRQD M
-------------------------------
