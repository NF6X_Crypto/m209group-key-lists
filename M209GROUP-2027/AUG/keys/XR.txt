EFFECTIVE PERIOD:
13-AUG-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  A  A  -
02 0-3   -  B  B  -  -  B
03 0-3   C  -  -  C  -  C
04 0-3   -  -  -  D  D  -
05 0-5   -  -  E  E  -  -
06 0-5   F  F  F  -  -  -
07 0-5   -  G  -  G  -  G
08 0-5   H  H  -  H  -  -
09 0-5   -  I  I  I  -  I
10 0-5   J  -  -  J  -  -
11 0-5   -  K  K  -  K  -
12 0-5   -  L  L  L  L  -
13 0-6   M  M  -  M  M  -
14 0-6   N  -  -  -  N  N
15 0-6   O  -  -  -  O  O
16 1-2   -  P  P  P  P  -
17 1-3   -  -  Q  -  -  Q
18 2-3   -  R  R  R  -   
19 2-3   S  S  -  -  S   
20 2-6   -  -  T  -      
21 3-4   U  U  -  -      
22 3-5   V  V  -         
23 3-5   -  -  -         
24 3-5   -  -            
25 3-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NQVAA CSQRI OXEIM RFEXQ AAQVC S
-------------------------------
