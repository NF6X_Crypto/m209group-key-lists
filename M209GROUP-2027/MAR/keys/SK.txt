EFFECTIVE PERIOD:
29-MAR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  A  -  A
02 0-3   B  -  -  -  B  B
03 0-3   C  C  C  -  C  -
04 0-3   -  -  -  -  -  -
05 0-3   E  -  -  E  E  -
06 0-4   -  F  F  -  -  -
07 0-4   -  -  -  G  G  G
08 0-4   H  -  -  H  H  H
09 0-4   I  -  -  I  I  I
10 0-5   J  -  J  -  J  -
11 0-5   -  K  K  K  -  -
12 0-6   -  -  L  L  -  L
13 0-6   M  -  M  -  M  -
14 0-6   -  N  N  -  -  -
15 0-6   O  -  -  -  O  O
16 0-6   -  -  P  P  P  -
17 0-6   Q  Q  -  Q  -  -
18 0-6   R  -  R  -  R   
19 0-6   S  S  -  S  -   
20 1-5   -  -  T  -      
21 2-6   U  U  -  -      
22 3-4   V  -  V         
23 3-6   -  X  -         
24 3-6   -  Y            
25 3-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JQLIA ASMSJ OANTQ AJINP RSNNS A
-------------------------------
