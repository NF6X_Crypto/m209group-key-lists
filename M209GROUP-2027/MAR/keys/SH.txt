EFFECTIVE PERIOD:
26-MAR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  B  -  -  B
03 1-0   C  -  -  C  -  C
04 1-0   D  D  -  D  -  -
05 1-0   E  E  E  -  E  -
06 2-0   F  -  -  -  F  F
07 2-0   -  G  -  G  -  G
08 2-0   -  H  -  -  H  H
09 2-0   -  -  -  I  -  I
10 2-0   J  J  J  J  J  -
11 0-3   -  K  -  -  K  -
12 0-3   L  L  -  L  L  L
13 0-3   -  -  M  M  -  M
14 0-3   N  -  N  N  -  -
15 0-6   O  -  O  -  -  O
16 0-6   -  -  -  P  P  -
17 0-6   Q  Q  Q  -  -  -
18 1-2   -  R  R  R  -   
19 1-6   -  S  -  -  S   
20 2-3   T  T  T  -      
21 2-3   -  -  U  U      
22 2-3   -  -  -         
23 2-3   -  -  X         
24 3-4   X  -            
25 3-5   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

LVPRA LKPMP QLNMA WGMRP RQXKV L
-------------------------------
