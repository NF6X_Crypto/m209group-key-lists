EFFECTIVE PERIOD:
31-MAR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  -  -  -  B  -
03 1-0   C  -  -  C  C  C
04 0-3   D  -  D  D  D  -
05 0-3   -  -  -  E  -  E
06 0-3   -  F  F  -  -  -
07 0-3   G  -  G  -  G  -
08 0-3   -  -  -  H  -  -
09 0-3   -  -  -  -  I  I
10 0-3   J  J  J  -  -  J
11 0-6   K  K  -  -  K  K
12 0-6   -  -  L  L  -  L
13 0-6   M  M  M  M  -  -
14 0-6   N  -  -  -  -  N
15 0-6   O  -  O  -  O  -
16 1-3   P  -  P  -  P  P
17 1-5   -  -  -  -  Q  -
18 1-6   R  R  R  R  -   
19 2-4   -  S  -  S  -   
20 3-6   -  T  -  T      
21 3-6   -  -  -  U      
22 3-6   V  V  V         
23 3-6   -  X  -         
24 4-5   -  -            
25 4-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TUQPQ PIORO UPCTI UXROL TTWJZ Q
-------------------------------
