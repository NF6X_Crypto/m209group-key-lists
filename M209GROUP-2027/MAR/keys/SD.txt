EFFECTIVE PERIOD:
22-MAR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   -  -  -  -  B  -
03 1-0   -  -  -  -  -  -
04 1-0   -  D  -  D  D  D
05 1-0   E  -  -  E  E  -
06 0-3   F  F  -  F  F  -
07 0-3   -  -  G  G  -  -
08 0-3   H  -  H  H  -  -
09 0-4   -  -  -  -  -  I
10 0-4   -  J  J  J  -  -
11 0-4   K  K  K  -  -  -
12 0-4   -  L  -  L  L  L
13 0-4   M  M  -  M  -  M
14 0-4   -  N  -  -  N  N
15 0-4   -  O  O  O  O  O
16 1-3   P  -  P  -  P  -
17 1-3   -  -  Q  Q  -  Q
18 1-3   R  -  -  R  R   
19 1-3   -  -  S  -  -   
20 1-4   T  T  -  -      
21 2-3   U  -  -  -      
22 2-3   V  -  V         
23 2-3   W  X  X         
24 2-3   -  -            
25 2-4   Y  -            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

XMAMT ALKNR KAJJN SALSM VXGXN V
-------------------------------
