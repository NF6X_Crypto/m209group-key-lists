EFFECTIVE PERIOD:
21-JUN-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   -  -  B  -  -  B
03 1-0   C  C  C  -  C  -
04 1-0   -  -  -  D  -  D
05 1-0   E  -  -  -  -  -
06 1-0   F  F  F  F  F  F
07 1-0   G  G  -  G  G  G
08 1-0   -  H  -  -  -  H
09 1-0   -  I  I  I  -  I
10 1-0   J  J  J  J  J  J
11 2-0   -  K  K  -  -  -
12 0-3   L  -  -  L  -  -
13 0-3   M  -  -  -  M  -
14 0-3   N  -  N  -  N  N
15 0-4   O  -  -  -  -  -
16 0-4   -  P  -  P  -  -
17 0-4   Q  -  -  Q  Q  Q
18 0-4   R  R  R  -  R   
19 0-4   S  -  S  -  S   
20 0-5   -  T  -  T      
21 0-5   U  -  -  -      
22 1-4   -  -  V         
23 1-4   W  X  -         
24 2-5   X  -            
25 2-6   -  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

ERPUL VEWVH JHMRM WOXOH AFLRW W
-------------------------------
