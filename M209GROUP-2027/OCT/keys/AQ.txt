EFFECTIVE PERIOD:
29-OCT-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 0-3   -  B  -  -  -  -
03 0-3   -  -  -  C  -  C
04 0-3   -  D  -  -  D  D
05 0-3   -  -  -  E  -  E
06 0-3   F  -  -  -  -  -
07 0-3   G  -  G  -  G  -
08 0-3   H  H  -  -  H  -
09 0-3   I  I  -  I  -  I
10 0-3   -  J  J  J  -  -
11 0-4   K  -  -  K  -  K
12 0-4   -  -  -  -  L  L
13 0-4   M  -  M  M  -  -
14 0-4   -  N  -  -  N  -
15 0-4   -  -  O  -  -  -
16 0-4   -  -  P  -  P  P
17 0-4   -  Q  Q  Q  -  Q
18 0-5   R  -  R  -  R   
19 0-5   -  -  S  S  S   
20 0-5   -  -  T  -      
21 0-5   -  U  -  -      
22 0-5   V  -  -         
23 0-5   W  X  X         
24 2-6   X  Y            
25 3-4   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QLTJY LAIJU MQAII IKLLA AQLJK Y
-------------------------------
