EFFECTIVE PERIOD:
21-OCT-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  B  B  B  B  B
03 0-3   C  C  -  C  C  C
04 0-4   -  -  D  D  -  D
05 0-4   -  -  E  E  E  -
06 0-4   F  -  -  F  -  -
07 0-4   -  G  G  G  -  -
08 0-4   H  H  -  -  H  H
09 0-4   I  -  -  -  -  I
10 0-4   J  -  J  J  -  -
11 0-4   -  K  -  -  K  K
12 0-5   L  L  -  -  L  L
13 0-5   M  M  M  M  M  -
14 0-5   N  -  N  -  -  N
15 0-5   -  -  -  O  -  O
16 0-6   P  P  -  P  -  -
17 0-6   Q  Q  -  -  -  Q
18 0-6   -  -  R  -  -   
19 0-6   S  S  S  -  S   
20 0-6   -  T  -  T      
21 0-6   -  U  U  U      
22 1-2   -  V  V         
23 2-6   W  -  X         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZRNUK DJUXR ZOTPT WCJZK QUEWM H
-------------------------------
