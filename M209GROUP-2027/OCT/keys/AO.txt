EFFECTIVE PERIOD:
27-OCT-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  -  B  -  B
03 1-0   -  C  -  C  C  -
04 1-0   -  -  D  D  -  D
05 1-0   -  E  -  -  E  -
06 2-0   -  -  F  -  F  -
07 2-0   G  G  -  G  -  G
08 2-0   H  -  H  -  -  -
09 2-0   -  -  -  -  -  I
10 2-0   -  J  J  -  -  -
11 0-3   -  -  -  K  K  -
12 0-4   -  -  -  L  L  -
13 0-4   -  -  M  -  M  -
14 0-4   -  -  -  -  -  N
15 0-4   O  O  O  O  O  O
16 0-4   P  P  P  -  P  -
17 0-4   -  Q  Q  -  Q  Q
18 0-4   -  R  R  R  R   
19 0-5   S  S  -  -  -   
20 1-2   T  -  T  T      
21 1-2   -  U  -  U      
22 1-2   V  V  -         
23 1-2   W  X  X         
24 1-6   -  Y            
25 2-4   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SNTTT KTXUZ UIZOZ UZRML UADLD T
-------------------------------
