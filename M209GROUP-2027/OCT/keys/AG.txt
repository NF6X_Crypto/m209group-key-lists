EFFECTIVE PERIOD:
19-OCT-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  -  -  B  -  -
03 1-0   C  -  C  C  C  -
04 1-0   -  D  -  D  -  D
05 2-0   E  E  -  -  E  -
06 2-0   -  F  -  -  F  F
07 2-0   G  -  G  G  G  -
08 2-0   -  H  -  -  -  H
09 2-0   I  I  -  -  -  -
10 0-3   J  J  -  J  -  -
11 0-3   K  -  -  -  K  -
12 0-6   -  -  L  L  -  L
13 0-6   -  -  M  -  -  M
14 0-6   -  -  N  N  N  N
15 0-6   O  O  O  -  O  O
16 0-6   -  -  P  -  -  -
17 0-6   Q  Q  -  Q  -  -
18 1-2   -  R  -  R  R   
19 1-2   S  -  S  S  -   
20 1-2   T  T  -  -      
21 1-2   -  -  U  U      
22 1-3   -  V  V         
23 1-3   -  X  -         
24 1-5   X  Y            
25 2-6   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

PVWXT PBVUJ YLNKR RPKSX YVGFM U
-------------------------------
