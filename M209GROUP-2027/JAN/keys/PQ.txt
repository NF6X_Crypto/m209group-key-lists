EFFECTIVE PERIOD:
16-JAN-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   B  -  B  B  B  B
03 2-0   -  C  -  -  -  C
04 2-0   D  D  -  D  -  -
05 2-0   -  -  E  E  E  -
06 2-0   F  F  F  -  F  F
07 2-0   G  G  G  G  G  -
08 2-0   H  -  H  H  H  -
09 2-0   -  I  I  -  I  -
10 2-0   J  J  -  -  J  J
11 0-3   -  K  -  K  -  -
12 0-5   -  -  L  L  L  -
13 0-5   -  -  -  -  -  M
14 0-5   N  N  -  N  -  -
15 0-5   -  -  -  O  -  -
16 0-6   -  P  P  -  -  P
17 0-6   -  -  -  Q  -  -
18 0-6   R  R  -  -  -   
19 0-6   S  S  -  -  S   
20 1-3   -  T  -  T      
21 1-5   -  -  -  -      
22 2-4   V  -  -         
23 2-6   W  X  X         
24 2-6   X  -            
25 2-6   Y  -            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FMOQM RDRZS CERSW IAWEW QOJAV L
-------------------------------
