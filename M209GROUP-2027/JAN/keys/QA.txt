EFFECTIVE PERIOD:
26-JAN-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  A  A
02 2-0   -  B  B  -  B  -
03 2-0   C  -  -  C  C  -
04 2-0   -  D  -  D  D  D
05 2-0   E  E  E  -  -  -
06 2-0   F  F  F  -  F  F
07 0-3   G  G  -  -  -  -
08 0-4   H  H  -  H  -  H
09 0-4   I  -  -  I  I  -
10 0-4   -  J  J  J  J  -
11 0-4   -  K  K  -  -  K
12 0-6   L  -  -  -  -  L
13 0-6   M  -  M  M  -  M
14 0-6   -  N  -  -  N  N
15 0-6   O  O  -  -  O  -
16 1-3   -  P  P  P  -  -
17 1-4   Q  -  -  Q  Q  Q
18 1-5   R  R  -  -  -   
19 2-4   -  S  -  -  -   
20 2-4   -  T  -  T      
21 2-4   -  -  U  -      
22 2-4   -  -  V         
23 2-6   -  -  X         
24 3-4   -  -            
25 3-4   Y  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

XSMYS LFTGP IWXCU SUZZM UTHIY W
-------------------------------
