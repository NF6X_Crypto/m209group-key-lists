EFFECTIVE PERIOD:
10-APR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  -
02 2-0   B  -  B  B  B  -
03 2-0   C  C  C  C  -  C
04 2-0   D  D  D  -  D  D
05 2-0   -  -  E  E  E  E
06 2-0   -  F  -  -  F  F
07 2-0   -  G  G  G  -  -
08 2-0   H  H  -  -  -  H
09 0-3   I  I  -  I  I  I
10 0-3   -  -  J  -  J  -
11 0-3   K  -  K  -  -  K
12 0-3   L  L  -  -  L  -
13 0-3   -  -  -  -  -  M
14 0-3   N  -  -  -  -  -
15 0-3   O  O  -  O  -  O
16 0-3   -  -  P  -  -  -
17 0-3   Q  -  Q  -  -  -
18 0-3   -  -  R  -  R   
19 0-4   -  S  -  S  S   
20 0-6   -  T  -  T      
21 0-6   U  -  U  -      
22 0-6   -  V  -         
23 0-6   -  X  -         
24 1-5   X  -            
25 2-3   -  -            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PARKY KCWKH RSRWZ GVQKZ KGKAA V
-------------------------------
