EFFECTIVE PERIOD:
21-APR-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  A  -  A
02 0-3   B  B  B  B  B  B
03 0-3   -  -  C  -  C  C
04 0-3   -  -  -  D  -  D
05 0-4   E  E  -  -  E  E
06 0-4   F  -  -  -  F  F
07 0-4   -  -  G  G  G  -
08 0-4   H  H  -  -  -  H
09 0-4   -  -  -  I  I  -
10 0-4   J  -  J  -  J  -
11 0-4   K  K  K  -  K  K
12 0-4   -  -  -  -  -  -
13 0-4   -  -  M  -  M  -
14 0-4   N  N  N  -  -  -
15 0-4   -  O  O  -  -  -
16 0-5   P  -  -  P  P  -
17 0-6   Q  -  -  Q  -  -
18 0-6   -  R  -  -  R   
19 0-6   S  S  S  -  -   
20 0-6   -  -  T  T      
21 0-6   -  -  -  U      
22 0-6   -  -  V         
23 0-6   -  X  X         
24 1-2   -  Y            
25 2-3   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OHZNO FGTZA UOAAJ NHUCF HBFGZ W
-------------------------------
