EFFECTIVE PERIOD:
21-SEP-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 2-0   -  B  -  -  B  B
03 2-0   C  C  C  -  -  C
04 2-0   D  -  D  -  -  D
05 2-0   -  E  E  -  -  E
06 2-0   -  F  -  F  -  -
07 0-3   -  G  -  G  G  G
08 0-3   -  -  H  -  H  H
09 0-3   -  I  -  I  -  I
10 0-3   J  -  J  -  J  J
11 0-4   K  K  K  K  K  -
12 0-4   -  -  L  L  -  -
13 0-4   -  M  M  -  M  -
14 0-4   N  -  N  -  -  N
15 0-4   -  O  -  O  -  -
16 0-4   -  P  -  P  -  -
17 0-4   -  Q  Q  Q  Q  -
18 0-5   R  R  -  -  R   
19 0-5   S  -  -  S  -   
20 1-5   -  -  -  T      
21 1-6   -  -  U  U      
22 2-3   V  V  V         
23 2-4   W  X  X         
24 2-4   X  -            
25 2-4   -  -            
26 2-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

ONTYK DQYYJ ACORV VKYDV VRFVQ V
-------------------------------
