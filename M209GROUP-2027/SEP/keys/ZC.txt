EFFECTIVE PERIOD:
19-SEP-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  B  -  -  B  B
03 1-0   C  C  -  -  C  -
04 1-0   D  D  D  D  D  -
05 1-0   -  E  E  E  -  E
06 1-0   F  -  F  -  F  F
07 1-0   -  -  G  G  G  G
08 2-0   H  H  H  H  -  -
09 2-0   -  -  -  I  -  -
10 2-0   -  -  J  -  -  J
11 2-0   -  -  -  -  K  K
12 2-0   L  -  -  -  -  L
13 0-4   -  -  -  M  M  -
14 0-4   -  N  N  N  -  -
15 0-5   -  O  -  -  O  -
16 0-6   P  P  P  P  -  P
17 0-6   -  -  Q  Q  -  Q
18 0-6   R  -  R  -  -   
19 0-6   S  -  S  S  S   
20 0-6   -  -  -  T      
21 0-6   -  U  -  -      
22 0-6   V  -  -         
23 0-6   W  -  -         
24 1-2   -  -            
25 2-4   Y  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZTINE IYVKK JYRAQ KUMYA CDVIR S
-------------------------------
