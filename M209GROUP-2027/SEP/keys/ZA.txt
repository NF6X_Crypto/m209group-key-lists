EFFECTIVE PERIOD:
17-SEP-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 2-0   -  -  B  -  B  B
03 2-0   C  -  C  C  -  C
04 2-0   D  -  -  D  -  D
05 2-0   -  E  -  -  -  -
06 2-0   -  -  F  F  F  F
07 0-3   -  -  -  G  -  -
08 0-4   H  H  -  H  -  H
09 0-4   -  -  -  I  I  -
10 0-4   J  J  J  -  -  -
11 0-4   -  -  K  K  K  -
12 0-4   -  L  -  -  -  L
13 0-4   -  -  -  -  M  -
14 0-5   N  N  -  -  N  N
15 0-6   O  -  O  O  O  O
16 0-6   P  P  -  P  -  -
17 0-6   -  -  Q  Q  Q  -
18 0-6   R  R  -  R  R   
19 0-6   -  S  S  S  -   
20 0-6   -  -  -  -      
21 0-6   U  -  -  -      
22 1-2   -  -  V         
23 1-5   -  X  X         
24 2-4   X  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

CTNZW OLJUU KQRDT QLJTK ULIJA U
-------------------------------
