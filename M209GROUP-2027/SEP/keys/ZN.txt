EFFECTIVE PERIOD:
30-SEP-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 0-3   B  B  B  -  -  -
03 0-3   -  C  -  -  -  C
04 0-3   -  -  D  -  -  D
05 0-3   E  -  -  -  E  -
06 0-3   F  -  -  F  F  -
07 0-3   G  -  -  G  G  -
08 0-4   H  H  H  H  -  -
09 0-4   -  I  I  I  I  -
10 0-5   -  J  -  -  -  J
11 0-5   K  -  K  -  -  K
12 0-5   -  -  -  L  L  -
13 0-5   -  M  M  M  -  M
14 0-6   N  -  N  -  N  -
15 0-6   O  -  -  O  -  O
16 0-6   P  -  -  -  P  P
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  S  S   
20 0-6   -  T  T  -      
21 0-6   -  U  -  -      
22 1-4   -  V  V         
23 3-5   W  -  X         
24 3-6   X  -            
25 3-6   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZAUJU XWSKV KKHPI DTVLR OTRAR D
-------------------------------
