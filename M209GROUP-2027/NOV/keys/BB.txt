EFFECTIVE PERIOD:
09-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  -  C  C
04 1-0   -  -  D  -  D  D
05 1-0   E  E  -  E  -  -
06 1-0   -  -  -  F  -  -
07 2-0   G  -  -  -  G  G
08 0-3   -  H  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-4   J  J  J  J  J  -
11 0-4   -  -  K  K  K  K
12 0-4   -  L  -  -  -  L
13 0-4   -  M  M  M  -  M
14 0-4   N  N  N  -  N  -
15 0-4   O  -  -  -  -  -
16 0-6   P  -  P  P  P  -
17 0-6   -  Q  Q  -  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   -  -  -  S  -   
20 1-4   -  -  T  T      
21 1-4   -  U  -  U      
22 1-4   V  -  -         
23 1-4   W  -  -         
24 1-6   X  -            
25 2-3   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZFKPE AAWWQ APZAQ MAZUU PKEKY S
-------------------------------
