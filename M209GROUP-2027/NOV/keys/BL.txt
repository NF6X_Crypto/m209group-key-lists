EFFECTIVE PERIOD:
19-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  -  -  -
03 1-0   C  C  -  -  -  C
04 1-0   -  -  D  -  D  D
05 0-3   -  E  E  E  E  E
06 0-3   F  F  F  F  -  F
07 0-3   G  -  G  G  G  G
08 0-3   H  -  -  -  -  -
09 0-4   -  I  I  -  I  I
10 0-4   -  -  -  -  -  -
11 0-4   K  -  -  K  -  -
12 0-4   -  L  -  L  L  -
13 0-6   -  M  -  -  M  -
14 0-6   N  -  N  -  -  N
15 0-6   O  O  -  -  -  O
16 0-6   -  -  P  -  P  P
17 0-6   Q  -  -  Q  Q  -
18 1-2   R  -  R  -  -   
19 1-6   -  S  S  S  -   
20 2-4   -  T  T  T      
21 2-4   U  U  -  -      
22 2-5   V  V  V         
23 3-4   -  -  -         
24 3-4   -  -            
25 3-4   -  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MGOGW WVSJQ TMHAP WRFOL RLKMM V
-------------------------------
