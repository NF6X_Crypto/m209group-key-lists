SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF NOV 2027

    01 NOV 2027  00:00-23:59 GMT:  USE KEY AT
    02 NOV 2027  00:00-23:59 GMT:  USE KEY AU
    03 NOV 2027  00:00-23:59 GMT:  USE KEY AV
    04 NOV 2027  00:00-23:59 GMT:  USE KEY AW
    05 NOV 2027  00:00-23:59 GMT:  USE KEY AX
    06 NOV 2027  00:00-23:59 GMT:  USE KEY AY
    07 NOV 2027  00:00-23:59 GMT:  USE KEY AZ
    08 NOV 2027  00:00-23:59 GMT:  USE KEY BA
    09 NOV 2027  00:00-23:59 GMT:  USE KEY BB
    10 NOV 2027  00:00-23:59 GMT:  USE KEY BC
    11 NOV 2027  00:00-23:59 GMT:  USE KEY BD
    12 NOV 2027  00:00-23:59 GMT:  USE KEY BE
    13 NOV 2027  00:00-23:59 GMT:  USE KEY BF
    14 NOV 2027  00:00-23:59 GMT:  USE KEY BG
    15 NOV 2027  00:00-23:59 GMT:  USE KEY BH
    16 NOV 2027  00:00-23:59 GMT:  USE KEY BI
    17 NOV 2027  00:00-23:59 GMT:  USE KEY BJ
    18 NOV 2027  00:00-23:59 GMT:  USE KEY BK
    19 NOV 2027  00:00-23:59 GMT:  USE KEY BL
    20 NOV 2027  00:00-23:59 GMT:  USE KEY BM
    21 NOV 2027  00:00-23:59 GMT:  USE KEY BN
    22 NOV 2027  00:00-23:59 GMT:  USE KEY BO
    23 NOV 2027  00:00-23:59 GMT:  USE KEY BP
    24 NOV 2027  00:00-23:59 GMT:  USE KEY BQ
    25 NOV 2027  00:00-23:59 GMT:  USE KEY BR
    26 NOV 2027  00:00-23:59 GMT:  USE KEY BS
    27 NOV 2027  00:00-23:59 GMT:  USE KEY BT
    28 NOV 2027  00:00-23:59 GMT:  USE KEY BU
    29 NOV 2027  00:00-23:59 GMT:  USE KEY BV
    30 NOV 2027  00:00-23:59 GMT:  USE KEY BW

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  -  -  B  B  B
03 1-0   C  C  -  -  C  -
04 1-0   -  -  D  -  D  -
05 1-0   -  -  -  E  -  E
06 1-0   F  -  F  F  F  -
07 1-0   -  -  G  G  G  G
08 2-0   H  H  H  H  -  H
09 2-0   I  I  I  I  -  -
10 2-0   J  J  J  J  J  -
11 2-0   -  K  -  -  K  -
12 0-3   -  -  -  -  L  -
13 0-3   M  M  -  M  -  -
14 0-3   -  -  N  -  -  N
15 0-4   -  O  O  O  -  -
16 0-4   P  P  -  P  P  -
17 0-4   Q  Q  Q  -  Q  Q
18 1-3   -  R  R  R  -   
19 1-3   S  S  S  S  -   
20 1-3   -  -  T  -      
21 1-3   -  -  U  -      
22 1-5   -  -  -         
23 1-5   W  -  -         
24 2-3   X  Y            
25 2-4   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AVJAQ PNRNU XDMAH MVPAX TJZAP G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  -  -  -  -  B
03 2-0   C  C  -  -  -  C
04 2-0   -  D  D  D  -  -
05 2-0   -  E  E  E  -  -
06 2-0   F  -  F  F  F  -
07 2-0   G  G  G  -  G  G
08 0-3   H  H  -  H  H  -
09 0-5   -  -  I  -  I  I
10 0-5   -  J  J  J  -  -
11 0-5   -  K  K  -  K  -
12 0-5   L  -  -  L  L  L
13 0-5   M  M  -  -  M  M
14 0-5   N  N  -  -  -  N
15 0-5   -  -  O  -  O  -
16 1-2   -  P  -  -  P  -
17 1-2   -  Q  -  Q  -  -
18 1-2   -  -  -  -  R   
19 1-2   S  S  S  S  -   
20 1-3   T  -  -  T      
21 1-3   U  -  -  -      
22 1-3   -  V  V         
23 1-4   -  X  X         
24 2-5   -  -            
25 3-4   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IXKTZ MLMXN PJCAY XVPAO YJSXS L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  B  -  B  -  -
03 1-0   C  C  C  -  C  C
04 1-0   D  D  -  -  -  D
05 2-0   E  E  -  E  E  E
06 2-0   -  F  F  F  -  -
07 2-0   -  -  -  G  G  G
08 2-0   -  -  -  H  -  H
09 0-3   -  I  -  -  I  -
10 0-3   J  -  J  -  J  -
11 0-3   -  K  K  K  -  K
12 0-3   L  -  L  -  L  L
13 0-3   M  M  -  -  -  -
14 0-5   -  -  -  -  N  N
15 0-5   O  O  O  -  O  O
16 0-5   P  P  P  P  P  -
17 0-5   Q  -  -  Q  Q  -
18 1-2   -  -  -  -  -   
19 1-5   -  -  -  S  -   
20 2-6   T  -  T  T      
21 3-5   U  U  U  -      
22 3-5   -  -  V         
23 3-5   -  -  X         
24 3-5   X  -            
25 3-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

VWPMR NRLQS RIZNV WPMDW SWNSQ W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  B  B  B  -  B
03 2-0   C  C  C  -  C  C
04 2-0   D  -  -  -  -  -
05 2-0   E  -  E  -  E  -
06 2-0   F  F  F  F  F  F
07 2-0   -  -  G  -  -  -
08 2-0   H  H  -  H  -  -
09 2-0   I  -  I  -  -  -
10 0-4   J  J  J  J  -  J
11 0-4   K  -  -  -  K  K
12 0-4   -  L  -  L  -  -
13 0-4   M  -  M  -  M  M
14 0-4   N  -  -  N  N  N
15 0-4   -  O  O  -  O  O
16 0-6   P  -  -  -  -  -
17 0-6   Q  -  -  -  -  -
18 0-6   -  -  R  R  -   
19 0-6   -  S  S  S  -   
20 1-3   -  T  -  T      
21 1-4   -  U  -  U      
22 2-5   V  -  V         
23 2-6   -  -  X         
24 2-6   -  -            
25 2-6   -  Z            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ULJJR UWPRJ WAUPK DAIWM ANUDM Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  B  -  -  -
03 1-0   C  -  -  -  C  -
04 1-0   -  D  -  D  -  D
05 2-0   E  -  -  E  E  -
06 2-0   F  -  -  -  -  -
07 2-0   G  -  -  -  -  G
08 2-0   H  H  -  -  H  H
09 2-0   I  I  I  I  I  -
10 2-0   -  -  -  -  -  -
11 0-4   -  K  K  -  -  K
12 0-4   -  -  -  L  L  -
13 0-4   M  M  M  M  M  -
14 0-4   -  N  -  N  N  N
15 0-5   -  -  O  O  -  O
16 1-2   P  P  P  -  P  P
17 1-5   -  Q  -  Q  -  Q
18 2-4   -  -  R  R  R   
19 2-4   -  -  S  -  -   
20 2-4   -  T  T  -      
21 2-4   -  U  U  -      
22 3-4   -  -  V         
23 3-4   W  X  -         
24 3-5   -  -            
25 3-6   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

UWWAP USJOF FTTSL JJSTT PWNAO P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  B  B  B  B  B
03 1-0   -  C  -  C  C  C
04 1-0   D  D  D  -  -  D
05 1-0   -  -  E  E  E  -
06 1-0   -  -  F  F  F  F
07 2-0   G  -  -  G  -  -
08 2-0   -  H  -  -  -  H
09 2-0   -  I  -  -  -  -
10 2-0   J  -  J  J  -  J
11 2-0   -  -  K  -  -  -
12 2-0   L  L  -  L  -  L
13 2-0   M  M  -  M  M  -
14 2-0   N  N  N  -  -  N
15 0-3   -  -  -  O  O  -
16 0-3   -  -  -  -  P  -
17 0-3   Q  -  Q  Q  -  -
18 0-3   -  -  R  R  -   
19 0-3   -  S  -  -  S   
20 0-4   -  -  T  T      
21 0-5   -  U  U  -      
22 0-6   -  -  -         
23 0-6   W  -  X         
24 1-2   X  Y            
25 1-3   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

HMAKS APQLZ UWNSZ HDHNP SARYP E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  -  B  B  B  -
03 1-0   -  -  -  C  -  -
04 1-0   -  -  -  -  D  D
05 1-0   -  E  E  -  -  -
06 1-0   -  F  F  -  -  -
07 0-4   -  -  G  -  G  G
08 0-4   H  H  H  -  -  -
09 0-4   I  -  I  I  I  -
10 0-4   -  J  -  -  -  -
11 0-4   -  -  K  K  K  -
12 0-5   L  L  -  L  L  -
13 0-5   -  M  -  M  -  -
14 0-5   N  -  -  -  -  N
15 0-6   O  O  O  O  O  O
16 0-6   P  P  -  P  P  P
17 0-6   -  -  -  Q  -  Q
18 1-3   R  R  -  -  R   
19 1-3   S  -  S  S  S   
20 1-5   -  T  -  -      
21 1-5   U  -  U  U      
22 1-5   -  -  -         
23 1-5   W  -  X         
24 2-3   -  Y            
25 3-6   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WDSAR RRVGX KRLEL DLKRV NKVSX W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   -  -  B  -  B  B
03 2-0   C  -  -  -  C  C
04 2-0   D  -  D  D  -  D
05 2-0   -  E  -  -  E  -
06 2-0   -  -  F  F  -  -
07 2-0   G  G  -  -  -  -
08 2-0   H  -  -  -  H  -
09 2-0   -  I  I  I  I  I
10 2-0   -  -  J  -  -  J
11 2-0   -  K  -  K  -  K
12 0-4   -  L  -  -  L  L
13 0-4   M  -  -  -  M  -
14 0-4   -  N  -  -  N  -
15 0-5   -  -  -  O  O  O
16 0-5   P  -  P  P  -  -
17 0-5   Q  -  -  Q  -  -
18 0-5   R  -  R  R  -   
19 0-5   S  -  -  -  -   
20 0-5   T  T  T  T      
21 0-5   -  U  U  U      
22 0-5   V  -  -         
23 0-6   W  -  X         
24 1-3   X  -            
25 1-4   -  Z            
26 2-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MMOOG QNFPL XNNBF WKSCZ JVUFQ G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  -  C  C
04 1-0   -  -  D  -  D  D
05 1-0   E  E  -  E  -  -
06 1-0   -  -  -  F  -  -
07 2-0   G  -  -  -  G  G
08 0-3   -  H  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-4   J  J  J  J  J  -
11 0-4   -  -  K  K  K  K
12 0-4   -  L  -  -  -  L
13 0-4   -  M  M  M  -  M
14 0-4   N  N  N  -  N  -
15 0-4   O  -  -  -  -  -
16 0-6   P  -  P  P  P  -
17 0-6   -  Q  Q  -  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   -  -  -  S  -   
20 1-4   -  -  T  T      
21 1-4   -  U  -  U      
22 1-4   V  -  -         
23 1-4   W  -  -         
24 1-6   X  -            
25 2-3   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZFKPE AAWWQ APZAQ MAZUU PKEKY S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  -  -  B  -
03 0-3   C  -  C  -  -  C
04 0-3   -  D  D  -  -  D
05 0-3   E  E  -  E  E  -
06 0-3   -  -  F  F  F  -
07 0-3   G  G  -  G  -  G
08 0-3   -  H  -  -  -  -
09 0-3   -  I  I  I  I  -
10 0-3   -  -  J  -  J  -
11 0-3   K  -  -  K  -  K
12 0-4   -  L  L  L  -  L
13 0-5   M  -  M  -  -  -
14 0-5   N  -  N  -  N  N
15 0-5   O  -  O  O  -  O
16 0-5   -  P  P  P  -  -
17 0-5   Q  Q  Q  Q  Q  -
18 0-5   -  R  -  R  -   
19 0-5   S  -  -  -  -   
20 0-5   T  T  T  -      
21 0-5   -  U  U  -      
22 0-5   V  -  -         
23 0-5   W  -  -         
24 1-3   -  -            
25 1-6   -  Z            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

MOXPF NOOLO BEVAB PLRMM CZLOA O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  B  B  -  B  B
03 1-0   C  C  C  -  -  -
04 1-0   D  -  -  D  -  D
05 2-0   -  E  E  E  E  -
06 2-0   F  F  -  -  F  F
07 2-0   G  -  G  G  G  -
08 2-0   -  -  -  -  -  -
09 2-0   -  -  -  -  I  I
10 2-0   J  J  -  -  -  -
11 2-0   -  -  -  K  K  K
12 2-0   -  -  -  -  -  -
13 2-0   M  M  -  M  M  -
14 0-3   N  N  N  N  N  N
15 0-3   -  O  O  -  -  O
16 0-4   P  -  -  P  P  P
17 0-4   -  Q  -  -  -  -
18 0-4   R  -  R  R  R   
19 0-4   S  -  -  -  -   
20 0-4   -  T  -  T      
21 0-6   -  -  U  -      
22 1-3   -  V  -         
23 1-4   -  -  X         
24 2-4   -  -            
25 2-4   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AMRPF TAWCQ KIWWJ TUIAH TKKUA N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 2-0   B  B  B  -  B  B
03 2-0   C  C  C  -  -  C
04 2-0   -  -  -  D  -  D
05 2-0   -  E  -  E  -  -
06 2-0   F  -  F  -  F  F
07 0-3   G  -  G  G  G  G
08 0-3   H  -  -  -  -  -
09 0-3   -  I  I  -  I  -
10 0-3   -  -  J  J  J  J
11 0-4   K  K  K  -  K  K
12 0-4   L  L  -  L  L  L
13 0-4   -  -  -  M  -  -
14 0-4   N  N  -  -  -  -
15 0-4   -  -  O  O  O  -
16 0-4   -  P  -  P  -  P
17 0-6   -  -  Q  -  -  -
18 0-6   R  R  -  R  R   
19 0-6   S  -  -  S  -   
20 1-6   T  T  T  T      
21 2-3   -  U  U  U      
22 2-4   -  -  V         
23 2-4   W  X  X         
24 2-4   X  Y            
25 2-4   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PSMXB VVSAT EFAWA VLGVP QSQKX F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  -  C  C
04 1-0   D  D  -  D  -  -
05 1-0   -  -  -  -  E  -
06 1-0   -  F  -  F  F  -
07 1-0   -  -  -  -  G  G
08 1-0   -  H  H  H  -  H
09 1-0   -  -  -  I  -  I
10 2-0   J  -  -  -  -  -
11 2-0   -  K  K  K  K  -
12 2-0   -  L  -  -  L  -
13 0-3   -  M  -  -  -  M
14 0-3   N  N  N  -  -  N
15 0-3   -  -  O  -  -  O
16 0-3   P  P  -  P  -  -
17 0-3   Q  Q  -  Q  Q  -
18 0-4   R  R  R  -  R   
19 0-4   S  -  S  -  -   
20 0-4   T  -  -  T      
21 0-4   -  U  U  -      
22 0-4   -  -  -         
23 0-4   -  -  X         
24 0-5   X  Y            
25 0-6   -  -            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

QSOUV HFMLR NSUGV AKLFI PFEJA I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   B  -  B  B  B  -
03 2-0   -  -  C  C  C  -
04 2-0   -  -  -  -  -  -
05 2-0   E  -  -  E  -  -
06 0-4   -  F  -  F  F  -
07 0-4   -  G  G  -  G  G
08 0-4   H  H  H  -  -  H
09 0-4   -  I  I  -  -  -
10 0-4   -  J  -  -  -  J
11 0-4   K  -  K  K  -  -
12 0-4   -  L  -  L  L  L
13 0-4   M  M  -  M  M  M
14 0-4   -  -  N  N  N  N
15 0-4   O  O  -  O  O  O
16 0-5   P  -  P  P  -  P
17 0-5   Q  -  -  -  Q  -
18 0-5   -  R  -  -  -   
19 0-5   S  S  -  -  -   
20 0-6   -  -  T  -      
21 0-6   -  U  U  U      
22 2-5   V  -  -         
23 2-6   -  -  X         
24 3-6   -  -            
25 4-5   Y  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

YWVAI LHDRJ YOBVV AIGJZ ZWZUV H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 2-0   B  -  B  -  -  -
03 2-0   C  -  C  C  C  -
04 2-0   -  -  D  -  -  -
05 2-0   -  E  E  -  E  E
06 2-0   F  F  -  F  F  F
07 2-0   -  -  G  G  G  G
08 2-0   H  -  -  -  H  H
09 2-0   I  -  -  I  I  I
10 0-3   J  -  -  J  -  -
11 0-4   -  K  -  K  K  -
12 0-4   L  L  L  -  -  L
13 0-5   -  -  -  -  -  -
14 0-5   N  -  -  -  -  -
15 0-5   O  O  O  -  -  O
16 0-5   -  -  -  P  -  P
17 0-6   -  Q  -  -  -  -
18 0-6   -  R  -  -  R   
19 0-6   S  -  S  S  -   
20 0-6   -  T  -  -      
21 0-6   -  U  U  -      
22 1-4   -  -  V         
23 2-6   W  -  -         
24 2-6   -  Y            
25 2-6   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KJKPI RTLGP RVQQR YTDRE BRSZO S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  -  B  B  -  -
03 1-0   -  -  -  C  C  -
04 1-0   D  D  D  D  D  -
05 1-0   -  E  -  -  -  E
06 1-0   -  -  F  -  -  -
07 1-0   G  -  G  G  G  -
08 0-3   -  -  H  -  -  -
09 0-3   I  I  I  I  I  I
10 0-3   J  -  -  -  -  J
11 0-3   -  K  K  K  K  -
12 0-5   L  -  -  -  L  L
13 0-5   M  M  -  M  M  M
14 0-6   N  -  -  -  -  N
15 0-6   -  O  O  -  -  -
16 0-6   -  P  -  P  -  P
17 0-6   Q  -  -  Q  Q  -
18 0-6   -  R  -  R  -   
19 0-6   S  S  -  -  -   
20 1-2   T  T  T  -      
21 1-6   -  U  -  U      
22 1-6   V  V  V         
23 1-6   W  -  X         
24 1-6   X  Y            
25 3-5   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

EAKTM PRSSG DSMFP RSSSW ABSLA B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   B  B  B  -  B  -
03 2-0   -  -  -  -  C  -
04 2-0   -  D  D  -  -  D
05 0-3   -  E  -  -  -  E
06 0-3   -  F  -  F  -  -
07 0-3   G  -  G  -  -  G
08 0-3   -  -  H  -  -  H
09 0-3   I  -  -  I  -  I
10 0-3   -  -  -  J  J  -
11 0-3   K  K  K  K  K  -
12 0-6   L  L  -  L  L  -
13 0-6   -  -  M  -  M  M
14 0-6   -  -  -  N  N  N
15 0-6   O  O  -  -  -  -
16 1-6   -  -  P  -  -  P
17 2-3   -  Q  Q  Q  -  -
18 2-5   R  R  R  R  -   
19 2-6   S  -  -  -  S   
20 2-6   T  -  T  -      
21 3-6   U  U  U  U      
22 3-6   V  V  -         
23 3-6   -  -  -         
24 3-6   -  -            
25 4-5   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KDAZN NTVDE OZIUT SSQDO WLAZZ U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 0-3   B  -  B  B  -  B
03 0-3   C  C  C  C  C  C
04 0-3   -  -  -  D  D  D
05 0-3   -  E  E  -  -  -
06 0-4   -  -  F  F  F  F
07 0-4   G  -  G  G  G  -
08 0-4   H  H  -  -  H  -
09 0-4   I  -  -  -  -  I
10 0-4   -  J  J  J  -  -
11 0-4   -  K  K  K  K  -
12 0-5   L  -  -  -  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  N  -  N  N  N
15 0-5   O  -  O  -  O  -
16 1-2   P  P  -  P  P  P
17 1-4   Q  -  Q  -  -  -
18 1-5   R  R  R  -  R   
19 1-5   -  -  -  -  S   
20 1-5   T  T  T  -      
21 2-5   U  U  -  U      
22 3-4   -  -  -         
23 3-5   -  X  X         
24 3-5   -  -            
25 3-5   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

FWPZU HSMKP ZYVPY ZIFWU LNWPS P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  -  -  -
03 1-0   C  C  -  -  -  C
04 1-0   -  -  D  -  D  D
05 0-3   -  E  E  E  E  E
06 0-3   F  F  F  F  -  F
07 0-3   G  -  G  G  G  G
08 0-3   H  -  -  -  -  -
09 0-4   -  I  I  -  I  I
10 0-4   -  -  -  -  -  -
11 0-4   K  -  -  K  -  -
12 0-4   -  L  -  L  L  -
13 0-6   -  M  -  -  M  -
14 0-6   N  -  N  -  -  N
15 0-6   O  O  -  -  -  O
16 0-6   -  -  P  -  P  P
17 0-6   Q  -  -  Q  Q  -
18 1-2   R  -  R  -  -   
19 1-6   -  S  S  S  -   
20 2-4   -  T  T  T      
21 2-4   U  U  -  -      
22 2-5   V  V  V         
23 3-4   -  -  -         
24 3-4   -  -            
25 3-4   -  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MGOGW WVSJQ TMHAP WRFOL RLKMM V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  A
02 2-0   B  -  B  -  B  -
03 2-0   -  -  -  C  -  -
04 2-0   D  -  -  -  -  D
05 2-0   E  -  -  E  -  -
06 0-5   -  -  -  F  F  F
07 0-5   -  G  G  -  G  G
08 0-5   H  H  H  -  -  -
09 0-5   I  -  I  -  I  I
10 0-5   -  J  -  J  -  -
11 0-5   -  -  -  -  K  -
12 0-5   -  L  -  L  -  -
13 0-6   -  M  -  -  -  M
14 0-6   N  -  N  -  N  N
15 0-6   O  -  O  -  -  -
16 1-4   -  P  -  P  P  -
17 2-3   Q  -  Q  Q  -  Q
18 2-3   R  R  -  R  R   
19 2-4   S  S  S  -  S   
20 2-5   T  T  T  T      
21 2-5   -  U  U  U      
22 2-5   V  V  V         
23 2-5   -  X  -         
24 2-6   -  -            
25 3-4   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

DPRTW PAAXL SFGTO GPOXG TLMZK O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  -  -  B  B  -
03 1-0   -  C  -  C  -  C
04 1-0   -  D  -  D  -  D
05 1-0   -  E  -  E  E  E
06 2-0   F  -  F  -  F  -
07 0-3   -  -  G  G  -  -
08 0-3   -  H  H  -  -  H
09 0-4   -  -  -  -  -  I
10 0-5   J  J  -  J  -  J
11 0-5   -  K  K  -  K  K
12 0-6   L  L  -  -  L  L
13 0-6   -  -  M  M  M  -
14 0-6   N  N  N  N  -  N
15 0-6   O  -  O  O  -  -
16 0-6   -  -  P  P  P  -
17 0-6   -  -  Q  Q  Q  Q
18 0-6   -  R  -  R  R   
19 0-6   S  -  S  -  S   
20 0-6   -  -  -  -      
21 0-6   U  -  -  -      
22 0-6   V  V  -         
23 0-6   W  X  X         
24 1-5   X  Y            
25 1-6   -  -            
26 2-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

UUEYE NVLLU RTKGO SWQTD ZCMUO N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 2-0   -  -  -  -  B  B
03 2-0   -  -  -  -  C  C
04 2-0   -  -  -  D  D  D
05 0-3   E  -  E  E  E  -
06 0-3   F  F  -  -  -  -
07 0-3   -  -  G  -  -  G
08 0-3   H  H  H  -  H  H
09 0-3   -  I  -  -  -  I
10 0-3   J  -  J  J  J  -
11 0-3   K  K  K  -  K  -
12 0-4   -  -  L  L  -  -
13 0-5   -  M  M  M  M  M
14 0-6   -  N  -  N  -  N
15 0-6   O  O  -  -  -  -
16 0-6   P  P  P  P  -  -
17 0-6   -  -  -  -  -  -
18 0-6   -  -  R  R  R   
19 0-6   -  -  S  S  S   
20 0-6   T  T  -  -      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GGSNH NHNSS ODDNS PLJHW XNIDF Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   B  B  B  B  -  B
03 2-0   -  -  -  -  -  -
04 2-0   D  D  D  -  D  D
05 2-0   E  E  -  E  E  -
06 2-0   -  F  F  -  F  F
07 2-0   G  -  -  -  G  G
08 2-0   H  H  -  -  H  H
09 2-0   I  -  I  I  -  I
10 2-0   J  -  J  -  -  -
11 2-0   -  K  -  -  -  -
12 0-3   L  L  L  L  L  -
13 0-3   M  M  -  -  -  M
14 0-3   -  -  N  N  N  -
15 0-3   -  O  O  -  -  -
16 0-4   -  -  -  P  -  P
17 0-4   -  Q  -  Q  Q  -
18 0-4   -  -  -  R  R   
19 0-5   -  -  S  -  -   
20 0-5   T  T  -  T      
21 0-6   -  U  U  U      
22 2-4   V  -  V         
23 2-4   -  -  X         
24 2-4   -  Y            
25 3-4   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VLRGH ZRLPT SISPS XMHTK HVTTO V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  -  B  B  B
03 1-0   C  C  -  C  C  C
04 1-0   D  D  D  -  D  -
05 0-4   -  -  -  -  E  -
06 0-4   F  F  -  F  -  F
07 0-5   -  G  G  -  -  G
08 0-5   H  -  -  H  -  -
09 0-5   I  -  -  -  -  -
10 0-5   J  -  J  J  J  J
11 0-6   -  K  -  K  K  K
12 0-6   L  L  -  -  L  L
13 0-6   -  M  -  M  -  -
14 0-6   -  N  N  -  -  -
15 0-6   -  -  -  O  -  -
16 1-5   P  P  P  P  -  -
17 1-5   Q  Q  Q  Q  -  Q
18 1-5   -  R  R  -  -   
19 1-5   -  -  S  -  S   
20 1-6   T  -  T  -      
21 2-3   -  U  -  U      
22 3-4   -  V  -         
23 3-5   -  -  -         
24 3-5   -  -            
25 4-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

APIIR DKTLS VUTSP ZWTZV IGJOM O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  A
02 2-0   B  -  B  -  -  -
03 2-0   C  C  C  C  -  C
04 2-0   D  -  -  -  -  D
05 2-0   E  -  E  E  E  -
06 0-3   F  F  -  -  F  -
07 0-3   -  G  G  -  G  -
08 0-3   H  -  H  -  -  H
09 0-3   I  -  I  -  I  I
10 0-3   -  -  J  J  J  -
11 0-3   -  K  -  -  K  -
12 0-5   -  -  L  L  L  L
13 0-5   M  -  M  -  M  -
14 0-5   -  N  -  N  N  -
15 0-6   O  -  O  O  -  -
16 0-6   P  -  P  P  -  P
17 0-6   -  -  -  -  -  Q
18 1-4   R  -  -  -  R   
19 2-3   S  -  -  -  -   
20 2-3   -  T  -  T      
21 2-3   U  U  U  U      
22 2-3   -  V  -         
23 2-6   -  X  -         
24 3-4   -  Y            
25 3-4   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FVRDZ ORXNN RNRRV FXWVR HWSIX R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  A
02 2-0   -  B  B  B  -  B
03 2-0   -  -  C  C  C  C
04 2-0   D  D  -  -  D  -
05 2-0   E  -  E  -  -  E
06 2-0   -  F  -  -  -  -
07 2-0   G  -  -  -  -  G
08 2-0   -  H  -  H  H  -
09 0-4   I  -  -  I  -  I
10 0-5   -  -  J  -  -  J
11 0-5   K  K  -  K  K  K
12 0-5   -  -  L  -  L  -
13 0-5   -  -  M  M  -  M
14 0-5   N  N  N  N  N  N
15 0-5   O  O  -  O  O  -
16 0-6   -  P  P  -  -  -
17 0-6   Q  -  -  -  -  -
18 0-6   -  R  -  -  R   
19 0-6   S  -  -  -  -   
20 0-6   T  -  T  T      
21 0-6   -  -  -  -      
22 0-6   -  -  -         
23 0-6   -  X  X         
24 0-6   X  -            
25 0-6   -  Z            
26 1-3   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

AKSZB ZHKZL QAIJJ RSMOS JQIBT Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   -  -  -  B  B  B
03 1-0   C  -  -  -  -  -
04 1-0   -  -  -  -  -  -
05 1-0   -  E  E  -  E  -
06 2-0   -  -  F  -  -  -
07 2-0   -  -  -  -  -  G
08 2-0   H  H  H  H  -  H
09 2-0   -  I  I  I  I  -
10 2-0   J  J  -  J  -  J
11 2-0   -  -  -  -  K  -
12 0-4   L  L  L  L  L  -
13 0-4   M  M  M  -  M  M
14 0-4   N  N  N  N  -  -
15 0-4   O  -  O  O  O  O
16 0-6   P  P  -  P  P  -
17 0-6   Q  -  -  -  Q  Q
18 0-6   -  R  R  R  -   
19 0-6   -  S  S  S  S   
20 1-3   -  T  -  T      
21 1-6   U  U  -  -      
22 1-6   -  V  -         
23 1-6   -  X  X         
24 1-6   X  -            
25 2-4   Y  -            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KWOMR WUMVP PHAOJ IOGWQ WJWAW O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  -  B  -  B  -
03 2-0   -  -  -  C  -  -
04 0-3   D  -  -  -  D  -
05 0-3   E  E  -  E  E  -
06 0-5   F  -  -  F  -  F
07 0-5   G  G  G  -  -  -
08 0-5   -  -  H  -  H  H
09 0-5   -  -  I  -  -  I
10 0-5   -  -  -  -  -  -
11 0-5   -  K  -  -  -  K
12 0-5   -  L  -  L  -  L
13 0-5   M  -  -  M  -  M
14 0-6   -  -  -  N  -  N
15 0-6   -  -  O  -  O  -
16 0-6   P  -  -  P  P  P
17 0-6   Q  -  -  -  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  -  S   
20 0-6   T  T  T  T      
21 0-6   -  U  U  U      
22 0-6   V  -  V         
23 0-6   W  -  -         
24 1-3   X  Y            
25 1-5   -  Z            
26 2-3   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

ORNTO PIZPL IQSAO YGSNJ KCVLO Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 0-3   -  -  -  B  B  B
03 0-3   C  C  -  C  -  -
04 0-3   -  D  D  D  -  D
05 0-3   E  E  E  E  E  E
06 0-3   -  -  -  F  -  F
07 0-4   -  -  -  -  G  -
08 0-4   -  H  H  -  H  -
09 0-4   I  -  -  -  -  -
10 0-4   J  -  J  J  -  J
11 0-4   -  K  K  -  -  -
12 0-4   -  L  L  -  L  L
13 0-4   -  M  M  -  M  M
14 0-4   N  -  N  N  -  -
15 0-5   -  -  O  O  -  O
16 0-5   -  -  -  P  P  -
17 0-5   Q  Q  -  Q  Q  -
18 0-5   R  R  -  -  -   
19 0-6   S  -  S  -  S   
20 1-2   T  T  -  -      
21 1-6   U  U  -  -      
22 3-4   -  -  V         
23 3-4   -  X  -         
24 3-4   X  -            
25 3-4   Y  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OAQOV RAHOI OOTUX ICJPS RNSXA O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-NOV-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  -  -  B  B
03 1-0   -  -  -  C  C  -
04 1-0   D  -  D  D  -  -
05 1-0   E  E  -  E  E  -
06 2-0   -  F  -  F  F  F
07 0-4   G  G  -  G  -  G
08 0-4   -  -  H  -  H  H
09 0-4   -  I  I  I  -  -
10 0-6   J  -  -  J  -  J
11 0-6   K  K  -  -  K  -
12 0-6   L  L  L  -  -  L
13 0-6   -  -  M  M  M  M
14 0-6   N  N  -  N  -  N
15 0-6   O  O  O  -  -  O
16 1-3   -  P  P  -  P  -
17 1-6   -  -  -  -  -  -
18 2-3   R  -  -  -  R   
19 2-3   S  -  -  S  -   
20 2-3   T  -  -  T      
21 2-3   U  -  U  -      
22 2-4   -  V  V         
23 2-4   -  -  X         
24 2-4   X  Y            
25 2-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MVSNO GIVSK LOUUU UQUUQ OAUAX X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
