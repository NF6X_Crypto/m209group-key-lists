EFFECTIVE PERIOD:
12-JUL-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  B  B  -  B
03 2-0   -  C  -  -  -  -
04 2-0   D  D  -  -  -  -
05 2-0   E  -  -  E  E  -
06 0-3   F  F  F  -  -  -
07 0-3   -  G  G  -  G  G
08 0-3   H  H  -  -  -  H
09 0-3   -  I  I  I  -  -
10 0-3   J  J  -  J  -  -
11 0-4   K  -  K  K  K  K
12 0-4   L  L  -  -  L  L
13 0-4   -  M  M  M  M  -
14 0-4   -  -  N  -  N  -
15 0-4   -  -  -  O  -  O
16 0-4   P  P  P  P  -  -
17 0-4   -  -  Q  Q  Q  -
18 0-4   -  -  R  R  -   
19 0-6   S  S  -  -  S   
20 1-2   -  -  T  -      
21 1-6   -  -  -  U      
22 2-3   -  V  V         
23 3-4   W  -  -         
24 3-4   -  -            
25 3-4   Y  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

XHSVY MJJUP VQITJ NQTAR RRQVO U
-------------------------------
