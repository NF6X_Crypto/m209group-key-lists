EFFECTIVE PERIOD:
02-JUL-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   -  -  -  -  -  -
03 2-0   -  -  -  C  -  C
04 2-0   D  D  D  D  -  D
05 0-3   -  E  E  E  E  E
06 0-3   -  -  -  -  F  -
07 0-3   G  -  -  -  G  -
08 0-3   -  H  -  H  -  -
09 0-3   I  -  -  I  I  I
10 0-3   -  -  J  -  J  -
11 0-4   K  K  -  K  -  -
12 0-4   -  L  L  -  -  L
13 0-4   -  -  -  -  M  M
14 0-4   N  N  -  N  N  -
15 0-4   -  O  O  O  O  -
16 1-6   P  -  P  -  P  P
17 2-3   -  -  Q  Q  Q  Q
18 2-3   -  R  -  -  -   
19 2-3   S  S  S  S  -   
20 2-3   -  T  T  -      
21 2-4   -  U  U  U      
22 2-5   -  -  V         
23 2-5   -  X  X         
24 2-6   X  Y            
25 3-4   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MWWVT VAKAA NUKUV NKLNU DVUMV Y
-------------------------------
