EFFECTIVE PERIOD:
10-JUL-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  A
02 2-0   -  B  B  B  B  B
03 0-3   C  -  C  -  C  -
04 0-3   D  D  -  D  D  -
05 0-3   E  E  E  E  E  E
06 0-3   F  -  -  F  -  F
07 0-3   G  G  G  G  -  G
08 0-4   -  H  -  -  -  H
09 0-4   I  -  I  -  -  -
10 0-4   J  J  -  -  J  -
11 0-4   -  K  -  -  K  -
12 0-4   L  L  L  L  -  L
13 0-4   -  -  -  -  -  M
14 0-4   -  N  N  N  N  N
15 0-4   O  O  -  -  O  O
16 1-2   -  -  -  -  -  -
17 2-3   Q  Q  Q  Q  Q  -
18 2-3   -  R  R  R  -   
19 2-3   S  S  S  -  S   
20 2-3   -  -  -  T      
21 2-5   -  U  U  U      
22 2-6   V  -  V         
23 2-6   W  -  -         
24 2-6   X  -            
25 3-4   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QOMSA UKYXB MSAOA UKLSF VSVJM V
-------------------------------
