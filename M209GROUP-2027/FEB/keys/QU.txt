EFFECTIVE PERIOD:
15-FEB-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  -  B  -  -  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  -  -  D  D
05 1-0   E  -  E  E  -  E
06 1-0   -  F  -  F  F  -
07 1-0   G  -  G  G  G  -
08 2-0   H  -  H  -  -  H
09 0-3   I  I  -  -  I  -
10 0-3   -  -  -  -  -  J
11 0-3   -  K  -  K  K  -
12 0-3   -  -  -  L  -  -
13 0-5   -  -  M  M  M  M
14 0-5   N  -  -  N  -  N
15 0-5   -  O  O  O  -  -
16 0-6   -  -  -  P  P  -
17 0-6   Q  Q  -  -  Q  Q
18 0-6   R  -  R  -  -   
19 0-6   S  -  S  S  S   
20 0-6   -  T  -  -      
21 0-6   U  U  U  -      
22 0-6   V  V  V         
23 0-6   -  -  -         
24 0-6   X  -            
25 0-6   -  -            
26 2-4   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

CPTRH QTJKZ QADMA HTKKT OCEOP C
-------------------------------
