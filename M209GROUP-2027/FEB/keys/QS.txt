EFFECTIVE PERIOD:
13-FEB-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   B  -  -  -  -  -
03 1-0   C  C  -  C  -  C
04 1-0   -  D  -  D  -  D
05 2-0   E  -  -  -  -  -
06 2-0   F  -  F  -  F  F
07 2-0   G  -  -  G  -  G
08 2-0   H  H  H  H  -  -
09 0-4   I  -  -  -  I  -
10 0-4   -  J  J  -  J  -
11 0-4   K  K  K  K  K  K
12 0-4   -  -  -  -  -  -
13 0-5   -  M  -  -  M  -
14 0-5   -  N  N  N  N  -
15 0-5   -  -  O  O  O  O
16 0-5   P  -  -  -  -  -
17 0-5   Q  Q  Q  Q  -  Q
18 1-4   R  R  -  R  -   
19 1-4   -  S  S  S  -   
20 1-4   T  -  -  -      
21 1-4   -  U  U  U      
22 1-5   -  V  V         
23 2-5   W  X  X         
24 2-6   -  -            
25 3-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MVZMV KQRWL IOQWF PUSTP MWQQI J
-------------------------------
