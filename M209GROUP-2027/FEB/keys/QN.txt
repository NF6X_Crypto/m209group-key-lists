EFFECTIVE PERIOD:
08-FEB-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  -  -  -  -  B
03 1-0   -  C  C  C  -  C
04 1-0   -  -  D  -  D  -
05 2-0   E  E  E  -  -  -
06 2-0   F  -  -  F  F  F
07 2-0   -  -  G  G  -  G
08 0-3   -  -  H  H  -  H
09 0-3   -  I  -  -  -  -
10 0-3   J  -  -  J  J  J
11 0-3   K  K  -  K  -  -
12 0-4   -  -  L  -  L  -
13 0-4   -  M  -  M  -  M
14 0-4   N  N  -  N  N  N
15 0-4   -  -  O  -  -  O
16 0-4   P  P  -  -  P  -
17 0-4   Q  -  Q  -  Q  -
18 1-3   R  R  -  R  -   
19 1-3   -  -  S  S  S   
20 1-3   T  -  T  -      
21 1-3   -  U  -  U      
22 1-4   -  -  -         
23 2-3   W  -  X         
24 2-4   X  Y            
25 2-6   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QZLPL SVLSA ZIQPT UXKHO LAHGU C
-------------------------------
