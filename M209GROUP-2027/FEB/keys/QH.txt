EFFECTIVE PERIOD:
02-FEB-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 2-0   -  B  -  -  B  -
03 2-0   C  -  -  C  -  C
04 2-0   -  -  D  D  -  -
05 2-0   -  E  E  -  -  -
06 2-0   F  F  F  -  F  -
07 2-0   -  -  -  -  G  -
08 0-3   -  -  H  H  -  -
09 0-3   I  -  -  -  -  I
10 0-3   J  J  J  -  J  -
11 0-3   K  -  K  -  -  K
12 0-3   -  L  L  L  L  L
13 0-3   -  M  -  M  -  M
14 0-3   N  N  -  -  N  -
15 0-3   -  O  -  O  O  O
16 0-4   -  P  -  P  -  P
17 0-5   -  -  -  -  -  Q
18 0-5   -  R  R  R  R   
19 0-6   S  -  -  S  -   
20 0-6   T  -  T  T      
21 0-6   U  U  -  U      
22 1-5   -  -  -         
23 2-3   W  -  X         
24 2-3   X  Y            
25 2-3   Y  Z            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RNIPR HZHFZ RKKPO VMSET UUQOQ N
-------------------------------
