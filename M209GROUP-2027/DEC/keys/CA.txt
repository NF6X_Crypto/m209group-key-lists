EFFECTIVE PERIOD:
04-DEC-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  -  B  B  -
03 1-0   -  -  C  -  -  C
04 2-0   D  D  D  -  -  D
05 0-3   E  E  -  E  -  -
06 0-3   F  F  F  F  F  -
07 0-3   G  -  G  G  G  -
08 0-3   H  H  -  H  -  -
09 0-3   I  -  I  -  I  I
10 0-3   -  J  -  J  -  J
11 0-3   K  -  K  K  -  K
12 0-4   L  L  L  L  L  -
13 0-4   -  -  -  -  -  M
14 0-4   N  -  N  -  N  -
15 0-4   -  -  O  O  -  O
16 0-4   -  -  -  P  P  -
17 0-4   -  Q  Q  Q  Q  Q
18 0-4   R  R  -  R  -   
19 0-4   -  S  -  -  S   
20 0-4   -  T  -  -      
21 0-4   -  U  U  -      
22 0-5   -  -  V         
23 0-5   -  -  -         
24 1-3   X  -            
25 1-5   Y  Z            
26 2-5   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

MHTUP PCMOI UZOVT YAFYS UPHWZ J
-------------------------------
