EFFECTIVE PERIOD:
22-DEC-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  C  C  C
04 1-0   -  -  -  -  D  -
05 1-0   E  E  -  E  E  E
06 1-0   -  -  F  F  -  -
07 1-0   G  G  G  G  -  -
08 1-0   -  -  -  -  H  -
09 1-0   -  -  I  -  -  I
10 2-0   J  J  -  J  -  J
11 2-0   K  -  K  -  K  -
12 2-0   -  L  L  L  -  L
13 2-0   -  -  M  -  M  M
14 2-0   N  -  N  -  -  -
15 2-0   -  -  O  -  -  -
16 2-0   P  -  -  P  P  -
17 2-0   -  -  -  Q  Q  Q
18 2-0   -  R  R  R  -   
19 2-0   S  S  S  -  -   
20 0-5   T  T  -  T      
21 0-6   -  U  -  -      
22 0-6   V  -  -         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KHFTV MYGPA VRTKA AGSHF VKLHQ G
-------------------------------
