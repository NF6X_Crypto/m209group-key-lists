EFFECTIVE PERIOD:
24-DEC-2027 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  B  B  -
03 1-0   C  C  C  -  C  -
04 1-0   D  -  -  -  -  -
05 0-3   E  -  -  E  -  -
06 0-3   -  -  F  -  F  F
07 0-3   -  G  -  -  -  -
08 0-3   H  -  -  -  -  H
09 0-3   I  I  -  -  I  -
10 0-3   J  J  J  J  -  -
11 0-4   -  -  K  -  -  K
12 0-4   -  L  L  L  -  -
13 0-4   M  -  M  M  -  -
14 0-4   -  N  -  -  N  N
15 0-6   O  O  O  -  O  O
16 0-6   P  -  -  P  P  -
17 0-6   Q  -  -  Q  -  Q
18 1-2   R  R  R  -  -   
19 1-4   -  -  S  S  S   
20 1-5   -  T  -  T      
21 1-6   U  U  U  -      
22 1-6   -  -  -         
23 1-6   -  X  X         
24 1-6   X  Y            
25 3-4   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

USLQV SPVLK XSOQO SBIQS AVKPV Q
-------------------------------
