EFFECTIVE PERIOD:
17-FEB-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  A  A
02 0-3   -  B  B  B  B  -
03 0-3   C  C  C  -  C  -
04 0-3   -  -  -  -  -  -
05 0-3   -  E  E  -  E  E
06 0-3   F  -  -  -  F  F
07 0-3   G  -  -  G  -  -
08 0-4   H  H  H  H  -  -
09 0-4   I  -  I  I  -  I
10 0-4   -  -  J  -  -  J
11 0-4   -  K  K  K  -  K
12 0-4   L  L  -  -  -  -
13 0-6   M  M  -  M  M  -
14 0-6   N  -  N  -  -  N
15 0-6   O  -  -  O  -  O
16 0-6   -  P  P  -  -  P
17 0-6   Q  -  -  -  Q  -
18 1-3   -  -  -  -  R   
19 1-5   -  -  S  S  S   
20 1-6   T  T  T  T      
21 2-6   U  U  -  -      
22 3-4   V  -  V         
23 4-6   -  -  -         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZERLN VVSKD UUVEI RTALT VRNUN L
-------------------------------
