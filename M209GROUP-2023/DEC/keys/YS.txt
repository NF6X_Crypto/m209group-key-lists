EFFECTIVE PERIOD:
27-DEC-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  -  B  B  B  B
03 1-0   C  -  C  C  -  -
04 1-0   D  D  D  -  D  D
05 1-0   E  E  -  -  -  -
06 2-0   -  -  F  -  F  F
07 2-0   G  -  G  -  -  G
08 2-0   H  -  H  -  H  -
09 2-0   -  -  -  I  I  -
10 0-3   -  -  J  -  J  J
11 0-6   -  -  -  K  -  K
12 0-6   -  L  -  -  -  -
13 0-6   M  M  -  M  -  M
14 0-6   N  -  N  -  N  -
15 0-6   O  -  -  O  O  -
16 1-2   -  -  -  -  -  -
17 1-2   Q  Q  -  -  -  -
18 1-2   R  R  R  -  R   
19 1-2   S  -  S  -  S   
20 1-6   -  T  T  T      
21 2-3   U  U  U  U      
22 2-3   -  -  V         
23 2-4   -  X  -         
24 2-4   -  -            
25 3-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

NJZSE UPNVN UHTUR VLWJN ZDMVP Q
-------------------------------
