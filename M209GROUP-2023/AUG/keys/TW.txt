EFFECTIVE PERIOD:
23-AUG-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  -  B  -
03 1-0   -  -  -  C  C  C
04 1-0   D  -  -  D  D  D
05 1-0   -  E  -  E  E  E
06 1-0   -  -  F  F  -  -
07 1-0   G  G  G  -  G  G
08 1-0   -  -  H  H  H  -
09 1-0   -  -  -  I  -  I
10 1-0   J  -  -  -  -  -
11 1-0   -  -  K  -  -  -
12 2-0   -  L  L  L  L  -
13 2-0   M  M  -  -  -  M
14 2-0   N  N  -  -  N  N
15 2-0   O  O  -  O  O  O
16 0-3   P  -  -  P  -  P
17 0-3   -  Q  -  Q  -  -
18 0-3   -  -  R  -  R   
19 0-3   S  -  S  -  -   
20 0-3   -  T  -  -      
21 0-4   U  U  -  U      
22 0-5   V  -  V         
23 0-6   W  -  X         
24 1-3   -  -            
25 2-3   Y  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SZJQK YSNKU ZWDDP OJYZC VJITU U
-------------------------------
