EFFECTIVE PERIOD:
24-AUG-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   B  B  -  B  -  -
03 1-0   -  C  C  -  -  C
04 1-0   D  D  D  -  D  D
05 1-0   -  -  E  E  -  -
06 0-3   F  -  F  F  F  -
07 0-3   -  G  G  -  -  -
08 0-3   H  -  H  H  -  H
09 0-5   -  -  -  -  -  I
10 0-5   -  J  J  -  -  J
11 0-5   K  -  -  -  K  K
12 0-5   -  L  L  -  L  -
13 0-5   M  M  -  -  M  M
14 0-6   N  -  N  N  N  N
15 0-6   O  O  O  -  -  -
16 1-3   -  -  -  P  P  -
17 1-5   -  Q  Q  -  -  Q
18 2-4   -  -  R  R  R   
19 3-4   S  S  -  S  -   
20 3-6   T  -  -  -      
21 4-6   -  U  U  -      
22 4-6   V  V  -         
23 4-6   W  -  -         
24 5-6   X  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VTQVV EXMGL SJMTV OVPPU QUEAP Q
-------------------------------
