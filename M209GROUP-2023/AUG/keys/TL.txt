EFFECTIVE PERIOD:
12-AUG-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  B  B  -  -  B
03 1-0   -  C  -  C  -  -
04 1-0   -  -  -  -  -  D
05 2-0   E  -  -  -  E  -
06 2-0   -  -  F  F  F  -
07 2-0   -  -  -  -  G  -
08 2-0   H  -  -  H  -  -
09 2-0   -  I  I  -  -  I
10 2-0   -  -  J  -  J  J
11 2-0   K  K  K  K  -  K
12 0-3   L  -  L  -  -  L
13 0-3   M  -  -  -  M  M
14 0-3   N  N  -  N  -  -
15 0-3   -  O  -  O  -  -
16 0-3   P  P  P  P  P  -
17 0-3   -  -  -  Q  -  -
18 0-3   -  R  -  -  -   
19 0-3   S  -  -  S  S   
20 0-3   T  -  T  -      
21 0-3   -  U  U  -      
22 0-5   -  V  -         
23 0-6   -  X  X         
24 1-2   -  -            
25 1-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PMMNI ZSUMQ UXOKD SCESU MWGIN U
-------------------------------
