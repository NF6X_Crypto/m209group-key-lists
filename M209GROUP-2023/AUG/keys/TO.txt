EFFECTIVE PERIOD:
15-AUG-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  B  -  B  B  B
03 1-0   -  C  C  -  C  -
04 1-0   D  D  -  D  -  D
05 2-0   -  -  -  E  -  -
06 2-0   F  F  F  F  F  F
07 2-0   G  -  G  -  G  -
08 2-0   H  -  H  -  -  -
09 2-0   -  I  I  -  I  I
10 2-0   -  -  -  J  J  J
11 0-5   -  -  K  K  -  -
12 0-5   -  L  -  L  L  -
13 0-5   M  -  -  M  -  -
14 0-5   -  -  N  -  -  -
15 0-5   O  -  -  O  -  O
16 0-5   P  -  -  -  -  P
17 0-6   -  Q  -  Q  -  Q
18 1-2   R  -  R  -  -   
19 1-6   S  -  S  S  S   
20 2-5   T  -  T  -      
21 2-5   -  U  -  -      
22 2-5   -  V  V         
23 2-5   -  -  -         
24 3-4   X  -            
25 4-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TZSFK EQVJL PKQUZ EASIU CFOKD K
-------------------------------
