EFFECTIVE PERIOD:
13-MAY-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 2-0   B  -  B  B  -  B
03 2-0   C  -  C  C  C  -
04 2-0   D  -  D  D  -  D
05 0-4   E  -  E  E  -  -
06 0-4   -  F  -  -  F  F
07 0-4   -  G  G  G  G  -
08 0-4   -  H  -  -  H  H
09 0-4   I  I  -  -  -  I
10 0-4   -  J  -  -  J  -
11 0-4   K  -  -  -  K  K
12 0-4   -  L  L  L  -  -
13 0-5   M  -  -  -  -  M
14 0-6   -  -  -  -  -  N
15 0-6   O  O  -  -  O  -
16 0-6   -  -  P  P  P  P
17 0-6   -  -  Q  -  Q  -
18 0-6   R  -  -  R  R   
19 0-6   -  S  -  S  -   
20 0-6   T  -  -  T      
21 0-6   -  U  U  -      
22 0-6   -  V  -         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   -  -            
26 2-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

PZNWG WNMPE WMVLP XERFQ XBRDK S
-------------------------------
