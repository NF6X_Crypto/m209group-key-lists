EFFECTIVE PERIOD:
18-MAY-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  -  -  A
02 0-3   -  -  -  B  B  -
03 0-3   -  -  -  C  C  -
04 0-3   -  -  -  D  D  D
05 0-3   E  -  E  E  -  E
06 0-3   -  -  F  F  F  -
07 0-3   G  G  G  G  G  -
08 0-5   -  -  H  -  -  -
09 0-5   I  -  I  -  -  -
10 0-5   J  -  J  -  -  J
11 0-6   -  -  -  -  K  -
12 0-6   -  L  L  L  -  L
13 0-6   M  -  M  M  M  M
14 0-6   N  -  -  N  -  N
15 0-6   O  -  O  -  O  O
16 1-5   -  -  -  P  P  -
17 2-4   Q  Q  -  -  Q  -
18 2-5   -  -  R  -  -   
19 3-5   S  S  -  S  -   
20 3-5   -  T  T  T      
21 3-5   -  U  U  -      
22 3-5   V  V  -         
23 3-6   W  -  -         
24 4-5   -  Y            
25 4-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CAKTA MQXZM ATCNV PTOZU TVVLA A
-------------------------------
