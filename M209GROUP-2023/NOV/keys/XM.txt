EFFECTIVE PERIOD:
25-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 1-0   -  -  -  B  -  B
03 1-0   C  -  C  C  -  -
04 1-0   -  D  D  -  D  -
05 1-0   E  -  -  -  E  -
06 0-3   F  F  -  F  -  -
07 0-3   G  -  -  -  -  -
08 0-3   H  H  H  -  -  H
09 0-3   -  I  -  I  I  I
10 0-3   -  -  -  -  J  -
11 0-3   K  K  -  K  -  K
12 0-3   -  L  L  -  -  L
13 0-4   -  M  M  M  -  M
14 0-4   -  -  N  N  N  -
15 0-5   O  -  -  O  O  O
16 0-6   P  -  P  -  -  P
17 0-6   Q  -  Q  Q  Q  -
18 0-6   R  R  R  -  R   
19 0-6   -  S  -  -  -   
20 1-4   -  T  T  -      
21 1-6   -  -  -  U      
22 2-3   -  V  -         
23 3-6   -  X  -         
24 3-6   -  -            
25 3-6   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZRANU LNJNJ VKNSN OJAPN ZAVPK A
-------------------------------
