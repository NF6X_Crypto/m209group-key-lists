EFFECTIVE PERIOD:
10-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  B  -  -  B  B
03 1-0   -  C  C  -  -  C
04 1-0   -  -  -  -  -  -
05 1-0   E  -  E  E  E  -
06 1-0   -  F  -  F  -  -
07 1-0   -  -  G  -  G  G
08 1-0   H  H  -  H  H  H
09 1-0   I  I  -  I  I  I
10 1-0   J  -  J  J  -  J
11 1-0   -  -  K  -  K  K
12 1-0   -  L  L  L  L  L
13 2-0   -  M  -  -  -  -
14 2-0   N  -  N  -  -  -
15 2-0   -  O  -  O  O  O
16 2-0   -  -  P  -  P  -
17 2-0   -  -  Q  Q  Q  -
18 2-0   R  R  R  -  R   
19 0-3   S  -  -  S  -   
20 0-4   T  T  T  -      
21 0-5   U  -  -  -      
22 0-5   V  V  -         
23 0-6   -  -  X         
24 1-2   -  -            
25 2-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NCPYU WTKCT BZYLH SODZY PDKNS O
-------------------------------
