EFFECTIVE PERIOD:
21-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  -  -  -  B  B
03 1-0   -  C  -  -  -  -
04 1-0   -  -  D  D  -  D
05 2-0   -  E  E  E  E  E
06 2-0   F  -  -  F  F  -
07 0-3   G  -  G  G  -  -
08 0-3   -  -  H  -  -  -
09 0-3   I  -  -  I  -  I
10 0-3   J  -  -  -  -  J
11 0-3   -  -  K  K  K  K
12 0-3   -  L  L  L  -  -
13 0-3   M  -  M  M  M  -
14 0-3   N  -  N  N  N  -
15 0-3   O  -  -  -  O  -
16 0-5   -  P  P  -  -  P
17 0-6   Q  Q  Q  Q  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  -  S   
20 0-6   -  -  T  -      
21 0-6   U  -  -  U      
22 1-2   -  V  V         
23 1-6   W  -  -         
24 2-4   X  Y            
25 3-6   -  Z            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

SJVMD WLRXE QQBQM MWXAG TNWRN X
-------------------------------
