EFFECTIVE PERIOD:
19-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  B  B  -  B  B
03 0-3   C  C  -  C  -  C
04 0-3   -  D  -  D  D  -
05 0-3   -  -  -  E  E  -
06 0-3   F  F  F  F  -  F
07 0-3   G  -  G  -  -  G
08 0-3   -  H  -  H  H  -
09 0-3   -  I  I  I  -  -
10 0-3   -  -  -  J  -  J
11 0-3   K  -  K  K  -  K
12 0-3   -  -  -  L  L  L
13 0-4   M  -  M  -  M  -
14 0-4   -  -  N  -  -  N
15 0-4   O  O  -  -  -  -
16 0-4   -  -  -  P  P  -
17 0-5   Q  Q  -  -  -  -
18 0-5   R  -  R  -  R   
19 0-5   -  S  S  -  -   
20 0-5   T  T  T  -      
21 0-5   -  -  U  -      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 0-5   X  -            
25 0-6   Y  Z            
26 1-6   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

UMDGV KRKMQ AZOIE RSIQM YCJUD M
-------------------------------
