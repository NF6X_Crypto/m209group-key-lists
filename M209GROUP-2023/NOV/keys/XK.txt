EFFECTIVE PERIOD:
23-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   -  B  -  -  -  -
03 2-0   -  -  -  C  -  -
04 2-0   -  -  D  -  -  -
05 2-0   E  E  E  E  -  -
06 0-4   -  -  -  -  F  F
07 0-4   G  -  G  G  -  G
08 0-4   H  H  H  -  H  H
09 0-4   I  I  -  I  I  I
10 0-4   -  J  -  -  J  -
11 0-5   K  K  K  -  K  -
12 0-5   L  L  L  -  -  L
13 0-5   -  -  -  -  -  M
14 0-6   N  N  N  -  -  N
15 0-6   O  O  -  O  O  -
16 0-6   -  -  -  P  -  P
17 0-6   Q  -  -  -  Q  Q
18 1-4   -  -  R  R  -   
19 2-5   S  S  S  -  S   
20 2-6   -  -  T  T      
21 3-4   U  -  U  -      
22 3-5   -  V  V         
23 4-5   -  X  -         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IJQVI VKANL GSOVK AGQUP QJURA H
-------------------------------
