EFFECTIVE PERIOD:
07-NOV-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 2-0   -  B  B  B  -  B
03 2-0   -  C  -  C  C  -
04 2-0   D  -  D  D  D  -
05 2-0   -  E  E  -  -  -
06 2-0   -  F  F  -  -  -
07 2-0   G  G  -  G  G  G
08 2-0   H  -  -  -  H  -
09 2-0   I  I  -  I  I  -
10 2-0   J  J  J  -  -  J
11 2-0   K  K  K  K  K  -
12 2-0   -  L  L  -  -  -
13 0-3   M  -  -  -  -  M
14 0-3   -  -  -  N  -  N
15 0-3   O  O  -  -  O  O
16 0-3   P  P  -  P  P  P
17 0-3   Q  Q  Q  -  -  -
18 0-4   -  -  R  R  -   
19 0-4   -  -  S  S  S   
20 0-4   T  -  -  T      
21 0-4   U  -  U  -      
22 0-5   -  -  -         
23 0-6   -  -  -         
24 1-5   X  Y            
25 2-3   Y  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NTMOL OFOCT OYRSK YPTDZ UUUDG O
-------------------------------
