EFFECTIVE PERIOD:
23-JUL-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  -
02 2-0   -  -  B  B  -  -
03 2-0   C  -  -  C  -  C
04 0-3   D  D  -  -  -  D
05 0-3   E  -  -  E  -  -
06 0-3   -  F  F  -  F  F
07 0-3   -  G  G  G  G  G
08 0-3   -  H  -  -  -  -
09 0-3   I  I  -  -  I  I
10 0-4   -  -  J  -  J  J
11 0-4   -  -  K  -  K  -
12 0-4   L  L  -  L  -  -
13 0-4   M  -  -  -  M  M
14 0-4   N  N  N  -  -  N
15 0-4   -  -  O  O  O  -
16 1-2   P  P  P  -  P  -
17 1-6   -  -  -  Q  Q  -
18 2-3   -  -  R  -  -   
19 2-3   -  -  S  -  -   
20 2-4   -  T  -  T      
21 2-4   -  U  -  U      
22 2-4   V  V  V         
23 2-4   -  -  -         
24 2-5   X  Y            
25 2-6   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

LTVDU TMSWN AACPM WULLU WZMWU T
-------------------------------
