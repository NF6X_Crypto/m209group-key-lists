EFFECTIVE PERIOD:
10-JUL-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  B  -  -  B
03 1-0   -  -  -  C  C  -
04 1-0   D  D  -  D  D  D
05 1-0   -  E  -  E  E  E
06 1-0   -  -  F  F  -  -
07 1-0   G  G  -  G  -  -
08 2-0   -  H  H  -  -  H
09 0-3   -  -  -  -  -  -
10 0-3   -  J  -  J  J  -
11 0-3   K  K  K  -  K  K
12 0-4   L  L  -  L  -  L
13 0-4   -  M  M  M  -  M
14 0-4   N  N  N  N  -  -
15 0-4   O  -  O  O  -  -
16 0-4   -  -  P  -  P  P
17 0-4   -  -  Q  -  Q  -
18 0-4   R  R  -  -  R   
19 0-4   -  -  -  S  -   
20 0-4   T  -  -  T      
21 0-4   U  -  U  -      
22 0-6   V  -  -         
23 0-6   W  X  -         
24 1-3   -  -            
25 2-5   -  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

RRYHI MSYFZ IAKJX RZQPD RUARQ P
-------------------------------
