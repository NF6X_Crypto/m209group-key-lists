EFFECTIVE PERIOD:
25-JUL-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ST
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  -  -  -  B  B
03 1-0   -  -  -  C  C  -
04 2-0   -  D  -  -  -  D
05 2-0   -  -  E  E  E  -
06 2-0   F  -  F  -  -  -
07 2-0   G  G  -  -  -  G
08 2-0   -  -  -  H  -  H
09 2-0   I  I  I  -  -  -
10 0-5   -  -  -  J  -  J
11 0-5   K  K  K  K  -  -
12 0-5   -  -  -  L  L  -
13 0-5   M  -  -  -  M  M
14 0-5   N  -  N  -  -  N
15 0-5   O  O  O  -  -  O
16 1-2   -  -  -  -  -  P
17 1-2   -  Q  Q  -  Q  Q
18 1-2   -  R  -  -  R   
19 1-2   S  S  S  S  -   
20 1-3   -  -  -  T      
21 1-4   U  -  U  -      
22 1-4   -  V  -         
23 1-4   -  X  X         
24 1-6   X  Y            
25 2-5   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SIMTM UMQAW TUHFN ANSFA NTBMT A
-------------------------------
