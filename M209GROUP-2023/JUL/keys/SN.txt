EFFECTIVE PERIOD:
19-JUL-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 2-0   -  -  B  B  -  B
03 2-0   -  C  C  -  C  -
04 2-0   -  D  -  -  D  D
05 2-0   E  E  -  E  E  -
06 2-0   -  -  -  -  -  -
07 2-0   -  -  -  G  -  -
08 0-3   -  H  -  H  -  H
09 0-3   I  -  I  -  I  I
10 0-3   -  -  J  J  J  J
11 0-3   K  K  K  K  -  -
12 0-3   -  L  L  L  L  L
13 0-3   M  -  M  -  -  M
14 0-3   -  -  -  N  -  N
15 0-3   -  O  -  -  O  O
16 0-4   P  P  -  P  P  -
17 0-4   Q  -  Q  -  Q  Q
18 0-4   -  -  -  -  R   
19 0-4   S  -  S  S  -   
20 0-4   T  T  -  T      
21 0-4   U  U  U  U      
22 0-5   -  V  -         
23 0-6   W  X  X         
24 2-3   X  Y            
25 2-4   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LRBUE TRXSP KMLOJ RKCYU SSYEJ L
-------------------------------
