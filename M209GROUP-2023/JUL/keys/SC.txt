EFFECTIVE PERIOD:
08-JUL-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   B  B  -  -  -  -
03 2-0   -  -  C  C  -  -
04 2-0   D  D  D  -  D  D
05 2-0   -  -  -  -  -  -
06 2-0   F  F  -  F  -  -
07 0-3   G  G  -  G  G  G
08 0-4   -  H  H  -  H  -
09 0-4   I  I  I  -  I  -
10 0-5   -  J  -  -  -  -
11 0-5   -  -  K  K  K  K
12 0-5   -  -  L  -  L  L
13 0-5   -  M  -  M  M  M
14 0-5   N  -  -  -  -  N
15 0-5   -  -  -  O  O  -
16 0-5   P  -  -  P  -  -
17 0-5   Q  Q  Q  -  -  Q
18 0-5   R  R  R  -  -   
19 0-6   S  S  -  S  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 2-5   -  -  -         
23 2-5   -  X  X         
24 2-5   -  -            
25 2-6   Y  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

URQAK APQVC HQLGW PSPXZ XELPI Z
-------------------------------
