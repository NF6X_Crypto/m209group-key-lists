EFFECTIVE PERIOD:
06-JUN-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  B  B  B  B  -
03 1-0   -  -  -  C  -  -
04 1-0   D  -  D  D  D  -
05 1-0   E  E  E  E  -  -
06 1-0   -  F  F  F  F  -
07 1-0   -  -  G  -  -  G
08 1-0   H  -  -  -  H  H
09 1-0   I  -  -  I  I  I
10 2-0   -  -  -  J  -  J
11 0-3   -  -  K  -  K  K
12 0-4   -  -  L  -  L  L
13 0-5   M  M  M  M  M  -
14 0-5   N  N  -  -  -  N
15 0-5   O  -  -  O  O  -
16 0-5   -  P  P  -  P  -
17 0-6   -  -  -  -  -  Q
18 0-6   R  R  R  -  R   
19 0-6   S  S  S  S  -   
20 0-6   -  -  T  -      
21 0-6   -  -  U  -      
22 0-6   V  V  -         
23 0-6   -  X  -         
24 1-6   -  -            
25 2-3   -  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OPNYI CZMBG AJMQV ZQJXP PDHZT R
-------------------------------
