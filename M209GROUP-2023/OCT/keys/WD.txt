EFFECTIVE PERIOD:
21-OCT-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  B  -  B  -  -
03 1-0   -  C  C  C  -  C
04 1-0   D  -  D  D  -  -
05 1-0   E  -  -  -  E  -
06 1-0   -  -  F  F  F  F
07 2-0   G  -  G  -  G  G
08 0-3   -  H  H  -  H  -
09 0-3   I  I  -  I  -  I
10 0-3   -  J  J  -  J  -
11 0-3   K  -  -  K  K  -
12 0-3   -  L  L  L  L  L
13 0-3   M  -  -  M  -  M
14 0-3   -  N  -  -  N  -
15 0-3   -  O  -  -  O  -
16 0-3   P  P  P  -  -  P
17 0-3   -  Q  Q  -  -  -
18 0-3   -  R  R  R  -   
19 0-3   S  S  -  S  S   
20 0-4   -  -  -  -      
21 0-5   U  -  -  -      
22 0-5   V  -  V         
23 0-5   -  X  -         
24 0-5   -  -            
25 0-6   Y  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NDTYN JGENI SGUTL YLXHA CXHFB N
-------------------------------
