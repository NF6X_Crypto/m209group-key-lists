EFFECTIVE PERIOD:
24-OCT-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   B  B  -  -  B  B
03 0-3   -  -  C  C  C  -
04 0-4   D  -  D  -  -  -
05 0-4   E  -  -  -  E  E
06 0-5   -  F  F  F  -  F
07 0-5   -  -  -  -  -  -
08 0-5   H  -  -  H  -  -
09 0-5   -  I  I  -  I  -
10 0-5   -  J  J  J  J  J
11 0-6   K  K  -  K  K  K
12 0-6   -  L  L  L  -  L
13 0-6   -  -  M  -  -  -
14 0-6   N  N  N  -  -  N
15 0-6   -  -  O  O  O  -
16 0-6   P  -  P  P  -  -
17 0-6   -  Q  -  -  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  -  -   
20 1-3   -  -  T  T      
21 2-4   U  U  -  -      
22 2-5   -  V  -         
23 3-4   W  -  -         
24 5-6   X  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPIWD CGHTN JUYCE CYSYI TGPFI P
-------------------------------
