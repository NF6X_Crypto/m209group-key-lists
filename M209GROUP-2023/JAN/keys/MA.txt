EFFECTIVE PERIOD:
31-JAN-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 2-0   -  B  -  B  B  B
03 2-0   -  C  C  -  C  C
04 2-0   -  D  D  D  -  -
05 2-0   -  -  -  -  -  E
06 2-0   F  F  F  -  -  -
07 2-0   G  -  -  G  G  -
08 0-3   H  -  H  -  -  -
09 0-4   -  -  -  -  I  -
10 0-4   J  J  -  J  -  -
11 0-4   K  -  K  -  -  K
12 0-4   -  L  L  -  L  L
13 0-5   -  -  M  M  -  M
14 0-6   N  -  -  N  -  N
15 0-6   -  -  -  O  O  -
16 0-6   P  P  P  P  -  -
17 0-6   Q  Q  -  -  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   S  -  -  -  S   
20 0-6   T  T  -  T      
21 0-6   -  U  U  U      
22 1-3   V  -  V         
23 2-4   -  X  X         
24 2-6   X  -            
25 2-6   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

TYZZB PTTAU PRQMK HMOSL VIRQT N
-------------------------------
