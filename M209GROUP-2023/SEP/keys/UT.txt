EFFECTIVE PERIOD:
15-SEP-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  A  -
02 2-0   B  -  -  B  -  -
03 2-0   -  C  C  C  -  C
04 0-3   D  -  -  -  -  D
05 0-3   E  -  -  E  -  -
06 0-3   -  -  F  F  -  F
07 0-3   G  G  -  -  -  -
08 0-3   H  H  -  H  H  H
09 0-4   -  I  I  I  I  I
10 0-5   J  J  -  J  J  -
11 0-5   -  -  -  -  -  K
12 0-5   L  L  L  L  L  -
13 0-5   M  -  -  -  M  M
14 0-6   N  N  N  -  N  -
15 0-6   -  O  O  -  O  -
16 0-6   -  P  -  -  -  P
17 0-6   -  -  -  -  Q  Q
18 0-6   R  -  R  -  -   
19 0-6   -  -  -  S  -   
20 1-6   -  T  T  T      
21 2-4   U  U  U  U      
22 2-5   V  -  V         
23 3-5   W  X  X         
24 3-6   X  Y            
25 3-6   -  Z            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZSQFJ UWMUW QFQFM VHVQU LOUQT E
-------------------------------
