EFFECTIVE PERIOD:
19-SEP-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 2-0   -  -  -  -  B  -
03 0-3   -  -  C  -  -  -
04 0-3   D  -  -  D  -  D
05 0-3   -  -  E  E  E  -
06 0-3   F  F  F  -  F  -
07 0-3   G  -  G  -  -  -
08 0-3   -  H  H  -  -  H
09 0-4   -  -  -  -  I  -
10 0-4   J  J  -  -  -  J
11 0-4   K  -  -  K  K  K
12 0-4   -  L  L  L  L  L
13 0-5   -  -  M  M  M  M
14 0-5   N  N  -  -  N  -
15 0-5   -  -  O  -  O  -
16 0-5   P  -  -  P  -  P
17 0-5   Q  Q  Q  Q  -  -
18 0-5   -  R  -  R  -   
19 0-5   S  -  S  -  -   
20 0-5   T  T  -  T      
21 0-5   -  -  U  U      
22 2-4   -  -  -         
23 2-6   -  -  -         
24 3-4   -  Y            
25 3-5   -  Z            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

TUOAG QZPRT NCXRL JWLOS UYVZR F
-------------------------------
