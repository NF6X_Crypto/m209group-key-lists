EFFECTIVE PERIOD:
01-SEP-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  B  -  -  -  B
03 1-0   C  -  C  -  -  C
04 1-0   -  -  D  -  D  D
05 1-0   E  -  -  E  E  -
06 2-0   -  F  -  F  F  F
07 2-0   -  -  -  -  G  G
08 2-0   H  -  H  -  H  -
09 2-0   -  -  I  -  -  -
10 0-3   -  J  -  J  J  -
11 0-3   K  -  -  K  -  -
12 0-5   L  -  -  -  -  L
13 0-6   -  M  -  M  -  M
14 0-6   -  N  -  -  -  -
15 0-6   O  -  O  -  O  O
16 0-6   -  P  -  -  -  -
17 0-6   Q  Q  -  -  -  -
18 0-6   R  R  R  R  R   
19 0-6   -  S  S  S  -   
20 1-2   -  -  -  T      
21 1-6   U  -  -  -      
22 1-6   V  V  V         
23 1-6   W  X  X         
24 1-6   -  -            
25 2-3   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

AYVIT OMWVP YIAWC MFFVL FRMFT T
-------------------------------
