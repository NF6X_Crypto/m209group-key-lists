EFFECTIVE PERIOD:
28-SEP-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  -  -  -
02 0-3   -  B  B  -  -  B
03 0-4   -  C  C  -  C  -
04 0-4   -  -  D  D  D  -
05 0-4   E  -  -  E  E  E
06 0-4   F  F  -  -  F  F
07 0-4   -  -  G  G  G  -
08 0-5   H  H  -  H  H  H
09 0-5   -  -  -  I  -  -
10 0-5   -  -  -  J  -  -
11 0-5   K  -  K  -  K  -
12 0-6   -  -  L  L  -  L
13 0-6   M  -  -  M  -  M
14 0-6   -  N  -  -  -  N
15 0-6   -  O  O  -  -  O
16 0-6   P  P  P  -  P  P
17 0-6   Q  Q  Q  -  Q  -
18 1-2   -  -  R  R  R   
19 2-5   S  S  -  S  S   
20 2-6   T  -  -  T      
21 2-6   U  -  -  -      
22 3-4   -  -  V         
23 3-6   -  -  X         
24 3-6   -  Y            
25 3-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KQQPV YLLOK PVAUR MQGYL UVLGP V
-------------------------------
