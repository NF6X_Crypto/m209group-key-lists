EFFECTIVE PERIOD:
05-SEP-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  B  B  -  -
03 1-0   -  -  C  C  -  -
04 1-0   -  -  -  D  D  -
05 1-0   E  E  -  E  E  E
06 1-0   -  -  F  -  F  -
07 1-0   G  G  -  -  -  G
08 2-0   H  H  H  -  -  H
09 2-0   -  -  I  -  I  -
10 2-0   -  -  J  -  J  J
11 0-3   K  -  -  K  K  K
12 0-3   L  L  L  L  L  -
13 0-3   -  M  -  -  -  -
14 0-3   N  N  -  N  N  N
15 0-3   O  O  O  O  -  O
16 0-5   P  -  -  -  P  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   R  -  R  -  -   
19 0-5   S  S  -  S  S   
20 1-2   -  -  -  -      
21 1-2   -  -  -  -      
22 1-2   V  -  -         
23 1-2   W  X  -         
24 1-6   X  Y            
25 2-3   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VTVST ZMRVI WMSGA VGAAL BXQOV Q
-------------------------------
