EFFECTIVE PERIOD:
26-MAR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  -  -  B  -  B
03 1-0   C  -  C  C  C  C
04 1-0   -  D  D  -  -  D
05 1-0   -  E  E  -  E  -
06 2-0   -  F  F  F  F  F
07 2-0   G  G  -  -  G  -
08 2-0   -  -  -  -  H  H
09 2-0   I  I  I  -  -  -
10 2-0   J  -  -  J  -  -
11 0-3   -  -  -  -  -  K
12 0-4   L  L  -  -  L  -
13 0-4   -  -  -  -  M  M
14 0-6   -  N  -  N  N  N
15 0-6   O  -  O  O  -  O
16 0-6   P  -  P  P  -  -
17 0-6   -  -  Q  -  Q  -
18 0-6   -  R  R  R  -   
19 0-6   -  S  S  -  -   
20 1-2   T  T  T  T      
21 1-4   U  U  -  -      
22 2-6   V  -  -         
23 2-6   -  X  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

RFPVY USNPT ODTJV MJVFW RKSDV T
-------------------------------
