EFFECTIVE PERIOD:
14-MAR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   -  B  -  -  B  B
03 2-0   -  C  -  -  -  C
04 2-0   -  -  D  D  -  -
05 0-3   -  -  E  -  -  E
06 0-3   -  F  F  F  F  -
07 0-3   G  G  G  -  -  -
08 0-3   -  H  -  H  H  H
09 0-3   -  -  I  -  -  I
10 0-5   J  J  J  -  J  -
11 0-5   -  -  -  K  K  -
12 0-6   L  L  -  -  -  -
13 0-6   M  -  M  -  -  M
14 0-6   -  N  N  -  N  -
15 0-6   O  -  -  O  -  -
16 0-6   -  -  -  P  P  -
17 0-6   -  Q  Q  Q  Q  Q
18 1-4   -  -  -  R  R   
19 1-5   S  S  S  -  S   
20 1-6   -  T  -  -      
21 1-6   -  -  U  U      
22 2-3   V  -  -         
23 2-6   W  X  X         
24 2-6   X  -            
25 2-6   Y  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

GZVKN SQWSW MNJRT VPPRT MDSWV Q
-------------------------------
