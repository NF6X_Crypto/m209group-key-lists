EFFECTIVE PERIOD:
27-MAR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  B  B  -  B  -
03 1-0   -  C  -  C  -  -
04 1-0   -  D  -  -  D  D
05 1-0   -  -  -  E  -  E
06 2-0   -  F  -  -  -  -
07 2-0   G  -  G  -  G  G
08 0-3   -  H  -  -  -  -
09 0-3   -  -  -  -  -  I
10 0-3   -  J  J  -  -  -
11 0-3   K  K  K  K  -  -
12 0-3   -  L  -  L  -  -
13 0-6   M  M  -  M  M  M
14 0-6   N  N  N  N  -  N
15 0-6   O  -  -  O  -  -
16 1-2   P  P  P  -  P  -
17 1-2   -  -  Q  Q  Q  -
18 1-4   R  R  R  R  R   
19 1-4   S  -  S  S  -   
20 1-6   T  T  T  T      
21 1-6   U  -  -  -      
22 1-6   V  -  -         
23 1-6   -  X  -         
24 2-3   X  -            
25 2-4   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VIYZV XVLRD HAFQG HLMFR VRIXV V
-------------------------------
