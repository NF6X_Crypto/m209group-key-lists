EFFECTIVE PERIOD:
21-MAR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  -  B  -  B  B
03 1-0   -  -  -  C  C  -
04 1-0   -  -  -  -  -  D
05 2-0   -  -  E  -  E  -
06 2-0   F  -  -  -  F  -
07 2-0   -  -  -  G  G  G
08 2-0   H  H  -  H  H  -
09 2-0   -  -  I  I  I  -
10 2-0   J  -  -  -  J  J
11 2-0   K  K  K  K  -  K
12 2-0   -  -  L  L  L  -
13 2-0   M  M  M  M  -  M
14 0-3   -  N  -  N  -  -
15 0-4   -  -  -  O  O  O
16 0-4   -  P  P  P  P  -
17 0-4   Q  Q  -  -  -  Q
18 0-5   R  -  -  -  -   
19 0-5   -  S  -  S  -   
20 0-5   -  -  -  -      
21 0-6   U  -  U  -      
22 1-4   V  V  V         
23 1-5   -  X  X         
24 2-4   X  -            
25 2-4   -  Z            
26 2-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MUXLR FNOUF SQJFF LQMSM MXMZR C
-------------------------------
