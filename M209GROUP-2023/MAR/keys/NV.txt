EFFECTIVE PERIOD:
19-MAR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   B  B  -  -  B  -
03 1-0   C  -  C  C  -  C
04 1-0   D  -  D  -  D  D
05 1-0   -  -  -  -  -  -
06 2-0   F  F  F  F  -  F
07 2-0   G  -  -  G  -  -
08 0-3   -  -  H  -  -  -
09 0-3   I  I  -  I  I  -
10 0-3   -  J  J  -  -  J
11 0-3   K  K  K  K  -  K
12 0-3   -  -  L  L  L  L
13 0-3   M  -  -  -  M  M
14 0-6   N  -  -  N  N  -
15 0-6   -  O  O  O  -  O
16 1-3   -  -  P  P  -  P
17 1-6   -  -  Q  -  Q  Q
18 1-6   -  -  -  R  -   
19 1-6   -  S  -  S  -   
20 1-6   T  -  -  -      
21 2-3   -  U  U  U      
22 2-4   V  -  -         
23 2-6   -  X  -         
24 2-6   -  Y            
25 2-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OTTJU LUHIU UMAQL JUYKA PALRT N
-------------------------------
