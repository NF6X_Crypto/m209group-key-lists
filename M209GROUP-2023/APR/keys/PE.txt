EFFECTIVE PERIOD:
23-APR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  B  B  -  B  -
03 1-0   C  -  -  C  -  -
04 1-0   D  D  -  D  D  -
05 1-0   -  E  E  -  -  -
06 1-0   -  F  -  F  F  F
07 1-0   -  -  G  -  G  -
08 1-0   -  -  H  -  H  H
09 1-0   -  -  -  I  I  I
10 1-0   J  -  -  J  J  -
11 1-0   K  K  K  -  -  K
12 1-0   -  L  -  -  -  L
13 2-0   M  -  -  M  -  M
14 2-0   N  N  N  N  N  -
15 2-0   -  -  O  O  -  O
16 2-0   -  P  P  -  P  P
17 0-3   Q  -  -  -  -  -
18 0-4   -  -  R  R  -   
19 0-5   -  S  -  -  S   
20 0-5   -  -  T  T      
21 0-5   U  -  -  U      
22 0-5   -  -  V         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-5   Y  Z            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

DTKZN OAUAX QAGSA MMKCB MZGVR A
-------------------------------
