EFFECTIVE PERIOD:
20-APR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   B  -  -  -  B  B
03 1-0   C  C  C  C  C  -
04 1-0   -  -  -  D  -  D
05 1-0   E  E  E  E  -  -
06 1-0   -  F  F  -  F  F
07 1-0   -  -  -  -  -  -
08 1-0   -  -  H  -  H  -
09 2-0   I  -  -  I  -  -
10 0-3   -  -  J  -  -  -
11 0-3   -  K  -  -  -  -
12 0-3   L  -  -  L  -  L
13 0-3   -  M  M  -  M  M
14 0-3   N  N  -  N  N  N
15 0-3   -  O  O  O  -  O
16 0-4   -  -  P  P  -  -
17 0-4   Q  Q  -  Q  Q  Q
18 0-4   -  R  R  R  R   
19 0-5   S  S  S  -  -   
20 0-5   -  T  T  T      
21 0-5   U  U  U  -      
22 0-5   V  -  V         
23 0-6   -  -  -         
24 1-3   X  -            
25 3-5   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AZSPO ZXMRJ HEVTH VAMBR KIUGZ S
-------------------------------
