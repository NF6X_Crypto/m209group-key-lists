EFFECTIVE PERIOD:
18-APR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  B  B  -  -  B
03 1-0   -  -  -  C  C  -
04 1-0   -  -  -  -  D  -
05 1-0   -  -  E  -  -  E
06 1-0   F  -  F  F  F  -
07 2-0   G  G  G  -  G  G
08 2-0   H  H  H  -  H  -
09 0-4   I  I  -  I  -  -
10 0-4   -  J  J  J  J  -
11 0-4   -  -  K  K  -  K
12 0-4   -  L  -  L  -  L
13 0-5   -  -  M  -  -  M
14 0-5   -  N  -  -  -  -
15 0-5   -  O  -  -  -  -
16 0-5   P  -  -  -  -  -
17 0-5   Q  Q  Q  Q  Q  -
18 1-2   R  -  R  R  R   
19 1-4   -  -  -  -  S   
20 2-3   -  -  -  T      
21 2-5   U  U  U  -      
22 2-5   V  -  V         
23 4-5   -  -  X         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NINAJ XWRMQ TUFLR MIAVV OZFZP U
-------------------------------
