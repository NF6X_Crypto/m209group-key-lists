EFFECTIVE PERIOD:
25-APR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  B  -  -  B  B
03 1-0   -  C  -  -  C  -
04 1-0   D  D  -  D  -  D
05 2-0   -  -  -  E  E  E
06 2-0   -  F  -  -  -  -
07 2-0   G  -  G  -  G  -
08 2-0   H  -  -  H  H  -
09 2-0   I  I  -  I  I  I
10 2-0   -  J  J  -  -  -
11 2-0   K  -  -  K  K  K
12 2-0   L  -  -  L  -  L
13 2-0   M  -  M  -  -  -
14 2-0   -  -  N  N  N  N
15 2-0   O  -  O  O  -  -
16 0-3   P  -  P  -  -  -
17 0-4   -  Q  -  -  Q  Q
18 0-5   R  R  R  -  R   
19 0-5   -  S  S  S  -   
20 0-6   T  T  T  -      
21 0-6   U  U  -  -      
22 0-6   -  -  -         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

MIUQR TWZKT KFPLS JQLNS RHHNE O
-------------------------------
