EFFECTIVE PERIOD:
22-APR-2023 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   B  B  B  -  B  -
03 2-0   C  C  C  C  C  -
04 2-0   -  D  D  D  D  D
05 0-3   E  -  E  E  -  E
06 0-3   F  -  -  -  F  -
07 0-3   G  -  G  -  -  -
08 0-3   H  -  -  H  H  H
09 0-4   -  I  -  I  I  -
10 0-5   J  J  -  J  -  J
11 0-5   -  -  -  -  -  K
12 0-5   L  -  L  -  -  L
13 0-5   M  -  M  -  M  -
14 0-6   -  N  N  -  -  N
15 0-6   O  O  -  O  -  -
16 0-6   -  -  -  P  P  P
17 0-6   Q  -  Q  Q  -  Q
18 0-6   -  -  -  -  R   
19 0-6   S  -  S  -  S   
20 1-2   -  T  T  -      
21 2-3   U  -  -  U      
22 2-3   -  V  -         
23 2-3   -  -  X         
24 2-3   -  Y            
25 3-6   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UNBSG UUOGT VAPUU GWKKI VIQWK P
-------------------------------
