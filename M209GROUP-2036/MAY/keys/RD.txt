EFFECTIVE PERIOD:
27-MAY-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  B  B  B  -  B
03 1-0   C  -  -  -  -  C
04 2-0   D  -  -  -  D  -
05 2-0   -  E  -  E  E  E
06 2-0   -  F  F  F  F  F
07 2-0   G  -  -  G  -  -
08 0-3   -  H  H  -  H  H
09 0-3   I  -  -  I  I  -
10 0-3   J  J  J  -  J  J
11 0-3   -  -  -  K  K  -
12 0-3   -  L  L  L  L  -
13 0-3   -  -  M  -  M  -
14 0-5   N  -  N  N  -  N
15 0-5   -  -  -  -  -  O
16 1-3   -  -  P  P  -  -
17 1-4   Q  -  Q  -  Q  -
18 1-5   -  R  -  -  -   
19 1-5   -  S  -  S  -   
20 2-3   T  -  T  -      
21 2-5   -  -  -  U      
22 2-5   V  V  -         
23 2-5   -  -  -         
24 2-5   -  -            
25 4-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RPHTQ JQJLD SWUPJ XPWOW UITHP I
-------------------------------
