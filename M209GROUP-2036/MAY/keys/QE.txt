EFFECTIVE PERIOD:
02-MAY-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  B  -  B  -  -
03 1-0   -  C  -  -  -  C
04 1-0   D  -  -  D  -  D
05 1-0   -  -  -  -  -  -
06 0-3   -  -  -  F  F  F
07 0-4   -  G  G  G  -  -
08 0-4   H  -  H  H  -  H
09 0-4   I  -  I  -  -  I
10 0-5   -  -  J  J  J  J
11 0-5   K  K  -  K  -  -
12 0-5   -  -  -  -  L  -
13 0-5   M  M  -  -  M  -
14 0-5   N  N  N  -  N  -
15 0-5   -  -  O  O  O  -
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  Q  -  Q  Q
18 0-6   R  -  R  R  -   
19 0-6   -  S  -  S  S   
20 1-4   -  T  -  T      
21 1-6   U  -  U  -      
22 2-3   V  V  -         
23 3-6   W  -  X         
24 4-5   -  -            
25 4-5   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

OVYOC WQALN NZRMQ LNOQY LTLSX C
-------------------------------
