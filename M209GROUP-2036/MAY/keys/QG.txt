EFFECTIVE PERIOD:
04-MAY-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  -  -  -
02 0-3   -  B  -  B  B  B
03 0-3   -  C  -  -  C  C
04 0-3   -  D  -  D  D  -
05 0-4   E  -  E  E  E  E
06 0-4   F  -  F  -  F  -
07 0-4   -  G  -  -  -  -
08 0-5   -  H  H  H  H  -
09 0-5   -  I  -  -  I  I
10 0-5   J  J  -  J  -  J
11 0-5   K  -  K  K  K  K
12 0-5   -  -  L  L  -  L
13 0-5   M  M  M  -  M  -
14 0-6   N  N  -  -  -  -
15 0-6   -  O  -  O  -  O
16 1-2   -  -  P  -  -  -
17 2-4   Q  Q  Q  Q  Q  -
18 2-4   R  -  -  -  -   
19 2-6   S  S  -  -  S   
20 3-4   T  -  T  T      
21 3-4   U  U  -  -      
22 3-4   V  -  V         
23 3-4   -  -  X         
24 3-5   X  Y            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

INRJP IMSPX XPUUM VWRWM YPIML U
-------------------------------
