EFFECTIVE PERIOD:
05-APR-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  A  A
02 0-3   -  -  -  B  -  B
03 0-3   -  C  -  C  -  -
04 0-4   -  D  -  -  D  D
05 0-4   E  E  E  E  E  -
06 0-4   F  -  F  -  -  -
07 0-4   G  -  G  -  G  G
08 0-4   -  -  H  -  -  H
09 0-5   -  I  -  -  I  I
10 0-5   -  -  -  J  J  -
11 0-5   -  -  -  -  K  -
12 0-5   -  -  L  -  -  -
13 0-5   -  -  -  M  -  M
14 0-5   N  -  N  -  N  N
15 0-5   -  O  -  O  -  -
16 1-3   P  -  -  P  -  -
17 2-6   Q  -  -  -  -  Q
18 3-4   R  -  R  R  R   
19 3-4   -  S  S  -  -   
20 3-4   -  T  T  T      
21 3-4   U  -  U  U      
22 3-6   -  -  V         
23 3-6   -  X  -         
24 3-6   X  Y            
25 3-6   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SAXPM SVTAL QSIPV NUURF FZZTQ N
-------------------------------
