EFFECTIVE PERIOD:
08-APR-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  -  -  -  -  B
03 1-0   -  -  C  C  -  C
04 1-0   D  -  -  -  D  -
05 2-0   E  -  -  E  E  -
06 0-3   -  -  F  -  -  F
07 0-3   -  -  G  G  -  -
08 0-3   -  H  -  -  H  H
09 0-3   I  -  -  -  I  -
10 0-3   J  -  -  -  J  J
11 0-3   -  K  K  K  -  K
12 0-4   L  -  L  L  L  L
13 0-4   M  -  -  -  M  M
14 0-4   N  N  -  N  -  -
15 0-4   -  O  -  O  O  -
16 0-4   P  -  P  -  -  P
17 0-4   Q  Q  -  -  -  -
18 0-4   R  R  R  -  R   
19 0-4   -  -  S  -  -   
20 0-5   -  -  -  T      
21 0-5   U  U  -  U      
22 1-3   -  -  V         
23 1-5   W  X  -         
24 2-5   -  -            
25 2-6   Y  Z            
26 3-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

KTYFI OFQTQ LPRJG YNECT YLTPI O
-------------------------------
