EFFECTIVE PERIOD:
27-MAR-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   -  -  B  -  -  B
03 2-0   -  C  C  -  C  -
04 2-0   -  D  -  D  D  D
05 2-0   -  E  -  E  E  -
06 0-3   F  -  -  -  -  F
07 0-4   -  G  -  -  -  G
08 0-4   H  H  H  H  H  -
09 0-4   I  -  I  I  I  I
10 0-4   J  -  -  -  -  J
11 0-4   -  K  K  -  K  -
12 0-4   L  L  -  L  L  L
13 0-4   M  M  M  -  -  M
14 0-4   -  N  N  -  N  N
15 0-5   O  O  -  -  -  -
16 0-5   P  -  P  -  -  -
17 0-5   -  Q  -  Q  Q  Q
18 0-5   -  R  -  R  R   
19 0-5   -  S  S  S  -   
20 0-5   -  -  -  -      
21 0-6   -  -  -  U      
22 2-4   V  V  -         
23 2-4   W  -  X         
24 2-4   X  -            
25 2-5   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RCRKP UVIVC LQVSH QVRSK POKMO A
-------------------------------
