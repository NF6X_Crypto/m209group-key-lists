EFFECTIVE PERIOD:
09-DEC-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   B  -  B  B  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  -  -  D  D  D
05 1-0   -  -  -  -  E  E
06 1-0   -  -  -  F  -  -
07 1-0   -  -  -  -  G  G
08 1-0   H  -  H  H  -  -
09 1-0   I  I  -  -  I  I
10 1-0   -  -  J  J  -  -
11 1-0   -  K  K  -  K  -
12 1-0   L  L  -  -  L  L
13 2-0   -  -  M  M  -  -
14 0-4   -  N  N  N  -  N
15 0-4   -  O  O  -  -  O
16 0-4   P  P  P  -  -  P
17 0-4   Q  -  -  Q  -  -
18 0-4   -  -  R  R  R   
19 0-4   -  S  S  S  S   
20 0-4   T  T  T  T      
21 0-4   U  -  -  -      
22 0-4   V  -  -         
23 0-5   -  -  X         
24 0-5   X  -            
25 0-5   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

AXBAX XZMLK ARZNM AMBZR BKXBC O
-------------------------------
