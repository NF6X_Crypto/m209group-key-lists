EFFECTIVE PERIOD:
02-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 2-0   B  -  B  -  -  B
03 2-0   C  C  C  C  C  -
04 2-0   D  D  -  D  -  D
05 2-0   -  E  E  -  E  E
06 2-0   -  F  F  F  -  -
07 2-0   -  -  -  -  G  G
08 2-0   H  -  H  -  -  H
09 2-0   I  -  I  I  -  -
10 0-4   -  -  -  -  J  -
11 0-4   K  -  K  K  -  -
12 0-4   L  L  L  -  L  L
13 0-4   -  -  M  M  -  M
14 0-4   -  N  -  -  N  -
15 0-4   O  O  -  -  O  O
16 0-4   P  P  -  -  P  P
17 0-4   Q  Q  -  Q  Q  -
18 0-4   -  R  R  -  R   
19 0-4   S  -  -  S  -   
20 0-4   T  -  T  T      
21 0-4   -  -  -  U      
22 0-4   -  -  V         
23 0-5   W  -  -         
24 0-5   X  Y            
25 0-5   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZJMMS EROSI MOSJA JHRCS BSNDM A
-------------------------------
