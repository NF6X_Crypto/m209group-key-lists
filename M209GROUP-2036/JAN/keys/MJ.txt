EFFECTIVE PERIOD:
24-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 0-4   -  B  B  -  B  B
03 0-4   C  C  -  -  C  -
04 0-4   D  D  D  D  -  -
05 0-4   E  E  -  E  E  E
06 0-5   -  -  -  -  F  -
07 0-5   G  G  -  -  G  -
08 0-5   H  H  -  -  H  -
09 0-5   -  I  I  -  -  -
10 0-5   -  -  J  -  -  J
11 0-5   -  -  -  K  K  K
12 0-5   -  L  L  L  -  L
13 0-5   M  M  M  M  -  M
14 0-5   -  N  N  N  N  -
15 0-5   -  -  -  -  -  O
16 0-5   -  -  -  -  -  P
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   -  -  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  -  -  -      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 1-3   X  Y            
25 1-4   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OOWAN ATBVT LKSTO OILWP FZPLM Y
-------------------------------
