EFFECTIVE PERIOD:
31-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   B  -  -  B  -  -
03 1-0   -  -  C  C  C  -
04 1-0   D  -  D  -  D  -
05 1-0   -  -  -  -  E  E
06 1-0   F  F  F  F  -  F
07 1-0   -  -  G  -  G  -
08 1-0   -  -  -  H  -  -
09 0-4   -  -  -  I  -  I
10 0-4   -  J  -  -  -  -
11 0-4   -  K  -  -  K  -
12 0-4   L  -  L  L  -  -
13 0-4   -  M  -  -  -  M
14 0-4   N  N  N  N  N  -
15 0-5   -  O  O  -  -  O
16 0-5   P  -  P  -  -  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   -  R  -  R  R   
19 0-5   -  -  -  -  -   
20 1-3   T  T  T  T      
21 1-5   U  -  -  U      
22 1-5   V  V  V         
23 1-5   W  X  X         
24 1-5   X  Y            
25 2-4   Y  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

OIMCV ATVVO RATAZ BVHJA IMVCU S
-------------------------------
