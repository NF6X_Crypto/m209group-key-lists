SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JAN 2036

    01 JAN 2036  00:00-23:59 GMT:  USE KEY LM
    02 JAN 2036  00:00-23:59 GMT:  USE KEY LN
    03 JAN 2036  00:00-23:59 GMT:  USE KEY LO
    04 JAN 2036  00:00-23:59 GMT:  USE KEY LP
    05 JAN 2036  00:00-23:59 GMT:  USE KEY LQ
    06 JAN 2036  00:00-23:59 GMT:  USE KEY LR
    07 JAN 2036  00:00-23:59 GMT:  USE KEY LS
    08 JAN 2036  00:00-23:59 GMT:  USE KEY LT
    09 JAN 2036  00:00-23:59 GMT:  USE KEY LU
    10 JAN 2036  00:00-23:59 GMT:  USE KEY LV
    11 JAN 2036  00:00-23:59 GMT:  USE KEY LW
    12 JAN 2036  00:00-23:59 GMT:  USE KEY LX
    13 JAN 2036  00:00-23:59 GMT:  USE KEY LY
    14 JAN 2036  00:00-23:59 GMT:  USE KEY LZ
    15 JAN 2036  00:00-23:59 GMT:  USE KEY MA
    16 JAN 2036  00:00-23:59 GMT:  USE KEY MB
    17 JAN 2036  00:00-23:59 GMT:  USE KEY MC
    18 JAN 2036  00:00-23:59 GMT:  USE KEY MD
    19 JAN 2036  00:00-23:59 GMT:  USE KEY ME
    20 JAN 2036  00:00-23:59 GMT:  USE KEY MF
    21 JAN 2036  00:00-23:59 GMT:  USE KEY MG
    22 JAN 2036  00:00-23:59 GMT:  USE KEY MH
    23 JAN 2036  00:00-23:59 GMT:  USE KEY MI
    24 JAN 2036  00:00-23:59 GMT:  USE KEY MJ
    25 JAN 2036  00:00-23:59 GMT:  USE KEY MK
    26 JAN 2036  00:00-23:59 GMT:  USE KEY ML
    27 JAN 2036  00:00-23:59 GMT:  USE KEY MM
    28 JAN 2036  00:00-23:59 GMT:  USE KEY MN
    29 JAN 2036  00:00-23:59 GMT:  USE KEY MO
    30 JAN 2036  00:00-23:59 GMT:  USE KEY MP
    31 JAN 2036  00:00-23:59 GMT:  USE KEY MQ

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  B  B  -  B
03 1-0   -  -  -  C  C  -
04 1-0   -  D  D  D  D  -
05 0-3   -  -  -  -  -  -
06 0-3   F  -  -  -  F  F
07 0-3   G  G  -  -  G  -
08 0-4   H  -  -  -  -  -
09 0-4   I  I  -  I  -  -
10 0-4   -  -  -  -  -  -
11 0-4   K  K  K  -  -  K
12 0-4   -  -  L  -  -  -
13 0-4   -  M  M  -  M  M
14 0-5   N  -  N  N  -  N
15 0-5   -  O  -  O  -  -
16 0-5   -  P  -  -  P  P
17 0-5   -  Q  Q  Q  Q  -
18 0-5   -  -  -  R  R   
19 0-6   S  -  S  -  S   
20 1-4   T  -  T  -      
21 1-6   U  -  -  U      
22 2-6   V  -  -         
23 3-4   -  -  -         
24 3-5   -  Y            
25 3-5   Y  Z            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

KRKPV ASLTN UTKMJ ZFOFH PORUQ T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 2-0   B  -  B  -  -  B
03 2-0   C  C  C  C  C  -
04 2-0   D  D  -  D  -  D
05 2-0   -  E  E  -  E  E
06 2-0   -  F  F  F  -  -
07 2-0   -  -  -  -  G  G
08 2-0   H  -  H  -  -  H
09 2-0   I  -  I  I  -  -
10 0-4   -  -  -  -  J  -
11 0-4   K  -  K  K  -  -
12 0-4   L  L  L  -  L  L
13 0-4   -  -  M  M  -  M
14 0-4   -  N  -  -  N  -
15 0-4   O  O  -  -  O  O
16 0-4   P  P  -  -  P  P
17 0-4   Q  Q  -  Q  Q  -
18 0-4   -  R  R  -  R   
19 0-4   S  -  -  S  -   
20 0-4   T  -  T  T      
21 0-4   -  -  -  U      
22 0-4   -  -  V         
23 0-5   W  -  -         
24 0-5   X  Y            
25 0-5   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZJMMS EROSI MOSJA JHRCS BSNDM A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  -  -  -  -
03 1-0   -  -  -  C  -  C
04 1-0   D  -  D  D  D  -
05 2-0   -  -  -  -  -  E
06 2-0   F  -  F  -  -  -
07 2-0   G  G  G  G  -  -
08 2-0   H  H  -  H  -  H
09 2-0   I  -  -  I  -  I
10 2-0   J  -  J  -  J  J
11 2-0   -  -  -  K  K  -
12 2-0   -  L  L  -  L  -
13 0-3   -  M  -  -  M  M
14 0-3   -  -  N  N  -  -
15 0-3   O  O  O  -  O  O
16 0-3   P  P  P  -  P  -
17 0-3   -  -  Q  Q  Q  -
18 0-3   R  R  R  R  R   
19 0-3   -  -  -  S  S   
20 1-3   -  T  -  -      
21 1-6   -  U  U  -      
22 2-3   V  V  V         
23 2-3   -  X  -         
24 2-3   X  -            
25 2-3   -  Z            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZVRUO NTWZB SOGLG NRTSS GNNUO A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  A
02 2-0   B  B  -  B  -  -
03 0-3   -  C  C  -  C  C
04 0-3   D  D  -  -  -  -
05 0-3   E  -  -  -  -  -
06 0-3   F  F  -  F  F  -
07 0-3   -  G  G  -  G  G
08 0-5   -  -  H  -  -  H
09 0-5   -  -  -  -  I  I
10 0-5   J  -  -  -  -  -
11 0-5   -  -  K  K  -  K
12 0-5   L  L  L  -  -  -
13 0-6   M  M  M  M  M  M
14 0-6   -  -  -  N  -  -
15 0-6   O  O  -  O  O  -
16 0-6   -  -  P  P  P  -
17 0-6   -  Q  Q  -  Q  -
18 0-6   -  -  R  -  R   
19 0-6   S  S  -  -  -   
20 1-2   -  -  T  T      
21 2-5   U  -  -  U      
22 3-5   -  V  -         
23 3-6   W  -  -         
24 3-6   X  -            
25 3-6   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PHVRS HPPBL QDNVV MSVZL QKTKS S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 2-0   -  -  -  -  -  B
03 0-3   -  C  -  C  C  C
04 0-3   D  -  D  D  -  D
05 0-3   -  -  E  E  -  -
06 0-3   F  F  F  F  F  -
07 0-3   G  G  -  G  G  G
08 0-4   H  H  -  H  H  -
09 0-4   -  I  -  -  I  -
10 0-4   J  J  -  -  -  -
11 0-4   -  K  K  -  K  -
12 0-5   L  -  L  L  -  L
13 0-5   -  -  M  -  -  M
14 0-5   -  -  -  -  -  N
15 0-5   -  -  -  O  O  O
16 0-5   P  -  -  P  -  P
17 0-5   -  Q  Q  -  -  -
18 0-5   R  -  -  R  -   
19 0-5   S  S  S  -  S   
20 0-5   T  T  -  -      
21 0-5   U  U  U  U      
22 1-6   -  V  -         
23 3-4   -  X  X         
24 3-5   -  -            
25 3-5   Y  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

BBIZD QAVHL VHFZZ XVCQC QLVFG B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  B  -  B  B  -
03 1-0   -  -  -  C  C  -
04 1-0   D  -  -  D  D  D
05 1-0   -  -  -  -  -  E
06 1-0   -  -  -  -  F  F
07 2-0   -  G  G  G  G  G
08 0-3   -  -  H  -  H  H
09 0-3   I  -  -  -  I  -
10 0-3   -  -  J  -  -  J
11 0-3   K  K  K  -  -  K
12 0-3   -  L  L  L  -  L
13 0-3   M  M  M  -  -  -
14 0-3   -  -  N  -  -  -
15 0-6   O  -  -  O  O  -
16 0-6   -  -  -  P  -  P
17 0-6   Q  -  Q  -  -  -
18 1-2   -  R  R  R  -   
19 1-3   -  S  -  -  S   
20 1-3   T  T  -  T      
21 1-3   -  U  -  -      
22 1-3   -  V  V         
23 1-5   -  X  X         
24 2-5   -  -            
25 2-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

PQARW IDFPV TTTHS CSSET MWUXA L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  B  B  B  B  -
03 1-0   C  -  -  -  C  -
04 1-0   -  D  -  D  D  -
05 2-0   -  E  -  E  -  E
06 0-4   F  -  F  -  -  F
07 0-4   G  -  G  -  G  G
08 0-4   -  -  -  H  -  H
09 0-4   I  I  -  I  I  I
10 0-4   -  -  J  J  -  J
11 0-4   -  -  -  K  -  -
12 0-4   -  -  -  -  -  -
13 0-4   -  M  M  M  M  M
14 0-5   -  -  -  -  N  N
15 0-5   O  O  O  O  -  -
16 0-5   P  P  -  -  -  P
17 0-5   -  Q  Q  -  -  -
18 0-5   R  -  R  -  -   
19 0-5   S  -  -  -  S   
20 0-6   -  -  -  T      
21 0-6   U  U  -  -      
22 1-5   -  -  V         
23 1-6   W  X  -         
24 2-3   X  -            
25 2-6   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SLAJA ICVQQ RSUSO FZPJU MQEMA I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  B  B  B  B
03 1-0   C  C  C  C  -  -
04 1-0   -  -  -  -  D  -
05 1-0   E  E  -  E  -  E
06 2-0   -  F  -  F  -  F
07 2-0   G  G  G  -  -  -
08 2-0   -  -  -  H  H  H
09 2-0   -  -  I  I  I  -
10 2-0   J  -  J  J  J  -
11 0-3   K  K  -  -  -  K
12 0-5   L  L  L  -  L  -
13 0-5   -  -  M  -  M  M
14 0-5   N  -  -  N  -  N
15 0-5   O  O  -  -  O  O
16 0-5   -  P  P  P  P  -
17 0-5   -  -  -  -  Q  -
18 0-5   R  -  R  R  -   
19 0-6   S  -  S  S  S   
20 1-2   -  T  -  -      
21 1-5   -  U  -  U      
22 1-5   V  -  -         
23 1-5   -  -  -         
24 1-5   X  Y            
25 2-6   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

USNUP UQLNX JADQD XUZCP USZLR S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  B  -  -  B  B
03 2-0   -  C  -  -  C  -
04 2-0   D  D  -  D  D  -
05 2-0   E  -  -  E  E  E
06 2-0   F  -  F  F  -  F
07 2-0   -  G  G  -  G  -
08 2-0   -  H  -  H  -  -
09 2-0   -  I  I  I  -  -
10 2-0   -  -  -  -  J  J
11 0-4   K  K  K  -  -  K
12 0-4   -  -  -  L  -  L
13 0-4   M  -  -  M  M  M
14 0-4   N  -  N  N  -  -
15 0-5   O  O  O  -  O  -
16 0-5   P  -  -  P  -  -
17 0-5   -  -  Q  -  -  Q
18 0-5   R  -  -  R  R   
19 0-5   -  S  S  S  S   
20 0-5   -  T  T  -      
21 0-6   -  U  U  U      
22 1-3   -  V  V         
23 1-4   W  -  X         
24 2-5   -  -            
25 2-5   Y  -            
26 2-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UQEPK RYVHN NWXQZ DJXML PTOEI W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  -  -  -  -  B
03 1-0   -  -  C  C  -  -
04 1-0   D  -  D  D  -  -
05 1-0   -  E  E  E  E  -
06 1-0   -  F  -  -  -  F
07 0-3   G  -  -  -  -  -
08 0-3   H  -  -  H  -  -
09 0-3   -  I  I  I  I  -
10 0-3   J  -  J  J  -  J
11 0-5   -  K  -  -  K  K
12 0-5   L  -  L  L  L  L
13 0-5   -  -  -  M  M  -
14 0-6   N  N  N  -  -  N
15 0-6   O  O  O  -  O  O
16 0-6   P  P  -  -  -  -
17 0-6   Q  -  -  -  Q  -
18 1-3   -  R  -  R  -   
19 1-5   S  S  S  S  S   
20 2-3   T  -  -  T      
21 2-6   -  -  -  -      
22 3-6   V  -  V         
23 4-6   -  X  X         
24 5-6   -  Y            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AAZWT PIOUP HAAHN BPSAP VQOGA T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  B  -  B  B  B
03 1-0   C  -  -  -  C  -
04 1-0   -  -  D  -  -  -
05 1-0   -  -  -  -  E  E
06 1-0   -  F  F  -  -  -
07 1-0   -  G  G  G  G  G
08 0-3   H  H  H  -  H  H
09 0-3   -  I  I  -  -  I
10 0-3   J  -  J  -  J  J
11 0-4   -  K  -  -  K  K
12 0-4   -  L  L  L  -  -
13 0-4   -  -  M  M  -  -
14 0-4   N  N  -  N  N  -
15 0-5   O  O  -  -  -  O
16 0-6   -  -  -  -  -  -
17 0-6   Q  -  Q  Q  Q  -
18 0-6   -  -  R  R  -   
19 0-6   S  -  -  -  S   
20 0-6   -  T  T  -      
21 0-6   U  -  -  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

OKCKR NKTNN GQJUM TOPME KTIZT V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 2-0   B  B  B  B  B  B
03 2-0   -  -  C  -  -  -
04 2-0   D  -  -  -  D  -
05 2-0   -  E  E  E  E  -
06 2-0   F  F  -  F  F  F
07 2-0   G  G  G  -  G  G
08 2-0   -  -  H  H  -  H
09 2-0   -  -  -  I  -  I
10 2-0   J  -  J  -  J  -
11 2-0   -  K  -  K  -  -
12 0-3   L  -  L  -  L  L
13 0-3   M  -  -  M  M  -
14 0-3   N  N  N  N  -  N
15 0-4   -  O  -  -  -  -
16 0-5   P  -  P  P  -  P
17 0-5   -  Q  -  -  Q  -
18 0-5   R  R  -  -  R   
19 0-5   S  -  S  -  -   
20 0-5   -  -  -  -      
21 0-5   U  -  U  U      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 1-3   X  -            
25 1-6   -  Z            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

ALROZ OJRCK OYZLP MSAAO LRCFX Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   -  B  -  -  B  B
03 0-3   C  C  -  C  C  -
04 0-3   D  -  D  D  D  -
05 0-3   -  E  E  E  -  -
06 0-3   F  -  -  -  -  -
07 0-5   -  -  G  -  G  G
08 0-5   H  H  H  -  -  H
09 0-5   I  I  -  I  I  -
10 0-5   J  -  J  -  J  J
11 0-6   -  -  -  -  -  -
12 0-6   L  L  L  L  L  L
13 0-6   -  M  M  -  M  M
14 0-6   N  -  N  N  N  N
15 0-6   -  -  -  O  -  O
16 1-4   P  -  P  -  P  -
17 1-5   -  Q  -  -  -  -
18 1-5   R  -  -  R  -   
19 1-6   S  -  -  S  S   
20 2-4   T  -  -  -      
21 3-5   -  -  U  -      
22 3-5   -  V  V         
23 3-5   W  X  -         
24 3-5   -  -            
25 3-6   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

UUALV DDAPN QNDWZ AZSMA WWRSL O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 2-0   B  -  B  B  B  -
03 2-0   -  -  -  -  C  C
04 2-0   D  D  -  D  D  D
05 2-0   E  E  -  E  E  -
06 2-0   -  -  F  -  -  -
07 2-0   -  -  -  G  G  -
08 0-3   H  H  H  H  -  H
09 0-3   -  I  -  -  I  I
10 0-3   J  J  -  -  -  -
11 0-3   -  K  K  K  -  K
12 0-3   L  -  L  -  -  -
13 0-3   -  M  M  -  M  M
14 0-6   N  N  -  -  -  N
15 0-6   O  O  O  O  -  -
16 0-6   -  -  P  P  -  -
17 0-6   -  Q  Q  Q  -  Q
18 1-3   -  -  -  -  R   
19 1-5   S  S  S  -  S   
20 1-6   T  -  -  -      
21 2-3   U  U  U  -      
22 2-3   V  V  V         
23 2-3   W  -  X         
24 2-3   -  Y            
25 2-6   Y  Z            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

PNYUP PTWMA UNFAT ACSOQ ATPQW W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  B  B  -  B  -
03 1-0   C  C  -  C  C  C
04 1-0   -  D  D  -  -  D
05 2-0   E  -  -  E  E  E
06 2-0   F  -  -  F  F  F
07 2-0   G  G  -  -  -  G
08 2-0   -  H  H  -  H  -
09 2-0   -  I  -  -  -  I
10 2-0   -  J  -  -  -  J
11 0-3   K  -  -  K  K  K
12 0-3   L  L  L  L  -  -
13 0-3   M  M  -  M  M  M
14 0-3   -  -  N  N  -  -
15 0-3   O  -  -  O  -  -
16 0-3   -  -  P  -  P  -
17 0-4   Q  Q  Q  Q  Q  Q
18 1-2   -  -  -  R  R   
19 1-2   S  -  S  S  S   
20 1-2   T  T  T  -      
21 1-2   -  -  -  -      
22 1-3   V  -  V         
23 2-4   W  X  -         
24 2-6   X  -            
25 3-4   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MNWPN WUJXC CUNAO LLZMP PSTAA T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  A  -  -
02 0-3   -  -  -  B  -  B
03 0-3   C  -  -  -  C  C
04 0-3   -  D  -  -  D  -
05 0-3   E  -  E  E  -  E
06 0-3   -  -  F  -  F  F
07 0-3   G  G  G  -  -  G
08 0-4   -  H  H  H  -  H
09 0-4   -  -  -  -  -  I
10 0-4   J  J  -  J  -  -
11 0-4   -  -  -  K  -  K
12 0-5   L  L  L  -  L  -
13 0-5   M  -  M  -  M  M
14 0-5   N  N  -  N  -  -
15 0-5   O  O  O  O  O  -
16 0-5   P  P  -  P  P  P
17 0-5   -  -  -  -  Q  -
18 0-6   -  -  -  R  R   
19 0-6   -  S  S  -  -   
20 1-6   T  -  T  -      
21 2-3   U  -  U  -      
22 3-5   -  V  V         
23 3-5   W  X  X         
24 3-5   -  Y            
25 3-5   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PXIPM ASMPU XODSP PRPGS MLAQY P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  -  C  C  -
04 0-3   -  D  -  D  -  -
05 0-3   E  -  E  E  -  -
06 0-3   -  -  F  F  F  -
07 0-3   -  -  G  -  -  G
08 0-3   H  -  -  -  H  -
09 0-5   I  I  -  -  -  I
10 0-5   J  -  -  J  J  -
11 0-5   K  -  K  K  K  K
12 0-5   -  L  -  -  L  L
13 0-5   M  M  -  -  -  M
14 0-6   -  N  -  N  N  N
15 0-6   O  O  -  -  -  -
16 0-6   P  -  -  P  -  P
17 0-6   -  -  Q  Q  -  -
18 1-2   -  -  -  -  R   
19 1-6   S  S  S  S  -   
20 2-4   T  T  T  T      
21 2-5   U  -  U  -      
22 2-5   -  -  -         
23 3-5   W  X  X         
24 3-5   -  Y            
25 3-5   -  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

VGQTH VRTDS VKDVN ZJQVV SPJHJ Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 2-0   -  B  -  B  -  -
03 2-0   -  -  -  -  -  C
04 2-0   D  -  D  D  -  -
05 2-0   E  E  -  -  E  E
06 2-0   -  F  -  -  F  -
07 2-0   G  G  -  -  G  G
08 2-0   -  -  -  -  -  H
09 2-0   -  -  I  I  I  -
10 2-0   J  J  -  -  -  -
11 2-0   K  -  K  K  K  -
12 0-3   L  -  L  L  L  L
13 0-5   -  -  M  M  -  M
14 0-5   -  -  N  N  -  N
15 0-5   O  O  O  -  -  O
16 0-6   -  -  -  P  P  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   -  -  S  S  -   
20 0-6   T  -  T  -      
21 0-6   -  U  -  U      
22 0-6   V  -  V         
23 0-6   W  -  X         
24 2-6   X  -            
25 3-4   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RMTVP PXAHM CTQPO LPNPO XNNPG N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ME
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  B  -  B  -  -
03 1-0   C  C  C  C  -  C
04 1-0   -  -  -  D  -  -
05 2-0   E  E  E  E  E  -
06 2-0   -  -  F  -  -  -
07 2-0   -  G  -  G  -  G
08 2-0   H  -  H  H  -  -
09 2-0   -  -  I  -  I  -
10 2-0   -  J  -  J  J  J
11 0-3   K  -  -  K  K  K
12 0-6   -  L  L  -  -  L
13 0-6   M  -  M  -  M  -
14 0-6   N  N  -  -  -  N
15 0-6   O  -  -  -  -  O
16 0-6   -  -  -  -  -  P
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  -  S  S   
20 1-2   T  -  T  -      
21 1-6   -  -  -  -      
22 1-6   V  V  -         
23 1-6   -  -  -         
24 1-6   -  Y            
25 2-4   Y  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MMHOZ OUIAY RKKZB ROWUB UTWRQ L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   B  B  -  -  -  -
03 0-3   C  C  -  -  -  C
04 0-3   -  -  -  -  -  -
05 0-3   E  -  E  E  E  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  -  -  -  G
08 0-3   H  H  H  -  H  -
09 0-4   -  I  -  I  I  I
10 0-4   J  -  J  J  J  -
11 0-4   -  K  -  -  -  -
12 0-4   -  -  L  -  -  L
13 0-4   -  M  -  -  M  M
14 0-4   N  -  N  -  N  N
15 0-4   O  O  -  O  O  -
16 0-5   P  -  P  P  P  -
17 0-5   Q  -  -  -  -  Q
18 0-5   -  -  R  R  -   
19 0-5   S  S  S  S  S   
20 0-5   T  -  T  T      
21 0-6   U  -  -  U      
22 1-2   V  -  V         
23 1-3   -  X  -         
24 3-5   -  -            
25 4-5   -  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

KMXTS TUQYK JZNOH MKZOK MRIZN K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  B  -  -  B  -
03 1-0   -  -  C  C  C  C
04 1-0   D  D  D  -  -  D
05 1-0   E  E  E  E  E  -
06 1-0   F  F  F  -  -  F
07 0-4   -  G  G  G  -  G
08 0-4   H  -  -  -  -  H
09 0-4   -  -  -  -  I  -
10 0-4   -  J  J  J  -  J
11 0-4   -  K  -  -  -  -
12 0-6   L  -  L  -  L  -
13 0-6   M  -  M  M  -  -
14 0-6   N  -  -  N  N  -
15 0-6   O  -  -  O  O  O
16 0-6   P  -  P  -  P  P
17 0-6   -  Q  -  Q  -  Q
18 1-2   -  R  -  R  -   
19 1-4   -  S  -  S  -   
20 2-3   T  T  T  -      
21 2-6   -  U  U  U      
22 3-6   V  -  -         
23 4-6   -  X  -         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RJODA UABTM OQOTV TVCQC CUAZH S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 0-3   -  B  B  -  B  B
03 0-3   -  -  -  C  C  -
04 0-3   D  -  D  D  -  D
05 0-3   -  E  -  -  -  -
06 0-3   F  -  F  -  F  -
07 0-3   -  G  G  -  G  G
08 0-4   H  -  -  H  -  H
09 0-4   I  -  I  -  -  -
10 0-4   -  J  J  -  -  J
11 0-4   -  -  -  -  K  K
12 0-5   L  L  L  -  L  L
13 0-5   -  -  -  M  -  M
14 0-5   N  -  N  N  -  N
15 0-5   O  -  -  -  -  -
16 0-5   -  -  -  -  -  -
17 0-6   -  Q  -  -  Q  Q
18 0-6   R  R  -  R  -   
19 0-6   -  S  S  S  -   
20 1-5   -  T  -  T      
21 2-6   U  U  -  U      
22 3-4   -  V  V         
23 3-6   W  -  X         
24 4-5   -  -            
25 4-5   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZNGFB XOLZV OQKVO JUFVV FPLLJ F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  -
02 0-4   -  -  B  B  B  -
03 0-4   -  -  -  -  C  C
04 0-4   D  D  D  -  -  -
05 0-4   -  E  E  E  -  E
06 0-4   F  F  -  F  F  -
07 0-4   G  -  -  G  G  G
08 0-4   -  H  -  H  -  H
09 0-4   I  I  I  -  I  I
10 0-4   J  -  -  J  J  -
11 0-4   -  K  -  K  K  K
12 0-4   -  -  L  -  L  -
13 0-4   M  -  -  M  M  -
14 0-5   N  N  -  -  -  N
15 0-5   -  -  -  -  -  -
16 0-5   -  -  -  -  -  -
17 0-5   Q  -  Q  Q  -  Q
18 0-6   R  R  -  -  R   
19 0-6   -  S  S  -  -   
20 0-6   T  -  T  -      
21 0-6   U  -  -  -      
22 0-6   -  V  V         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 1-3   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

YFAKN BNABO SKZZA USNSS NOZIB Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 0-4   -  B  B  -  B  B
03 0-4   C  C  -  -  C  -
04 0-4   D  D  D  D  -  -
05 0-4   E  E  -  E  E  E
06 0-5   -  -  -  -  F  -
07 0-5   G  G  -  -  G  -
08 0-5   H  H  -  -  H  -
09 0-5   -  I  I  -  -  -
10 0-5   -  -  J  -  -  J
11 0-5   -  -  -  K  K  K
12 0-5   -  L  L  L  -  L
13 0-5   M  M  M  M  -  M
14 0-5   -  N  N  N  N  -
15 0-5   -  -  -  -  -  O
16 0-5   -  -  -  -  -  P
17 0-6   Q  Q  Q  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   -  -  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  -  -  -      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 1-3   X  Y            
25 1-4   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OOWAN ATBVT LKSTO OILWP FZPLM Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  -
02 0-3   -  B  B  -  -  -
03 0-3   C  C  C  -  -  -
04 0-3   -  D  -  D  -  -
05 0-3   E  E  E  E  -  -
06 0-3   -  -  -  -  F  -
07 0-4   G  G  -  -  G  G
08 0-4   -  -  H  H  H  H
09 0-4   -  I  -  -  I  -
10 0-5   J  -  J  -  -  J
11 0-5   -  K  K  K  -  -
12 0-5   L  -  -  -  L  L
13 0-5   M  M  M  -  M  M
14 0-5   -  N  N  -  N  N
15 0-5   O  O  -  O  -  O
16 1-2   P  -  P  -  -  P
17 1-4   -  Q  -  Q  Q  Q
18 1-6   R  R  -  R  R   
19 2-4   -  -  S  S  S   
20 2-4   T  T  -  T      
21 2-4   U  U  U  U      
22 2-5   V  -  -         
23 3-4   W  -  -         
24 3-4   -  -            
25 3-4   -  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

XIAGM AVZSZ ZMTVW OTRAA TNMUM A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ML
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  B  B  -  -
03 1-0   C  -  C  -  -  -
04 1-0   -  D  D  D  -  -
05 1-0   -  E  E  -  E  E
06 0-3   F  -  F  F  F  -
07 0-3   -  G  -  G  -  -
08 0-3   H  -  H  -  H  H
09 0-3   -  -  I  I  -  I
10 0-4   -  J  -  J  J  -
11 0-5   -  -  -  K  K  -
12 0-5   L  L  -  -  L  L
13 0-5   M  M  -  M  -  M
14 0-5   -  -  N  -  N  N
15 0-6   -  -  -  O  O  -
16 0-6   -  P  P  P  P  -
17 0-6   -  Q  -  Q  -  Q
18 0-6   -  -  R  -  -   
19 0-6   S  S  -  -  S   
20 0-6   T  T  -  -      
21 0-6   -  U  U  -      
22 1-3   -  -  V         
23 1-6   W  -  -         
24 1-6   -  Y            
25 2-4   Y  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MJVWT AJYKK CTTRR VHMAQ JHOAL I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  A
02 2-0   B  -  B  -  B  B
03 2-0   C  C  C  -  C  -
04 0-3   -  -  -  -  -  D
05 0-3   E  E  -  E  E  E
06 0-3   -  F  F  F  -  -
07 0-3   -  -  -  -  -  -
08 0-4   -  H  -  H  -  H
09 0-5   -  I  -  -  -  -
10 0-5   J  J  -  J  -  J
11 0-5   K  K  K  -  K  -
12 0-5   -  -  -  L  L  -
13 0-6   M  M  M  M  M  M
14 0-6   N  -  -  N  N  -
15 0-6   -  O  O  O  -  O
16 0-6   -  -  -  P  P  -
17 0-6   Q  Q  -  -  -  -
18 0-6   R  -  R  R  -   
19 0-6   -  -  S  -  -   
20 1-6   T  T  T  -      
21 2-4   -  -  -  -      
22 2-5   -  V  -         
23 3-5   -  X  X         
24 3-6   -  -            
25 3-6   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KQWBV OIQQP VQOGN ORZKV TWKLS Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   -  B  B  B  -  B
03 1-0   C  C  -  C  -  -
04 1-0   -  -  -  D  -  D
05 1-0   E  E  -  -  E  E
06 1-0   -  F  -  -  -  F
07 0-4   G  -  G  G  G  G
08 0-4   H  -  -  -  -  -
09 0-4   -  -  I  -  I  -
10 0-6   J  -  -  -  J  -
11 0-6   K  -  -  K  -  -
12 0-6   L  -  L  -  L  L
13 0-6   -  M  -  -  -  -
14 0-6   N  N  N  -  N  -
15 0-6   O  O  -  O  O  -
16 1-4   -  -  P  P  -  P
17 1-4   Q  -  -  Q  Q  Q
18 1-4   R  R  -  -  R   
19 1-4   -  S  S  -  S   
20 1-6   -  T  -  T      
21 2-4   -  -  U  -      
22 3-5   V  -  V         
23 4-5   -  X  X         
24 4-5   X  -            
25 4-5   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

GRUNL NWNTL TUUSF SGQSN KQWUU A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  B  B  B  B  -
03 1-0   C  -  C  -  -  C
04 1-0   -  -  D  -  D  -
05 1-0   E  E  -  -  -  E
06 0-3   F  F  -  F  -  F
07 0-4   -  G  G  G  -  -
08 0-4   H  -  H  -  H  -
09 0-5   -  I  -  -  -  -
10 0-5   -  -  J  J  J  J
11 0-5   -  -  K  K  K  K
12 0-5   -  -  -  L  L  -
13 0-5   M  -  -  M  M  M
14 0-5   N  -  -  N  -  -
15 0-6   O  O  -  O  O  O
16 0-6   -  P  P  -  P  P
17 0-6   -  -  Q  -  -  Q
18 0-6   -  -  -  -  R   
19 0-6   -  -  S  -  -   
20 1-5   T  -  -  T      
21 1-5   -  -  U  U      
22 1-5   V  V  V         
23 1-5   W  X  X         
24 1-6   X  -            
25 2-3   -  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UOLMV JVPPT OYSMD USTTV PJTYJ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  B  -  B  -  -
03 1-0   -  C  -  C  C  -
04 1-0   D  D  -  -  -  -
05 1-0   -  -  E  E  E  -
06 1-0   F  -  F  -  F  F
07 1-0   G  G  -  G  -  G
08 1-0   H  -  -  H  -  -
09 1-0   -  -  -  I  -  I
10 1-0   -  -  -  -  -  -
11 1-0   K  K  -  K  K  -
12 1-0   L  L  L  L  L  L
13 2-0   M  M  M  M  -  M
14 2-0   -  -  -  -  N  N
15 2-0   O  -  O  O  -  -
16 2-0   P  P  -  -  P  -
17 0-3   -  Q  -  Q  -  -
18 0-4   -  -  -  -  R   
19 0-4   -  -  -  -  -   
20 0-4   T  -  T  -      
21 0-4   -  -  U  -      
22 0-4   V  V  V         
23 0-4   W  -  X         
24 0-4   -  -            
25 0-5   -  Z            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

TGNBT ZOSCB AXGNS JSXOJ IASYC T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JAN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   B  -  -  B  -  -
03 1-0   -  -  C  C  C  -
04 1-0   D  -  D  -  D  -
05 1-0   -  -  -  -  E  E
06 1-0   F  F  F  F  -  F
07 1-0   -  -  G  -  G  -
08 1-0   -  -  -  H  -  -
09 0-4   -  -  -  I  -  I
10 0-4   -  J  -  -  -  -
11 0-4   -  K  -  -  K  -
12 0-4   L  -  L  L  -  -
13 0-4   -  M  -  -  -  M
14 0-4   N  N  N  N  N  -
15 0-5   -  O  O  -  -  O
16 0-5   P  -  P  -  -  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   -  R  -  R  R   
19 0-5   -  -  -  -  -   
20 1-3   T  T  T  T      
21 1-5   U  -  -  U      
22 1-5   V  V  V         
23 1-5   W  X  X         
24 1-5   X  Y            
25 2-4   Y  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

OIMCV ATVVO RATAZ BVHJA IMVCU S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
