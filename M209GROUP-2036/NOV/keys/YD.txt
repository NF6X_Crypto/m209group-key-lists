EFFECTIVE PERIOD:
25-NOV-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   -  B  B  B  B  -
03 2-0   -  -  -  C  C  C
04 2-0   D  -  D  -  -  D
05 2-0   E  E  E  -  -  -
06 0-4   -  F  -  -  -  -
07 0-4   -  -  G  -  G  -
08 0-4   -  -  H  H  -  H
09 0-5   -  -  I  -  I  -
10 0-5   J  J  -  J  J  -
11 0-5   K  -  -  -  -  K
12 0-6   L  L  L  -  -  L
13 0-6   -  M  -  M  M  -
14 0-6   -  N  -  N  N  -
15 0-6   -  O  -  -  -  O
16 1-3   -  -  P  P  P  P
17 1-4   -  Q  Q  Q  -  -
18 1-5   R  R  R  R  -   
19 1-5   S  S  S  S  -   
20 1-5   -  -  T  T      
21 2-5   U  U  -  U      
22 2-5   -  -  -         
23 2-5   -  X  X         
24 2-5   X  Y            
25 2-6   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

TUSTA OQKTX AJJAS OWRLZ SVXQT Q
-------------------------------
