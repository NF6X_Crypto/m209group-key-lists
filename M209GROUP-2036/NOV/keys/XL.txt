EFFECTIVE PERIOD:
07-NOV-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 2-0   B  B  -  -  -  B
03 2-0   -  -  C  C  C  -
04 2-0   D  D  D  -  -  D
05 2-0   -  E  E  E  -  -
06 2-0   F  -  F  -  -  -
07 2-0   G  G  G  G  G  G
08 2-0   H  H  -  -  H  H
09 2-0   -  -  -  I  I  -
10 0-3   -  -  -  -  J  -
11 0-4   K  -  -  K  -  -
12 0-4   L  -  L  L  L  L
13 0-4   -  -  M  -  M  M
14 0-4   N  N  -  N  -  N
15 0-4   -  O  -  -  -  -
16 0-4   P  -  -  -  -  P
17 0-4   -  Q  Q  Q  Q  -
18 0-4   R  -  -  -  R   
19 0-4   -  -  -  S  -   
20 0-4   T  -  -  -      
21 0-5   -  -  -  -      
22 0-5   -  -  V         
23 0-5   -  X  -         
24 2-4   -  Y            
25 2-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OQMCL FPBMU GUMNQ ZHYMQ QCOZP Y
-------------------------------
