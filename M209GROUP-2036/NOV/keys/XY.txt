EFFECTIVE PERIOD:
20-NOV-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  B  -  B  -
03 1-0   -  -  -  -  -  -
04 1-0   D  D  D  D  -  D
05 2-0   E  -  -  E  -  -
06 2-0   F  F  -  -  F  F
07 2-0   -  -  -  -  -  G
08 2-0   H  H  -  H  -  H
09 2-0   I  I  -  I  -  -
10 2-0   J  J  -  -  J  -
11 0-5   -  -  K  -  -  K
12 0-5   L  L  L  -  -  -
13 0-5   -  M  M  M  M  M
14 0-6   N  -  -  N  N  N
15 0-6   -  O  -  O  O  O
16 0-6   -  P  -  -  -  -
17 0-6   -  Q  Q  -  -  -
18 1-4   -  -  R  -  R   
19 1-4   S  -  S  -  S   
20 1-6   -  -  T  -      
21 1-6   U  U  U  U      
22 1-6   -  -  -         
23 1-6   W  X  X         
24 2-5   X  Y            
25 2-6   -  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

TOWUJ KMUWO PSMQV JTQRV AQWLO L
-------------------------------
