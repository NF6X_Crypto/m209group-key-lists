EFFECTIVE PERIOD:
21-NOV-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  -  B  B  -  B
03 1-0   C  -  -  C  C  -
04 1-0   -  -  D  -  D  D
05 1-0   -  E  E  E  -  E
06 1-0   F  F  -  -  -  -
07 2-0   -  -  G  G  G  G
08 0-3   -  -  H  H  H  -
09 0-3   -  I  I  -  -  I
10 0-4   J  J  -  -  J  J
11 0-4   -  -  -  -  -  -
12 0-4   -  L  -  -  L  -
13 0-4   M  -  -  -  M  M
14 0-4   N  N  -  N  -  N
15 0-5   -  -  O  -  O  -
16 0-5   -  P  P  P  P  -
17 0-5   Q  Q  -  Q  Q  -
18 0-5   R  R  R  -  -   
19 0-5   -  S  S  -  S   
20 0-5   T  -  -  -      
21 0-6   U  -  -  U      
22 1-3   V  -  -         
23 1-4   -  X  -         
24 2-3   X  Y            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

IZMRZ THJSJ ZZZTM RUMDL TZQQA N
-------------------------------
