EFFECTIVE PERIOD:
30-NOV-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   -  -  -  B  -  B
03 1-0   C  C  C  C  C  -
04 1-0   D  D  -  D  -  D
05 2-0   E  E  -  E  E  E
06 2-0   F  -  -  -  -  -
07 0-5   G  -  -  -  G  -
08 0-5   H  H  H  -  H  -
09 0-5   -  -  -  I  -  I
10 0-5   J  J  -  -  J  -
11 0-5   -  -  K  K  -  K
12 0-6   L  L  L  L  -  L
13 0-6   -  -  -  M  M  -
14 0-6   N  N  N  N  -  N
15 0-6   O  O  -  -  O  -
16 0-6   P  P  P  -  -  -
17 0-6   Q  -  Q  Q  Q  -
18 1-2   -  R  -  R  R   
19 1-6   -  -  S  S  S   
20 2-4   -  T  -  -      
21 2-5   -  U  U  -      
22 3-5   -  -  -         
23 4-5   W  -  -         
24 5-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VAAWB PPRXF TQMKU AFYTB ACUAU A
-------------------------------
