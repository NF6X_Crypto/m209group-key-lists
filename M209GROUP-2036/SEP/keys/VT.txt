EFFECTIVE PERIOD:
24-SEP-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  -  -  -  B  B
03 1-0   -  C  -  -  -  C
04 1-0   D  -  -  -  D  -
05 1-0   E  E  -  E  -  -
06 2-0   -  -  -  -  F  -
07 2-0   -  -  G  -  G  G
08 2-0   H  -  -  -  -  H
09 2-0   I  -  -  I  -  -
10 0-3   J  -  J  J  -  -
11 0-3   -  K  K  K  -  K
12 0-6   -  -  -  L  L  -
13 0-6   M  -  M  -  -  M
14 0-6   N  N  -  -  -  N
15 0-6   O  O  -  O  O  -
16 0-6   -  P  P  -  -  -
17 0-6   -  -  Q  -  -  Q
18 1-2   R  R  -  R  R   
19 1-2   S  S  S  S  S   
20 1-2   -  T  T  T      
21 1-2   U  -  -  -      
22 1-3   -  V  V         
23 1-4   W  X  X         
24 1-5   -  -            
25 2-6   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

QIWAI UNYUW QALST QEJMW WIFUX A
-------------------------------
