EFFECTIVE PERIOD:
26-SEP-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  A  -  A
02 0-3   -  B  B  -  -  B
03 0-3   -  -  -  -  C  -
04 0-3   -  -  -  D  D  -
05 0-4   E  E  -  -  -  -
06 0-4   F  F  -  -  F  -
07 0-4   G  -  -  -  -  -
08 0-4   H  H  H  H  -  H
09 0-5   I  -  -  -  I  I
10 0-5   J  -  -  J  -  J
11 0-5   -  -  -  -  K  -
12 0-5   -  -  -  L  L  L
13 0-5   -  M  -  -  -  M
14 0-6   -  N  N  N  -  -
15 0-6   -  -  O  O  -  O
16 0-6   P  P  P  P  -  -
17 0-6   Q  Q  -  -  Q  -
18 1-2   -  -  -  -  R   
19 2-4   -  S  S  -  S   
20 2-6   -  -  T  T      
21 2-6   U  U  U  U      
22 3-5   V  V  -         
23 3-6   -  -  X         
24 3-6   -  -            
25 3-6   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

PVVFL PSAPO SSIGJ LWJWR GJFRA U
-------------------------------
