EFFECTIVE PERIOD:
29-SEP-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  -  B  -  -  -
03 1-0   C  C  -  C  -  C
04 2-0   D  D  D  D  -  D
05 2-0   -  E  E  E  -  -
06 2-0   F  F  F  F  -  -
07 2-0   G  G  G  -  G  G
08 0-3   H  H  -  -  H  H
09 0-4   I  -  I  -  -  I
10 0-4   -  -  J  J  J  -
11 0-4   -  -  -  K  K  K
12 0-4   -  L  L  -  L  -
13 0-4   M  M  -  M  -  -
14 0-4   N  -  -  N  N  N
15 0-4   -  O  O  -  -  O
16 0-4   -  -  -  -  -  P
17 0-5   -  Q  Q  -  -  Q
18 0-6   R  R  -  R  R   
19 0-6   S  -  -  S  -   
20 0-6   -  -  -  T      
21 0-6   -  -  U  -      
22 1-2   -  -  -         
23 1-3   W  X  X         
24 2-6   -  Y            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VMKRH PQQGW HVPZA PKRWR HSUQF G
-------------------------------
