EFFECTIVE PERIOD:
18-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  -  -  -  B
03 1-0   C  -  C  -  C  C
04 1-0   D  -  -  -  -  -
05 1-0   E  E  -  E  E  E
06 2-0   -  -  F  -  -  F
07 2-0   -  G  G  G  -  G
08 2-0   H  -  H  H  H  -
09 2-0   I  I  I  -  I  I
10 2-0   -  -  J  J  J  -
11 2-0   K  -  -  K  -  -
12 0-3   -  -  -  -  L  -
13 0-3   -  -  -  -  -  M
14 0-3   N  -  N  N  N  N
15 0-3   -  O  O  O  O  O
16 0-3   P  P  -  -  -  -
17 0-3   -  -  -  -  Q  -
18 1-2   -  R  R  -  -   
19 1-3   S  S  -  S  -   
20 1-3   T  -  -  T      
21 1-3   U  U  -  -      
22 1-3   V  -  V         
23 2-3   W  -  -         
24 2-5   X  -            
25 3-4   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AUDTA LSMTB VKVMJ AOVSA BMCMU K
-------------------------------
