EFFECTIVE PERIOD:
09-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   -  -  -  -  -  B
03 2-0   -  C  -  -  -  C
04 2-0   -  D  -  D  -  -
05 0-3   -  -  -  E  E  -
06 0-3   F  F  F  -  -  F
07 0-3   -  G  -  -  -  -
08 0-3   H  -  -  H  H  -
09 0-4   I  I  I  I  I  I
10 0-4   J  J  J  -  J  -
11 0-4   -  -  -  K  K  -
12 0-4   L  -  -  -  L  L
13 0-4   -  M  M  M  -  M
14 0-4   N  -  N  -  -  N
15 0-4   -  O  -  O  -  O
16 1-2   P  -  P  P  -  -
17 1-2   Q  Q  -  Q  -  Q
18 1-2   -  R  R  R  -   
19 1-3   S  S  S  S  S   
20 1-3   T  T  -  -      
21 1-3   U  U  U  -      
22 1-3   V  V  V         
23 1-5   -  -  X         
24 1-6   -  -            
25 2-4   Y  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

AKYWA AWSLX IZLPH LWQPA OKOYT Y
-------------------------------
