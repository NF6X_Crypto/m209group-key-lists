EFFECTIVE PERIOD:
30-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  -
02 2-0   -  B  B  -  -  -
03 2-0   -  -  C  -  C  C
04 2-0   D  D  D  D  D  D
05 0-3   E  E  -  E  E  E
06 0-3   F  F  -  -  F  F
07 0-3   G  G  G  -  G  -
08 0-3   -  -  -  H  -  H
09 0-3   I  I  -  I  -  -
10 0-3   -  J  J  J  -  J
11 0-5   -  -  K  -  -  -
12 0-5   -  -  -  L  L  L
13 0-5   M  -  -  M  M  M
14 0-5   N  -  N  -  N  -
15 0-5   -  O  -  O  -  O
16 1-2   P  P  -  P  -  -
17 2-4   Q  -  Q  -  -  Q
18 2-4   R  R  R  -  -   
19 2-4   S  S  S  S  S   
20 2-4   -  -  T  -      
21 2-5   U  U  -  -      
22 2-5   V  -  -         
23 2-5   W  X  -         
24 2-5   -  -            
25 3-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ATQVV IVKMA SRAVT MVOFA APZNG M
-------------------------------
