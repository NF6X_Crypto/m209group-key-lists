EFFECTIVE PERIOD:
21-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  B  B  -  -
03 1-0   -  -  C  -  -  -
04 2-0   -  -  -  D  -  D
05 0-3   E  -  -  -  E  -
06 0-3   F  F  F  -  F  F
07 0-3   G  G  G  -  G  G
08 0-3   -  -  H  H  -  H
09 0-3   -  -  -  I  -  I
10 0-4   -  -  J  -  J  J
11 0-4   -  K  K  K  K  -
12 0-4   L  L  -  L  L  -
13 0-4   M  M  -  -  M  -
14 0-4   N  N  -  N  -  -
15 0-4   O  -  O  O  O  O
16 0-4   -  P  -  P  -  -
17 0-4   Q  Q  -  -  -  -
18 0-4   -  -  R  -  R   
19 0-5   -  S  -  S  -   
20 0-6   -  -  -  T      
21 0-6   -  U  U  U      
22 0-6   -  V  -         
23 0-6   W  X  -         
24 1-5   X  -            
25 1-6   Y  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

BWKLM ZMZQE THZHH PZWBR RGQPZ F
-------------------------------
