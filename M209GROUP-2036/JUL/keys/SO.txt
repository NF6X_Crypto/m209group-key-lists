EFFECTIVE PERIOD:
03-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  -  B  B  -  B
03 1-0   -  C  C  -  -  -
04 1-0   D  D  -  D  D  D
05 1-0   E  E  -  -  -  -
06 1-0   F  -  F  F  -  F
07 0-4   -  -  G  -  -  G
08 0-4   H  H  H  H  H  H
09 0-4   -  -  -  -  I  -
10 0-4   -  J  J  -  J  J
11 0-4   K  -  -  -  K  K
12 0-4   -  L  -  -  L  -
13 0-5   -  M  -  M  -  -
14 0-5   -  -  N  -  -  -
15 0-5   -  O  -  -  O  O
16 0-5   -  -  P  P  -  -
17 0-6   -  Q  Q  -  Q  -
18 1-4   R  R  R  R  R   
19 1-5   S  S  -  S  S   
20 1-5   T  -  -  T      
21 1-5   U  -  -  -      
22 1-5   V  -  -         
23 2-5   -  X  -         
24 3-5   -  -            
25 3-6   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UZNZQ YYSNZ LLVEV SANRL AUKCS N
-------------------------------
