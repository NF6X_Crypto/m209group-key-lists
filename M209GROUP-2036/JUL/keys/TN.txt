EFFECTIVE PERIOD:
28-JUL-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  -  B  B  B  -
03 1-0   -  -  C  -  -  C
04 2-0   D  D  -  D  -  -
05 2-0   -  E  E  E  -  -
06 2-0   F  -  -  F  F  F
07 2-0   G  G  G  G  -  -
08 2-0   -  -  H  H  H  -
09 2-0   -  I  -  -  -  I
10 2-0   -  -  -  -  J  -
11 2-0   -  K  -  K  -  K
12 0-3   L  -  -  -  -  L
13 0-4   M  -  -  -  M  -
14 0-4   N  N  N  N  -  N
15 0-6   -  -  O  O  -  -
16 0-6   -  -  P  P  P  P
17 0-6   Q  -  -  -  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   S  S  S  -  -   
20 1-4   -  T  -  T      
21 1-6   U  U  -  -      
22 2-5   V  -  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 2-6   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

LSEZM YWNRA RTOEV WIYJQ KMQSA B
-------------------------------
