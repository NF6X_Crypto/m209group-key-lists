EFFECTIVE PERIOD:
19-JUN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  B  -  -  B  -
03 1-0   -  -  -  -  -  -
04 1-0   -  D  D  -  -  -
05 1-0   E  E  E  -  E  -
06 1-0   F  -  -  F  -  F
07 0-3   -  G  G  G  -  -
08 0-3   H  H  -  -  H  H
09 0-3   -  I  I  -  -  I
10 0-3   -  J  J  J  -  J
11 0-3   -  K  -  K  -  K
12 0-6   -  -  L  L  -  L
13 0-6   -  -  -  -  -  -
14 0-6   -  -  N  -  N  -
15 0-6   O  O  O  -  O  -
16 0-6   P  P  P  P  P  -
17 0-6   Q  Q  Q  -  -  Q
18 1-2   -  R  -  -  R   
19 1-3   S  -  S  S  S   
20 1-3   T  -  -  T      
21 1-3   -  U  U  -      
22 1-3   V  -  -         
23 1-4   W  X  -         
24 1-5   X  -            
25 2-5   Y  -            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

AANUN JVAON SCVJT VZJAN DNBSR U
-------------------------------
