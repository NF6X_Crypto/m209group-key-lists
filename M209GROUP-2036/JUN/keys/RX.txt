EFFECTIVE PERIOD:
16-JUN-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   -  -  -  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   D  -  D  D  -  -
05 1-0   -  E  -  -  E  -
06 1-0   F  -  F  -  F  F
07 1-0   -  -  -  G  G  G
08 1-0   -  -  H  H  H  H
09 1-0   I  I  I  -  -  -
10 1-0   -  J  -  J  -  J
11 2-0   K  K  -  K  -  -
12 0-3   -  L  -  L  -  -
13 0-3   -  -  -  -  -  M
14 0-3   N  -  N  -  -  N
15 0-3   O  -  O  -  O  O
16 0-3   -  P  P  -  P  -
17 0-3   Q  -  -  -  Q  Q
18 0-4   -  R  R  R  R   
19 0-5   S  -  -  S  -   
20 0-6   T  T  -  T      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   W  -  X         
24 1-3   -  -            
25 2-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HTMTY PUZAX OZIYS MRHIT ISDOY U
-------------------------------
