EFFECTIVE PERIOD:
12-FEB-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 0-3   -  B  B  -  -  B
03 0-3   C  C  C  C  C  -
04 0-3   -  -  D  -  -  D
05 0-3   E  -  -  E  -  E
06 0-3   F  -  F  -  F  -
07 0-3   -  G  -  G  -  -
08 0-3   -  H  H  H  -  H
09 0-3   -  -  -  -  -  -
10 0-3   J  J  J  -  J  -
11 0-3   K  -  -  -  K  -
12 0-4   -  -  L  L  L  -
13 0-4   -  -  M  M  M  M
14 0-4   N  N  -  -  -  -
15 0-4   O  O  -  -  O  -
16 0-4   -  P  -  -  P  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-6   R  -  R  R  R   
19 0-6   S  S  -  S  -   
20 0-6   T  -  -  T      
21 0-6   -  -  -  -      
22 0-6   -  V  -         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-2   -               
27 2-4                   
-------------------------------
26 LETTER CHECK

IDKLS GOPLG LZMSH QGNDS IQKIL Q
-------------------------------
