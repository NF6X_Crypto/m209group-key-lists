EFFECTIVE PERIOD:
29-FEB-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  B  -  -  B
03 0-3   C  -  -  -  C  C
04 0-3   D  -  D  D  -  -
05 0-3   -  E  E  E  -  E
06 0-3   -  -  F  -  F  -
07 0-3   G  G  -  -  G  -
08 0-3   -  -  H  -  -  H
09 0-4   I  -  -  -  -  I
10 0-4   J  -  J  J  J  -
11 0-4   -  -  K  K  K  -
12 0-4   L  L  -  L  L  L
13 0-4   -  M  -  -  M  -
14 0-4   -  N  N  -  -  N
15 0-6   -  O  O  -  -  O
16 0-6   P  P  -  P  -  P
17 0-6   Q  Q  Q  Q  -  Q
18 1-3   -  -  -  -  -   
19 1-4   -  -  -  -  S   
20 1-5   -  T  T  T      
21 2-5   -  U  U  U      
22 3-5   -  V  -         
23 3-6   W  -  X         
24 3-6   -  Y            
25 3-6   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QUAWO IQXOU CULXU QZQRX AHNQU P
-------------------------------
