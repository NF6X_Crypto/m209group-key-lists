EFFECTIVE PERIOD:
28-FEB-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  B  -  -  -
03 1-0   -  C  -  C  -  -
04 1-0   D  -  D  D  -  D
05 2-0   -  E  -  -  -  -
06 2-0   -  -  F  F  F  -
07 2-0   -  -  G  G  -  G
08 2-0   H  H  -  H  H  -
09 2-0   I  I  I  I  -  -
10 2-0   J  -  J  J  J  J
11 2-0   K  K  -  K  K  K
12 0-6   -  -  L  -  -  -
13 0-6   -  -  -  M  M  M
14 0-6   -  N  N  -  -  -
15 0-6   O  O  O  O  O  O
16 1-2   P  P  P  P  -  -
17 1-2   Q  Q  Q  -  Q  -
18 1-2   R  R  -  R  R   
19 1-2   -  -  -  -  S   
20 1-4   T  -  -  -      
21 1-5   -  -  U  -      
22 1-5   -  V  -         
23 1-6   W  X  -         
24 2-6   -  -            
25 3-4   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWARR FDALZ GVOVN RJLWA NADCW W
-------------------------------
