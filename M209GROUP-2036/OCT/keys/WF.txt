EFFECTIVE PERIOD:
06-OCT-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  -  B  B  B
03 1-0   C  -  C  -  -  C
04 1-0   -  D  D  -  -  -
05 1-0   E  -  E  -  -  -
06 1-0   F  F  F  -  -  F
07 2-0   G  -  -  G  -  G
08 2-0   -  H  H  -  H  -
09 2-0   I  I  I  -  I  -
10 0-5   -  -  J  J  -  J
11 0-5   K  -  -  K  -  K
12 0-5   -  L  -  L  L  L
13 0-5   -  M  M  M  M  -
14 0-5   -  -  N  N  N  -
15 0-5   -  O  -  -  -  -
16 1-2   P  -  -  P  -  P
17 1-2   -  -  -  -  Q  -
18 1-2   R  -  -  R  -   
19 1-2   S  S  S  S  -   
20 1-5   -  T  T  T      
21 2-3   -  -  U  -      
22 2-4   V  -  -         
23 2-5   W  -  X         
24 2-5   -  Y            
25 2-6   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AMTLL UNNLA LLOOM WAONA KUNMK N
-------------------------------
