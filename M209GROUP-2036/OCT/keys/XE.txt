EFFECTIVE PERIOD:
31-OCT-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   -  B  -  B  -  -
03 1-0   C  -  C  -  C  -
04 1-0   -  D  D  D  D  D
05 1-0   E  E  E  -  -  -
06 1-0   -  -  -  F  -  F
07 1-0   -  G  -  G  G  -
08 1-0   H  H  H  -  H  H
09 1-0   -  I  -  -  -  I
10 1-0   J  J  -  J  J  -
11 2-0   K  -  -  K  -  -
12 2-0   L  L  -  -  -  L
13 0-3   -  -  -  -  -  -
14 0-3   -  -  N  N  N  -
15 0-3   O  -  O  O  O  -
16 0-3   -  P  P  P  P  P
17 0-3   -  -  -  Q  Q  Q
18 0-3   R  R  -  -  R   
19 0-4   S  -  S  -  -   
20 0-4   T  T  T  T      
21 0-5   U  -  -  -      
22 1-3   V  V  V         
23 1-3   W  -  -         
24 2-3   -  Y            
25 2-4   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

IPYUR YVSIS YGPSQ UZOTG POUOO Y
-------------------------------
