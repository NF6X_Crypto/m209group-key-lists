EFFECTIVE PERIOD:
24-AUG-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  A
02 2-0   B  -  B  B  -  B
03 0-3   -  C  -  -  -  C
04 0-4   D  -  D  D  D  D
05 0-4   E  -  E  E  E  E
06 0-4   F  -  F  -  F  F
07 0-4   G  -  G  -  -  -
08 0-4   -  -  -  -  H  -
09 0-4   -  I  -  I  I  -
10 0-4   J  -  J  J  -  -
11 0-5   K  -  K  K  K  K
12 0-5   L  -  L  -  L  -
13 0-5   M  -  M  -  -  M
14 0-5   -  N  -  N  N  N
15 0-6   -  -  O  O  -  O
16 0-6   -  P  P  -  P  -
17 0-6   -  -  Q  Q  Q  -
18 0-6   R  R  -  -  -   
19 0-6   -  S  S  -  -   
20 1-4   -  T  -  T      
21 2-3   -  -  -  U      
22 2-6   -  V  -         
23 4-5   -  X  -         
24 4-5   -  Y            
25 4-5   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OQYKZ NZVVK SQWVC LLSQA JRNQS U
-------------------------------
