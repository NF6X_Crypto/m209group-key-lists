EFFECTIVE PERIOD:
30-AUG-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   B  B  B  B  -  B
03 0-3   -  -  -  -  C  -
04 0-3   -  -  D  -  D  D
05 0-3   -  E  E  E  -  E
06 0-5   F  F  F  F  -  F
07 0-5   G  G  G  -  G  -
08 0-5   -  H  H  H  -  -
09 0-5   I  I  -  -  -  I
10 0-5   -  J  -  -  J  -
11 0-5   -  -  -  -  K  K
12 0-5   L  -  -  L  L  L
13 0-5   -  -  -  -  -  M
14 0-5   -  -  N  -  -  -
15 0-5   O  -  -  -  -  -
16 0-5   P  P  P  P  -  -
17 0-5   -  Q  Q  Q  -  -
18 0-6   R  R  R  -  R   
19 0-6   S  S  S  S  S   
20 0-6   T  T  T  T      
21 0-6   U  -  -  U      
22 0-6   -  -  V         
23 0-6   W  X  -         
24 2-4   -  Y            
25 3-4   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YJOHH HSWND VXKFS NHAVW CZAEJ Z
-------------------------------
