EFFECTIVE PERIOD:
21-AUG-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  -
02 2-0   B  B  -  B  -  -
03 2-0   C  C  -  -  C  -
04 2-0   -  D  D  -  -  D
05 2-0   E  -  E  -  -  -
06 2-0   -  F  F  F  -  -
07 0-3   -  G  -  G  -  -
08 0-4   H  -  H  -  H  H
09 0-4   I  -  I  -  I  I
10 0-4   J  J  J  -  J  J
11 0-4   K  -  K  K  -  -
12 0-4   L  -  -  -  -  -
13 0-4   -  -  -  M  M  -
14 0-4   N  -  N  N  -  N
15 0-5   -  O  O  O  O  O
16 0-5   P  -  P  -  -  -
17 0-5   Q  -  -  -  -  Q
18 1-2   -  R  -  R  R   
19 2-3   -  -  S  -  -   
20 2-4   -  T  T  T      
21 2-4   U  U  U  U      
22 2-4   -  -  -         
23 2-4   W  X  -         
24 2-6   -  Y            
25 3-5   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

XTPSB XTQYU RZMPX USTEQ TOQQE T
-------------------------------
