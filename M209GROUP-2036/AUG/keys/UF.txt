EFFECTIVE PERIOD:
15-AUG-2036 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   -  -  -  B  -  B
03 1-0   C  -  C  -  -  C
04 1-0   -  D  D  D  D  D
05 1-0   -  -  E  -  -  -
06 1-0   -  F  -  F  -  F
07 1-0   G  -  G  -  G  -
08 1-0   H  H  -  -  H  -
09 1-0   I  I  -  I  I  -
10 2-0   J  J  J  -  J  J
11 2-0   -  K  -  -  -  K
12 2-0   L  -  L  L  L  L
13 2-0   M  M  M  M  -  -
14 2-0   N  N  N  N  N  N
15 0-3   O  -  O  O  O  -
16 0-3   -  -  -  -  -  -
17 0-3   Q  -  -  -  -  Q
18 0-3   R  -  -  R  R   
19 0-3   -  S  -  S  -   
20 0-3   T  T  -  -      
21 0-3   -  U  U  -      
22 0-3   -  -  V         
23 0-3   W  X  -         
24 0-3   X  -            
25 0-4   -  -            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MVKBO GHSZC MAMSR AQLPZ QRUAY K
-------------------------------
