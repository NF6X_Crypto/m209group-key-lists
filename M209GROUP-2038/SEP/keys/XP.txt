EFFECTIVE PERIOD:
18-SEP-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  B  -  -  B  B
03 1-0   -  -  C  C  -  -
04 1-0   D  -  D  -  D  -
05 1-0   E  E  E  E  -  -
06 1-0   F  F  -  -  -  -
07 2-0   -  -  -  -  G  -
08 2-0   -  H  -  H  H  H
09 2-0   -  -  I  I  -  I
10 2-0   J  J  -  J  J  -
11 0-5   K  K  K  K  K  -
12 0-5   -  L  -  L  L  L
13 0-5   -  -  -  -  M  M
14 0-5   -  -  -  -  N  N
15 0-5   O  -  -  O  -  -
16 0-5   P  -  P  P  P  -
17 0-6   -  Q  Q  Q  Q  Q
18 1-2   -  R  -  -  -   
19 1-5   S  -  S  -  -   
20 1-5   T  T  T  -      
21 1-5   U  U  U  U      
22 1-5   V  -  V         
23 2-6   -  -  -         
24 3-4   X  Y            
25 3-5   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SOUUT ZUFQX ISSCW ZQMUP AXFPP Q
-------------------------------
