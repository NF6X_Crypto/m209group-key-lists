EFFECTIVE PERIOD:
04-SEP-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  B  B  -  B  -
03 1-0   -  C  C  C  -  -
04 1-0   D  D  -  D  D  -
05 2-0   E  -  -  -  -  -
06 0-4   F  F  F  -  -  -
07 0-4   G  G  -  -  -  G
08 0-4   H  H  -  H  -  H
09 0-4   -  I  I  I  I  I
10 0-4   J  J  -  J  J  -
11 0-4   K  -  K  -  -  -
12 0-4   -  -  -  L  L  L
13 0-4   -  M  M  -  M  M
14 0-5   N  N  N  -  -  -
15 0-5   -  -  O  O  O  O
16 0-6   P  P  -  -  -  -
17 0-6   -  Q  -  Q  -  Q
18 0-6   -  -  -  -  R   
19 0-6   -  S  S  -  S   
20 1-5   T  -  T  T      
21 1-6   U  U  U  -      
22 2-6   V  -  -         
23 3-4   W  -  -         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MOTGY GSUFG CPUGL LWWYN WKHTI P
-------------------------------
