EFFECTIVE PERIOD:
08-APR-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   B  B  -  -  B  B
03 1-0   C  C  C  C  C  C
04 2-0   D  -  -  D  D  -
05 2-0   -  E  E  E  -  E
06 2-0   -  F  F  F  -  F
07 2-0   -  G  -  -  G  -
08 0-3   -  H  H  H  -  H
09 0-4   -  I  -  -  -  I
10 0-4   J  -  -  J  J  J
11 0-4   -  K  K  -  K  -
12 0-4   -  -  -  L  -  -
13 0-4   M  M  M  M  M  -
14 0-5   N  -  -  -  N  -
15 0-6   -  -  -  -  -  O
16 0-6   -  -  P  -  P  -
17 0-6   -  Q  Q  Q  -  Q
18 0-6   R  -  -  R  R   
19 0-6   -  S  -  S  S   
20 0-6   -  T  -  -      
21 0-6   -  U  U  -      
22 1-4   V  -  -         
23 1-5   W  X  X         
24 2-4   -  -            
25 2-6   Y  -            
26 2-6   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

GBOAW NZGOW GZZRW PUKPH MWAUE T
-------------------------------
