EFFECTIVE PERIOD:
01-JAN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  B  B  -  B  -
03 1-0   -  -  C  C  C  C
04 1-0   -  D  -  D  -  D
05 1-0   E  -  E  -  E  -
06 1-0   -  -  -  F  -  F
07 2-0   -  -  G  -  G  -
08 2-0   -  H  -  H  -  -
09 0-3   I  I  -  I  I  I
10 0-3   J  J  -  J  J  -
11 0-3   -  -  K  K  -  K
12 0-3   -  -  L  L  -  -
13 0-3   M  M  M  -  M  M
14 0-5   N  N  -  -  -  N
15 0-6   O  -  O  O  -  -
16 0-6   P  -  -  P  P  P
17 0-6   Q  Q  -  -  -  -
18 0-6   -  R  -  -  R   
19 0-6   -  S  S  S  -   
20 1-3   -  T  T  -      
21 1-3   -  U  U  -      
22 1-3   V  V  -         
23 1-3   W  -  -         
24 2-5   -  Y            
25 2-6   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QSKKF SVYCL TUUZP QMJEU AJPPZ M
-------------------------------
