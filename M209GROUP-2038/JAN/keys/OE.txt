EFFECTIVE PERIOD:
16-JAN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  -  -  -  B  B
03 1-0   C  -  C  C  -  -
04 2-0   -  -  -  D  D  -
05 2-0   -  E  E  -  E  E
06 2-0   F  F  F  F  -  F
07 2-0   G  G  G  -  -  -
08 2-0   -  -  H  H  H  H
09 2-0   I  I  I  I  I  -
10 2-0   -  J  J  J  -  -
11 2-0   K  K  -  K  -  K
12 2-0   -  -  L  -  L  L
13 2-0   M  M  -  -  M  M
14 0-3   -  -  -  -  -  -
15 0-3   O  -  -  -  -  -
16 0-3   -  -  -  -  -  -
17 0-3   -  -  Q  -  Q  Q
18 0-3   R  -  R  R  R   
19 0-3   -  S  -  S  S   
20 0-3   T  T  -  T      
21 0-3   -  -  -  U      
22 0-3   V  -  V         
23 0-4   W  -  X         
24 0-5   X  -            
25 0-6   Y  -            
26 1-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

CCFIW NHHPN DRNDM TZZMA KWZPC G
-------------------------------
