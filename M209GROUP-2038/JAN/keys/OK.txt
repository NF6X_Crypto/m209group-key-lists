EFFECTIVE PERIOD:
22-JAN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   -  B  -  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   -  -  D  D  -  -
05 1-0   E  E  -  E  -  E
06 1-0   F  -  -  F  F  F
07 1-0   G  G  -  -  -  -
08 2-0   -  H  -  -  H  -
09 0-3   I  -  -  I  I  I
10 0-3   -  J  J  -  J  -
11 0-3   -  -  -  -  -  K
12 0-3   -  L  L  L  L  -
13 0-3   -  -  -  -  M  -
14 0-3   N  N  N  N  N  N
15 0-3   O  O  O  -  -  O
16 0-3   -  -  -  -  -  -
17 0-3   -  Q  -  -  Q  -
18 0-3   -  -  R  -  R   
19 0-3   -  S  S  -  -   
20 0-3   T  T  -  T      
21 0-4   U  -  U  -      
22 0-5   -  -  -         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SPCDX VBYDA WNGQC NIOAU HRDLZ N
-------------------------------
