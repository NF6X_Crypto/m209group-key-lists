EFFECTIVE PERIOD:
09-JUN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   -  B  -  B  -  -
03 2-0   -  C  C  -  -  C
04 2-0   D  D  -  D  D  D
05 2-0   E  -  E  -  E  -
06 2-0   F  -  -  -  F  F
07 2-0   -  -  G  -  -  -
08 2-0   -  H  -  -  -  -
09 0-3   -  -  -  I  -  -
10 0-3   -  -  J  -  -  J
11 0-4   -  K  K  -  K  -
12 0-6   L  L  -  -  -  L
13 0-6   M  -  -  -  -  M
14 0-6   N  -  N  N  N  N
15 0-6   O  O  O  O  O  O
16 0-6   -  P  -  P  -  -
17 0-6   Q  -  -  Q  Q  -
18 0-6   -  -  -  R  -   
19 0-6   S  -  S  -  S   
20 0-6   -  -  -  T      
21 0-6   -  -  -  -      
22 0-6   V  V  -         
23 0-6   W  X  X         
24 1-5   -  Y            
25 2-3   -  -            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

MAPPN FAYLZ SBALN ARAXQ QXMFS L
-------------------------------
