EFFECTIVE PERIOD:
07-JUN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  -  -  B  B
03 1-0   -  C  C  C  C  -
04 1-0   D  -  -  -  D  -
05 2-0   E  -  -  E  -  E
06 2-0   -  F  F  F  F  F
07 2-0   G  G  G  -  G  -
08 2-0   -  H  H  -  H  H
09 2-0   I  I  I  -  -  -
10 0-4   -  -  -  -  -  -
11 0-4   K  -  -  K  K  K
12 0-4   L  -  -  -  L  L
13 0-4   -  M  M  M  M  -
14 0-4   -  N  N  N  N  -
15 0-5   O  -  -  -  -  O
16 0-6   -  -  -  -  -  P
17 0-6   -  -  Q  -  -  -
18 0-6   R  -  R  R  -   
19 0-6   -  -  -  S  -   
20 1-4   -  T  T  T      
21 1-5   -  U  U  -      
22 2-3   V  V  V         
23 2-6   W  X  X         
24 2-6   X  Y            
25 2-6   -  -            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RQGAJ UVZUS GQUSU HJUQW BUUJO O
-------------------------------
