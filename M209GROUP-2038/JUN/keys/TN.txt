EFFECTIVE PERIOD:
04-JUN-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  -  -  -  -
03 1-0   C  -  -  C  C  -
04 1-0   -  -  D  D  -  -
05 1-0   -  E  -  E  -  E
06 1-0   F  F  F  F  -  F
07 1-0   -  -  -  -  -  -
08 1-0   -  H  H  H  -  H
09 2-0   I  -  -  -  I  I
10 0-3   -  J  J  -  J  -
11 0-3   K  -  -  -  K  -
12 0-4   L  -  L  L  -  -
13 0-4   M  -  M  -  -  M
14 0-4   -  -  -  N  N  -
15 0-4   O  O  O  O  O  -
16 0-4   -  P  -  P  P  -
17 0-6   Q  -  Q  Q  -  Q
18 0-6   -  R  -  -  -   
19 0-6   S  -  S  -  S   
20 1-4   T  -  T  -      
21 1-4   U  -  -  -      
22 1-4   V  V  -         
23 1-4   -  -  X         
24 2-3   X  Y            
25 2-5   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

CXOXS PTHLR FNVVN JUECT MQWWQ U
-------------------------------
