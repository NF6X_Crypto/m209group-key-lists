SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF MAY 2038

    01 MAY 2038  00:00-23:59 GMT:  USE KEY SF
    02 MAY 2038  00:00-23:59 GMT:  USE KEY SG
    03 MAY 2038  00:00-23:59 GMT:  USE KEY SH
    04 MAY 2038  00:00-23:59 GMT:  USE KEY SI
    05 MAY 2038  00:00-23:59 GMT:  USE KEY SJ
    06 MAY 2038  00:00-23:59 GMT:  USE KEY SK
    07 MAY 2038  00:00-23:59 GMT:  USE KEY SL
    08 MAY 2038  00:00-23:59 GMT:  USE KEY SM
    09 MAY 2038  00:00-23:59 GMT:  USE KEY SN
    10 MAY 2038  00:00-23:59 GMT:  USE KEY SO
    11 MAY 2038  00:00-23:59 GMT:  USE KEY SP
    12 MAY 2038  00:00-23:59 GMT:  USE KEY SQ
    13 MAY 2038  00:00-23:59 GMT:  USE KEY SR
    14 MAY 2038  00:00-23:59 GMT:  USE KEY SS
    15 MAY 2038  00:00-23:59 GMT:  USE KEY ST
    16 MAY 2038  00:00-23:59 GMT:  USE KEY SU
    17 MAY 2038  00:00-23:59 GMT:  USE KEY SV
    18 MAY 2038  00:00-23:59 GMT:  USE KEY SW
    19 MAY 2038  00:00-23:59 GMT:  USE KEY SX
    20 MAY 2038  00:00-23:59 GMT:  USE KEY SY
    21 MAY 2038  00:00-23:59 GMT:  USE KEY SZ
    22 MAY 2038  00:00-23:59 GMT:  USE KEY TA
    23 MAY 2038  00:00-23:59 GMT:  USE KEY TB
    24 MAY 2038  00:00-23:59 GMT:  USE KEY TC
    25 MAY 2038  00:00-23:59 GMT:  USE KEY TD
    26 MAY 2038  00:00-23:59 GMT:  USE KEY TE
    27 MAY 2038  00:00-23:59 GMT:  USE KEY TF
    28 MAY 2038  00:00-23:59 GMT:  USE KEY TG
    29 MAY 2038  00:00-23:59 GMT:  USE KEY TH
    30 MAY 2038  00:00-23:59 GMT:  USE KEY TI
    31 MAY 2038  00:00-23:59 GMT:  USE KEY TJ

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  -  -  -
02 0-3   B  -  -  B  B  B
03 0-3   C  -  C  C  -  C
04 0-3   -  D  -  D  -  -
05 0-3   E  -  -  -  -  -
06 0-3   F  F  -  F  F  F
07 0-3   -  -  G  G  -  -
08 0-3   H  -  H  H  H  -
09 0-4   -  -  I  -  I  I
10 0-4   J  J  J  -  -  J
11 0-5   K  K  -  -  K  -
12 0-5   L  L  -  -  L  L
13 0-5   -  M  -  M  -  -
14 0-5   -  -  -  -  N  N
15 0-5   -  -  O  O  O  O
16 0-6   -  -  P  P  P  P
17 0-6   Q  -  -  -  -  -
18 0-6   R  R  -  R  R   
19 0-6   S  -  -  -  -   
20 1-4   T  T  -  T      
21 2-3   -  U  U  U      
22 3-5   -  -  V         
23 3-5   W  -  -         
24 3-5   -  Y            
25 3-5   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NTWSP JKQVQ QEVKE RAMYS HSQOQ X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   B  -  -  -  B  -
03 1-0   -  -  C  -  C  -
04 1-0   D  -  D  -  -  D
05 1-0   -  E  -  -  -  E
06 0-4   F  F  F  F  -  -
07 0-4   G  G  G  G  G  G
08 0-4   -  -  -  H  -  H
09 0-5   -  -  I  I  -  -
10 0-5   J  -  -  J  J  -
11 0-5   K  K  K  -  K  K
12 0-6   L  L  L  L  -  L
13 0-6   -  M  -  -  -  -
14 0-6   -  -  -  -  -  N
15 0-6   O  O  -  -  O  O
16 1-2   -  P  P  -  P  -
17 1-2   -  Q  Q  -  -  Q
18 1-2   R  -  -  R  R   
19 1-4   S  -  -  S  S   
20 1-5   -  -  T  T      
21 1-5   U  -  -  -      
22 1-5   -  -  V         
23 1-5   W  X  -         
24 2-3   X  Y            
25 2-4   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KKOWM RSVSM XSNGZ JSASZ XUSRZ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  A  A
02 0-4   -  B  -  -  -  B
03 0-4   C  C  -  C  C  -
04 0-4   D  D  D  D  -  -
05 0-4   E  -  E  E  -  -
06 0-4   -  -  F  F  -  F
07 0-4   G  -  -  -  -  G
08 0-5   -  -  H  H  -  -
09 0-5   I  -  -  -  I  I
10 0-5   -  J  -  -  -  -
11 0-6   K  -  K  K  K  K
12 0-6   -  -  L  -  L  L
13 0-6   M  M  -  M  -  M
14 0-6   -  N  N  N  N  -
15 0-6   O  O  O  -  O  -
16 1-5   -  -  P  -  P  -
17 2-3   -  Q  Q  -  -  Q
18 3-4   -  R  -  R  -   
19 3-5   -  -  -  S  -   
20 3-5   T  T  -  T      
21 3-5   U  -  -  -      
22 3-5   -  V  -         
23 4-6   -  -  -         
24 5-6   X  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWUOL JQRRA OHRXO RTRRY GVTUQ R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  B  B  -  B  -
03 1-0   C  -  -  C  C  -
04 2-0   -  D  -  -  D  D
05 0-3   -  -  -  -  -  E
06 0-3   -  -  -  F  -  -
07 0-3   -  -  G  G  G  G
08 0-3   -  H  -  H  -  -
09 0-3   I  -  I  I  I  I
10 0-3   J  J  J  -  J  J
11 0-3   -  -  K  K  -  K
12 0-3   -  L  -  -  -  -
13 0-3   -  M  M  M  -  M
14 0-4   -  N  N  N  N  -
15 0-5   -  -  -  -  -  O
16 0-5   P  -  P  P  P  -
17 0-5   Q  -  -  Q  -  -
18 0-5   -  -  R  R  R   
19 0-5   S  S  -  S  -   
20 0-6   -  -  -  -      
21 0-6   U  -  U  -      
22 1-5   V  -  V         
23 1-6   -  X  -         
24 2-6   -  -            
25 3-5   Y  -            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

UFVFX FNLNM PABHL EWNNU SNOTC S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  B  B  B  B  -
03 1-0   -  -  C  -  -  C
04 1-0   D  D  -  -  D  D
05 1-0   -  E  -  E  E  -
06 0-4   -  F  -  F  F  F
07 0-4   G  G  -  -  G  G
08 0-4   H  -  -  -  H  -
09 0-4   -  I  I  -  -  -
10 0-4   J  J  -  -  J  J
11 0-4   K  K  K  -  -  -
12 0-5   L  L  L  L  -  -
13 0-5   -  -  -  -  M  M
14 0-5   -  -  N  -  -  N
15 0-6   O  O  O  -  -  -
16 1-4   -  -  P  -  -  -
17 1-5   Q  Q  -  Q  Q  -
18 1-5   R  -  -  R  -   
19 1-5   S  -  S  -  S   
20 1-5   T  T  -  T      
21 2-3   -  U  -  U      
22 3-5   -  -  V         
23 3-5   W  X  -         
24 3-6   X  -            
25 4-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

DSROQ UAXUZ LSOUY LFRNF ANKDM U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  C  C  -
04 1-0   -  -  D  -  D  D
05 0-3   -  E  E  E  E  E
06 0-3   F  F  -  -  F  -
07 0-3   G  G  -  -  G  -
08 0-4   -  -  H  -  -  -
09 0-4   -  I  -  I  I  -
10 0-4   -  J  -  J  J  J
11 0-4   -  K  -  -  -  -
12 0-4   L  -  -  -  -  -
13 0-4   M  M  -  -  -  M
14 0-6   N  N  N  -  -  -
15 0-6   O  -  -  O  O  O
16 1-4   -  -  P  P  P  P
17 1-6   -  -  Q  Q  -  -
18 1-6   R  R  R  -  R   
19 1-6   -  S  -  S  -   
20 1-6   -  T  T  T      
21 2-3   -  U  U  -      
22 2-5   V  V  V         
23 2-6   -  -  X         
24 2-6   -  -            
25 3-4   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

GZZQW LYQOG WAAYH DQYHL DLPYT A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  B  B  -  -  -
03 1-0   C  -  -  -  C  -
04 1-0   D  -  -  D  D  -
05 1-0   -  -  -  E  -  E
06 1-0   F  F  F  -  F  -
07 0-3   -  -  G  G  -  -
08 0-3   H  -  H  -  H  -
09 0-3   I  -  -  I  I  -
10 0-3   -  J  J  -  -  -
11 0-3   -  K  K  -  -  -
12 0-4   L  -  -  L  L  L
13 0-4   -  -  -  M  M  M
14 0-4   -  -  -  -  -  -
15 0-4   O  -  O  -  -  O
16 0-4   -  -  P  P  -  P
17 0-4   Q  Q  Q  -  Q  Q
18 0-4   -  R  -  R  R   
19 0-4   S  S  -  -  -   
20 0-4   -  T  -  T      
21 0-4   -  -  U  U      
22 0-5   -  -  V         
23 0-6   -  X  -         
24 1-3   X  Y            
25 1-4   Y  -            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

MQIYC OCSYT BJMTH TPRIQ TNIAU P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  -  -  -  -
03 1-0   C  -  C  -  C  -
04 1-0   D  -  -  D  D  D
05 2-0   E  -  E  -  -  -
06 2-0   F  F  F  F  F  F
07 2-0   -  -  -  -  G  G
08 2-0   H  -  H  H  -  -
09 0-4   I  -  -  -  -  I
10 0-5   -  J  J  J  -  -
11 0-5   K  -  -  -  K  -
12 0-5   -  -  -  -  L  L
13 0-5   -  M  M  -  -  -
14 0-5   N  N  N  -  -  -
15 0-5   -  O  -  O  O  O
16 0-5   -  P  P  -  -  P
17 0-6   -  -  Q  Q  -  -
18 0-6   R  R  -  R  -   
19 0-6   S  -  S  S  S   
20 1-2   -  -  T  T      
21 1-5   -  U  -  U      
22 1-5   -  -  -         
23 1-5   -  X  -         
24 1-5   X  -            
25 2-6   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RJLVF GIRXZ VRPVS OPAWQ FNQIR I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 0-3   -  B  -  B  B  -
03 0-3   C  C  -  -  C  -
04 0-3   D  -  -  D  D  -
05 0-3   E  E  -  E  -  E
06 0-3   F  -  F  F  -  F
07 0-3   G  -  -  G  -  -
08 0-3   -  -  -  -  H  -
09 0-5   I  I  -  -  I  I
10 0-5   -  -  J  J  J  J
11 0-5   K  -  K  -  -  K
12 0-5   -  -  L  L  -  L
13 0-5   -  -  M  M  -  M
14 0-5   N  N  N  -  -  -
15 0-6   -  O  -  O  -  O
16 0-6   -  P  P  P  -  -
17 0-6   -  Q  -  -  Q  Q
18 0-6   R  -  -  -  -   
19 0-6   S  S  S  -  S   
20 1-4   T  T  -  -      
21 2-5   U  -  U  U      
22 3-4   -  -  V         
23 3-6   W  -  -         
24 5-6   X  -            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TKUCM VJAKM NQMDC ASSLU JTIST J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  B  -  -  B
03 1-0   -  C  C  C  C  C
04 1-0   -  -  D  -  -  D
05 1-0   -  -  -  -  -  -
06 0-3   -  F  F  -  F  -
07 0-3   G  G  -  G  -  G
08 0-3   H  H  H  -  -  H
09 0-3   -  I  -  I  I  -
10 0-6   -  -  -  J  J  -
11 0-6   -  -  K  -  -  -
12 0-6   L  -  -  -  L  L
13 0-6   M  -  M  -  M  M
14 0-6   -  N  -  N  N  -
15 0-6   O  O  -  O  -  O
16 1-2   -  P  -  P  -  -
17 1-2   -  -  Q  Q  -  Q
18 1-5   R  R  -  R  -   
19 1-5   -  S  -  -  S   
20 1-6   -  T  T  T      
21 1-6   U  U  U  U      
22 1-6   -  -  -         
23 1-6   -  -  X         
24 2-4   X  Y            
25 2-5   Y  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

GRWJO OTJWA AUSVK GGUVR IOTUD V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 2-0   -  B  B  -  -  B
03 2-0   C  C  -  C  -  -
04 2-0   D  D  D  -  -  -
05 2-0   E  -  E  E  E  E
06 2-0   -  F  -  F  F  -
07 2-0   -  G  -  -  -  G
08 2-0   H  H  H  -  H  H
09 0-3   -  I  I  -  I  I
10 0-3   -  J  -  -  -  J
11 0-3   K  K  -  -  -  K
12 0-3   -  -  -  -  L  L
13 0-3   -  M  M  M  M  -
14 0-4   -  N  -  N  -  -
15 0-4   O  -  O  O  O  -
16 0-4   -  P  P  -  -  P
17 0-4   -  Q  Q  -  -  -
18 0-4   -  -  -  -  -   
19 0-5   S  -  -  S  S   
20 0-6   -  -  -  T      
21 0-6   U  U  U  U      
22 2-3   V  -  V         
23 2-3   W  -  X         
24 2-3   X  Y            
25 3-4   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KZVLZ ERTSW IRLAZ FOTUL LRSQT N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  -  B  -  B  -
03 2-0   -  C  -  C  -  -
04 2-0   D  -  D  -  -  -
05 2-0   -  -  -  E  -  E
06 2-0   -  -  -  F  F  F
07 2-0   G  G  G  G  -  -
08 2-0   H  H  H  -  H  H
09 2-0   I  I  -  I  I  -
10 2-0   -  -  J  -  -  J
11 2-0   -  -  -  K  K  -
12 2-0   L  L  L  -  L  -
13 0-3   M  M  M  M  M  -
14 0-3   N  N  N  N  -  N
15 0-3   O  -  O  O  -  O
16 0-3   -  -  P  -  P  P
17 0-3   -  Q  -  Q  -  -
18 0-3   R  -  -  -  -   
19 0-3   -  S  -  -  S   
20 0-3   -  -  -  -      
21 0-4   -  U  U  U      
22 0-5   V  -  -         
23 0-6   -  X  X         
24 1-3   X  -            
25 1-4   -  Z            
26 2-3   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

NNYBP BOMRK TXBXG DMYPZ CNZZY O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  B  -  B  -
03 1-0   C  C  -  -  -  C
04 1-0   -  D  -  -  -  -
05 2-0   -  E  -  E  -  -
06 2-0   F  F  F  -  -  -
07 2-0   -  -  G  G  G  -
08 2-0   -  -  H  H  H  -
09 0-3   I  I  I  I  -  I
10 0-3   -  -  J  J  J  J
11 0-3   -  -  -  K  K  K
12 0-3   -  L  L  -  L  -
13 0-3   M  -  -  -  -  M
14 0-3   -  -  -  -  N  N
15 0-3   O  -  -  O  -  O
16 0-3   -  P  -  P  -  P
17 0-4   Q  Q  -  -  -  -
18 0-4   -  -  R  -  -   
19 0-6   S  -  S  S  S   
20 1-2   T  T  T  -      
21 1-4   -  U  -  U      
22 2-6   V  V  V         
23 3-4   -  -  -         
24 3-4   -  Y            
25 3-4   -  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CKOOU TYHPO RWKQG WARPA COHLJ F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  -
02 2-0   B  -  B  B  B  -
03 2-0   C  -  C  -  -  C
04 2-0   -  -  -  -  D  D
05 2-0   E  -  -  -  -  -
06 0-3   -  F  F  F  -  F
07 0-3   -  -  G  -  G  -
08 0-3   -  H  -  H  -  -
09 0-4   I  I  -  -  -  I
10 0-4   J  J  J  -  J  J
11 0-4   -  K  -  K  -  K
12 0-4   L  L  L  L  L  L
13 0-4   M  -  M  -  M  M
14 0-4   -  -  N  N  -  -
15 0-5   O  O  O  -  -  -
16 1-5   P  P  -  -  P  P
17 2-3   Q  Q  -  -  -  Q
18 2-3   -  R  R  R  R   
19 2-3   -  -  S  S  S   
20 2-3   -  -  -  T      
21 2-4   -  -  -  -      
22 3-5   V  V  -         
23 3-5   W  -  -         
24 3-5   X  -            
25 3-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

UAKJO TOWGO YOMAN MYHKA WRZOL Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ST
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  B  -  B  -  -
03 1-0   C  -  -  C  -  C
04 1-0   -  -  -  D  D  D
05 1-0   E  -  E  E  E  -
06 1-0   F  F  F  -  F  F
07 2-0   G  -  -  G  -  -
08 2-0   H  -  H  H  -  -
09 2-0   -  -  I  I  -  -
10 2-0   J  J  J  -  J  J
11 2-0   -  K  -  -  K  K
12 2-0   -  -  -  -  L  -
13 2-0   M  M  M  -  M  M
14 2-0   -  N  N  -  N  -
15 2-0   -  O  -  -  -  -
16 0-3   -  P  -  P  -  P
17 0-4   -  Q  -  -  Q  Q
18 0-5   R  R  R  -  R   
19 0-5   -  -  S  -  S   
20 0-5   T  -  -  T      
21 0-5   U  -  U  -      
22 0-5   V  -  V         
23 0-6   -  -  X         
24 1-2   -  Y            
25 1-5   -  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

TQYKK QPJCO EOZOC LTRPJ WQDUR Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  -
02 2-0   -  -  B  B  B  B
03 2-0   -  -  -  C  -  -
04 2-0   D  D  D  D  -  -
05 2-0   -  -  -  E  E  E
06 2-0   F  -  F  -  -  -
07 0-3   G  -  -  -  G  G
08 0-3   H  -  -  -  -  -
09 0-3   -  I  I  -  I  -
10 0-5   -  -  -  J  -  J
11 0-5   K  -  K  -  -  K
12 0-5   L  -  -  -  L  L
13 0-5   M  M  -  M  M  M
14 0-5   N  N  -  -  N  N
15 0-6   -  O  -  O  O  O
16 0-6   P  -  P  P  -  -
17 0-6   -  Q  -  Q  -  Q
18 1-4   -  -  R  -  R   
19 2-4   -  -  S  -  S   
20 2-4   -  T  -  -      
21 2-6   U  U  U  U      
22 2-6   -  V  V         
23 2-6   W  -  -         
24 2-6   -  -            
25 3-4   -  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XNARP RVXUK RPKPT EOIPV RRHRN A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   B  -  B  -  -  B
03 2-0   C  -  -  -  -  C
04 2-0   -  D  -  D  -  -
05 2-0   E  E  E  E  -  E
06 0-4   F  -  -  -  F  -
07 0-4   -  -  G  G  -  G
08 0-4   -  H  H  H  H  -
09 0-4   I  I  -  I  I  I
10 0-5   J  -  -  J  J  -
11 0-5   -  K  -  -  K  -
12 0-5   L  L  -  -  L  -
13 0-5   M  M  M  M  -  -
14 0-6   N  N  -  N  -  N
15 0-6   O  -  O  -  O  -
16 0-6   -  -  P  -  P  -
17 0-6   -  -  Q  -  Q  -
18 0-6   -  R  -  R  -   
19 0-6   S  -  S  S  S   
20 1-2   T  T  T  -      
21 1-3   -  -  -  U      
22 2-5   -  -  -         
23 4-5   W  -  X         
24 4-6   X  -            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

NJAWI TJSWK GMLWG AQOSM MOUMW M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 0-3   B  -  -  B  B  -
03 0-3   C  C  C  -  -  -
04 0-3   D  -  D  -  -  -
05 0-3   E  -  -  E  E  -
06 0-5   -  -  -  F  F  -
07 0-5   G  -  G  -  -  G
08 0-5   H  -  H  -  H  H
09 0-5   I  I  I  I  I  -
10 0-5   -  -  -  -  -  J
11 0-5   -  -  -  -  K  -
12 0-5   L  L  L  L  -  L
13 0-5   M  M  -  M  -  M
14 0-5   -  N  -  N  N  N
15 0-5   -  -  O  O  -  -
16 0-6   -  -  P  P  P  P
17 0-6   Q  Q  Q  -  -  Q
18 0-6   -  -  -  -  R   
19 0-6   S  -  S  S  S   
20 0-6   T  -  -  T      
21 0-6   -  -  -  -      
22 1-4   -  V  V         
23 3-4   W  X  X         
24 3-6   X  -            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

BPAPN OVWUT HBTAL NYGZL LTMWQ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  -  -  C
04 1-0   D  -  -  -  D  -
05 1-0   E  -  E  E  E  -
06 1-0   -  F  -  F  F  -
07 1-0   -  -  -  -  G  G
08 2-0   H  -  H  H  -  -
09 2-0   I  -  I  I  I  I
10 2-0   J  J  J  J  J  J
11 2-0   -  K  K  K  K  K
12 2-0   -  L  -  L  -  L
13 2-0   M  M  -  -  M  M
14 2-0   -  -  -  -  N  -
15 2-0   O  O  -  -  O  -
16 0-3   P  P  P  P  P  -
17 0-3   -  Q  Q  -  -  Q
18 0-3   R  -  -  R  -   
19 0-4   S  -  -  -  -   
20 0-5   -  -  T  -      
21 0-6   -  -  U  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 0-6   X  Y            
25 0-6   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UOZNA BQMZW UNAXN MQLSR VIIVB Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  A  -
02 2-0   B  -  B  B  -  B
03 2-0   C  -  -  -  C  C
04 2-0   -  -  -  D  -  D
05 0-3   E  -  E  -  E  E
06 0-3   F  F  -  F  -  -
07 0-3   -  -  -  G  -  -
08 0-3   -  -  H  -  H  H
09 0-3   -  I  I  -  -  -
10 0-4   -  -  -  -  J  -
11 0-4   K  K  K  K  K  -
12 0-4   L  L  L  L  -  L
13 0-4   -  -  -  -  -  -
14 0-4   -  -  -  -  N  -
15 0-5   -  O  -  -  -  O
16 0-6   P  P  P  P  -  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  R  R  -  R   
19 0-6   -  S  S  S  S   
20 1-3   -  T  T  -      
21 2-4   U  -  U  U      
22 2-5   V  V  -         
23 3-6   W  -  -         
24 3-6   -  -            
25 3-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

GOLVU QWQQP RUJAU JGLQL QVGII V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   B  B  -  B  -  B
03 1-0   -  -  C  C  C  C
04 1-0   D  D  -  -  D  -
05 2-0   -  E  E  E  -  -
06 2-0   -  F  F  F  F  -
07 0-4   -  G  -  -  -  -
08 0-4   -  H  H  -  H  H
09 0-4   I  -  I  -  -  -
10 0-4   -  J  -  -  J  -
11 0-4   K  -  -  -  K  -
12 0-4   L  L  -  -  -  -
13 0-5   -  -  M  M  M  M
14 0-6   -  -  -  N  -  N
15 0-6   O  -  O  O  O  O
16 0-6   -  P  P  P  P  P
17 0-6   -  -  -  -  -  -
18 0-6   -  R  -  R  R   
19 0-6   -  S  -  -  -   
20 1-4   T  -  T  -      
21 1-4   U  U  -  U      
22 1-4   -  -  -         
23 1-4   W  X  -         
24 1-6   -  Y            
25 2-5   Y  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

KPSWT WFPVS URNMZ FDFFP LWAMS S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  -
02 2-0   B  B  -  -  -  B
03 2-0   -  C  -  C  C  C
04 2-0   D  -  -  -  D  -
05 2-0   -  E  -  E  -  -
06 0-3   -  -  -  F  F  -
07 0-5   -  -  G  -  -  -
08 0-5   H  H  -  H  -  H
09 0-5   I  -  I  -  -  -
10 0-5   -  -  -  J  -  -
11 0-5   K  K  K  -  K  -
12 0-5   -  -  L  -  L  -
13 0-5   -  -  -  M  M  M
14 0-5   N  N  N  -  N  N
15 0-5   O  O  -  O  O  O
16 0-5   P  P  -  -  -  -
17 0-5   Q  -  Q  -  Q  Q
18 0-6   -  R  R  R  -   
19 0-6   -  S  S  S  -   
20 0-6   -  T  -  T      
21 0-6   U  -  U  -      
22 0-6   V  V  -         
23 0-6   -  -  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-2   Z               
27 1-4                   
-------------------------------
26 LETTER CHECK

SRTZO SJHHK SRGRJ FBZSU TAKJR O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  B  -  -  -
03 0-3   -  -  -  C  C  C
04 0-3   D  -  -  -  D  D
05 0-3   E  E  E  -  E  -
06 0-3   -  F  F  F  -  F
07 0-3   -  G  -  G  -  -
08 0-3   H  H  -  H  H  -
09 0-4   -  -  -  -  I  I
10 0-4   -  J  J  J  -  -
11 0-4   K  K  K  K  -  K
12 0-4   -  L  L  -  L  -
13 0-4   M  -  -  -  -  M
14 0-4   -  -  -  -  -  N
15 0-4   O  -  O  O  O  -
16 0-6   -  P  -  P  -  P
17 0-6   -  -  Q  -  -  -
18 0-6   R  R  R  R  R   
19 0-6   -  S  -  S  -   
20 1-2   -  -  T  -      
21 1-3   U  U  -  U      
22 3-6   V  V  -         
23 4-5   W  X  -         
24 4-6   -  -            
25 4-6   Y  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JIUYI UWMMM TTNZW AMJPX QKKPX E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  -
02 2-0   -  -  -  -  B  -
03 2-0   C  C  -  -  -  -
04 2-0   -  D  -  -  -  D
05 2-0   E  E  -  -  -  -
06 2-0   -  -  F  -  -  F
07 2-0   -  -  G  G  -  G
08 2-0   H  -  H  -  H  -
09 0-3   I  I  I  I  -  I
10 0-4   -  J  J  J  J  -
11 0-4   -  K  K  K  K  -
12 0-4   -  -  -  -  -  L
13 0-4   M  -  M  -  M  -
14 0-4   -  -  -  N  N  -
15 0-4   O  -  -  O  -  O
16 0-4   P  P  P  -  P  -
17 0-4   Q  -  -  Q  -  Q
18 0-4   R  R  -  R  -   
19 0-4   -  -  -  S  S   
20 0-4   T  -  T  -      
21 0-4   U  U  U  -      
22 0-5   V  V  -         
23 0-5   W  X  X         
24 0-5   -  Y            
25 0-6   Y  -            
26 1-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

AZRHR MXNHZ DHAJH ACRSW NMORZ O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  B  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   -  D  -  D  -  -
05 2-0   E  E  E  -  E  E
06 2-0   -  -  -  -  -  -
07 2-0   G  G  -  G  -  G
08 0-3   -  -  H  H  H  H
09 0-3   I  I  -  I  I  I
10 0-3   -  -  -  -  -  -
11 0-3   -  K  -  K  -  -
12 0-4   L  -  -  -  -  -
13 0-4   -  M  -  -  -  M
14 0-4   N  N  N  -  -  N
15 0-4   O  -  -  O  -  O
16 1-3   P  P  P  -  P  -
17 1-4   Q  -  Q  -  Q  -
18 1-4   -  R  R  -  R   
19 1-4   -  S  S  S  S   
20 1-4   T  T  -  -      
21 2-3   U  -  U  U      
22 2-4   -  -  -         
23 2-5   -  X  X         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NOSLW WNKWT RSRGL WNZZW AGIWS S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  -  B  B  B  -
03 1-0   -  C  -  C  C  C
04 0-3   -  -  D  D  -  -
05 0-3   -  -  -  -  E  E
06 0-3   -  F  -  -  F  -
07 0-3   G  -  -  -  -  -
08 0-4   -  -  H  H  H  -
09 0-4   I  I  I  -  I  I
10 0-4   -  -  -  -  -  J
11 0-4   K  K  K  K  -  -
12 0-5   L  -  L  -  L  -
13 0-5   -  -  M  -  -  M
14 0-5   N  N  N  N  N  N
15 0-5   -  -  -  O  -  O
16 0-5   -  P  -  P  -  P
17 0-5   Q  -  -  -  Q  Q
18 1-5   -  R  -  -  R   
19 1-6   S  S  -  -  -   
20 2-6   T  T  T  T      
21 3-4   U  U  -  U      
22 3-4   V  -  -         
23 3-4   -  X  X         
24 3-4   -  -            
25 3-6   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ROTQH OWIQX AQPSZ LTOJW LWUQU H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  B  B  B  B
03 0-3   -  C  -  -  C  -
04 0-3   -  D  D  -  -  -
05 0-3   E  -  E  -  -  -
06 0-3   F  -  -  -  F  F
07 0-3   -  -  G  G  -  G
08 0-3   H  H  -  -  -  H
09 0-3   I  I  I  I  -  -
10 0-4   -  -  -  J  -  J
11 0-5   K  K  -  K  -  -
12 0-5   L  L  -  L  L  -
13 0-5   -  -  -  -  -  M
14 0-5   -  -  N  N  N  N
15 0-5   -  O  O  O  O  O
16 1-4   -  P  P  -  P  P
17 1-4   -  Q  Q  Q  -  -
18 1-4   R  -  -  R  -   
19 1-5   -  -  S  -  S   
20 1-5   -  T  -  -      
21 1-5   -  -  U  U      
22 1-5   -  V  -         
23 1-6   W  -  -         
24 2-6   X  Y            
25 3-4   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SOAUR PZYNQ TTLUL MUFVT HRTWI P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   -  -  B  B  B  B
03 1-0   C  -  C  -  -  C
04 1-0   D  -  -  D  D  -
05 1-0   E  -  -  -  E  -
06 1-0   -  F  -  F  F  F
07 2-0   G  G  G  G  G  G
08 2-0   H  H  H  H  -  -
09 2-0   -  I  -  -  -  I
10 2-0   -  J  J  J  J  -
11 0-4   K  K  K  -  -  K
12 0-4   L  -  -  -  -  L
13 0-4   M  M  -  -  -  M
14 0-4   N  N  -  N  -  N
15 0-5   -  -  O  -  O  -
16 0-6   -  P  -  -  P  P
17 0-6   Q  -  Q  -  -  -
18 1-5   -  -  R  R  -   
19 1-5   -  -  S  -  -   
20 1-6   -  T  -  T      
21 1-6   -  -  U  -      
22 1-6   V  -  -         
23 1-6   W  -  -         
24 2-4   X  Y            
25 2-6   -  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GZGLE RLSOU LEQRT ULWYM MRWUS G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   -  B  -  -  B  -
03 1-0   C  -  -  -  -  -
04 1-0   D  D  D  D  D  -
05 1-0   -  -  -  E  E  E
06 1-0   -  -  -  -  F  F
07 1-0   -  G  G  -  -  G
08 0-4   H  H  H  -  H  -
09 0-4   I  -  I  -  -  I
10 0-4   J  -  J  J  J  J
11 0-4   -  K  -  -  -  K
12 0-4   -  L  L  -  -  -
13 0-4   -  -  M  -  M  -
14 0-4   N  -  N  -  N  -
15 0-4   O  O  -  O  O  O
16 0-4   P  -  P  P  -  -
17 0-4   Q  Q  -  -  -  Q
18 0-4   -  -  -  R  R   
19 0-5   S  -  S  S  S   
20 0-6   T  T  T  -      
21 0-6   -  -  -  U      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 1-4   -  -            
25 1-6   -  -            
26 2-3   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

OJOUN NAPMM BPTAJ GGHVK JHCSY L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   -  -  B  -  -  -
03 1-0   -  C  -  -  C  -
04 1-0   -  -  -  D  -  D
05 1-0   E  -  -  -  -  E
06 2-0   F  -  F  F  F  -
07 0-3   -  G  G  -  G  G
08 0-3   -  -  H  -  H  -
09 0-3   I  I  -  I  -  -
10 0-3   -  -  J  J  -  J
11 0-3   -  K  K  -  K  -
12 0-3   L  L  -  -  -  -
13 0-4   M  M  M  -  -  M
14 0-6   -  -  N  N  -  N
15 0-6   O  -  -  O  O  O
16 0-6   P  P  P  -  P  P
17 0-6   -  -  -  Q  Q  Q
18 0-6   -  R  -  R  -   
19 0-6   -  -  S  S  S   
20 0-6   -  -  T  T      
21 0-6   U  -  U  -      
22 1-3   -  -  -         
23 1-5   W  -  X         
24 3-6   X  Y            
25 3-6   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

PCTNN ZTRHM SSBNC QXMCY RLAAB R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   B  -  -  B  B  -
03 1-0   -  C  -  -  C  C
04 1-0   -  -  -  -  -  -
05 0-4   -  E  E  E  -  E
06 0-4   F  -  -  F  -  -
07 0-5   G  G  G  G  G  -
08 0-5   H  -  -  -  -  H
09 0-5   -  -  -  -  -  -
10 0-5   J  J  -  J  J  J
11 0-5   K  K  K  -  -  K
12 0-6   -  -  L  -  -  L
13 0-6   -  -  -  -  M  M
14 0-6   N  -  N  N  N  N
15 0-6   -  O  O  -  -  -
16 0-6   P  P  -  P  P  P
17 0-6   -  -  Q  Q  Q  -
18 1-5   R  -  -  R  -   
19 1-5   S  -  S  S  S   
20 1-5   -  T  T  -      
21 1-5   U  U  -  -      
22 1-6   V  -  V         
23 2-4   W  X  X         
24 2-5   -  Y            
25 3-5   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SWXUP APXCW PRIFW FQYTH YRLLA F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
