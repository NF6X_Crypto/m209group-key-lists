EFFECTIVE PERIOD:
03-MAY-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  A  A
02 0-4   -  B  -  -  -  B
03 0-4   C  C  -  C  C  -
04 0-4   D  D  D  D  -  -
05 0-4   E  -  E  E  -  -
06 0-4   -  -  F  F  -  F
07 0-4   G  -  -  -  -  G
08 0-5   -  -  H  H  -  -
09 0-5   I  -  -  -  I  I
10 0-5   -  J  -  -  -  -
11 0-6   K  -  K  K  K  K
12 0-6   -  -  L  -  L  L
13 0-6   M  M  -  M  -  M
14 0-6   -  N  N  N  N  -
15 0-6   O  O  O  -  O  -
16 1-5   -  -  P  -  P  -
17 2-3   -  Q  Q  -  -  Q
18 3-4   -  R  -  R  -   
19 3-5   -  -  -  S  -   
20 3-5   T  T  -  T      
21 3-5   U  -  -  -      
22 3-5   -  V  -         
23 4-6   -  -  -         
24 5-6   X  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWUOL JQRRA OHRXO RTRRY GVTUQ R
-------------------------------
