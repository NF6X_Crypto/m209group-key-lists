EFFECTIVE PERIOD:
07-MAR-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  -  -  B  B  B
03 1-0   C  -  -  C  -  C
04 1-0   -  -  D  D  D  D
05 1-0   E  -  -  E  -  -
06 2-0   F  -  F  F  -  F
07 2-0   G  G  -  G  G  G
08 2-0   H  -  H  -  H  -
09 2-0   -  I  I  I  -  I
10 0-3   J  -  -  J  -  -
11 0-3   -  -  -  -  -  K
12 0-3   -  -  -  L  L  L
13 0-3   M  M  M  -  -  M
14 0-4   -  N  -  -  N  -
15 0-5   -  -  -  -  O  -
16 0-5   -  -  P  P  -  -
17 0-5   -  Q  -  -  -  -
18 0-5   -  R  R  -  R   
19 0-5   S  -  -  -  S   
20 0-5   T  T  T  T      
21 0-5   U  U  -  U      
22 1-2   -  -  V         
23 1-3   -  X  X         
24 2-6   X  Y            
25 3-5   -  Z            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

KAUWN LULAU TKBPK WGTPP HJOLG P
-------------------------------
