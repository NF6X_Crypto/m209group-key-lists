EFFECTIVE PERIOD:
19-FEB-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   B  B  -  -  B  -
03 2-0   C  -  -  -  -  -
04 2-0   D  D  D  -  D  D
05 2-0   -  -  E  E  -  E
06 2-0   -  F  F  F  F  -
07 2-0   -  -  -  G  G  G
08 2-0   -  -  H  H  -  -
09 0-3   I  I  -  -  I  -
10 0-3   J  J  J  J  J  J
11 0-3   -  K  K  K  -  -
12 0-3   L  -  -  -  -  -
13 0-4   -  M  -  -  -  M
14 0-4   -  N  N  N  -  N
15 0-4   O  -  O  O  O  -
16 0-4   -  -  -  -  -  -
17 0-4   -  Q  Q  -  Q  Q
18 0-4   R  -  -  R  R   
19 0-5   -  -  S  -  -   
20 1-2   T  -  -  T      
21 2-3   U  U  U  -      
22 2-3   V  V  -         
23 2-3   -  -  X         
24 2-3   X  Y            
25 3-4   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

IPVLA SPYAT KPMSA PWKHZ TMKZT C
-------------------------------
