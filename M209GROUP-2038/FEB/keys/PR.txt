EFFECTIVE PERIOD:
24-FEB-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  B  -  B  -  B
03 1-0   C  C  C  C  C  C
04 1-0   -  D  -  -  D  -
05 1-0   -  E  E  E  E  -
06 1-0   -  F  -  F  -  F
07 1-0   G  -  -  G  -  G
08 1-0   -  -  H  -  H  -
09 1-0   I  -  I  -  I  I
10 1-0   J  J  -  -  -  -
11 1-0   -  -  K  -  K  K
12 0-3   L  L  L  L  -  -
13 0-3   M  -  M  -  M  M
14 0-3   -  -  -  -  N  N
15 0-5   -  -  O  O  -  O
16 0-6   P  P  P  P  -  -
17 0-6   Q  -  Q  -  Q  -
18 0-6   -  -  R  -  R   
19 0-6   -  -  -  -  S   
20 0-6   T  -  -  -      
21 0-6   -  U  U  U      
22 0-6   V  -  -         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

UQPOY AMELU QNAWE MBYEU AOPMF O
-------------------------------
