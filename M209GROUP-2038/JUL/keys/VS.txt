EFFECTIVE PERIOD:
31-JUL-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 0-3   B  B  -  -  B  -
03 0-3   -  -  C  C  -  -
04 0-3   D  -  D  D  -  D
05 0-3   -  E  -  E  -  E
06 0-3   -  F  -  -  -  -
07 0-4   G  G  G  G  G  -
08 0-4   -  -  H  -  H  -
09 0-4   -  I  -  -  I  I
10 0-4   -  -  -  -  J  J
11 0-4   -  -  -  -  -  -
12 0-4   L  L  -  -  L  L
13 0-6   -  M  -  M  M  M
14 0-6   -  N  N  N  -  -
15 0-6   O  -  -  -  O  -
16 1-2   -  -  P  P  P  -
17 1-3   -  Q  -  -  -  Q
18 1-6   -  -  -  -  R   
19 1-6   S  -  -  -  -   
20 1-6   T  T  -  T      
21 2-6   -  U  U  U      
22 3-4   V  V  V         
23 4-6   W  -  X         
24 4-6   -  -            
25 4-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RSTRG BOARM USWQG TTNLM AKSTT M
-------------------------------
