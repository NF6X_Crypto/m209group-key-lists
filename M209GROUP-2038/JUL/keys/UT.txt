EFFECTIVE PERIOD:
06-JUL-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   B  B  -  -  B  -
03 1-0   -  -  -  C  -  C
04 1-0   D  D  D  -  D  -
05 2-0   -  E  E  E  -  -
06 2-0   F  -  F  -  -  -
07 0-3   -  G  -  G  G  -
08 0-3   H  -  H  -  -  -
09 0-3   I  I  I  I  I  -
10 0-3   -  -  -  J  J  J
11 0-3   -  -  -  K  K  -
12 0-3   -  L  L  L  -  L
13 0-6   M  -  -  M  M  -
14 0-6   -  -  N  -  -  N
15 0-6   O  O  -  O  -  O
16 0-6   -  -  P  -  -  P
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   S  S  -  S  -   
20 1-3   T  T  -  T      
21 1-6   U  U  U  -      
22 1-6   V  V  -         
23 1-6   W  -  -         
24 1-6   X  -            
25 2-3   -  -            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LMIAU UUTJM PBMIS LRLYN MTPXA S
-------------------------------
