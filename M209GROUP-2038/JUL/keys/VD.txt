EFFECTIVE PERIOD:
16-JUL-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  B  B  -  B
03 2-0   -  C  C  C  C  -
04 2-0   -  D  -  -  -  D
05 0-3   E  E  -  -  E  -
06 0-4   F  F  -  F  F  F
07 0-4   -  G  -  G  G  G
08 0-4   H  -  H  H  -  H
09 0-4   I  I  -  -  I  -
10 0-4   -  J  J  -  -  J
11 0-4   -  -  K  K  -  -
12 0-4   L  -  -  L  -  L
13 0-4   M  -  M  -  M  M
14 0-4   -  N  N  N  -  -
15 0-6   O  -  O  -  O  -
16 0-6   P  P  -  -  P  -
17 0-6   -  -  -  -  -  Q
18 0-6   -  R  R  -  R   
19 0-6   S  -  S  S  S   
20 1-2   T  -  T  T      
21 1-3   -  -  -  -      
22 2-6   V  V  -         
23 3-5   W  X  -         
24 4-6   X  -            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

FYJHF CYZPZ VPPNW TZGNA TZSPP O
-------------------------------
