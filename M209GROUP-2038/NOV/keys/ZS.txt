EFFECTIVE PERIOD:
12-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  -  -  -  B  B
03 1-0   -  -  C  C  C  C
04 1-0   -  D  D  -  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  F  F  -  F  F
07 1-0   G  G  G  -  -  G
08 1-0   H  -  -  -  -  -
09 1-0   -  I  -  I  I  -
10 1-0   -  J  J  J  -  -
11 1-0   K  K  K  -  -  -
12 2-0   L  -  -  L  -  -
13 0-3   -  M  -  M  M  -
14 0-3   -  N  -  N  -  N
15 0-3   O  -  O  -  O  -
16 0-3   -  P  P  -  P  P
17 0-4   Q  -  -  Q  -  Q
18 0-4   -  R  R  -  -   
19 0-4   -  -  -  S  -   
20 0-5   -  T  -  T      
21 0-5   -  -  U  U      
22 1-4   V  -  V         
23 1-4   W  X  X         
24 2-5   -  -            
25 2-6   -  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

FAFFN CRVNJ RJWJC IXTQI PTVFI W
-------------------------------
