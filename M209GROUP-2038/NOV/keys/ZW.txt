EFFECTIVE PERIOD:
16-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   -  B  B  B  B  B
03 0-3   -  -  -  -  C  -
04 0-3   D  -  -  -  D  D
05 0-3   -  -  E  E  E  -
06 0-3   F  F  F  F  F  -
07 0-3   -  G  G  G  -  G
08 0-3   -  -  -  H  -  -
09 0-3   I  -  I  -  -  -
10 0-3   J  -  -  -  -  -
11 0-3   -  -  K  K  -  -
12 0-3   -  -  L  -  L  -
13 0-3   M  M  M  M  M  M
14 0-3   -  N  N  N  N  -
15 0-3   O  O  -  -  O  -
16 0-4   P  P  -  P  -  P
17 0-4   Q  -  -  -  Q  Q
18 0-5   R  -  R  R  -   
19 0-5   S  S  -  -  -   
20 0-5   -  T  T  -      
21 0-6   -  -  U  U      
22 0-6   V  -  -         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 1-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VIKHN MTTMT NRELR QSAMM STYUM H
-------------------------------
