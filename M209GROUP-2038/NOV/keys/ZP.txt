EFFECTIVE PERIOD:
09-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  B  B  -  B  -
03 1-0   -  -  -  -  C  -
04 0-3   -  -  -  D  -  D
05 0-3   E  -  -  E  E  E
06 0-3   -  -  -  F  F  F
07 0-3   -  -  -  G  -  G
08 0-3   H  H  H  -  H  H
09 0-3   I  I  I  -  -  -
10 0-4   -  -  J  -  -  J
11 0-4   K  K  K  K  -  K
12 0-4   -  -  -  L  L  -
13 0-4   -  M  -  M  -  -
14 0-4   -  -  -  -  -  -
15 0-4   O  -  -  O  -  -
16 1-2   P  P  P  P  P  -
17 1-2   Q  Q  Q  -  -  Q
18 1-2   -  R  -  -  -   
19 1-4   -  S  S  -  -   
20 1-4   T  -  T  -      
21 1-4   -  -  U  U      
22 1-4   V  V  V         
23 1-5   -  -  -         
24 1-6   -  -            
25 2-3   Y  -            
26 2-5   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

TNVVN HTLTM UTRNL TAIUA RUZRK T
-------------------------------
