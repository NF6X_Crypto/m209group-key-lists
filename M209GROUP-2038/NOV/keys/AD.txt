EFFECTIVE PERIOD:
23-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  A  A
02 0-3   B  -  -  -  -  B
03 0-3   -  -  C  C  C  C
04 0-3   -  D  -  -  -  -
05 0-3   E  E  E  -  E  -
06 0-3   F  -  -  -  F  F
07 0-3   -  G  -  G  G  -
08 0-3   H  -  H  -  H  -
09 0-3   I  I  I  I  I  -
10 0-4   -  J  -  J  -  J
11 0-5   -  K  -  -  -  -
12 0-5   -  L  -  L  -  -
13 0-5   -  -  -  M  M  -
14 0-5   N  N  N  N  N  N
15 0-5   -  -  O  -  -  O
16 0-6   -  P  -  P  -  -
17 0-6   Q  Q  Q  -  -  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  S  S  S   
20 0-6   T  -  T  T      
21 0-6   U  -  -  -      
22 1-2   -  -  V         
23 1-5   W  X  X         
24 3-6   X  Y            
25 3-6   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MUHTL NMRZU TCZGT IQNOB RRBLV J
-------------------------------
