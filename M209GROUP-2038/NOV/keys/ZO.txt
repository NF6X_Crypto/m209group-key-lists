EFFECTIVE PERIOD:
08-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   -  -  -  -  B  -
03 2-0   -  -  C  C  C  C
04 2-0   D  -  -  D  D  D
05 2-0   E  E  -  -  -  E
06 0-4   -  -  -  F  -  F
07 0-4   G  G  -  G  G  G
08 0-4   -  H  H  -  -  -
09 0-4   -  I  -  -  -  I
10 0-4   J  -  J  -  J  -
11 0-5   -  -  K  -  -  K
12 0-5   L  -  L  L  L  -
13 0-5   -  M  -  M  M  -
14 0-5   -  N  -  -  -  N
15 0-6   -  O  -  O  O  -
16 1-3   P  P  P  -  -  -
17 2-4   Q  Q  Q  Q  Q  Q
18 2-4   R  -  R  R  R   
19 2-4   S  -  S  S  -   
20 2-4   T  T  -  T      
21 2-5   U  U  -  U      
22 3-4   -  V  V         
23 3-6   W  -  -         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

APUMO ZZWUU VUQFT WZVJR VOGVT Z
-------------------------------
