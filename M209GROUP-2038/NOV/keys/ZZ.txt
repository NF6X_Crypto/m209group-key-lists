EFFECTIVE PERIOD:
19-NOV-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  B  -  -  -  -
03 1-0   C  -  -  -  C  -
04 1-0   -  D  -  -  -  -
05 1-0   E  E  -  E  E  -
06 0-3   -  F  F  -  -  F
07 0-4   -  -  G  -  G  G
08 0-4   -  H  -  H  H  H
09 0-5   -  -  -  -  -  -
10 0-5   -  -  J  J  -  J
11 0-5   K  -  K  K  K  K
12 0-5   -  -  L  L  L  -
13 0-5   M  M  -  -  M  -
14 0-5   N  N  N  N  -  -
15 0-6   O  -  O  -  O  O
16 0-6   -  -  P  P  P  P
17 0-6   -  -  Q  Q  -  -
18 0-6   R  R  -  -  -   
19 0-6   S  -  -  -  -   
20 0-6   T  T  T  T      
21 0-6   -  U  -  U      
22 1-5   -  V  -         
23 1-6   W  -  -         
24 1-6   -  Y            
25 1-6   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SLJTZ KBTKD QAXTS ROJKL LQURU N
-------------------------------
