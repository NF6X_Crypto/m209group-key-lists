EFFECTIVE PERIOD:
10-DEC-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   -  B  -  B  B  -
03 1-0   C  -  C  C  -  C
04 1-0   D  D  -  D  D  -
05 2-0   -  -  E  -  E  E
06 2-0   F  F  F  -  F  F
07 2-0   -  G  -  G  -  G
08 2-0   H  -  -  -  -  -
09 2-0   I  I  -  I  I  -
10 2-0   -  J  -  -  -  J
11 2-0   -  K  -  K  -  -
12 2-0   L  L  -  L  L  -
13 0-3   M  M  M  M  -  -
14 0-4   N  -  N  N  N  -
15 0-4   -  O  -  -  -  -
16 0-4   -  P  P  -  -  P
17 0-4   Q  Q  Q  Q  -  -
18 0-4   -  -  -  -  R   
19 0-4   S  S  S  -  S   
20 0-5   T  -  T  -      
21 0-5   U  -  -  -      
22 0-5   V  V  -         
23 0-6   -  -  X         
24 1-4   X  Y            
25 1-5   -  -            
26 2-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

VUOHU IKOJP HNWKQ JTPFY PUXQG V
-------------------------------
