EFFECTIVE PERIOD:
13-DEC-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  -  -  B  B  -
03 1-0   -  C  -  C  -  C
04 1-0   -  -  -  -  D  D
05 2-0   -  -  E  E  E  E
06 2-0   -  F  -  F  F  -
07 2-0   -  -  -  -  -  G
08 2-0   -  -  H  H  -  -
09 0-4   I  -  -  I  -  I
10 0-4   J  J  J  -  J  J
11 0-4   -  -  K  -  K  -
12 0-4   L  L  L  -  L  -
13 0-4   -  -  -  M  -  M
14 0-4   -  N  N  -  N  -
15 0-4   O  -  -  O  O  O
16 0-4   -  P  -  -  -  -
17 0-5   Q  Q  Q  -  -  -
18 0-5   R  -  -  R  -   
19 0-5   S  -  S  S  S   
20 0-5   -  -  -  T      
21 0-6   U  -  U  -      
22 1-2   -  -  V         
23 1-4   -  X  -         
24 1-4   X  Y            
25 2-5   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QLANC PJQRW RWMWK CSQFO MRQKF G
-------------------------------
