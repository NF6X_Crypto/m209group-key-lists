EFFECTIVE PERIOD:
04-DEC-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   -  -  -  -  -  -
03 2-0   C  C  -  -  C  C
04 2-0   D  -  -  -  -  D
05 2-0   E  E  -  E  -  -
06 0-3   -  F  -  F  F  F
07 0-4   G  -  -  -  G  G
08 0-4   -  -  H  H  H  -
09 0-5   I  -  -  -  -  I
10 0-5   J  J  J  J  -  J
11 0-5   K  -  -  K  -  -
12 0-5   -  L  -  -  -  -
13 0-5   M  -  M  -  M  M
14 0-5   -  -  -  -  -  -
15 0-6   -  O  O  O  O  O
16 0-6   -  -  P  -  P  -
17 0-6   Q  Q  Q  -  -  Q
18 0-6   -  R  R  -  R   
19 0-6   S  -  S  S  -   
20 1-5   -  T  -  T      
21 2-5   -  -  U  U      
22 2-5   -  -  V         
23 2-5   -  X  -         
24 2-5   X  Y            
25 2-6   Y  -            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JMTST LUKVT TKGZO WSCDL QHSUI S
-------------------------------
