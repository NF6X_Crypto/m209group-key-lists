EFFECTIVE PERIOD:
14-DEC-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  -  B  -  -
03 1-0   -  C  C  -  -  -
04 1-0   -  -  D  -  -  D
05 1-0   -  E  -  -  -  -
06 2-0   F  -  -  F  F  F
07 2-0   G  -  -  -  G  -
08 2-0   -  -  -  -  H  H
09 2-0   -  -  -  I  I  -
10 2-0   J  -  J  J  J  -
11 2-0   -  K  -  K  -  -
12 0-3   -  L  -  -  L  -
13 0-3   -  -  M  -  -  M
14 0-3   N  -  -  N  -  -
15 0-4   O  O  O  -  O  O
16 0-4   P  P  -  -  P  P
17 0-4   Q  -  Q  -  Q  -
18 1-3   -  -  R  -  -   
19 1-4   -  S  S  S  S   
20 2-3   T  -  T  T      
21 2-4   U  U  -  U      
22 2-4   -  V  V         
23 2-4   W  -  -         
24 2-4   -  -            
25 2-5   -  Z            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

TABGX NVXNZ XZXRK MHOHP GNVAN T
-------------------------------
