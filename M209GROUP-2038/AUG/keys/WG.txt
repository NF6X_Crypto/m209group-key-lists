EFFECTIVE PERIOD:
14-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   B  B  B  -  B  B
03 2-0   -  C  C  C  C  C
04 2-0   -  D  -  D  -  D
05 0-3   -  -  E  -  E  -
06 0-3   F  F  F  -  F  F
07 0-3   G  -  G  G  G  -
08 0-3   H  H  H  H  H  H
09 0-3   -  -  -  I  -  -
10 0-3   -  -  -  J  -  -
11 0-3   -  K  -  K  -  K
12 0-6   -  L  -  -  -  -
13 0-6   M  M  M  M  -  -
14 0-6   -  -  -  N  N  N
15 0-6   -  O  O  -  O  -
16 1-5   P  -  P  P  -  P
17 2-3   Q  -  -  -  -  -
18 2-5   R  R  R  -  R   
19 2-6   -  -  -  S  -   
20 2-6   -  T  -  T      
21 3-6   U  U  U  -      
22 3-6   V  -  V         
23 3-6   W  X  X         
24 3-6   X  Y            
25 4-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TNPTL TLVAA ONAKA QUVJA MLAIU A
-------------------------------
