EFFECTIVE PERIOD:
17-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   -  -  -  -  -  -
03 1-0   C  -  -  -  -  C
04 2-0   D  -  -  -  D  D
05 2-0   E  E  E  -  E  -
06 2-0   F  -  F  F  -  F
07 2-0   -  -  -  G  -  G
08 2-0   H  -  -  -  -  -
09 2-0   -  -  I  I  -  -
10 2-0   J  J  -  J  -  J
11 2-0   -  -  -  -  K  -
12 2-0   L  L  -  L  -  L
13 2-0   -  -  -  M  M  -
14 0-3   -  -  N  -  N  N
15 0-4   -  O  O  O  -  -
16 0-4   -  -  P  -  P  -
17 0-4   Q  Q  -  Q  Q  Q
18 0-4   -  R  R  -  R   
19 0-4   -  S  S  -  S   
20 0-5   T  T  -  -      
21 0-5   U  U  -  -      
22 0-5   V  -  V         
23 0-5   -  -  -         
24 1-3   X  Y            
25 1-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SLXKA VUGGV MPGFM WFFQZ ZKONI X
-------------------------------
