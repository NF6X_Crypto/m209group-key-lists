EFFECTIVE PERIOD:
11-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  B  B  B  -  -
03 1-0   -  -  C  -  C  -
04 1-0   -  -  D  D  D  -
05 1-0   -  E  -  -  E  E
06 1-0   F  -  -  -  -  F
07 1-0   G  -  -  -  G  G
08 2-0   -  H  -  -  -  H
09 2-0   -  -  -  I  -  I
10 2-0   -  -  J  J  J  -
11 2-0   -  K  K  -  K  -
12 0-3   -  -  -  L  -  -
13 0-3   M  -  M  M  M  M
14 0-3   N  N  -  -  N  N
15 0-4   O  -  -  -  -  O
16 0-4   -  P  P  P  P  P
17 0-4   -  Q  Q  Q  -  Q
18 0-4   -  -  R  R  -   
19 0-4   S  S  S  S  -   
20 1-3   -  -  -  -      
21 1-3   U  -  U  U      
22 1-3   -  V  -         
23 1-3   W  -  X         
24 1-6   -  Y            
25 2-4   Y  Z            
26 2-5   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

GFOVT VHVGV LQAIB GLTLI PMIMX V
-------------------------------
