EFFECTIVE PERIOD:
05-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  -  B  -  B  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  D  -  -  D
05 1-0   -  E  -  E  -  -
06 1-0   -  -  F  F  F  F
07 1-0   -  G  -  -  -  G
08 1-0   H  H  -  -  H  -
09 1-0   I  I  I  -  I  -
10 1-0   -  -  J  J  -  J
11 1-0   -  K  -  -  -  -
12 1-0   L  -  L  L  -  -
13 1-0   M  -  M  M  M  M
14 2-0   N  N  -  -  -  N
15 0-3   O  -  O  O  O  O
16 0-3   P  P  P  -  P  P
17 0-3   -  -  -  Q  Q  Q
18 0-3   R  R  -  R  -   
19 0-3   -  S  -  S  -   
20 0-4   -  -  T  T      
21 0-4   -  -  -  -      
22 0-5   -  -  -         
23 0-5   -  -  X         
24 0-5   X  Y            
25 0-6   -  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QKWGI HJDQL GRAZE KEBWU HKWUX V
-------------------------------
