EFFECTIVE PERIOD:
18-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  B  B  -  -  -
03 1-0   -  C  C  -  C  C
04 1-0   D  -  -  D  -  D
05 0-3   E  -  E  -  E  -
06 0-3   F  -  -  -  -  -
07 0-3   G  G  -  -  G  G
08 0-3   H  H  -  -  H  H
09 0-3   I  -  -  I  -  -
10 0-3   -  J  -  -  J  J
11 0-5   K  -  K  K  K  K
12 0-5   -  -  -  -  -  -
13 0-5   -  -  M  M  M  -
14 0-5   N  -  -  N  -  N
15 0-6   -  O  O  -  O  -
16 0-6   -  P  P  P  -  -
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  R  -   
19 0-6   S  S  -  -  -   
20 1-3   -  -  T  -      
21 1-6   -  U  -  -      
22 1-6   -  -  V         
23 1-6   -  -  -         
24 1-6   -  Y            
25 2-6   -  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KNQAJ AIJHK AQVJA OOUPJ WJMZW J
-------------------------------
