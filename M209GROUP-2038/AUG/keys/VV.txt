EFFECTIVE PERIOD:
03-AUG-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  -  -  B  B  B
03 1-0   C  -  C  C  -  -
04 1-0   D  D  D  -  -  -
05 1-0   -  -  E  -  E  E
06 1-0   F  -  F  F  F  F
07 1-0   G  G  -  G  -  -
08 2-0   H  H  H  -  H  -
09 2-0   -  -  I  I  I  I
10 2-0   J  -  J  J  -  -
11 2-0   -  K  -  K  K  K
12 2-0   L  L  L  L  L  L
13 0-3   M  M  -  M  -  M
14 0-5   N  N  -  -  -  N
15 0-5   -  O  O  -  -  O
16 0-5   P  -  -  P  -  -
17 0-5   Q  -  -  -  -  -
18 0-5   -  R  R  R  R   
19 0-6   S  S  -  S  S   
20 1-2   -  -  -  -      
21 1-2   -  U  U  -      
22 1-2   -  -  -         
23 1-2   W  -  -         
24 1-4   -  -            
25 2-5   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZPCUN AQUIG KUJUT KSVAK OORRT R
-------------------------------
