EFFECTIVE PERIOD:
09-OCT-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  C  -  -
04 1-0   -  -  D  D  D  D
05 1-0   -  E  E  -  E  -
06 1-0   F  F  -  F  F  F
07 1-0   -  -  G  -  -  G
08 1-0   H  H  H  H  H  -
09 1-0   I  I  -  -  I  -
10 2-0   -  -  -  -  -  -
11 2-0   -  K  -  K  -  -
12 2-0   -  -  -  -  L  L
13 0-3   M  M  M  M  M  -
14 0-4   N  N  -  -  N  N
15 0-4   O  -  O  O  O  -
16 0-4   -  P  -  P  -  P
17 0-4   -  -  -  -  -  -
18 0-4   -  R  -  -  -   
19 0-4   S  -  S  -  -   
20 0-5   T  -  -  T      
21 0-6   U  -  U  -      
22 1-4   V  -  V         
23 1-4   W  X  X         
24 1-4   -  Y            
25 2-4   -  -            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MIMZL OLVHO OQZQK GZASY DPEWZ L
-------------------------------
