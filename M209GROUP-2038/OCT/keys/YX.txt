EFFECTIVE PERIOD:
22-OCT-2038 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 2-0   -  -  -  B  -  -
03 2-0   -  C  C  C  -  C
04 2-0   -  -  -  -  D  D
05 2-0   E  -  E  -  -  -
06 2-0   F  F  -  -  F  F
07 2-0   G  -  G  G  G  -
08 2-0   H  H  H  -  -  -
09 2-0   -  I  I  I  I  I
10 2-0   J  -  -  J  J  J
11 0-3   -  K  -  -  K  -
12 0-3   -  L  L  -  -  L
13 0-3   -  M  -  M  M  M
14 0-3   N  -  N  -  N  N
15 0-3   -  O  O  O  -  -
16 0-4   P  P  P  P  P  P
17 0-5   Q  Q  -  -  Q  -
18 0-5   R  R  R  R  -   
19 0-5   -  -  S  S  S   
20 0-5   T  -  -  -      
21 0-5   -  -  -  U      
22 0-5   -  V  V         
23 0-5   W  -  -         
24 0-5   X  -            
25 0-6   -  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RZAMO QCRID DLHQN EZQAK KYKYQ O
-------------------------------
