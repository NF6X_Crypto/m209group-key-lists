EFFECTIVE PERIOD:
19-MAR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 0-3   -  B  B  B  B  B
03 0-3   -  C  C  -  -  -
04 0-3   -  -  -  -  -  D
05 0-3   E  -  E  E  -  E
06 0-3   F  -  -  F  F  F
07 0-3   -  -  -  G  -  G
08 0-3   -  -  H  -  H  H
09 0-3   -  -  -  I  I  I
10 0-3   J  J  J  -  -  -
11 0-3   -  K  -  K  -  -
12 0-3   -  -  -  -  -  L
13 0-3   M  M  M  M  M  -
14 0-5   N  N  -  N  -  -
15 0-5   -  O  O  O  O  -
16 0-5   -  P  -  -  P  -
17 0-5   Q  -  Q  -  -  Q
18 0-5   -  R  -  R  R   
19 0-6   S  S  S  -  -   
20 0-6   T  -  T  -      
21 0-6   -  U  U  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 0-6   X  Y            
25 0-6   -  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

BABTH NAMVG UTTNU OGNGN HBSON N
-------------------------------
