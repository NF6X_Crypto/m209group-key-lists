EFFECTIVE PERIOD:
22-MAR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  A
02 2-0   -  -  -  -  -  B
03 2-0   C  C  -  C  C  -
04 2-0   -  D  D  D  D  D
05 0-3   -  E  -  -  -  -
06 0-3   F  -  F  F  -  F
07 0-3   -  -  -  -  G  G
08 0-3   H  H  H  -  H  H
09 0-4   I  I  -  I  -  I
10 0-4   J  -  -  J  J  J
11 0-4   -  -  K  -  -  -
12 0-4   L  L  L  -  -  L
13 0-4   M  -  -  -  -  M
14 0-5   N  -  -  N  N  -
15 0-5   -  -  O  O  -  -
16 0-5   -  P  P  P  -  -
17 0-5   Q  Q  -  -  -  -
18 0-5   -  -  R  R  -   
19 0-5   S  S  -  S  S   
20 1-3   T  -  T  -      
21 2-4   -  -  -  U      
22 2-4   -  -  V         
23 2-4   W  -  X         
24 2-4   X  Y            
25 2-5   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZUAKO JPJWO ISJPV VBWPK FMPKP G
-------------------------------
