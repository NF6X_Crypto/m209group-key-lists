EFFECTIVE PERIOD:
11-MAR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  -  -  A
02 0-3   B  -  B  B  B  -
03 0-3   C  -  C  C  C  C
04 0-3   -  D  -  -  D  -
05 0-3   -  E  -  -  -  -
06 0-4   -  -  F  -  -  -
07 0-5   G  -  -  G  -  G
08 0-5   -  H  -  H  -  -
09 0-5   I  I  I  -  -  -
10 0-5   -  J  J  J  J  J
11 0-5   K  -  -  -  K  K
12 0-5   -  L  -  -  -  L
13 0-5   M  -  -  M  M  -
14 0-5   -  -  N  -  N  N
15 0-6   -  O  O  O  -  O
16 0-6   P  -  P  -  -  P
17 0-6   -  Q  -  Q  -  -
18 0-6   R  -  -  R  R   
19 0-6   S  -  -  S  S   
20 0-6   -  T  -  T      
21 0-6   U  -  -  -      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 0-6   -  Y            
25 0-6   -  -            
26 1-2   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

SYPLL NJVGU OPGLB HLTAG UUMHQ P
-------------------------------
