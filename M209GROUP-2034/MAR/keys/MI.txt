EFFECTIVE PERIOD:
18-MAR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  B  -  B  B
03 1-0   C  C  C  C  C  -
04 1-0   -  D  D  -  -  -
05 1-0   -  -  E  -  -  -
06 1-0   F  -  F  F  F  -
07 0-3   -  G  -  G  G  G
08 0-3   -  -  -  -  -  H
09 0-3   -  -  I  -  -  I
10 0-4   -  -  -  J  J  -
11 0-4   K  K  -  K  K  K
12 0-5   L  L  L  -  -  L
13 0-5   -  -  -  -  M  -
14 0-5   N  -  -  N  N  N
15 0-5   O  O  O  O  -  -
16 1-3   P  -  -  -  -  -
17 1-4   Q  Q  -  Q  -  -
18 2-4   -  -  R  -  R   
19 2-5   S  S  -  -  -   
20 2-6   -  T  T  -      
21 3-5   U  -  U  U      
22 3-5   -  -  -         
23 3-5   W  -  -         
24 3-5   -  Y            
25 4-5   -  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

KAQIQ KPPQN NOOYU PHNUG MYPGP X
-------------------------------
