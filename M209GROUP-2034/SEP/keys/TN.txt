EFFECTIVE PERIOD:
21-SEP-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  B  B  -  B  B
03 1-0   C  -  C  -  -  -
04 1-0   -  -  D  D  D  D
05 1-0   E  E  -  E  E  -
06 1-0   -  F  -  F  F  F
07 1-0   G  -  G  G  G  G
08 1-0   H  -  -  H  -  H
09 1-0   -  -  -  -  -  -
10 2-0   J  J  J  -  J  -
11 2-0   K  K  K  -  -  -
12 2-0   L  L  L  -  -  -
13 0-3   -  -  M  M  M  -
14 0-3   -  -  -  -  N  -
15 0-3   O  O  -  O  -  O
16 0-3   -  -  P  -  P  -
17 0-4   -  -  -  Q  Q  Q
18 0-4   -  -  R  R  R   
19 0-4   -  -  -  -  -   
20 0-5   -  -  -  T      
21 0-6   U  U  -  -      
22 1-3   -  -  V         
23 1-3   -  X  -         
24 1-3   X  Y            
25 2-3   -  -            
26 2-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IANZO RHFWF BRAXG UPMLH VAUGC A
-------------------------------
