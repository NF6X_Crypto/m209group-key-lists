EFFECTIVE PERIOD:
15-SEP-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   B  B  B  -  -  -
03 2-0   C  C  C  C  -  -
04 2-0   D  D  D  -  D  D
05 2-0   -  E  -  -  -  -
06 0-3   -  -  F  F  -  -
07 0-3   G  -  -  -  G  G
08 0-3   H  H  -  H  H  -
09 0-3   I  -  I  -  I  -
10 0-4   -  J  J  J  -  J
11 0-4   -  -  -  -  K  -
12 0-6   -  -  -  L  -  L
13 0-6   -  M  M  -  M  -
14 0-6   N  N  -  -  N  N
15 0-6   -  O  O  O  -  O
16 0-6   -  P  P  -  -  -
17 0-6   Q  -  Q  -  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   S  S  S  -  S   
20 1-4   T  T  -  -      
21 2-3   -  -  U  U      
22 2-6   -  -  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 2-6   -  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JYJTT SMGOR NSPYO PQAAG WMFWQ P
-------------------------------
