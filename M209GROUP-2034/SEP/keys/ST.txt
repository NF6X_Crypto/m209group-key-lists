EFFECTIVE PERIOD:
01-SEP-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ST
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  B  -  B  -  B
03 1-0   C  -  -  -  C  -
04 1-0   -  D  D  D  -  -
05 1-0   -  E  E  E  -  E
06 2-0   -  F  F  F  -  -
07 2-0   -  G  -  G  G  -
08 0-3   H  H  H  H  -  -
09 0-3   I  -  I  -  I  -
10 0-3   -  -  -  J  J  -
11 0-4   K  -  -  K  K  K
12 0-4   -  -  L  -  -  L
13 0-4   -  M  -  -  M  -
14 0-4   -  -  N  N  N  N
15 0-4   -  -  O  O  O  O
16 0-4   -  -  P  -  P  -
17 0-4   Q  -  -  -  Q  Q
18 0-4   -  R  R  R  R   
19 0-4   S  -  -  -  -   
20 0-4   T  T  T  T      
21 0-6   -  U  -  -      
22 1-3   V  -  V         
23 1-4   W  -  -         
24 1-4   -  Y            
25 2-3   Y  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

HYRNO QQUHL SKVVU TQJAR COMVH H
-------------------------------
