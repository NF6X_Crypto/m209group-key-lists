EFFECTIVE PERIOD:
19-SEP-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  B  B  B  -  B
03 1-0   C  -  -  -  C  -
04 1-0   -  -  -  -  D  -
05 1-0   -  -  -  -  -  E
06 2-0   F  F  F  F  -  -
07 2-0   -  G  -  -  -  G
08 2-0   -  H  H  H  H  H
09 2-0   I  -  -  I  -  -
10 2-0   J  J  J  J  -  J
11 2-0   -  K  -  -  -  -
12 0-3   L  -  L  L  L  -
13 0-3   -  M  -  M  M  -
14 0-3   N  -  N  N  -  -
15 0-6   -  -  O  O  -  O
16 0-6   P  -  P  -  -  P
17 0-6   -  -  -  Q  Q  -
18 1-3   R  -  R  -  R   
19 1-4   S  -  S  S  -   
20 1-6   -  T  -  -      
21 1-6   -  -  U  -      
22 1-6   V  V  V         
23 1-6   -  X  X         
24 2-3   -  Y            
25 2-6   Y  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QHQKP UMMUX KUSOK GXOKQ UVQOQ P
-------------------------------
