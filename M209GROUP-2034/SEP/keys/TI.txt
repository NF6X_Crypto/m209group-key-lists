EFFECTIVE PERIOD:
16-SEP-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  A  -  A
02 0-4   B  B  -  -  B  -
03 0-4   -  -  C  C  C  C
04 0-4   -  -  D  D  D  -
05 0-4   E  -  -  E  -  E
06 0-5   -  -  -  F  -  -
07 0-5   G  G  G  G  G  -
08 0-5   -  H  -  -  H  H
09 0-5   -  -  -  -  -  -
10 0-5   J  J  -  J  J  -
11 0-6   K  -  -  K  -  K
12 0-6   L  -  -  -  L  -
13 0-6   -  M  M  -  M  M
14 0-6   N  N  N  -  -  -
15 0-6   O  O  -  O  O  O
16 1-2   -  P  P  P  -  P
17 2-3   Q  Q  -  -  Q  -
18 2-6   R  -  R  -  -   
19 2-6   -  S  S  S  -   
20 3-4   -  -  -  -      
21 3-6   U  U  U  -      
22 3-6   V  -  V         
23 4-5   W  -  -         
24 5-6   X  Y            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WNVOQ WJQFZ RZMJX PEUJX NNWMZ H
-------------------------------
