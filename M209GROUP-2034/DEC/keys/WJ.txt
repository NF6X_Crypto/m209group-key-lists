EFFECTIVE PERIOD:
04-DEC-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  B  B  B  B  B
03 1-0   -  C  C  C  -  -
04 1-0   D  D  D  -  D  D
05 2-0   E  -  E  E  E  E
06 2-0   F  F  -  -  -  -
07 2-0   G  G  G  G  -  G
08 2-0   -  -  H  -  -  H
09 0-3   -  I  -  I  -  -
10 0-3   J  -  -  -  -  -
11 0-3   K  K  K  K  K  K
12 0-3   L  L  L  L  L  L
13 0-3   -  -  M  M  M  -
14 0-4   N  N  -  -  N  -
15 0-4   -  O  O  O  -  -
16 0-4   -  -  -  P  -  -
17 0-4   Q  -  Q  -  -  Q
18 0-4   R  -  R  -  R   
19 0-4   -  -  -  S  S   
20 0-4   T  T  -  -      
21 0-5   -  U  -  -      
22 1-3   -  V  -         
23 1-6   -  X  X         
24 2-3   -  -            
25 2-4   -  Z            
26 2-4   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

QMQQL HQJIH UWOVS PSZBP WASKZ J
-------------------------------
