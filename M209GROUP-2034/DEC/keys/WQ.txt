EFFECTIVE PERIOD:
11-DEC-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  -  -  -  -
03 1-0   -  C  C  -  -  -
04 1-0   D  D  D  -  D  -
05 0-3   E  E  E  E  E  E
06 0-3   F  -  F  F  F  F
07 0-3   G  G  -  G  -  -
08 0-5   -  -  -  -  H  H
09 0-5   -  -  I  -  I  I
10 0-5   J  J  J  J  -  J
11 0-5   -  -  -  K  -  -
12 0-5   -  -  -  -  -  L
13 0-5   M  -  M  M  M  M
14 0-6   -  N  N  N  -  -
15 0-6   -  O  O  -  -  O
16 1-5   -  -  P  -  -  P
17 1-6   Q  Q  Q  Q  Q  -
18 1-6   R  R  -  R  -   
19 1-6   S  -  -  S  S   
20 1-6   T  T  T  -      
21 2-3   U  -  -  -      
22 2-4   -  -  -         
23 2-6   -  -  -         
24 3-5   -  Y            
25 3-6   Y  Z            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

JAUSP XIHZL CXZQU AVLPP SKPQQ J
-------------------------------
