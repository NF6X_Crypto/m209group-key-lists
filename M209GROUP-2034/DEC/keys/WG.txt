EFFECTIVE PERIOD:
01-DEC-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  -  B  -  -  -
03 1-0   -  C  C  C  C  C
04 1-0   D  -  D  D  D  -
05 1-0   E  -  -  -  -  -
06 1-0   -  -  F  F  -  F
07 1-0   -  -  G  G  G  -
08 1-0   H  H  -  -  -  -
09 1-0   -  I  I  I  -  -
10 1-0   -  J  J  J  J  -
11 1-0   K  -  -  -  -  K
12 1-0   L  -  -  L  L  L
13 1-0   M  M  -  -  M  M
14 2-0   -  -  -  -  N  N
15 2-0   O  O  O  -  -  -
16 0-3   -  -  P  -  P  P
17 0-4   Q  Q  -  Q  Q  -
18 0-4   R  R  -  R  -   
19 0-4   S  -  -  -  -   
20 0-4   T  T  -  T      
21 0-4   U  -  U  -      
22 0-6   V  V  V         
23 0-6   -  X  X         
24 2-3   X  Y            
25 2-6   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

HTVTT ZMNSV ALLFT VCJAI MNQRG Y
-------------------------------
