SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF OCT 2034

    01 OCT 2034  00:00-23:59 GMT:  USE KEY TX
    02 OCT 2034  00:00-23:59 GMT:  USE KEY TY
    03 OCT 2034  00:00-23:59 GMT:  USE KEY TZ
    04 OCT 2034  00:00-23:59 GMT:  USE KEY UA
    05 OCT 2034  00:00-23:59 GMT:  USE KEY UB
    06 OCT 2034  00:00-23:59 GMT:  USE KEY UC
    07 OCT 2034  00:00-23:59 GMT:  USE KEY UD
    08 OCT 2034  00:00-23:59 GMT:  USE KEY UE
    09 OCT 2034  00:00-23:59 GMT:  USE KEY UF
    10 OCT 2034  00:00-23:59 GMT:  USE KEY UG
    11 OCT 2034  00:00-23:59 GMT:  USE KEY UH
    12 OCT 2034  00:00-23:59 GMT:  USE KEY UI
    13 OCT 2034  00:00-23:59 GMT:  USE KEY UJ
    14 OCT 2034  00:00-23:59 GMT:  USE KEY UK
    15 OCT 2034  00:00-23:59 GMT:  USE KEY UL
    16 OCT 2034  00:00-23:59 GMT:  USE KEY UM
    17 OCT 2034  00:00-23:59 GMT:  USE KEY UN
    18 OCT 2034  00:00-23:59 GMT:  USE KEY UO
    19 OCT 2034  00:00-23:59 GMT:  USE KEY UP
    20 OCT 2034  00:00-23:59 GMT:  USE KEY UQ
    21 OCT 2034  00:00-23:59 GMT:  USE KEY UR
    22 OCT 2034  00:00-23:59 GMT:  USE KEY US
    23 OCT 2034  00:00-23:59 GMT:  USE KEY UT
    24 OCT 2034  00:00-23:59 GMT:  USE KEY UU
    25 OCT 2034  00:00-23:59 GMT:  USE KEY UV
    26 OCT 2034  00:00-23:59 GMT:  USE KEY UW
    27 OCT 2034  00:00-23:59 GMT:  USE KEY UX
    28 OCT 2034  00:00-23:59 GMT:  USE KEY UY
    29 OCT 2034  00:00-23:59 GMT:  USE KEY UZ
    30 OCT 2034  00:00-23:59 GMT:  USE KEY VA
    31 OCT 2034  00:00-23:59 GMT:  USE KEY VB

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  B  B  B  B  B
03 1-0   C  -  -  C  -  C
04 1-0   -  -  D  -  D  -
05 1-0   -  -  E  E  E  E
06 1-0   -  F  -  F  -  -
07 2-0   G  G  -  -  G  -
08 2-0   H  H  -  -  H  -
09 2-0   -  I  I  -  I  I
10 2-0   J  -  -  J  -  J
11 2-0   -  K  -  -  -  -
12 2-0   L  L  -  L  L  L
13 2-0   M  -  M  -  -  -
14 0-3   -  -  N  N  -  N
15 0-3   O  -  -  O  O  O
16 0-3   P  -  P  P  -  -
17 0-3   Q  Q  Q  -  -  Q
18 0-4   R  R  R  -  R   
19 0-6   -  -  -  -  -   
20 1-2   T  -  -  T      
21 1-2   U  U  -  -      
22 1-2   -  V  V         
23 1-2   W  -  -         
24 1-3   X  -            
25 2-5   -  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QOZUR KNWSE CULRI CNSAL ONZSD S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 2-0   B  B  B  B  -  B
03 2-0   C  -  -  -  C  -
04 2-0   -  D  -  -  D  -
05 0-3   E  -  -  -  E  -
06 0-3   -  -  F  -  -  -
07 0-3   -  G  -  G  G  -
08 0-4   H  H  H  -  -  H
09 0-4   -  I  I  I  I  -
10 0-4   J  J  -  J  -  J
11 0-4   -  -  K  K  -  -
12 0-4   L  -  L  L  -  L
13 0-4   -  M  M  -  M  -
14 0-4   -  -  N  -  N  -
15 0-4   -  -  -  O  -  -
16 0-6   -  P  -  -  P  P
17 0-6   Q  Q  Q  -  -  Q
18 0-6   R  R  -  R  R   
19 0-6   S  S  -  -  S   
20 1-2   T  T  -  T      
21 2-6   -  -  U  -      
22 3-4   -  V  -         
23 3-4   W  -  X         
24 3-4   -  -            
25 3-4   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HLSNG WGAGH VOGBK SGVAR WIOZF G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 2-0   B  B  -  -  B  -
03 2-0   -  -  -  -  C  C
04 0-3   D  D  D  -  D  D
05 0-3   E  E  E  E  -  E
06 0-3   F  F  F  F  -  F
07 0-3   -  G  -  G  -  G
08 0-3   -  -  H  H  -  -
09 0-4   -  I  I  I  I  I
10 0-4   J  -  J  -  J  -
11 0-4   -  K  -  K  K  -
12 0-4   L  L  -  L  -  -
13 0-5   -  M  M  -  M  M
14 0-5   N  N  N  -  N  -
15 0-5   -  O  -  O  -  -
16 0-5   P  -  -  -  -  -
17 0-5   Q  -  Q  -  Q  Q
18 0-5   R  -  -  -  -   
19 0-5   -  S  S  -  -   
20 0-5   T  -  -  -      
21 0-6   -  U  -  -      
22 1-2   V  -  V         
23 2-3   -  -  X         
24 3-4   -  Y            
25 4-5   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KMSGE RFZRZ UPFOR SUYRR ROKRP D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  -  -  -  -  -
03 1-0   C  C  C  C  C  -
04 1-0   -  -  D  D  D  D
05 1-0   E  E  -  -  -  E
06 1-0   -  F  F  F  -  -
07 1-0   G  G  -  -  G  -
08 0-3   -  -  H  H  H  H
09 0-4   I  I  I  I  -  I
10 0-4   -  J  J  -  J  J
11 0-4   -  K  -  K  -  -
12 0-4   -  L  -  L  -  -
13 0-4   -  -  -  M  -  -
14 0-4   -  -  -  N  N  N
15 0-5   O  -  -  -  -  O
16 0-5   P  P  -  P  P  -
17 0-5   -  Q  Q  Q  -  Q
18 1-4   -  R  -  -  R   
19 1-4   S  S  S  -  -   
20 1-4   -  -  T  -      
21 1-4   U  U  -  -      
22 1-5   -  -  -         
23 2-3   W  X  X         
24 2-4   X  -            
25 3-4   Y  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UNGXS PJMNX TRXQR UNADX NSNFM Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 2-0   B  -  B  -  B  -
03 2-0   -  -  C  C  C  -
04 0-3   -  -  D  -  D  -
05 0-3   E  E  -  E  E  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  -  G  -  -
08 0-4   -  -  -  -  -  -
09 0-4   I  I  I  I  I  I
10 0-4   J  J  J  J  J  J
11 0-4   -  K  -  K  K  -
12 0-5   -  L  L  -  L  -
13 0-5   -  -  M  -  -  M
14 0-5   N  -  N  -  -  N
15 0-5   O  -  -  O  -  O
16 0-5   -  -  -  -  P  -
17 0-5   -  -  -  -  -  -
18 0-5   R  R  R  R  -   
19 0-5   S  S  S  S  S   
20 1-2   -  T  -  T      
21 1-6   U  -  U  -      
22 2-4   -  -  -         
23 3-4   W  X  -         
24 3-5   -  Y            
25 3-5   -  -            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

QZIFV SFYPP LATNZ KQWVA FNMVC C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  -  B  -  B  B
03 1-0   -  C  C  C  C  C
04 2-0   D  -  -  D  -  D
05 0-3   E  E  -  -  -  E
06 0-4   F  -  F  -  -  -
07 0-4   -  G  -  G  -  -
08 0-4   -  H  H  H  H  H
09 0-4   I  -  I  I  -  -
10 0-4   J  -  -  -  J  -
11 0-4   -  K  -  -  -  K
12 0-4   -  L  -  L  L  -
13 0-4   -  -  M  M  -  M
14 0-4   N  -  N  -  N  -
15 0-4   -  O  -  -  O  -
16 0-4   P  P  P  P  -  -
17 0-4   -  -  -  -  Q  -
18 0-5   -  R  -  R  -   
19 0-5   S  S  S  -  S   
20 0-6   T  T  -  T      
21 0-6   U  U  U  U      
22 0-6   -  -  V         
23 0-6   -  X  X         
24 1-5   X  -            
25 1-6   Y  Z            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ABKVG WBWAN LSSBJ WRXCM RRFAY I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 2-0   -  -  B  -  -  -
03 2-0   C  C  -  C  -  C
04 2-0   D  D  -  D  -  D
05 2-0   E  -  -  -  -  -
06 2-0   F  -  F  F  F  -
07 2-0   -  G  -  -  G  G
08 2-0   H  -  -  -  H  H
09 2-0   -  I  -  -  I  I
10 0-3   J  -  J  J  J  J
11 0-4   -  K  K  -  K  -
12 0-4   -  -  -  -  -  -
13 0-5   -  -  M  -  -  M
14 0-5   -  N  -  -  N  N
15 0-5   O  -  -  -  -  O
16 0-5   -  -  -  P  -  -
17 0-5   Q  Q  Q  Q  -  -
18 0-5   R  R  R  R  -   
19 0-5   S  S  -  -  -   
20 0-6   T  -  T  T      
21 0-6   -  U  -  -      
22 0-6   -  V  V         
23 0-6   W  -  X         
24 1-4   X  Y            
25 2-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZBXTT DJNLS GFDXZ QPQNP IVAZM E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  A  A
02 0-3   -  -  -  -  -  -
03 0-3   -  -  C  -  C  -
04 0-3   D  -  D  D  D  D
05 0-4   E  E  -  E  -  E
06 0-4   -  -  F  F  F  -
07 0-4   -  G  -  G  -  G
08 0-4   H  -  H  H  -  -
09 0-4   -  I  I  I  I  -
10 0-4   -  J  J  -  J  -
11 0-4   -  K  -  K  -  -
12 0-5   L  -  L  -  L  L
13 0-5   -  M  -  -  -  M
14 0-5   -  N  N  N  -  N
15 0-5   -  O  -  -  O  O
16 1-5   -  -  -  -  P  -
17 1-6   Q  -  Q  Q  -  Q
18 2-5   R  -  -  -  -   
19 3-4   -  -  S  S  -   
20 3-5   -  T  T  T      
21 3-5   U  -  -  -      
22 3-5   V  V  V         
23 3-6   W  X  -         
24 4-5   X  Y            
25 4-5   Y  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SOCTW LVPTC TOTAN WUZKA CKVTC M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  -  B  -  -  -
03 1-0   -  C  -  -  C  C
04 1-0   D  -  D  -  -  -
05 1-0   E  -  E  E  -  E
06 2-0   -  F  -  -  F  -
07 2-0   -  -  G  G  G  G
08 2-0   H  -  -  H  -  -
09 2-0   -  I  I  I  I  -
10 2-0   -  J  -  -  -  J
11 0-4   -  -  -  -  K  -
12 0-4   L  -  L  L  -  L
13 0-4   M  M  -  -  -  -
14 0-4   -  N  -  N  -  N
15 0-4   -  -  -  O  O  O
16 0-4   -  P  -  -  P  P
17 0-6   Q  Q  Q  -  Q  -
18 1-2   R  -  R  -  -   
19 1-4   -  -  S  S  S   
20 1-4   -  T  -  T      
21 1-4   U  U  -  U      
22 1-4   V  -  V         
23 2-6   W  -  -         
24 3-4   -  Y            
25 3-6   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZAYFF UYSNA MTZVM SVMPG VEVYH Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  -  A
02 0-3   -  B  -  B  -  B
03 0-3   C  C  C  C  C  C
04 0-3   D  -  D  -  D  -
05 0-4   -  E  -  -  -  -
06 0-5   F  F  -  -  -  -
07 0-5   -  G  -  -  G  G
08 0-5   H  H  -  H  -  -
09 0-5   I  -  I  -  -  -
10 0-5   J  -  -  -  J  J
11 0-6   -  K  K  K  -  -
12 0-6   -  L  L  -  -  L
13 0-6   M  M  -  M  M  M
14 0-6   -  -  -  -  N  -
15 0-6   -  -  -  -  O  O
16 0-6   P  -  P  -  P  -
17 0-6   Q  Q  Q  Q  -  -
18 1-3   -  -  R  R  -   
19 1-4   -  -  -  S  -   
20 2-3   T  T  T  T      
21 3-4   -  U  U  U      
22 3-5   -  -  V         
23 3-5   -  X  X         
24 3-5   X  -            
25 3-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ETYQA ENVVU OOKUL ANKUM RAKMI V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   -  -  B  -  -  -
03 2-0   -  C  C  C  C  C
04 2-0   -  D  -  D  -  D
05 2-0   E  -  -  -  -  E
06 2-0   -  F  F  -  -  F
07 0-3   -  -  -  G  G  G
08 0-3   H  H  -  -  -  -
09 0-3   -  -  -  -  I  I
10 0-3   -  J  -  J  -  -
11 0-3   K  -  K  K  K  K
12 0-3   -  L  L  L  -  -
13 0-5   -  M  M  -  -  -
14 0-5   N  -  -  -  N  -
15 0-5   O  -  O  -  -  -
16 0-5   P  P  P  -  P  -
17 0-5   Q  Q  -  Q  -  -
18 1-5   R  R  R  R  R   
19 2-3   -  S  S  S  -   
20 2-4   -  T  -  T      
21 3-5   U  U  -  U      
22 3-5   -  -  V         
23 3-5   -  X  X         
24 3-5   X  -            
25 4-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

CSZHA UUDAO TPOJT PTBUC TSRTN U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 0-3   B  B  -  -  B  -
03 0-3   -  C  C  -  -  -
04 0-3   D  -  D  -  D  D
05 0-3   E  E  E  E  -  -
06 0-3   -  -  F  -  -  F
07 0-3   -  -  -  -  -  G
08 0-3   H  H  -  H  H  H
09 0-5   I  I  -  I  I  -
10 0-5   -  J  -  J  -  J
11 0-5   -  K  -  K  -  K
12 0-5   L  L  L  L  L  -
13 0-5   -  -  -  M  M  -
14 0-5   N  N  -  -  N  -
15 0-6   -  O  O  O  -  -
16 0-6   P  P  -  P  -  -
17 0-6   Q  -  -  -  Q  Q
18 0-6   -  -  R  R  -   
19 0-6   S  -  S  -  S   
20 1-3   T  T  -  T      
21 2-4   -  -  U  -      
22 3-5   -  V  V         
23 3-5   W  X  X         
24 3-5   X  -            
25 3-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TUMCL VGANU GLUUS URHDO NVCHA B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 2-0   B  -  -  B  -  -
03 2-0   -  C  -  -  C  -
04 2-0   D  -  D  D  D  -
05 2-0   E  E  E  -  -  E
06 2-0   -  F  -  F  -  F
07 2-0   G  -  -  G  G  -
08 0-3   H  -  -  -  -  -
09 0-3   -  I  -  I  I  I
10 0-3   -  -  J  -  -  -
11 0-3   K  -  -  -  -  K
12 0-3   -  -  L  -  -  L
13 0-4   -  M  M  -  M  M
14 0-4   N  N  -  -  -  -
15 0-4   -  O  -  O  -  -
16 0-4   P  -  -  -  P  P
17 0-4   -  -  Q  -  Q  -
18 0-4   -  -  R  R  -   
19 0-4   S  -  S  S  S   
20 1-6   T  T  T  -      
21 2-3   -  -  -  U      
22 2-3   V  V  V         
23 2-3   W  X  -         
24 2-3   X  Y            
25 2-5   -  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NCJKL ASNAL VNJRT VLJBT AKLUN L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  -  -  B  -  B
03 1-0   -  C  -  C  C  C
04 1-0   D  -  D  -  -  -
05 2-0   E  E  -  -  -  -
06 2-0   F  F  F  -  -  -
07 2-0   -  -  G  -  -  G
08 2-0   -  -  H  -  H  -
09 2-0   -  I  -  -  -  I
10 0-3   -  J  J  J  J  -
11 0-3   -  -  K  -  K  -
12 0-3   -  L  -  -  -  -
13 0-3   M  -  M  M  -  -
14 0-3   N  N  -  N  N  -
15 0-3   O  -  -  -  O  O
16 0-3   -  P  -  P  -  P
17 0-4   Q  -  Q  -  Q  -
18 0-4   R  -  R  R  -   
19 0-4   S  -  -  -  S   
20 0-4   -  T  T  -      
21 0-6   U  -  U  U      
22 1-2   -  -  -         
23 1-5   -  X  X         
24 2-4   -  Y            
25 3-4   -  -            
26 3-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

HSFUK TJLPV PHFRF WVKIQ QJWFT G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  B  B  -  -  -
03 1-0   -  -  -  -  -  -
04 1-0   -  D  -  -  D  -
05 1-0   -  -  E  E  E  E
06 1-0   F  -  F  -  -  F
07 1-0   -  G  G  G  -  G
08 1-0   -  -  -  H  H  -
09 1-0   I  -  I  -  I  -
10 1-0   J  -  J  -  J  J
11 2-0   -  K  K  K  -  -
12 0-3   -  -  L  L  -  L
13 0-3   -  -  -  M  M  -
14 0-4   N  N  N  N  N  -
15 0-4   O  O  -  -  O  O
16 0-4   P  P  -  -  P  P
17 0-4   -  Q  Q  Q  Q  Q
18 0-4   -  -  R  -  -   
19 0-4   S  S  -  S  -   
20 0-4   T  T  -  T      
21 0-4   -  -  U  U      
22 1-4   -  -  -         
23 1-4   W  -  X         
24 1-4   X  -            
25 3-4   Y  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZOCRA CMYXQ ZSPME PZCEY ZQNEP W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   -  -  -  -  -  B
03 1-0   -  C  C  C  C  C
04 1-0   D  D  -  D  -  D
05 2-0   -  E  E  -  -  E
06 2-0   F  -  -  -  F  -
07 2-0   G  -  -  G  G  -
08 2-0   -  H  H  -  -  H
09 2-0   -  -  I  I  I  -
10 2-0   -  -  -  -  J  -
11 2-0   K  K  K  K  -  -
12 0-5   -  L  -  -  L  L
13 0-5   M  M  M  -  -  M
14 0-5   N  -  -  N  -  N
15 0-5   O  -  -  -  -  -
16 1-2   P  -  -  P  P  P
17 1-5   -  -  -  Q  Q  Q
18 1-6   R  R  -  R  R   
19 2-5   -  -  S  S  -   
20 2-5   T  T  T  T      
21 2-5   -  U  U  -      
22 2-5   -  V  -         
23 3-4   W  -  X         
24 3-5   X  Y            
25 3-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

REJQS WWUSR NQCTA MRTRO FRRZS T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  -  B  B  -  B
03 1-0   C  C  -  -  -  -
04 1-0   D  D  D  D  D  D
05 2-0   -  E  E  -  -  -
06 2-0   F  -  F  F  F  -
07 2-0   -  -  -  -  G  -
08 2-0   -  -  -  -  H  -
09 2-0   I  I  -  -  -  I
10 2-0   -  -  J  J  -  J
11 0-3   K  -  K  K  -  K
12 0-3   -  L  -  -  -  -
13 0-3   -  M  -  -  M  M
14 0-3   N  N  N  N  -  N
15 0-4   O  -  -  O  O  -
16 0-5   -  -  P  -  P  P
17 0-5   Q  -  Q  Q  Q  -
18 0-5   R  R  R  R  -   
19 0-5   S  S  -  -  -   
20 1-3   T  -  T  -      
21 1-5   -  -  U  U      
22 2-3   -  V  -         
23 2-3   W  -  -         
24 2-3   X  Y            
25 2-3   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JMQZA HIUMM IOIYA VGACW PFRWV Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  -
02 2-0   B  B  -  B  -  B
03 2-0   C  C  -  C  -  C
04 2-0   -  -  D  -  D  -
05 2-0   -  -  -  -  E  E
06 0-5   -  -  F  -  -  F
07 0-5   G  G  -  -  -  G
08 0-5   H  H  H  H  -  -
09 0-5   -  I  -  I  I  -
10 0-5   -  J  J  J  -  -
11 0-5   -  -  -  K  K  -
12 0-6   -  -  -  -  -  L
13 0-6   M  -  -  -  -  -
14 0-6   N  -  N  -  N  -
15 0-6   O  -  -  -  -  O
16 1-3   -  P  P  P  -  P
17 1-5   Q  Q  Q  Q  Q  -
18 1-6   R  R  R  R  R   
19 1-6   S  -  -  -  -   
20 2-5   T  -  -  -      
21 2-6   -  U  U  U      
22 2-6   -  V  V         
23 2-6   -  -  -         
24 2-6   -  -            
25 3-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

BUVPO URLVO FAAUZ BJPAM JUMEM V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   B  -  -  -  B  B
03 0-3   -  C  -  -  -  C
04 0-3   -  D  -  D  -  D
05 0-3   -  -  -  -  E  -
06 0-4   F  F  F  -  -  -
07 0-4   G  -  -  -  -  -
08 0-4   H  -  H  H  H  H
09 0-4   -  I  I  -  -  -
10 0-4   -  -  -  -  J  J
11 0-4   K  K  -  K  K  -
12 0-4   L  -  L  -  L  -
13 0-5   M  M  -  -  M  M
14 0-6   N  N  N  N  N  N
15 0-6   O  -  O  O  -  O
16 0-6   -  -  P  -  P  P
17 0-6   Q  -  -  Q  -  -
18 0-6   R  -  -  -  -   
19 0-6   S  S  -  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  -  U      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   Y  -            
26 1-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

FNZNW NGIWT PAKAM FVGGW OTUOG Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  A
02 2-0   B  -  -  -  B  B
03 2-0   C  -  -  C  -  C
04 2-0   D  D  -  D  D  -
05 0-3   -  E  E  -  -  -
06 0-3   F  -  F  -  -  F
07 0-3   -  -  -  G  G  G
08 0-3   H  -  H  -  H  -
09 0-3   -  I  I  I  -  -
10 0-4   J  -  J  -  -  J
11 0-4   -  K  K  K  -  -
12 0-6   -  -  -  L  L  L
13 0-6   -  -  -  M  M  M
14 0-6   N  N  -  -  -  -
15 0-6   -  -  O  -  -  O
16 0-6   -  P  P  -  P  -
17 0-6   Q  -  -  Q  Q  Q
18 1-4   R  R  R  -  -   
19 1-5   -  -  S  -  S   
20 1-6   -  -  -  -      
21 2-3   -  -  -  U      
22 2-4   V  V  V         
23 3-6   W  -  X         
24 3-6   -  Y            
25 3-6   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

FYTFW KNTWA IGQNR TNOTA QUZMY V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  B  -  B  B  -
03 0-3   -  -  -  -  -  C
04 0-3   -  D  D  D  D  D
05 0-3   E  -  E  E  E  E
06 0-3   F  -  -  -  -  -
07 0-3   G  G  G  -  -  G
08 0-5   H  H  -  H  H  H
09 0-5   -  -  I  I  -  I
10 0-5   -  -  -  J  J  J
11 0-5   -  -  K  -  -  K
12 0-5   L  L  -  -  L  -
13 0-5   -  M  -  M  -  M
14 0-5   -  N  -  -  -  -
15 0-5   -  -  -  O  O  O
16 0-6   P  P  P  P  -  -
17 0-6   -  Q  -  -  -  -
18 0-6   R  R  R  -  R   
19 0-6   S  -  -  -  S   
20 1-4   -  -  T  T      
21 1-6   U  -  U  -      
22 2-5   -  V  -         
23 3-5   W  -  -         
24 3-5   -  -            
25 3-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

VASQO DSWMS QSQPP VRVOS JGQBB S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: US
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 2-0   -  -  -  -  B  -
03 2-0   C  C  C  C  -  -
04 2-0   -  D  -  -  D  -
05 0-3   -  E  -  -  -  E
06 0-3   -  -  F  F  F  -
07 0-3   -  -  G  G  -  -
08 0-3   -  H  -  -  H  -
09 0-4   I  I  I  -  -  -
10 0-4   J  -  -  J  -  -
11 0-4   -  -  -  -  -  K
12 0-4   -  L  L  -  -  L
13 0-5   M  -  M  M  M  M
14 0-5   N  N  -  N  N  N
15 0-5   O  O  O  -  O  O
16 0-5   P  -  -  -  -  -
17 0-5   Q  Q  -  Q  Q  -
18 0-5   R  -  -  R  R   
19 0-5   -  -  S  S  S   
20 0-5   -  T  T  T      
21 0-6   -  -  -  -      
22 2-3   -  -  V         
23 2-6   -  X  X         
24 3-4   -  Y            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

OXRMJ YURJZ GVFQM YFBRG NWYAW V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  -
02 2-0   -  B  B  B  -  -
03 2-0   C  -  -  C  C  C
04 2-0   -  -  D  D  D  D
05 2-0   E  -  -  E  -  E
06 2-0   -  -  -  -  -  F
07 2-0   G  G  G  -  G  -
08 0-3   H  H  H  -  H  -
09 0-3   -  I  I  I  -  I
10 0-3   -  J  -  -  J  -
11 0-6   -  K  K  -  K  K
12 0-6   -  -  -  L  -  -
13 0-6   -  -  M  M  -  -
14 0-6   N  -  N  -  N  N
15 0-6   -  -  -  -  O  -
16 1-4   P  -  -  -  P  P
17 2-5   Q  Q  Q  Q  -  -
18 2-6   R  -  R  R  -   
19 3-4   S  -  -  S  S   
20 3-4   -  T  T  -      
21 3-5   -  -  -  -      
22 3-5   V  -  V         
23 3-6   -  X  -         
24 3-6   X  -            
25 3-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

SAMTN VMDVK AAVMD OKAVI AOSMA D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   -  -  -  -  B  -
03 1-0   C  C  -  -  -  C
04 1-0   D  -  -  D  D  D
05 1-0   -  -  E  -  -  -
06 1-0   F  -  -  F  -  -
07 2-0   G  -  G  G  G  G
08 2-0   -  -  H  H  -  -
09 2-0   I  -  I  I  -  -
10 2-0   -  J  -  -  J  J
11 2-0   -  -  K  K  K  K
12 2-0   L  L  L  -  L  -
13 0-5   -  -  M  M  M  -
14 0-5   N  N  N  -  -  N
15 0-5   -  -  -  -  -  -
16 0-5   P  P  -  P  -  P
17 0-5   Q  -  -  Q  Q  Q
18 1-3   -  -  -  -  -   
19 1-3   -  -  -  -  -   
20 1-4   -  T  T  T      
21 1-5   -  U  U  U      
22 1-5   -  -  -         
23 1-5   W  X  X         
24 1-5   X  Y            
25 2-3   -  Z            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UZZBA RKOUU VQAVU AOUNS TEVDA A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  -  -  B  B
03 1-0   C  C  C  -  -  C
04 1-0   -  -  -  D  -  D
05 1-0   -  E  -  E  E  E
06 1-0   F  -  -  F  -  -
07 1-0   G  G  G  G  -  -
08 1-0   -  H  H  -  H  H
09 1-0   I  I  -  I  I  -
10 1-0   J  J  J  J  -  -
11 1-0   K  -  -  -  K  K
12 1-0   L  -  -  L  -  -
13 2-0   M  M  -  M  M  M
14 0-3   N  N  N  -  N  -
15 0-3   -  O  O  O  O  -
16 0-3   P  P  P  -  -  P
17 0-3   Q  Q  Q  -  -  -
18 0-3   -  -  R  -  R   
19 0-3   S  -  S  -  -   
20 0-3   -  -  -  T      
21 0-6   U  -  U  -      
22 0-6   -  V  V         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AUOTG YAANO GGUOG CTACZ ONSVZ O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  A  A
02 0-3   -  B  B  -  B  -
03 0-3   -  C  -  C  C  C
04 0-3   D  -  D  -  D  D
05 0-4   E  E  -  E  -  -
06 0-4   F  F  F  -  -  -
07 0-4   -  G  -  -  G  -
08 0-4   -  -  -  H  H  -
09 0-5   -  I  I  -  -  I
10 0-5   -  J  J  -  J  J
11 0-5   K  -  K  -  -  K
12 0-5   L  -  -  L  -  L
13 0-5   M  -  -  M  -  -
14 0-5   -  N  -  -  -  -
15 0-5   -  O  O  -  -  -
16 0-5   P  P  P  -  -  P
17 0-5   -  Q  Q  Q  Q  -
18 0-6   R  -  -  -  -   
19 0-6   S  -  -  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  U  -      
22 0-6   V  -  -         
23 0-6   W  X  X         
24 0-6   -  -            
25 0-6   -  Z            
26 1-2   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

ONFED REQLY TSCJJ WOTLF JEIAW J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  -  -  -  B  -
03 1-0   -  C  -  -  C  -
04 1-0   -  D  D  D  -  -
05 0-3   E  -  -  E  E  E
06 0-3   -  F  F  F  -  F
07 0-3   -  -  -  G  G  G
08 0-3   H  H  H  -  H  H
09 0-3   I  -  -  -  -  -
10 0-5   -  -  J  -  -  J
11 0-5   K  -  -  -  -  K
12 0-5   -  -  -  L  L  -
13 0-5   -  M  M  M  -  -
14 0-5   N  -  -  N  -  -
15 0-6   -  -  -  -  -  O
16 0-6   P  P  P  P  P  -
17 0-6   Q  Q  Q  -  -  -
18 1-3   -  R  R  R  R   
19 1-3   S  S  S  S  S   
20 1-3   T  T  -  -      
21 1-3   -  U  U  -      
22 1-4   V  -  -         
23 1-6   -  X  -         
24 2-4   -  Y            
25 3-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VLKRA MSPAR QONRN XGPKV CPMHL P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  C  C  C  -  -
04 1-0   -  -  D  -  -  D
05 1-0   E  E  E  E  -  -
06 1-0   F  F  F  F  -  -
07 1-0   G  -  -  G  G  -
08 1-0   H  -  -  H  -  H
09 2-0   -  I  -  -  I  -
10 0-4   -  J  -  -  -  J
11 0-4   -  K  K  K  K  K
12 0-4   L  -  L  -  L  L
13 0-4   M  M  -  -  M  -
14 0-6   -  -  -  -  N  N
15 0-6   -  -  O  -  -  O
16 0-6   -  P  P  P  -  -
17 0-6   Q  -  Q  Q  Q  Q
18 0-6   R  -  R  -  -   
19 0-6   S  S  -  S  S   
20 1-3   T  T  -  T      
21 1-4   -  -  -  -      
22 1-4   V  V  V         
23 1-4   -  -  X         
24 1-4   -  Y            
25 2-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

IWOYA CYRCC ARCTR TAUVL KKMWR J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  A
02 2-0   B  -  -  -  B  B
03 2-0   -  C  -  -  -  -
04 0-3   -  D  D  D  D  D
05 0-4   -  -  -  E  -  E
06 0-4   -  -  F  F  -  -
07 0-4   -  G  -  -  G  -
08 0-4   H  H  H  -  -  -
09 0-5   I  -  I  I  -  -
10 0-5   -  -  J  J  -  J
11 0-5   K  K  -  K  K  -
12 0-5   -  -  -  -  -  L
13 0-5   -  M  -  -  M  M
14 0-5   N  -  -  -  N  N
15 0-6   -  O  O  -  O  O
16 0-6   -  -  P  -  -  -
17 0-6   Q  -  -  Q  -  -
18 1-3   -  R  R  -  -   
19 2-3   S  -  -  S  -   
20 2-4   T  T  T  -      
21 3-5   -  U  U  -      
22 3-5   V  V  -         
23 4-6   W  X  X         
24 5-6   -  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ESSVR ENQUX MUGNV EHNAR PVLEM U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 2-0   B  -  -  -  B  B
03 2-0   C  -  C  C  -  -
04 2-0   D  D  D  D  D  D
05 2-0   -  E  E  -  E  -
06 2-0   -  F  F  F  -  -
07 2-0   -  G  G  G  -  -
08 2-0   H  -  -  H  H  H
09 2-0   I  I  I  -  -  I
10 0-3   -  -  -  J  J  -
11 0-3   -  K  K  K  -  -
12 0-5   L  L  L  -  L  L
13 0-5   M  M  -  -  -  M
14 0-5   N  N  N  -  -  N
15 0-5   -  O  O  -  O  -
16 0-6   P  -  -  -  -  -
17 0-6   -  Q  -  -  -  Q
18 0-6   -  R  -  R  R   
19 0-6   S  S  -  S  -   
20 1-3   -  -  T  -      
21 2-4   U  -  U  -      
22 2-6   V  -  -         
23 2-6   -  -  -         
24 2-6   -  Y            
25 2-6   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WMRYM CSDIU RIKEW ZQSOY MQUWR W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  -  -  -  -
03 2-0   C  -  C  -  C  C
04 2-0   D  D  D  -  D  D
05 2-0   -  E  E  E  E  -
06 2-0   F  F  F  -  F  -
07 2-0   -  G  -  G  -  G
08 2-0   -  -  H  -  H  H
09 2-0   I  -  -  I  -  I
10 2-0   -  J  J  J  -  J
11 2-0   -  -  K  K  -  K
12 0-3   -  L  -  -  -  -
13 0-5   M  -  -  M  M  M
14 0-5   N  N  -  -  -  N
15 0-5   -  -  O  -  O  O
16 0-5   -  P  P  P  P  -
17 0-6   Q  Q  -  -  Q  -
18 0-6   -  -  -  -  -   
19 0-6   -  S  S  S  S   
20 1-3   -  -  T  T      
21 1-5   -  U  -  -      
22 2-6   -  V  V         
23 2-6   W  X  X         
24 2-6   X  -            
25 2-6   Y  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

IXTNV IPWXN YFVRW YUTAN YISIX C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
