EFFECTIVE PERIOD:
10-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  -  A
02 0-3   -  B  -  B  -  B
03 0-3   C  C  C  C  C  C
04 0-3   D  -  D  -  D  -
05 0-4   -  E  -  -  -  -
06 0-5   F  F  -  -  -  -
07 0-5   -  G  -  -  G  G
08 0-5   H  H  -  H  -  -
09 0-5   I  -  I  -  -  -
10 0-5   J  -  -  -  J  J
11 0-6   -  K  K  K  -  -
12 0-6   -  L  L  -  -  L
13 0-6   M  M  -  M  M  M
14 0-6   -  -  -  -  N  -
15 0-6   -  -  -  -  O  O
16 0-6   P  -  P  -  P  -
17 0-6   Q  Q  Q  Q  -  -
18 1-3   -  -  R  R  -   
19 1-4   -  -  -  S  -   
20 2-3   T  T  T  T      
21 3-4   -  U  U  U      
22 3-5   -  -  V         
23 3-5   -  X  X         
24 3-5   X  -            
25 3-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ETYQA ENVVU OOKUL ANKUM RAKMI V
-------------------------------
