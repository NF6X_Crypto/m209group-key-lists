EFFECTIVE PERIOD:
18-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  -
02 2-0   B  B  -  B  -  B
03 2-0   C  C  -  C  -  C
04 2-0   -  -  D  -  D  -
05 2-0   -  -  -  -  E  E
06 0-5   -  -  F  -  -  F
07 0-5   G  G  -  -  -  G
08 0-5   H  H  H  H  -  -
09 0-5   -  I  -  I  I  -
10 0-5   -  J  J  J  -  -
11 0-5   -  -  -  K  K  -
12 0-6   -  -  -  -  -  L
13 0-6   M  -  -  -  -  -
14 0-6   N  -  N  -  N  -
15 0-6   O  -  -  -  -  O
16 1-3   -  P  P  P  -  P
17 1-5   Q  Q  Q  Q  Q  -
18 1-6   R  R  R  R  R   
19 1-6   S  -  -  -  -   
20 2-5   T  -  -  -      
21 2-6   -  U  U  U      
22 2-6   -  V  V         
23 2-6   -  -  -         
24 2-6   -  -            
25 3-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

BUVPO URLVO FAAUZ BJPAM JUMEM V
-------------------------------
