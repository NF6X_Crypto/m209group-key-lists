EFFECTIVE PERIOD:
19-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   B  -  -  -  B  B
03 0-3   -  C  -  -  -  C
04 0-3   -  D  -  D  -  D
05 0-3   -  -  -  -  E  -
06 0-4   F  F  F  -  -  -
07 0-4   G  -  -  -  -  -
08 0-4   H  -  H  H  H  H
09 0-4   -  I  I  -  -  -
10 0-4   -  -  -  -  J  J
11 0-4   K  K  -  K  K  -
12 0-4   L  -  L  -  L  -
13 0-5   M  M  -  -  M  M
14 0-6   N  N  N  N  N  N
15 0-6   O  -  O  O  -  O
16 0-6   -  -  P  -  P  P
17 0-6   Q  -  -  Q  -  -
18 0-6   R  -  -  -  -   
19 0-6   S  S  -  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  -  U      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   Y  -            
26 1-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

FNZNW NGIWT PAKAM FVGGW OTUOG Y
-------------------------------
