EFFECTIVE PERIOD:
03-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 2-0   B  B  -  -  B  -
03 2-0   -  -  -  -  C  C
04 0-3   D  D  D  -  D  D
05 0-3   E  E  E  E  -  E
06 0-3   F  F  F  F  -  F
07 0-3   -  G  -  G  -  G
08 0-3   -  -  H  H  -  -
09 0-4   -  I  I  I  I  I
10 0-4   J  -  J  -  J  -
11 0-4   -  K  -  K  K  -
12 0-4   L  L  -  L  -  -
13 0-5   -  M  M  -  M  M
14 0-5   N  N  N  -  N  -
15 0-5   -  O  -  O  -  -
16 0-5   P  -  -  -  -  -
17 0-5   Q  -  Q  -  Q  Q
18 0-5   R  -  -  -  -   
19 0-5   -  S  S  -  -   
20 0-5   T  -  -  -      
21 0-6   -  U  -  -      
22 1-2   V  -  V         
23 2-3   -  -  X         
24 3-4   -  Y            
25 4-5   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KMSGE RFZRZ UPFOR SUYRR ROKRP D
-------------------------------
