EFFECTIVE PERIOD:
13-OCT-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 2-0   B  -  -  B  -  -
03 2-0   -  C  -  -  C  -
04 2-0   D  -  D  D  D  -
05 2-0   E  E  E  -  -  E
06 2-0   -  F  -  F  -  F
07 2-0   G  -  -  G  G  -
08 0-3   H  -  -  -  -  -
09 0-3   -  I  -  I  I  I
10 0-3   -  -  J  -  -  -
11 0-3   K  -  -  -  -  K
12 0-3   -  -  L  -  -  L
13 0-4   -  M  M  -  M  M
14 0-4   N  N  -  -  -  -
15 0-4   -  O  -  O  -  -
16 0-4   P  -  -  -  P  P
17 0-4   -  -  Q  -  Q  -
18 0-4   -  -  R  R  -   
19 0-4   S  -  S  S  S   
20 1-6   T  T  T  -      
21 2-3   -  -  -  U      
22 2-3   V  V  V         
23 2-3   W  X  -         
24 2-3   X  Y            
25 2-5   -  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NCJKL ASNAL VNJRT VLJBT AKLUN L
-------------------------------
