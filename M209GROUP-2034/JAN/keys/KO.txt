EFFECTIVE PERIOD:
31-JAN-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 2-0   -  B  -  B  -  B
03 2-0   C  -  -  -  C  C
04 2-0   D  D  -  -  D  -
05 2-0   E  E  E  E  -  -
06 2-0   -  -  -  F  -  F
07 0-3   G  G  -  -  G  -
08 0-3   H  H  H  H  H  H
09 0-3   -  I  I  -  -  -
10 0-3   J  J  -  -  J  J
11 0-4   K  K  -  K  K  K
12 0-4   L  -  -  -  -  -
13 0-4   -  -  M  -  M  M
14 0-4   -  -  N  -  -  -
15 0-4   O  O  O  -  O  -
16 0-4   P  P  P  P  -  -
17 0-5   -  -  -  -  Q  Q
18 1-2   R  -  R  R  R   
19 1-5   S  -  S  S  S   
20 1-6   T  T  -  T      
21 2-4   -  -  U  -      
22 2-4   -  V  V         
23 2-4   -  X  -         
24 2-4   -  -            
25 2-5   -  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

TTIQI ZPASL OFMNA PLUUT PNKGT U
-------------------------------
