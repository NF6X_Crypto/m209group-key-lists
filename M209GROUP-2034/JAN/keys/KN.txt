EFFECTIVE PERIOD:
30-JAN-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  A
02 2-0   -  B  B  -  B  B
03 2-0   C  -  C  C  -  C
04 2-0   D  D  D  -  -  -
05 2-0   -  -  E  -  -  -
06 2-0   -  F  -  F  -  -
07 0-3   G  G  G  -  G  G
08 0-3   -  -  H  -  H  -
09 0-3   -  I  -  I  -  I
10 0-3   J  -  J  J  -  -
11 0-3   -  -  -  -  -  -
12 0-3   -  L  -  L  -  L
13 0-3   M  -  M  -  -  -
14 0-3   -  -  -  N  N  N
15 0-3   -  O  O  -  O  -
16 0-3   -  -  P  P  P  -
17 0-3   -  Q  Q  Q  -  Q
18 0-3   R  R  -  R  R   
19 0-4   S  -  S  S  S   
20 0-5   T  T  -  T      
21 0-5   -  -  -  -      
22 0-5   -  V  -         
23 0-5   -  X  -         
24 0-6   X  Y            
25 0-6   -  Z            
26 1-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FQXAG SIOJO SNRPY HRTER EEXHQ A
-------------------------------
