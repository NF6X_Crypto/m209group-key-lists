EFFECTIVE PERIOD:
09-AUG-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  B  B  -  B  -
03 1-0   -  -  C  C  -  C
04 1-0   -  -  -  -  -  -
05 1-0   -  -  E  E  -  -
06 2-0   -  -  F  -  -  F
07 2-0   -  -  -  G  -  -
08 2-0   H  H  H  H  H  H
09 0-4   -  -  -  I  -  I
10 0-4   J  -  -  -  J  J
11 0-4   -  K  -  -  K  -
12 0-4   L  L  L  -  L  L
13 0-6   M  M  -  M  M  -
14 0-6   -  N  N  N  -  -
15 0-6   -  -  O  O  O  -
16 0-6   -  -  -  -  P  P
17 0-6   -  Q  Q  Q  Q  -
18 1-2   R  -  R  -  -   
19 1-2   S  S  S  -  S   
20 1-2   -  -  -  -      
21 1-2   -  -  -  -      
22 1-5   V  -  -         
23 1-5   W  X  X         
24 2-6   X  Y            
25 3-5   Y  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

IOUMQ KKTQW OOVFH OLIWD MOAVR F
-------------------------------
