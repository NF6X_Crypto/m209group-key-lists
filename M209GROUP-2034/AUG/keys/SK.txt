EFFECTIVE PERIOD:
23-AUG-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  -  B  B  -  -
03 1-0   -  -  C  -  -  -
04 1-0   D  -  -  D  -  -
05 1-0   -  -  -  E  E  -
06 1-0   -  -  -  -  F  F
07 1-0   G  -  -  -  G  G
08 1-0   -  H  H  H  H  H
09 1-0   I  I  I  I  -  -
10 2-0   J  -  -  J  -  J
11 2-0   K  K  -  K  K  K
12 2-0   -  L  -  -  -  L
13 2-0   M  -  -  M  M  M
14 2-0   N  N  N  -  -  -
15 0-3   -  O  -  -  O  O
16 0-4   P  P  P  P  P  P
17 0-4   Q  Q  -  -  -  Q
18 0-4   R  R  R  -  R   
19 0-4   -  -  S  -  -   
20 0-4   -  -  T  -      
21 0-6   U  -  -  U      
22 1-4   V  -  -         
23 1-4   -  -  -         
24 1-4   X  Y            
25 2-4   Y  -            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AUVPD OPZTX JHMLO BKURZ NZIPX I
-------------------------------
