SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JUL 2034

    01 JUL 2034  00:00-23:59 GMT:  USE KEY QJ
    02 JUL 2034  00:00-23:59 GMT:  USE KEY QK
    03 JUL 2034  00:00-23:59 GMT:  USE KEY QL
    04 JUL 2034  00:00-23:59 GMT:  USE KEY QM
    05 JUL 2034  00:00-23:59 GMT:  USE KEY QN
    06 JUL 2034  00:00-23:59 GMT:  USE KEY QO
    07 JUL 2034  00:00-23:59 GMT:  USE KEY QP
    08 JUL 2034  00:00-23:59 GMT:  USE KEY QQ
    09 JUL 2034  00:00-23:59 GMT:  USE KEY QR
    10 JUL 2034  00:00-23:59 GMT:  USE KEY QS
    11 JUL 2034  00:00-23:59 GMT:  USE KEY QT
    12 JUL 2034  00:00-23:59 GMT:  USE KEY QU
    13 JUL 2034  00:00-23:59 GMT:  USE KEY QV
    14 JUL 2034  00:00-23:59 GMT:  USE KEY QW
    15 JUL 2034  00:00-23:59 GMT:  USE KEY QX
    16 JUL 2034  00:00-23:59 GMT:  USE KEY QY
    17 JUL 2034  00:00-23:59 GMT:  USE KEY QZ
    18 JUL 2034  00:00-23:59 GMT:  USE KEY RA
    19 JUL 2034  00:00-23:59 GMT:  USE KEY RB
    20 JUL 2034  00:00-23:59 GMT:  USE KEY RC
    21 JUL 2034  00:00-23:59 GMT:  USE KEY RD
    22 JUL 2034  00:00-23:59 GMT:  USE KEY RE
    23 JUL 2034  00:00-23:59 GMT:  USE KEY RF
    24 JUL 2034  00:00-23:59 GMT:  USE KEY RG
    25 JUL 2034  00:00-23:59 GMT:  USE KEY RH
    26 JUL 2034  00:00-23:59 GMT:  USE KEY RI
    27 JUL 2034  00:00-23:59 GMT:  USE KEY RJ
    28 JUL 2034  00:00-23:59 GMT:  USE KEY RK
    29 JUL 2034  00:00-23:59 GMT:  USE KEY RL
    30 JUL 2034  00:00-23:59 GMT:  USE KEY RM
    31 JUL 2034  00:00-23:59 GMT:  USE KEY RN

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  B  -  B  B  B
03 1-0   C  C  C  -  C  -
04 1-0   -  D  D  -  D  -
05 1-0   E  E  -  E  E  E
06 1-0   -  -  F  -  -  -
07 1-0   -  -  -  -  G  -
08 2-0   H  -  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-3   -  -  -  J  -  -
11 0-3   K  -  -  -  K  K
12 0-3   -  L  -  L  L  L
13 0-3   M  -  -  -  -  -
14 0-3   -  N  N  N  -  -
15 0-3   -  -  O  O  -  O
16 0-3   -  -  -  -  -  P
17 0-3   Q  -  -  Q  -  Q
18 0-3   -  -  R  -  R   
19 0-3   S  S  S  S  -   
20 0-4   T  -  T  -      
21 0-4   U  -  -  U      
22 0-4   V  -  -         
23 0-5   -  X  X         
24 0-5   X  -            
25 0-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PXDGY TZBNR HFPWF SDQZE DIGXN H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   -  -  B  B  -  -
03 1-0   C  -  C  -  -  -
04 1-0   -  -  -  -  D  D
05 1-0   E  E  -  E  E  -
06 2-0   F  -  F  -  F  -
07 0-3   G  G  -  G  G  G
08 0-3   H  -  H  -  H  -
09 0-3   -  -  -  -  -  I
10 0-3   -  J  J  J  J  -
11 0-3   K  -  K  K  -  K
12 0-3   L  L  -  L  -  -
13 0-3   -  M  M  M  M  M
14 0-3   -  N  -  -  N  N
15 0-3   -  -  -  -  -  O
16 0-4   P  -  -  -  P  -
17 0-4   Q  Q  Q  -  -  Q
18 0-5   -  R  -  -  R   
19 0-5   -  -  S  S  S   
20 0-5   T  -  -  -      
21 0-5   -  -  U  U      
22 1-3   V  V  -         
23 1-3   -  X  X         
24 1-5   X  Y            
25 2-4   -  Z            
26 2-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MMINN KQTNN WTQQF AHYLP VQTIC F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  -  -  B  B  -
03 1-0   -  -  C  C  C  C
04 1-0   -  -  D  -  D  D
05 1-0   E  E  -  -  E  E
06 0-4   F  -  -  -  -  F
07 0-4   -  G  G  G  -  -
08 0-4   -  H  H  -  H  -
09 0-4   -  I  I  I  I  -
10 0-4   -  J  J  J  J  -
11 0-6   K  -  K  K  K  K
12 0-6   L  L  -  L  -  L
13 0-6   M  M  -  -  M  -
14 0-6   N  N  -  -  -  -
15 0-6   -  -  -  -  -  -
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  Q  -  -  Q
18 1-4   R  R  -  R  -   
19 1-4   S  S  S  -  -   
20 1-4   -  T  -  -      
21 1-4   -  -  -  U      
22 1-6   -  -  -         
23 2-4   -  -  X         
24 2-5   X  Y            
25 2-6   -  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

IUNTD NIIRM AAUAZ TMVVN NJMUA A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  -  -  B  -  B
03 1-0   -  -  C  C  C  C
04 2-0   D  D  D  -  D  -
05 2-0   -  -  E  -  -  E
06 2-0   F  F  -  -  F  -
07 2-0   -  G  -  -  -  -
08 2-0   -  H  -  -  H  -
09 2-0   I  I  I  -  I  I
10 2-0   J  -  J  J  -  J
11 2-0   K  K  K  K  -  -
12 0-3   L  -  -  -  L  -
13 0-5   M  M  M  -  -  M
14 0-6   -  -  -  N  -  N
15 0-6   -  O  -  O  -  O
16 0-6   -  P  P  P  -  P
17 0-6   -  -  -  Q  -  -
18 0-6   R  -  -  -  R   
19 0-6   S  S  S  S  S   
20 0-6   T  -  T  T      
21 0-6   -  U  U  U      
22 0-6   -  V  V         
23 0-6   W  -  -         
24 1-2   -  Y            
25 1-4   -  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

KLRQY MNAOV DWGCP LYJMA RXROM X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  -  -  C  C
04 1-0   D  -  -  D  D  -
05 1-0   -  -  -  -  -  -
06 2-0   F  -  -  F  -  -
07 2-0   -  G  G  G  G  -
08 0-3   H  H  -  -  H  -
09 0-4   I  -  I  -  I  -
10 0-5   J  J  J  J  -  J
11 0-5   -  K  K  K  -  K
12 0-5   -  -  -  -  L  L
13 0-6   -  M  M  M  -  -
14 0-6   N  N  N  N  -  N
15 0-6   O  -  -  -  O  -
16 0-6   P  -  -  -  -  -
17 0-6   -  Q  -  -  -  -
18 0-6   R  R  R  R  R   
19 0-6   -  S  S  -  -   
20 0-6   T  -  T  T      
21 0-6   -  U  U  U      
22 1-5   -  -  V         
23 1-6   W  X  X         
24 1-6   X  Y            
25 1-6   -  Z            
26 2-4   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

YNMTN CIUAV QHEIO GWPXP WEHEV V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  -  B  B
03 1-0   -  -  C  C  -  -
04 1-0   -  D  D  D  -  D
05 1-0   E  -  -  -  -  E
06 1-0   F  F  -  F  -  -
07 1-0   G  -  G  G  -  G
08 1-0   -  H  H  -  H  H
09 1-0   I  -  I  -  I  I
10 1-0   J  -  -  -  J  -
11 1-0   K  -  -  -  -  -
12 2-0   -  L  -  L  -  -
13 0-3   -  M  M  -  -  -
14 0-5   -  N  N  N  N  -
15 0-5   -  O  O  -  -  -
16 0-6   P  P  P  P  -  P
17 0-6   Q  -  Q  Q  -  -
18 0-6   -  -  -  R  R   
19 0-6   S  -  S  -  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 0-6   -  -  -         
23 0-6   -  X  -         
24 1-6   -  Y            
25 3-4   Y  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPFOA XEDOR XNDML YSRDL MPNDD P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  B  B  B  B
03 2-0   C  C  C  -  C  C
04 2-0   -  D  D  D  D  -
05 2-0   -  -  E  E  -  -
06 2-0   -  F  -  -  -  F
07 2-0   -  G  -  -  -  G
08 0-3   H  -  H  -  H  H
09 0-3   I  I  -  I  I  I
10 0-3   J  -  J  -  J  -
11 0-3   K  K  -  -  K  -
12 0-3   -  -  L  -  -  L
13 0-3   M  M  -  M  -  M
14 0-3   N  N  -  N  N  -
15 0-4   O  O  -  O  -  O
16 0-4   -  -  -  -  P  -
17 0-4   -  -  -  Q  Q  -
18 0-4   R  R  R  R  R   
19 0-4   -  S  -  S  -   
20 1-2   -  -  T  T      
21 1-5   U  -  -  -      
22 2-4   -  -  -         
23 3-4   W  -  -         
24 3-4   X  Y            
25 3-4   Y  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

PKOGY NSOEK APXAS BHYEV DAJSK T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  B  -  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  D  D  D  -
05 1-0   -  E  E  E  E  -
06 1-0   F  F  F  -  -  F
07 1-0   G  -  G  G  -  G
08 1-0   H  -  H  H  -  -
09 2-0   -  I  I  I  I  I
10 2-0   -  -  -  -  -  -
11 2-0   -  -  K  K  K  -
12 2-0   L  -  -  L  -  -
13 0-3   M  -  -  M  M  -
14 0-5   -  -  -  N  N  N
15 0-5   O  O  O  -  -  -
16 0-5   P  P  P  -  -  P
17 0-5   -  -  -  Q  Q  -
18 0-6   -  R  -  R  R   
19 0-6   S  S  S  -  S   
20 1-2   -  -  -  -      
21 1-2   -  -  U  -      
22 1-2   V  -  V         
23 1-2   W  X  X         
24 1-4   -  -            
25 2-5   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RQHVJ KSWKE SSZRK OYOUJ KGQMJ Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 2-0   B  -  B  B  -  -
03 2-0   -  C  -  C  C  -
04 2-0   D  -  D  D  D  D
05 2-0   E  -  E  -  E  E
06 2-0   -  F  F  -  -  F
07 2-0   -  G  G  G  G  G
08 2-0   -  H  -  H  H  -
09 2-0   -  -  -  -  -  -
10 2-0   J  J  J  J  -  -
11 0-3   K  -  -  -  -  -
12 0-3   L  L  -  L  -  -
13 0-3   M  M  M  M  -  M
14 0-3   N  -  N  N  N  N
15 0-4   -  -  -  O  -  -
16 0-4   P  -  -  -  P  -
17 0-4   Q  -  Q  Q  Q  Q
18 0-6   R  -  R  -  R   
19 0-6   -  S  -  S  S   
20 0-6   T  -  T  -      
21 0-6   U  -  -  -      
22 0-6   -  -  -         
23 0-6   W  -  -         
24 0-6   -  Y            
25 0-6   Y  -            
26 1-4   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

JFNLS CQKZL QZNUG JQIML WFSAK E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  A  -  -
02 0-4   B  -  -  B  B  B
03 0-4   -  C  C  -  C  -
04 0-4   D  -  D  -  -  -
05 0-4   -  -  -  -  -  E
06 0-4   F  -  -  -  -  F
07 0-5   -  G  G  -  -  G
08 0-5   H  -  -  -  -  -
09 0-5   I  -  I  I  -  -
10 0-5   -  J  J  J  J  J
11 0-5   K  K  K  K  -  K
12 0-6   L  -  -  -  L  -
13 0-6   -  M  -  -  -  -
14 0-6   N  N  N  N  N  N
15 0-6   -  O  -  -  O  O
16 1-3   P  -  -  P  -  -
17 1-6   -  Q  -  Q  Q  -
18 2-6   -  R  R  -  R   
19 3-5   -  S  -  -  S   
20 3-6   T  -  -  -      
21 3-6   -  U  -  U      
22 3-6   V  V  V         
23 4-5   W  -  -         
24 4-6   -  Y            
25 4-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VJHYV GJJVQ VNPPB ZPYQV PURGS Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  B  -  -  -
03 1-0   C  -  C  C  -  -
04 1-0   -  -  D  D  -  D
05 0-3   E  E  -  -  E  E
06 0-3   -  -  F  F  F  -
07 0-3   -  G  -  -  -  -
08 0-3   H  -  H  -  -  H
09 0-3   -  I  I  I  -  -
10 0-3   J  J  J  J  -  -
11 0-5   -  -  K  K  K  K
12 0-5   L  -  L  -  L  L
13 0-5   M  M  M  M  M  -
14 0-5   N  N  -  -  N  N
15 0-5   -  -  -  -  -  -
16 0-5   P  P  P  -  P  -
17 0-5   -  -  -  -  -  -
18 0-5   R  R  -  -  R   
19 0-6   -  -  -  S  S   
20 1-2   -  T  T  -      
21 1-3   -  U  -  U      
22 2-6   -  V  -         
23 3-5   W  -  -         
24 3-5   X  Y            
25 3-5   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UPYCM BVUGB ROVYC QZOAV ATPTH T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  -  -  B  -  B
03 1-0   -  C  -  -  C  C
04 1-0   D  -  D  -  D  -
05 1-0   E  -  -  E  E  -
06 0-3   F  -  F  F  F  -
07 0-5   G  G  -  G  -  -
08 0-5   -  H  -  -  H  H
09 0-5   -  I  I  I  -  -
10 0-5   J  J  J  J  -  -
11 0-5   -  -  K  -  -  -
12 0-5   L  L  -  -  -  -
13 0-6   M  -  M  -  -  -
14 0-6   -  -  N  N  N  -
15 0-6   -  O  -  -  O  O
16 1-5   -  P  P  P  -  P
17 1-6   Q  Q  -  -  -  Q
18 1-6   -  R  R  -  R   
19 1-6   S  S  -  S  -   
20 1-6   -  -  T  -      
21 2-3   -  -  U  -      
22 2-4   -  V  -         
23 2-6   -  X  X         
24 2-6   -  -            
25 3-5   -  Z            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

HNOUO KVOUN UQQOR VJNHH RSATL V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  B  B  -  -
03 1-0   -  -  -  C  C  -
04 2-0   D  -  -  -  -  -
05 2-0   E  -  E  -  -  -
06 2-0   -  F  -  -  F  F
07 2-0   -  -  G  G  -  -
08 0-4   H  -  H  H  H  H
09 0-4   -  -  -  -  -  I
10 0-4   -  J  J  J  -  -
11 0-4   K  -  -  -  -  K
12 0-4   L  L  -  -  L  L
13 0-4   M  M  M  M  -  -
14 0-6   N  -  N  -  -  -
15 0-6   -  -  -  -  -  -
16 0-6   P  P  P  P  P  -
17 0-6   -  -  Q  Q  -  Q
18 1-3   R  -  -  -  R   
19 1-4   -  S  -  S  S   
20 1-6   T  T  T  T      
21 2-4   U  -  -  U      
22 2-4   -  V  -         
23 2-4   W  -  -         
24 2-4   X  Y            
25 2-6   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

WNGNW OTARO WNNAL WSHWC IRPUW N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   B  -  B  -  B  -
03 0-3   -  C  C  -  -  C
04 0-4   -  -  D  D  D  -
05 0-4   E  E  -  E  -  E
06 0-4   F  F  F  F  -  F
07 0-4   G  G  -  G  G  -
08 0-4   H  -  -  -  H  -
09 0-4   -  I  -  I  I  -
10 0-5   J  J  J  -  J  J
11 0-5   -  K  -  -  -  K
12 0-6   -  -  -  -  -  L
13 0-6   -  M  M  -  M  -
14 0-6   N  -  N  -  N  -
15 0-6   -  O  -  -  -  O
16 0-6   -  P  -  P  -  -
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  R  -   
19 0-6   -  -  S  S  -   
20 0-6   -  T  -  -      
21 0-6   U  -  U  -      
22 0-6   V  -  -         
23 0-6   W  -  -         
24 1-4   X  Y            
25 1-5   -  -            
26 2-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

OQHJV MJGDX CUYJK OTZPC HQCCJ H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   -  -  B  B  B  -
03 2-0   C  -  C  -  C  C
04 2-0   D  D  -  -  -  -
05 2-0   E  E  E  -  -  -
06 2-0   F  F  -  -  -  F
07 2-0   -  -  -  -  -  -
08 0-3   H  -  -  -  -  H
09 0-3   I  -  I  I  -  -
10 0-3   J  -  J  J  J  J
11 0-3   -  K  K  -  -  -
12 0-5   L  -  L  -  L  L
13 0-5   M  M  -  -  -  -
14 0-5   -  -  N  -  N  N
15 0-5   O  -  O  O  O  O
16 1-4   -  -  P  P  -  P
17 1-5   -  Q  -  Q  -  -
18 1-6   -  -  -  R  -   
19 2-3   -  -  S  -  S   
20 2-5   T  T  T  -      
21 2-5   -  -  -  U      
22 2-5   -  -  -         
23 2-5   W  X  X         
24 3-6   -  Y            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RJZOG VFNUA UTJCW OSMLT LZOTG I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   -  -  -  -  -  B
03 2-0   -  -  C  -  C  -
04 2-0   -  D  D  D  D  D
05 2-0   E  E  E  E  E  -
06 2-0   -  -  F  F  F  -
07 0-3   G  -  -  G  -  -
08 0-3   H  H  -  -  H  H
09 0-3   -  -  -  -  -  -
10 0-3   J  -  -  -  J  J
11 0-3   -  K  -  -  -  K
12 0-3   L  -  L  L  -  L
13 0-5   -  M  M  M  -  -
14 0-5   N  -  N  -  N  N
15 0-5   -  -  -  O  O  -
16 0-5   -  P  P  P  -  -
17 0-5   Q  -  Q  Q  -  Q
18 1-2   R  -  R  -  -   
19 1-3   -  -  -  S  -   
20 1-4   -  -  -  -      
21 2-5   U  -  -  -      
22 3-4   V  V  -         
23 3-5   -  X  -         
24 3-5   X  Y            
25 3-5   Y  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

NKWTN EGGTK WOTTT AAPKZ GYWTG N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  B  B  B  B  -
03 1-0   -  -  -  -  C  C
04 1-0   D  -  D  -  D  D
05 1-0   E  -  -  -  -  -
06 1-0   -  F  F  -  -  -
07 1-0   -  -  G  -  -  -
08 2-0   H  -  H  H  -  H
09 2-0   I  -  I  I  I  I
10 2-0   J  -  -  J  J  J
11 2-0   K  -  -  -  K  -
12 2-0   -  L  -  -  L  -
13 0-3   M  M  M  M  M  M
14 0-3   N  N  -  -  N  -
15 0-3   -  -  -  O  -  O
16 0-3   -  -  P  P  -  -
17 0-3   Q  -  Q  -  Q  -
18 0-3   R  -  R  R  -   
19 0-4   -  S  -  S  -   
20 1-3   -  T  T  -      
21 1-3   U  U  -  -      
22 1-3   -  -  -         
23 1-3   W  -  X         
24 1-6   X  -            
25 2-3   -  Z            
26 2-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

CNVMR USLNO URSKL STNVN NOQUU K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 2-0   -  -  -  -  -  -
03 2-0   -  C  C  C  C  -
04 2-0   -  -  -  D  D  -
05 2-0   -  -  -  E  E  -
06 0-3   -  F  -  -  -  F
07 0-3   G  -  -  G  -  G
08 0-3   -  H  H  -  H  H
09 0-3   I  -  -  -  -  -
10 0-3   -  -  J  -  J  -
11 0-3   K  K  K  -  K  K
12 0-3   L  -  -  L  -  -
13 0-4   -  -  -  M  -  M
14 0-4   -  -  -  -  -  N
15 0-4   O  O  O  O  -  O
16 0-4   P  P  -  -  -  -
17 0-4   -  -  -  -  Q  Q
18 0-4   -  R  R  -  R   
19 0-4   S  S  S  -  -   
20 0-4   -  T  T  T      
21 0-4   U  -  -  U      
22 0-5   V  -  -         
23 0-6   -  X  X         
24 2-3   X  -            
25 2-6   -  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SNMIP QNJSI PMVRC LFNHM USAMK I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  B  B  -  B  B
03 1-0   C  C  C  -  -  -
04 1-0   D  D  D  D  -  D
05 1-0   -  -  -  -  E  -
06 1-0   F  F  -  -  F  F
07 2-0   -  -  -  G  G  G
08 0-4   -  H  H  H  H  -
09 0-4   -  -  -  I  -  -
10 0-4   -  -  J  J  -  -
11 0-4   -  K  K  -  -  K
12 0-4   L  L  L  L  L  L
13 0-4   M  M  -  -  M  -
14 0-4   -  -  -  -  -  -
15 0-4   O  -  O  O  -  -
16 0-5   P  P  -  P  P  -
17 0-5   Q  -  Q  Q  Q  Q
18 0-5   R  -  -  -  R   
19 0-5   -  S  -  -  -   
20 0-6   -  T  -  T      
21 0-6   -  -  U  -      
22 1-4   V  -  -         
23 1-4   -  -  -         
24 1-5   X  -            
25 2-3   -  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QSQKS IQKQP NJUSQ PTOPG QCLAQ Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  -
02 2-0   B  B  -  -  B  -
03 2-0   C  C  -  C  -  C
04 2-0   -  -  D  D  -  -
05 0-3   -  -  E  E  -  E
06 0-3   -  -  F  -  -  F
07 0-3   -  -  -  -  G  G
08 0-3   H  H  H  H  H  H
09 0-4   -  I  I  -  -  -
10 0-4   -  J  J  -  J  -
11 0-4   K  -  -  -  -  -
12 0-5   L  L  -  L  L  -
13 0-5   -  M  M  -  -  -
14 0-5   N  N  N  N  N  -
15 0-5   O  O  O  O  O  O
16 1-4   P  P  P  P  -  P
17 1-5   Q  -  -  Q  -  Q
18 1-5   R  -  -  R  R   
19 1-5   -  S  -  -  -   
20 1-6   -  -  -  -      
21 2-3   -  -  -  -      
22 2-5   -  -  V         
23 2-5   W  X  X         
24 2-5   X  -            
25 2-5   -  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

XWWQP GMEXW KGSWS RFRSV ONATW A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  -
02 2-0   B  -  B  B  B  -
03 2-0   C  C  -  -  -  C
04 2-0   D  -  D  -  -  -
05 2-0   E  -  -  E  -  -
06 0-3   -  F  F  -  F  F
07 0-3   G  G  G  -  -  G
08 0-3   H  -  -  H  -  -
09 0-3   -  I  -  -  I  -
10 0-3   J  J  -  J  J  J
11 0-3   K  K  -  -  K  K
12 0-4   L  L  L  L  -  -
13 0-4   M  -  -  M  M  -
14 0-4   -  -  -  N  N  N
15 0-4   -  -  O  -  -  O
16 0-4   P  -  -  -  -  -
17 0-6   -  -  Q  Q  -  -
18 1-3   R  R  R  -  -   
19 1-5   S  S  S  -  -   
20 1-6   -  T  T  -      
21 2-4   -  -  -  U      
22 2-6   -  V  V         
23 3-4   -  X  -         
24 3-4   X  -            
25 3-4   -  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PSCVZ SLGZE RSFAC NALMG UASCL J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 2-0   -  -  -  -  B  -
03 2-0   -  C  -  C  -  C
04 2-0   D  D  D  D  D  -
05 2-0   -  -  -  -  -  E
06 0-3   -  -  F  F  -  -
07 0-3   G  -  G  -  G  G
08 0-3   -  H  -  H  H  H
09 0-3   -  -  I  -  I  -
10 0-3   J  -  -  J  -  J
11 0-4   K  -  K  -  -  -
12 0-4   -  L  L  -  -  L
13 0-4   -  -  -  M  -  -
14 0-4   N  N  -  N  N  N
15 0-6   -  -  O  -  -  O
16 0-6   -  -  P  P  -  -
17 0-6   Q  -  -  -  Q  -
18 0-6   -  R  -  R  R   
19 0-6   -  S  S  -  -   
20 1-4   T  T  T  -      
21 2-3   -  U  -  -      
22 2-6   -  -  V         
23 2-6   W  -  X         
24 2-6   -  Y            
25 2-6   Y  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FWHUQ UPBGW BVFUV HVBZZ SWOZV H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  -  -  B  -  B
03 1-0   -  C  C  C  C  C
04 1-0   -  -  -  -  D  -
05 1-0   -  E  -  E  E  E
06 1-0   F  F  F  -  -  -
07 1-0   G  G  G  -  G  -
08 1-0   -  -  H  H  -  H
09 1-0   -  -  -  I  I  I
10 2-0   -  -  -  -  -  J
11 2-0   K  K  K  -  -  K
12 2-0   L  -  -  -  L  L
13 2-0   -  M  -  -  -  -
14 2-0   -  -  N  N  -  N
15 2-0   O  O  O  -  O  O
16 2-0   P  P  -  P  P  -
17 2-0   Q  Q  -  -  -  -
18 0-3   -  R  -  -  -   
19 0-3   S  -  S  -  -   
20 0-3   T  T  -  T      
21 0-4   U  U  -  -      
22 0-6   V  -  V         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZAHXN UQCQA FPNKE PAADA HULMF U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 2-0   -  B  B  B  -  B
03 2-0   -  C  -  -  C  C
04 2-0   -  D  D  D  D  -
05 2-0   -  -  E  -  E  -
06 2-0   F  -  -  F  -  -
07 2-0   -  G  -  G  -  -
08 2-0   H  -  -  -  H  H
09 0-3   -  I  I  I  -  -
10 0-5   J  J  -  -  -  J
11 0-5   -  -  -  -  K  -
12 0-5   L  L  -  L  -  L
13 0-5   -  -  -  M  -  M
14 0-5   -  N  N  -  -  N
15 0-6   O  -  O  -  O  O
16 0-6   P  P  -  P  P  -
17 0-6   -  Q  Q  -  -  Q
18 0-6   -  R  -  -  R   
19 0-6   -  -  S  -  S   
20 1-3   -  T  -  -      
21 1-6   -  U  U  U      
22 2-4   V  -  V         
23 2-5   W  -  X         
24 2-5   -  -            
25 2-5   Y  Z            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

EUSZP NZSSJ UVRNU KILSS UHURU R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  -  B  -  B  B
03 1-0   C  C  C  C  C  -
04 0-3   D  -  D  D  D  D
05 0-3   -  E  -  -  -  -
06 0-3   -  F  F  F  -  F
07 0-3   -  -  G  -  G  -
08 0-3   H  H  -  -  H  -
09 0-3   I  -  I  -  -  I
10 0-3   -  -  -  J  J  J
11 0-3   K  K  -  -  -  -
12 0-3   L  -  -  L  -  -
13 0-3   M  M  -  M  -  M
14 0-3   N  -  N  -  N  -
15 0-3   O  O  O  O  O  O
16 0-4   -  P  P  P  P  -
17 0-4   -  -  Q  -  -  Q
18 0-4   R  -  -  -  -   
19 0-4   S  -  -  -  -   
20 0-4   -  T  -  T      
21 0-5   -  U  U  U      
22 0-6   -  -  -         
23 0-6   -  -  -         
24 1-4   X  -            
25 1-6   -  -            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SMAYF AEVOI VUYRM RQCWH FOHAY U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  A
02 2-0   B  B  -  -  -  B
03 2-0   -  -  C  C  C  -
04 2-0   D  D  -  -  -  D
05 2-0   E  -  -  -  -  -
06 2-0   F  F  F  -  -  -
07 0-3   G  G  -  G  -  -
08 0-3   -  -  H  H  -  -
09 0-3   I  I  -  -  -  -
10 0-4   J  J  J  -  J  -
11 0-4   K  -  K  -  K  K
12 0-4   -  -  L  -  L  -
13 0-4   -  M  -  -  -  -
14 0-6   N  -  N  N  -  N
15 0-6   -  O  O  O  O  -
16 0-6   -  -  P  -  P  P
17 0-6   Q  Q  Q  Q  Q  Q
18 1-3   -  -  -  R  -   
19 1-3   S  -  -  S  -   
20 1-5   -  T  -  T      
21 1-6   U  U  -  -      
22 2-4   V  V  V         
23 2-6   W  X  X         
24 3-4   -  -            
25 3-4   -  -            
26 3-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

WPUPJ XXXWI IPSZL PTWAS WKPAP Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  B  -  B  B
03 1-0   C  C  -  -  -  C
04 1-0   D  D  -  D  D  D
05 1-0   -  E  E  E  E  E
06 0-4   F  -  F  F  F  F
07 0-4   G  -  G  G  G  G
08 0-4   H  H  H  H  -  -
09 0-4   -  -  I  -  I  I
10 0-4   -  -  J  J  -  J
11 0-4   K  K  -  -  -  -
12 0-6   -  -  L  L  -  -
13 0-6   M  -  -  M  M  M
14 0-6   N  N  -  N  N  -
15 0-6   -  -  O  -  -  O
16 0-6   -  P  -  P  P  -
17 0-6   -  -  Q  -  Q  -
18 1-2   -  R  R  -  -   
19 1-4   S  -  -  S  -   
20 1-6   -  -  -  -      
21 2-5   U  -  -  -      
22 3-4   V  V  -         
23 4-5   W  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MOMUN JTIKN LUUAU VAIUL NAMJU K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   -  -  B  -  -  -
03 2-0   C  C  -  -  C  C
04 2-0   -  D  D  D  -  D
05 2-0   E  -  -  -  E  -
06 2-0   -  -  -  -  F  -
07 2-0   G  G  -  G  G  G
08 0-3   H  -  -  H  H  -
09 0-3   -  I  -  I  -  I
10 0-3   J  J  J  -  J  -
11 0-3   K  K  -  K  -  -
12 0-3   -  -  -  -  L  -
13 0-3   -  -  -  -  -  M
14 0-3   -  N  N  N  -  -
15 0-3   -  O  -  -  O  -
16 0-3   -  -  -  P  -  P
17 0-3   Q  Q  Q  Q  Q  -
18 0-3   R  -  R  R  -   
19 0-3   S  S  S  S  -   
20 0-3   -  T  -  T      
21 0-4   -  -  U  -      
22 0-4   V  V  V         
23 0-4   W  -  -         
24 0-5   X  Y            
25 0-6   -  Z            
26 1-4   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

UFJTW ZFZQK VMFXH HDMCZ IIGMM J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   B  -  -  B  -  -
03 2-0   -  C  -  C  C  -
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  E  E  E
06 2-0   F  -  -  -  F  -
07 0-3   G  G  -  G  -  -
08 0-3   -  -  H  H  -  H
09 0-3   I  I  I  I  -  -
10 0-3   -  J  -  -  -  J
11 0-6   K  K  -  K  K  K
12 0-6   L  L  -  -  -  L
13 0-6   -  -  M  M  M  -
14 0-6   N  N  N  -  -  N
15 0-6   O  O  -  -  O  O
16 1-3   -  -  -  -  P  -
17 1-5   Q  -  -  -  -  Q
18 1-6   R  -  R  R  R   
19 1-6   -  -  S  -  -   
20 2-3   T  T  T  -      
21 2-6   U  U  -  -      
22 2-6   V  V  -         
23 2-6   W  -  X         
24 2-6   -  -            
25 4-5   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YQEOV TAVZG UNHSS FQAUP UWVUV R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  -  C  -  -
04 1-0   -  -  -  D  D  -
05 1-0   -  E  -  -  -  E
06 1-0   -  -  F  -  -  F
07 1-0   G  -  G  G  -  -
08 1-0   -  -  H  H  H  H
09 2-0   -  I  -  -  I  I
10 2-0   J  J  -  J  -  J
11 2-0   -  K  K  K  -  -
12 0-3   -  -  -  -  -  -
13 0-4   M  M  M  -  M  -
14 0-4   -  -  -  -  N  -
15 0-4   O  O  O  O  O  -
16 0-4   P  -  P  P  P  -
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  -  -   
20 0-5   T  -  -  T      
21 0-5   -  -  U  U      
22 0-5   -  -  V         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-5   -  -            
26 2-3   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

LLASU LJORM GVIHA SPKDH WMLZP A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  B  -  B  -  -
03 1-0   -  -  -  C  -  C
04 1-0   -  D  D  -  -  D
05 1-0   -  E  E  E  -  E
06 1-0   F  -  F  F  F  -
07 2-0   G  G  -  G  G  G
08 2-0   -  H  -  -  H  -
09 2-0   I  -  -  I  I  I
10 2-0   -  -  -  -  -  J
11 2-0   -  K  -  K  K  K
12 0-4   -  -  -  L  L  -
13 0-4   M  -  M  M  M  M
14 0-4   -  -  N  N  N  -
15 0-4   O  O  -  O  O  O
16 0-6   P  -  P  -  -  P
17 0-6   -  Q  -  -  -  -
18 1-2   -  R  R  R  R   
19 1-2   S  -  -  -  -   
20 1-2   T  T  T  -      
21 1-2   U  U  U  -      
22 1-3   V  V  V         
23 1-5   W  -  X         
24 1-6   X  -            
25 2-4   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AGUSQ ATTNF AVSLR UQAKV GTLTZ T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
