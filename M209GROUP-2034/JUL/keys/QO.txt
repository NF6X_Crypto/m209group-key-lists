EFFECTIVE PERIOD:
06-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  -  B  B
03 1-0   -  -  C  C  -  -
04 1-0   -  D  D  D  -  D
05 1-0   E  -  -  -  -  E
06 1-0   F  F  -  F  -  -
07 1-0   G  -  G  G  -  G
08 1-0   -  H  H  -  H  H
09 1-0   I  -  I  -  I  I
10 1-0   J  -  -  -  J  -
11 1-0   K  -  -  -  -  -
12 2-0   -  L  -  L  -  -
13 0-3   -  M  M  -  -  -
14 0-5   -  N  N  N  N  -
15 0-5   -  O  O  -  -  -
16 0-6   P  P  P  P  -  P
17 0-6   Q  -  Q  Q  -  -
18 0-6   -  -  -  R  R   
19 0-6   S  -  S  -  S   
20 0-6   -  -  -  -      
21 0-6   U  U  -  U      
22 0-6   -  -  -         
23 0-6   -  X  -         
24 1-6   -  Y            
25 3-4   Y  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPFOA XEDOR XNDML YSRDL MPNDD P
-------------------------------
