EFFECTIVE PERIOD:
29-JUL-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   B  -  -  B  -  -
03 2-0   -  C  -  C  C  -
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  E  E  E
06 2-0   F  -  -  -  F  -
07 0-3   G  G  -  G  -  -
08 0-3   -  -  H  H  -  H
09 0-3   I  I  I  I  -  -
10 0-3   -  J  -  -  -  J
11 0-6   K  K  -  K  K  K
12 0-6   L  L  -  -  -  L
13 0-6   -  -  M  M  M  -
14 0-6   N  N  N  -  -  N
15 0-6   O  O  -  -  O  O
16 1-3   -  -  -  -  P  -
17 1-5   Q  -  -  -  -  Q
18 1-6   R  -  R  R  R   
19 1-6   -  -  S  -  -   
20 2-3   T  T  T  -      
21 2-6   U  U  -  -      
22 2-6   V  V  -         
23 2-6   W  -  X         
24 2-6   -  -            
25 4-5   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YQEOV TAVZG UNHSS FQAUP UWVUV R
-------------------------------
