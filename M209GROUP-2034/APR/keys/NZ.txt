EFFECTIVE PERIOD:
30-APR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  -  B  -  -  B
03 1-0   C  C  C  C  C  C
04 1-0   D  D  -  -  -  -
05 2-0   -  E  -  -  -  -
06 2-0   F  -  F  -  -  F
07 2-0   -  -  G  G  G  -
08 2-0   H  H  -  -  H  H
09 2-0   I  -  -  I  I  -
10 2-0   J  J  J  J  J  J
11 2-0   -  -  K  -  -  -
12 0-4   -  -  L  L  -  -
13 0-4   -  M  -  M  M  M
14 0-4   -  N  -  N  -  -
15 0-4   O  -  O  -  O  -
16 1-2   -  -  -  P  -  P
17 1-3   -  -  Q  -  Q  Q
18 2-4   R  R  -  -  -   
19 2-4   -  -  -  -  S   
20 2-4   T  -  -  T      
21 2-4   -  U  U  U      
22 3-4   V  -  -         
23 3-4   -  X  -         
24 3-4   X  -            
25 3-6   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LOIWF CTOAN FZLQT WLMYL ANNOW I
-------------------------------
