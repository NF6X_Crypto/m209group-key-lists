EFFECTIVE PERIOD:
08-APR-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ND
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   -  -  B  B  B  B
03 1-0   -  -  C  C  -  -
04 1-0   D  -  -  D  D  D
05 0-3   -  E  -  -  E  -
06 0-3   F  -  F  -  F  -
07 0-4   -  -  G  G  -  G
08 0-5   H  H  -  H  H  H
09 0-5   I  I  -  I  I  -
10 0-5   -  J  -  J  J  J
11 0-5   K  K  K  -  -  -
12 0-5   L  -  L  -  -  -
13 0-5   -  M  -  -  M  -
14 0-6   N  N  -  -  -  -
15 0-6   O  O  O  O  -  O
16 0-6   P  P  P  -  -  P
17 0-6   Q  -  Q  Q  -  Q
18 0-6   R  R  -  R  -   
19 0-6   -  -  S  S  -   
20 1-5   -  T  -  -      
21 1-5   -  -  U  -      
22 1-5   -  -  -         
23 1-5   -  -  -         
24 1-6   X  -            
25 2-5   Y  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

JTNEJ MJKWR KPPSP PAMLJ OTPKT U
-------------------------------
