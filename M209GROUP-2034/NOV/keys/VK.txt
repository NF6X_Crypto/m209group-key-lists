EFFECTIVE PERIOD:
09-NOV-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  B  -  -  -  -
03 1-0   C  C  C  -  C  C
04 1-0   -  D  -  -  -  D
05 1-0   -  E  -  E  E  -
06 1-0   F  -  F  F  F  F
07 2-0   G  -  -  -  G  G
08 2-0   H  -  -  H  -  -
09 2-0   -  I  -  I  -  -
10 2-0   -  -  J  -  J  J
11 2-0   K  K  K  K  -  -
12 2-0   L  -  L  -  -  L
13 0-3   -  -  -  M  -  -
14 0-5   N  -  -  N  N  -
15 0-5   O  O  -  -  O  O
16 0-5   -  P  P  -  P  -
17 0-5   Q  -  Q  Q  -  -
18 0-6   R  R  -  R  -   
19 0-6   S  -  -  -  S   
20 1-4   T  -  T  T      
21 1-5   -  -  U  -      
22 1-5   -  V  V         
23 1-5   W  -  X         
24 1-5   -  Y            
25 2-5   -  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

IVRYT ABYKL MZAZQ JOPOK BAPIK U
-------------------------------
