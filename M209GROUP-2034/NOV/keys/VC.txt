EFFECTIVE PERIOD:
01-NOV-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  -  -  -  -  -
03 1-0   C  -  C  -  -  -
04 1-0   -  -  D  D  -  D
05 1-0   E  E  -  E  E  E
06 1-0   -  -  -  -  -  -
07 1-0   G  -  G  G  -  -
08 1-0   H  H  H  -  H  H
09 1-0   I  I  -  -  I  I
10 2-0   -  -  J  J  -  -
11 2-0   K  K  K  K  K  -
12 2-0   L  -  L  L  -  L
13 2-0   -  -  M  M  -  -
14 0-3   N  N  -  -  N  N
15 0-3   O  O  O  O  O  O
16 0-3   -  P  -  P  -  P
17 0-3   -  Q  -  Q  Q  -
18 0-3   R  -  -  R  -   
19 0-3   S  -  -  -  S   
20 0-5   T  T  T  -      
21 0-6   -  U  U  U      
22 1-3   V  -  -         
23 1-3   -  -  -         
24 1-3   X  Y            
25 2-3   -  -            
26 2-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GPTPN IXMOF KMMTA UMSUY LAWAZ S
-------------------------------
