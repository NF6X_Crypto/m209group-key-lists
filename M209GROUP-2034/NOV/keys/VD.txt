EFFECTIVE PERIOD:
02-NOV-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 2-0   B  B  B  -  B  -
03 2-0   C  -  C  -  C  C
04 0-3   -  D  D  -  -  D
05 0-3   -  -  E  -  -  E
06 0-3   -  -  -  -  -  -
07 0-3   G  -  -  G  G  G
08 0-3   -  H  H  -  -  -
09 0-4   -  -  I  I  -  I
10 0-4   J  -  J  -  J  J
11 0-6   -  K  -  -  K  -
12 0-6   -  -  -  L  -  L
13 0-6   M  -  M  -  M  -
14 0-6   -  -  -  N  -  N
15 0-6   O  O  O  -  -  -
16 1-2   -  P  P  P  P  -
17 1-2   Q  Q  -  Q  Q  Q
18 1-4   R  R  -  -  R   
19 1-5   -  -  -  S  S   
20 2-4   T  -  -  -      
21 2-4   U  -  -  U      
22 2-6   -  V  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 2-6   Y  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KATUI DQLPA NWTTS PJOXG SKLNM V
-------------------------------
