EFFECTIVE PERIOD:
28-MAY-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  -  -  B  -  -
03 1-0   C  C  C  C  C  -
04 1-0   D  -  D  -  D  -
05 2-0   -  E  -  -  -  E
06 2-0   F  -  F  -  -  F
07 2-0   -  -  -  G  G  G
08 2-0   -  -  -  -  H  H
09 2-0   -  I  I  I  -  I
10 2-0   -  J  -  -  J  -
11 2-0   K  -  K  -  -  K
12 2-0   L  -  -  L  L  -
13 2-0   M  M  M  M  -  -
14 2-0   -  N  -  -  -  -
15 2-0   -  -  O  O  O  O
16 0-3   P  P  P  P  P  -
17 0-3   Q  -  Q  -  Q  Q
18 0-3   -  -  -  -  R   
19 0-3   -  S  -  S  -   
20 0-3   T  T  -  -      
21 0-3   U  -  U  -      
22 0-4   V  V  V         
23 0-4   W  -  X         
24 0-4   -  -            
25 0-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JUJKR PJUGN VQIXG WDFFL UJKUV G
-------------------------------
