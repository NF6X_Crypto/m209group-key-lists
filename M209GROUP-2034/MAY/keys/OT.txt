EFFECTIVE PERIOD:
20-MAY-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  -  B  -  B  -
03 2-0   C  C  -  -  -  C
04 2-0   -  D  D  D  D  -
05 2-0   -  -  E  E  E  E
06 0-3   -  F  -  -  -  -
07 0-3   -  -  -  G  -  -
08 0-3   -  -  -  -  -  -
09 0-3   -  I  I  I  -  I
10 0-5   J  -  -  J  -  J
11 0-5   K  -  K  -  -  K
12 0-5   -  L  -  -  L  -
13 0-5   -  -  M  M  M  M
14 0-5   -  -  N  -  N  N
15 0-5   O  -  O  O  -  -
16 1-2   -  P  P  -  P  -
17 1-3   Q  -  -  Q  -  -
18 1-3   -  R  R  R  -   
19 1-3   -  S  S  -  S   
20 1-3   T  -  T  -      
21 1-6   U  U  -  U      
22 1-6   V  V  -         
23 1-6   -  X  X         
24 2-5   -  -            
25 2-6   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UZUJA PTQGU JASUO EJEJQ XSOQN P
-------------------------------
