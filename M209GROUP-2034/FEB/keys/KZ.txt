EFFECTIVE PERIOD:
11-FEB-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  -  B  B  B  -
03 1-0   C  -  C  C  -  C
04 1-0   D  -  -  D  -  D
05 2-0   E  E  -  -  -  -
06 0-3   -  -  -  F  F  -
07 0-4   -  G  -  G  -  G
08 0-5   H  -  -  H  -  H
09 0-5   I  I  I  -  -  -
10 0-5   -  J  J  -  J  -
11 0-5   -  K  -  K  -  -
12 0-5   L  -  L  -  L  L
13 0-5   M  -  -  M  -  -
14 0-5   -  -  N  -  N  N
15 0-5   O  -  O  -  O  -
16 0-5   -  P  -  -  -  -
17 0-5   Q  -  Q  -  Q  -
18 0-6   R  R  -  R  R   
19 0-6   -  S  S  S  -   
20 0-6   -  T  T  T      
21 0-6   U  U  -  U      
22 0-6   -  V  -         
23 0-6   -  -  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

NPOUA SOLMW MDHWO HCKOS IOOGT O
-------------------------------
