EFFECTIVE PERIOD:
12-FEB-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   -  B  B  -  B  -
03 2-0   -  C  C  -  C  -
04 2-0   D  D  -  D  -  D
05 2-0   E  E  E  -  -  E
06 0-3   -  -  -  F  -  -
07 0-3   -  G  G  G  G  G
08 0-3   H  H  -  -  H  -
09 0-3   I  I  -  I  -  -
10 0-4   -  J  J  -  J  -
11 0-5   K  -  K  -  -  -
12 0-5   -  L  -  -  -  -
13 0-5   M  -  M  -  -  -
14 0-5   -  -  -  N  N  N
15 0-5   -  O  -  -  -  O
16 1-3   P  -  P  P  -  P
17 1-3   -  -  Q  -  -  Q
18 1-4   -  -  -  R  R   
19 1-6   -  -  -  S  S   
20 2-3   -  T  -  -      
21 2-3   -  U  U  U      
22 2-3   -  -  -         
23 2-3   W  -  -         
24 2-5   -  -            
25 3-4   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NESPA PUUKN TTUKA WLTNT SKVDF X
-------------------------------
