EFFECTIVE PERIOD:
27-JUN-2034 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   B  -  -  B  B  -
03 2-0   -  C  C  -  C  -
04 2-0   D  -  -  -  D  -
05 2-0   -  E  E  E  -  E
06 0-4   F  -  -  F  -  F
07 0-4   -  -  G  G  -  G
08 0-4   -  -  H  H  H  -
09 0-4   -  -  -  I  I  -
10 0-4   -  J  J  -  -  J
11 0-4   -  -  K  -  -  -
12 0-5   L  L  -  L  -  L
13 0-5   -  M  -  -  M  -
14 0-5   -  N  -  -  N  -
15 0-5   O  O  O  -  O  O
16 0-6   P  P  P  -  P  P
17 0-6   -  -  -  -  -  Q
18 1-3   -  R  R  R  -   
19 2-4   S  S  -  S  S   
20 2-4   T  T  -  T      
21 2-4   -  U  -  -      
22 2-4   -  -  -         
23 2-5   -  X  X         
24 3-4   X  Y            
25 3-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KUIPU VVRPR STGVN WVNEC YJTSM M
-------------------------------
