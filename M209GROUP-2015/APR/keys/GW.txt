EFFECTIVE PERIOD:
25-APR-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 2-0   -  -  B  B  -  -
03 2-0   C  -  -  -  -  -
04 0-3   -  D  D  D  D  D
05 0-3   E  E  E  E  -  -
06 0-3   F  -  F  -  -  F
07 0-3   -  -  -  -  G  G
08 0-3   H  -  H  H  H  -
09 0-3   I  -  -  -  -  I
10 0-3   J  J  J  J  J  -
11 0-3   K  K  K  K  K  -
12 0-4   L  L  L  -  L  L
13 0-4   -  M  -  -  M  M
14 0-5   N  -  N  N  -  -
15 0-6   O  O  O  -  -  -
16 0-6   -  -  P  -  P  -
17 0-6   -  -  -  Q  -  Q
18 0-6   R  -  -  R  R   
19 0-6   -  -  S  S  S   
20 1-3   -  T  T  -      
21 2-4   -  U  -  -      
22 2-6   -  -  -         
23 3-6   W  X  -         
24 3-6   -  -            
25 3-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TXSZE UACEE RTNWT YEOPX TRIYM V
-------------------------------
