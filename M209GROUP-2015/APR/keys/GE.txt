EFFECTIVE PERIOD:
07-APR-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   -  B  B  B  -  -
03 1-0   -  -  C  -  -  C
04 1-0   -  D  -  D  D  -
05 1-0   -  -  E  -  -  E
06 2-0   F  F  F  F  F  F
07 0-3   G  -  G  G  -  G
08 0-3   -  -  H  H  -  -
09 0-3   I  -  -  -  I  I
10 0-3   -  J  -  J  -  -
11 0-3   K  -  -  -  -  K
12 0-3   L  -  L  L  L  -
13 0-3   -  -  -  -  -  -
14 0-3   -  -  -  N  N  -
15 0-3   O  -  O  O  -  -
16 0-4   P  P  -  -  P  -
17 0-5   -  -  -  -  -  Q
18 0-6   -  R  -  R  R   
19 0-6   S  S  -  S  S   
20 0-6   T  T  -  T      
21 0-6   U  -  -  -      
22 1-3   -  V  V         
23 1-3   W  X  X         
24 1-3   X  -            
25 1-6   -  -            
26 2-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PODKM LIRTR JTUHO SWWAL CUJIR L
-------------------------------
