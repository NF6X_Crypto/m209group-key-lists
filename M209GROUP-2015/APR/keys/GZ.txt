EFFECTIVE PERIOD:
28-APR-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  -  A  A
02 0-3   -  -  B  B  -  -
03 0-3   -  -  -  -  C  C
04 0-3   -  -  -  D  -  D
05 0-3   -  -  E  E  E  -
06 0-3   -  F  F  -  -  F
07 0-4   G  G  -  -  -  G
08 0-4   H  H  H  H  -  -
09 0-4   I  I  -  -  -  -
10 0-6   -  -  -  J  J  J
11 0-6   -  -  -  K  K  -
12 0-6   -  L  -  -  -  -
13 0-6   -  M  M  -  -  -
14 0-6   -  N  N  -  N  N
15 0-6   O  O  O  O  -  O
16 1-4   P  P  -  -  P  P
17 2-4   Q  Q  Q  -  Q  -
18 2-5   R  -  R  R  R   
19 3-4   -  -  S  -  S   
20 3-4   T  T  -  -      
21 3-4   -  -  -  U      
22 3-4   -  -  V         
23 3-6   W  -  X         
24 4-5   X  Y            
25 4-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NNNSW ADMUV ULUTK KUQNM ILUUW F
-------------------------------
