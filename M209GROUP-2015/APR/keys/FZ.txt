EFFECTIVE PERIOD:
02-APR-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  B  -  B  -  -
03 2-0   -  C  -  -  -  -
04 0-3   -  D  D  -  D  -
05 0-4   E  E  E  E  -  E
06 0-4   -  F  F  -  F  -
07 0-4   -  -  -  -  G  G
08 0-4   -  H  H  -  H  H
09 0-4   I  -  I  I  -  -
10 0-4   J  J  -  J  J  J
11 0-4   K  -  -  K  -  K
12 0-4   L  -  -  L  L  L
13 0-4   M  -  M  -  -  -
14 0-5   -  -  N  -  -  N
15 0-5   O  O  -  -  -  -
16 0-5   P  -  P  P  P  -
17 0-6   -  -  Q  -  -  -
18 0-6   R  R  -  -  -   
19 0-6   S  S  S  -  S   
20 0-6   -  T  -  T      
21 0-6   -  -  U  -      
22 1-3   -  -  V         
23 1-5   -  -  -         
24 4-6   -  Y            
25 4-6   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RIEXF CZXBQ NQEYP JJXUT WDEIR F
-------------------------------
