EFFECTIVE PERIOD:
18-JUN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  B  -  B  B
03 2-0   -  C  -  C  -  C
04 2-0   D  -  D  D  -  -
05 2-0   E  -  -  E  -  E
06 2-0   F  F  -  F  F  F
07 2-0   -  G  -  G  -  -
08 0-3   H  -  -  -  -  H
09 0-3   I  -  I  I  -  I
10 0-3   J  -  -  -  J  -
11 0-3   -  K  -  -  K  -
12 0-3   -  L  L  -  L  L
13 0-3   -  M  M  -  M  -
14 0-3   -  -  -  -  -  -
15 0-3   O  O  O  -  -  -
16 0-3   P  -  P  P  -  P
17 0-4   -  Q  Q  -  -  Q
18 0-4   R  R  R  -  R   
19 0-6   -  -  -  S  S   
20 1-2   T  T  T  -      
21 1-4   -  U  -  U      
22 2-3   V  -  V         
23 2-3   W  -  -         
24 2-3   X  Y            
25 2-3   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NPVWQ MAGVM DAPXI IMOTE ESVMS Y
-------------------------------
