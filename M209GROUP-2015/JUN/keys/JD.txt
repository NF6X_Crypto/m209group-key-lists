EFFECTIVE PERIOD:
23-JUN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   -  -  B  -  B  -
03 1-0   -  C  C  -  C  -
04 1-0   D  -  D  -  D  -
05 1-0   E  -  E  -  -  -
06 1-0   -  F  -  -  F  F
07 2-0   G  -  G  -  -  G
08 0-3   -  -  H  H  H  -
09 0-3   I  -  I  -  -  -
10 0-3   -  -  -  J  -  -
11 0-3   K  K  -  -  -  K
12 0-3   L  L  -  -  -  L
13 0-3   -  -  M  M  M  M
14 0-3   N  -  -  N  -  -
15 0-4   O  O  -  O  -  -
16 0-4   P  P  -  -  -  -
17 0-4   Q  -  Q  Q  -  Q
18 0-4   R  -  R  R  R   
19 0-5   -  S  S  S  -   
20 1-3   T  T  -  -      
21 1-3   -  -  -  U      
22 1-3   -  -  -         
23 1-3   W  X  -         
24 1-4   -  Y            
25 2-4   Y  -            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

STQLZ TILIS BNLPS MTSRL BRZGL S
-------------------------------
