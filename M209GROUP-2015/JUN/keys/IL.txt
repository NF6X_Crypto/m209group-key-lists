EFFECTIVE PERIOD:
05-JUN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   -  -  -  B  -  B
03 1-0   C  -  C  C  -  C
04 1-0   -  -  D  -  -  -
05 2-0   E  E  -  E  E  E
06 0-3   F  F  F  F  -  F
07 0-3   G  -  G  -  -  G
08 0-3   H  -  -  H  H  H
09 0-3   -  -  -  -  I  I
10 0-3   -  -  J  -  -  -
11 0-3   K  K  -  K  -  -
12 0-3   L  L  -  -  L  -
13 0-3   M  M  -  -  M  -
14 0-3   -  N  -  -  N  N
15 0-5   O  -  -  O  -  -
16 0-5   -  -  P  -  -  -
17 0-5   Q  -  Q  -  -  Q
18 0-5   R  R  -  R  R   
19 0-5   -  -  S  -  -   
20 0-6   T  T  T  T      
21 0-6   -  -  -  -      
22 1-5   V  -  V         
23 1-6   -  -  X         
24 3-5   X  Y            
25 3-5   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

INXQF NZAZV BDVUD AYFFS MOGJA Z
-------------------------------
