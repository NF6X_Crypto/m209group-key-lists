EFFECTIVE PERIOD:
23-DEC-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   B  -  B  -  B  B
03 2-0   -  -  C  -  -  -
04 2-0   D  -  -  -  -  D
05 2-0   -  -  -  E  -  -
06 2-0   -  F  -  F  -  -
07 0-5   -  -  G  -  G  -
08 0-5   H  H  -  H  -  -
09 0-5   -  I  -  -  -  -
10 0-5   J  J  -  -  J  J
11 0-5   K  K  K  -  -  K
12 0-5   L  L  L  L  L  L
13 0-5   -  -  M  -  M  -
14 0-6   -  N  N  -  N  -
15 0-6   O  O  O  -  -  O
16 0-6   P  P  -  P  P  P
17 0-6   -  -  -  -  Q  Q
18 1-2   -  R  -  R  -   
19 2-3   S  S  S  -  S   
20 2-3   T  -  -  T      
21 2-5   -  -  U  U      
22 2-5   -  V  -         
23 2-5   W  X  X         
24 2-5   -  -            
25 3-4   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AALNZ TFAAQ FOEAN ZTQSU WATTA S
-------------------------------
