EFFECTIVE PERIOD:
15-DEC-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  B  B  B  B  -
03 1-0   -  -  -  C  -  C
04 1-0   -  D  D  -  D  -
05 1-0   E  E  -  -  E  E
06 0-3   F  -  F  -  -  F
07 0-3   -  G  G  G  G  G
08 0-3   H  -  -  H  H  -
09 0-3   -  -  I  I  I  -
10 0-3   -  J  J  -  -  J
11 0-4   K  K  K  K  -  -
12 0-4   -  -  -  L  L  L
13 0-4   M  -  M  M  M  -
14 0-4   N  -  N  -  -  N
15 0-4   O  -  -  -  O  -
16 0-4   P  P  P  P  -  -
17 0-5   -  Q  -  Q  -  Q
18 0-6   R  -  R  -  -   
19 0-6   -  S  -  -  S   
20 1-3   -  T  -  -      
21 1-4   -  -  U  -      
22 1-4   V  -  -         
23 1-4   -  X  -         
24 1-4   X  Y            
25 2-4   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

YFQPR ANKMF QAPJC YAKST VQISU W
-------------------------------
