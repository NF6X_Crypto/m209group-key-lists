EFFECTIVE PERIOD:
08-JUL-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  -  B  B  -
03 1-0   -  C  C  C  -  -
04 1-0   -  -  D  -  -  D
05 1-0   E  -  -  -  E  -
06 2-0   F  -  F  F  F  -
07 2-0   G  G  -  -  -  -
08 2-0   -  -  H  H  -  -
09 0-4   I  -  I  -  I  I
10 0-4   -  -  -  J  J  J
11 0-4   K  -  -  K  -  K
12 0-4   -  -  -  L  L  L
13 0-4   -  -  -  -  -  M
14 0-5   N  N  -  -  N  -
15 0-5   O  O  O  O  O  -
16 1-4   -  P  -  P  -  -
17 1-5   -  Q  -  -  -  -
18 2-3   R  -  -  R  R   
19 2-4   S  -  S  S  S   
20 2-4   -  -  -  -      
21 2-4   -  U  U  -      
22 2-4   -  -  V         
23 2-5   W  X  X         
24 2-5   -  -            
25 2-5   -  Z            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PMAVZ KVOPR SXVPJ JWVMW JMGJY V
-------------------------------
