EFFECTIVE PERIOD:
21-JUL-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   B  -  -  B  -  -
03 1-0   C  -  C  -  -  -
04 2-0   D  -  D  -  -  D
05 2-0   -  -  E  E  -  -
06 2-0   -  F  -  -  -  -
07 0-5   -  -  -  -  G  -
08 0-5   H  H  -  -  H  H
09 0-5   -  I  I  -  I  I
10 0-5   J  -  -  -  J  J
11 0-5   -  K  K  K  K  K
12 0-5   L  L  L  L  -  -
13 0-6   M  M  -  M  -  -
14 0-6   -  -  -  N  N  N
15 0-6   O  O  -  -  O  O
16 0-6   -  P  P  -  P  P
17 0-6   -  -  Q  -  Q  -
18 1-3   -  R  R  R  R   
19 1-5   S  -  S  S  -   
20 1-6   -  T  T  T      
21 2-5   -  U  -  U      
22 2-5   -  -  -         
23 2-5   W  X  X         
24 2-5   X  Y            
25 2-6   Y  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

OINAU XRMVP JFHHX SVSVI NXAUS V
-------------------------------
