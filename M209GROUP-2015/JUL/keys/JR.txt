EFFECTIVE PERIOD:
07-JUL-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  B  -  B  B  B
03 1-0   C  C  -  -  -  -
04 1-0   D  D  D  D  D  D
05 1-0   -  E  -  -  E  -
06 0-3   F  -  -  F  -  F
07 0-3   -  -  -  G  G  -
08 0-3   H  -  H  H  -  -
09 0-4   -  I  -  -  -  I
10 0-5   J  J  J  J  J  J
11 0-5   -  -  K  -  K  K
12 0-5   -  -  -  -  -  L
13 0-5   -  -  -  M  M  M
14 0-6   N  -  N  -  -  N
15 0-6   -  O  O  -  O  -
16 0-6   P  -  P  -  -  P
17 0-6   -  Q  Q  Q  Q  -
18 1-3   -  R  -  -  -   
19 1-3   S  S  S  S  S   
20 1-3   T  T  -  -      
21 1-3   -  -  -  U      
22 1-4   -  -  V         
23 1-4   -  X  X         
24 2-4   X  Y            
25 3-5   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WROZN SEFWU SFOPT JSRXO ZEKHT R
-------------------------------
