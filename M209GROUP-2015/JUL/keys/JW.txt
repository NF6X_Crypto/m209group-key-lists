EFFECTIVE PERIOD:
12-JUL-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  -  B  -  B  -
03 1-0   -  C  C  -  C  -
04 2-0   -  -  -  -  D  -
05 2-0   -  E  E  E  -  E
06 2-0   F  -  -  F  -  -
07 2-0   G  G  G  G  G  -
08 2-0   H  H  -  H  -  -
09 2-0   -  I  -  I  I  I
10 2-0   J  J  J  -  -  J
11 2-0   K  -  -  K  -  K
12 2-0   L  L  L  L  L  -
13 0-3   -  -  -  -  M  M
14 0-5   N  N  N  N  -  -
15 0-6   O  O  -  O  O  O
16 0-6   -  P  -  -  P  -
17 0-6   -  -  Q  -  -  Q
18 0-6   R  R  R  R  R   
19 0-6   S  S  -  S  -   
20 0-6   T  -  -  -      
21 0-6   -  U  -  -      
22 1-4   -  V  -         
23 1-6   W  X  -         
24 2-6   X  -            
25 2-6   Y  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

WOPTA BWQSG CNFND VRYQX SPSOQ S
-------------------------------
