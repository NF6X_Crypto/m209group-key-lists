EFFECTIVE PERIOD:
25-FEB-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 2-0   B  B  -  B  -  -
03 2-0   -  -  C  C  C  C
04 2-0   -  D  D  D  D  -
05 2-0   E  -  E  -  E  E
06 2-0   -  -  -  -  -  F
07 2-0   G  -  -  -  -  -
08 0-3   -  -  -  -  -  H
09 0-4   -  I  I  -  -  I
10 0-5   -  -  -  J  J  J
11 0-5   K  -  -  -  -  K
12 0-5   L  L  L  L  -  -
13 0-5   M  M  M  M  M  -
14 0-5   N  N  -  -  N  -
15 0-5   -  O  O  -  O  -
16 0-5   -  P  P  -  -  P
17 0-5   Q  -  -  Q  Q  -
18 0-5   -  -  -  -  -   
19 0-5   -  S  S  -  S   
20 0-5   -  T  -  T      
21 0-5   -  -  -  U      
22 0-5   -  -  V         
23 0-6   -  -  -         
24 0-6   X  -            
25 0-6   Y  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

LWPMH YZSEE PSACL EDRDF ORZGZ J
-------------------------------
