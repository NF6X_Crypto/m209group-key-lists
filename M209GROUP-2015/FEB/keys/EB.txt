EFFECTIVE PERIOD:
11-FEB-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   B  B  -  -  -  -
03 1-0   C  C  C  C  C  -
04 1-0   -  D  -  D  D  D
05 1-0   -  E  -  -  -  E
06 2-0   -  -  -  F  -  F
07 2-0   -  G  -  -  -  -
08 0-3   H  H  -  -  -  H
09 0-3   I  -  I  -  I  I
10 0-3   -  J  -  -  J  -
11 0-3   -  K  -  -  -  -
12 0-3   -  L  L  L  L  -
13 0-3   -  M  -  M  M  M
14 0-6   N  N  N  N  -  N
15 0-6   -  -  -  O  -  -
16 0-6   -  P  P  -  P  -
17 0-6   Q  -  Q  Q  Q  Q
18 1-2   -  -  R  -  R   
19 1-4   S  S  -  S  S   
20 1-5   T  T  T  T      
21 1-6   -  -  U  U      
22 1-6   V  -  V         
23 1-6   -  X  -         
24 1-6   -  -            
25 2-3   Y  -            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KPQXW FYKML RLAPO NBFWR PMFVP K
-------------------------------
