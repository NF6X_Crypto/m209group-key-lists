EFFECTIVE PERIOD:
30-AUG-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 2-0   -  B  B  B  B  -
03 2-0   -  C  -  C  -  C
04 0-3   D  D  -  -  D  -
05 0-4   -  -  E  -  E  -
06 0-4   F  -  -  F  -  F
07 0-4   G  G  -  G  -  -
08 0-4   H  -  -  -  H  H
09 0-4   I  -  -  -  I  I
10 0-5   J  J  -  -  -  J
11 0-5   -  -  K  K  K  K
12 0-5   -  -  -  L  -  -
13 0-5   -  M  -  -  -  M
14 0-6   N  N  -  -  -  -
15 0-6   -  O  O  O  -  -
16 0-6   -  P  P  -  P  -
17 0-6   -  -  Q  -  Q  -
18 0-6   R  -  R  -  R   
19 0-6   -  S  S  S  S   
20 0-6   T  T  -  T      
21 0-6   U  -  U  -      
22 0-6   V  V  V         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   -  -            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

PETBJ JKACU EOTDW VRQFR KZZMV I
-------------------------------
