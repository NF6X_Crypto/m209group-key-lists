EFFECTIVE PERIOD:
10-AUG-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  B  -  -  B
03 2-0   C  C  C  C  -  -
04 2-0   -  -  -  D  -  -
05 2-0   -  E  -  -  E  E
06 2-0   F  F  -  F  F  -
07 2-0   G  -  -  G  -  -
08 0-4   H  H  -  H  -  H
09 0-5   -  -  I  I  I  -
10 0-5   -  -  J  J  -  -
11 0-5   -  -  K  -  K  K
12 0-5   -  -  -  -  -  L
13 0-6   M  -  M  -  -  -
14 0-6   N  N  -  -  -  N
15 0-6   -  -  O  -  O  O
16 0-6   P  P  -  P  P  -
17 0-6   -  Q  -  Q  Q  -
18 0-6   -  R  -  R  R   
19 0-6   -  S  S  -  -   
20 0-6   -  T  T  T      
21 0-6   -  -  U  -      
22 1-2   V  V  V         
23 1-3   W  X  X         
24 2-5   -  Y            
25 5-6   Y  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PTGXX PLWXK DQMQM HRQAA MLILA R
-------------------------------
