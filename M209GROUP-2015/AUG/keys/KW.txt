EFFECTIVE PERIOD:
07-AUG-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  A
02 2-0   B  -  -  B  B  -
03 2-0   -  C  -  C  -  C
04 0-3   -  -  -  D  D  -
05 0-3   E  E  E  -  -  -
06 0-3   -  F  -  -  -  -
07 0-3   G  G  -  G  G  -
08 0-3   H  -  H  -  -  -
09 0-3   I  I  -  I  -  I
10 0-3   J  -  J  J  J  -
11 0-3   -  K  K  -  -  K
12 0-4   L  L  -  -  L  L
13 0-4   M  -  M  -  M  M
14 0-4   -  N  -  -  -  -
15 0-4   -  -  -  -  -  -
16 0-4   -  P  P  P  -  P
17 0-4   -  -  -  -  -  Q
18 0-4   R  -  -  R  R   
19 0-4   -  S  S  S  S   
20 0-4   T  T  T  T      
21 0-4   -  U  U  U      
22 0-4   V  -  -         
23 0-4   -  -  -         
24 0-5   -  Y            
25 0-6   -  Z            
26 1-5   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

DGJLE YAXOG XRMDK RDANO KDNBQ K
-------------------------------
