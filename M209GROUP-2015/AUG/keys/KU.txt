EFFECTIVE PERIOD:
05-AUG-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 2-0   -  B  -  -  -  -
03 2-0   C  C  -  -  C  -
04 2-0   D  -  D  D  -  D
05 2-0   E  -  E  E  E  E
06 2-0   F  -  -  F  -  -
07 0-3   -  G  -  -  G  -
08 0-3   -  -  H  -  H  H
09 0-3   I  I  -  -  -  -
10 0-3   -  -  J  -  -  -
11 0-3   K  -  -  K  -  -
12 0-3   -  -  L  L  -  L
13 0-3   M  M  M  M  M  M
14 0-3   N  N  N  -  N  -
15 0-4   -  O  -  -  O  O
16 0-4   -  -  P  P  P  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  S  S  S   
20 0-6   -  T  -  T      
21 0-6   -  -  U  -      
22 0-6   V  -  -         
23 0-6   W  X  X         
24 0-6   -  Y            
25 0-6   Y  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QJSRJ ONVFO POIRV BANIA DVIYS N
-------------------------------
