EFFECTIVE PERIOD:
16-MAR-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  A  -  -
02 0-3   B  -  B  -  B  -
03 0-3   C  C  C  -  -  -
04 0-3   D  D  -  D  -  -
05 0-3   E  -  E  E  -  -
06 0-3   -  -  F  F  -  -
07 0-4   G  -  -  G  -  G
08 0-4   H  H  -  -  H  H
09 0-4   I  I  I  -  -  -
10 0-4   J  -  J  -  -  J
11 0-4   K  K  K  -  K  K
12 0-4   -  L  L  L  -  L
13 0-5   M  M  -  -  M  -
14 0-5   -  -  -  N  N  -
15 0-5   -  -  O  -  O  O
16 1-2   -  -  -  -  P  P
17 1-4   Q  -  -  -  Q  Q
18 1-5   R  R  -  R  R   
19 1-5   -  S  -  -  -   
20 1-5   T  -  T  T      
21 1-5   -  -  -  -      
22 3-4   -  V  -         
23 3-5   -  -  X         
24 3-5   -  Y            
25 3-5   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AANMN TSIZA AKWNX AAGUQ AUUNS L
-------------------------------
