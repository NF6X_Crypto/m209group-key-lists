EFFECTIVE PERIOD:
31-OCT-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   -  -  B  B  B  -
03 2-0   C  C  C  C  C  C
04 2-0   -  -  D  -  -  -
05 2-0   -  E  -  E  -  E
06 2-0   -  -  -  -  -  -
07 2-0   G  -  G  -  -  G
08 2-0   H  H  H  H  -  -
09 0-3   I  -  -  I  -  -
10 0-3   J  -  -  -  J  -
11 0-3   -  K  K  -  -  K
12 0-3   L  L  L  -  L  L
13 0-3   -  M  -  -  M  M
14 0-3   N  N  N  -  N  N
15 0-4   -  -  -  O  -  -
16 0-4   -  P  -  P  P  P
17 0-4   Q  -  Q  -  -  -
18 0-4   R  -  -  R  -   
19 0-6   -  -  S  S  S   
20 1-6   T  T  -  -      
21 2-4   -  -  -  -      
22 2-4   V  -  -         
23 2-4   -  X  X         
24 2-4   -  Y            
25 2-5   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

KPNRC ZKALW QZHAZ LPTPE LTPXS Z
-------------------------------
