EFFECTIVE PERIOD:
19-OCT-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  -  -  B  B  B
03 1-0   -  -  C  -  -  -
04 1-0   D  -  -  -  -  -
05 1-0   E  -  -  -  -  E
06 0-4   F  -  F  F  -  F
07 0-4   G  -  G  -  -  G
08 0-4   -  H  H  -  -  -
09 0-5   -  -  -  I  I  -
10 0-5   J  J  -  -  J  -
11 0-5   K  -  -  -  K  -
12 0-5   -  L  L  L  -  L
13 0-6   -  -  M  M  M  -
14 0-6   -  N  -  -  N  N
15 0-6   O  O  O  O  O  -
16 0-6   -  -  -  -  P  P
17 0-6   Q  Q  -  -  -  -
18 1-5   -  R  -  -  -   
19 1-6   -  S  S  S  -   
20 1-6   -  -  -  T      
21 1-6   U  U  U  U      
22 1-6   -  -  -         
23 2-3   -  X  -         
24 3-4   -  Y            
25 3-6   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

GXLRG SGQQU HRSKP MRVRZ AVSKM R
-------------------------------
