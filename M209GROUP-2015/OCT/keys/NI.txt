EFFECTIVE PERIOD:
10-OCT-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   B  -  B  B  B  -
03 2-0   -  -  -  C  C  C
04 2-0   D  -  -  D  -  -
05 2-0   -  -  E  -  -  -
06 2-0   F  F  F  F  -  -
07 2-0   G  G  G  -  -  -
08 0-3   -  H  H  H  -  -
09 0-4   -  I  I  I  I  I
10 0-4   -  J  -  -  -  J
11 0-4   K  -  -  -  K  K
12 0-4   -  L  -  L  -  L
13 0-4   M  -  -  -  -  -
14 0-4   -  N  N  -  -  N
15 0-4   -  O  -  -  O  -
16 0-4   P  P  P  P  -  -
17 0-5   Q  -  -  -  Q  Q
18 0-5   R  -  R  -  R   
19 0-5   S  -  S  S  S   
20 0-5   -  -  -  T      
21 0-6   -  U  U  -      
22 1-3   V  V  V         
23 2-4   W  X  X         
24 2-4   -  Y            
25 2-4   Y  Z            
26 2-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

OMDTC HZAMA WNMZO DAWRJ SBOEX Q
-------------------------------
