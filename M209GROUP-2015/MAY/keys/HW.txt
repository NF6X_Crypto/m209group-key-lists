EFFECTIVE PERIOD:
21-MAY-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  -
02 2-0   -  -  B  B  B  B
03 2-0   C  -  C  -  C  -
04 2-0   D  D  -  D  -  -
05 0-3   E  E  -  -  -  E
06 0-3   -  F  F  F  -  -
07 0-3   G  -  -  -  G  G
08 0-3   -  H  -  -  H  H
09 0-3   I  -  -  I  -  -
10 0-3   -  J  J  J  J  -
11 0-3   -  -  K  -  K  -
12 0-3   L  L  L  -  -  L
13 0-4   M  -  -  -  -  M
14 0-4   -  N  N  N  -  N
15 0-4   -  -  -  -  -  O
16 0-4   -  P  -  -  -  -
17 0-4   Q  Q  Q  -  Q  -
18 0-4   -  R  -  R  R   
19 0-5   -  -  -  S  S   
20 0-5   T  T  T  T      
21 0-6   U  -  -  -      
22 1-6   V  -  -         
23 2-4   -  -  X         
24 2-5   -  Y            
25 3-4   -  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JQISC RVMFS RMQCS TFNSM SMOKI F
-------------------------------
