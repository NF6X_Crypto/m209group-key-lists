EFFECTIVE PERIOD:
05-MAY-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 2-0   -  B  -  -  -  B
03 0-3   -  C  -  -  C  -
04 0-3   D  -  -  D  -  -
05 0-3   -  E  E  E  E  E
06 0-4   -  F  F  -  -  -
07 0-4   G  G  G  G  -  -
08 0-4   -  -  H  H  H  H
09 0-4   I  -  I  I  -  -
10 0-4   -  -  -  J  -  -
11 0-4   -  K  K  -  K  -
12 0-4   L  L  -  -  L  -
13 0-4   -  -  M  -  M  M
14 0-4   N  N  -  N  N  N
15 0-4   O  -  -  O  O  O
16 0-4   P  -  P  P  P  P
17 0-4   Q  -  -  Q  -  -
18 0-4   -  R  -  -  -   
19 0-5   S  -  S  -  -   
20 0-6   -  T  T  -      
21 0-6   -  U  U  -      
22 0-6   V  V  V         
23 0-6   W  X  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-2   -               
27 1-3                   
-------------------------------
26 LETTER CHECK

DQXXT LGMMS CJPOL TSTRJ KIANW Y
-------------------------------
