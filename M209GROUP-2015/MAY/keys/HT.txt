EFFECTIVE PERIOD:
18-MAY-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   -  B  B  -  B  -
03 2-0   -  -  -  -  -  C
04 2-0   -  D  D  -  D  D
05 2-0   -  -  E  E  -  -
06 2-0   F  -  F  F  F  F
07 2-0   G  -  -  G  -  -
08 0-3   -  -  -  H  H  -
09 0-3   I  I  -  -  I  I
10 0-3   -  J  J  J  -  J
11 0-4   -  -  -  K  K  K
12 0-4   L  L  -  -  -  L
13 0-4   -  -  -  -  -  M
14 0-4   -  -  N  -  -  N
15 0-4   -  O  O  O  -  -
16 0-4   P  -  -  -  -  -
17 0-4   Q  Q  Q  -  Q  -
18 0-4   R  -  -  R  R   
19 0-4   -  -  -  S  -   
20 0-6   T  -  T  T      
21 0-6   -  -  U  U      
22 0-6   V  V  V         
23 0-6   -  -  -         
24 0-6   X  Y            
25 0-6   -  Z            
26 1-3   Z               
27 1-5                   
-------------------------------
26 LETTER CHECK

RLXJC JOSOV RELNS TRIPA LZIME G
-------------------------------
