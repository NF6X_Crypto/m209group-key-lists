EFFECTIVE PERIOD:
30-MAY-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   B  B  B  -  B  B
03 1-0   C  -  C  C  C  -
04 1-0   D  -  -  -  -  D
05 1-0   -  E  -  -  E  -
06 1-0   -  -  F  F  -  -
07 1-0   G  G  G  G  -  -
08 1-0   -  -  -  H  -  H
09 2-0   -  -  I  -  -  -
10 2-0   J  J  J  J  -  -
11 0-3   K  -  -  -  -  -
12 0-5   -  L  L  L  L  L
13 0-5   M  M  -  -  -  M
14 0-5   -  -  -  -  N  N
15 0-5   -  -  -  O  -  -
16 0-5   P  -  -  P  P  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  R  R  -  R   
19 0-5   S  S  S  S  S   
20 0-5   -  -  -  -      
21 0-6   -  -  -  -      
22 0-6   -  V  -         
23 0-6   W  -  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-4   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

QNRRN JPZFQ REVHO EBLHP QRDZP P
-------------------------------
