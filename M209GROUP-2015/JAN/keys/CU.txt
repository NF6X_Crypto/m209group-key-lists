EFFECTIVE PERIOD:
09-JAN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  -  B  B  B
03 1-0   -  C  -  C  -  -
04 1-0   D  -  -  -  -  -
05 0-3   -  E  E  -  -  E
06 0-3   -  -  -  -  F  F
07 0-3   -  G  -  -  -  -
08 0-3   -  -  H  H  -  -
09 0-5   I  -  -  -  -  I
10 0-5   J  -  -  -  J  -
11 0-5   K  -  K  K  K  -
12 0-5   -  L  -  L  L  L
13 0-5   M  M  M  M  -  M
14 0-5   N  N  N  -  -  -
15 0-6   -  O  O  O  -  -
16 1-2   -  -  P  P  -  P
17 1-3   Q  -  Q  -  Q  -
18 1-3   -  R  -  -  -   
19 1-3   S  S  -  S  S   
20 1-3   T  -  -  T      
21 1-4   U  -  -  U      
22 1-6   -  -  V         
23 1-6   W  X  -         
24 1-6   -  -            
25 2-6   Y  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SUPSN WAUWU SPPJR GARMW RMPSZ P
-------------------------------
