EFFECTIVE PERIOD:
26-JAN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  -  B  -  B  B
03 1-0   -  C  -  -  -  C
04 1-0   -  -  -  D  D  D
05 2-0   E  E  -  -  -  E
06 2-0   F  F  F  F  F  -
07 2-0   G  G  -  -  -  G
08 2-0   H  -  -  -  H  -
09 0-5   -  -  I  -  -  -
10 0-5   J  J  J  -  J  J
11 0-5   -  -  K  K  K  K
12 0-5   L  -  -  L  L  -
13 0-5   M  -  M  -  -  -
14 0-5   -  -  N  N  -  -
15 0-5   O  -  -  O  -  O
16 1-2   P  -  -  P  -  -
17 1-3   Q  Q  Q  Q  -  -
18 1-4   -  -  -  -  R   
19 1-4   S  S  -  S  -   
20 1-5   -  T  T  T      
21 1-5   -  U  U  U      
22 1-5   V  V  V         
23 1-5   W  -  X         
24 2-4   X  -            
25 2-5   -  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

VNJOK GATOO HONSD ASZVR TAOOD W
-------------------------------
