EFFECTIVE PERIOD:
13-JAN-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  B  B  -  B  B
03 1-0   C  C  -  C  C  -
04 1-0   D  -  D  D  D  D
05 1-0   E  E  -  -  -  -
06 2-0   F  -  F  F  -  -
07 0-3   G  -  -  -  -  G
08 0-3   H  H  -  -  -  -
09 0-3   -  I  -  I  -  -
10 0-3   J  J  J  J  J  J
11 0-6   -  K  K  -  -  -
12 0-6   -  L  L  -  -  L
13 0-6   -  -  M  M  M  M
14 0-6   -  -  -  -  N  N
15 0-6   -  -  -  O  O  -
16 1-2   P  -  -  -  P  P
17 1-6   -  -  -  Q  -  Q
18 2-3   R  -  R  R  -   
19 2-3   S  S  -  -  -   
20 2-3   T  -  -  -      
21 2-5   U  U  -  -      
22 3-5   V  -  -         
23 3-6   W  -  X         
24 3-6   -  Y            
25 3-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HMSSV QUPMP VVGZH UPJVU APVMJ K
-------------------------------
