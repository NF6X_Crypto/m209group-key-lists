EFFECTIVE PERIOD:
25-SEP-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   A  -  -  A  -  A
02 0-4   B  B  B  B  B  B
03 0-4   C  C  C  -  -  C
04 0-4   D  -  -  D  -  D
05 0-5   E  -  -  -  -  -
06 0-5   -  -  -  -  -  F
07 0-5   G  G  G  G  -  -
08 0-5   H  -  H  -  H  -
09 0-5   I  -  -  -  I  -
10 0-6   J  -  J  J  -  J
11 0-6   K  K  K  K  K  K
12 0-6   L  -  L  -  L  L
13 0-6   -  M  -  -  M  M
14 0-6   -  N  -  N  N  -
15 0-6   O  -  O  -  -  O
16 1-3   -  -  P  -  P  -
17 1-4   -  -  Q  Q  -  -
18 1-4   R  -  -  -  -   
19 1-4   -  -  S  -  S   
20 1-4   T  T  -  T      
21 1-6   -  -  -  U      
22 2-4   -  V  V         
23 4-5   -  X  -         
24 4-5   -  Y            
25 4-5   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QAVAA BZZVZ VFVGF URNAA PWOKJ V
-------------------------------
