EFFECTIVE PERIOD:
04-SEP-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 0-3   B  -  -  B  -  B
03 0-3   -  -  -  -  C  C
04 0-3   D  D  D  D  -  D
05 0-3   -  E  -  -  -  E
06 0-3   -  -  -  -  -  -
07 0-3   -  G  -  G  G  -
08 0-3   H  -  H  H  H  -
09 0-3   I  -  I  I  -  -
10 0-4   J  J  J  -  J  J
11 0-4   -  K  -  -  K  K
12 0-4   L  -  -  -  -  L
13 0-4   -  M  M  M  -  M
14 0-4   -  -  -  N  -  N
15 0-4   O  O  -  -  O  -
16 0-6   P  P  P  -  -  P
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  R  R  R   
19 0-6   S  -  S  -  -   
20 0-6   T  T  -  -      
21 0-6   -  U  U  U      
22 0-6   -  V  -         
23 0-6   W  X  -         
24 0-6   -  Y            
25 0-6   -  Z            
26 2-4   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

SRTTP ZBKRL RKHHC HQZTS MHABA A
-------------------------------
