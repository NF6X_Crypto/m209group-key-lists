EFFECTIVE PERIOD:
09-NOV-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   -  B  B  B  B  -
03 1-0   C  C  -  -  -  C
04 1-0   D  D  D  D  -  -
05 2-0   -  E  E  -  -  E
06 0-3   -  -  -  F  -  -
07 0-3   -  -  -  G  -  G
08 0-3   H  H  H  -  -  H
09 0-3   -  -  I  I  I  I
10 0-3   J  -  J  -  J  J
11 0-6   K  K  -  -  -  K
12 0-6   L  L  L  -  L  L
13 0-6   -  M  M  -  -  -
14 0-6   N  -  N  N  -  -
15 0-6   O  O  -  O  -  O
16 0-6   P  -  -  P  P  -
17 0-6   -  Q  Q  -  -  Q
18 1-2   -  -  -  R  R   
19 1-3   S  -  S  -  S   
20 1-3   T  -  -  -      
21 1-3   -  -  U  U      
22 1-3   -  V  -         
23 1-5   W  X  X         
24 2-5   X  -            
25 2-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

AJEQU KLTNU EREZA MLUZM JZTWK N
-------------------------------
