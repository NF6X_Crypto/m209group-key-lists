EFFECTIVE PERIOD:
23-NOV-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  -  B  -  B  -
03 1-0   C  -  C  C  C  C
04 1-0   -  -  -  D  D  D
05 1-0   E  -  -  -  E  E
06 1-0   F  F  F  F  -  -
07 1-0   G  -  -  G  -  G
08 1-0   -  H  H  -  H  -
09 1-0   I  I  -  I  I  -
10 1-0   -  -  J  -  J  -
11 1-0   K  -  K  K  -  -
12 1-0   L  L  -  -  L  -
13 2-0   -  -  M  -  -  -
14 0-3   -  N  -  -  N  N
15 0-3   -  O  -  O  -  O
16 0-3   -  P  P  P  P  P
17 0-3   Q  Q  -  -  -  Q
18 0-3   -  R  -  R  R   
19 0-3   -  -  -  S  -   
20 0-4   T  -  T  T      
21 0-6   -  -  U  -      
22 0-6   V  -  -         
23 0-6   W  X  X         
24 1-3   -  Y            
25 2-5   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

COLHS FZYBZ GNWRA WPWBX LSZGH K
-------------------------------
