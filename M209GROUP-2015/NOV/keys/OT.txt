EFFECTIVE PERIOD:
16-NOV-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 2-0   -  B  -  -  -  B
03 2-0   -  -  -  C  -  -
04 2-0   D  D  D  D  D  -
05 2-0   -  -  -  -  -  E
06 0-3   F  F  F  F  -  F
07 0-4   G  -  -  -  G  G
08 0-4   H  -  H  -  H  -
09 0-4   I  I  I  I  -  I
10 0-4   -  J  -  J  J  -
11 0-4   -  K  -  K  -  -
12 0-5   L  -  L  -  L  L
13 0-5   M  -  M  -  M  -
14 0-5   N  N  -  N  N  -
15 0-5   O  O  -  O  O  O
16 0-5   -  -  P  P  P  -
17 0-5   Q  -  -  -  -  Q
18 1-6   R  -  R  -  -   
19 2-3   -  -  S  -  -   
20 2-4   -  -  T  T      
21 2-4   -  U  -  -      
22 2-4   -  V  V         
23 2-4   W  X  -         
24 2-6   X  -            
25 3-5   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

STUQM DOSOJ ZTSFY MLUPY SVUTS O
-------------------------------
