EFFECTIVE PERIOD:
11-NOV-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  B  -  B  B  B
03 1-0   -  C  C  -  C  -
04 1-0   -  D  -  D  D  D
05 1-0   -  E  -  -  -  E
06 0-3   F  -  F  -  F  -
07 0-3   G  -  G  -  -  -
08 0-3   -  -  H  H  -  H
09 0-3   -  I  I  -  -  I
10 0-3   J  J  -  J  J  J
11 0-3   -  -  K  -  K  -
12 0-4   L  L  L  L  -  L
13 0-4   M  -  M  M  -  -
14 0-4   -  -  -  N  -  N
15 0-5   -  O  -  O  O  -
16 0-5   -  P  P  -  P  P
17 0-5   -  -  -  Q  -  Q
18 1-4   -  -  -  R  -   
19 1-5   S  -  S  -  -   
20 2-3   -  -  -  -      
21 2-3   U  U  -  -      
22 2-5   V  V  -         
23 2-6   -  -  -         
24 3-4   -  Y            
25 3-4   Y  -            
26 3-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

LLPLM WKDFU ANXHD NWWVD XPASZ R
-------------------------------
