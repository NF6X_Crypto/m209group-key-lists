EFFECTIVE PERIOD:
06-NOV-2015 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  B  -  -  -  -
03 1-0   -  -  -  -  C  C
04 1-0   -  D  -  -  -  D
05 1-0   E  -  E  E  -  -
06 1-0   F  F  -  F  F  -
07 2-0   -  -  -  -  -  G
08 0-3   -  H  H  H  H  H
09 0-3   I  -  I  -  -  -
10 0-3   -  -  J  J  -  -
11 0-3   K  -  -  K  -  -
12 0-3   L  L  -  L  -  L
13 0-3   M  -  M  M  M  M
14 0-3   N  -  N  -  -  -
15 0-3   -  O  O  O  O  -
16 0-4   -  P  P  P  P  -
17 0-4   -  -  Q  -  Q  -
18 0-6   -  -  -  -  R   
19 0-6   -  -  -  -  -   
20 0-6   -  T  T  -      
21 0-6   U  -  -  U      
22 1-3   V  -  -         
23 1-3   W  -  X         
24 1-3   -  Y            
25 1-6   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PSTOA LSODA HJKIU KOKUZ DNTVZ M
-------------------------------
