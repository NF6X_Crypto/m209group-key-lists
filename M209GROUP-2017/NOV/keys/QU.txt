EFFECTIVE PERIOD:
14-NOV-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  -  A
02 0-3   -  B  -  B  -  B
03 0-3   C  -  C  -  -  C
04 0-3   D  -  D  D  D  D
05 0-4   E  E  E  E  E  -
06 0-4   F  -  -  F  -  F
07 0-4   -  G  G  -  G  -
08 0-5   H  H  -  -  H  -
09 0-5   I  -  I  -  I  I
10 0-5   -  J  J  J  J  J
11 0-5   -  K  -  K  -  -
12 0-5   -  -  L  L  L  -
13 0-5   M  -  -  -  -  M
14 0-6   N  N  N  -  N  N
15 0-6   -  O  -  O  -  -
16 1-2   P  P  P  -  -  -
17 2-4   -  Q  Q  -  Q  -
18 2-6   -  R  R  R  -   
19 2-6   -  -  -  -  S   
20 2-6   -  -  -  -      
21 3-5   -  U  U  -      
22 3-6   V  V  -         
23 3-6   W  X  -         
24 3-6   -  Y            
25 3-6   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UEAQI MEXLL TIAPV TWMSA PUZPT Q
-------------------------------
