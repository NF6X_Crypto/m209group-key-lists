EFFECTIVE PERIOD:
08-NOV-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  A  -  A  -
02 0-3   -  -  B  B  -  -
03 0-3   C  -  -  -  C  C
04 0-3   D  D  D  -  D  D
05 0-3   E  -  E  E  E  E
06 0-3   -  -  -  -  F  F
07 0-3   G  -  -  -  -  -
08 0-4   -  -  -  -  -  H
09 0-4   I  -  I  I  I  -
10 0-4   J  -  J  -  -  -
11 0-5   -  K  -  K  K  -
12 0-5   L  -  L  -  -  -
13 0-5   -  -  M  -  -  M
14 0-5   N  -  -  N  N  N
15 0-5   O  -  O  -  -  O
16 1-4   P  P  P  -  P  P
17 1-6   -  Q  Q  Q  -  -
18 2-4   -  R  -  R  R   
19 3-4   S  S  -  S  -   
20 3-4   -  -  -  T      
21 3-4   -  U  U  U      
22 3-4   V  -  V         
23 3-5   -  X  -         
24 4-5   -  Y            
25 4-5   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

BTWQT NTTZD AWKAQ BIMTV DWVIN A
-------------------------------
