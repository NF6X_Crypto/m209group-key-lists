EFFECTIVE PERIOD:
03-JUN-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  B  -  B  -  -
03 2-0   C  -  -  C  -  C
04 2-0   -  D  D  D  D  D
05 2-0   -  E  E  -  -  -
06 0-3   F  F  F  F  F  F
07 0-3   -  G  G  -  -  -
08 0-3   H  -  -  -  -  -
09 0-3   I  I  I  I  -  I
10 0-3   J  -  -  -  J  J
11 0-3   -  -  K  -  K  -
12 0-5   L  L  -  -  L  -
13 0-5   -  M  M  M  M  M
14 0-5   N  -  N  N  -  -
15 0-5   -  O  -  -  -  -
16 1-3   -  -  -  P  -  -
17 1-4   Q  Q  -  -  Q  Q
18 1-5   R  -  -  R  R   
19 1-5   -  S  S  -  S   
20 1-5   -  T  T  -      
21 2-3   U  -  U  U      
22 2-5   V  -  -         
23 2-5   W  -  -         
24 2-5   -  Y            
25 2-5   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YQPBH AXXNQ KUKXQ PVRXU XBVPQ Q
-------------------------------
