EFFECTIVE PERIOD:
09-JUN-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  -  -  -  B
03 1-0   C  C  -  C  C  -
04 1-0   -  -  -  D  -  -
05 1-0   -  -  E  -  -  E
06 1-0   -  F  -  F  -  -
07 1-0   G  G  -  -  G  G
08 0-3   H  H  H  -  H  -
09 0-3   -  -  I  -  I  -
10 0-3   -  -  -  -  -  J
11 0-3   K  K  K  K  K  -
12 0-6   L  -  L  L  L  -
13 0-6   -  -  M  M  M  M
14 0-6   -  N  -  -  -  -
15 0-6   -  O  -  O  -  O
16 1-3   -  -  P  -  P  P
17 1-6   Q  -  -  Q  Q  -
18 1-6   -  R  -  R  -   
19 1-6   S  S  S  -  S   
20 1-6   T  -  T  T      
21 2-6   U  -  -  U      
22 3-4   -  V  V         
23 4-5   W  -  -         
24 4-6   X  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GNTMW AFWAR TOFUK BNRTO KAZFN T
-------------------------------
