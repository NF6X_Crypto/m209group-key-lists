EFFECTIVE PERIOD:
23-JUN-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   -  B  B  -  -  B
03 2-0   C  -  -  C  -  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  -  -  E  -
06 0-3   -  F  -  -  F  F
07 0-3   G  -  G  G  G  G
08 0-3   -  H  H  -  H  -
09 0-3   I  I  I  I  I  I
10 0-3   -  -  J  J  J  -
11 0-3   K  -  K  -  -  -
12 0-4   -  L  L  -  -  L
13 0-6   M  -  -  M  -  M
14 0-6   -  -  -  N  N  -
15 0-6   -  O  -  O  -  O
16 0-6   -  P  P  -  -  -
17 0-6   Q  Q  -  -  Q  -
18 0-6   R  -  -  R  -   
19 0-6   S  -  S  -  -   
20 0-6   -  -  -  T      
21 0-6   -  U  -  U      
22 2-3   V  V  -         
23 2-4   W  X  -         
24 3-6   X  -            
25 3-6   -  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

HUAAL MGIUZ GMTUH PPNZY JWHDA X
-------------------------------
