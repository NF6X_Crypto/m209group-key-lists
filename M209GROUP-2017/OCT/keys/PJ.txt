EFFECTIVE PERIOD:
08-OCT-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  B  -  -  -  B
03 1-0   C  -  -  -  C  C
04 1-0   -  D  D  D  -  D
05 1-0   -  E  E  E  E  -
06 1-0   -  -  -  F  F  F
07 1-0   G  G  -  -  -  -
08 1-0   H  -  H  H  H  H
09 0-3   -  I  I  -  -  -
10 0-3   J  -  -  J  J  J
11 0-4   K  K  K  K  K  -
12 0-5   -  L  L  L  L  L
13 0-5   -  -  M  M  M  -
14 0-5   N  -  N  -  N  -
15 0-5   O  O  O  O  -  -
16 0-6   -  -  -  -  -  -
17 0-6   Q  Q  -  Q  Q  -
18 0-6   -  -  -  -  -   
19 0-6   -  -  S  -  S   
20 0-6   -  -  -  T      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 0-6   -  Y            
25 0-6   Y  -            
26 2-3   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

IZGZA PIQLS WHYQO FKPAG QQDEP T
-------------------------------
