EFFECTIVE PERIOD:
30-OCT-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   -  B  -  -  B  -
03 1-0   C  -  C  -  -  -
04 1-0   D  D  D  D  -  D
05 1-0   -  E  E  -  E  -
06 1-0   -  -  -  -  F  -
07 1-0   -  -  G  G  -  -
08 2-0   H  H  -  -  H  H
09 2-0   I  -  I  -  -  I
10 0-3   -  J  -  J  J  J
11 0-4   -  K  K  -  K  K
12 0-4   L  -  L  -  -  L
13 0-4   -  M  M  M  M  -
14 0-4   N  N  -  N  -  -
15 0-4   O  O  -  -  -  -
16 0-4   -  -  P  P  -  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   R  R  R  -  -   
19 0-6   -  S  -  -  S   
20 1-4   T  -  -  T      
21 1-4   U  U  U  -      
22 1-4   V  -  -         
23 1-4   W  X  X         
24 1-5   X  -            
25 2-3   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RPQOX QTUPU ELQNU SGFAL FPYHW X
-------------------------------
