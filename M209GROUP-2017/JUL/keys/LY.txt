EFFECTIVE PERIOD:
11-JUL-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 2-0   B  B  -  -  B  B
03 0-3   -  C  C  C  -  -
04 0-3   D  D  -  -  -  -
05 0-3   E  E  E  -  E  -
06 0-4   F  -  F  F  F  -
07 0-4   -  -  G  G  G  G
08 0-4   H  -  -  H  -  H
09 0-4   -  I  -  I  -  -
10 0-4   J  -  -  J  J  J
11 0-4   -  -  K  -  -  -
12 0-5   L  -  L  -  L  -
13 0-5   M  -  M  -  -  M
14 0-5   N  -  N  -  -  N
15 0-5   O  -  O  -  O  -
16 0-5   -  P  -  -  -  P
17 0-5   -  -  -  Q  Q  Q
18 0-5   R  R  -  R  R   
19 0-5   -  S  S  -  S   
20 0-5   T  -  T  T      
21 0-5   -  -  -  U      
22 0-6   -  V  -         
23 0-6   W  X  X         
24 0-6   X  -            
25 0-6   -  -            
26 2-3   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

OFFQI YSSXU AFQQA MPPGQ JETJP V
-------------------------------
