EFFECTIVE PERIOD:
18-FEB-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   -  B  B  -  -  -
03 2-0   C  C  C  -  C  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  -  -  E  -
06 2-0   F  F  -  F  F  F
07 2-0   -  -  -  G  G  G
08 2-0   -  H  H  H  -  -
09 2-0   -  I  -  -  I  I
10 0-3   J  J  -  J  J  -
11 0-3   -  K  -  K  K  K
12 0-3   L  L  L  L  L  L
13 0-3   -  -  M  -  -  -
14 0-3   -  N  N  -  -  -
15 0-3   -  O  O  -  -  -
16 0-3   P  P  -  P  P  -
17 0-3   -  -  -  -  Q  -
18 0-3   -  R  -  R  -   
19 0-3   S  -  S  -  -   
20 0-3   -  T  T  T      
21 0-3   U  U  U  U      
22 0-3   V  -  V         
23 0-5   W  -  -         
24 0-5   -  -            
25 0-5   Y  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AWZJD AVPRC DUZNE CNRMM JUVQZ H
-------------------------------
