EFFECTIVE PERIOD:
16-DEC-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   C  -  C  C  -  -
04 1-0   -  D  -  -  D  -
05 2-0   -  -  -  -  E  -
06 2-0   -  F  F  F  -  F
07 2-0   G  G  G  -  -  -
08 2-0   -  -  -  H  H  -
09 2-0   -  I  I  I  I  I
10 0-3   J  -  -  J  -  J
11 0-4   K  -  K  -  -  -
12 0-5   -  -  -  -  -  L
13 0-6   -  M  M  -  M  M
14 0-6   N  N  N  N  -  N
15 0-6   O  O  O  -  O  O
16 0-6   P  -  -  P  -  P
17 0-6   Q  Q  Q  -  -  -
18 0-6   -  R  -  R  R   
19 0-6   S  -  -  S  S   
20 0-6   T  T  -  -      
21 0-6   -  -  U  -      
22 1-2   V  -  V         
23 1-5   -  -  -         
24 2-6   X  Y            
25 2-6   Y  Z            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

PVRAR SIPIO KNZFJ PWOOR URQKZ P
-------------------------------
