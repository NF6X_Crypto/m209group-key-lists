EFFECTIVE PERIOD:
27-DEC-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 2-0   B  -  B  -  B  B
03 2-0   -  -  -  C  -  -
04 2-0   D  D  D  D  D  -
05 0-3   -  E  -  E  -  -
06 0-3   F  -  -  -  -  -
07 0-3   -  -  G  -  -  G
08 0-3   -  H  -  H  H  -
09 0-4   I  -  I  -  I  -
10 0-5   -  -  -  J  J  J
11 0-5   -  K  K  -  -  -
12 0-5   L  -  -  -  -  -
13 0-5   M  M  M  -  -  M
14 0-5   -  N  -  N  -  N
15 0-5   -  -  -  O  O  -
16 0-5   P  P  P  -  P  -
17 0-6   Q  Q  -  Q  Q  Q
18 0-6   -  -  -  -  R   
19 0-6   S  -  S  -  -   
20 0-6   -  -  -  -      
21 0-6   -  U  U  U      
22 1-2   -  V  V         
23 2-6   W  X  X         
24 3-5   X  -            
25 3-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

BGSSK OUSAP EVCOF OXJNI NERWU V
-------------------------------
