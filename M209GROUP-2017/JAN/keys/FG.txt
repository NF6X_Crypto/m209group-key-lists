EFFECTIVE PERIOD:
18-JAN-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   -  -  B  -  B  -
03 1-0   -  C  -  C  -  -
04 0-3   D  D  D  -  D  D
05 0-3   E  E  -  -  -  E
06 0-3   -  F  F  F  F  F
07 0-3   G  -  G  G  -  G
08 0-4   -  H  -  H  H  -
09 0-4   I  -  -  -  I  I
10 0-4   -  -  -  -  -  -
11 0-4   -  -  K  K  K  K
12 0-4   -  L  L  -  L  L
13 0-4   M  -  -  M  M  M
14 0-4   N  -  -  -  -  -
15 0-5   O  -  -  O  -  O
16 0-5   P  -  P  -  -  -
17 0-5   -  -  -  -  Q  Q
18 0-5   R  R  R  R  -   
19 0-5   S  S  -  -  -   
20 1-4   T  -  -  -      
21 1-4   U  U  U  -      
22 1-4   -  V  -         
23 1-4   -  -  X         
24 1-5   -  Y            
25 2-4   Y  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

WHVIO VBGGA AXAMP AXIIW LRAQN N
-------------------------------
