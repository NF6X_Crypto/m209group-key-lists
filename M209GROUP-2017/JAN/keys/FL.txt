EFFECTIVE PERIOD:
23-JAN-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  B  -  -  B
03 1-0   -  C  C  C  C  -
04 1-0   -  -  -  D  D  D
05 2-0   E  -  E  E  -  E
06 2-0   F  F  -  F  -  -
07 2-0   -  G  -  -  -  G
08 2-0   -  -  -  -  -  H
09 2-0   -  -  -  -  I  -
10 0-3   -  J  J  -  J  J
11 0-6   -  -  -  K  -  -
12 0-6   -  -  -  -  L  L
13 0-6   M  M  -  -  M  M
14 0-6   -  -  -  -  N  N
15 0-6   -  -  O  O  O  -
16 1-3   P  -  P  -  -  -
17 1-3   -  Q  Q  -  -  -
18 1-3   R  R  R  R  R   
19 1-4   S  S  -  S  -   
20 1-5   T  -  T  T      
21 1-6   U  -  U  U      
22 1-6   -  V  -         
23 1-6   -  X  X         
24 1-6   -  -            
25 2-3   Y  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

MQAAV NHMSP QAPRQ MYHJR QAAUF Q
-------------------------------
