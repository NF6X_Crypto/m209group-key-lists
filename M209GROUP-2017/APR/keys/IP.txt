EFFECTIVE PERIOD:
15-APR-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 2-0   B  -  -  -  B  -
03 0-3   C  -  -  -  C  -
04 0-3   D  D  D  -  -  -
05 0-3   -  E  -  -  -  E
06 0-3   -  -  -  F  F  F
07 0-3   G  G  G  G  -  -
08 0-3   H  -  -  -  H  H
09 0-3   -  I  I  -  I  I
10 0-3   -  J  -  J  -  J
11 0-3   K  -  -  K  -  K
12 0-3   L  L  L  -  -  -
13 0-4   -  M  -  M  M  M
14 0-4   -  N  N  -  -  -
15 0-4   -  O  O  O  -  O
16 0-4   P  P  P  P  P  P
17 0-5   -  Q  Q  Q  -  -
18 0-5   R  -  R  R  R   
19 0-5   S  S  -  S  -   
20 0-5   T  -  T  T      
21 0-6   -  -  -  -      
22 1-2   V  -  V         
23 2-4   W  X  -         
24 3-5   X  Y            
25 3-5   Y  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

WUWUA GYIQL HDCTJ PSEUI KVEYV Y
-------------------------------
