EFFECTIVE PERIOD:
02-APR-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  A  -  A
02 0-3   B  -  B  -  B  B
03 0-3   C  -  C  -  -  C
04 0-4   -  D  D  D  -  D
05 0-4   -  E  -  -  -  -
06 0-4   -  F  F  F  -  F
07 0-4   -  G  -  -  -  G
08 0-4   -  -  H  -  H  H
09 0-4   I  -  -  I  I  I
10 0-5   -  -  J  -  J  -
11 0-6   K  -  -  K  -  K
12 0-6   L  -  L  -  -  L
13 0-6   -  M  M  M  M  -
14 0-6   -  -  -  -  -  -
15 0-6   O  O  -  -  O  -
16 1-3   P  -  -  P  P  -
17 2-5   -  Q  Q  -  -  -
18 3-5   R  -  -  -  R   
19 3-5   -  -  -  -  -   
20 3-5   -  T  T  T      
21 3-5   -  U  -  U      
22 3-6   V  -  -         
23 3-6   -  X  -         
24 3-6   -  -            
25 3-6   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SQBVG AOQRX KMYRR UGAJT GTOGQ R
-------------------------------
