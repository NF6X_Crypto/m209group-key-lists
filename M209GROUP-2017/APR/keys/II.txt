EFFECTIVE PERIOD:
08-APR-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: II
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  -  B  -  B
03 1-0   -  C  -  -  -  C
04 2-0   D  -  D  -  D  D
05 2-0   E  -  -  -  -  -
06 2-0   -  F  F  F  -  F
07 2-0   G  -  G  -  G  -
08 0-3   -  H  H  H  H  -
09 0-3   I  -  -  I  I  I
10 0-3   J  -  -  J  -  -
11 0-3   K  K  -  K  K  K
12 0-3   -  L  L  -  -  -
13 0-3   -  -  M  M  -  M
14 0-3   N  N  -  -  N  -
15 0-3   -  O  O  O  -  O
16 0-3   P  -  P  -  -  -
17 0-3   -  Q  Q  -  Q  -
18 0-4   R  -  R  R  -   
19 0-5   -  -  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  -  -  U      
22 1-2   -  V  -         
23 1-6   W  X  X         
24 2-3   X  -            
25 2-3   Y  Z            
26 2-3   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PMAMQ UZOQV WBLRH VUVKI EARAZ V
-------------------------------
