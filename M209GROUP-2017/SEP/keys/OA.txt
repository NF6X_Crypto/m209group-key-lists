EFFECTIVE PERIOD:
03-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  -  -  B  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  D  D  D  -  -
05 1-0   E  E  E  E  E  E
06 1-0   F  -  -  F  F  -
07 1-0   -  -  -  -  -  -
08 0-4   -  H  H  -  H  -
09 0-4   -  I  I  I  -  I
10 0-4   J  -  J  -  -  J
11 0-6   K  -  K  -  -  -
12 0-6   -  -  L  L  L  L
13 0-6   M  -  -  M  -  M
14 0-6   -  N  -  N  N  N
15 0-6   -  O  O  -  -  -
16 1-4   -  P  P  P  -  P
17 1-6   -  Q  -  Q  Q  -
18 1-6   R  -  R  -  -   
19 1-6   S  -  -  S  S   
20 1-6   -  T  T  -      
21 2-3   U  U  U  -      
22 2-4   V  -  -         
23 2-6   W  X  -         
24 2-6   X  -            
25 3-5   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

JTXYI XVWQL GUIVK XUIPP ATRLT K
-------------------------------
