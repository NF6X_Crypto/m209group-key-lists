EFFECTIVE PERIOD:
13-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   -  -  B  -  B  B
03 2-0   -  C  C  C  -  -
04 2-0   D  -  D  D  -  -
05 2-0   -  E  -  E  E  E
06 2-0   -  -  F  -  -  -
07 2-0   -  G  G  -  G  G
08 0-5   -  H  -  -  -  -
09 0-5   -  -  I  I  -  -
10 0-5   J  J  -  -  J  -
11 0-5   K  K  K  K  -  K
12 0-6   L  -  -  -  -  -
13 0-6   -  -  M  -  M  M
14 0-6   N  N  N  -  -  N
15 0-6   -  -  -  O  O  O
16 0-6   -  P  P  -  P  P
17 0-6   Q  Q  -  Q  -  Q
18 1-2   -  R  -  -  R   
19 1-6   -  S  -  -  S   
20 1-6   -  T  -  T      
21 1-6   U  U  U  U      
22 1-6   V  -  -         
23 2-5   W  -  -         
24 3-4   -  -            
25 4-5   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MRUXY UZPSF NQIUM HAYHN AUVSM S
-------------------------------
