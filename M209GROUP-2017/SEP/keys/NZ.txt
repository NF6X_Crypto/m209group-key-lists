EFFECTIVE PERIOD:
02-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  B  -  B  B
03 1-0   -  -  C  C  -  -
04 1-0   D  -  D  D  -  -
05 2-0   -  E  E  -  E  E
06 2-0   -  -  -  -  -  F
07 2-0   -  -  -  G  -  -
08 2-0   H  -  H  H  -  H
09 2-0   I  I  -  I  I  -
10 2-0   -  -  -  -  J  J
11 0-3   K  K  -  -  -  K
12 0-3   L  L  -  -  L  -
13 0-3   -  M  M  M  -  M
14 0-3   N  N  -  N  -  -
15 0-6   -  O  O  -  -  -
16 1-3   P  -  -  P  -  P
17 1-3   -  Q  Q  Q  Q  -
18 1-3   R  R  R  -  -   
19 1-3   S  -  -  S  S   
20 1-5   T  T  T  T      
21 1-5   -  -  -  -      
22 1-6   V  V  V         
23 1-6   -  X  -         
24 2-3   -  Y            
25 2-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WJVYR ORLWV LOWIT IHQLW ZUAHZ M
-------------------------------
