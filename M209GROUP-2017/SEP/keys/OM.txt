EFFECTIVE PERIOD:
15-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   B  B  B  B  -  B
03 1-0   C  -  -  C  -  C
04 1-0   -  D  D  -  D  D
05 1-0   E  -  -  E  -  E
06 1-0   -  -  F  F  F  F
07 1-0   -  -  G  -  G  G
08 1-0   -  H  H  -  -  -
09 1-0   I  I  I  I  I  I
10 0-3   -  J  -  J  -  J
11 0-3   -  -  K  -  -  -
12 0-3   L  -  -  L  L  -
13 0-3   M  M  -  -  M  M
14 0-3   N  N  N  N  N  -
15 0-3   O  O  -  -  O  O
16 0-4   -  -  P  P  -  -
17 0-5   Q  -  -  -  -  -
18 0-5   -  R  -  -  -   
19 0-5   S  S  -  -  S   
20 0-5   T  -  -  T      
21 0-5   U  -  U  U      
22 0-5   V  -  -         
23 0-6   -  X  X         
24 0-6   -  Y            
25 0-6   -  Z            
26 2-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MRPRN IIOLK FANAI SPIIT OJRPL T
-------------------------------
