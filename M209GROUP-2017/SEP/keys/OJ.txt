EFFECTIVE PERIOD:
12-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   -  B  B  -  B  B
03 1-0   -  -  -  C  C  -
04 1-0   D  D  D  -  -  -
05 1-0   -  E  E  E  E  E
06 2-0   F  -  -  F  F  F
07 2-0   G  G  G  G  G  -
08 2-0   H  -  H  -  -  H
09 2-0   -  I  I  -  I  I
10 2-0   J  -  -  J  J  -
11 2-0   K  K  -  -  -  K
12 2-0   -  L  L  L  -  -
13 0-3   -  -  -  M  M  M
14 0-4   -  -  -  -  -  N
15 0-4   -  O  -  -  O  -
16 0-4   P  P  P  -  -  P
17 0-4   Q  -  Q  -  Q  -
18 0-5   R  R  -  -  R   
19 0-6   S  S  -  S  -   
20 0-6   -  -  T  T      
21 0-6   -  U  U  -      
22 0-6   V  V  -         
23 0-6   -  X  -         
24 1-2   -  Y            
25 1-6   Y  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

YOOWH HVOOU ASPHA TJWNO SUPZA R
-------------------------------
