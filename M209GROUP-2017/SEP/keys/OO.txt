EFFECTIVE PERIOD:
17-SEP-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  A  A
02 2-0   -  B  -  -  B  -
03 2-0   -  C  -  C  -  -
04 2-0   -  -  -  -  D  -
05 2-0   E  -  E  -  -  -
06 2-0   F  -  F  F  F  -
07 0-3   G  G  -  G  -  G
08 0-4   H  -  H  H  H  H
09 0-4   -  -  -  -  I  -
10 0-4   -  J  -  J  -  -
11 0-4   K  -  K  K  -  -
12 0-5   L  L  -  -  -  -
13 0-5   M  -  M  M  -  M
14 0-5   -  -  N  -  N  N
15 0-5   O  O  O  O  O  O
16 0-6   -  -  -  P  -  -
17 0-6   -  Q  -  -  Q  Q
18 1-3   R  -  -  R  R   
19 2-3   -  -  S  -  S   
20 2-3   T  T  T  T      
21 2-6   -  U  U  U      
22 2-6   V  -  -         
23 2-6   W  X  -         
24 2-6   X  Y            
25 3-4   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TEZQH YWUXS LLLTQ OLOGM RRUQU S
-------------------------------
