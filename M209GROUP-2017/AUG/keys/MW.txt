EFFECTIVE PERIOD:
04-AUG-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  -
02 1-0   B  -  B  -  B  -
03 1-0   C  C  C  -  C  -
04 0-3   D  -  -  D  -  -
05 0-3   -  -  E  E  E  -
06 0-3   -  F  -  -  F  F
07 0-3   G  -  -  G  -  G
08 0-3   H  H  H  -  H  -
09 0-3   -  -  I  -  -  I
10 0-3   -  J  J  J  -  -
11 0-3   -  K  -  -  -  K
12 0-3   L  -  -  L  L  L
13 0-3   -  M  -  M  M  M
14 0-3   N  -  -  -  N  N
15 0-4   -  O  -  -  O  O
16 0-5   P  P  -  -  P  P
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  R  R  -   
19 0-6   S  S  -  S  -   
20 0-6   T  T  T  T      
21 0-6   -  -  U  U      
22 0-6   -  V  V         
23 0-6   W  -  -         
24 1-2   X  Y            
25 1-6   -  Z            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PLLZA MMSRD OLAPQ LBXZV MMOCG C
-------------------------------
