EFFECTIVE PERIOD:
25-AUG-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 2-0   -  -  B  B  B  -
03 2-0   C  C  -  -  -  C
04 2-0   -  -  -  -  -  D
05 2-0   E  E  E  -  -  -
06 0-3   -  F  -  F  F  -
07 0-3   G  -  -  -  -  -
08 0-5   H  -  -  H  -  -
09 0-5   I  -  I  I  -  I
10 0-5   J  J  J  -  J  -
11 0-5   K  K  K  -  K  K
12 0-5   L  -  -  -  L  -
13 0-5   -  -  M  M  M  M
14 0-6   N  -  -  N  N  N
15 0-6   -  O  -  O  O  O
16 0-6   P  P  -  P  -  -
17 0-6   -  Q  -  Q  Q  -
18 0-6   -  -  R  -  R   
19 0-6   S  S  -  -  -   
20 0-6   T  -  T  T      
21 0-6   -  U  U  -      
22 0-6   -  -  V         
23 0-6   W  X  X         
24 1-3   -  Y            
25 1-4   Y  Z            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

YMWTU GOPIA QFZOE SGTMW AYMAT M
-------------------------------
