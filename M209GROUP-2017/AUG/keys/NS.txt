EFFECTIVE PERIOD:
26-AUG-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  -  -
02 2-0   -  B  B  B  -  B
03 2-0   C  -  C  C  C  -
04 2-0   -  D  D  -  D  -
05 0-5   E  -  E  E  -  E
06 0-5   F  F  -  F  -  -
07 0-5   G  G  -  -  G  G
08 0-5   -  H  -  -  -  H
09 0-5   I  -  I  -  -  I
10 0-5   -  -  J  J  -  -
11 0-5   -  -  -  -  -  K
12 0-6   -  L  -  -  -  -
13 0-6   M  -  -  -  M  -
14 0-6   -  N  -  N  -  N
15 0-6   -  -  O  O  O  O
16 0-6   P  -  P  P  P  -
17 0-6   Q  -  -  -  -  Q
18 0-6   R  -  R  R  R   
19 0-6   S  -  S  -  S   
20 1-3   -  -  T  -      
21 2-3   U  U  -  U      
22 2-5   V  V  -         
23 4-6   -  X  -         
24 5-6   X  -            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NOCVW CAQTZ OWGOA NGTHB UMRVH W
-------------------------------
