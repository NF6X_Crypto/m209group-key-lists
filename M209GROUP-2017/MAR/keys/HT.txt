EFFECTIVE PERIOD:
24-MAR-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  B  B  -  -
03 1-0   C  -  C  -  C  -
04 0-4   D  -  D  -  -  -
05 0-4   E  -  -  -  -  E
06 0-5   -  F  -  F  F  F
07 0-5   G  G  G  G  G  -
08 0-5   H  -  H  -  -  H
09 0-5   -  I  I  -  I  I
10 0-5   -  -  J  -  -  J
11 0-5   K  K  -  -  K  K
12 0-6   -  L  L  L  -  L
13 0-6   M  -  M  -  M  -
14 0-6   -  -  N  N  -  -
15 0-6   -  O  -  O  O  -
16 1-3   -  P  -  -  -  P
17 1-4   Q  -  Q  Q  -  -
18 1-4   R  R  -  R  R   
19 1-4   -  S  -  -  -   
20 1-5   -  -  -  -      
21 2-4   U  -  -  -      
22 3-4   -  -  -         
23 4-6   -  X  X         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LAPKK UHIIP WLRPA USSXC SJXPI S
-------------------------------
