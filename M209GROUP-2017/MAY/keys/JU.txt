EFFECTIVE PERIOD:
16-MAY-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  -  -  -  B  B
03 1-0   C  C  -  C  -  -
04 2-0   D  D  D  -  D  -
05 2-0   -  -  -  -  E  E
06 2-0   F  -  F  F  -  F
07 2-0   -  -  G  G  G  G
08 2-0   H  -  -  H  H  H
09 0-3   -  I  -  I  -  I
10 0-4   -  J  -  -  -  -
11 0-4   K  K  K  -  K  -
12 0-4   -  -  L  L  -  -
13 0-4   -  M  M  -  -  M
14 0-4   -  N  N  -  N  -
15 0-4   O  O  -  O  O  O
16 0-4   -  P  P  -  -  -
17 0-6   Q  -  Q  Q  -  -
18 0-6   R  R  R  -  -   
19 0-6   -  S  S  -  -   
20 1-2   -  -  -  T      
21 1-3   -  -  U  U      
22 2-6   V  V  -         
23 4-5   -  X  X         
24 4-6   X  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IKVJX BEXZX ZVISZ RRLFN UTCUN A
-------------------------------
