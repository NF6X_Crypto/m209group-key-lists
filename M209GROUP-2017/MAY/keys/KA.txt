EFFECTIVE PERIOD:
22-MAY-2017 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 2-0   B  -  -  -  B  B
03 2-0   C  C  C  -  -  -
04 2-0   D  -  -  D  D  D
05 2-0   E  E  E  -  E  E
06 2-0   -  -  -  F  F  -
07 2-0   -  -  -  G  -  G
08 0-3   -  -  H  H  H  H
09 0-3   -  I  -  I  -  -
10 0-3   -  J  -  -  -  -
11 0-3   -  K  -  K  K  K
12 0-3   L  L  L  L  L  -
13 0-5   M  M  M  -  M  M
14 0-5   -  N  N  -  -  -
15 0-5   O  -  O  -  -  -
16 1-3   -  -  -  P  P  P
17 1-3   Q  Q  Q  Q  Q  Q
18 1-4   R  -  R  -  -   
19 1-6   S  -  S  -  -   
20 2-3   T  -  T  -      
21 2-3   -  -  -  -      
22 2-3   -  V  -         
23 2-3   -  X  X         
24 2-5   X  -            
25 3-4   Y  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

STPPT SNMPT MITTP VDPET SUWAA G
-------------------------------
