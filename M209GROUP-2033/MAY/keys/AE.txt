EFFECTIVE PERIOD:
06-MAY-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   B  -  -  B  B  -
03 1-0   C  C  C  -  -  C
04 0-3   D  -  -  -  -  -
05 0-3   -  -  E  -  E  E
06 0-3   F  F  F  F  -  F
07 0-3   -  -  G  G  G  -
08 0-3   H  H  -  H  H  H
09 0-3   -  I  I  -  -  I
10 0-3   -  -  J  -  J  -
11 0-3   K  K  -  K  K  K
12 0-3   -  -  L  -  -  -
13 0-3   M  -  M  -  -  -
14 0-4   N  -  N  -  N  -
15 0-4   -  O  -  O  O  O
16 0-4   -  P  -  P  -  P
17 0-4   Q  Q  Q  Q  Q  -
18 0-4   -  -  R  -  -   
19 0-5   -  S  -  S  -   
20 0-5   -  T  -  -      
21 0-5   U  U  -  U      
22 0-5   -  V  -         
23 0-6   W  -  X         
24 1-5   -  Y            
25 1-6   Y  Z            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MFLUS LGFFA GGVFA XVIUQ ZVSUO G
-------------------------------
