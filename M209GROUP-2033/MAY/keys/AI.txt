EFFECTIVE PERIOD:
10-MAY-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   B  B  B  B  -  B
03 2-0   C  -  -  -  C  -
04 2-0   -  D  -  D  -  -
05 2-0   E  -  -  -  E  E
06 2-0   F  F  -  -  -  F
07 2-0   -  G  G  G  G  -
08 2-0   H  -  H  H  -  -
09 2-0   I  -  -  -  I  -
10 2-0   -  -  -  J  J  J
11 0-4   -  K  -  K  -  K
12 0-4   L  L  -  L  L  L
13 0-4   M  -  M  -  M  -
14 0-4   N  -  N  -  -  N
15 0-5   O  -  -  -  -  -
16 0-5   -  P  -  P  -  -
17 0-5   Q  -  Q  Q  Q  Q
18 0-5   R  R  R  -  R   
19 0-5   S  S  S  -  S   
20 0-6   -  -  T  -      
21 0-6   -  -  -  -      
22 2-5   V  V  V         
23 2-5   W  X  X         
24 2-5   -  -            
25 3-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MWAVS JNSVA WDAAR GAVJO QLNEO R
-------------------------------
