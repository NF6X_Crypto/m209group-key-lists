EFFECTIVE PERIOD:
25-JUL-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 2-0   -  -  -  -  B  -
03 2-0   -  C  C  -  -  C
04 2-0   D  -  D  D  -  D
05 2-0   E  -  -  E  E  -
06 2-0   -  -  F  -  F  F
07 2-0   -  G  -  -  -  -
08 0-3   -  H  -  -  H  H
09 0-3   I  -  -  -  I  I
10 0-4   -  J  J  J  J  -
11 0-4   K  K  K  -  -  K
12 0-4   -  L  L  L  L  L
13 0-4   M  -  -  M  M  -
14 0-5   -  -  -  -  -  -
15 0-6   O  O  -  O  O  -
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  -  Q  -  Q
18 0-6   -  R  -  -  -   
19 0-6   -  -  -  S  -   
20 1-6   -  -  -  -      
21 2-3   U  -  U  U      
22 2-4   V  V  -         
23 3-5   W  -  X         
24 4-6   -  Y            
25 4-6   Y  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

YUTSK AOMIW NJTUJ OBUWF UUKQI U
-------------------------------
