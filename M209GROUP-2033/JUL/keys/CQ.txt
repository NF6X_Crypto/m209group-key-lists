EFFECTIVE PERIOD:
09-JUL-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  -  B  B  B  -
03 1-0   C  -  -  -  C  -
04 2-0   D  D  -  -  D  D
05 2-0   E  E  E  E  -  -
06 2-0   -  -  F  -  -  -
07 2-0   -  -  G  G  -  -
08 0-4   H  H  -  -  H  H
09 0-4   -  -  I  -  -  -
10 0-4   J  J  -  J  J  J
11 0-4   -  K  -  -  -  -
12 0-4   L  L  L  L  -  L
13 0-5   M  -  M  -  M  -
14 0-5   -  N  -  N  -  N
15 0-5   O  O  O  O  -  O
16 1-4   P  P  P  -  -  -
17 1-4   Q  -  Q  Q  -  Q
18 1-4   -  R  -  -  -   
19 1-4   -  -  -  S  S   
20 1-5   T  -  T  -      
21 1-6   U  -  -  U      
22 1-6   -  -  -         
23 1-6   -  X  -         
24 2-4   -  Y            
25 2-5   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QAKKS KOFTT AVKQS AEFXZ AKWSG W
-------------------------------
