EFFECTIVE PERIOD:
13-JUL-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  -  -  -
03 1-0   -  -  -  C  C  C
04 1-0   -  -  -  D  D  D
05 0-3   E  E  E  -  E  -
06 0-3   F  F  -  -  F  -
07 0-3   G  G  G  -  -  G
08 0-4   H  -  -  H  -  H
09 0-4   -  -  -  -  I  -
10 0-4   -  J  J  J  -  -
11 0-4   -  K  -  -  K  K
12 0-4   -  L  L  L  L  -
13 0-4   -  -  M  -  -  M
14 0-4   N  -  N  -  N  N
15 0-4   O  -  -  -  -  O
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  Q  -  -  -
18 0-5   R  -  -  R  R   
19 0-6   S  S  -  S  -   
20 1-3   -  T  -  T      
21 1-5   U  -  U  -      
22 2-6   V  V  V         
23 3-6   W  X  -         
24 4-5   -  -            
25 4-5   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

OGSLC SUWNO SVQKG GZIUO OOFEF Q
-------------------------------
