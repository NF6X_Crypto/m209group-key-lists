EFFECTIVE PERIOD:
27-JUL-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  -  -  B  -  -
03 1-0   -  -  -  C  -  C
04 1-0   D  D  -  -  -  D
05 1-0   E  -  -  E  E  -
06 1-0   F  -  F  F  -  -
07 0-3   G  -  G  G  G  -
08 0-3   H  -  -  -  -  -
09 0-4   I  -  -  -  I  I
10 0-5   -  J  J  -  J  J
11 0-5   -  -  K  -  -  -
12 0-5   L  L  L  L  L  L
13 0-5   M  M  -  M  -  M
14 0-5   -  -  -  -  N  -
15 0-5   -  O  O  -  O  O
16 0-6   -  P  P  -  -  P
17 0-6   -  -  -  -  -  -
18 0-6   -  R  R  R  R   
19 0-6   -  S  S  S  S   
20 1-3   T  -  -  T      
21 1-6   U  U  U  -      
22 2-5   -  V  V         
23 3-4   -  X  X         
24 5-6   -  -            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MNTKT TMUOM LJKBU TZSPZ FUWIM O
-------------------------------
