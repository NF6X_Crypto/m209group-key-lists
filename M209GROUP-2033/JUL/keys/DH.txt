EFFECTIVE PERIOD:
26-JUL-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  B  B  B  B  -
03 1-0   C  C  -  -  C  C
04 1-0   -  D  D  -  -  D
05 2-0   E  -  -  E  -  -
06 2-0   -  F  -  -  -  -
07 2-0   -  G  -  -  -  G
08 2-0   -  -  -  H  H  H
09 0-3   I  I  I  -  I  -
10 0-3   -  -  J  J  J  J
11 0-3   -  -  K  K  K  -
12 0-4   L  -  -  -  L  L
13 0-5   M  M  M  -  -  M
14 0-5   N  N  N  N  -  -
15 0-5   O  -  -  -  O  O
16 0-5   -  P  -  P  P  P
17 0-5   -  Q  Q  -  -  -
18 0-5   R  -  R  -  -   
19 0-5   -  -  S  S  -   
20 0-5   -  -  T  -      
21 0-6   U  U  -  U      
22 1-2   -  V  V         
23 1-3   -  -  X         
24 2-4   X  -            
25 3-5   -  -            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

OHLQP MNFNP TGFSG QSQRA GAWYL F
-------------------------------
