EFFECTIVE PERIOD:
10-FEB-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  -
02 2-0   B  B  -  B  B  B
03 2-0   -  C  C  C  C  -
04 2-0   D  D  D  D  -  -
05 0-3   -  -  -  -  E  E
06 0-4   F  F  -  -  -  -
07 0-4   -  G  -  G  -  -
08 0-4   H  H  -  H  -  -
09 0-4   I  I  I  I  I  I
10 0-5   -  -  J  J  -  J
11 0-5   -  -  -  -  -  K
12 0-5   L  L  -  -  L  L
13 0-5   M  -  M  M  M  M
14 0-5   N  N  -  -  N  -
15 0-5   -  -  -  -  -  O
16 0-5   P  P  -  P  P  P
17 0-5   -  Q  Q  Q  -  Q
18 0-5   -  R  -  -  R   
19 0-5   -  S  S  S  -   
20 0-6   -  -  T  -      
21 0-6   -  U  U  U      
22 1-3   -  -  V         
23 2-4   W  -  -         
24 2-6   -  Y            
25 3-6   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VUUOY FACWR TLYIG KZOQW LUKQM U
-------------------------------
