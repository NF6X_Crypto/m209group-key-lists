EFFECTIVE PERIOD:
28-FEB-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   -  B  -  B  B  B
03 0-3   C  -  C  C  -  -
04 0-3   D  D  D  -  D  -
05 0-3   -  -  -  -  -  E
06 0-3   F  -  F  F  F  F
07 0-3   -  G  G  -  G  G
08 0-3   H  H  H  -  H  -
09 0-4   I  I  -  -  -  I
10 0-4   -  J  -  -  J  -
11 0-4   K  -  K  -  -  K
12 0-5   L  L  L  L  L  L
13 0-5   -  -  M  M  M  -
14 0-5   N  -  -  -  -  -
15 0-5   -  -  -  O  O  O
16 1-2   P  P  -  P  P  P
17 1-4   Q  Q  -  Q  -  -
18 2-3   R  R  -  -  -   
19 2-4   -  S  -  S  -   
20 2-4   T  -  T  T      
21 2-4   U  U  U  U      
22 3-5   -  -  V         
23 4-5   -  X  -         
24 4-5   -  -            
25 4-5   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RRPUU CWTAQ PUOSU QJWIQ AUXQM L
-------------------------------
