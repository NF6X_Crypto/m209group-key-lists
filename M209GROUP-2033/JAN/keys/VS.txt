EFFECTIVE PERIOD:
10-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 2-0   B  -  B  -  B  -
03 0-3   -  -  -  C  C  C
04 0-3   -  D  D  D  -  D
05 0-3   E  -  -  -  -  -
06 0-3   F  -  F  F  -  F
07 0-3   -  G  G  -  G  -
08 0-3   -  -  -  -  -  -
09 0-3   I  -  I  I  I  -
10 0-5   -  -  J  -  -  J
11 0-5   K  -  -  -  -  K
12 0-5   L  L  L  L  L  L
13 0-5   M  M  M  M  M  -
14 0-5   -  N  -  N  -  N
15 0-5   -  -  O  O  O  -
16 0-5   -  P  P  P  -  P
17 0-5   -  Q  -  -  -  -
18 0-5   R  -  -  -  -   
19 0-5   S  -  S  -  S   
20 0-5   -  -  T  -      
21 0-5   U  U  -  U      
22 0-6   -  -  -         
23 0-6   -  X  -         
24 1-2   -  -            
25 1-6   Y  Z            
26 2-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

QZMSZ MKLOS ZIGHO KYGKW QLAEK H
-------------------------------
