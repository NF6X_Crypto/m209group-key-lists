EFFECTIVE PERIOD:
04-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 0-3   B  B  -  B  -  -
03 0-4   -  -  C  C  -  -
04 0-4   D  -  D  -  -  D
05 0-4   -  E  E  -  E  E
06 0-4   F  F  -  F  F  -
07 0-5   -  G  -  G  G  G
08 0-5   H  -  H  -  -  -
09 0-5   -  -  I  I  I  -
10 0-5   -  -  J  -  -  J
11 0-5   -  K  -  -  -  K
12 0-5   L  -  -  L  L  L
13 0-6   -  -  M  M  M  M
14 0-6   -  -  -  -  N  -
15 0-6   O  O  O  -  O  -
16 0-6   -  -  -  -  -  P
17 0-6   Q  Q  -  Q  -  Q
18 0-6   R  -  R  -  -   
19 0-6   -  S  -  S  S   
20 1-6   T  -  -  T      
21 2-3   U  -  U  -      
22 3-4   V  V  -         
23 4-5   -  X  -         
24 5-6   -  -            
25 5-6   Y  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZVUKJ LRABG RRPVS HUUOV PRVLO I
-------------------------------
