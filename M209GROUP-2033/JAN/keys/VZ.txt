EFFECTIVE PERIOD:
17-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 2-0   -  B  -  B  -  -
03 2-0   C  -  C  C  -  -
04 2-0   D  -  -  -  D  -
05 2-0   E  -  E  E  E  E
06 2-0   -  F  F  -  F  F
07 2-0   G  -  G  G  -  -
08 0-3   -  -  -  -  H  -
09 0-4   -  -  -  -  -  I
10 0-4   J  -  -  J  J  J
11 0-4   -  -  -  K  K  -
12 0-4   -  L  L  L  L  -
13 0-4   M  -  M  -  -  -
14 0-4   -  N  N  -  N  N
15 0-4   O  O  O  O  -  O
16 0-5   P  P  -  P  -  P
17 0-5   -  -  Q  Q  Q  Q
18 0-5   R  R  -  R  R   
19 0-5   -  -  S  -  -   
20 0-5   T  T  T  -      
21 0-6   U  U  U  -      
22 2-4   -  -  -         
23 2-4   W  -  -         
24 2-4   X  Y            
25 2-5   -  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

SMRUM ASDAJ NZSRD UNHQQ MJNOY X
-------------------------------
