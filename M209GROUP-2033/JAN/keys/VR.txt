EFFECTIVE PERIOD:
09-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  -  C  C
04 1-0   D  D  D  -  -  -
05 1-0   -  -  -  -  E  -
06 0-3   -  -  -  F  -  F
07 0-3   G  G  -  -  -  G
08 0-3   -  -  H  -  H  -
09 0-3   -  I  -  I  I  I
10 0-6   -  J  J  J  J  -
11 0-6   K  -  -  K  -  -
12 0-6   -  -  L  L  L  -
13 0-6   -  -  -  -  -  -
14 0-6   -  N  N  N  -  -
15 0-6   O  O  O  O  -  O
16 1-3   -  -  P  -  P  -
17 1-3   -  Q  -  -  -  -
18 1-3   -  -  -  -  -   
19 1-3   S  -  S  S  S   
20 1-6   -  -  T  -      
21 2-5   -  U  U  -      
22 3-4   V  V  -         
23 3-4   W  -  -         
24 3-4   X  Y            
25 3-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

NNOJK LUATN QSTUP KCUEO FYEOU Z
-------------------------------
