EFFECTIVE PERIOD:
11-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  A
02 0-3   -  B  -  -  -  -
03 0-3   C  C  -  -  -  C
04 0-3   D  D  D  D  -  D
05 0-3   E  -  -  -  -  E
06 0-3   -  -  F  -  F  F
07 0-3   -  G  G  G  G  G
08 0-4   H  H  -  -  -  -
09 0-4   -  -  I  I  I  I
10 0-4   J  J  J  -  -  J
11 0-4   K  K  -  K  K  -
12 0-4   L  -  -  -  -  -
13 0-4   -  M  M  M  -  M
14 0-5   -  -  -  -  -  -
15 0-5   O  -  -  O  O  -
16 0-5   P  P  P  P  P  P
17 0-5   Q  -  Q  Q  Q  -
18 0-5   R  R  -  -  R   
19 0-6   -  -  S  S  S   
20 1-3   -  T  -  -      
21 2-6   -  -  -  -      
22 3-5   V  V  -         
23 3-5   -  X  -         
24 3-5   -  Y            
25 3-5   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ICUZQ URNLK NDKUM UTJSU CKTSS X
-------------------------------
