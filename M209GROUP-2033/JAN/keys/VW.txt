EFFECTIVE PERIOD:
14-JAN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  B  B  B  B  -
03 1-0   C  C  -  C  C  -
04 2-0   D  D  -  D  -  D
05 2-0   -  -  -  E  E  -
06 2-0   F  -  F  -  F  -
07 2-0   -  -  -  -  -  -
08 2-0   -  H  -  -  -  -
09 2-0   I  -  -  I  I  -
10 2-0   J  -  J  -  -  J
11 2-0   -  -  K  K  -  K
12 0-3   -  L  -  -  L  L
13 0-3   -  -  M  M  -  M
14 0-3   -  N  -  N  -  N
15 0-3   -  -  O  -  -  O
16 0-4   P  P  -  -  P  -
17 0-5   -  Q  Q  Q  -  Q
18 0-5   -  -  R  -  -   
19 0-5   S  -  S  -  -   
20 1-3   -  -  T  T      
21 1-4   U  U  -  -      
22 2-5   V  V  -         
23 2-5   -  X  -         
24 2-5   -  -            
25 2-5   -  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OUMKP JWOPH SLWWQ FNCMS KCUIS C
-------------------------------
