EFFECTIVE PERIOD:
21-DEC-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   -  B  -  B  -  -
03 1-0   C  -  -  -  C  C
04 1-0   D  -  D  D  D  -
05 2-0   E  -  E  -  -  -
06 0-3   -  F  F  F  F  -
07 0-3   G  -  -  -  G  G
08 0-3   -  -  H  H  H  H
09 0-3   I  -  -  I  I  -
10 0-3   -  -  J  J  J  -
11 0-5   K  K  K  -  K  K
12 0-5   -  -  L  -  -  -
13 0-5   -  M  M  M  M  -
14 0-5   -  N  -  N  -  N
15 0-5   O  O  -  -  -  O
16 0-5   P  -  -  -  -  P
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   -  -  -  -  -   
19 0-6   -  S  S  S  S   
20 0-6   -  -  T  T      
21 0-6   U  -  -  -      
22 1-3   -  V  V         
23 1-6   -  X  -         
24 2-4   -  Y            
25 2-6   -  -            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

LQCMG QNQSG QJCYZ UMZKM USNGG I
-------------------------------
