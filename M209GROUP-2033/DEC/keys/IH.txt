EFFECTIVE PERIOD:
03-DEC-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  B  -  -  -
03 1-0   -  -  -  C  -  -
04 1-0   -  D  D  -  -  -
05 1-0   E  E  E  E  E  E
06 1-0   F  F  F  F  F  F
07 1-0   -  G  -  -  G  G
08 2-0   -  -  -  -  -  H
09 2-0   I  I  -  I  I  -
10 0-3   -  -  J  -  J  -
11 0-6   K  -  -  -  K  K
12 0-6   -  L  -  -  L  L
13 0-6   M  -  M  -  -  M
14 0-6   -  N  -  N  -  N
15 0-6   -  -  -  -  -  -
16 0-6   P  P  P  P  -  P
17 0-6   Q  Q  -  Q  -  -
18 1-3   R  -  R  -  R   
19 1-5   S  S  -  -  S   
20 1-6   T  -  -  T      
21 1-6   -  U  U  U      
22 1-6   V  -  V         
23 1-6   -  X  -         
24 2-3   -  -            
25 2-6   -  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

WYZVT LSRIA QSWRT SSROA LMSMP F
-------------------------------
