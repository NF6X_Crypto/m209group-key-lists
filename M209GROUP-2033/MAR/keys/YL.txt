EFFECTIVE PERIOD:
22-MAR-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  B  B  B  B  -
03 1-0   -  -  C  C  -  C
04 1-0   -  D  -  -  D  D
05 0-3   E  E  E  E  E  E
06 0-3   F  -  -  -  F  -
07 0-5   G  -  -  -  G  G
08 0-5   -  H  -  H  H  -
09 0-5   -  I  -  I  -  -
10 0-5   -  J  -  -  J  -
11 0-5   -  K  K  -  K  K
12 0-5   L  -  -  -  L  L
13 0-6   M  M  -  -  -  -
14 0-6   -  N  -  N  -  -
15 0-6   -  O  O  O  -  -
16 1-5   P  -  P  P  -  P
17 1-6   -  -  Q  -  Q  Q
18 1-6   R  -  -  R  -   
19 1-6   S  -  S  -  -   
20 1-6   -  T  T  T      
21 2-3   U  U  -  U      
22 2-6   V  V  V         
23 3-5   W  X  -         
24 3-6   X  Y            
25 3-6   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

YPQQZ AULRY PWWTO RJXUJ HBQRP H
-------------------------------
