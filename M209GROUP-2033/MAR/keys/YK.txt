EFFECTIVE PERIOD:
21-MAR-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   B  B  -  -  -  -
03 1-0   C  -  C  C  -  -
04 1-0   -  D  -  -  -  D
05 1-0   -  -  -  -  -  -
06 0-3   F  F  -  -  F  F
07 0-3   G  -  -  -  G  -
08 0-3   -  H  H  -  H  -
09 0-3   -  I  I  I  I  I
10 0-4   J  J  J  J  -  J
11 0-4   K  K  -  K  -  -
12 0-4   L  -  L  L  -  L
13 0-4   -  -  -  -  M  -
14 0-5   N  -  -  N  N  -
15 0-6   -  O  -  -  O  O
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  Q  -  Q  Q
18 0-6   R  -  R  R  -   
19 0-6   -  S  -  -  S   
20 0-6   T  T  -  T      
21 0-6   -  -  U  -      
22 1-4   -  V  V         
23 1-6   -  -  X         
24 1-6   -  -            
25 2-5   Y  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

AWPMW IIMHR JMVKC LRRUL PWPLP G
-------------------------------
