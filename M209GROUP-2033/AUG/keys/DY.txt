EFFECTIVE PERIOD:
12-AUG-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  -
02 0-3   B  B  -  -  -  B
03 0-3   C  C  -  -  C  C
04 0-3   -  -  D  D  -  -
05 0-3   -  E  -  E  E  -
06 0-3   F  F  F  -  -  -
07 0-3   G  -  G  -  -  G
08 0-3   -  -  H  H  H  H
09 0-3   -  -  -  I  I  I
10 0-3   -  J  J  -  J  J
11 0-3   -  -  -  -  -  K
12 0-4   -  L  -  -  -  -
13 0-4   M  M  -  M  M  M
14 0-4   N  -  N  N  -  -
15 0-4   -  O  O  -  O  -
16 0-4   -  -  P  P  P  -
17 0-4   -  -  Q  Q  -  -
18 0-4   R  -  -  -  R   
19 0-4   S  S  S  -  S   
20 0-5   -  T  -  T      
21 0-6   U  -  -  -      
22 0-6   V  V  V         
23 0-6   -  -  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YUQUL LCLRQ NBLZF SAZZP DHLNS A
-------------------------------
