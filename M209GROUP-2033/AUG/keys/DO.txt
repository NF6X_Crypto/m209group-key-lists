EFFECTIVE PERIOD:
02-AUG-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  B  B  B  -
03 1-0   -  -  -  C  -  C
04 1-0   -  -  D  D  -  D
05 0-3   -  E  -  -  -  -
06 0-3   F  F  -  F  F  -
07 0-3   G  -  -  -  G  G
08 0-3   -  H  H  -  -  -
09 0-3   I  I  I  I  I  I
10 0-3   J  J  J  -  -  J
11 0-4   -  -  -  K  -  K
12 0-4   L  L  L  -  -  L
13 0-4   M  -  M  M  M  -
14 0-4   N  -  -  -  -  N
15 0-6   O  -  O  -  -  -
16 0-6   P  -  -  -  -  P
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  R  -  R   
19 0-6   S  S  S  -  -   
20 1-3   T  -  -  T      
21 1-5   -  -  -  -      
22 2-6   -  -  -         
23 3-4   -  X  -         
24 4-6   -  -            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

USAWV SAOBP VPPSJ JPARV PPWFW O
-------------------------------
