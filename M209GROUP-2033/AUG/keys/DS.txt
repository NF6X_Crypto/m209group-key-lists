EFFECTIVE PERIOD:
06-AUG-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  -
02 2-0   -  B  -  B  -  -
03 2-0   C  C  -  -  C  C
04 2-0   D  D  -  D  -  -
05 0-3   -  -  -  E  E  E
06 0-4   F  F  F  -  F  -
07 0-4   -  -  -  -  G  -
08 0-4   -  -  -  H  -  H
09 0-4   I  I  -  I  -  I
10 0-4   J  -  J  -  -  -
11 0-5   -  -  K  K  K  K
12 0-5   L  L  L  -  L  L
13 0-5   -  M  M  -  -  M
14 0-5   -  -  N  N  -  -
15 0-5   -  O  O  O  O  O
16 0-5   P  -  -  P  P  P
17 0-5   -  Q  -  Q  Q  Q
18 1-2   R  R  R  R  -   
19 1-3   -  S  -  -  -   
20 1-6   T  T  T  -      
21 2-3   -  -  -  -      
22 2-4   -  V  -         
23 2-4   -  X  X         
24 2-4   -  -            
25 2-4   Y  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VORXA YEJJF RLXZU ERKJA FEAAU P
-------------------------------
