EFFECTIVE PERIOD:
14-AUG-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  -  -  B  B
03 1-0   C  -  C  -  C  C
04 1-0   -  -  -  D  -  -
05 1-0   -  -  -  -  -  -
06 1-0   -  -  F  F  -  F
07 2-0   G  G  -  G  G  G
08 2-0   -  -  -  H  -  H
09 2-0   -  -  I  -  I  -
10 2-0   J  -  J  J  -  J
11 2-0   K  K  K  -  K  -
12 0-3   -  -  -  -  -  L
13 0-4   -  -  -  -  -  -
14 0-4   -  N  -  N  N  -
15 0-4   O  -  O  -  -  -
16 1-2   P  P  -  P  P  P
17 1-4   -  -  -  Q  -  Q
18 1-4   R  R  R  R  R   
19 1-4   -  -  -  -  S   
20 1-4   -  T  T  -      
21 2-3   U  U  U  -      
22 3-4   -  V  V         
23 3-4   -  X  -         
24 3-4   X  Y            
25 3-6   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KULSO ZNNXT RKVMM OMQOF QRMCU T
-------------------------------
