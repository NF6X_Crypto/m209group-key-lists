EFFECTIVE PERIOD:
15-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  B  -  B  -  -
03 1-0   -  -  C  -  C  C
04 1-0   D  D  -  -  D  -
05 0-3   -  E  -  E  -  E
06 0-3   F  -  F  F  -  -
07 0-3   -  G  G  -  -  G
08 0-3   -  -  -  H  H  H
09 0-6   I  I  I  -  -  I
10 0-6   J  -  -  J  -  -
11 0-6   -  -  -  K  K  K
12 0-6   -  L  -  -  -  -
13 0-6   -  -  M  -  -  M
14 0-6   N  N  N  N  N  -
15 0-6   -  -  -  O  O  O
16 1-3   -  P  P  -  -  -
17 1-3   -  -  Q  -  -  Q
18 1-4   R  R  R  -  R   
19 1-5   S  -  -  S  S   
20 1-5   -  -  T  T      
21 1-6   U  U  U  U      
22 1-6   -  V  V         
23 1-6   -  -  -         
24 1-6   -  Y            
25 2-5   Y  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

SBATS MWWIV TPTMV VSMLV MPLTV K
-------------------------------
