EFFECTIVE PERIOD:
16-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  B  -  -  B  -
03 1-0   C  -  C  -  C  C
04 1-0   D  D  -  D  D  D
05 1-0   -  -  E  E  E  -
06 1-0   F  -  F  F  -  -
07 1-0   -  -  -  G  -  G
08 1-0   H  H  H  -  -  H
09 1-0   I  -  I  I  I  -
10 1-0   -  J  -  -  -  -
11 0-3   -  K  K  K  K  -
12 0-4   -  L  L  L  -  L
13 0-4   -  M  -  M  M  -
14 0-4   -  -  N  -  N  -
15 0-4   O  -  O  -  -  -
16 0-5   P  -  -  P  P  P
17 0-6   -  -  -  -  Q  Q
18 0-6   R  R  -  R  R   
19 0-6   -  S  S  S  -   
20 0-6   -  T  T  -      
21 0-6   -  -  U  U      
22 1-6   -  -  -         
23 1-6   W  X  -         
24 1-6   X  Y            
25 2-3   -  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ULNGC QQOTL VNIXZ HNHWS IIKMI N
-------------------------------
