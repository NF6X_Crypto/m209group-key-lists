EFFECTIVE PERIOD:
17-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  B  B  -  -  -
03 1-0   C  -  -  C  -  -
04 1-0   -  D  -  D  D  D
05 1-0   -  -  -  -  -  -
06 1-0   -  -  F  F  -  -
07 2-0   G  G  G  -  G  G
08 2-0   H  -  -  H  -  H
09 2-0   -  I  -  -  I  -
10 0-4   J  J  -  -  J  J
11 0-4   K  -  K  -  -  -
12 0-4   -  L  L  -  -  L
13 0-4   M  M  M  -  M  M
14 0-5   -  N  -  N  -  N
15 0-6   -  O  O  -  O  O
16 0-6   -  -  P  P  P  -
17 0-6   Q  Q  Q  Q  -  -
18 1-4   R  -  R  R  -   
19 1-6   -  S  -  S  S   
20 2-5   T  -  T  -      
21 2-5   -  -  U  -      
22 2-6   V  -  -         
23 2-6   W  -  -         
24 2-6   X  Y            
25 2-6   -  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

IXURA FNTMO MQOOU ARTOM WPQTT P
-------------------------------
