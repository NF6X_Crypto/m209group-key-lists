EFFECTIVE PERIOD:
04-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  -  B  -  -  B
03 1-0   C  -  -  C  C  -
04 1-0   D  D  -  D  -  -
05 1-0   -  -  -  E  E  -
06 0-3   -  -  F  F  F  -
07 0-3   G  G  G  -  G  G
08 0-3   -  -  H  -  -  -
09 0-4   I  I  I  -  -  -
10 0-4   -  -  -  J  J  J
11 0-4   -  -  -  K  -  -
12 0-4   L  -  -  -  -  L
13 0-4   M  -  M  M  M  M
14 0-4   -  -  N  -  -  -
15 0-5   -  O  -  O  O  O
16 0-5   P  -  P  -  P  -
17 0-5   Q  Q  -  -  Q  Q
18 1-4   R  -  R  -  -   
19 1-4   -  -  S  -  -   
20 1-4   T  T  T  T      
21 1-4   U  U  -  U      
22 1-5   V  V  V         
23 2-3   -  -  X         
24 2-4   -  Y            
25 2-4   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

XOXES URNVR RWQWX EFURT EMRAF H
-------------------------------
