EFFECTIVE PERIOD:
12-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  -  -  B  B  B
03 1-0   C  C  -  -  C  -
04 1-0   D  D  -  -  D  D
05 1-0   -  -  E  E  -  -
06 1-0   F  -  F  F  -  -
07 1-0   -  -  -  G  -  -
08 1-0   H  -  H  -  H  H
09 1-0   -  I  I  I  I  I
10 1-0   J  -  J  J  -  -
11 1-0   K  -  K  -  -  -
12 2-0   L  L  -  L  L  L
13 0-3   -  M  M  M  M  M
14 0-3   -  N  -  -  N  N
15 0-3   O  -  -  O  -  -
16 0-3   -  P  P  P  P  P
17 0-3   Q  -  Q  -  Q  -
18 0-3   -  R  R  -  -   
19 0-4   S  S  S  -  -   
20 0-4   -  -  -  T      
21 0-4   U  U  U  U      
22 0-4   V  -  -         
23 0-5   -  X  X         
24 1-3   -  Y            
25 3-4   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

HPOZL VMMIL IPZSZ OOMYL ZTUII T
-------------------------------
