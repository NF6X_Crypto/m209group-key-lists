EFFECTIVE PERIOD:
07-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 2-0   -  -  -  B  B  -
03 2-0   -  C  C  C  C  -
04 2-0   -  -  -  -  -  D
05 0-3   -  E  -  E  E  -
06 0-3   F  -  F  -  F  -
07 0-3   -  G  -  -  -  G
08 0-3   -  -  H  H  -  H
09 0-3   I  -  -  -  I  I
10 0-3   J  -  -  -  -  -
11 0-3   K  -  K  K  -  K
12 0-3   L  L  -  -  -  L
13 0-3   -  -  -  -  M  -
14 0-3   N  N  N  N  N  -
15 0-5   O  O  O  -  -  -
16 0-5   P  -  -  -  -  P
17 0-5   Q  -  Q  Q  Q  Q
18 0-6   -  R  -  -  -   
19 0-6   S  -  -  S  S   
20 0-6   -  T  T  T      
21 0-6   U  U  U  -      
22 1-4   -  V  V         
23 1-5   -  -  X         
24 2-3   X  -            
25 2-3   -  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XXESA QVVOR XGQPF UGUFC SGNRW I
-------------------------------
