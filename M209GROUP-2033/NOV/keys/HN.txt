EFFECTIVE PERIOD:
13-NOV-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   -  B  -  -  B  B
03 1-0   C  -  -  C  C  C
04 1-0   -  -  D  D  D  -
05 1-0   -  E  E  -  -  -
06 1-0   -  -  F  -  -  -
07 2-0   -  G  -  G  -  G
08 0-3   H  H  -  -  H  H
09 0-3   I  I  I  -  I  I
10 0-4   J  J  -  -  -  -
11 0-4   K  -  -  K  -  -
12 0-4   L  -  L  -  -  -
13 0-4   M  -  -  M  M  M
14 0-4   -  -  -  N  N  -
15 0-4   O  -  O  -  O  -
16 0-5   -  -  -  -  -  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-5   R  -  -  -  R   
19 0-5   -  S  -  S  -   
20 1-5   -  T  T  T      
21 1-5   -  U  U  -      
22 1-5   V  V  -         
23 1-5   -  -  X         
24 1-6   -  Y            
25 2-3   Y  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

HZRMJ BAMAA URJOE UIBPU RZWKE W
-------------------------------
