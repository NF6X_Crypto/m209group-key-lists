EFFECTIVE PERIOD:
19-JUN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  B  B  -  B
03 1-0   -  -  C  -  -  -
04 1-0   -  -  -  D  -  -
05 1-0   -  E  E  -  E  E
06 0-4   F  F  F  -  F  F
07 0-4   -  -  -  -  G  G
08 0-4   -  -  -  H  -  H
09 0-5   -  I  -  -  I  -
10 0-5   -  J  -  J  -  -
11 0-5   -  K  -  -  K  -
12 0-5   -  L  L  L  L  -
13 0-5   M  -  -  -  M  -
14 0-5   N  -  -  -  N  N
15 0-5   -  O  -  -  -  -
16 1-4   P  -  P  -  P  -
17 1-4   Q  Q  -  -  -  -
18 1-4   R  R  -  R  -   
19 1-4   S  S  S  -  -   
20 1-5   -  -  T  T      
21 2-6   U  -  U  U      
22 3-4   V  V  V         
23 3-4   W  X  X         
24 3-5   X  -            
25 3-6   -  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SRVNJ SNTAM LGAMO EZZTJ VMARN G
-------------------------------
