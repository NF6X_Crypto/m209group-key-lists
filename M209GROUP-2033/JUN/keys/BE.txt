EFFECTIVE PERIOD:
01-JUN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  A
02 2-0   B  -  -  B  B  -
03 2-0   -  C  -  -  -  C
04 2-0   -  -  D  -  -  -
05 2-0   -  -  E  -  -  E
06 0-3   F  -  -  F  -  F
07 0-3   -  -  G  G  G  G
08 0-3   -  -  -  -  -  H
09 0-3   -  I  I  I  I  I
10 0-3   -  J  J  -  -  -
11 0-3   -  K  K  K  K  K
12 0-3   L  -  -  -  -  -
13 0-4   -  -  M  -  M  -
14 0-4   -  -  -  N  -  -
15 0-4   O  -  O  O  -  -
16 1-4   -  P  P  -  P  P
17 2-3   Q  Q  -  Q  -  Q
18 2-4   R  -  R  -  R   
19 2-4   S  S  -  S  -   
20 2-4   T  T  -  -      
21 2-4   U  U  -  -      
22 3-6   V  V  V         
23 4-5   -  X  X         
24 4-6   X  Y            
25 4-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

EAZAA PTOUA UEFVV NLNKZ TJVNM V
-------------------------------
