EFFECTIVE PERIOD:
27-JUN-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  -  B  -  B
03 1-0   C  -  C  -  C  C
04 1-0   D  D  D  D  -  D
05 1-0   -  E  -  -  -  E
06 1-0   F  F  -  F  F  -
07 2-0   G  -  -  -  -  -
08 2-0   H  -  H  H  H  -
09 2-0   -  I  -  I  I  I
10 2-0   J  J  -  -  J  -
11 0-4   K  -  K  -  -  -
12 0-5   L  L  -  L  L  L
13 0-5   M  M  M  -  M  M
14 0-5   -  -  N  N  N  N
15 0-5   O  O  O  O  O  O
16 0-5   -  P  -  -  -  -
17 0-5   Q  Q  Q  Q  -  Q
18 0-6   -  -  R  R  -   
19 0-6   -  -  -  -  S   
20 1-2   T  -  T  T      
21 1-6   -  -  U  U      
22 2-5   -  -  -         
23 2-5   -  X  X         
24 2-5   -  -            
25 2-5   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

PAUOM BEMUL ADLSR TYUKT AVYUA T
-------------------------------
