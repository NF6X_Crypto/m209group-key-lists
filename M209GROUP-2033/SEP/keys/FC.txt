EFFECTIVE PERIOD:
11-SEP-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  A
02 2-0   B  B  -  -  B  -
03 2-0   -  -  C  -  C  C
04 2-0   D  -  D  -  D  -
05 2-0   E  E  -  E  -  E
06 0-3   -  F  -  -  F  -
07 0-3   G  G  G  G  -  G
08 0-5   -  H  -  H  H  H
09 0-5   -  -  -  I  -  I
10 0-5   J  J  -  -  -  -
11 0-6   -  -  K  -  K  K
12 0-6   -  -  -  L  L  -
13 0-6   M  M  M  M  M  -
14 0-6   -  N  N  -  -  N
15 0-6   -  -  -  -  -  -
16 1-4   P  -  P  P  -  P
17 2-5   Q  -  -  -  -  -
18 2-5   -  -  -  R  -   
19 2-5   -  -  S  S  S   
20 2-5   T  -  -  T      
21 2-6   U  U  -  -      
22 3-4   V  -  V         
23 3-5   W  -  X         
24 3-5   X  Y            
25 3-6   -  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VLFKA ONTGA RXQVH YSTQK ADLQT F
-------------------------------
