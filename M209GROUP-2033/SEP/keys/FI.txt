EFFECTIVE PERIOD:
17-SEP-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   -  B  B  -  -  -
03 1-0   C  -  -  -  -  -
04 1-0   D  D  -  -  -  -
05 1-0   E  E  E  -  E  E
06 2-0   F  F  F  -  -  -
07 2-0   G  -  G  -  G  -
08 2-0   H  H  H  H  H  -
09 2-0   -  I  -  I  -  -
10 2-0   -  J  J  -  -  -
11 0-3   K  K  -  K  K  K
12 0-3   -  -  -  L  L  L
13 0-3   -  -  -  M  M  M
14 0-3   -  -  -  -  -  N
15 0-5   O  O  O  O  -  -
16 0-5   -  P  P  -  -  -
17 0-5   Q  Q  Q  Q  Q  Q
18 0-5   -  R  R  R  -   
19 0-6   -  S  S  S  S   
20 1-3   -  -  -  T      
21 1-5   U  -  -  U      
22 2-3   -  -  -         
23 2-3   W  -  -         
24 2-3   -  Y            
25 2-3   -  -            
26 2-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QAQQO UCNGJ OKPGZ ZURWV QQZCP U
-------------------------------
