EFFECTIVE PERIOD:
12-SEP-2033 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  A
02 2-0   B  B  B  B  -  -
03 0-4   C  C  -  -  C  -
04 0-4   D  D  D  -  D  -
05 0-5   -  -  -  -  E  -
06 0-5   -  -  F  -  F  F
07 0-5   -  G  G  -  -  G
08 0-5   H  -  -  -  -  -
09 0-6   I  I  I  I  -  I
10 0-6   J  -  J  J  J  J
11 0-6   -  -  -  K  -  -
12 0-6   L  -  L  L  L  -
13 0-6   M  -  M  M  -  M
14 0-6   N  N  -  -  N  N
15 0-6   -  -  -  -  O  O
16 1-2   -  -  -  -  -  P
17 1-2   Q  -  -  -  Q  Q
18 1-3   -  R  R  -  R   
19 1-4   -  -  -  S  -   
20 2-4   T  -  T  T      
21 2-4   -  U  -  U      
22 2-5   -  -  -         
23 2-5   -  X  -         
24 2-5   X  Y            
25 2-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ODQWM QANTN TJYYL JJSIA QWUMY I
-------------------------------
