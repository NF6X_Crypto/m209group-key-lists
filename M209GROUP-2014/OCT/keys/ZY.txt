EFFECTIVE PERIOD:
27-OCT-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  B  -  -  B  B
03 1-0   C  C  -  -  -  C
04 1-0   -  -  -  -  D  D
05 1-0   -  -  E  E  E  -
06 1-0   F  -  -  -  F  F
07 1-0   -  -  -  G  -  -
08 1-0   -  -  -  H  -  H
09 1-0   -  -  I  -  I  I
10 1-0   -  J  J  J  J  J
11 0-3   K  K  K  K  K  -
12 0-3   L  -  L  L  -  L
13 0-3   M  -  -  M  -  -
14 0-3   -  -  -  -  -  N
15 0-4   O  O  -  O  -  -
16 0-5   -  -  P  -  P  -
17 0-5   -  -  Q  Q  Q  Q
18 0-5   R  R  -  R  R   
19 0-5   -  S  S  -  -   
20 0-6   T  -  -  -      
21 0-6   U  U  -  U      
22 1-3   -  -  -         
23 1-3   -  X  X         
24 2-4   -  -            
25 3-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

CDTLU YJZFO KSVUK FWFQJ MTLWF A
-------------------------------
