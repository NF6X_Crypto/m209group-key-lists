EFFECTIVE PERIOD:
31-OCT-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  -  B  B  B  -
03 1-0   -  -  C  C  -  C
04 1-0   D  D  -  -  D  -
05 1-0   E  -  -  -  E  E
06 2-0   F  -  -  F  -  -
07 0-3   -  -  G  -  G  -
08 0-4   -  H  H  -  -  H
09 0-4   I  -  I  I  I  I
10 0-4   J  J  -  -  J  -
11 0-4   -  -  K  -  K  K
12 0-4   L  -  -  -  L  -
13 0-4   -  -  M  M  M  -
14 0-4   -  N  -  -  N  -
15 0-4   -  O  -  -  -  O
16 0-4   -  -  -  P  -  P
17 0-5   -  Q  Q  Q  Q  -
18 0-6   -  R  -  -  -   
19 0-6   S  S  -  S  -   
20 0-6   -  -  -  -      
21 0-6   U  U  U  -      
22 1-3   V  V  -         
23 1-6   -  -  X         
24 2-3   -  Y            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TACVS OULZV LZJZZ NJUEH KRPZT N
-------------------------------
