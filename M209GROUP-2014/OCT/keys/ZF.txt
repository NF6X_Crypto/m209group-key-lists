EFFECTIVE PERIOD:
08-OCT-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  C  -  -  -  -
04 2-0   D  D  -  -  -  -
05 2-0   -  E  -  E  E  -
06 0-3   F  -  F  -  -  -
07 0-3   G  G  G  G  -  G
08 0-3   H  H  H  H  -  H
09 0-3   -  -  -  -  I  I
10 0-3   -  -  -  -  J  -
11 0-6   -  -  K  -  -  -
12 0-6   L  -  -  -  -  L
13 0-6   -  -  -  -  M  M
14 0-6   -  N  -  N  N  -
15 0-6   O  -  -  O  O  -
16 1-3   -  -  P  P  -  P
17 1-3   Q  Q  -  Q  Q  Q
18 1-3   -  -  R  R  -   
19 1-3   -  -  S  -  -   
20 1-6   -  -  -  T      
21 2-3   U  -  U  U      
22 2-3   -  -  V         
23 2-5   -  X  X         
24 2-6   X  Y            
25 3-5   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZRXDN YGUVV RUITS XSVSG ODRHO R
-------------------------------
