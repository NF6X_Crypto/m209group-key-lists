EFFECTIVE PERIOD:
25-OCT-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 2-0   B  B  -  B  -  -
03 2-0   -  -  C  C  C  C
04 2-0   D  D  D  -  -  -
05 2-0   E  E  E  E  E  E
06 0-4   -  -  -  F  F  -
07 0-4   G  -  -  G  G  G
08 0-4   H  H  H  -  -  -
09 0-6   -  I  -  I  -  -
10 0-6   J  -  -  -  -  J
11 0-6   -  K  K  -  K  K
12 0-6   L  -  -  -  -  L
13 0-6   M  -  -  -  -  -
14 0-6   N  N  N  N  -  -
15 0-6   -  O  O  O  O  O
16 1-3   -  P  P  -  P  P
17 1-4   -  -  -  Q  Q  Q
18 1-4   -  -  -  -  R   
19 1-4   S  S  -  S  S   
20 1-6   T  T  T  T      
21 2-4   U  U  U  -      
22 2-4   -  V  V         
23 2-4   W  X  X         
24 2-4   -  Y            
25 2-6   Y  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TAMFA WAALA NUQVT TIVWJ FRVVX S
-------------------------------
