EFFECTIVE PERIOD:
19-AUG-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  -  B  B  -
03 1-0   -  C  C  -  C  -
04 2-0   -  D  -  -  -  D
05 2-0   -  -  -  -  E  -
06 2-0   -  F  F  -  -  F
07 2-0   G  -  -  G  -  G
08 0-5   H  H  H  H  H  -
09 0-5   -  -  I  -  -  -
10 0-5   J  J  -  J  -  J
11 0-5   -  -  K  K  -  K
12 0-5   L  -  L  L  -  -
13 0-5   -  M  M  M  -  -
14 0-6   -  -  -  N  N  N
15 0-6   -  O  O  O  O  O
16 1-2   -  P  P  -  -  -
17 1-2   Q  -  Q  -  Q  -
18 1-2   -  -  -  R  R   
19 1-2   S  -  -  -  S   
20 1-3   -  T  T  -      
21 1-4   U  -  U  -      
22 1-6   -  -  -         
23 1-6   W  X  -         
24 1-6   -  -            
25 2-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QRKWG XJWPT AIWPI KQPRA UPUJO R
-------------------------------
