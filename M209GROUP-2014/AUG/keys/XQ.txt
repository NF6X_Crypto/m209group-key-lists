EFFECTIVE PERIOD:
28-AUG-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 2-0   -  -  B  -  B  B
03 2-0   C  -  -  -  -  -
04 2-0   -  D  -  D  D  D
05 2-0   E  -  -  -  -  -
06 0-3   F  -  -  F  F  F
07 0-3   -  G  G  G  G  G
08 0-3   -  -  -  -  H  -
09 0-3   -  -  I  -  I  I
10 0-4   J  -  -  J  -  J
11 0-5   K  K  -  K  K  -
12 0-5   L  L  L  L  -  -
13 0-5   M  -  M  M  -  M
14 0-6   -  -  N  N  -  -
15 0-6   -  O  -  O  -  O
16 0-6   P  P  P  -  P  -
17 0-6   Q  Q  Q  -  Q  Q
18 0-6   -  -  R  -  R   
19 0-6   S  -  -  -  S   
20 0-6   -  T  -  T      
21 0-6   -  U  -  -      
22 1-5   -  -  V         
23 2-3   W  X  -         
24 2-5   -  Y            
25 3-6   Y  Z            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PIVJU JUGUL UQLQK RVFNL CXPWE Y
-------------------------------
