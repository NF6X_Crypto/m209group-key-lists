EFFECTIVE PERIOD:
26-AUG-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 2-0   B  B  B  -  -  -
03 2-0   C  C  C  -  C  -
04 2-0   -  -  -  D  -  -
05 2-0   -  E  E  -  E  E
06 0-3   F  F  F  F  F  -
07 0-4   -  -  -  G  G  -
08 0-4   -  -  H  H  H  -
09 0-4   -  -  I  -  -  I
10 0-4   -  J  -  J  -  J
11 0-4   K  K  K  -  K  -
12 0-4   L  L  -  -  L  -
13 0-4   M  -  -  M  -  M
14 0-5   -  -  N  -  N  N
15 0-6   O  O  O  -  O  O
16 0-6   -  -  -  P  -  P
17 0-6   Q  -  -  -  -  Q
18 0-6   -  R  -  R  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  -  -      
21 0-6   -  -  U  U      
22 0-6   -  -  -         
23 0-6   -  -  X         
24 2-3   -  Y            
25 2-4   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QDLZQ WRNKQ IRRIM ZXCML RZSNU G
-------------------------------
