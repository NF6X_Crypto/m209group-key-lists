EFFECTIVE PERIOD:
07-AUG-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 2-0   B  -  -  -  -  B
03 2-0   -  -  C  -  C  -
04 2-0   -  D  -  D  -  D
05 2-0   -  E  E  E  -  -
06 2-0   -  -  F  -  -  F
07 0-3   G  -  G  -  G  -
08 0-3   H  -  H  -  -  H
09 0-3   -  -  -  -  I  -
10 0-3   J  J  J  J  -  J
11 0-3   K  K  K  -  -  -
12 0-3   -  -  -  L  -  -
13 0-3   -  M  -  -  M  -
14 0-3   N  -  -  -  -  N
15 0-3   O  -  O  O  O  -
16 0-4   -  -  P  P  P  -
17 0-4   -  Q  -  Q  -  Q
18 0-4   R  -  -  R  R   
19 0-4   -  S  S  S  S   
20 0-5   T  -  -  -      
21 0-6   -  U  U  -      
22 1-4   V  -  V         
23 1-6   W  X  -         
24 2-3   -  Y            
25 2-3   Y  Z            
26 2-3   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

ZMYIQ RRQUP NCUFU ROOHL ALAQU V
-------------------------------
