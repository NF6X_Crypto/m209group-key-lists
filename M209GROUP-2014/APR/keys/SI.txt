EFFECTIVE PERIOD:
12-APR-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 0-3   B  -  -  -  B  B
03 0-3   -  -  C  C  C  -
04 0-3   -  -  D  -  -  D
05 0-3   E  -  -  -  -  -
06 0-3   -  -  -  F  -  F
07 0-3   G  -  G  G  -  G
08 0-3   -  H  -  -  H  -
09 0-4   I  I  -  -  I  -
10 0-5   -  J  J  J  -  -
11 0-5   K  K  -  -  K  K
12 0-5   -  -  L  -  -  L
13 0-5   -  -  -  M  -  -
14 0-5   -  N  -  N  N  -
15 0-5   -  O  O  -  -  O
16 0-5   -  P  -  -  P  P
17 0-6   -  -  Q  Q  -  -
18 0-6   R  R  -  R  R   
19 0-6   S  S  S  S  S   
20 1-4   -  -  -  T      
21 1-6   -  U  U  -      
22 2-5   V  -  -         
23 3-5   W  -  X         
24 3-5   X  -            
25 3-5   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

HSOPS RPAGT QERPB PTGSP AAGLQ S
-------------------------------
