EFFECTIVE PERIOD:
11-MAY-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  -  B  -  -  B
03 1-0   -  -  -  C  C  -
04 1-0   -  -  D  D  -  -
05 1-0   -  -  -  -  -  E
06 1-0   F  F  F  F  -  -
07 2-0   G  -  G  G  -  -
08 2-0   -  H  -  -  -  H
09 2-0   -  I  -  I  I  -
10 2-0   -  J  -  -  J  J
11 2-0   K  -  -  -  K  K
12 2-0   L  L  L  L  L  L
13 2-0   M  -  M  -  -  -
14 0-3   N  -  -  -  -  N
15 0-5   -  -  -  O  -  O
16 0-5   P  P  -  -  P  P
17 0-6   -  Q  Q  -  Q  -
18 0-6   R  -  -  -  R   
19 0-6   -  -  S  S  S   
20 0-6   T  T  -  T      
21 0-6   -  U  U  -      
22 1-5   -  -  V         
23 1-6   W  X  X         
24 2-6   -  Y            
25 2-6   Y  Z            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SPSJS UNLUT NYIAJ BASTN QQXKK M
-------------------------------
