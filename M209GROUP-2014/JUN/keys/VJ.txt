EFFECTIVE PERIOD:
30-JUN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  A  A  -
02 0-3   B  -  B  B  -  B
03 0-3   -  C  -  C  C  -
04 0-3   -  -  D  D  -  D
05 0-3   E  -  E  -  E  -
06 0-3   F  -  -  -  F  F
07 0-4   -  G  G  G  -  G
08 0-5   H  -  -  -  -  -
09 0-5   -  -  I  -  I  -
10 0-5   J  J  J  J  -  -
11 0-5   K  K  -  -  K  -
12 0-5   -  -  L  L  L  L
13 0-5   -  -  -  -  -  -
14 0-5   N  N  N  -  N  N
15 0-5   -  O  -  -  -  O
16 0-6   -  -  P  -  P  -
17 0-6   Q  Q  -  -  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   -  S  -  S  -   
20 0-6   -  T  T  -      
21 0-6   U  -  U  -      
22 0-6   -  V  -         
23 0-6   W  X  X         
24 0-6   X  -            
25 0-6   -  -            
26 1-2   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

PKPJT KZHTH ZITZG BPBAO MZZYJ Z
-------------------------------
