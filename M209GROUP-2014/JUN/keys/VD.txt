EFFECTIVE PERIOD:
24-JUN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   C  C  C  -  C  -
04 1-0   -  D  D  D  -  D
05 1-0   E  -  E  -  E  -
06 1-0   F  F  -  F  F  -
07 1-0   G  -  G  -  -  -
08 1-0   -  -  -  -  H  H
09 1-0   I  I  -  I  I  -
10 1-0   -  -  J  -  -  J
11 1-0   -  -  -  -  -  K
12 2-0   L  L  L  L  -  -
13 2-0   -  M  M  -  -  -
14 2-0   N  N  -  N  -  N
15 0-3   -  O  -  -  O  -
16 0-4   -  -  -  -  P  -
17 0-4   Q  -  Q  -  -  Q
18 0-4   R  R  -  R  -   
19 0-4   -  S  -  S  S   
20 0-4   T  -  T  -      
21 0-4   -  -  U  U      
22 0-6   -  V  -         
23 0-6   W  -  -         
24 0-6   -  Y            
25 0-6   Y  Z            
26 2-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

HQKJP FMUGU IFWQP LUUZQ JCWIS D
-------------------------------
