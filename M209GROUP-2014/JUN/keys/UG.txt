EFFECTIVE PERIOD:
01-JUN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  A
02 2-0   -  -  B  -  -  -
03 0-3   -  -  -  C  -  C
04 0-3   D  D  -  D  D  -
05 0-3   E  E  E  E  E  -
06 0-4   F  -  -  -  F  -
07 0-4   G  -  -  G  G  G
08 0-4   -  -  H  H  -  -
09 0-4   -  I  -  -  I  -
10 0-4   J  -  J  -  -  J
11 0-6   -  K  K  -  K  K
12 0-6   L  -  L  L  L  -
13 0-6   M  -  -  -  -  M
14 0-6   N  N  -  -  -  N
15 0-6   -  -  O  -  -  O
16 1-5   P  P  P  -  P  P
17 2-3   -  Q  Q  -  -  Q
18 2-3   -  R  R  R  -   
19 2-5   -  S  -  S  S   
20 2-6   T  -  -  -      
21 3-4   U  U  U  -      
22 3-4   V  -  -         
23 3-4   -  X  -         
24 3-4   -  -            
25 3-5   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KOUUV QVTIQ ZSNTP QTSDV XUHKV K
-------------------------------
