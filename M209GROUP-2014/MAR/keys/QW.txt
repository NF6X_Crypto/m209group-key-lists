EFFECTIVE PERIOD:
05-MAR-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  B  -  B  -  -
03 1-0   -  C  C  C  -  C
04 1-0   D  D  -  -  D  D
05 2-0   E  -  -  -  -  E
06 2-0   -  F  F  F  F  F
07 2-0   G  -  -  G  -  -
08 2-0   H  -  H  H  H  -
09 0-4   I  -  I  -  -  I
10 0-5   -  -  J  J  -  -
11 0-5   K  -  K  -  K  -
12 0-5   -  L  -  -  -  L
13 0-6   M  -  M  M  -  -
14 0-6   N  N  -  N  N  N
15 0-6   -  O  -  -  O  O
16 0-6   P  P  -  -  P  -
17 0-6   -  -  -  -  Q  Q
18 0-6   -  -  -  -  -   
19 0-6   -  -  S  -  S   
20 0-6   T  -  T  T      
21 0-6   -  -  U  -      
22 1-2   V  -  -         
23 1-5   -  X  X         
24 2-6   X  Y            
25 2-6   Y  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

NREQV ESRQW NARMN WIGWG MWNVZ H
-------------------------------
