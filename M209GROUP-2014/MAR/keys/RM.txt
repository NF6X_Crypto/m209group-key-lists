EFFECTIVE PERIOD:
21-MAR-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 2-0   B  -  -  -  -  B
03 2-0   C  C  C  C  C  C
04 2-0   D  -  -  D  D  D
05 0-3   E  -  -  -  -  -
06 0-3   F  F  F  F  F  -
07 0-3   -  -  -  -  G  -
08 0-3   -  -  -  H  -  -
09 0-3   -  I  -  -  I  -
10 0-3   J  J  J  -  J  J
11 0-3   K  -  K  K  K  -
12 0-3   L  L  L  L  -  L
13 0-4   -  M  M  M  -  -
14 0-4   N  -  -  N  -  -
15 0-4   -  O  -  O  -  O
16 0-4   -  P  P  -  P  -
17 0-4   -  Q  Q  -  Q  Q
18 0-6   -  R  -  -  R   
19 0-6   S  S  S  -  -   
20 1-6   T  -  T  -      
21 2-4   U  -  -  U      
22 2-6   -  -  -         
23 3-4   -  X  X         
24 3-4   X  -            
25 3-4   Y  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

NQWTJ OQZRR FKUHM OSAEW YXYRC U
-------------------------------
