EFFECTIVE PERIOD:
20-JUL-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  -
02 2-0   B  B  B  B  B  -
03 2-0   C  C  -  C  C  -
04 2-0   D  -  -  D  -  -
05 2-0   E  E  E  -  -  E
06 0-4   F  F  -  F  -  F
07 0-4   -  G  G  -  G  -
08 0-4   H  -  H  H  H  -
09 0-4   I  I  -  I  -  I
10 0-4   -  J  -  J  -  J
11 0-5   K  -  -  -  K  K
12 0-6   -  -  -  -  -  -
13 0-6   M  M  M  M  M  -
14 0-6   -  N  -  N  -  N
15 0-6   O  O  O  -  O  -
16 1-3   P  P  -  P  -  -
17 1-4   -  -  -  -  Q  Q
18 1-5   -  -  -  -  -   
19 2-4   S  -  S  S  S   
20 2-4   T  -  T  T      
21 2-4   -  U  -  -      
22 2-4   -  V  -         
23 2-6   -  X  X         
24 4-5   -  -            
25 4-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZWZVC GYVPH QTZMQ VZYQM TOQRW M
-------------------------------
