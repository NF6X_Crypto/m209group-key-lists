EFFECTIVE PERIOD:
07-JUL-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  -  A  -
02 0-3   -  -  B  B  B  B
03 0-3   -  C  -  -  -  C
04 0-3   D  D  D  -  D  -
05 0-3   E  -  E  -  E  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  G  G  G  -
08 0-3   -  H  -  H  -  H
09 0-3   I  -  -  I  I  I
10 0-3   -  -  J  -  J  J
11 0-4   -  -  -  -  K  -
12 0-5   L  -  -  L  -  L
13 0-5   -  M  M  -  -  M
14 0-5   N  -  N  N  -  N
15 0-5   -  -  -  O  -  -
16 0-5   -  P  P  P  P  -
17 0-5   -  -  Q  -  -  -
18 0-5   -  R  R  -  R   
19 0-5   S  S  -  -  -   
20 0-6   -  T  -  T      
21 0-6   -  -  U  -      
22 0-6   V  -  -         
23 0-6   -  -  X         
24 0-6   X  -            
25 0-6   -  Z            
26 1-2   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

JIRST BIKPY MTOLZ IAKPR HZLUB Y
-------------------------------
