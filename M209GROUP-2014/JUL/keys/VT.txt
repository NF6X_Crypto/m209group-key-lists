EFFECTIVE PERIOD:
10-JUL-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  -  B  B  B  B
03 1-0   -  C  -  C  -  C
04 1-0   -  -  -  D  D  -
05 1-0   E  -  -  E  E  -
06 1-0   -  -  F  -  -  -
07 1-0   G  G  -  -  -  -
08 0-4   -  H  -  H  -  -
09 0-4   I  -  -  I  I  I
10 0-4   -  J  J  J  -  J
11 0-5   -  K  K  -  -  -
12 0-5   -  -  -  L  -  L
13 0-5   -  -  -  -  M  M
14 0-6   N  -  N  -  -  N
15 0-6   O  O  -  -  O  -
16 0-6   -  -  -  -  P  P
17 0-6   Q  Q  Q  Q  -  Q
18 1-3   -  -  R  -  -   
19 1-4   S  -  -  S  S   
20 1-5   -  T  T  T      
21 1-5   -  -  U  -      
22 1-5   -  V  -         
23 1-5   -  X  X         
24 2-3   X  Y            
25 3-4   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CVGXP GMPTX SWVGZ URSLU TAMPM X
-------------------------------
