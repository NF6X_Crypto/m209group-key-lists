EFFECTIVE PERIOD:
20-DEC-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  -  -  B  -  B
03 1-0   -  -  -  C  -  -
04 1-0   D  D  -  -  D  -
05 1-0   -  -  -  -  -  -
06 1-0   -  -  F  -  F  F
07 1-0   G  -  G  -  G  -
08 1-0   -  -  H  H  H  H
09 2-0   -  I  I  I  -  -
10 0-3   J  -  J  -  J  J
11 0-3   K  K  -  K  -  K
12 0-3   L  -  L  L  -  L
13 0-3   M  M  -  -  M  -
14 0-3   N  -  N  -  -  -
15 0-3   -  O  -  O  O  -
16 0-4   P  -  P  -  P  -
17 0-4   Q  Q  Q  -  Q  Q
18 0-5   -  -  -  -  -   
19 0-5   S  -  -  S  -   
20 1-3   T  T  T  T      
21 1-3   -  U  -  -      
22 1-3   V  V  V         
23 1-3   -  X  -         
24 1-6   X  Y            
25 2-4   -  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

VQPWQ FYEVB QHMDS NPZLP TVQAQ N
-------------------------------
