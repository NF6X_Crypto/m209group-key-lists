EFFECTIVE PERIOD:
18-DEC-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 2-0   -  -  B  B  -  -
03 2-0   C  -  -  C  C  C
04 2-0   -  D  D  D  -  -
05 2-0   -  E  E  E  E  -
06 0-3   F  F  -  -  -  -
07 0-3   -  G  G  G  G  -
08 0-3   -  H  -  -  -  H
09 0-3   -  -  -  -  -  -
10 0-5   -  J  J  -  J  J
11 0-5   K  K  -  -  K  K
12 0-5   -  -  L  -  -  L
13 0-5   M  M  -  -  M  -
14 0-5   -  N  -  N  -  N
15 0-5   -  -  O  O  O  O
16 1-2   -  -  P  P  P  -
17 1-2   Q  Q  -  -  Q  Q
18 1-2   R  -  -  -  -   
19 1-4   S  -  S  -  S   
20 1-5   T  -  T  -      
21 2-3   -  U  -  U      
22 2-3   V  -  -         
23 2-3   -  -  X         
24 2-3   X  Y            
25 2-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AWUON PUHHS OLKQY NUUUI UUKWZ I
-------------------------------
