EFFECTIVE PERIOD:
26-DEC-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   B  -  -  -  -  B
03 1-0   -  -  C  -  -  -
04 1-0   D  D  -  -  -  D
05 2-0   -  E  -  E  E  E
06 2-0   F  -  -  -  F  -
07 2-0   -  G  G  G  -  -
08 0-3   H  -  H  -  H  H
09 0-5   I  -  -  -  I  -
10 0-5   -  -  -  J  -  J
11 0-5   K  K  -  K  -  -
12 0-5   L  L  L  -  L  -
13 0-5   -  -  -  M  M  M
14 0-6   N  N  N  N  N  -
15 0-6   -  -  O  O  -  O
16 0-6   -  -  -  -  P  P
17 0-6   -  Q  -  -  -  -
18 0-6   -  R  -  R  -   
19 0-6   S  -  S  S  S   
20 1-3   -  T  T  T      
21 1-5   U  U  -  U      
22 2-5   -  -  V         
23 2-6   -  X  -         
24 2-6   X  -            
25 2-6   Y  Z            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

JJUNR VKRAF VQMHU OXJGW GAUJV K
-------------------------------
