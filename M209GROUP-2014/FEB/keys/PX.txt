EFFECTIVE PERIOD:
08-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   B  -  -  -  -  B
03 2-0   C  C  -  C  C  C
04 2-0   D  D  -  -  -  -
05 2-0   -  E  -  -  E  -
06 0-4   F  F  F  F  F  -
07 0-4   G  -  G  -  G  G
08 0-4   -  -  -  H  -  -
09 0-4   -  -  -  -  I  -
10 0-4   -  -  -  J  J  J
11 0-4   -  -  K  -  -  K
12 0-6   -  L  L  -  -  L
13 0-6   M  M  M  -  M  M
14 0-6   N  N  -  N  -  -
15 0-6   O  -  O  O  -  O
16 1-6   -  -  P  P  -  P
17 2-4   Q  Q  -  Q  -  Q
18 2-5   R  -  R  -  R   
19 2-6   S  S  -  S  S   
20 3-5   T  T  T  -      
21 3-6   -  -  -  U      
22 4-6   V  -  V         
23 4-6   W  X  -         
24 4-6   -  Y            
25 4-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MVALA TVEAT LREUV JAOTM ENAZU V
-------------------------------
