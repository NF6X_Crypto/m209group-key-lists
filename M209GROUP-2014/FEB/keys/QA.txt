EFFECTIVE PERIOD:
11-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  A
02 2-0   B  -  -  B  B  B
03 2-0   C  -  C  -  -  -
04 2-0   -  D  D  D  -  -
05 2-0   -  -  -  -  -  -
06 2-0   F  -  -  -  F  -
07 0-3   -  -  G  G  -  G
08 0-3   H  H  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-3   J  J  -  J  J  J
11 0-3   K  K  K  K  K  -
12 0-3   -  -  L  L  -  L
13 0-3   -  -  M  -  M  M
14 0-4   -  N  N  -  N  -
15 0-6   O  -  -  -  -  -
16 0-6   -  P  -  P  P  P
17 0-6   Q  -  -  -  -  -
18 1-5   R  R  -  -  -   
19 2-4   S  S  S  -  S   
20 2-6   T  -  T  T      
21 3-4   -  U  -  -      
22 3-5   V  V  V         
23 3-6   -  X  -         
24 3-6   X  Y            
25 3-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MRKLK QVTYK WHQSO DSQAR XWQOR I
-------------------------------
