EFFECTIVE PERIOD:
12-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  -  D  D  D
05 1-0   E  -  E  -  -  E
06 1-0   F  F  F  F  F  F
07 1-0   -  -  G  G  -  -
08 1-0   H  H  H  -  -  -
09 1-0   -  -  -  I  I  I
10 1-0   -  -  -  -  -  -
11 1-0   -  -  K  K  -  -
12 2-0   L  -  -  L  L  -
13 0-3   M  M  M  -  M  -
14 0-4   N  N  -  N  -  -
15 0-5   -  -  -  O  O  -
16 0-5   -  P  -  -  -  P
17 0-5   Q  Q  -  Q  -  -
18 0-5   -  R  R  R  R   
19 0-5   -  -  -  -  S   
20 0-6   -  -  T  T      
21 0-6   -  -  U  -      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 1-5   -  Y            
25 3-4   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GUCHD HUPDU ZNURE UVDMI KZZOS I
-------------------------------
