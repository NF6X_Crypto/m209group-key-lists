EFFECTIVE PERIOD:
25-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   B  B  B  B  B  -
03 2-0   C  C  -  -  -  C
04 2-0   -  D  -  D  -  D
05 2-0   E  E  -  -  E  -
06 0-4   F  F  F  -  F  -
07 0-4   G  -  -  G  -  G
08 0-4   -  -  -  H  -  -
09 0-4   I  I  -  I  I  -
10 0-4   J  J  -  J  -  J
11 0-5   K  -  -  -  K  -
12 0-5   -  -  -  -  L  L
13 0-6   -  M  M  M  -  M
14 0-6   -  N  -  -  -  -
15 0-6   -  O  O  O  O  -
16 0-6   -  -  P  -  -  P
17 0-6   Q  Q  -  Q  -  -
18 0-6   -  -  R  -  -   
19 0-6   S  S  -  -  -   
20 1-5   -  -  T  -      
21 2-4   -  U  U  -      
22 2-5   V  -  V         
23 3-6   -  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SMXDQ LILEV SASVG RAKVV HKPPS Q
-------------------------------
