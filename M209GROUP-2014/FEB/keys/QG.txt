EFFECTIVE PERIOD:
17-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  B  B  -  -
03 0-3   -  C  C  -  C  C
04 0-4   -  -  -  -  -  -
05 0-4   -  E  E  -  -  E
06 0-4   F  F  F  -  F  F
07 0-4   -  G  -  G  -  G
08 0-4   -  -  -  H  H  -
09 0-4   -  I  -  -  I  I
10 0-4   J  -  J  J  J  -
11 0-4   K  K  -  K  -  -
12 0-4   L  L  L  L  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  -  N  -  -  N
15 0-5   -  -  -  O  O  -
16 0-5   P  P  -  P  P  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  -  R  R  -   
19 0-6   -  -  -  -  -   
20 1-3   T  T  T  T      
21 1-5   -  -  U  -      
22 2-3   -  V  -         
23 4-6   W  X  -         
24 4-6   -  -            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SAMYK TKWAH VKNNC YWGMP USKAY Z
-------------------------------
