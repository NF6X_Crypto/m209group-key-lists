SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF FEB 2014

    01 FEB 2014  00:00-23:59 GMT:  USE KEY PQ
    02 FEB 2014  00:00-23:59 GMT:  USE KEY PR
    03 FEB 2014  00:00-23:59 GMT:  USE KEY PS
    04 FEB 2014  00:00-23:59 GMT:  USE KEY PT
    05 FEB 2014  00:00-23:59 GMT:  USE KEY PU
    06 FEB 2014  00:00-23:59 GMT:  USE KEY PV
    07 FEB 2014  00:00-23:59 GMT:  USE KEY PW
    08 FEB 2014  00:00-23:59 GMT:  USE KEY PX
    09 FEB 2014  00:00-23:59 GMT:  USE KEY PY
    10 FEB 2014  00:00-23:59 GMT:  USE KEY PZ
    11 FEB 2014  00:00-23:59 GMT:  USE KEY QA
    12 FEB 2014  00:00-23:59 GMT:  USE KEY QB
    13 FEB 2014  00:00-23:59 GMT:  USE KEY QC
    14 FEB 2014  00:00-23:59 GMT:  USE KEY QD
    15 FEB 2014  00:00-23:59 GMT:  USE KEY QE
    16 FEB 2014  00:00-23:59 GMT:  USE KEY QF
    17 FEB 2014  00:00-23:59 GMT:  USE KEY QG
    18 FEB 2014  00:00-23:59 GMT:  USE KEY QH
    19 FEB 2014  00:00-23:59 GMT:  USE KEY QI
    20 FEB 2014  00:00-23:59 GMT:  USE KEY QJ
    21 FEB 2014  00:00-23:59 GMT:  USE KEY QK
    22 FEB 2014  00:00-23:59 GMT:  USE KEY QL
    23 FEB 2014  00:00-23:59 GMT:  USE KEY QM
    24 FEB 2014  00:00-23:59 GMT:  USE KEY QN
    25 FEB 2014  00:00-23:59 GMT:  USE KEY QO
    26 FEB 2014  00:00-23:59 GMT:  USE KEY QP
    27 FEB 2014  00:00-23:59 GMT:  USE KEY QQ
    28 FEB 2014  00:00-23:59 GMT:  USE KEY QR

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  -  B  B  -
03 1-0   C  C  C  -  C  -
04 1-0   D  D  -  -  -  D
05 1-0   -  -  E  -  E  E
06 1-0   F  F  F  F  F  -
07 1-0   G  G  G  G  -  -
08 2-0   -  -  -  H  -  -
09 2-0   I  I  -  I  I  -
10 2-0   -  J  -  -  J  J
11 2-0   K  K  K  -  -  -
12 2-0   -  L  L  -  L  L
13 0-3   M  M  -  M  -  -
14 0-3   N  -  -  -  -  N
15 0-3   -  O  -  O  O  O
16 0-4   -  P  -  -  -  P
17 0-4   -  -  Q  -  -  Q
18 0-4   -  R  R  R  R   
19 0-4   -  S  -  S  S   
20 0-5   T  -  -  -      
21 0-6   U  -  U  U      
22 1-2   -  V  V         
23 1-2   -  -  -         
24 1-2   X  -            
25 2-4   -  -            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

PJFPV QIPTB VAPRY JAWAR OZLVK K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  -  A  A
02 0-3   B  B  B  B  -  B
03 0-3   C  -  C  -  C  C
04 0-3   D  -  -  -  D  D
05 0-4   E  -  -  E  -  E
06 0-4   -  -  -  -  F  F
07 0-4   G  G  G  G  -  -
08 0-4   -  -  H  H  H  H
09 0-4   -  I  I  -  I  -
10 0-4   J  J  J  J  J  J
11 0-4   K  K  -  -  -  K
12 0-5   -  L  L  L  -  L
13 0-5   M  M  M  -  M  -
14 0-5   -  -  N  -  N  -
15 0-5   -  O  O  -  O  -
16 1-3   P  -  -  -  -  -
17 2-3   -  Q  Q  Q  -  -
18 2-6   -  R  -  R  -   
19 3-4   -  -  -  S  S   
20 3-4   T  T  T  T      
21 3-4   U  -  U  U      
22 3-4   V  -  -         
23 3-5   W  X  -         
24 3-5   -  Y            
25 3-6   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OTJTM OUAVE VTOTD TATWN VTTNO P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  -  -  B  B  B
03 2-0   -  -  C  -  -  -
04 2-0   D  -  -  -  D  -
05 2-0   E  E  E  E  E  E
06 0-3   -  F  -  F  F  F
07 0-3   G  -  G  -  -  -
08 0-3   -  -  H  H  -  H
09 0-4   I  -  I  -  I  -
10 0-4   -  -  -  J  -  -
11 0-4   K  -  -  K  -  -
12 0-4   L  L  L  L  -  L
13 0-4   -  M  -  -  -  M
14 0-4   -  N  -  N  N  N
15 0-6   -  O  O  O  -  O
16 0-6   P  -  -  -  P  P
17 0-6   -  -  -  -  -  Q
18 0-6   -  -  R  R  -   
19 0-6   -  S  -  S  -   
20 1-3   -  T  T  -      
21 2-3   -  U  U  U      
22 2-6   V  -  -         
23 4-5   -  X  X         
24 4-6   -  -            
25 4-6   Y  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

HVQJT VAEVG QPQKW HUFVS QQXVJ O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  -
02 0-3   -  -  -  -  B  B
03 0-3   C  C  -  -  C  -
04 0-3   D  -  -  D  D  -
05 0-3   -  E  E  -  -  -
06 0-3   F  -  F  F  F  -
07 0-3   -  G  G  G  -  -
08 0-3   -  H  -  -  H  H
09 0-3   -  I  I  I  I  I
10 0-3   -  J  -  J  -  J
11 0-3   K  K  -  K  -  -
12 0-3   -  -  L  L  -  L
13 0-3   -  -  -  M  -  M
14 0-5   -  -  N  -  N  -
15 0-5   O  -  O  -  O  -
16 0-5   -  P  P  -  -  P
17 0-5   Q  -  -  -  -  Q
18 0-6   R  -  R  -  -   
19 0-6   S  S  -  S  S   
20 0-6   -  T  T  T      
21 0-6   -  -  -  U      
22 0-6   V  -  -         
23 0-6   W  -  X         
24 0-6   -  Y            
25 0-6   Y  Z            
26 1-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

NVZFM IVFNS RFBGV AZCWK CSMTL U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  -  -  B  -  -
03 1-0   C  C  C  C  C  -
04 0-4   D  -  D  -  D  -
05 0-4   -  -  E  E  E  E
06 0-4   F  -  F  F  F  F
07 0-4   -  -  -  G  G  -
08 0-4   -  H  H  -  -  H
09 0-4   -  -  I  -  -  I
10 0-4   J  J  J  J  J  J
11 0-5   -  -  -  K  K  K
12 0-5   L  L  -  L  -  L
13 0-5   M  -  M  M  -  -
14 0-5   N  N  -  N  -  N
15 0-6   O  -  -  -  -  -
16 0-6   P  -  -  -  -  -
17 0-6   Q  -  -  Q  -  Q
18 1-4   -  -  R  -  R   
19 1-4   -  -  -  -  -   
20 1-4   T  T  T  -      
21 1-4   -  U  U  -      
22 1-5   V  V  -         
23 2-3   W  X  -         
24 3-4   -  -            
25 3-4   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VVZES ATLGH SXMXA SMMXO WMSMI V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  B  -  B  B  B
03 1-0   -  C  C  -  C  C
04 1-0   -  -  D  D  -  -
05 1-0   E  E  -  E  E  E
06 1-0   -  -  F  -  F  -
07 0-3   -  -  -  G  -  -
08 0-3   H  -  H  H  H  H
09 0-3   I  I  -  -  I  I
10 0-3   J  -  -  -  J  J
11 0-4   K  K  K  K  -  -
12 0-4   L  -  -  -  -  L
13 0-4   -  -  -  M  -  M
14 0-4   N  -  -  N  -  -
15 0-4   -  O  O  O  O  -
16 0-4   -  -  -  -  -  -
17 0-4   -  Q  Q  -  -  -
18 1-3   -  R  R  R  R   
19 1-3   S  -  -  S  -   
20 1-4   -  T  T  -      
21 1-4   U  U  U  -      
22 1-4   -  -  V         
23 1-4   -  -  -         
24 1-5   X  Y            
25 2-6   -  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

MTNTH VUITO NBAUB AOIOW SVULT C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  -  B  B  -
03 1-0   -  C  C  C  C  C
04 1-0   -  -  -  -  -  -
05 1-0   -  E  E  -  -  -
06 2-0   F  F  -  -  F  F
07 2-0   -  -  -  G  -  G
08 2-0   H  -  H  H  H  -
09 2-0   I  -  -  -  -  -
10 2-0   -  J  J  -  J  -
11 0-3   K  K  -  -  K  K
12 0-3   -  -  -  L  -  L
13 0-3   -  M  M  M  M  M
14 0-3   N  N  -  -  N  -
15 0-3   -  O  -  O  -  O
16 0-3   P  -  -  -  -  -
17 0-3   Q  -  -  Q  Q  -
18 1-2   R  -  -  -  R   
19 1-2   S  S  S  S  -   
20 1-2   -  T  T  T      
21 1-2   -  -  U  U      
22 1-3   V  -  V         
23 2-5   -  X  -         
24 2-5   X  -            
25 2-6   -  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SOOJM VUJAE LSSNI NSMOS NTIMT N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   B  -  -  -  -  B
03 2-0   C  C  -  C  C  C
04 2-0   D  D  -  -  -  -
05 2-0   -  E  -  -  E  -
06 0-4   F  F  F  F  F  -
07 0-4   G  -  G  -  G  G
08 0-4   -  -  -  H  -  -
09 0-4   -  -  -  -  I  -
10 0-4   -  -  -  J  J  J
11 0-4   -  -  K  -  -  K
12 0-6   -  L  L  -  -  L
13 0-6   M  M  M  -  M  M
14 0-6   N  N  -  N  -  -
15 0-6   O  -  O  O  -  O
16 1-6   -  -  P  P  -  P
17 2-4   Q  Q  -  Q  -  Q
18 2-5   R  -  R  -  R   
19 2-6   S  S  -  S  S   
20 3-5   T  T  T  -      
21 3-6   -  -  -  U      
22 4-6   V  -  V         
23 4-6   W  X  -         
24 4-6   -  Y            
25 4-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MVALA TVEAT LREUV JAOTM ENAZU V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   -  -  -  -  -  -
03 2-0   C  -  C  -  C  C
04 2-0   D  D  D  -  -  -
05 2-0   E  -  E  E  -  -
06 2-0   -  -  -  -  F  -
07 2-0   -  -  -  -  G  -
08 0-3   -  H  -  -  H  H
09 0-3   I  I  I  I  -  I
10 0-3   J  J  J  J  J  -
11 0-3   K  -  -  K  K  K
12 0-3   -  L  -  L  L  -
13 0-6   -  M  -  -  -  M
14 0-6   -  -  N  -  -  -
15 0-6   -  -  O  O  O  O
16 0-6   P  P  -  P  P  -
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   R  -  R  -  R   
19 0-6   S  -  -  S  -   
20 1-5   T  -  -  T      
21 2-3   U  -  -  -      
22 2-6   V  V  -         
23 2-6   -  -  X         
24 2-6   -  Y            
25 2-6   Y  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UQUJU COLZA SKGJA OZNSA NNMZI C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   B  B  B  B  B  -
03 1-0   -  -  -  C  -  -
04 1-0   D  D  D  D  -  D
05 2-0   -  -  E  -  E  -
06 2-0   -  F  F  -  -  F
07 2-0   G  G  -  -  -  -
08 2-0   -  -  -  H  -  H
09 0-3   -  -  -  -  I  I
10 0-3   J  -  -  J  J  J
11 0-3   K  -  -  -  K  -
12 0-3   L  L  L  -  L  L
13 0-3   M  M  M  M  -  -
14 0-3   N  -  -  N  -  -
15 0-3   -  -  O  O  O  O
16 0-3   -  P  P  P  P  P
17 0-4   -  -  Q  -  -  Q
18 0-4   -  -  -  -  -   
19 0-4   S  -  S  S  -   
20 0-4   -  T  -  T      
21 0-6   U  U  U  -      
22 1-2   -  -  V         
23 1-3   W  -  -         
24 1-3   X  -            
25 2-4   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

GWPVC WMWON MYSJW MMMHA GWASW J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  A
02 2-0   B  -  -  B  B  B
03 2-0   C  -  C  -  -  -
04 2-0   -  D  D  D  -  -
05 2-0   -  -  -  -  -  -
06 2-0   F  -  -  -  F  -
07 0-3   -  -  G  G  -  G
08 0-3   H  H  H  -  -  H
09 0-3   -  I  I  I  I  -
10 0-3   J  J  -  J  J  J
11 0-3   K  K  K  K  K  -
12 0-3   -  -  L  L  -  L
13 0-3   -  -  M  -  M  M
14 0-4   -  N  N  -  N  -
15 0-6   O  -  -  -  -  -
16 0-6   -  P  -  P  P  P
17 0-6   Q  -  -  -  -  -
18 1-5   R  R  -  -  -   
19 2-4   S  S  S  -  S   
20 2-6   T  -  T  T      
21 3-4   -  U  -  -      
22 3-5   V  V  V         
23 3-6   -  X  -         
24 3-6   X  Y            
25 3-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MRKLK QVTYK WHQSO DSQAR XWQOR I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  -  D  D  D
05 1-0   E  -  E  -  -  E
06 1-0   F  F  F  F  F  F
07 1-0   -  -  G  G  -  -
08 1-0   H  H  H  -  -  -
09 1-0   -  -  -  I  I  I
10 1-0   -  -  -  -  -  -
11 1-0   -  -  K  K  -  -
12 2-0   L  -  -  L  L  -
13 0-3   M  M  M  -  M  -
14 0-4   N  N  -  N  -  -
15 0-5   -  -  -  O  O  -
16 0-5   -  P  -  -  -  P
17 0-5   Q  Q  -  Q  -  -
18 0-5   -  R  R  R  R   
19 0-5   -  -  -  -  S   
20 0-6   -  -  T  T      
21 0-6   -  -  U  -      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 1-5   -  Y            
25 3-4   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GUCHD HUPDU ZNURE UVDMI KZZOS I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  -  -  B  B  B
03 2-0   -  C  -  -  -  C
04 2-0   -  -  -  D  D  D
05 2-0   E  -  -  E  E  -
06 2-0   -  F  -  F  F  F
07 0-3   -  -  G  G  G  -
08 0-3   H  H  H  -  H  H
09 0-5   I  I  -  I  I  I
10 0-5   -  -  J  -  -  J
11 0-5   -  K  K  -  K  -
12 0-5   L  L  L  L  L  -
13 0-5   -  M  -  -  -  -
14 0-5   -  N  -  N  N  N
15 0-5   O  -  O  O  -  O
16 1-2   P  P  P  P  -  -
17 1-2   Q  Q  Q  Q  -  -
18 1-2   R  -  R  R  -   
19 1-2   S  -  -  -  S   
20 1-3   -  -  -  -      
21 1-3   -  -  U  -      
22 1-4   V  -  -         
23 1-4   -  X  X         
24 2-5   -  -            
25 3-4   Y  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

STAOI DJOJW LSUUM UWIWZ IWQMR O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  -
02 2-0   -  B  B  B  B  -
03 2-0   C  C  C  -  C  C
04 2-0   -  D  D  D  D  -
05 2-0   -  E  -  -  E  -
06 2-0   F  F  -  -  F  F
07 2-0   -  -  -  G  -  -
08 2-0   -  -  H  -  -  -
09 0-3   -  I  I  -  I  -
10 0-3   -  J  J  -  -  J
11 0-4   K  K  -  K  -  K
12 0-4   L  L  L  L  L  L
13 0-4   -  -  M  -  -  M
14 0-4   N  -  N  N  -  N
15 0-4   -  O  O  -  O  -
16 0-4   P  P  P  P  -  -
17 0-4   Q  -  -  Q  -  Q
18 0-4   R  -  R  R  R   
19 0-4   -  -  S  -  -   
20 0-4   T  -  -  T      
21 0-4   -  U  -  U      
22 0-5   -  V  -         
23 0-6   -  -  -         
24 1-6   -  -            
25 2-3   Y  -            
26 2-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PZFPQ PNUMP MOAOJ UNFFA XNZFQ Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  B  -  B  B  B
03 2-0   C  -  C  -  -  C
04 2-0   -  -  -  -  -  -
05 2-0   E  -  E  E  E  E
06 2-0   -  F  -  F  -  F
07 0-3   -  G  -  G  -  -
08 0-3   -  H  -  -  H  H
09 0-3   I  I  I  -  -  -
10 0-3   J  -  -  J  -  J
11 0-3   K  -  -  -  K  K
12 0-6   -  -  -  L  -  -
13 0-6   M  M  M  M  M  -
14 0-6   -  -  N  -  N  -
15 0-6   O  -  O  -  -  O
16 0-6   -  P  P  -  -  -
17 0-6   Q  -  -  -  Q  Q
18 1-2   -  -  -  R  R   
19 1-5   -  S  -  S  -   
20 1-6   T  -  T  T      
21 2-3   -  U  -  -      
22 3-6   V  V  -         
23 3-6   -  -  X         
24 3-6   X  Y            
25 3-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

STLZV CASQA PQYFS GSSRJ GQJAT W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   -  B  -  B  -  B
03 0-3   C  C  C  -  -  -
04 0-3   D  -  D  D  D  D
05 0-3   E  -  E  -  -  -
06 0-3   -  F  -  -  F  F
07 0-3   G  -  G  G  -  -
08 0-4   -  H  H  H  H  -
09 0-4   -  I  -  -  I  I
10 0-4   J  J  J  -  -  J
11 0-6   -  -  -  K  -  -
12 0-6   -  -  -  L  -  L
13 0-6   M  -  -  -  M  -
14 0-6   -  N  N  -  N  -
15 0-6   O  -  -  O  O  O
16 0-6   P  P  P  P  -  P
17 0-6   Q  -  -  -  Q  -
18 1-2   R  -  R  R  R   
19 1-5   -  S  -  S  -   
20 1-6   -  -  T  -      
21 1-6   -  U  -  U      
22 2-3   -  V  -         
23 3-4   W  X  -         
24 4-6   -  Y            
25 4-6   Y  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KRRIW RRGUG YEZMK SAKQQ ONNRM V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  B  B  -  -
03 0-3   -  C  C  -  C  C
04 0-4   -  -  -  -  -  -
05 0-4   -  E  E  -  -  E
06 0-4   F  F  F  -  F  F
07 0-4   -  G  -  G  -  G
08 0-4   -  -  -  H  H  -
09 0-4   -  I  -  -  I  I
10 0-4   J  -  J  J  J  -
11 0-4   K  K  -  K  -  -
12 0-4   L  L  L  L  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  -  N  -  -  N
15 0-5   -  -  -  O  O  -
16 0-5   P  P  -  P  P  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  -  R  R  -   
19 0-6   -  -  -  -  -   
20 1-3   T  T  T  T      
21 1-5   -  -  U  -      
22 2-3   -  V  -         
23 4-6   W  X  -         
24 4-6   -  -            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SAMYK TKWAH VKNNC YWGMP USKAY Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   -  B  -  -  -  -
03 1-0   -  -  -  C  C  C
04 1-0   D  -  D  -  -  D
05 1-0   -  -  E  E  E  -
06 1-0   F  F  F  -  F  -
07 1-0   -  -  G  G  -  G
08 2-0   H  -  -  H  H  -
09 0-3   -  I  I  I  -  -
10 0-3   J  J  J  J  -  J
11 0-3   -  -  -  -  -  -
12 0-3   -  -  L  L  L  -
13 0-3   M  -  -  M  M  -
14 0-3   N  N  -  N  -  N
15 0-3   O  -  O  -  -  O
16 0-3   P  -  P  -  -  P
17 0-3   -  -  Q  -  -  -
18 0-3   R  R  -  R  -   
19 0-3   S  -  S  S  S   
20 0-3   -  T  T  -      
21 0-5   U  U  -  -      
22 0-5   -  V  -         
23 0-5   W  -  -         
24 0-5   -  Y            
25 0-6   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MRSVA WONED MCGSS RWBUS OOCGU S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  B  -  -  -  -
03 1-0   C  C  -  C  -  C
04 1-0   D  -  -  D  D  D
05 1-0   -  -  -  -  E  E
06 1-0   -  -  F  F  F  F
07 2-0   -  G  G  -  -  G
08 0-4   H  -  -  -  -  H
09 0-4   I  I  -  -  -  -
10 0-4   J  J  -  -  -  -
11 0-4   -  K  K  -  K  K
12 0-6   -  -  L  -  L  -
13 0-6   M  M  -  M  M  M
14 0-6   N  N  N  N  N  -
15 0-6   -  -  O  O  -  O
16 0-6   P  -  P  -  -  -
17 0-6   Q  -  -  Q  Q  -
18 1-2   R  R  R  -  R   
19 1-3   S  -  S  -  S   
20 1-4   -  T  -  -      
21 1-4   U  U  -  U      
22 1-4   V  V  -         
23 1-4   -  X  X         
24 2-3   -  -            
25 2-6   Y  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZSZOS PPENZ KATWX CCKMW PUIUP L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  -  B  B  B
03 1-0   C  -  -  C  -  C
04 1-0   -  D  D  -  -  -
05 1-0   -  E  -  E  -  E
06 1-0   F  F  -  F  -  -
07 0-3   -  -  G  -  G  G
08 0-3   H  H  H  H  H  -
09 0-3   I  I  I  -  -  -
10 0-3   J  -  J  J  -  -
11 0-3   K  K  -  -  -  -
12 0-4   -  L  L  -  -  -
13 0-4   -  M  -  -  M  M
14 0-4   -  -  -  -  N  N
15 0-4   -  O  O  O  -  -
16 1-3   P  P  -  P  P  -
17 1-4   -  -  Q  Q  -  -
18 1-4   R  -  -  -  R   
19 1-4   -  S  -  S  -   
20 1-4   T  -  -  -      
21 2-3   -  -  U  -      
22 2-4   -  V  V         
23 2-4   W  -  -         
24 2-6   X  Y            
25 3-4   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OLUUV ZMWUF YNDAO EZHZW NVDOU O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   B  -  -  -  -  -
03 1-0   C  -  -  C  -  -
04 1-0   -  -  D  D  -  D
05 2-0   E  E  E  E  -  -
06 0-3   F  -  -  -  F  F
07 0-3   G  G  G  G  G  -
08 0-3   H  H  -  -  H  H
09 0-3   -  I  -  I  -  -
10 0-3   -  -  -  -  J  J
11 0-4   -  K  -  -  K  K
12 0-4   L  -  L  -  L  -
13 0-4   M  -  M  M  M  -
14 0-4   -  N  -  -  -  -
15 0-6   -  -  O  -  -  O
16 0-6   -  P  -  -  -  -
17 0-6   -  -  Q  Q  -  Q
18 0-6   -  -  R  -  -   
19 0-6   -  -  S  S  -   
20 1-3   T  T  T  -      
21 1-6   -  U  -  U      
22 1-6   -  V  V         
23 1-6   -  X  -         
24 1-6   X  -            
25 2-4   -  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PJZGZ IWBVF UGUUQ QUVLC AWOOI F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  B  B  -  -
03 1-0   C  -  C  -  C  C
04 1-0   D  D  D  -  D  D
05 2-0   E  E  E  E  E  -
06 2-0   -  F  -  F  -  F
07 2-0   -  -  -  -  G  G
08 2-0   -  -  H  -  H  -
09 2-0   I  I  -  -  I  I
10 2-0   -  J  -  J  -  J
11 2-0   -  -  K  -  -  K
12 0-4   -  L  -  L  -  -
13 0-4   -  -  M  M  -  -
14 0-4   N  N  -  N  N  N
15 0-4   O  -  O  -  -  -
16 0-4   -  -  P  -  -  -
17 0-6   Q  Q  Q  Q  -  -
18 1-3   R  R  -  -  -   
19 1-4   S  S  -  S  S   
20 1-4   T  T  -  T      
21 1-4   -  U  U  U      
22 1-4   V  -  V         
23 1-5   W  X  -         
24 1-6   -  -            
25 2-4   Y  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

NRUVY IYRUN OCNTZ VMEJT VKTZU R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  A
02 2-0   B  -  B  B  -  B
03 2-0   -  C  -  -  -  C
04 2-0   -  D  D  D  -  -
05 0-3   E  E  E  E  E  E
06 0-3   -  -  F  F  -  -
07 0-3   -  G  G  G  -  G
08 0-3   H  -  -  -  H  -
09 0-3   I  I  I  -  -  I
10 0-3   -  J  -  -  -  J
11 0-4   -  K  -  -  K  K
12 0-5   -  -  L  L  L  -
13 0-5   -  -  -  M  -  M
14 0-5   -  -  N  N  -  -
15 0-6   -  O  -  O  O  -
16 0-6   P  -  -  -  P  P
17 0-6   Q  -  Q  Q  -  -
18 0-6   -  -  -  -  R   
19 0-6   S  -  -  S  -   
20 0-6   T  T  -  -      
21 0-6   U  U  -  -      
22 0-6   V  -  -         
23 0-6   -  -  X         
24 1-5   X  Y            
25 2-3   Y  Z            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ABKMF QKJOX GRFWN AQPMT GNPNJ U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 2-0   B  -  B  B  -  B
03 2-0   C  C  -  -  -  -
04 0-3   -  D  D  -  D  D
05 0-3   E  -  -  E  E  E
06 0-3   -  -  -  -  -  F
07 0-3   -  -  G  G  G  G
08 0-4   -  H  H  H  H  -
09 0-4   I  -  -  I  I  -
10 0-4   J  J  -  J  -  -
11 0-4   K  K  K  K  -  K
12 0-4   -  -  L  L  -  L
13 0-4   -  -  M  -  -  -
14 0-4   -  -  N  N  N  -
15 0-4   O  O  -  -  O  -
16 0-4   P  P  P  -  P  -
17 0-4   Q  -  Q  -  Q  Q
18 0-6   -  R  R  R  R   
19 0-6   -  -  S  S  -   
20 0-6   T  T  T  T      
21 0-6   -  -  -  -      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 2-3   X  -            
25 2-5   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QWGIH TYWMA ROJAD MRWVL UJRNI Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   B  B  B  B  B  -
03 2-0   C  C  -  -  -  C
04 2-0   -  D  -  D  -  D
05 2-0   E  E  -  -  E  -
06 0-4   F  F  F  -  F  -
07 0-4   G  -  -  G  -  G
08 0-4   -  -  -  H  -  -
09 0-4   I  I  -  I  I  -
10 0-4   J  J  -  J  -  J
11 0-5   K  -  -  -  K  -
12 0-5   -  -  -  -  L  L
13 0-6   -  M  M  M  -  M
14 0-6   -  N  -  -  -  -
15 0-6   -  O  O  O  O  -
16 0-6   -  -  P  -  -  P
17 0-6   Q  Q  -  Q  -  -
18 0-6   -  -  R  -  -   
19 0-6   S  S  -  -  -   
20 1-5   -  -  T  -      
21 2-4   -  U  U  -      
22 2-5   V  -  V         
23 3-6   -  -  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SMXDQ LILEV SASVG RAKVV HKPPS Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   B  -  -  B  -  B
03 2-0   -  C  -  -  -  C
04 2-0   -  -  D  D  -  -
05 2-0   E  -  -  -  E  E
06 2-0   F  F  F  -  F  -
07 2-0   G  -  -  G  G  -
08 0-3   -  H  H  -  -  H
09 0-3   I  -  -  -  -  -
10 0-3   -  J  -  J  J  -
11 0-4   -  -  K  -  -  K
12 0-4   L  -  L  L  L  -
13 0-4   M  M  -  -  M  M
14 0-4   N  -  -  N  N  -
15 0-4   O  O  O  O  -  -
16 0-4   -  P  -  -  P  -
17 0-4   Q  Q  -  Q  -  Q
18 0-4   R  -  -  -  -   
19 0-4   S  S  S  S  S   
20 0-4   T  T  -  T      
21 0-4   -  -  U  -      
22 0-4   -  V  V         
23 0-6   W  -  -         
24 1-3   -  -            
25 1-6   Y  Z            
26 2-3   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

IZXDW JOZHF VIQKC TJJRF WFPXH X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  B  -  B  -  B
03 1-0   C  -  C  -  C  -
04 1-0   -  -  -  D  -  -
05 1-0   -  E  E  -  -  E
06 1-0   F  F  F  -  F  -
07 1-0   G  -  -  G  G  G
08 0-3   H  -  H  -  -  H
09 0-3   I  I  I  -  -  -
10 0-3   -  J  -  J  -  -
11 0-3   K  -  K  -  K  -
12 0-3   L  L  L  L  L  -
13 0-3   M  -  -  M  M  M
14 0-4   N  -  N  -  N  N
15 0-4   -  -  O  -  O  -
16 0-4   -  P  P  P  -  P
17 0-4   -  -  -  Q  Q  -
18 0-4   R  R  -  R  -   
19 0-4   S  S  -  S  S   
20 1-3   -  -  -  -      
21 1-3   U  U  U  -      
22 1-3   -  V  -         
23 1-3   W  X  X         
24 1-5   -  -            
25 2-6   Y  Z            
26 3-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SMUUI UITCZ ABSNN SMSZN TZNSZ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-FEB-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 2-0   -  -  -  B  -  -
03 0-3   C  C  C  -  -  -
04 0-3   D  D  -  D  D  D
05 0-3   -  E  -  E  -  -
06 0-3   -  -  F  -  -  -
07 0-3   -  G  -  -  -  G
08 0-3   H  H  H  H  H  H
09 0-3   -  -  -  -  I  -
10 0-3   -  -  J  J  -  -
11 0-3   -  K  -  -  K  K
12 0-3   -  -  -  -  -  L
13 0-4   -  -  M  -  M  -
14 0-4   N  N  -  N  -  N
15 0-4   O  -  -  -  O  O
16 0-4   P  P  P  P  -  P
17 0-5   Q  -  Q  Q  Q  -
18 0-5   R  -  R  R  R   
19 0-5   -  S  -  S  S   
20 0-5   -  -  T  T      
21 0-5   U  -  -  U      
22 0-5   -  V  V         
23 0-6   -  X  -         
24 2-4   X  Y            
25 2-6   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

CQLYT PWOTD TLQJE UMOQK JDYZP X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
