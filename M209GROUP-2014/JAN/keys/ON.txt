EFFECTIVE PERIOD:
03-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ON
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  C  -  -
04 1-0   D  -  -  D  D  D
05 2-0   E  E  E  -  E  -
06 2-0   -  -  F  -  -  F
07 2-0   G  G  G  -  -  G
08 2-0   H  -  -  -  -  -
09 2-0   I  I  I  -  I  -
10 2-0   J  -  J  -  J  J
11 0-4   -  -  K  K  -  K
12 0-4   L  -  L  L  -  -
13 0-4   M  M  -  M  M  -
14 0-6   -  N  N  -  N  N
15 0-6   -  -  -  -  -  -
16 1-2   P  -  -  P  -  -
17 1-4   -  Q  Q  -  -  Q
18 1-4   -  R  -  R  -   
19 1-4   S  S  -  S  S   
20 1-4   T  T  -  T      
21 2-6   U  U  -  -      
22 3-4   V  V  -         
23 4-5   -  -  X         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UBJAP XAWZL NMWVJ QIWJX UMIWW Z
-------------------------------
