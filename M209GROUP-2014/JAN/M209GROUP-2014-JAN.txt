SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JAN 2014

    01 JAN 2014  00:00-23:59 GMT:  USE KEY OL
    02 JAN 2014  00:00-23:59 GMT:  USE KEY OM
    03 JAN 2014  00:00-23:59 GMT:  USE KEY ON
    04 JAN 2014  00:00-23:59 GMT:  USE KEY OO
    05 JAN 2014  00:00-23:59 GMT:  USE KEY OP
    06 JAN 2014  00:00-23:59 GMT:  USE KEY OQ
    07 JAN 2014  00:00-23:59 GMT:  USE KEY OR
    08 JAN 2014  00:00-23:59 GMT:  USE KEY OS
    09 JAN 2014  00:00-23:59 GMT:  USE KEY OT
    10 JAN 2014  00:00-23:59 GMT:  USE KEY OU
    11 JAN 2014  00:00-23:59 GMT:  USE KEY OV
    12 JAN 2014  00:00-23:59 GMT:  USE KEY OW
    13 JAN 2014  00:00-23:59 GMT:  USE KEY OX
    14 JAN 2014  00:00-23:59 GMT:  USE KEY OY
    15 JAN 2014  00:00-23:59 GMT:  USE KEY OZ
    16 JAN 2014  00:00-23:59 GMT:  USE KEY PA
    17 JAN 2014  00:00-23:59 GMT:  USE KEY PB
    18 JAN 2014  00:00-23:59 GMT:  USE KEY PC
    19 JAN 2014  00:00-23:59 GMT:  USE KEY PD
    20 JAN 2014  00:00-23:59 GMT:  USE KEY PE
    21 JAN 2014  00:00-23:59 GMT:  USE KEY PF
    22 JAN 2014  00:00-23:59 GMT:  USE KEY PG
    23 JAN 2014  00:00-23:59 GMT:  USE KEY PH
    24 JAN 2014  00:00-23:59 GMT:  USE KEY PI
    25 JAN 2014  00:00-23:59 GMT:  USE KEY PJ
    26 JAN 2014  00:00-23:59 GMT:  USE KEY PK
    27 JAN 2014  00:00-23:59 GMT:  USE KEY PL
    28 JAN 2014  00:00-23:59 GMT:  USE KEY PM
    29 JAN 2014  00:00-23:59 GMT:  USE KEY PN
    30 JAN 2014  00:00-23:59 GMT:  USE KEY PO
    31 JAN 2014  00:00-23:59 GMT:  USE KEY PP

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 2-0   B  B  -  -  B  B
03 2-0   -  -  -  C  -  -
04 2-0   D  -  -  -  D  D
05 2-0   E  -  E  -  E  -
06 2-0   -  F  F  F  -  -
07 2-0   G  G  -  G  G  G
08 2-0   -  -  -  -  -  -
09 2-0   I  I  I  -  -  I
10 0-3   J  -  -  J  -  J
11 0-3   -  -  K  K  K  -
12 0-3   L  -  -  -  -  -
13 0-3   -  -  M  -  M  -
14 0-4   N  N  N  -  N  -
15 0-4   -  -  -  O  O  O
16 0-5   P  P  -  -  -  P
17 0-6   -  Q  Q  Q  Q  Q
18 0-6   R  -  -  R  -   
19 0-6   -  S  S  S  S   
20 0-6   T  -  -  T      
21 0-6   U  U  U  U      
22 2-6   -  V  V         
23 2-6   -  -  X         
24 2-6   X  Y            
25 3-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

GMQHV SUURV VPNIK RQLJP IKQQN M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   B  -  B  B  B  B
03 1-0   -  C  C  -  -  C
04 1-0   -  D  D  D  D  D
05 1-0   E  -  E  -  -  -
06 1-0   F  F  -  -  F  F
07 1-0   G  -  G  G  G  -
08 1-0   -  -  H  -  -  H
09 1-0   -  -  -  I  -  -
10 1-0   -  -  -  J  -  J
11 1-0   K  K  -  -  -  -
12 1-0   -  L  -  -  -  -
13 1-0   -  -  -  -  M  -
14 2-0   N  N  N  N  N  -
15 0-4   O  O  -  -  -  O
16 0-4   P  -  -  P  P  P
17 0-4   -  Q  -  Q  -  -
18 0-4   -  R  R  -  R   
19 0-4   S  S  S  -  S   
20 0-4   T  T  -  T      
21 0-4   -  U  U  U      
22 0-4   V  -  V         
23 0-5   -  X  X         
24 0-5   -  Y            
25 0-6   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PKDVA DQLLA KDXEF UQWHZ CSKDV A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ON
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  C  -  -
04 1-0   D  -  -  D  D  D
05 2-0   E  E  E  -  E  -
06 2-0   -  -  F  -  -  F
07 2-0   G  G  G  -  -  G
08 2-0   H  -  -  -  -  -
09 2-0   I  I  I  -  I  -
10 2-0   J  -  J  -  J  J
11 0-4   -  -  K  K  -  K
12 0-4   L  -  L  L  -  -
13 0-4   M  M  -  M  M  -
14 0-6   -  N  N  -  N  N
15 0-6   -  -  -  -  -  -
16 1-2   P  -  -  P  -  -
17 1-4   -  Q  Q  -  -  Q
18 1-4   -  R  -  R  -   
19 1-4   S  S  -  S  S   
20 1-4   T  T  -  T      
21 2-6   U  U  -  -      
22 3-4   V  V  -         
23 4-5   -  -  X         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UBJAP XAWZL NMWVJ QIWJX UMIWW Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 2-0   B  -  B  -  -  B
03 2-0   C  -  -  C  -  C
04 2-0   -  D  -  -  -  D
05 2-0   -  E  -  E  E  E
06 0-3   F  F  F  -  -  F
07 0-3   -  -  G  G  -  -
08 0-3   H  H  H  -  H  H
09 0-3   -  -  I  -  -  -
10 0-4   J  -  -  J  -  -
11 0-4   -  K  K  -  -  -
12 0-4   L  L  -  -  -  L
13 0-4   M  -  -  -  M  -
14 0-4   N  N  N  -  N  -
15 0-4   O  O  -  O  O  O
16 0-4   -  P  -  P  P  -
17 0-4   -  -  Q  Q  Q  -
18 0-5   R  -  -  -  -   
19 0-5   S  S  S  S  S   
20 1-5   -  T  T  T      
21 1-6   -  -  U  -      
22 2-3   V  V  V         
23 2-5   W  X  X         
24 3-4   -  -            
25 3-4   -  Z            
26 3-4   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

NJRWV QTAOR ITFOG YQIYZ OJKWW U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  B  -  -  B
03 1-0   -  C  -  -  C  C
04 1-0   -  D  D  -  D  D
05 1-0   -  -  E  -  -  -
06 1-0   -  F  -  F  F  -
07 2-0   -  G  -  G  -  -
08 2-0   H  H  H  H  H  H
09 2-0   I  -  I  I  -  -
10 2-0   -  J  J  J  J  J
11 2-0   -  -  -  K  K  -
12 0-3   -  -  -  -  L  L
13 0-3   M  -  -  M  -  M
14 0-3   N  -  N  N  N  -
15 0-4   O  O  -  -  -  O
16 0-4   P  P  -  -  -  P
17 0-4   -  -  -  Q  -  -
18 0-4   R  R  R  R  R   
19 0-5   S  S  S  S  S   
20 1-3   -  T  T  -      
21 1-3   -  U  -  U      
22 1-3   V  -  -         
23 1-3   -  X  X         
24 1-6   X  -            
25 2-3   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

AMMRT TACXI QJRPT GMTAV GHGOK X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 1-0   B  -  B  -  -  B
03 1-0   -  -  C  C  -  C
04 1-0   -  D  -  D  -  D
05 1-0   -  E  -  E  -  -
06 1-0   F  -  F  F  F  -
07 1-0   -  G  -  -  G  G
08 2-0   H  -  H  H  -  -
09 2-0   I  I  -  I  I  -
10 2-0   -  -  J  J  J  J
11 0-4   K  -  -  -  -  -
12 0-4   -  -  L  -  -  L
13 0-6   -  M  M  -  -  M
14 0-6   -  -  -  N  -  N
15 0-6   -  O  -  O  O  -
16 0-6   P  -  -  -  -  P
17 0-6   -  -  -  -  Q  -
18 1-5   R  -  -  -  R   
19 1-5   S  -  S  S  S   
20 1-6   T  -  T  -      
21 1-6   -  -  U  U      
22 1-6   V  V  -         
23 1-6   -  X  -         
24 2-4   X  Y            
25 2-6   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

RNWUM NRRZZ TIRQK FXKWX JNQHN R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  B  B  B  -
03 1-0   C  -  C  -  -  -
04 2-0   D  -  -  -  D  D
05 2-0   E  -  E  -  -  E
06 2-0   -  F  F  F  F  F
07 2-0   G  G  G  G  -  -
08 2-0   -  H  -  H  -  H
09 2-0   -  -  -  I  I  I
10 0-3   -  J  -  J  -  -
11 0-3   K  -  -  -  K  K
12 0-4   L  L  -  -  -  -
13 0-4   -  -  M  -  -  M
14 0-4   N  N  -  N  -  N
15 0-4   O  O  O  -  O  O
16 0-4   P  -  P  -  -  -
17 0-4   -  -  Q  Q  -  -
18 1-2   R  R  R  -  R   
19 1-3   S  -  S  S  S   
20 2-4   T  T  -  -      
21 2-4   -  U  U  -      
22 2-4   -  -  -         
23 2-4   W  X  -         
24 3-5   X  Y            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PPIUU OUKSO TUPIU UXXUA LFAZS A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  B  -  -  -
03 1-0   -  C  C  -  -  -
04 1-0   -  D  -  -  -  D
05 2-0   -  -  E  E  E  E
06 2-0   F  F  -  -  -  F
07 2-0   -  -  -  G  -  G
08 0-3   -  H  H  -  -  -
09 0-3   I  -  I  I  I  I
10 0-3   J  -  -  -  J  -
11 0-3   K  -  K  K  -  K
12 0-3   L  L  L  L  -  -
13 0-3   -  M  -  -  -  M
14 0-4   -  N  -  N  N  N
15 0-4   O  O  -  O  O  -
16 0-4   -  P  -  P  -  -
17 0-4   -  -  Q  Q  Q  Q
18 1-4   -  R  -  -  R   
19 1-4   S  -  S  S  -   
20 1-4   T  -  T  -      
21 1-4   -  U  U  -      
22 1-5   -  -  V         
23 1-5   W  X  X         
24 2-3   -  -            
25 2-5   -  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FNMAO LWSOP PMTOL JPTLP WTUOQ U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  -  -  -  -  -
03 1-0   -  C  C  C  -  -
04 1-0   -  D  -  -  -  D
05 1-0   E  -  E  -  -  E
06 2-0   -  -  F  F  -  -
07 2-0   G  G  G  -  G  -
08 2-0   H  H  -  H  H  H
09 0-3   I  -  I  -  -  -
10 0-3   J  J  J  J  J  -
11 0-3   -  -  K  -  -  K
12 0-6   L  -  -  L  L  L
13 0-6   -  M  M  -  M  -
14 0-6   N  N  -  -  -  -
15 0-6   -  -  -  O  O  -
16 0-6   P  -  P  P  P  P
17 0-6   Q  -  -  -  Q  Q
18 1-2   R  R  -  -  -   
19 1-3   -  S  -  S  S   
20 2-6   -  T  T  -      
21 2-6   U  -  -  U      
22 2-6   -  V  -         
23 2-6   -  -  -         
24 3-5   -  -            
25 3-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UUMKJ XRRAH WQLHV MGXAM UZXRX F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  B  B  B  -  -
03 1-0   -  -  C  C  C  C
04 1-0   -  -  -  D  D  D
05 2-0   -  E  -  -  E  -
06 2-0   -  F  F  -  -  -
07 2-0   G  G  G  G  -  G
08 2-0   H  -  -  H  -  H
09 2-0   -  -  -  I  I  I
10 2-0   J  J  J  -  -  J
11 0-3   -  K  K  K  K  -
12 0-4   L  L  -  L  L  -
13 0-5   -  -  -  -  M  M
14 0-5   N  -  -  -  -  N
15 0-5   -  -  -  O  O  -
16 0-5   -  P  P  -  -  -
17 0-5   -  -  Q  -  Q  Q
18 0-5   -  -  -  R  R   
19 0-5   S  S  S  S  -   
20 0-5   T  T  -  -      
21 0-5   -  U  -  -      
22 0-5   -  V  -         
23 0-5   W  X  X         
24 1-2   X  Y            
25 1-3   -  -            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

LSDTM WUNML TUAAA LWNRP AHXBS O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  -  -  B  B  -
03 1-0   C  -  C  C  C  -
04 1-0   -  D  -  -  D  D
05 2-0   -  -  E  E  -  E
06 2-0   -  F  -  F  F  -
07 2-0   G  G  -  G  G  G
08 2-0   -  -  H  -  -  H
09 2-0   I  I  -  I  I  I
10 2-0   J  J  -  -  -  J
11 0-3   -  -  K  K  -  -
12 0-3   -  -  -  -  -  L
13 0-3   -  M  -  M  M  M
14 0-3   N  -  -  -  N  N
15 0-4   -  -  O  -  -  -
16 0-4   P  -  -  P  P  -
17 0-4   Q  Q  Q  -  -  -
18 1-2   -  -  R  R  -   
19 1-4   -  -  S  -  -   
20 1-4   -  T  T  T      
21 1-4   U  U  U  U      
22 1-4   V  V  V         
23 2-3   W  X  -         
24 3-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NRLPG ZUAWJ WSFPW KLZDW XJZIU Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  -
02 2-0   B  B  -  -  -  -
03 0-3   -  -  C  C  C  C
04 0-3   D  -  D  D  -  -
05 0-3   -  E  -  E  -  E
06 0-3   -  -  -  -  -  F
07 0-3   G  -  G  -  -  G
08 0-4   H  -  H  -  H  -
09 0-4   -  -  I  I  I  -
10 0-4   J  J  -  -  -  J
11 0-4   K  -  K  -  K  K
12 0-4   L  L  -  L  L  -
13 0-4   -  -  -  M  -  M
14 0-5   N  N  -  -  N  -
15 0-5   O  O  -  O  O  -
16 0-5   -  -  P  P  -  -
17 0-5   Q  Q  -  Q  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   S  -  S  -  S   
20 1-2   -  T  T  T      
21 2-5   -  U  -  U      
22 3-4   V  V  -         
23 3-4   W  -  X         
24 3-4   -  -            
25 3-4   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VAAVQ TOVSL TOUQA KRRRI UTKKV U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  B  -  -  B  B
03 1-0   C  C  C  C  -  C
04 1-0   D  D  D  -  D  D
05 1-0   E  -  E  E  -  -
06 1-0   -  F  -  -  -  F
07 1-0   G  G  G  -  -  -
08 1-0   -  -  -  H  H  -
09 1-0   I  -  I  I  -  -
10 1-0   J  J  -  -  -  J
11 1-0   -  K  -  K  K  K
12 1-0   L  -  L  -  L  -
13 2-0   -  -  -  M  M  -
14 2-0   -  -  N  N  N  N
15 0-3   O  O  -  O  -  -
16 0-5   P  -  P  -  -  -
17 0-5   -  Q  -  -  -  Q
18 0-5   -  -  -  -  R   
19 0-5   -  -  S  -  S   
20 0-5   T  -  T  -      
21 0-5   U  U  U  U      
22 0-5   V  -  -         
23 0-6   W  X  X         
24 1-5   X  -            
25 2-4   -  Z            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AKNBM NAVSL FMTSR UGSJW ZNODZ A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   -  B  B  B  -  -
03 1-0   -  -  -  C  -  -
04 1-0   D  D  -  -  -  -
05 1-0   E  E  -  E  E  -
06 0-4   -  F  -  F  -  F
07 0-4   -  G  G  G  G  G
08 0-4   H  H  H  -  H  -
09 0-4   I  -  I  -  I  -
10 0-4   J  J  -  -  -  J
11 0-4   -  -  K  -  K  K
12 0-5   -  -  L  -  L  -
13 0-5   M  M  -  -  -  M
14 0-5   N  -  N  N  N  -
15 0-5   O  O  O  -  -  O
16 0-5   P  P  -  P  -  P
17 0-5   -  -  -  -  Q  Q
18 1-2   R  R  -  -  R   
19 1-3   S  S  -  S  S   
20 1-4   T  T  -  -      
21 1-4   U  -  U  -      
22 1-4   -  -  V         
23 1-4   -  -  X         
24 1-6   -  Y            
25 3-6   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UNUSN UTJBT LTKNZ ZLEUU MKNNA T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  -
02 2-0   B  B  -  B  B  -
03 2-0   -  -  C  -  -  C
04 2-0   D  -  -  -  D  -
05 2-0   -  -  -  E  -  -
06 2-0   -  -  F  -  F  F
07 2-0   G  -  G  -  G  -
08 0-3   H  H  H  H  -  -
09 0-3   I  I  I  -  I  -
10 0-3   J  J  -  J  J  J
11 0-4   -  K  -  -  K  K
12 0-5   -  L  -  L  -  L
13 0-5   M  -  -  -  M  M
14 0-5   -  N  N  -  -  N
15 0-5   O  O  O  O  O  -
16 0-5   P  -  -  -  P  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   R  R  -  R  -   
19 0-5   -  S  S  -  -   
20 0-5   -  T  -  T      
21 0-6   U  -  -  -      
22 1-3   -  V  V         
23 1-4   -  -  X         
24 2-3   X  Y            
25 2-5   -  -            
26 2-5   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

QROYV AAMFA CTQNN ETSAN XKUTX N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 0-3   B  -  -  -  -  -
03 0-3   -  C  C  -  -  C
04 0-3   D  -  D  -  D  D
05 0-3   E  E  E  -  E  E
06 0-4   -  F  -  F  -  F
07 0-4   G  G  -  G  G  -
08 0-4   H  H  H  H  -  H
09 0-4   I  -  I  I  -  -
10 0-5   J  J  J  J  J  -
11 0-5   -  -  K  -  K  K
12 0-5   -  L  -  L  -  -
13 0-5   M  -  M  -  M  M
14 0-5   -  N  N  -  N  -
15 0-5   -  -  -  -  -  O
16 1-2   -  -  -  -  -  P
17 1-3   -  Q  Q  -  -  -
18 1-3   R  -  -  R  R   
19 1-3   -  S  S  S  S   
20 1-5   T  -  -  -      
21 2-3   -  -  -  -      
22 3-4   V  V  -         
23 3-4   W  -  -         
24 3-4   X  Y            
25 3-4   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MHOMH RUOFL CNWNA PKAAP AINPC H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   -  -  -  B  -  -
03 1-0   C  -  C  C  -  C
04 1-0   D  D  -  -  D  D
05 1-0   -  E  E  E  E  -
06 1-0   F  -  -  -  F  F
07 2-0   G  G  G  -  G  G
08 2-0   -  -  H  -  H  H
09 0-3   -  -  I  -  I  -
10 0-3   J  -  -  -  -  -
11 0-3   -  K  -  K  -  -
12 0-3   -  -  -  -  -  L
13 0-3   M  M  M  M  -  M
14 0-3   -  N  -  -  -  N
15 0-3   O  -  O  O  O  -
16 0-3   -  -  P  -  P  P
17 0-4   -  Q  Q  Q  -  Q
18 0-5   R  -  -  -  -   
19 0-5   S  S  -  S  -   
20 0-5   T  T  T  -      
21 0-5   U  U  -  U      
22 0-5   V  -  -         
23 0-5   -  X  -         
24 0-5   -  -            
25 0-6   -  -            
26 1-2   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

AJTXR KTZEB JLQQZ KVRJC IEQSL P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  -  -  B  -
03 1-0   -  -  -  C  C  -
04 1-0   D  -  D  -  -  D
05 1-0   E  E  -  E  -  E
06 1-0   -  F  F  -  F  F
07 1-0   G  G  -  G  -  -
08 2-0   H  H  -  -  -  -
09 2-0   -  I  -  -  -  -
10 2-0   -  J  J  J  J  -
11 2-0   K  -  -  -  K  -
12 2-0   -  L  L  -  L  L
13 2-0   -  -  -  M  -  -
14 2-0   -  -  -  -  -  N
15 2-0   -  -  O  -  O  O
16 2-0   P  -  P  P  -  P
17 2-0   Q  Q  Q  Q  -  -
18 0-3   R  R  R  -  R   
19 0-4   -  S  -  S  -   
20 0-5   -  -  -  -      
21 0-5   U  U  -  U      
22 0-5   V  V  V         
23 0-5   -  -  X         
24 0-5   -  -            
25 0-6   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NKSTO JXMII RKJIG TJWZR MLRZE I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  -  -  B  -  B
03 1-0   C  C  C  C  -  C
04 1-0   -  D  D  -  D  D
05 1-0   -  -  -  -  E  E
06 0-3   -  -  F  -  -  -
07 0-3   G  -  G  G  -  -
08 0-3   H  H  -  -  -  -
09 0-3   -  -  -  -  I  -
10 0-3   -  J  J  -  -  -
11 0-4   K  -  K  -  K  K
12 0-4   L  -  L  L  -  -
13 0-6   -  M  M  -  -  -
14 0-6   -  N  -  N  N  N
15 0-6   O  -  O  O  O  O
16 0-6   P  -  -  -  P  -
17 0-6   Q  -  -  Q  Q  Q
18 1-2   R  R  -  R  R   
19 1-4   S  S  S  -  -   
20 1-5   -  -  -  T      
21 1-6   U  U  -  U      
22 1-6   -  -  -         
23 1-6   W  X  -         
24 1-6   X  -            
25 3-4   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

RVPSQ VMPVO UPISA KIRMG UYVQJ V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  -  B  B  B  B
03 1-0   -  -  -  C  -  -
04 1-0   D  -  D  -  -  D
05 1-0   E  E  E  -  E  E
06 1-0   -  F  F  F  -  F
07 2-0   G  -  G  G  G  -
08 2-0   H  H  -  H  H  -
09 0-3   -  I  -  -  I  -
10 0-3   J  -  -  -  -  -
11 0-3   K  K  -  -  K  K
12 0-3   -  L  -  L  L  L
13 0-4   M  M  -  -  -  M
14 0-4   N  -  N  N  -  -
15 0-4   O  -  O  O  O  -
16 0-4   -  -  P  -  -  P
17 0-4   -  -  -  -  -  Q
18 0-4   R  R  R  -  R   
19 0-4   S  S  S  S  -   
20 0-4   T  -  -  T      
21 0-4   U  -  U  U      
22 0-4   V  V  -         
23 0-4   -  X  -         
24 0-4   -  Y            
25 0-6   Y  -            
26 2-3   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

GRXJN LTTQJ RRBEX ZTGKE RJQAK M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  B  B  -  -
03 1-0   C  -  -  C  C  -
04 1-0   D  -  D  D  D  D
05 1-0   -  E  E  -  -  E
06 1-0   -  -  -  -  -  F
07 1-0   G  -  G  -  G  G
08 0-3   H  H  H  -  -  H
09 0-3   -  I  -  I  I  -
10 0-3   J  -  -  J  -  J
11 0-3   K  -  K  K  K  K
12 0-5   -  -  L  L  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  N  N  -  N  -
15 0-5   -  -  -  -  -  -
16 1-3   -  P  P  -  -  -
17 1-5   Q  -  -  Q  Q  -
18 1-5   R  -  R  -  R   
19 1-5   S  -  S  S  S   
20 1-5   -  T  -  T      
21 2-3   -  U  U  -      
22 2-6   -  -  -         
23 3-5   W  X  -         
24 3-5   X  -            
25 3-5   -  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

KNZLK TOANZ AABVA BTWWC VZZVO S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  C  -  -  -
04 0-3   -  D  -  -  D  D
05 0-3   -  -  E  E  E  -
06 0-3   F  -  F  F  -  -
07 0-3   G  -  G  -  -  G
08 0-3   H  -  -  H  -  -
09 0-3   -  -  I  I  -  -
10 0-4   J  J  J  J  J  J
11 0-5   K  -  -  K  -  K
12 0-5   L  L  -  -  L  -
13 0-5   M  M  M  M  -  -
14 0-5   N  N  N  -  N  -
15 0-6   -  -  O  O  -  O
16 0-6   P  -  P  P  -  P
17 0-6   -  Q  -  -  Q  -
18 0-6   R  R  -  -  R   
19 0-6   -  -  S  -  S   
20 1-4   -  -  T  -      
21 1-5   -  U  U  U      
22 2-4   -  -  -         
23 3-6   -  X  -         
24 3-6   X  Y            
25 3-6   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

AJVCJ UQULA OJQQM JWUQS FUVQA P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  -  -  -  -
03 0-4   -  -  C  C  -  C
04 0-4   D  -  -  D  -  -
05 0-4   E  -  E  E  -  E
06 0-4   -  -  -  -  -  -
07 0-4   -  G  -  G  G  G
08 0-5   -  H  H  H  -  -
09 0-5   I  -  I  I  -  I
10 0-5   -  -  J  -  -  J
11 0-5   -  K  K  -  K  K
12 0-5   -  L  -  -  -  -
13 0-6   M  M  M  -  M  -
14 0-6   N  N  -  N  N  -
15 0-6   -  O  -  -  O  -
16 0-6   -  -  P  P  P  P
17 0-6   Q  Q  -  Q  Q  Q
18 1-3   R  R  -  -  -   
19 1-4   S  S  S  S  S   
20 2-3   -  -  -  -      
21 3-6   U  U  U  U      
22 3-6   -  V  V         
23 4-5   W  -  X         
24 5-6   X  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LSQTQ VSQMP DVKRQ URTNG NKYVT K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  -  -  B  -
03 1-0   C  -  -  C  -  -
04 1-0   D  -  -  D  -  -
05 1-0   -  E  -  -  -  E
06 1-0   F  -  F  F  F  -
07 1-0   G  G  G  -  -  G
08 1-0   -  -  -  -  H  H
09 1-0   -  -  -  -  I  -
10 1-0   -  -  -  -  J  -
11 1-0   K  -  K  -  K  -
12 2-0   L  L  L  L  L  L
13 2-0   M  M  M  M  M  -
14 2-0   -  N  -  N  -  N
15 0-3   -  O  O  O  O  O
16 0-5   -  -  P  P  -  P
17 0-5   -  -  -  Q  -  Q
18 0-5   R  R  R  -  -   
19 0-5   S  S  S  S  S   
20 0-5   -  -  -  -      
21 0-5   U  U  U  -      
22 0-5   V  V  V         
23 0-5   W  X  -         
24 0-5   X  Y            
25 0-5   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OMMZF NPAPY AEEWM OVYLJ MNAPF F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  -  -  -
04 1-0   D  -  D  -  D  D
05 0-3   E  E  -  E  E  -
06 0-3   F  -  -  F  F  -
07 0-3   G  G  G  -  G  -
08 0-3   -  H  -  -  -  -
09 0-3   -  -  -  I  I  I
10 0-3   -  J  -  J  J  -
11 0-3   K  -  -  -  -  -
12 0-4   -  L  L  L  -  L
13 0-4   -  M  M  M  -  M
14 0-4   N  -  -  -  N  N
15 0-4   O  O  -  O  -  O
16 1-2   P  P  P  -  P  P
17 1-3   Q  -  -  -  -  -
18 1-4   -  -  R  R  -   
19 2-4   -  S  -  -  S   
20 2-4   -  T  -  -      
21 2-5   -  -  -  U      
22 3-4   -  V  V         
23 3-4   -  X  X         
24 3-4   X  Y            
25 3-4   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SKVAR OZFAP URCAT KUTOA OAQOG V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  A
02 2-0   -  -  -  -  B  -
03 2-0   -  -  C  C  C  C
04 2-0   D  D  -  -  D  D
05 2-0   -  E  -  E  -  -
06 2-0   -  F  -  -  F  F
07 0-3   G  -  -  G  -  -
08 0-3   H  -  -  H  H  -
09 0-3   I  I  I  -  -  I
10 0-3   -  J  -  -  J  J
11 0-3   -  K  K  -  -  K
12 0-3   L  L  L  L  -  L
13 0-5   -  -  M  M  -  -
14 0-5   N  -  N  -  -  -
15 0-5   -  -  O  O  O  -
16 0-5   -  P  -  -  P  P
17 0-5   -  -  Q  Q  Q  -
18 1-2   -  R  -  R  -   
19 1-6   S  S  -  S  S   
20 2-3   T  -  T  T      
21 2-4   -  U  -  U      
22 2-5   V  -  V         
23 2-5   W  -  -         
24 2-5   -  -            
25 2-5   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KVAAT MVNJL UTJKN UOMKN UUAST A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  -  -  B  -  B
03 1-0   -  C  -  C  C  -
04 1-0   D  D  -  -  D  -
05 1-0   E  E  -  -  -  -
06 1-0   F  -  F  -  F  F
07 1-0   G  -  G  -  G  G
08 2-0   -  -  H  H  -  H
09 2-0   I  I  I  -  -  I
10 2-0   J  J  -  -  J  J
11 2-0   -  K  -  -  -  -
12 2-0   -  -  L  L  L  -
13 2-0   -  M  M  -  -  M
14 2-0   N  -  N  N  -  -
15 2-0   -  O  -  O  O  O
16 2-0   -  -  -  P  P  P
17 2-0   Q  Q  -  -  Q  -
18 2-0   R  R  -  R  -   
19 0-3   -  S  -  -  S   
20 0-3   -  -  T  -      
21 0-3   -  -  U  U      
22 0-4   -  -  V         
23 0-4   W  X  X         
24 0-5   -  -            
25 0-6   -  Z            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PKWPN FDNSG PLINU XOLDU VQISM M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 2-0   -  B  -  -  B  -
03 2-0   C  -  C  C  C  -
04 0-3   -  -  D  D  -  -
05 0-3   -  -  -  -  -  -
06 0-3   F  -  -  F  -  F
07 0-3   -  G  -  G  -  G
08 0-3   H  H  H  -  H  H
09 0-3   I  -  I  I  I  -
10 0-3   J  -  -  J  -  -
11 0-3   -  K  K  -  K  -
12 0-3   -  L  L  L  L  L
13 0-3   -  M  -  M  M  M
14 0-3   -  -  N  -  N  N
15 0-3   O  -  O  O  -  -
16 0-4   -  P  -  -  -  P
17 0-4   Q  Q  Q  -  Q  Q
18 0-4   R  R  R  -  R   
19 0-4   -  -  S  -  -   
20 0-4   -  T  T  T      
21 0-4   -  -  U  -      
22 0-6   V  -  -         
23 0-6   W  X  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-2   Z               
27 1-5                   
-------------------------------
26 LETTER CHECK

SUOSL TSYDI EFYSJ GCUAG YVGYQ C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 0-4   -  -  B  -  -  B
03 0-4   C  C  C  C  -  -
04 0-4   D  D  D  D  D  -
05 0-4   E  E  -  E  E  E
06 0-5   F  -  -  F  F  -
07 0-5   G  -  -  -  G  -
08 0-5   -  -  H  H  -  -
09 0-5   -  -  -  -  -  I
10 0-5   -  J  -  J  J  -
11 0-5   K  -  K  -  K  K
12 0-6   L  -  -  L  L  L
13 0-6   M  -  M  -  -  -
14 0-6   -  -  N  N  -  N
15 0-6   -  O  O  -  -  O
16 0-6   P  -  -  -  P  -
17 0-6   -  Q  Q  Q  Q  -
18 1-2   R  -  -  -  -   
19 1-3   -  S  -  -  S   
20 1-5   T  T  -  T      
21 2-4   U  -  -  -      
22 2-5   V  V  V         
23 4-6   -  X  -         
24 5-6   -  -            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WQFLA PCTPZ CASUU QIUXL TNINM S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 0-3   -  B  B  B  -  B
03 0-3   C  -  -  C  C  C
04 0-3   D  -  D  D  -  D
05 0-3   E  -  -  E  E  -
06 0-3   F  -  -  -  F  F
07 0-5   G  -  G  G  -  -
08 0-5   H  H  H  H  -  H
09 0-5   -  I  -  I  I  I
10 0-5   J  -  -  J  -  -
11 0-5   K  K  -  -  K  -
12 0-6   -  L  L  -  -  -
13 0-6   M  -  -  -  -  M
14 0-6   N  -  N  -  N  -
15 0-6   O  O  -  O  O  -
16 1-4   -  P  P  -  P  -
17 2-3   -  Q  Q  Q  -  -
18 2-4   R  R  -  -  R   
19 2-6   -  -  S  -  -   
20 2-6   -  T  -  -      
21 2-6   U  U  U  -      
22 3-5   V  V  -         
23 4-6   -  -  X         
24 5-6   -  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LFAUM MQULU TJVSM NSJRO AVPSM A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JAN-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   -  -  -  -  -  C
04 1-0   D  -  D  D  -  -
05 1-0   -  E  E  E  E  E
06 2-0   F  -  -  F  -  -
07 0-3   G  -  -  -  -  -
08 0-3   H  H  -  -  H  H
09 0-3   -  I  -  I  -  I
10 0-3   -  -  J  J  J  -
11 0-3   -  K  K  -  K  K
12 0-3   L  L  -  -  -  L
13 0-3   M  -  M  M  -  -
14 0-3   -  -  -  N  N  -
15 0-5   O  O  -  O  O  -
16 0-5   -  P  P  -  -  -
17 0-5   -  -  -  -  Q  -
18 0-5   R  R  -  R  R   
19 0-5   -  -  S  -  S   
20 0-5   -  T  -  T      
21 0-5   -  -  U  U      
22 0-5   -  -  V         
23 0-5   W  -  -         
24 0-6   X  -            
25 0-6   Y  -            
26 1-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

EEXJZ SIZOR OQKIQ NFRSI HKYGM X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
