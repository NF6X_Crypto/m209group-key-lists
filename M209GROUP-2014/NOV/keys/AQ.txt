EFFECTIVE PERIOD:
14-NOV-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  -  B  B  -  B
03 1-0   C  -  -  C  -  C
04 2-0   D  -  -  -  D  D
05 2-0   -  E  -  -  -  -
06 2-0   F  F  F  -  -  F
07 2-0   -  -  -  -  -  G
08 2-0   -  H  H  H  -  -
09 2-0   I  I  -  I  I  -
10 2-0   J  -  J  J  J  -
11 2-0   K  K  K  -  K  K
12 2-0   -  L  L  -  L  L
13 2-0   -  M  M  M  M  M
14 0-3   N  -  N  N  -  -
15 0-3   -  -  -  -  O  -
16 0-4   P  -  -  P  P  P
17 0-5   Q  Q  -  -  Q  Q
18 0-5   R  R  R  -  -   
19 0-5   -  S  S  S  -   
20 0-5   T  T  -  T      
21 0-6   U  U  U  -      
22 1-4   -  V  V         
23 1-5   W  -  X         
24 2-3   X  -            
25 2-3   -  -            
26 2-3   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

QLSUY ZPVPG CZAFE PXNVZ OVXVK A
-------------------------------
