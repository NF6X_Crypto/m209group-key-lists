EFFECTIVE PERIOD:
24-NOV-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  A
02 2-0   -  B  B  -  -  B
03 0-3   C  -  -  C  -  -
04 0-3   D  D  -  -  D  D
05 0-3   -  -  -  E  E  E
06 0-3   F  -  F  -  F  F
07 0-4   -  G  G  -  -  -
08 0-4   H  -  H  -  H  H
09 0-4   -  -  I  I  -  -
10 0-4   J  -  -  J  -  J
11 0-4   -  K  -  -  K  -
12 0-4   -  L  -  L  L  -
13 0-4   -  -  M  -  -  -
14 0-4   N  -  -  -  -  N
15 0-4   -  -  -  -  -  O
16 0-4   P  P  -  -  -  -
17 0-5   Q  -  Q  -  -  -
18 0-5   -  R  R  R  R   
19 0-5   S  -  -  -  S   
20 0-5   T  T  T  T      
21 0-6   -  U  U  U      
22 1-6   V  -  V         
23 2-5   W  -  X         
24 2-6   X  Y            
25 3-4   -  Z            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

GEZMJ PPYYU MYKSF LJUWF OFKZC I
-------------------------------
