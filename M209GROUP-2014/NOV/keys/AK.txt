EFFECTIVE PERIOD:
08-NOV-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  B  -  B  B  B
03 1-0   C  -  C  C  C  C
04 1-0   D  D  -  -  -  D
05 2-0   E  -  -  -  -  -
06 2-0   -  -  -  -  F  F
07 2-0   -  -  -  -  G  -
08 2-0   H  H  -  H  -  H
09 2-0   -  I  I  -  I  -
10 2-0   -  J  -  J  J  -
11 0-3   -  -  K  K  K  -
12 0-3   -  -  -  L  L  L
13 0-5   -  -  M  -  -  M
14 0-5   N  N  -  N  -  -
15 0-5   O  -  -  -  -  O
16 0-5   -  P  P  P  P  -
17 0-5   Q  -  -  Q  Q  Q
18 0-5   R  R  R  -  R   
19 0-5   S  S  -  S  -   
20 1-2   T  -  T  T      
21 1-5   U  -  U  U      
22 1-5   V  V  V         
23 1-5   -  -  X         
24 1-5   -  -            
25 2-3   Y  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LRNQA QUKEU UALQX IMLAU KADLY U
-------------------------------
