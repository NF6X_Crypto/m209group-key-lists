EFFECTIVE PERIOD:
03-NOV-2014 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  -  B  -  B  B
03 2-0   C  C  C  C  -  C
04 2-0   D  D  D  D  -  D
05 2-0   -  -  E  E  E  E
06 2-0   F  F  F  F  F  F
07 2-0   G  -  -  -  G  -
08 2-0   -  -  H  H  -  H
09 0-4   I  I  -  -  I  I
10 0-4   -  J  -  -  J  -
11 0-5   K  K  -  K  -  -
12 0-5   -  -  L  -  L  -
13 0-5   -  -  -  M  M  -
14 0-5   N  -  N  -  -  N
15 0-5   -  -  -  -  -  -
16 1-2   P  P  -  -  -  -
17 1-4   -  Q  -  -  Q  Q
18 1-4   R  R  R  -  -   
19 1-4   S  S  S  S  S   
20 1-6   T  -  T  T      
21 2-5   U  U  -  -      
22 3-4   -  V  V         
23 4-5   -  -  X         
24 4-5   X  Y            
25 4-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UQNOO RTXSY OVRAA CARZA SVZCQ B
-------------------------------
