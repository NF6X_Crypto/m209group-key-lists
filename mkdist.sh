#!/bin/sh
#
# Usage: mkdist.sh <directory>
#
# Creates archives <directory>.zip and <directory.tar.gz
# which must not already exist.
#
# Creates "tmp" temporary directory which must not already
# exist.

if [ $# -ne 1 ]; then
    echo "USAGE: $0 <directory>"
    exit 1
fi

keylistdir=$1
zipfile=${keylistdir}.zip
tarfile=${keylistdir}.tar.gz

if [ ! -d $keylistdir ]; then
    echo "ERROR: Directory $keylistdir not found."
    exit 1
fi

if [ -e $zipfile ]; then
    echo "ERROR: $zipfile already exists."
    exit 1
fi

if [ -e $tarfile ]; then
    echo "ERROR: $tarfile already exists."
    exit 1
fi

if [ -e tmp ]; then
    echo "ERROR: tmp already exists."
    exit 1
fi

mkdir tmp
find $keylistdir | cpio -p tmp
cd tmp
dos2unix `find . -name \*.txt`
tar czf ../$tarfile $keylistdir
echo "$tarfile created"
unix2dos `find . -name \*.txt`
zip -r ../$zipfile $keylistdir
echo "$zipfile created"
cd ..
rm -rf tmp

