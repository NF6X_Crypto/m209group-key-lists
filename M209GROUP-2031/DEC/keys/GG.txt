EFFECTIVE PERIOD:
05-DEC-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   -  B  -  B  -  -
03 1-0   C  -  -  -  -  C
04 1-0   -  D  D  D  -  D
05 0-3   -  E  E  -  E  E
06 0-4   F  F  F  F  -  -
07 0-4   G  -  G  G  G  -
08 0-4   H  H  H  H  H  H
09 0-5   I  -  -  -  I  I
10 0-5   -  J  J  -  J  J
11 0-5   K  -  K  K  -  -
12 0-5   -  L  -  -  L  L
13 0-5   M  M  M  -  M  -
14 0-5   -  N  N  N  N  -
15 0-6   -  -  O  -  O  O
16 0-6   -  -  -  -  P  -
17 0-6   Q  -  -  Q  Q  -
18 0-6   -  -  -  R  -   
19 0-6   -  -  S  S  -   
20 1-3   -  T  -  T      
21 1-6   -  -  -  -      
22 2-3   V  V  -         
23 4-5   -  X  -         
24 4-5   X  Y            
25 4-5   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MAJJT QNOGM QHOHW WARMV MVQRT H
-------------------------------
