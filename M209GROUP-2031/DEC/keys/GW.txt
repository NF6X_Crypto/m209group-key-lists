EFFECTIVE PERIOD:
21-DEC-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  -  -  C
04 1-0   -  D  -  D  D  D
05 1-0   -  -  -  E  E  E
06 2-0   F  -  -  F  -  -
07 2-0   -  G  G  -  G  G
08 2-0   -  H  -  H  -  H
09 2-0   I  -  I  I  I  I
10 2-0   -  -  J  -  J  -
11 2-0   K  K  -  -  -  -
12 2-0   -  -  -  -  L  L
13 0-3   -  M  -  M  -  -
14 0-4   N  -  N  -  N  N
15 0-4   -  O  O  O  -  -
16 0-4   P  -  P  P  -  P
17 0-4   Q  -  -  -  -  -
18 0-5   R  R  -  -  -   
19 0-5   -  -  S  -  -   
20 0-5   -  T  -  T      
21 0-6   -  U  U  -      
22 1-4   -  -  V         
23 1-5   -  -  -         
24 2-5   X  -            
25 2-5   Y  -            
26 2-5   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

ZJSQZ MJGIN AWUWU ASTMO AYAQM L
-------------------------------
