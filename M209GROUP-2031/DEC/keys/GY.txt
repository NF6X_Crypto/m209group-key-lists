EFFECTIVE PERIOD:
23-DEC-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   -  -  B  -  B  B
03 2-0   C  -  -  -  C  C
04 0-4   -  D  -  D  -  D
05 0-4   -  E  -  E  -  -
06 0-4   -  -  F  F  -  F
07 0-4   G  -  -  -  G  G
08 0-4   H  -  H  H  H  -
09 0-4   I  I  I  I  -  I
10 0-5   -  -  J  -  J  -
11 0-5   -  K  K  -  K  -
12 0-5   -  -  L  -  -  -
13 0-5   -  -  -  M  M  -
14 0-5   N  -  -  -  N  N
15 0-5   -  -  -  -  O  -
16 0-5   P  P  -  P  -  P
17 0-5   Q  Q  -  Q  Q  -
18 0-5   -  -  R  -  -   
19 0-5   -  S  S  -  -   
20 0-6   T  -  T  -      
21 0-6   U  U  U  -      
22 0-6   -  V  V         
23 0-6   W  X  -         
24 1-3   X  -            
25 1-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

LPLWM TBEJW RBMPS SHVOE RTQZV M
-------------------------------
