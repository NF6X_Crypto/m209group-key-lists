EFFECTIVE PERIOD:
11-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  A
02 2-0   -  -  -  B  B  B
03 2-0   C  -  C  C  C  -
04 2-0   -  -  D  -  -  -
05 2-0   E  -  -  -  E  -
06 2-0   F  F  F  -  F  F
07 0-3   -  -  -  G  G  G
08 0-4   H  H  H  -  -  -
09 0-4   -  I  -  I  -  -
10 0-4   -  -  -  J  -  -
11 0-5   K  -  K  -  -  -
12 0-5   L  L  -  -  L  -
13 0-5   -  -  -  -  -  -
14 0-5   N  N  -  N  -  N
15 0-5   O  O  -  O  -  O
16 0-5   -  P  -  P  P  -
17 0-5   -  -  -  -  -  Q
18 0-5   R  -  R  -  -   
19 0-5   S  -  -  S  S   
20 0-6   -  T  T  -      
21 0-6   U  U  U  U      
22 0-6   V  V  V         
23 0-6   -  X  X         
24 1-4   -  Y            
25 2-5   -  Z            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

QMEJQ IRWRW WEQRJ FIFMQ GGMTQ N
-------------------------------
