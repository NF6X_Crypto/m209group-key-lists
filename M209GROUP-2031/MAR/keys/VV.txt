EFFECTIVE PERIOD:
09-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  -  A
02 2-0   -  B  B  -  B  B
03 2-0   C  -  C  -  C  -
04 2-0   D  -  D  D  -  D
05 2-0   -  -  -  -  E  E
06 2-0   -  -  -  F  -  F
07 0-4   -  -  -  G  G  -
08 0-4   -  H  -  -  H  -
09 0-4   -  -  -  I  I  -
10 0-5   J  -  J  J  J  -
11 0-5   K  K  -  K  K  K
12 0-5   L  L  L  -  -  -
13 0-5   -  M  M  M  -  -
14 0-5   N  N  -  -  -  N
15 0-5   -  -  O  -  -  -
16 1-4   -  P  P  -  P  -
17 2-4   -  Q  Q  Q  Q  -
18 2-4   R  R  R  -  -   
19 2-4   S  -  S  -  -   
20 2-4   T  -  -  T      
21 2-5   U  U  U  -      
22 3-6   -  V  V         
23 4-5   -  -  -         
24 4-5   -  -            
25 4-6   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NLMPU LKUDT MPABT TJUZM ETMTX N
-------------------------------
