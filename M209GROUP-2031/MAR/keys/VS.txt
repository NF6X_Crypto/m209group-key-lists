EFFECTIVE PERIOD:
06-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  -  B  -  B  -
03 1-0   -  -  C  C  C  C
04 1-0   D  -  -  -  D  D
05 1-0   -  -  -  -  -  -
06 1-0   -  F  F  -  F  -
07 2-0   G  G  -  G  -  -
08 2-0   H  H  -  H  -  H
09 2-0   -  -  -  I  I  -
10 2-0   -  J  J  J  -  J
11 0-3   -  -  K  K  K  K
12 0-4   L  L  -  -  L  -
13 0-5   M  -  M  M  M  M
14 0-6   N  N  N  -  -  -
15 0-6   -  O  -  -  O  O
16 0-6   -  -  -  -  P  P
17 0-6   -  Q  -  Q  -  Q
18 0-6   R  -  -  -  R   
19 0-6   -  S  S  -  -   
20 0-6   T  -  T  -      
21 0-6   U  U  U  U      
22 1-2   -  V  V         
23 1-6   -  X  -         
24 1-6   X  -            
25 1-6   Y  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UBWBZ STGRV UJKBR DTYRP INZUO I
-------------------------------
