EFFECTIVE PERIOD:
04-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 2-0   -  -  -  -  -  -
03 2-0   C  C  C  C  C  C
04 2-0   -  -  -  -  D  -
05 2-0   E  E  E  -  -  -
06 0-3   -  F  F  -  -  -
07 0-3   -  G  -  -  G  G
08 0-3   -  -  H  H  -  H
09 0-5   -  -  -  I  I  I
10 0-5   -  -  J  J  J  -
11 0-5   K  -  K  K  K  -
12 0-5   -  -  -  L  L  L
13 0-5   -  M  -  M  M  -
14 0-5   -  N  N  -  N  N
15 0-6   O  -  O  O  -  O
16 0-6   P  P  P  -  -  P
17 0-6   Q  Q  -  -  -  Q
18 1-2   R  -  R  R  -   
19 1-4   -  -  -  S  S   
20 1-6   T  T  T  T      
21 1-6   U  U  U  -      
22 2-5   -  -  -         
23 3-5   -  X  -         
24 3-6   -  -            
25 3-6   -  Z            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

SAXIO QUUNI XLHQT UOVRP USIUT L
-------------------------------
