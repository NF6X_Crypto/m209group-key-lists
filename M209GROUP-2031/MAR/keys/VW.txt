EFFECTIVE PERIOD:
10-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   -  B  B  B  -  -
03 2-0   -  C  C  C  -  -
04 2-0   -  D  -  -  -  -
05 2-0   -  E  E  -  E  E
06 2-0   -  -  -  F  -  -
07 2-0   G  -  -  -  G  -
08 2-0   H  H  -  H  -  -
09 0-3   -  I  I  I  I  I
10 0-4   J  J  -  -  -  -
11 0-4   K  K  K  K  K  K
12 0-4   L  -  L  -  L  L
13 0-4   -  -  M  M  -  M
14 0-4   -  -  N  N  -  -
15 0-4   -  O  -  O  O  -
16 0-4   -  -  -  -  -  P
17 0-5   Q  Q  Q  Q  -  -
18 0-5   -  -  R  -  R   
19 0-5   S  -  S  -  -   
20 0-5   T  -  -  -      
21 0-5   -  U  -  U      
22 0-6   V  -  -         
23 0-6   W  X  X         
24 1-3   X  -            
25 3-6   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AMTJK FPEWK KVRHY LNMTY KSSKD V
-------------------------------
