EFFECTIVE PERIOD:
03-MAR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  -  B  B  -  -
03 1-0   C  -  C  C  C  C
04 1-0   D  D  -  D  -  D
05 0-3   -  E  E  -  E  E
06 0-3   F  -  -  -  -  -
07 0-3   -  G  -  G  -  G
08 0-3   -  -  -  H  H  H
09 0-3   I  I  -  -  I  -
10 0-3   -  -  J  J  J  J
11 0-4   -  K  -  K  K  -
12 0-4   -  -  L  L  -  -
13 0-4   -  M  M  -  -  -
14 0-4   N  -  N  -  -  N
15 0-5   O  O  O  O  O  -
16 0-5   -  P  P  -  P  -
17 0-6   Q  Q  Q  -  Q  -
18 1-4   R  R  -  R  -   
19 1-6   -  -  -  S  -   
20 2-6   T  T  T  -      
21 3-5   -  U  U  U      
22 3-5   -  -  -         
23 3-5   W  -  -         
24 3-5   X  Y            
25 3-6   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

MSXRM MSRKJ QYYJT MUHLX KMOUU S
-------------------------------
