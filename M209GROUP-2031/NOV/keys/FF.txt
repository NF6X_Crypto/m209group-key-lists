EFFECTIVE PERIOD:
08-NOV-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  -  -  -  -  C
04 1-0   -  D  -  -  D  D
05 2-0   -  E  -  -  E  E
06 2-0   F  F  F  F  -  -
07 2-0   G  -  G  -  -  -
08 2-0   H  -  -  -  -  -
09 2-0   -  I  -  I  -  -
10 0-3   -  J  J  -  J  J
11 0-3   K  -  -  K  K  K
12 0-3   -  -  L  L  -  L
13 0-3   M  M  -  M  M  M
14 0-4   -  -  N  -  N  -
15 0-4   O  -  O  O  O  -
16 0-4   -  P  -  -  -  P
17 0-6   -  Q  -  Q  Q  Q
18 1-3   R  R  R  -  R   
19 1-4   -  S  S  -  S   
20 2-4   T  -  -  -      
21 2-4   U  -  U  -      
22 2-4   V  -  V         
23 2-4   W  -  X         
24 2-6   X  Y            
25 2-6   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RSWLY OKOLW ROLFO QXRKW PSEAJ N
-------------------------------
