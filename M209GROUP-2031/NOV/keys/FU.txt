EFFECTIVE PERIOD:
23-NOV-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  B  B  -  -
03 1-0   C  C  C  C  C  -
04 1-0   -  -  D  -  -  D
05 2-0   E  E  E  -  E  -
06 2-0   -  -  -  -  F  F
07 2-0   G  G  G  -  -  -
08 2-0   H  -  H  -  H  H
09 0-5   -  I  -  I  -  I
10 0-6   -  -  -  J  J  J
11 0-6   -  K  -  K  -  -
12 0-6   -  L  -  -  -  L
13 0-6   -  M  M  M  M  -
14 0-6   -  N  N  -  N  -
15 0-6   O  -  O  O  -  -
16 1-2   -  -  -  -  -  -
17 1-2   -  Q  Q  Q  -  -
18 1-2   R  R  -  -  R   
19 1-2   S  S  -  -  S   
20 1-3   -  -  T  T      
21 1-3   -  U  -  U      
22 1-5   V  -  -         
23 1-5   W  -  X         
24 2-6   -  -            
25 3-4   -  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

DOUSJ ZUWAD WDAUL ZLUUJ EOMMT P
-------------------------------
