EFFECTIVE PERIOD:
24-AUG-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 0-3   B  B  B  B  -  B
03 0-3   -  C  C  C  C  -
04 0-3   -  D  -  D  D  D
05 0-3   E  -  E  -  E  E
06 0-3   -  F  F  -  F  -
07 0-5   G  -  -  G  -  -
08 0-5   -  -  -  H  H  -
09 0-5   I  -  I  -  -  I
10 0-5   -  -  -  -  -  -
11 0-5   K  -  K  K  K  -
12 0-6   L  -  -  L  -  L
13 0-6   -  M  -  M  -  -
14 0-6   -  -  N  -  -  -
15 0-6   -  O  O  -  -  O
16 0-6   -  P  -  P  P  -
17 0-6   Q  -  Q  Q  -  -
18 1-2   R  -  R  -  R   
19 1-3   -  S  -  S  -   
20 1-6   -  T  T  -      
21 2-6   U  U  -  -      
22 3-5   V  -  -         
23 4-6   W  -  -         
24 5-6   X  -            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JVDHZ EUPVE AYULI ACDYE HPDRT T
-------------------------------
