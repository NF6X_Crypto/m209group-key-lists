EFFECTIVE PERIOD:
15-AUG-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   B  -  B  B  -  -
03 1-0   C  C  -  C  C  C
04 1-0   -  -  D  D  -  -
05 1-0   E  E  E  -  E  E
06 1-0   F  F  -  F  -  F
07 0-4   G  -  G  -  G  G
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  I  I  I
10 0-4   J  J  -  J  J  J
11 0-4   -  -  K  -  -  -
12 0-4   -  -  -  -  -  -
13 0-6   -  -  -  M  -  -
14 0-6   -  -  N  -  N  -
15 0-6   -  O  O  -  -  -
16 0-6   -  P  -  -  P  P
17 0-6   Q  Q  -  -  -  -
18 1-2   R  R  R  -  -   
19 1-4   S  -  S  -  S   
20 2-5   T  -  -  T      
21 2-6   -  -  U  U      
22 2-6   -  -  -         
23 3-6   -  X  X         
24 4-6   -  Y            
25 4-6   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

PLKKJ LZTOU USAUM UATLK LLLBL D
-------------------------------
