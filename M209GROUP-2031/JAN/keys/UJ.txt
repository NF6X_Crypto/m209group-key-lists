EFFECTIVE PERIOD:
30-JAN-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   B  -  -  B  B  -
03 1-0   -  -  -  C  C  -
04 0-4   D  D  -  -  -  D
05 0-4   E  -  -  E  E  E
06 0-4   -  F  -  -  -  F
07 0-4   G  -  -  -  -  G
08 0-4   H  -  H  -  H  -
09 0-6   I  I  I  -  I  -
10 0-6   -  J  J  J  -  -
11 0-6   K  -  K  K  K  -
12 0-6   L  -  L  L  -  -
13 0-6   -  M  -  -  M  M
14 0-6   -  N  -  N  -  N
15 0-6   -  O  -  -  -  O
16 1-2   P  -  P  P  -  -
17 1-3   -  -  -  -  Q  Q
18 1-3   -  -  R  -  -   
19 1-3   -  S  -  -  S   
20 1-4   T  T  -  -      
21 1-4   -  U  U  U      
22 1-4   V  -  -         
23 1-4   -  X  X         
24 1-5   -  -            
25 2-3   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MKMRV CVFLN ATURM LCNTA XVRKM F
-------------------------------
