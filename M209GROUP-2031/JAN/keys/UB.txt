EFFECTIVE PERIOD:
22-JAN-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  B  B  B  B  -
03 1-0   -  C  -  -  C  -
04 1-0   -  D  D  D  D  D
05 0-3   E  E  E  -  E  E
06 0-3   F  -  -  -  F  F
07 0-4   -  -  G  G  G  -
08 0-4   H  -  H  H  -  -
09 0-4   -  -  I  -  -  I
10 0-5   J  J  -  J  J  J
11 0-6   K  K  -  K  K  -
12 0-6   -  -  -  L  L  -
13 0-6   -  -  M  -  -  M
14 0-6   -  N  N  -  -  -
15 0-6   O  -  O  O  -  -
16 0-6   -  P  P  -  -  P
17 0-6   -  -  -  -  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   S  S  -  S  -   
20 1-4   -  T  -  T      
21 1-5   U  -  -  -      
22 2-3   -  V  -         
23 3-4   -  X  X         
24 5-6   X  Y            
25 5-6   Y  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JDZFA YTSGR XSUJJ XZJRT KGQUS O
-------------------------------
