EFFECTIVE PERIOD:
17-JAN-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  A
02 1-0   -  -  -  -  -  B
03 1-0   C  C  C  -  C  -
04 2-0   -  -  -  D  -  -
05 2-0   -  -  -  -  E  E
06 2-0   -  -  F  -  F  -
07 2-0   -  G  G  G  -  -
08 2-0   H  -  H  H  H  H
09 0-4   -  I  -  -  -  -
10 0-5   J  -  -  -  -  J
11 0-5   -  K  -  -  -  -
12 0-5   L  L  -  -  -  L
13 0-5   -  -  -  M  M  M
14 0-5   N  -  N  -  -  -
15 0-5   O  -  O  O  O  -
16 0-6   -  P  P  -  -  P
17 0-6   -  -  -  -  -  -
18 0-6   -  -  R  -  -   
19 0-6   -  -  -  S  S   
20 1-4   -  -  T  T      
21 1-5   U  U  -  U      
22 2-3   V  -  -         
23 2-6   W  X  X         
24 2-6   X  -            
25 2-6   -  Z            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CVLHA NPUAC VOKVI KLJNO LLZCO O
-------------------------------
