EFFECTIVE PERIOD:
11-JAN-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  B  -  -  -
03 2-0   -  -  -  -  C  C
04 0-3   -  D  -  -  -  -
05 0-3   E  E  -  E  -  E
06 0-3   F  -  -  -  -  -
07 0-3   G  G  G  G  G  -
08 0-3   -  -  -  -  -  -
09 0-3   I  -  I  -  I  -
10 0-3   J  -  J  -  -  J
11 0-5   K  K  K  K  -  K
12 0-5   L  L  -  L  L  -
13 0-6   M  M  M  -  M  M
14 0-6   -  N  -  N  N  N
15 0-6   -  O  O  O  -  -
16 0-6   P  -  P  -  P  P
17 0-6   Q  Q  -  Q  -  Q
18 0-6   R  R  R  R  R   
19 0-6   -  S  -  S  S   
20 0-6   T  -  T  -      
21 0-6   -  U  U  -      
22 1-2   V  -  V         
23 1-5   -  -  -         
24 2-4   -  Y            
25 3-5   -  Z            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

TQANR LAMMN GSFAG FYQJZ ZIYTM P
-------------------------------
