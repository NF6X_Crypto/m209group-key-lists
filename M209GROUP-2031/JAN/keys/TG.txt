EFFECTIVE PERIOD:
01-JAN-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  -  B  B  B  B
03 1-0   C  C  -  C  -  -
04 1-0   D  D  D  -  D  D
05 1-0   E  -  E  E  E  E
06 1-0   F  -  F  -  F  F
07 1-0   -  -  G  -  -  G
08 2-0   -  H  -  -  -  -
09 2-0   -  -  -  -  -  I
10 2-0   -  -  J  J  J  -
11 2-0   -  -  -  -  K  K
12 0-3   L  L  -  L  L  -
13 0-3   -  M  M  M  -  -
14 0-3   N  N  -  -  -  -
15 0-3   O  O  O  -  O  O
16 0-3   P  -  -  P  -  -
17 0-3   -  Q  Q  -  -  Q
18 0-4   R  R  -  -  -   
19 0-4   -  S  S  S  S   
20 1-2   -  T  -  T      
21 1-2   U  U  -  U      
22 1-2   V  -  V         
23 1-2   -  -  -         
24 1-5   -  -            
25 2-3   -  Z            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UHQTJ XUEKE MTBAX UTBNI DDXJA R
-------------------------------
