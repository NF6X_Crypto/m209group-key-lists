EFFECTIVE PERIOD:
19-OCT-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 2-0   -  -  B  B  B  B
03 0-3   -  C  -  C  -  C
04 0-3   D  D  D  D  D  D
05 0-3   -  -  E  E  E  -
06 0-3   F  -  F  -  -  -
07 0-3   -  -  G  G  -  G
08 0-3   -  H  -  -  H  -
09 0-4   I  -  -  -  I  I
10 0-5   -  -  J  -  -  J
11 0-5   K  -  K  -  K  -
12 0-5   L  -  L  -  L  -
13 0-5   -  M  -  M  M  -
14 0-6   N  N  -  -  -  N
15 0-6   O  O  -  -  -  -
16 0-6   P  -  P  -  P  -
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  R  -   
19 0-6   -  -  -  S  -   
20 0-6   -  T  -  -      
21 0-6   U  U  -  U      
22 0-6   -  V  V         
23 0-6   W  -  -         
24 0-6   -  Y            
25 0-6   Y  -            
26 1-2   Z               
27 1-5                   
-------------------------------
26 LETTER CHECK

IEJRL ECPXU PNFUJ XYZGM WJCGP W
-------------------------------
