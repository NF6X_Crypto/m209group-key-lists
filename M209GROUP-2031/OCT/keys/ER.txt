EFFECTIVE PERIOD:
25-OCT-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ER
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  -
02 2-0   -  -  B  B  -  -
03 2-0   -  -  C  -  -  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  E  -  E  E
06 0-3   F  F  -  F  F  F
07 0-3   -  G  G  -  -  G
08 0-3   H  -  -  -  -  H
09 0-3   -  -  I  I  -  -
10 0-3   J  J  -  J  -  -
11 0-3   -  -  K  -  K  K
12 0-4   -  -  -  -  L  -
13 0-4   -  M  -  M  -  M
14 0-4   N  N  -  -  N  N
15 0-5   -  -  O  -  -  -
16 1-4   P  P  P  P  P  P
17 2-3   Q  -  Q  -  -  Q
18 2-5   -  R  -  R  R   
19 3-4   -  S  -  S  S   
20 3-4   -  T  T  T      
21 3-4   -  -  -  U      
22 3-4   V  V  V         
23 4-5   W  -  X         
24 4-5   X  Y            
25 4-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ORRZL TVUVQ SSVRZ ARTVG TOBMY Q
-------------------------------
