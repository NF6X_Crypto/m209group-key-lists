EFFECTIVE PERIOD:
15-OCT-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   B  -  -  B  B  -
03 2-0   C  -  -  -  -  -
04 2-0   -  -  D  D  D  -
05 2-0   -  E  E  -  -  E
06 2-0   F  F  F  -  F  -
07 0-3   G  G  G  G  G  -
08 0-4   H  H  -  -  H  -
09 0-4   -  -  -  -  -  I
10 0-5   -  J  -  J  J  -
11 0-5   K  K  -  K  K  K
12 0-5   L  -  -  L  -  L
13 0-5   -  -  M  M  -  M
14 0-5   -  -  N  -  -  N
15 0-5   -  -  -  O  -  O
16 0-5   -  P  P  -  P  P
17 0-5   Q  Q  -  -  -  -
18 0-6   -  -  R  R  -   
19 0-6   -  -  S  S  S   
20 1-5   -  T  T  T      
21 2-4   U  U  -  -      
22 2-5   V  -  -         
23 2-5   W  X  X         
24 2-5   -  Y            
25 2-5   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

HPRDR SQQKY FXEOO ANXZG QRFSZ B
-------------------------------
