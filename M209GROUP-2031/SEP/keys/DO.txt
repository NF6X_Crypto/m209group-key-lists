EFFECTIVE PERIOD:
26-SEP-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 2-0   -  B  -  B  B  B
03 2-0   C  -  -  C  C  C
04 2-0   -  -  -  -  D  -
05 2-0   -  -  E  -  -  -
06 2-0   -  -  F  F  -  F
07 2-0   -  G  G  -  G  -
08 0-3   H  H  -  -  H  -
09 0-3   -  I  -  I  -  -
10 0-3   J  J  J  -  J  -
11 0-4   K  -  -  K  -  -
12 0-5   -  -  -  -  L  -
13 0-5   M  -  M  -  M  M
14 0-5   N  N  N  -  N  N
15 0-5   O  O  -  O  -  -
16 0-5   P  P  -  -  -  P
17 0-5   -  -  Q  Q  Q  -
18 0-5   R  -  R  R  -   
19 0-5   S  S  S  S  -   
20 0-5   T  -  -  T      
21 0-5   U  -  -  -      
22 0-6   -  V  V         
23 0-6   -  -  -         
24 2-3   X  -            
25 2-5   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WRQIO UBMIO YTLAR PETIZ WLHHW N
-------------------------------
