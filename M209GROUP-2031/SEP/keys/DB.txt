EFFECTIVE PERIOD:
13-SEP-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  A
02 2-0   B  B  -  -  B  B
03 2-0   -  -  -  -  -  -
04 2-0   D  D  -  -  D  -
05 2-0   E  E  E  E  -  E
06 0-4   F  -  F  F  -  -
07 0-4   G  G  -  -  G  -
08 0-4   H  H  -  -  H  -
09 0-5   -  -  -  I  I  I
10 0-5   -  -  J  J  -  -
11 0-5   -  K  K  K  K  K
12 0-5   -  L  L  L  L  L
13 0-5   M  -  -  M  -  M
14 0-6   N  N  N  -  N  -
15 0-6   O  O  -  O  -  -
16 1-3   -  P  P  -  -  P
17 1-4   Q  -  -  -  Q  Q
18 1-4   -  -  -  -  R   
19 1-6   -  S  S  S  -   
20 2-5   T  -  T  -      
21 2-6   -  -  -  U      
22 4-5   -  V  V         
23 4-5   -  X  -         
24 4-5   X  -            
25 4-5   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AOVZK QVLAJ DPKKY XNVAV LXLPX X
-------------------------------
