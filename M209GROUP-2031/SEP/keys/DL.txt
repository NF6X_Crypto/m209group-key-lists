EFFECTIVE PERIOD:
23-SEP-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  -
02 2-0   -  -  B  -  B  -
03 2-0   -  -  -  C  -  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  E  -  -  E
06 2-0   -  -  F  -  -  F
07 2-0   -  G  -  G  -  -
08 0-3   -  H  -  H  H  -
09 0-3   -  -  I  -  -  I
10 0-3   -  J  J  -  -  J
11 0-3   K  -  -  K  K  -
12 0-5   -  -  -  L  L  -
13 0-5   M  -  M  M  M  M
14 0-5   N  N  N  N  N  N
15 0-5   -  O  O  O  O  O
16 0-5   -  -  -  -  P  P
17 0-6   Q  -  -  -  -  Q
18 0-6   -  -  -  -  -   
19 0-6   S  -  S  -  S   
20 1-3   -  T  -  -      
21 2-4   U  U  -  U      
22 2-6   V  V  -         
23 2-6   W  X  X         
24 2-6   -  Y            
25 2-6   -  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

XMOON VQTLX ZVIFR TILXT VPTTR L
-------------------------------
