EFFECTIVE PERIOD:
24-APR-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  -  B  B  B  -
03 2-0   -  -  -  C  -  C
04 2-0   D  -  D  -  D  -
05 2-0   -  E  E  E  -  -
06 2-0   F  -  F  F  F  -
07 2-0   -  G  -  -  -  G
08 2-0   -  H  H  -  H  -
09 2-0   I  I  I  I  -  -
10 0-3   -  J  -  J  -  J
11 0-3   -  -  -  -  -  -
12 0-3   -  -  L  -  -  L
13 0-3   M  -  M  M  -  M
14 0-3   -  -  -  N  N  N
15 0-3   O  O  -  -  O  -
16 0-4   P  -  P  P  -  -
17 0-4   Q  Q  Q  -  -  Q
18 0-4   R  R  R  -  -   
19 0-4   S  -  -  -  S   
20 1-4   -  -  T  T      
21 1-5   U  U  -  U      
22 2-3   -  V  -         
23 2-3   W  -  -         
24 2-3   -  Y            
25 2-3   Y  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

UJWAK DSUGP AYDRS TEXXP LQQEF W
-------------------------------
