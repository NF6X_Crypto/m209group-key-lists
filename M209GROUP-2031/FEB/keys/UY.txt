EFFECTIVE PERIOD:
14-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   B  -  -  B  B  -
03 2-0   -  C  -  -  -  -
04 2-0   -  D  D  D  -  -
05 2-0   E  E  E  -  -  E
06 2-0   -  -  F  -  F  -
07 0-3   -  G  -  -  G  G
08 0-4   H  H  -  -  -  -
09 0-4   I  I  -  I  -  I
10 0-4   J  -  J  J  J  -
11 0-4   K  K  K  K  K  -
12 0-4   L  L  L  -  L  L
13 0-5   -  M  -  M  -  -
14 0-5   -  -  -  N  -  -
15 0-6   O  -  O  O  -  O
16 0-6   P  -  -  P  -  P
17 0-6   -  Q  -  -  -  Q
18 0-6   -  -  -  R  R   
19 0-6   -  -  S  -  -   
20 1-3   -  T  T  -      
21 2-6   U  U  -  U      
22 2-6   -  V  V         
23 2-6   W  -  X         
24 2-6   X  -            
25 3-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KSQGJ YVQLL JKHVP SZTZV CYWVS E
-------------------------------
