EFFECTIVE PERIOD:
05-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  -  -  -  -
03 0-3   -  -  -  C  -  -
04 0-3   -  -  -  D  D  D
05 0-3   E  E  E  E  E  E
06 0-3   F  -  F  -  F  -
07 0-3   -  -  G  -  G  G
08 0-3   -  H  H  -  H  -
09 0-3   -  -  I  I  I  I
10 0-3   -  -  -  J  -  -
11 0-3   -  K  K  -  -  -
12 0-3   L  L  L  L  -  -
13 0-3   -  M  -  -  M  M
14 0-4   -  N  -  -  N  N
15 0-5   -  -  -  -  -  -
16 0-6   P  -  P  -  P  -
17 0-6   Q  Q  Q  -  -  -
18 0-6   R  R  R  R  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  T  -      
21 0-6   -  U  -  U      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 1-5   X  Y            
25 1-6   Y  -            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

DFZYR BNOSR JPGNO ZRMNJ YOECK L
-------------------------------
