EFFECTIVE PERIOD:
04-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  B  B  -  B  B
03 1-0   C  -  C  -  -  C
04 1-0   -  -  -  D  -  D
05 1-0   -  E  E  E  E  E
06 2-0   -  F  F  F  -  F
07 2-0   -  G  -  -  -  -
08 2-0   H  -  -  -  -  H
09 2-0   I  I  -  -  I  -
10 2-0   J  J  J  J  J  -
11 2-0   K  -  K  K  K  K
12 2-0   -  -  -  -  -  L
13 2-0   M  -  M  M  -  -
14 0-3   N  -  N  -  N  -
15 0-3   -  -  -  -  O  -
16 0-5   -  -  P  P  P  P
17 0-5   Q  Q  -  -  Q  -
18 0-5   R  R  -  -  -   
19 0-5   -  S  S  -  -   
20 0-5   T  T  T  T      
21 0-5   U  -  -  U      
22 0-5   -  V  V         
23 0-6   -  X  -         
24 1-3   -  Y            
25 1-5   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

FJYQV ADKNY KLVHF LPSQS RONQQ F
-------------------------------
