SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF FEB 2031

    01 FEB 2031  00:00-23:59 GMT:  USE KEY UL
    02 FEB 2031  00:00-23:59 GMT:  USE KEY UM
    03 FEB 2031  00:00-23:59 GMT:  USE KEY UN
    04 FEB 2031  00:00-23:59 GMT:  USE KEY UO
    05 FEB 2031  00:00-23:59 GMT:  USE KEY UP
    06 FEB 2031  00:00-23:59 GMT:  USE KEY UQ
    07 FEB 2031  00:00-23:59 GMT:  USE KEY UR
    08 FEB 2031  00:00-23:59 GMT:  USE KEY US
    09 FEB 2031  00:00-23:59 GMT:  USE KEY UT
    10 FEB 2031  00:00-23:59 GMT:  USE KEY UU
    11 FEB 2031  00:00-23:59 GMT:  USE KEY UV
    12 FEB 2031  00:00-23:59 GMT:  USE KEY UW
    13 FEB 2031  00:00-23:59 GMT:  USE KEY UX
    14 FEB 2031  00:00-23:59 GMT:  USE KEY UY
    15 FEB 2031  00:00-23:59 GMT:  USE KEY UZ
    16 FEB 2031  00:00-23:59 GMT:  USE KEY VA
    17 FEB 2031  00:00-23:59 GMT:  USE KEY VB
    18 FEB 2031  00:00-23:59 GMT:  USE KEY VC
    19 FEB 2031  00:00-23:59 GMT:  USE KEY VD
    20 FEB 2031  00:00-23:59 GMT:  USE KEY VE
    21 FEB 2031  00:00-23:59 GMT:  USE KEY VF
    22 FEB 2031  00:00-23:59 GMT:  USE KEY VG
    23 FEB 2031  00:00-23:59 GMT:  USE KEY VH
    24 FEB 2031  00:00-23:59 GMT:  USE KEY VI
    25 FEB 2031  00:00-23:59 GMT:  USE KEY VJ
    26 FEB 2031  00:00-23:59 GMT:  USE KEY VK
    27 FEB 2031  00:00-23:59 GMT:  USE KEY VL
    28 FEB 2031  00:00-23:59 GMT:  USE KEY VM

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   B  -  B  B  -  B
03 1-0   -  -  -  -  C  C
04 1-0   -  D  -  D  -  D
05 1-0   E  -  -  E  E  E
06 2-0   F  F  F  -  F  -
07 2-0   G  G  G  -  G  G
08 2-0   -  H  -  H  -  -
09 2-0   I  -  -  I  -  I
10 2-0   J  J  -  J  J  J
11 2-0   -  -  -  -  -  -
12 2-0   L  -  L  -  -  -
13 2-0   -  -  M  -  -  -
14 0-3   N  -  -  -  -  -
15 0-3   -  O  -  O  O  -
16 0-4   -  -  -  P  P  -
17 0-4   Q  -  -  -  -  Q
18 0-4   R  R  -  R  -   
19 0-4   -  S  S  -  -   
20 0-4   -  -  T  T      
21 0-6   -  U  U  U      
22 1-3   V  V  V         
23 1-4   W  X  X         
24 2-4   X  -            
25 2-4   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JGNQK LVYAN KLOKK LAWPG QDKKN M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 0-3   -  -  B  -  -  B
03 0-4   C  C  C  C  -  C
04 0-4   D  -  -  -  D  D
05 0-4   E  E  -  -  -  -
06 0-4   -  -  -  -  -  F
07 0-5   G  -  -  G  -  -
08 0-5   H  H  H  -  H  -
09 0-5   -  -  I  I  -  I
10 0-5   J  -  -  J  J  -
11 0-5   -  K  K  K  K  K
12 0-5   L  -  L  -  L  L
13 0-6   M  -  -  -  -  M
14 0-6   -  N  -  N  N  N
15 0-6   O  O  -  O  -  -
16 0-6   P  P  -  P  P  -
17 0-6   Q  Q  -  -  -  Q
18 0-6   R  -  R  R  -   
19 0-6   -  S  S  S  S   
20 1-3   T  T  T  -      
21 1-4   -  U  -  U      
22 2-6   -  -  V         
23 4-5   -  X  -         
24 5-6   X  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VTZRP ITKUR LTNDC ZZAAX PNHZG O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  A
02 2-0   -  -  B  -  -  B
03 2-0   C  C  C  C  C  -
04 2-0   -  -  D  D  D  -
05 0-3   -  E  E  -  -  -
06 0-3   F  -  F  -  F  -
07 0-4   G  G  -  -  G  G
08 0-4   -  -  -  H  H  H
09 0-4   I  -  I  -  I  -
10 0-4   -  -  -  -  -  J
11 0-4   K  -  -  K  K  K
12 0-6   L  L  L  -  -  L
13 0-6   M  -  -  -  -  -
14 0-6   N  N  N  N  N  -
15 0-6   O  O  -  -  -  -
16 0-6   -  P  P  P  -  P
17 0-6   -  -  -  -  -  -
18 1-5   -  R  -  -  R   
19 2-4   S  -  S  S  -   
20 2-4   T  T  -  T      
21 2-4   U  U  -  U      
22 2-4   -  -  -         
23 2-6   -  X  X         
24 3-5   X  -            
25 3-6   -  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

SWGQA TRSXD DUOVN PUQNL ZGXEX V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  B  B  -  B  B
03 1-0   C  -  C  -  -  C
04 1-0   -  -  -  D  -  D
05 1-0   -  E  E  E  E  E
06 2-0   -  F  F  F  -  F
07 2-0   -  G  -  -  -  -
08 2-0   H  -  -  -  -  H
09 2-0   I  I  -  -  I  -
10 2-0   J  J  J  J  J  -
11 2-0   K  -  K  K  K  K
12 2-0   -  -  -  -  -  L
13 2-0   M  -  M  M  -  -
14 0-3   N  -  N  -  N  -
15 0-3   -  -  -  -  O  -
16 0-5   -  -  P  P  P  P
17 0-5   Q  Q  -  -  Q  -
18 0-5   R  R  -  -  -   
19 0-5   -  S  S  -  -   
20 0-5   T  T  T  T      
21 0-5   U  -  -  U      
22 0-5   -  V  V         
23 0-6   -  X  -         
24 1-3   -  Y            
25 1-5   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

FJYQV ADKNY KLVHF LPSQS RONQQ F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   B  -  -  -  -  -
03 0-3   -  -  -  C  -  -
04 0-3   -  -  -  D  D  D
05 0-3   E  E  E  E  E  E
06 0-3   F  -  F  -  F  -
07 0-3   -  -  G  -  G  G
08 0-3   -  H  H  -  H  -
09 0-3   -  -  I  I  I  I
10 0-3   -  -  -  J  -  -
11 0-3   -  K  K  -  -  -
12 0-3   L  L  L  L  -  -
13 0-3   -  M  -  -  M  M
14 0-4   -  N  -  -  N  N
15 0-5   -  -  -  -  -  -
16 0-6   P  -  P  -  P  -
17 0-6   Q  Q  Q  -  -  -
18 0-6   R  R  R  R  -   
19 0-6   S  S  -  S  S   
20 0-6   T  T  T  -      
21 0-6   -  U  -  U      
22 0-6   -  -  V         
23 0-6   W  -  X         
24 1-5   X  Y            
25 1-6   Y  -            
26 2-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

DFZYR BNOSR JPGNO ZRMNJ YOECK L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  -  -  -  -  -
03 1-0   C  -  C  -  -  -
04 1-0   D  -  -  D  -  -
05 1-0   -  -  E  -  E  E
06 2-0   -  -  -  -  -  F
07 2-0   G  -  -  G  G  G
08 2-0   H  H  -  -  -  H
09 2-0   -  I  -  -  -  -
10 2-0   -  -  J  -  J  -
11 2-0   -  -  -  -  K  K
12 0-3   L  L  L  L  -  L
13 0-3   M  -  -  -  M  M
14 0-6   N  -  N  N  -  -
15 0-6   O  O  O  O  O  O
16 0-6   P  -  P  P  P  P
17 0-6   Q  Q  -  -  -  -
18 1-2   -  -  R  R  -   
19 1-2   S  -  -  -  -   
20 1-2   -  T  T  T      
21 1-2   U  U  -  U      
22 1-6   -  V  V         
23 2-3   W  X  X         
24 2-4   X  -            
25 3-4   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZUVMK YPTWE ZRLOT CZPXU CJYQQ V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  B  -  B  B  B
03 1-0   -  -  C  -  C  C
04 1-0   D  D  -  D  D  D
05 2-0   E  E  -  E  -  -
06 2-0   F  -  F  -  F  -
07 2-0   -  G  -  -  -  -
08 2-0   -  -  -  -  -  -
09 2-0   I  I  -  I  -  -
10 2-0   -  J  J  J  J  J
11 2-0   -  -  -  -  -  -
12 0-6   L  -  L  L  L  L
13 0-6   M  M  M  M  -  M
14 0-6   N  -  N  N  -  N
15 0-6   -  O  O  -  O  O
16 1-2   P  P  -  -  P  P
17 1-3   Q  Q  Q  Q  -  Q
18 1-6   R  -  R  R  R   
19 1-6   -  S  -  -  -   
20 2-6   T  T  T  -      
21 2-6   -  U  U  -      
22 2-6   -  -  -         
23 2-6   W  -  X         
24 3-4   -  -            
25 3-6   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WAZOA VWJMO AAPCS USONA VOTAJ V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: US
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  B  -  B  B  -
03 1-0   -  -  -  C  -  C
04 1-0   D  -  -  D  D  -
05 1-0   E  E  E  -  E  -
06 1-0   -  -  F  F  -  F
07 2-0   -  -  -  G  -  G
08 2-0   -  H  H  -  -  -
09 2-0   -  I  -  I  I  I
10 2-0   J  -  J  -  -  J
11 0-6   K  K  -  K  K  -
12 0-6   -  L  L  -  L  -
13 0-6   -  M  -  -  M  -
14 0-6   -  N  N  -  -  N
15 0-6   -  -  -  O  -  O
16 1-5   P  P  P  -  -  P
17 1-6   -  -  -  Q  Q  Q
18 2-4   R  -  R  -  R   
19 2-5   -  -  -  -  -   
20 2-5   -  -  T  T      
21 2-5   U  -  -  U      
22 2-5   -  -  V         
23 2-6   -  X  -         
24 2-6   X  -            
25 2-6   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

MOJJL VPKVQ UNAJJ APBOO VASTU G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  -  -  B  B  -
03 1-0   -  C  C  C  -  C
04 1-0   D  -  D  -  D  -
05 1-0   -  E  -  -  -  E
06 1-0   F  F  F  -  -  -
07 2-0   G  -  G  G  -  G
08 0-3   -  -  H  -  -  H
09 0-5   -  I  -  -  I  -
10 0-5   -  J  -  -  J  -
11 0-5   K  -  -  K  K  -
12 0-5   L  L  -  -  -  L
13 0-5   -  M  M  M  -  M
14 0-5   N  -  N  N  N  -
15 0-5   O  O  -  -  O  O
16 0-6   P  -  -  -  P  P
17 0-6   Q  Q  -  Q  Q  -
18 0-6   -  -  R  -  R   
19 0-6   S  -  S  -  S   
20 1-3   T  T  T  -      
21 1-6   U  U  -  U      
22 2-3   V  -  V         
23 4-5   -  -  -         
24 5-6   X  Y            
25 5-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LQSTZ AWLKS IQCMP EWVQL BXAPT V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  B  -  B  B  B
03 1-0   -  -  -  -  -  -
04 2-0   D  D  -  D  D  -
05 2-0   -  E  -  E  E  -
06 2-0   -  F  F  -  -  F
07 0-4   G  -  -  -  G  -
08 0-4   -  H  H  H  H  -
09 0-4   -  I  I  -  -  I
10 0-4   -  -  J  J  J  J
11 0-4   -  -  -  -  K  -
12 0-5   -  -  L  L  -  L
13 0-5   -  M  M  -  -  M
14 0-5   N  N  N  -  -  -
15 0-5   O  O  -  -  -  -
16 1-2   -  -  -  P  -  P
17 1-3   -  -  Q  -  -  Q
18 1-3   R  R  R  R  R   
19 1-3   S  S  -  -  -   
20 1-4   T  T  -  T      
21 1-4   U  U  -  -      
22 1-4   V  -  V         
23 1-4   W  -  -         
24 2-3   -  Y            
25 2-5   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

JALWQ WVQET SUUJU OQSXJ JEEJV V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  -  -  -  B  B
03 0-3   -  C  C  -  C  C
04 0-3   -  -  D  D  D  -
05 0-3   E  E  -  E  -  -
06 0-3   F  F  -  -  -  -
07 0-3   G  G  G  -  -  -
08 0-3   -  H  -  H  -  -
09 0-3   I  I  I  -  I  -
10 0-4   J  -  J  J  -  J
11 0-5   K  -  -  -  K  -
12 0-5   L  L  L  L  L  -
13 0-5   -  -  M  -  M  -
14 0-5   -  N  -  -  N  -
15 0-5   O  O  -  -  -  O
16 0-6   P  -  P  P  P  P
17 0-6   -  Q  Q  -  -  Q
18 0-6   R  -  R  R  R   
19 0-6   S  -  -  S  -   
20 1-3   -  T  -  -      
21 1-3   U  -  -  U      
22 1-3   -  V  V         
23 1-3   -  X  X         
24 1-5   -  Y            
25 2-3   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NGUQX OMGYY SNQNZ HAHUU OZULH Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  B  B  B  -  B
03 1-0   C  -  -  C  -  -
04 1-0   -  D  D  D  -  -
05 1-0   E  E  E  -  -  -
06 1-0   -  F  F  F  F  -
07 1-0   -  -  -  -  G  G
08 1-0   -  H  H  -  -  H
09 1-0   -  I  -  -  I  I
10 1-0   J  -  -  J  -  J
11 1-0   K  -  K  K  K  -
12 2-0   -  -  L  L  -  -
13 0-3   -  -  -  -  -  -
14 0-3   N  N  -  -  -  -
15 0-3   O  -  O  -  O  -
16 0-3   -  -  -  -  P  P
17 0-3   -  Q  Q  -  Q  Q
18 0-3   -  -  R  R  -   
19 0-4   S  -  S  -  -   
20 0-4   T  T  -  -      
21 0-4   U  -  U  -      
22 0-5   -  V  V         
23 0-6   W  X  -         
24 1-3   X  -            
25 2-5   -  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZHBVV AAVXE GNGWP VEHHG WSDDX M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 2-0   B  B  B  B  B  -
03 2-0   C  -  C  -  -  -
04 0-4   D  D  D  -  D  D
05 0-4   E  E  -  -  E  E
06 0-4   -  F  -  -  -  F
07 0-4   -  G  -  -  G  -
08 0-5   -  -  -  H  -  -
09 0-5   -  I  I  I  I  I
10 0-5   J  J  -  -  -  -
11 0-5   -  -  -  -  K  K
12 0-5   -  -  -  L  -  L
13 0-5   M  M  M  -  M  -
14 0-5   -  -  N  N  -  N
15 0-5   O  O  O  O  -  -
16 0-6   P  P  P  P  -  -
17 0-6   Q  Q  Q  Q  -  Q
18 0-6   -  R  R  -  -   
19 0-6   S  -  -  -  S   
20 1-2   -  -  -  T      
21 2-6   U  -  U  U      
22 3-5   -  V  V         
23 4-5   -  -  -         
24 4-5   -  -            
25 4-5   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KANSJ BFVJO QUWCR JVQSD AZJSJ O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  A
02 2-0   B  -  -  B  B  -
03 2-0   -  C  -  -  -  -
04 2-0   -  D  D  D  -  -
05 2-0   E  E  E  -  -  E
06 2-0   -  -  F  -  F  -
07 0-3   -  G  -  -  G  G
08 0-4   H  H  -  -  -  -
09 0-4   I  I  -  I  -  I
10 0-4   J  -  J  J  J  -
11 0-4   K  K  K  K  K  -
12 0-4   L  L  L  -  L  L
13 0-5   -  M  -  M  -  -
14 0-5   -  -  -  N  -  -
15 0-6   O  -  O  O  -  O
16 0-6   P  -  -  P  -  P
17 0-6   -  Q  -  -  -  Q
18 0-6   -  -  -  R  R   
19 0-6   -  -  S  -  -   
20 1-3   -  T  T  -      
21 2-6   U  U  -  U      
22 2-6   -  V  V         
23 2-6   W  -  X         
24 2-6   X  -            
25 3-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

KSQGJ YVQLL JKHVP SZTZV CYWVS E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  -  -  B  -
03 1-0   -  C  C  -  -  C
04 1-0   -  -  -  -  -  -
05 1-0   -  E  -  E  E  E
06 1-0   -  F  F  -  F  -
07 2-0   -  G  -  G  -  -
08 2-0   H  H  H  H  H  H
09 2-0   -  -  -  I  -  I
10 2-0   -  -  J  J  -  J
11 0-3   -  -  K  K  K  K
12 0-3   -  -  L  -  L  -
13 0-6   M  M  -  -  M  -
14 0-6   N  -  N  -  -  -
15 0-6   -  O  O  -  -  O
16 0-6   P  P  P  -  -  P
17 0-6   Q  -  Q  Q  -  Q
18 1-3   R  -  -  -  -   
19 1-4   S  S  S  S  S   
20 1-6   -  T  -  -      
21 1-6   -  U  -  U      
22 1-6   -  V  -         
23 1-6   W  X  X         
24 2-3   X  -            
25 2-6   -  Z            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

AVPPR SRXTP RJQIJ URUTG FOJZV L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  -  B  -  -  -
03 1-0   C  -  -  -  -  -
04 1-0   D  -  D  -  -  -
05 1-0   E  E  -  E  -  E
06 0-3   F  -  -  -  F  F
07 0-3   G  -  -  G  -  -
08 0-5   -  H  H  H  H  H
09 0-5   -  -  I  -  I  -
10 0-5   -  -  J  J  -  J
11 0-5   K  K  K  -  -  -
12 0-5   L  L  -  L  L  L
13 0-6   M  M  M  M  -  M
14 0-6   N  -  N  N  N  N
15 0-6   -  -  -  O  O  -
16 0-6   -  P  P  P  -  P
17 0-6   Q  Q  -  Q  -  -
18 1-5   -  -  -  -  -   
19 1-5   S  S  -  -  S   
20 1-5   T  -  T  T      
21 1-5   -  U  U  U      
22 1-6   -  -  -         
23 2-5   W  -  X         
24 3-4   -  Y            
25 3-5   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PSVYM GFAMF AKPVP SYOMV LSAQR F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   B  -  -  B  B  -
03 2-0   C  -  C  C  C  C
04 2-0   -  -  -  D  D  D
05 2-0   -  -  E  -  -  E
06 2-0   -  -  F  -  F  F
07 2-0   -  -  G  G  G  G
08 0-3   -  H  H  H  -  H
09 0-3   -  I  -  -  I  -
10 0-3   J  -  -  J  -  -
11 0-3   -  -  K  K  -  -
12 0-3   L  -  L  -  -  -
13 0-5   M  M  M  M  M  M
14 0-5   -  -  N  -  -  N
15 0-5   -  -  O  -  -  -
16 0-5   -  P  P  -  -  -
17 0-5   Q  -  -  -  Q  -
18 1-3   R  R  -  R  -   
19 1-4   S  -  -  -  -   
20 2-4   T  T  -  T      
21 2-5   -  U  U  U      
22 3-4   -  V  -         
23 3-5   W  -  -         
24 3-5   X  -            
25 3-5   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

NAMLB TIVDM AUNMR NTLKL SNMVR L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  A
02 2-0   -  B  -  B  B  B
03 2-0   -  C  -  -  C  -
04 2-0   -  D  D  D  -  D
05 0-5   E  -  -  -  -  E
06 0-5   -  F  F  F  -  -
07 0-5   -  -  G  -  G  -
08 0-5   H  H  H  H  H  H
09 0-5   I  I  I  -  I  -
10 0-5   -  J  J  -  -  J
11 0-6   K  K  K  -  K  -
12 0-6   L  -  -  -  -  L
13 0-6   -  -  -  M  M  -
14 0-6   N  -  -  N  -  N
15 0-6   O  -  -  -  -  -
16 1-2   P  P  P  P  P  -
17 1-2   Q  -  -  -  Q  Q
18 1-3   -  -  -  R  -   
19 1-4   -  S  S  S  S   
20 2-4   -  T  -  T      
21 2-4   U  U  -  -      
22 2-5   V  V  V         
23 2-5   W  X  X         
24 2-5   X  -            
25 2-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GAAJS VUAUR AGNMZ OAAAN ZOGQW K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   -  B  -  B  -  -
03 1-0   C  -  -  -  -  C
04 1-0   D  D  D  -  -  D
05 1-0   E  E  -  E  -  -
06 1-0   F  -  -  -  -  F
07 1-0   G  -  G  -  G  -
08 1-0   -  H  -  -  -  H
09 1-0   I  I  I  -  I  I
10 1-0   J  J  -  J  J  J
11 1-0   -  K  K  K  -  K
12 2-0   L  L  L  -  L  -
13 0-3   -  M  M  -  -  M
14 0-3   -  -  -  N  -  -
15 0-3   -  O  O  -  O  -
16 0-4   P  P  P  -  -  -
17 0-5   -  Q  Q  -  -  -
18 0-5   R  R  -  -  R   
19 0-5   S  -  S  S  S   
20 0-5   T  -  -  T      
21 0-5   U  U  U  U      
22 0-5   -  -  V         
23 0-5   W  -  X         
24 0-5   -  -            
25 0-6   -  Z            
26 2-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

VGRYK OLYNG EKCQV NXTGX QFZBD N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  C  C  C  -
04 1-0   -  -  -  D  D  D
05 1-0   E  -  E  E  E  -
06 1-0   -  F  -  F  -  F
07 2-0   -  -  G  -  G  G
08 2-0   -  -  -  -  -  -
09 2-0   I  I  -  I  I  -
10 2-0   -  J  -  J  J  J
11 0-4   -  -  -  -  -  -
12 0-6   L  L  L  -  -  -
13 0-6   -  M  M  M  -  M
14 0-6   N  -  N  N  -  N
15 0-6   O  -  -  O  -  O
16 1-2   -  -  P  P  -  -
17 1-6   -  -  Q  -  Q  -
18 1-6   R  -  -  -  -   
19 1-6   S  S  S  S  S   
20 1-6   T  -  T  -      
21 2-4   -  U  -  U      
22 3-4   V  -  V         
23 3-5   W  -  -         
24 3-6   X  -            
25 3-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

HEWWX DWTSL MWUZH KTFEX RPWMW O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 0-3   -  B  B  B  -  B
03 0-3   -  -  -  -  C  -
04 0-3   -  -  D  -  -  -
05 0-3   E  -  E  E  E  -
06 0-3   -  -  -  F  F  -
07 0-3   G  G  G  G  -  -
08 0-3   H  -  -  -  -  H
09 0-3   I  I  I  I  I  I
10 0-3   J  -  J  J  J  J
11 0-3   K  K  -  K  -  K
12 0-3   -  L  L  -  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  N  -  N  N  N
15 0-5   -  O  O  -  -  -
16 0-5   -  -  P  -  P  P
17 0-5   -  Q  Q  Q  -  Q
18 0-6   R  R  R  -  -   
19 0-6   -  S  -  -  S   
20 0-6   T  -  -  T      
21 0-6   -  -  -  -      
22 0-6   V  -  -         
23 0-6   -  X  -         
24 2-4   X  -            
25 2-5   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JTUUU HIOPA HLHTT JYPUO IUJPU U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  -  -  B  -  -
03 1-0   C  -  -  -  C  -
04 1-0   -  D  D  -  -  D
05 1-0   -  -  E  -  E  E
06 1-0   -  -  F  F  -  F
07 2-0   G  -  -  -  -  G
08 0-3   -  H  H  H  H  H
09 0-4   I  -  -  I  -  I
10 0-4   -  -  J  J  J  -
11 0-4   -  -  -  -  -  K
12 0-4   -  -  L  -  L  -
13 0-6   -  M  -  -  -  -
14 0-6   -  -  N  -  N  -
15 0-6   O  -  -  O  O  -
16 0-6   P  -  -  P  P  -
17 0-6   Q  Q  -  Q  Q  -
18 0-6   -  R  R  R  -   
19 0-6   -  -  S  S  -   
20 1-4   T  T  -  -      
21 1-6   U  -  -  -      
22 1-6   V  V  V         
23 1-6   -  -  X         
24 1-6   X  Y            
25 2-3   Y  Z            
26 2-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UKIIS TPOMO AOSUU NFZHN ZCAJR P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 2-0   B  B  -  B  -  -
03 2-0   C  -  C  -  -  C
04 2-0   D  -  -  D  -  -
05 0-3   -  -  E  -  E  E
06 0-3   F  F  -  -  F  F
07 0-5   -  G  G  -  G  G
08 0-5   -  -  H  H  H  H
09 0-5   -  I  -  -  -  -
10 0-5   J  J  -  -  J  -
11 0-5   K  K  -  -  K  -
12 0-5   -  -  L  -  -  L
13 0-5   M  -  -  -  M  -
14 0-6   N  N  N  N  -  N
15 0-6   O  -  O  O  -  -
16 0-6   P  P  -  P  -  P
17 0-6   -  Q  -  -  Q  Q
18 1-4   -  R  -  R  R   
19 2-3   -  S  -  S  -   
20 2-6   -  -  T  T      
21 3-5   -  -  -  U      
22 3-5   V  V  V         
23 3-5   W  X  X         
24 3-5   -  -            
25 4-5   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

HAJNF TWNPP DKATW MPUSF YMIPA N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  B  -  B  B  B
03 1-0   -  -  -  -  -  -
04 1-0   -  D  D  -  D  D
05 1-0   E  E  -  -  E  -
06 1-0   -  F  -  F  F  F
07 2-0   -  G  G  -  -  G
08 2-0   H  -  H  H  H  -
09 0-3   I  -  -  -  -  I
10 0-3   -  J  -  -  J  -
11 0-3   -  K  -  -  -  -
12 0-3   -  -  -  L  -  -
13 0-3   -  M  M  M  M  M
14 0-3   N  N  -  N  -  -
15 0-3   O  O  O  O  -  -
16 0-4   P  -  -  P  P  -
17 0-4   -  -  -  -  Q  Q
18 0-4   R  -  R  R  -   
19 0-4   S  -  -  S  S   
20 0-4   T  T  T  -      
21 0-4   -  U  U  U      
22 0-4   V  V  V         
23 0-4   W  -  -         
24 0-5   -  Y            
25 0-6   -  -            
26 1-2   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

RPQOO MZSRM QHFJL LSLGI MLONA K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  -  -  -  B  -
03 1-0   C  -  C  C  -  -
04 2-0   -  D  -  -  -  -
05 2-0   -  -  E  -  E  E
06 2-0   -  -  -  -  F  -
07 2-0   G  G  G  -  -  G
08 2-0   H  -  -  H  -  H
09 2-0   -  -  I  I  -  I
10 0-4   -  J  -  -  -  J
11 0-4   -  -  -  -  K  K
12 0-4   L  -  L  -  L  -
13 0-4   -  M  M  M  M  M
14 0-4   -  N  -  -  -  N
15 0-4   O  -  O  O  -  O
16 1-2   P  P  P  P  -  -
17 1-2   -  Q  Q  -  -  -
18 1-2   R  R  R  R  -   
19 1-2   S  S  -  S  S   
20 1-3   -  T  -  -      
21 1-4   U  -  -  U      
22 1-5   -  -  V         
23 1-5   -  -  X         
24 1-6   -  -            
25 2-4   Y  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UPAAN ZIDMN SJENN ALZNE PLMSU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  -  B  B  -
03 1-0   -  -  -  C  -  C
04 2-0   -  -  D  D  -  -
05 2-0   -  E  -  E  E  E
06 2-0   -  -  F  -  -  -
07 0-5   G  -  G  G  -  -
08 0-5   -  -  -  -  H  -
09 0-5   I  -  I  -  I  -
10 0-5   J  -  J  J  J  J
11 0-5   -  -  K  K  -  -
12 0-5   -  L  -  L  -  L
13 0-6   M  -  M  M  M  M
14 0-6   N  N  -  -  N  N
15 0-6   -  -  O  -  -  O
16 0-6   -  P  -  -  P  -
17 0-6   Q  Q  -  -  Q  Q
18 1-5   R  -  -  R  -   
19 1-5   -  S  S  -  S   
20 1-5   T  T  -  T      
21 1-5   -  U  U  -      
22 1-6   -  -  -         
23 2-3   W  X  X         
24 2-5   -  -            
25 2-6   Y  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

AAAGA RLRRX PTQCZ RUQJQ XTGWV Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  -  B  -  -  B
03 1-0   -  C  -  C  C  C
04 1-0   -  D  -  D  -  D
05 1-0   E  -  -  -  E  -
06 1-0   F  -  F  -  -  -
07 1-0   G  G  G  G  -  G
08 1-0   -  H  H  -  -  -
09 1-0   I  I  -  I  I  -
10 2-0   J  -  J  J  J  J
11 2-0   -  K  -  K  -  -
12 2-0   L  -  -  L  L  L
13 2-0   M  M  -  M  M  -
14 2-0   -  -  -  -  N  N
15 2-0   -  O  O  -  O  O
16 2-0   -  P  P  P  -  P
17 2-0   -  -  -  Q  Q  -
18 2-0   R  -  R  R  R   
19 2-0   -  -  -  S  -   
20 2-0   T  -  -  -      
21 2-0   U  U  U  -      
22 0-3   -  -  V         
23 0-3   W  X  -         
24 0-5   -  Y            
25 0-6   -  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

ORNDN MOLPM CPLML MOZOZ MPLUE R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-FEB-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: VM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   -  A  A  A  A  A
02 0-4   -  -  B  B  B  B
03 0-4   C  -  -  C  C  -
04 0-4   -  D  -  -  D  -
05 0-4   -  -  -  E  E  E
06 0-4   F  -  F  -  -  -
07 0-5   G  G  -  G  -  -
08 0-5   H  -  -  H  -  -
09 0-5   I  -  I  I  I  I
10 0-5   -  -  J  J  J  J
11 0-6   K  -  K  -  -  K
12 0-6   -  -  L  L  L  -
13 0-6   -  M  -  -  M  M
14 0-6   -  -  N  N  N  N
15 0-6   -  O  -  -  -  -
16 1-3   -  -  P  -  -  P
17 1-5   -  -  Q  -  -  Q
18 1-5   R  R  R  -  -   
19 1-6   -  -  S  -  S   
20 2-3   -  -  -  T      
21 3-5   U  U  -  -      
22 4-5   V  V  -         
23 4-5   -  -  -         
24 4-5   -  Y            
25 4-5   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

USUKC MLZSV VALOA UZAQU AORKH K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
