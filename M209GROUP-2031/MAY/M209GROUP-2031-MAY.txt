SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF MAY 2031

    01 MAY 2031  00:00-23:59 GMT:  USE KEY XW
    02 MAY 2031  00:00-23:59 GMT:  USE KEY XX
    03 MAY 2031  00:00-23:59 GMT:  USE KEY XY
    04 MAY 2031  00:00-23:59 GMT:  USE KEY XZ
    05 MAY 2031  00:00-23:59 GMT:  USE KEY YA
    06 MAY 2031  00:00-23:59 GMT:  USE KEY YB
    07 MAY 2031  00:00-23:59 GMT:  USE KEY YC
    08 MAY 2031  00:00-23:59 GMT:  USE KEY YD
    09 MAY 2031  00:00-23:59 GMT:  USE KEY YE
    10 MAY 2031  00:00-23:59 GMT:  USE KEY YF
    11 MAY 2031  00:00-23:59 GMT:  USE KEY YG
    12 MAY 2031  00:00-23:59 GMT:  USE KEY YH
    13 MAY 2031  00:00-23:59 GMT:  USE KEY YI
    14 MAY 2031  00:00-23:59 GMT:  USE KEY YJ
    15 MAY 2031  00:00-23:59 GMT:  USE KEY YK
    16 MAY 2031  00:00-23:59 GMT:  USE KEY YL
    17 MAY 2031  00:00-23:59 GMT:  USE KEY YM
    18 MAY 2031  00:00-23:59 GMT:  USE KEY YN
    19 MAY 2031  00:00-23:59 GMT:  USE KEY YO
    20 MAY 2031  00:00-23:59 GMT:  USE KEY YP
    21 MAY 2031  00:00-23:59 GMT:  USE KEY YQ
    22 MAY 2031  00:00-23:59 GMT:  USE KEY YR
    23 MAY 2031  00:00-23:59 GMT:  USE KEY YS
    24 MAY 2031  00:00-23:59 GMT:  USE KEY YT
    25 MAY 2031  00:00-23:59 GMT:  USE KEY YU
    26 MAY 2031  00:00-23:59 GMT:  USE KEY YV
    27 MAY 2031  00:00-23:59 GMT:  USE KEY YW
    28 MAY 2031  00:00-23:59 GMT:  USE KEY YX
    29 MAY 2031  00:00-23:59 GMT:  USE KEY YY
    30 MAY 2031  00:00-23:59 GMT:  USE KEY YZ
    31 MAY 2031  00:00-23:59 GMT:  USE KEY ZA

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-4   -  A  -  -  A  A
02 0-4   -  -  -  -  -  B
03 0-4   -  -  C  C  -  -
04 0-4   -  -  D  -  -  -
05 0-4   -  E  E  -  E  -
06 0-4   F  F  F  -  -  -
07 0-4   -  -  -  G  -  -
08 0-5   -  H  -  H  H  H
09 0-5   I  I  -  -  I  -
10 0-5   -  J  -  -  J  J
11 0-5   K  K  -  K  K  K
12 0-5   -  L  L  L  -  -
13 0-6   -  M  -  M  M  M
14 0-6   -  -  N  N  -  N
15 0-6   -  -  -  O  -  O
16 1-2   P  -  -  P  -  P
17 1-6   -  -  Q  -  Q  -
18 2-5   R  R  -  -  R   
19 2-6   S  -  S  -  -   
20 3-6   -  T  -  -      
21 4-5   U  -  U  U      
22 4-6   -  V  V         
23 4-6   W  X  -         
24 4-6   X  Y            
25 4-6   Y  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UTUVL TINAD BOJAL WASKV AVVPZ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  -  B  -  B  B
03 1-0   -  -  C  C  -  C
04 1-0   D  D  D  D  -  D
05 1-0   E  -  E  E  -  E
06 1-0   -  -  -  F  F  F
07 2-0   G  G  G  G  G  G
08 2-0   H  H  H  -  H  -
09 2-0   I  I  I  -  I  -
10 2-0   J  -  -  -  -  J
11 0-3   K  -  -  -  K  -
12 0-4   -  -  L  -  -  L
13 0-4   -  M  -  M  M  M
14 0-4   -  N  N  N  -  -
15 0-4   -  O  -  -  -  -
16 0-4   -  -  -  -  P  P
17 0-4   Q  -  Q  -  -  -
18 0-4   -  R  -  R  -   
19 0-4   S  S  -  S  -   
20 0-5   T  -  -  T      
21 0-6   U  U  -  -      
22 1-2   -  V  -         
23 1-6   W  -  X         
24 2-4   -  Y            
25 2-4   -  -            
26 2-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

RVCQP UPWDU KZOPZ TEQSQ IUNKH K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  B  B  -  B  -
03 1-0   C  C  -  -  C  -
04 2-0   D  -  D  -  D  -
05 2-0   -  E  -  -  E  E
06 2-0   F  -  F  F  -  F
07 2-0   G  G  G  -  G  G
08 0-3   -  H  H  -  H  H
09 0-3   I  -  I  I  -  -
10 0-3   -  -  -  J  J  J
11 0-3   K  K  -  -  K  -
12 0-3   L  L  -  -  L  -
13 0-3   M  -  -  M  -  -
14 0-4   -  N  N  N  -  N
15 0-4   O  -  -  O  -  O
16 1-3   P  -  -  -  -  -
17 1-4   Q  Q  Q  -  Q  -
18 1-4   R  -  R  R  R   
19 1-5   -  S  -  -  -   
20 2-3   -  T  T  T      
21 2-4   -  -  -  U      
22 2-4   -  V  -         
23 2-4   W  -  -         
24 2-4   -  Y            
25 4-5   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WOTIQ QLUJQ JQMWP TOAXJ AUTVM J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  A  -
02 0-3   B  B  B  B  B  B
03 0-3   C  -  C  C  -  C
04 0-3   -  D  D  -  D  -
05 0-3   E  -  -  -  -  -
06 0-3   F  -  -  -  F  -
07 0-4   -  G  -  G  -  -
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  I  -  I
10 0-5   -  -  -  J  -  -
11 0-5   K  -  -  -  K  K
12 0-5   -  L  -  -  -  L
13 0-6   -  M  M  M  M  M
14 0-6   -  N  -  N  N  N
15 0-6   O  O  -  -  -  O
16 0-6   P  -  P  -  -  -
17 0-6   Q  -  -  Q  Q  -
18 1-2   -  -  R  -  -   
19 1-3   S  S  -  S  -   
20 1-3   -  T  -  -      
21 1-4   -  U  U  U      
22 3-5   -  -  -         
23 3-5   W  -  X         
24 3-5   -  Y            
25 3-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NUVKX IFNRA HVNRA NHXZK NURAI X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 1-0   -  -  B  -  -  B
03 1-0   -  C  -  -  C  -
04 1-0   D  -  -  D  -  -
05 1-0   E  -  -  E  E  E
06 1-0   -  -  -  -  -  F
07 1-0   G  G  -  -  -  G
08 1-0   H  H  -  H  H  H
09 0-4   -  I  I  -  I  I
10 0-5   J  -  J  J  J  J
11 0-5   -  K  K  K  K  -
12 0-5   -  L  -  -  L  L
13 0-6   M  -  M  -  -  -
14 0-6   -  N  -  N  N  -
15 0-6   -  -  O  O  -  O
16 0-6   P  -  -  -  P  -
17 0-6   -  Q  Q  Q  Q  -
18 0-6   -  -  -  R  -   
19 0-6   S  -  S  -  -   
20 0-6   T  -  -  T      
21 0-6   -  U  U  U      
22 0-6   -  -  V         
23 0-6   -  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 2-3   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

LPFJZ FAPAJ MWROU YRMAF JRCMR N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  -  -
02 2-0   -  B  B  B  B  B
03 2-0   C  C  C  C  C  -
04 2-0   -  -  -  -  -  D
05 0-3   -  -  -  E  -  E
06 0-4   F  -  -  -  F  -
07 0-4   G  G  G  G  -  G
08 0-4   H  -  -  H  -  H
09 0-4   -  I  -  -  -  -
10 0-4   -  -  -  J  J  -
11 0-4   -  K  K  -  -  K
12 0-4   -  -  L  L  L  -
13 0-4   -  -  M  M  M  M
14 0-4   N  -  -  N  -  -
15 0-5   O  O  O  O  -  O
16 0-6   P  P  P  -  -  P
17 0-6   Q  -  -  -  -  Q
18 0-6   R  -  R  R  R   
19 0-6   -  -  S  -  S   
20 0-6   T  -  -  -      
21 0-6   -  U  -  U      
22 0-6   V  V  V         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 1-2   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

ZQVZM TQGYD AZFZJ ILQKQ LYTSC R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  -  -  -  -  -
03 1-0   -  C  C  -  C  C
04 2-0   D  -  D  D  -  D
05 2-0   E  -  -  -  E  E
06 2-0   -  -  -  F  F  -
07 0-3   G  -  -  -  -  G
08 0-3   -  -  H  -  -  -
09 0-3   -  I  -  I  I  -
10 0-3   J  -  -  J  J  J
11 0-3   K  -  K  K  -  K
12 0-3   L  -  -  -  -  -
13 0-3   M  -  -  M  M  M
14 0-5   N  N  N  N  -  -
15 0-6   -  O  -  -  -  O
16 0-6   P  P  P  -  P  P
17 0-6   -  -  Q  Q  Q  Q
18 0-6   -  -  R  -  R   
19 0-6   -  S  S  -  -   
20 1-3   T  T  T  T      
21 1-3   -  U  -  -      
22 1-3   V  V  V         
23 1-3   -  X  -         
24 1-6   -  -            
25 2-5   -  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

ZGSLA XAFRG AGRXI HIXAL ROOXN R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  B  -  -  -  B
03 1-0   -  -  C  C  -  C
04 1-0   -  -  D  -  -  -
05 1-0   E  -  -  -  -  -
06 2-0   F  F  -  F  -  -
07 2-0   G  G  -  G  G  -
08 2-0   H  -  H  H  -  H
09 2-0   -  I  I  -  I  I
10 2-0   -  -  J  J  J  J
11 2-0   K  K  -  -  -  K
12 0-3   -  L  -  L  -  L
13 0-3   M  M  M  M  -  M
14 0-3   -  -  N  N  N  -
15 0-3   -  -  O  -  O  -
16 0-3   P  -  -  -  -  -
17 0-3   -  Q  -  Q  Q  Q
18 0-3   R  R  R  R  R   
19 0-3   S  S  -  -  S   
20 0-3   -  -  T  T      
21 0-5   U  U  -  -      
22 1-2   -  -  V         
23 1-3   W  -  -         
24 1-3   X  -            
25 1-3   -  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TNRQU KVLUB ZAYLC KRATU ARVZO C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  -
02 2-0   -  B  -  -  -  -
03 2-0   C  C  -  C  -  C
04 2-0   D  D  -  -  D  D
05 0-3   E  E  E  E  E  -
06 0-3   F  -  F  F  F  F
07 0-3   -  -  -  G  G  G
08 0-3   H  -  H  -  -  -
09 0-3   I  I  -  -  -  I
10 0-5   J  -  J  -  J  -
11 0-5   K  -  K  K  K  K
12 0-5   -  -  -  -  L  -
13 0-5   -  M  -  M  M  -
14 0-5   N  N  N  N  -  N
15 0-6   -  -  O  O  -  -
16 1-3   -  P  P  -  -  -
17 1-4   Q  -  Q  -  Q  Q
18 1-6   -  R  -  R  R   
19 2-5   S  -  S  -  -   
20 2-6   -  T  -  T      
21 3-5   U  -  -  U      
22 3-5   V  -  -         
23 3-5   -  X  X         
24 3-5   X  Y            
25 3-6   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

WZNVZ ZRAIV QPTVZ VCVMZ WCAVZ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  -  B  B  B  B
03 1-0   -  C  -  C  C  -
04 1-0   D  -  -  -  -  -
05 1-0   -  E  -  E  E  E
06 1-0   F  -  F  -  F  F
07 0-4   -  G  -  -  -  G
08 0-4   -  -  -  H  H  -
09 0-4   I  -  -  I  -  I
10 0-4   J  J  J  J  J  J
11 0-4   K  -  K  K  K  -
12 0-6   L  L  -  -  L  -
13 0-6   M  -  M  -  M  M
14 0-6   -  -  N  N  -  N
15 0-6   O  -  O  -  -  -
16 1-3   P  P  -  P  -  -
17 1-4   -  -  Q  Q  -  Q
18 2-5   -  -  R  -  -   
19 3-5   S  -  S  -  S   
20 3-6   T  -  T  T      
21 3-6   -  U  U  -      
22 3-6   -  V  V         
23 4-6   -  X  -         
24 4-6   X  Y            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NQLZU EOUVM TZNTH OYMMA VHVUT Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   -  -  -  -  -  B
03 2-0   -  C  -  -  C  -
04 2-0   D  D  D  -  -  -
05 2-0   -  -  E  -  -  E
06 2-0   -  F  F  F  -  -
07 2-0   G  -  -  G  -  G
08 0-3   H  -  -  H  -  -
09 0-3   -  I  I  I  I  I
10 0-3   -  J  J  J  J  -
11 0-3   K  -  -  -  -  K
12 0-6   -  -  L  L  L  -
13 0-6   M  -  M  M  -  -
14 0-6   N  N  -  -  N  N
15 0-6   -  O  -  -  O  O
16 0-6   P  -  P  P  -  P
17 0-6   -  -  -  Q  Q  Q
18 1-2   R  -  R  -  R   
19 1-2   S  -  S  -  -   
20 1-4   -  -  T  T      
21 1-6   U  -  U  U      
22 2-3   -  V  -         
23 2-3   W  -  -         
24 2-3   -  Y            
25 2-3   Y  Z            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

WZPNQ VQUJP UWZXU QMNOQ UVQMP Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  C  C  C
04 2-0   -  D  D  -  -  -
05 0-3   E  -  E  -  -  -
06 0-4   F  -  F  -  -  -
07 0-5   G  -  G  G  G  G
08 0-5   H  -  -  H  H  -
09 0-5   I  -  I  -  I  I
10 0-5   -  J  -  J  -  J
11 0-5   -  -  K  K  K  K
12 0-5   L  -  L  L  -  L
13 0-6   M  -  M  -  -  -
14 0-6   -  N  -  N  N  -
15 0-6   O  O  -  O  O  -
16 0-6   -  P  P  P  P  P
17 0-6   -  -  -  -  -  -
18 0-6   -  R  R  R  -   
19 0-6   S  S  -  S  -   
20 0-6   -  -  -  -      
21 0-6   -  U  -  -      
22 1-2   -  V  V         
23 1-5   -  -  -         
24 2-3   -  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OPMQN MDOLS LEGSN ZZQZF ELZOP H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  -
02 1-0   B  B  B  B  B  B
03 1-0   -  C  C  C  -  -
04 1-0   -  -  -  D  D  -
05 1-0   -  E  E  -  -  -
06 2-0   -  F  F  F  F  -
07 2-0   G  G  G  -  -  -
08 2-0   H  -  -  H  H  H
09 2-0   I  -  -  -  I  -
10 0-3   -  -  J  J  -  J
11 0-3   K  K  K  K  K  K
12 0-4   -  L  L  -  L  L
13 0-4   M  M  -  -  M  -
14 0-4   -  N  N  N  -  N
15 0-4   -  -  -  -  O  O
16 1-2   P  P  -  -  P  -
17 1-2   -  -  -  Q  Q  -
18 1-2   R  -  R  R  -   
19 1-2   -  -  S  -  -   
20 1-3   T  -  T  -      
21 1-3   -  U  -  -      
22 1-6   -  -  -         
23 1-6   -  -  -         
24 2-4   X  Y            
25 3-4   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

RRMDW LOZOS WTZJN PVTUW IWOWJ J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   -  -  B  -  B  -
03 1-0   C  C  C  -  -  C
04 1-0   D  -  -  D  D  D
05 1-0   E  E  -  E  E  -
06 1-0   F  F  -  -  F  -
07 0-3   -  -  -  G  -  -
08 0-3   H  H  -  H  H  H
09 0-3   -  -  I  -  I  I
10 0-3   -  -  J  J  -  -
11 0-4   K  -  -  -  K  K
12 0-5   -  -  L  -  -  L
13 0-5   -  M  M  M  M  M
14 0-5   -  -  N  -  N  -
15 0-5   O  -  -  -  -  O
16 0-5   P  P  P  P  -  -
17 0-5   -  -  Q  -  -  Q
18 0-5   R  R  -  R  -   
19 0-6   S  -  S  S  -   
20 1-3   -  -  -  T      
21 1-5   -  -  -  U      
22 1-5   -  -  V         
23 1-5   -  X  -         
24 1-5   X  -            
25 2-5   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SOZSC HDBSO IRTUN WUCAH LAFIB P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  B  -  -  B  B
03 1-0   C  C  -  C  -  C
04 1-0   D  -  D  D  -  -
05 1-0   -  -  E  E  E  E
06 1-0   F  F  F  -  F  -
07 0-4   -  G  -  G  -  -
08 0-4   -  -  -  H  H  H
09 0-4   -  -  -  -  I  -
10 0-4   -  -  -  -  -  -
11 0-5   -  K  K  K  -  K
12 0-6   -  -  L  -  -  L
13 0-6   M  M  -  -  -  M
14 0-6   N  -  N  N  N  -
15 0-6   O  O  -  -  -  O
16 0-6   -  P  -  P  -  P
17 0-6   -  -  Q  -  -  -
18 0-6   R  R  -  -  -   
19 0-6   -  -  -  -  S   
20 1-2   T  T  T  T      
21 1-4   -  -  -  U      
22 2-5   V  V  V         
23 3-6   W  -  -         
24 4-6   X  Y            
25 4-6   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

OTWLU OIAAU JZRSU LROMR APRJY V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  -  -  B  -
03 1-0   -  -  C  C  -  C
04 1-0   -  -  -  -  D  D
05 1-0   -  -  E  E  -  -
06 1-0   F  -  F  -  F  -
07 2-0   G  G  G  G  G  -
08 0-3   H  H  H  H  -  H
09 0-4   -  I  -  -  -  -
10 0-4   -  J  J  -  J  -
11 0-5   K  K  -  -  -  K
12 0-5   L  L  L  L  L  L
13 0-5   M  -  -  M  M  -
14 0-5   -  N  -  N  -  -
15 0-5   O  -  O  O  O  -
16 0-5   -  -  P  P  P  P
17 0-5   -  Q  -  -  Q  -
18 0-5   R  R  -  -  -   
19 0-5   S  S  S  S  S   
20 0-5   -  -  T  T      
21 0-6   U  U  -  -      
22 1-4   -  V  -         
23 1-5   W  X  X         
24 1-5   -  Y            
25 1-5   -  -            
26 2-3   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

RTPYO XGZBR NYCQQ FOYMQ SPZWU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  -
02 1-0   -  -  B  B  B  B
03 1-0   -  -  C  -  C  C
04 2-0   D  D  D  D  -  -
05 0-3   -  E  -  -  E  E
06 0-3   F  -  F  F  -  F
07 0-3   G  G  G  G  -  -
08 0-3   H  -  H  H  H  -
09 0-3   -  I  -  I  I  -
10 0-4   -  -  -  -  J  -
11 0-4   K  -  K  K  K  K
12 0-4   -  -  L  -  -  -
13 0-4   M  M  -  -  -  M
14 0-4   N  N  -  -  N  -
15 0-4   O  O  O  O  O  O
16 0-4   -  P  P  P  P  -
17 0-4   Q  -  Q  -  -  Q
18 0-6   R  -  -  -  R   
19 0-6   S  S  S  S  -   
20 1-3   T  T  -  -      
21 1-4   U  -  -  -      
22 1-4   -  V  -         
23 1-4   W  -  X         
24 1-4   -  -            
25 2-6   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

EPZWN PQJDW OJWGS VWAIR GRAVY S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  -  B  B  B
03 1-0   -  C  C  C  C  -
04 1-0   D  -  D  -  D  D
05 1-0   E  E  -  E  -  -
06 1-0   F  F  -  -  F  -
07 1-0   -  -  G  G  -  G
08 1-0   H  H  H  H  -  -
09 1-0   I  I  -  -  I  -
10 1-0   J  J  J  J  J  -
11 2-0   K  -  -  K  -  K
12 0-3   -  L  -  -  -  -
13 0-3   -  M  -  M  M  M
14 0-3   N  N  -  N  N  N
15 0-3   -  -  O  -  -  -
16 0-4   -  P  -  -  -  P
17 0-4   Q  -  Q  -  Q  Q
18 0-4   R  -  R  R  -   
19 0-4   S  S  -  S  S   
20 0-4   -  -  -  T      
21 0-4   U  U  -  -      
22 0-4   V  V  -         
23 0-4   -  -  X         
24 0-5   X  Y            
25 0-6   Y  -            
26 2-3   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

JZMQG UVMMR PIGQJ UZHUZ OSLDS B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  -  -  -  B
03 1-0   -  C  -  -  -  -
04 2-0   -  D  -  -  D  -
05 2-0   -  -  E  -  E  E
06 2-0   -  F  -  F  -  F
07 2-0   -  -  G  -  -  G
08 2-0   -  H  H  H  -  H
09 2-0   I  -  I  I  -  -
10 2-0   J  -  J  -  -  J
11 2-0   K  -  K  K  K  -
12 2-0   -  -  -  L  L  L
13 0-3   -  M  -  M  -  -
14 0-4   -  N  N  N  -  N
15 0-4   O  -  O  -  O  -
16 0-4   -  -  -  -  P  P
17 0-5   -  Q  Q  Q  -  -
18 0-5   R  R  -  R  R   
19 0-5   S  -  S  -  S   
20 0-5   T  T  -  T      
21 0-6   U  U  -  U      
22 1-4   V  V  -         
23 1-6   W  X  X         
24 2-5   -  -            
25 2-5   -  Z            
26 2-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MHQXP WVZWA RJWNT ZTFTQ KINRO Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  -  -  -
02 0-3   -  -  -  B  -  B
03 0-3   C  C  C  C  C  C
04 0-3   -  -  -  D  D  D
05 0-5   -  -  E  -  E  E
06 0-5   -  F  -  F  -  -
07 0-5   G  -  G  G  G  G
08 0-5   -  -  -  -  -  H
09 0-5   -  I  -  -  -  -
10 0-5   -  J  -  J  -  J
11 0-6   K  -  K  K  K  -
12 0-6   L  -  L  -  -  -
13 0-6   M  -  M  M  -  M
14 0-6   -  -  N  -  -  -
15 0-6   -  O  O  O  -  O
16 1-4   P  -  -  -  P  -
17 2-4   -  Q  -  -  Q  -
18 2-5   R  -  R  -  R   
19 2-6   S  -  S  -  -   
20 2-6   -  T  -  T      
21 3-5   U  U  U  -      
22 3-6   -  -  -         
23 3-6   -  -  -         
24 3-6   X  Y            
25 3-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PIPAY TKMNP WWTPA DTALP MTJTP V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  A  A  A
02 0-3   B  -  -  -  B  B
03 0-3   C  -  C  C  -  -
04 0-3   -  D  -  -  D  -
05 0-3   -  -  -  E  -  -
06 0-3   -  F  F  -  F  -
07 0-3   -  G  -  G  -  G
08 0-4   -  H  -  H  -  H
09 0-4   I  -  I  -  I  -
10 0-4   J  J  J  -  -  -
11 0-4   K  -  -  -  -  -
12 0-5   L  -  -  -  -  L
13 0-5   -  -  M  M  -  M
14 0-5   N  -  N  N  N  -
15 0-5   O  -  -  O  -  O
16 1-2   P  P  P  P  -  -
17 1-5   -  Q  -  -  Q  -
18 1-6   -  R  -  R  -   
19 2-4   -  S  S  -  S   
20 2-5   -  -  -  T      
21 2-5   -  U  U  U      
22 3-4   V  V  V         
23 3-5   -  -  -         
24 3-5   X  -            
25 3-5   Y  Z            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UKAJE YFAAT FSOTE RSDQO FTURT O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  -
02 2-0   B  B  B  B  -  -
03 0-3   -  -  -  C  -  -
04 0-3   D  -  D  -  D  D
05 0-3   E  E  -  -  -  -
06 0-3   F  F  -  F  -  F
07 0-3   G  -  G  -  G  G
08 0-3   -  -  H  H  -  -
09 0-4   I  I  -  I  -  -
10 0-4   -  -  J  J  J  J
11 0-4   -  -  -  -  -  K
12 0-4   -  -  -  -  L  -
13 0-4   M  -  -  -  M  -
14 0-4   -  N  -  -  -  N
15 0-5   -  O  -  O  O  O
16 0-5   -  P  -  -  -  -
17 0-5   Q  Q  Q  Q  Q  -
18 0-5   -  -  R  R  R   
19 0-6   S  S  S  -  -   
20 1-4   T  -  -  -      
21 2-3   U  U  U  U      
22 2-6   V  -  V         
23 3-5   W  -  X         
24 4-5   -  Y            
25 4-5   -  Z            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

NNOSW WNISK PZFJP EWYKT KBIKM T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   B  B  B  -  B  -
03 0-3   C  C  -  C  C  C
04 0-3   D  -  -  -  -  D
05 0-3   E  -  -  -  E  E
06 0-3   -  -  F  F  -  -
07 0-3   G  G  G  -  G  -
08 0-3   -  -  -  H  -  -
09 0-3   -  I  I  I  I  -
10 0-3   -  J  -  J  -  J
11 0-4   K  -  K  -  K  -
12 0-4   L  L  -  -  -  -
13 0-4   -  M  -  -  -  -
14 0-4   N  -  -  N  N  N
15 0-5   O  O  -  -  O  O
16 0-5   -  P  P  -  P  P
17 0-5   -  Q  -  Q  -  Q
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  S  -   
20 0-5   T  -  T  T      
21 0-5   U  U  -  -      
22 0-5   -  -  V         
23 0-5   -  -  X         
24 0-5   X  -            
25 0-6   Y  Z            
26 1-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

AFUNR LZGTV VPMCQ JYHWA LNSND Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  -  -  -  B  -
03 1-0   -  -  -  C  -  -
04 1-0   D  D  -  D  -  D
05 1-0   E  E  -  -  E  -
06 1-0   F  F  -  F  -  F
07 0-3   G  -  G  G  -  -
08 0-3   H  -  -  -  -  H
09 0-3   -  -  I  -  -  I
10 0-3   -  -  -  J  -  -
11 0-3   K  -  -  -  K  -
12 0-3   L  L  -  -  L  -
13 0-3   M  -  M  M  -  M
14 0-3   N  N  N  -  N  -
15 0-3   -  -  O  O  -  -
16 0-3   -  P  P  P  P  P
17 0-3   -  -  Q  Q  Q  Q
18 0-4   -  -  -  R  R   
19 0-5   -  S  S  S  S   
20 0-5   T  T  T  -      
21 0-6   U  U  -  -      
22 0-6   V  -  V         
23 0-6   W  X  X         
24 1-3   -  -            
25 1-6   Y  Z            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QMUMM AAOMW RIIZN NTHXH ZMXAA Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  -  -  -  -  -
03 1-0   C  -  -  -  C  -
04 1-0   D  D  -  D  -  D
05 1-0   -  -  E  E  E  E
06 0-3   F  -  -  -  F  -
07 0-3   -  -  G  G  -  G
08 0-3   H  H  -  H  -  -
09 0-3   I  -  I  -  -  I
10 0-3   J  -  -  -  J  J
11 0-4   K  -  -  K  K  K
12 0-5   L  L  L  L  -  -
13 0-6   -  M  M  M  M  -
14 0-6   -  -  N  -  -  N
15 0-6   -  -  -  O  -  -
16 0-6   -  P  P  P  P  P
17 0-6   -  -  Q  -  Q  -
18 0-6   R  -  -  -  -   
19 0-6   -  S  -  -  -   
20 1-3   -  -  -  -      
21 1-4   U  U  U  U      
22 2-6   -  V  -         
23 3-6   W  -  X         
24 3-6   -  -            
25 3-6   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

UCRUC UAXCP USJVQ OZNSI VRXOP J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  B  -  -  -  -
03 1-0   -  -  -  C  C  -
04 1-0   D  -  -  -  D  -
05 2-0   -  E  E  -  E  E
06 2-0   F  -  F  -  -  F
07 2-0   -  -  -  G  G  -
08 2-0   H  -  H  -  -  -
09 2-0   -  -  -  I  I  -
10 2-0   -  J  J  J  J  J
11 2-0   K  K  K  -  -  -
12 2-0   L  L  L  L  L  -
13 2-0   M  -  -  -  M  M
14 2-0   N  N  N  -  N  N
15 2-0   -  -  O  O  -  O
16 2-0   -  -  -  -  -  P
17 0-3   Q  Q  Q  -  Q  Q
18 0-4   R  -  R  R  -   
19 0-4   -  S  -  S  -   
20 0-4   -  -  -  -      
21 0-4   U  U  U  U      
22 0-4   -  -  -         
23 0-4   W  X  X         
24 0-5   X  Y            
25 0-6   -  Z            
26 1-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

CHZDP NUGXN RZPZH THIKE WUSNT Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   B  -  -  B  -  B
03 1-0   -  -  C  C  -  C
04 2-0   -  D  D  -  -  D
05 2-0   -  -  -  -  E  -
06 2-0   F  -  -  -  F  -
07 0-3   -  G  -  G  -  G
08 0-3   H  H  H  H  -  H
09 0-3   I  -  I  -  I  I
10 0-3   J  J  -  -  J  J
11 0-3   -  -  K  -  -  -
12 0-3   -  L  -  L  L  L
13 0-3   M  -  -  M  M  -
14 0-3   N  N  N  N  N  -
15 0-3   O  O  O  -  O  O
16 0-4   P  P  P  P  -  -
17 0-5   -  Q  Q  Q  -  -
18 0-6   -  -  R  R  R   
19 0-6   S  S  -  -  -   
20 0-6   T  T  -  -      
21 0-6   -  -  -  U      
22 0-6   -  -  V         
23 0-6   -  X  -         
24 1-2   -  -            
25 1-4   Y  Z            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UXPQZ GAZWM ZGPRL JOPYF UKHOT A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  A  A
02 0-3   -  -  -  -  -  -
03 0-3   -  -  -  C  C  C
04 0-3   -  -  D  D  -  -
05 0-3   -  E  -  E  -  -
06 0-3   F  -  F  F  -  -
07 0-4   -  -  G  -  -  G
08 0-5   -  -  H  -  H  H
09 0-5   -  I  I  -  -  -
10 0-5   J  J  -  -  -  J
11 0-5   K  K  K  -  K  K
12 0-6   -  L  -  L  -  -
13 0-6   M  -  M  -  M  M
14 0-6   -  N  N  N  N  N
15 0-6   -  -  -  -  O  O
16 0-6   P  -  -  P  -  -
17 0-6   Q  -  -  Q  Q  Q
18 0-6   -  -  -  R  -   
19 0-6   S  -  -  -  -   
20 0-6   T  -  T  T      
21 0-6   U  U  -  -      
22 0-6   -  V  V         
23 0-6   -  -  X         
24 1-2   -  Y            
25 2-5   Y  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UHSUO HVPMV NBAUN NUTMO ATOUU A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  B  -  -  B  B
03 1-0   C  -  C  C  C  -
04 1-0   D  -  -  D  D  -
05 0-3   E  -  E  E  -  E
06 0-3   -  F  -  -  F  F
07 0-4   -  G  -  G  G  -
08 0-4   -  H  -  -  -  -
09 0-4   -  -  I  I  I  I
10 0-4   J  J  J  -  -  J
11 0-4   -  K  -  K  K  -
12 0-5   -  L  L  L  -  L
13 0-5   M  -  -  M  M  M
14 0-5   -  -  N  -  -  N
15 0-5   O  O  O  -  -  -
16 0-5   -  P  -  -  P  -
17 0-5   -  -  Q  Q  -  -
18 1-4   -  -  -  -  -   
19 1-4   S  -  -  -  -   
20 1-4   T  T  -  -      
21 1-4   -  U  U  -      
22 1-5   -  V  V         
23 2-4   -  X  X         
24 3-4   X  Y            
25 3-4   Y  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

PNAOX AAGVA MYPXU RJLNP YPAUF R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  B  B  -  B  -
03 2-0   C  -  C  -  C  -
04 2-0   D  -  -  D  D  D
05 2-0   -  -  E  E  -  E
06 2-0   -  F  -  -  -  F
07 2-0   G  G  -  G  G  -
08 2-0   -  -  -  -  H  H
09 2-0   I  I  -  -  -  -
10 0-3   J  -  -  -  J  -
11 0-3   -  -  -  K  -  K
12 0-3   -  -  L  L  -  -
13 0-3   -  M  M  -  -  M
14 0-3   N  N  -  N  -  -
15 0-3   -  -  -  O  O  -
16 0-3   P  P  -  P  P  -
17 0-3   Q  -  -  Q  Q  Q
18 0-3   -  -  R  -  R   
19 0-3   -  S  -  S  -   
20 0-4   T  -  T  T      
21 0-4   -  -  U  -      
22 0-5   -  V  V         
23 0-6   W  -  -         
24 1-4   -  Y            
25 1-5   Y  -            
26 2-3   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

EOEFZ BRYKN QYWGQ FIOZP GKQOP F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  -  B  B  B  B
03 1-0   -  -  C  C  -  -
04 1-0   D  D  -  D  -  D
05 1-0   E  E  E  E  E  -
06 0-3   F  -  -  -  -  -
07 0-3   -  -  G  -  G  G
08 0-3   H  H  -  -  -  -
09 0-3   I  I  -  I  I  -
10 0-3   -  -  J  J  J  J
11 0-3   -  -  -  K  K  -
12 0-4   -  L  -  -  -  L
13 0-4   -  M  M  -  -  M
14 0-4   -  N  N  -  N  -
15 0-4   O  -  -  -  O  O
16 0-4   -  P  -  P  P  -
17 0-4   -  -  -  Q  Q  Q
18 1-3   R  -  R  -  -   
19 1-4   -  S  -  S  -   
20 1-4   T  -  -  -      
21 1-4   U  U  -  -      
22 1-4   -  V  -         
23 2-6   -  -  X         
24 3-6   -  Y            
25 4-5   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

HDOMA KODDV AAZOS KUMUA ZEJLO U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
