EFFECTIVE PERIOD:
04-MAY-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  A  -
02 0-3   B  B  B  B  B  B
03 0-3   C  -  C  C  -  C
04 0-3   -  D  D  -  D  -
05 0-3   E  -  -  -  -  -
06 0-3   F  -  -  -  F  -
07 0-4   -  G  -  G  -  -
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  I  -  I
10 0-5   -  -  -  J  -  -
11 0-5   K  -  -  -  K  K
12 0-5   -  L  -  -  -  L
13 0-6   -  M  M  M  M  M
14 0-6   -  N  -  N  N  N
15 0-6   O  O  -  -  -  O
16 0-6   P  -  P  -  -  -
17 0-6   Q  -  -  Q  Q  -
18 1-2   -  -  R  -  -   
19 1-3   S  S  -  S  -   
20 1-3   -  T  -  -      
21 1-4   -  U  U  U      
22 3-5   -  -  -         
23 3-5   W  -  X         
24 3-5   -  Y            
25 3-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NUVKX IFNRA HVNRA NHXZK NURAI X
-------------------------------
