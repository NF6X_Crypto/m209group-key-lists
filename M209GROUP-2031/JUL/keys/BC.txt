EFFECTIVE PERIOD:
24-JUL-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  -  B  -  B
03 1-0   -  C  C  C  C  -
04 1-0   D  D  -  D  D  D
05 1-0   -  -  -  -  E  E
06 1-0   -  -  -  -  -  -
07 1-0   G  G  G  G  G  -
08 2-0   -  H  H  -  H  -
09 2-0   I  -  I  -  -  I
10 2-0   -  -  -  J  J  J
11 0-3   K  K  K  K  -  K
12 0-3   -  L  L  L  -  L
13 0-3   M  M  -  M  -  M
14 0-3   N  -  -  -  -  -
15 0-3   -  -  -  -  O  -
16 1-2   -  P  P  P  -  P
17 1-3   -  Q  Q  Q  -  -
18 1-3   -  R  R  R  R   
19 1-3   S  -  S  -  S   
20 1-3   T  -  -  -      
21 2-3   U  -  -  -      
22 2-5   -  V  -         
23 3-5   W  -  X         
24 3-5   X  Y            
25 3-6   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

CITAV WDVRR TFTFT FOATO NZKQO P
-------------------------------
