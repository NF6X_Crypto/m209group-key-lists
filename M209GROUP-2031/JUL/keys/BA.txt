EFFECTIVE PERIOD:
22-JUL-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  -  -  B  B  -
03 1-0   -  -  -  C  C  C
04 1-0   D  D  D  D  -  D
05 1-0   -  E  -  E  -  E
06 1-0   -  F  -  -  -  -
07 1-0   G  G  G  G  -  -
08 1-0   -  -  H  H  -  H
09 2-0   I  I  I  -  I  I
10 0-4   -  -  -  -  J  J
11 0-4   K  -  K  -  K  -
12 0-4   -  L  -  L  -  -
13 0-5   M  -  M  -  -  M
14 0-5   -  -  -  -  N  N
15 0-6   -  -  -  O  O  -
16 0-6   -  P  P  P  P  -
17 0-6   Q  Q  -  Q  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   -  -  -  S  S   
20 1-6   -  -  T  -      
21 1-6   U  U  -  -      
22 1-6   V  -  V         
23 1-6   W  X  X         
24 2-3   X  Y            
25 2-5   -  Z            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZOASH PYPRP ANEFU QSREU OPFYH F
-------------------------------
