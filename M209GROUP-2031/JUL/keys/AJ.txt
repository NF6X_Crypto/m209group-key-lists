EFFECTIVE PERIOD:
05-JUL-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  -
02 2-0   B  -  -  B  B  -
03 2-0   C  C  -  C  C  -
04 2-0   -  D  D  -  -  -
05 2-0   E  E  E  -  -  E
06 0-3   F  F  -  -  -  -
07 0-3   -  G  G  G  -  G
08 0-3   -  -  H  -  -  H
09 0-4   I  -  I  -  I  -
10 0-4   J  -  J  -  J  J
11 0-4   -  -  -  -  K  K
12 0-4   L  -  L  L  L  -
13 0-5   M  M  -  -  M  -
14 0-5   N  N  N  N  N  N
15 0-5   -  O  O  O  -  O
16 1-6   -  -  -  -  -  -
17 2-4   Q  Q  Q  Q  -  -
18 2-5   -  R  -  R  -   
19 2-5   -  -  S  -  S   
20 2-5   T  T  T  -      
21 2-5   U  -  -  U      
22 3-4   V  V  -         
23 3-5   -  -  X         
24 3-6   -  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WVVTK UGQQA QKATT PWWPQ SQGUQ O
-------------------------------
