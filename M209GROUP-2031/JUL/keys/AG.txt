EFFECTIVE PERIOD:
02-JUL-2031 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  -  -  B  -  B
03 1-0   -  C  C  C  -  -
04 1-0   D  D  D  -  -  -
05 2-0   E  E  E  -  -  E
06 2-0   F  -  -  -  -  -
07 2-0   -  G  G  -  -  -
08 2-0   H  -  H  -  H  H
09 2-0   I  -  I  I  -  -
10 0-3   J  -  J  -  -  -
11 0-3   -  -  K  -  K  K
12 0-3   L  -  -  -  L  L
13 0-6   M  M  -  -  -  M
14 0-6   -  -  N  -  N  N
15 0-6   O  -  -  O  O  -
16 1-3   P  P  -  P  P  P
17 1-4   -  Q  -  -  -  -
18 1-4   -  -  -  R  -   
19 1-4   -  -  -  S  S   
20 1-6   -  T  T  T      
21 1-6   -  U  U  U      
22 1-6   -  -  -         
23 1-6   W  -  X         
24 2-3   X  Y            
25 2-6   Y  Z            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VNSMA SPVRW PXFLZ TQWVV UAQXM P
-------------------------------
