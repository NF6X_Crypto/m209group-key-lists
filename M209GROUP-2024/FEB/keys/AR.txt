EFFECTIVE PERIOD:
16-FEB-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   B  B  -  B  -  B
03 1-0   C  C  -  -  C  C
04 1-0   D  -  D  -  D  -
05 1-0   E  E  -  -  -  E
06 1-0   -  -  F  -  F  F
07 2-0   G  G  G  -  G  G
08 2-0   -  -  H  -  -  H
09 0-3   I  -  I  I  -  -
10 0-5   J  -  -  J  -  J
11 0-5   K  K  K  K  K  -
12 0-6   L  -  L  L  -  -
13 0-6   -  M  -  M  M  M
14 0-6   N  N  N  -  -  -
15 0-6   O  O  O  -  O  -
16 0-6   -  P  -  P  P  -
17 0-6   Q  Q  Q  Q  Q  -
18 0-6   -  -  -  R  -   
19 0-6   -  S  S  S  S   
20 0-6   T  T  -  T      
21 0-6   -  -  -  -      
22 1-5   -  -  -         
23 1-6   -  -  -         
24 1-6   X  Y            
25 2-3   -  -            
26 2-5   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

FQPGP IAESP GYWAV OOOGT OOLIQ X
-------------------------------
