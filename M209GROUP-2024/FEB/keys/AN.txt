EFFECTIVE PERIOD:
12-FEB-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  -  B  -  -
03 1-0   C  -  C  -  C  C
04 2-0   D  -  -  D  D  D
05 2-0   E  E  -  -  -  E
06 2-0   F  F  F  -  F  -
07 2-0   -  G  -  G  G  G
08 2-0   H  -  -  H  H  -
09 0-3   I  I  -  I  -  -
10 0-4   J  -  J  -  J  -
11 0-4   -  -  -  K  -  -
12 0-4   -  -  L  -  L  L
13 0-4   M  M  -  M  -  M
14 0-4   -  -  N  N  N  N
15 0-4   -  -  O  -  -  -
16 0-4   P  P  P  -  -  -
17 0-4   -  -  -  Q  -  -
18 0-5   R  R  -  -  R   
19 0-5   -  -  S  S  S   
20 0-5   T  T  T  T      
21 0-5   -  U  -  -      
22 1-3   -  V  -         
23 1-5   -  X  -         
24 2-4   -  -            
25 2-4   -  Z            
26 2-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

VSLIO KXVHA AGSKJ AUUFA FSJOC S
-------------------------------
