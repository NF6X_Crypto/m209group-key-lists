EFFECTIVE PERIOD:
31-JAN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  B  B  -  -  -
03 1-0   C  -  C  -  C  C
04 1-0   D  D  -  -  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  -  F  F  -  -
07 1-0   G  G  -  -  -  G
08 1-0   -  -  -  H  -  -
09 2-0   I  -  I  I  -  I
10 2-0   -  J  -  J  -  -
11 2-0   K  -  K  -  K  K
12 0-3   -  L  -  -  L  L
13 0-3   -  -  -  M  M  M
14 0-3   -  -  -  -  -  N
15 0-3   -  O  O  O  O  -
16 0-3   P  -  P  -  P  -
17 0-3   -  -  Q  Q  -  Q
18 0-4   -  -  R  R  -   
19 0-5   -  -  S  -  S   
20 0-6   T  T  T  T      
21 0-6   U  U  -  -      
22 1-3   -  -  -         
23 1-3   W  -  -         
24 1-3   -  Y            
25 2-3   Y  -            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UOOOU WQNBT PXPOQ QLTPP RLGFJ O
-------------------------------
