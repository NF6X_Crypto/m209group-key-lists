EFFECTIVE PERIOD:
01-JAN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  B  -  B  -  -
03 1-0   C  -  C  C  C  C
04 1-0   -  -  D  D  -  -
05 1-0   E  -  E  -  E  -
06 2-0   F  F  F  F  -  F
07 2-0   -  G  G  -  -  -
08 0-3   -  -  -  -  -  -
09 0-4   -  I  I  I  I  I
10 0-4   -  J  -  -  -  J
11 0-4   -  -  -  -  -  -
12 0-4   L  -  L  L  L  L
13 0-4   M  M  -  -  M  M
14 0-4   N  N  -  -  -  -
15 0-4   -  O  O  -  -  -
16 0-4   P  P  -  P  P  -
17 0-4   Q  -  Q  Q  -  -
18 0-4   R  R  -  -  R   
19 0-5   S  -  -  -  -   
20 0-6   -  -  T  T      
21 0-6   -  -  U  -      
22 1-2   V  -  -         
23 1-4   -  -  -         
24 1-4   X  -            
25 1-4   Y  Z            
26 2-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

MQMXN ZLRJM YQVKO NQQEH RBIMZ R
-------------------------------
