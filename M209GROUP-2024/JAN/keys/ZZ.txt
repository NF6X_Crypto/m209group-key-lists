EFFECTIVE PERIOD:
29-JAN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  B  -  B  B  -
03 1-0   C  -  C  -  -  C
04 2-0   -  D  D  D  -  -
05 2-0   -  -  E  -  -  -
06 2-0   F  -  -  -  -  -
07 2-0   -  -  -  G  G  -
08 0-5   -  -  H  H  -  -
09 0-5   -  -  I  -  I  -
10 0-5   -  J  J  -  J  J
11 0-5   K  K  -  K  -  K
12 0-5   -  L  L  L  L  L
13 0-6   M  M  -  -  -  M
14 0-6   N  -  -  N  N  N
15 0-6   O  -  O  -  O  O
16 0-6   -  -  -  P  -  -
17 0-6   Q  Q  Q  -  -  Q
18 1-2   R  -  -  R  -   
19 1-3   -  -  S  S  S   
20 1-6   T  -  -  T      
21 2-5   -  -  U  -      
22 3-4   V  V  V         
23 3-6   -  -  -         
24 5-6   -  Y            
25 5-6   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QQZSP CQRCA MFMHR AWSMS RAPUH F
-------------------------------
