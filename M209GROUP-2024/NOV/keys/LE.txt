EFFECTIVE PERIOD:
15-NOV-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  -  B  -  B  -
03 2-0   C  C  -  C  C  C
04 2-0   -  D  -  D  D  -
05 2-0   -  -  -  -  -  -
06 2-0   F  F  -  -  -  F
07 0-4   G  G  G  G  G  G
08 0-4   -  -  -  H  H  -
09 0-5   I  I  I  -  I  -
10 0-5   -  -  J  -  J  J
11 0-5   K  K  -  K  -  K
12 0-5   L  -  L  -  -  L
13 0-5   M  -  M  -  M  -
14 0-5   -  -  -  N  N  N
15 0-5   -  O  O  -  O  -
16 1-2   P  -  -  -  -  -
17 1-2   Q  Q  -  -  -  Q
18 1-2   R  -  -  R  -   
19 1-2   S  S  S  S  S   
20 1-3   -  -  -  -      
21 1-3   -  U  U  -      
22 1-4   V  V  V         
23 1-4   W  X  -         
24 2-5   -  -            
25 3-4   -  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PWWTI ITYMO QYGPQ NAXDT JYWKD I
-------------------------------
