EFFECTIVE PERIOD:
23-NOV-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  B  -  -  B
03 1-0   -  -  C  C  -  -
04 1-0   D  D  -  D  D  -
05 2-0   -  -  E  -  -  E
06 2-0   F  F  F  -  -  -
07 2-0   G  -  -  -  G  G
08 0-3   H  H  -  -  H  -
09 0-3   I  -  -  I  -  I
10 0-4   -  J  J  J  J  J
11 0-4   -  -  K  K  -  K
12 0-4   L  L  L  L  L  L
13 0-4   M  M  -  -  -  -
14 0-4   -  -  N  -  N  N
15 0-4   O  -  O  -  O  -
16 1-2   P  -  -  -  P  -
17 1-2   -  Q  Q  Q  -  Q
18 1-2   -  -  -  R  -   
19 1-2   S  S  -  S  S   
20 1-4   -  T  T  -      
21 2-3   U  -  -  -      
22 2-3   -  -  V         
23 2-3   W  X  -         
24 2-5   -  Y            
25 2-6   -  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

RHMRU XOLPL PPWAU IQSXW WHRUH Q
-------------------------------
