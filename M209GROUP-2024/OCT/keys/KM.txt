EFFECTIVE PERIOD:
28-OCT-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  A
02 2-0   B  -  B  -  B  -
03 2-0   C  C  C  C  -  -
04 2-0   D  D  D  -  -  -
05 2-0   E  E  -  -  -  -
06 2-0   -  -  -  F  -  F
07 2-0   G  -  G  -  G  -
08 0-5   H  H  -  H  H  H
09 0-5   -  I  I  I  I  I
10 0-5   -  -  J  -  J  J
11 0-5   K  K  -  K  K  K
12 0-5   L  L  L  -  L  -
13 0-6   M  M  -  M  -  M
14 0-6   -  -  N  -  -  -
15 0-6   -  -  -  O  O  O
16 0-6   -  P  P  -  -  -
17 0-6   -  -  -  Q  Q  Q
18 0-6   -  -  -  -  R   
19 0-6   S  S  -  -  -   
20 1-6   T  -  -  T      
21 2-4   -  U  U  -      
22 2-5   V  V  V         
23 3-4   W  X  -         
24 5-6   X  -            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

TINKV JZVKM IBMKA ZTTTA ZVSVR L
-------------------------------
