EFFECTIVE PERIOD:
02-DEC-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   B  -  B  -  -  -
03 1-0   -  -  -  -  C  -
04 1-0   -  -  -  D  D  -
05 1-0   E  E  E  -  -  E
06 2-0   -  F  F  -  F  -
07 2-0   G  -  G  G  -  G
08 2-0   H  -  -  H  -  H
09 2-0   I  I  I  I  I  I
10 2-0   -  J  J  -  J  -
11 2-0   K  K  -  K  -  -
12 0-3   L  L  L  -  -  L
13 0-3   -  -  M  -  -  M
14 0-3   N  -  -  N  N  N
15 0-3   -  -  O  O  -  -
16 0-3   P  -  P  -  -  P
17 0-3   Q  Q  Q  -  -  Q
18 0-4   -  R  R  R  R   
19 0-4   S  -  -  -  S   
20 1-2   -  T  -  T      
21 1-2   -  U  U  U      
22 1-2   V  V  -         
23 1-2   W  X  -         
24 1-3   X  Y            
25 2-6   -  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JUVXH ORAUO AYNBI AVNRA KYAEQ I
-------------------------------
