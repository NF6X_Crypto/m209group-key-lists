EFFECTIVE PERIOD:
31-DEC-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   -  B  B  B  -  -
03 1-0   C  C  -  C  -  C
04 1-0   -  -  D  -  D  D
05 0-3   E  E  E  E  E  -
06 0-3   -  -  F  F  F  F
07 0-3   -  G  -  -  -  G
08 0-3   -  H  -  H  -  H
09 0-3   I  I  -  -  -  -
10 0-4   J  -  -  J  -  -
11 0-4   -  K  -  K  -  K
12 0-4   L  L  L  L  L  -
13 0-6   M  -  -  -  M  M
14 0-6   -  -  N  -  N  N
15 0-6   -  O  -  -  -  -
16 0-6   P  P  P  P  P  -
17 0-6   -  -  Q  -  Q  -
18 1-2   -  -  R  -  -   
19 1-2   S  -  S  -  -   
20 1-6   T  T  -  T      
21 1-6   -  U  U  U      
22 1-6   V  -  -         
23 1-6   -  X  -         
24 2-4   X  -            
25 2-5   Y  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

XDQZR KLTPP WLNAT SENHV VKLRP K
-------------------------------
