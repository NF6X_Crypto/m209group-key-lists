EFFECTIVE PERIOD:
20-DEC-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 0-3   B  B  B  -  B  -
03 0-3   -  C  -  -  C  C
04 0-3   -  D  -  D  D  -
05 0-3   E  E  -  -  E  -
06 0-3   -  F  F  F  F  -
07 0-3   G  -  G  G  -  G
08 0-4   H  -  H  H  -  H
09 0-4   -  I  I  I  -  I
10 0-4   J  -  J  -  J  -
11 0-4   -  K  -  K  -  -
12 0-4   -  -  L  L  L  L
13 0-4   M  M  -  -  -  M
14 0-4   -  -  -  N  -  -
15 0-5   O  -  -  O  O  -
16 0-5   -  -  P  -  P  -
17 0-5   -  Q  Q  -  Q  -
18 0-5   R  -  -  R  -   
19 0-5   -  S  -  -  -   
20 0-5   -  T  -  -      
21 0-5   U  U  U  -      
22 0-5   -  V  V         
23 0-5   -  X  X         
24 2-3   X  -            
25 2-6   Y  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

JJRRL UBIJA KTULK RQZJZ JIRUK I
-------------------------------
