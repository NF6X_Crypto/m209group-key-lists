EFFECTIVE PERIOD:
11-DEC-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ME
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  B  B  -  B
03 1-0   C  -  C  C  -  C
04 1-0   D  -  D  D  -  D
05 2-0   E  E  -  -  E  E
06 2-0   -  -  -  -  F  -
07 2-0   -  -  G  -  G  -
08 2-0   H  H  -  -  H  -
09 2-0   -  I  I  -  I  I
10 2-0   -  J  J  J  -  -
11 2-0   K  K  K  -  K  K
12 0-3   -  -  -  L  L  -
13 0-3   -  M  -  M  -  -
14 0-3   N  N  N  N  -  N
15 0-3   -  O  O  -  -  O
16 0-5   -  -  P  P  -  -
17 0-6   Q  Q  -  Q  -  Q
18 0-6   -  -  -  R  -   
19 0-6   -  -  -  -  S   
20 1-2   -  -  -  -      
21 1-2   -  -  U  U      
22 1-2   V  -  -         
23 1-2   -  -  X         
24 1-3   -  Y            
25 2-4   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WORFF ANHFZ SZIPK WHEXT VVAVV V
-------------------------------
