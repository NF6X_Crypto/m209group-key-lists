EFFECTIVE PERIOD:
26-JUL-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 2-0   B  B  -  B  -  B
03 2-0   -  -  -  -  C  C
04 2-0   -  -  -  -  D  D
05 2-0   -  E  -  -  -  -
06 2-0   F  F  F  -  F  -
07 0-3   G  -  -  G  -  G
08 0-3   H  -  -  H  H  -
09 0-3   -  I  -  I  -  -
10 0-3   -  -  J  -  -  -
11 0-3   -  K  K  -  -  -
12 0-4   L  L  -  L  -  -
13 0-4   -  -  M  -  M  M
14 0-4   -  N  -  N  -  N
15 0-4   O  O  -  O  -  O
16 0-4   P  -  P  -  P  -
17 0-4   -  Q  Q  -  Q  -
18 0-4   -  -  R  -  -   
19 0-5   S  S  -  S  -   
20 1-4   -  T  T  -      
21 1-5   -  -  U  U      
22 2-3   V  -  -         
23 2-3   W  -  X         
24 2-3   -  -            
25 2-3   Y  Z            
26 2-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

MKXMR LUMLM UBMCZ ZTKCP UKZKL S
-------------------------------
