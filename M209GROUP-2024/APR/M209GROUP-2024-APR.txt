SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF APR 2024

    01 APR 2024  00:00-23:59 GMT:  USE KEY CK
    02 APR 2024  00:00-23:59 GMT:  USE KEY CL
    03 APR 2024  00:00-23:59 GMT:  USE KEY CM
    04 APR 2024  00:00-23:59 GMT:  USE KEY CN
    05 APR 2024  00:00-23:59 GMT:  USE KEY CO
    06 APR 2024  00:00-23:59 GMT:  USE KEY CP
    07 APR 2024  00:00-23:59 GMT:  USE KEY CQ
    08 APR 2024  00:00-23:59 GMT:  USE KEY CR
    09 APR 2024  00:00-23:59 GMT:  USE KEY CS
    10 APR 2024  00:00-23:59 GMT:  USE KEY CT
    11 APR 2024  00:00-23:59 GMT:  USE KEY CU
    12 APR 2024  00:00-23:59 GMT:  USE KEY CV
    13 APR 2024  00:00-23:59 GMT:  USE KEY CW
    14 APR 2024  00:00-23:59 GMT:  USE KEY CX
    15 APR 2024  00:00-23:59 GMT:  USE KEY CY
    16 APR 2024  00:00-23:59 GMT:  USE KEY CZ
    17 APR 2024  00:00-23:59 GMT:  USE KEY DA
    18 APR 2024  00:00-23:59 GMT:  USE KEY DB
    19 APR 2024  00:00-23:59 GMT:  USE KEY DC
    20 APR 2024  00:00-23:59 GMT:  USE KEY DD
    21 APR 2024  00:00-23:59 GMT:  USE KEY DE
    22 APR 2024  00:00-23:59 GMT:  USE KEY DF
    23 APR 2024  00:00-23:59 GMT:  USE KEY DG
    24 APR 2024  00:00-23:59 GMT:  USE KEY DH
    25 APR 2024  00:00-23:59 GMT:  USE KEY DI
    26 APR 2024  00:00-23:59 GMT:  USE KEY DJ
    27 APR 2024  00:00-23:59 GMT:  USE KEY DK
    28 APR 2024  00:00-23:59 GMT:  USE KEY DL
    29 APR 2024  00:00-23:59 GMT:  USE KEY DM
    30 APR 2024  00:00-23:59 GMT:  USE KEY DN

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  -  -  -  -  B
03 1-0   C  C  -  -  -  C
04 1-0   D  -  D  D  D  D
05 1-0   -  E  -  E  E  -
06 1-0   -  F  -  F  F  -
07 1-0   -  G  -  G  -  -
08 1-0   H  H  H  -  H  H
09 2-0   I  I  -  I  -  -
10 0-3   -  -  -  -  J  J
11 0-4   K  K  -  K  -  K
12 0-4   -  L  L  -  -  -
13 0-4   -  -  M  M  -  -
14 0-4   -  -  N  -  N  -
15 0-4   O  -  -  -  -  -
16 0-5   P  -  P  -  P  -
17 0-6   -  Q  Q  -  Q  Q
18 0-6   R  R  R  -  -   
19 0-6   S  -  -  -  -   
20 0-6   T  -  -  T      
21 0-6   -  -  U  U      
22 0-6   V  V  V         
23 0-6   W  -  -         
24 0-6   -  Y            
25 0-6   -  Z            
26 3-4   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

YZMLL BQZQJ UBPRW IJSIO ZRDMK T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  -  -  -  B  B
03 1-0   -  -  -  -  -  C
04 1-0   D  D  D  -  D  -
05 1-0   E  E  E  -  E  E
06 2-0   F  F  F  F  F  -
07 2-0   G  -  G  G  -  G
08 2-0   -  -  H  H  -  -
09 2-0   -  I  -  -  I  -
10 2-0   -  -  -  J  J  -
11 2-0   K  K  K  -  -  -
12 0-5   -  -  -  L  -  L
13 0-5   M  M  -  M  M  M
14 0-5   -  -  -  -  -  N
15 0-5   -  O  O  O  -  -
16 0-5   P  P  -  P  -  -
17 0-5   Q  Q  Q  -  -  -
18 1-2   -  R  -  -  -   
19 1-2   -  -  -  S  S   
20 1-2   -  -  -  T      
21 1-2   U  U  U  U      
22 1-3   V  V  V         
23 1-4   -  X  X         
24 1-6   -  -            
25 2-5   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SAOPZ LSUPK BAHKT TAUDN LSZSC N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 2-0   B  B  B  B  B  B
03 2-0   C  -  -  -  C  -
04 2-0   -  D  D  D  -  -
05 2-0   E  -  -  E  E  E
06 2-0   -  F  F  -  F  -
07 2-0   -  -  -  -  -  -
08 0-3   -  -  H  H  H  H
09 0-3   -  -  I  -  -  I
10 0-3   -  J  J  J  J  -
11 0-3   K  -  -  -  K  -
12 0-3   L  L  L  -  -  -
13 0-3   M  -  -  M  -  -
14 0-3   -  N  N  N  N  N
15 0-3   O  O  -  O  -  O
16 0-3   -  -  -  P  -  P
17 0-3   Q  Q  -  -  Q  Q
18 0-5   R  R  -  R  R   
19 0-5   -  S  -  -  S   
20 0-6   -  T  -  T      
21 0-6   U  -  U  U      
22 0-6   -  V  -         
23 0-6   -  -  X         
24 2-3   X  -            
25 2-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SBMNM QIVHP PXMPM UIOOK MUOSL Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   B  B  B  -  B  B
03 1-0   -  C  C  C  C  C
04 1-0   D  D  -  -  -  D
05 1-0   -  -  E  E  -  E
06 2-0   F  -  -  F  F  -
07 2-0   G  -  -  -  -  -
08 2-0   H  H  -  H  H  H
09 0-3   I  I  I  -  I  I
10 0-3   J  -  -  -  -  J
11 0-3   -  -  -  K  K  -
12 0-3   -  -  L  -  -  L
13 0-3   M  -  -  -  -  -
14 0-3   -  N  -  -  N  -
15 0-3   O  O  O  O  -  -
16 0-3   P  P  P  P  -  P
17 0-3   Q  Q  Q  -  Q  -
18 0-3   -  R  -  -  R   
19 0-5   S  -  S  S  S   
20 0-6   -  -  T  T      
21 0-6   -  -  -  -      
22 1-2   V  V  -         
23 1-3   W  -  X         
24 1-3   -  -            
25 2-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LYTWZ RUODR FUVHZ FNHAM HODIT A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 1-0   -  -  -  B  -  -
03 1-0   -  -  -  -  -  -
04 1-0   -  -  -  -  -  -
05 0-3   -  E  -  E  E  E
06 0-4   -  -  F  F  -  F
07 0-4   G  G  G  G  -  G
08 0-4   -  -  -  H  H  H
09 0-4   I  -  I  I  I  -
10 0-4   -  J  -  -  -  -
11 0-5   K  -  K  -  K  K
12 0-5   -  L  L  -  -  L
13 0-6   -  M  M  M  M  M
14 0-6   N  -  -  N  N  N
15 0-6   O  O  -  -  O  -
16 0-6   -  P  -  -  P  -
17 0-6   Q  -  Q  Q  -  -
18 0-6   R  -  R  -  R   
19 0-6   -  S  -  S  -   
20 0-6   T  T  T  -      
21 0-6   U  U  U  U      
22 0-6   V  V  -         
23 0-6   -  X  X         
24 1-4   X  -            
25 1-5   Y  -            
26 2-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SZUQP GPNOM LRPRR LLXQR QPLDR O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  -  A
02 0-3   B  -  B  -  -  -
03 0-3   C  C  -  -  C  -
04 0-3   -  -  D  -  -  -
05 0-3   -  -  -  E  E  E
06 0-3   F  F  -  F  -  -
07 0-3   G  -  -  -  -  G
08 0-5   -  -  -  H  H  H
09 0-5   -  -  I  -  I  I
10 0-5   -  J  -  -  -  -
11 0-5   -  -  -  -  -  -
12 0-6   L  L  -  L  -  -
13 0-6   M  M  -  -  M  -
14 0-6   N  -  N  N  N  N
15 0-6   -  -  O  O  -  O
16 1-2   P  P  P  P  P  P
17 1-4   -  Q  Q  Q  Q  Q
18 1-5   -  -  R  -  -   
19 2-5   -  S  -  S  -   
20 2-5   T  T  T  T      
21 2-6   -  -  U  -      
22 3-5   V  V  -         
23 3-5   -  -  X         
24 3-5   -  Y            
25 3-5   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OWWSA SUVZP VRNWH ROCLZ TRLAA M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   -  -  -  -  B  B
03 0-3   -  C  C  C  -  C
04 0-3   D  D  -  D  D  D
05 0-3   E  E  E  -  -  -
06 0-3   -  -  F  F  -  -
07 0-3   -  G  G  -  -  -
08 0-3   -  -  -  H  -  -
09 0-3   -  I  -  -  -  I
10 0-3   -  J  J  -  J  -
11 0-6   K  K  K  K  K  K
12 0-6   -  -  -  -  -  -
13 0-6   M  -  -  -  M  M
14 0-6   -  N  -  -  N  -
15 0-6   -  -  -  O  -  -
16 1-2   P  P  P  P  P  -
17 1-4   -  Q  Q  Q  Q  Q
18 1-5   R  -  R  -  -   
19 1-5   S  -  -  S  S   
20 1-5   -  -  -  -      
21 1-6   U  -  -  -      
22 1-6   V  -  -         
23 1-6   -  -  X         
24 1-6   X  Y            
25 2-5   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

QFAUO MQEQA LGKBV AKPEV OMZSG T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  -
02 2-0   B  B  B  B  -  B
03 2-0   -  -  -  -  -  -
04 2-0   D  D  D  -  D  D
05 0-3   -  -  E  -  E  E
06 0-3   -  -  F  F  -  -
07 0-3   G  G  G  -  -  -
08 0-3   -  H  -  H  H  H
09 0-3   -  I  -  -  I  I
10 0-3   -  -  -  -  J  J
11 0-3   K  -  K  K  K  -
12 0-6   L  L  L  L  L  -
13 0-6   M  M  -  -  M  -
14 0-6   N  -  N  N  -  -
15 0-6   O  O  O  O  -  -
16 1-2   -  -  -  -  -  -
17 2-3   Q  Q  Q  Q  -  Q
18 2-3   R  -  -  R  R   
19 2-3   -  -  S  -  -   
20 2-3   T  -  -  T      
21 2-5   -  -  -  U      
22 2-5   -  V  -         
23 2-6   -  -  -         
24 2-6   X  Y            
25 3-6   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WQNLB MKNJM IAOLA VVOOO PMNAM T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  A
02 2-0   -  -  -  B  -  B
03 2-0   C  -  C  C  C  -
04 2-0   -  D  -  D  D  -
05 2-0   E  E  -  -  E  E
06 2-0   F  F  -  -  F  -
07 0-3   -  -  G  G  -  G
08 0-3   -  H  -  -  -  H
09 0-3   -  -  I  -  -  -
10 0-4   J  -  -  J  J  -
11 0-4   K  -  K  -  -  -
12 0-4   -  L  L  L  -  -
13 0-6   M  -  -  M  -  M
14 0-6   N  N  -  N  N  N
15 0-6   O  -  -  -  O  O
16 0-6   -  P  -  P  P  P
17 0-6   -  Q  Q  -  Q  -
18 1-2   -  -  R  R  R   
19 1-4   S  S  S  -  S   
20 2-3   -  T  T  -      
21 2-3   -  U  -  -      
22 2-3   V  -  V         
23 2-3   -  -  X         
24 2-4   -  Y            
25 2-5   Y  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

FRTUW ARHNR RNOJT RRAVG TUXCX J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  B  -  B  -  B
03 1-0   C  -  C  C  -  -
04 1-0   -  -  D  -  D  D
05 1-0   -  -  E  E  -  E
06 1-0   -  F  -  F  F  -
07 1-0   G  G  -  -  G  -
08 2-0   -  -  -  H  -  H
09 2-0   I  -  -  I  I  -
10 2-0   J  J  J  J  -  J
11 2-0   -  K  K  -  K  K
12 2-0   L  -  -  L  -  L
13 2-0   -  -  M  -  M  M
14 2-0   -  N  N  -  -  -
15 2-0   O  O  O  -  -  O
16 2-0   P  P  P  -  P  -
17 2-0   -  Q  -  Q  Q  -
18 2-0   R  -  -  R  -   
19 2-0   S  S  -  -  -   
20 0-3   T  -  -  T      
21 0-3   -  -  U  -      
22 0-4   -  -  V         
23 0-5   W  -  -         
24 1-3   X  Y            
25 3-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

XSXHY EZNLL NQWOH ZQWEL AMKFS Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 2-0   -  B  -  -  B  -
03 0-3   C  -  C  C  -  C
04 0-3   -  -  D  -  -  D
05 0-3   E  E  E  E  E  E
06 0-4   F  F  F  F  F  F
07 0-4   -  -  -  -  G  -
08 0-4   -  -  -  -  -  H
09 0-4   I  I  -  I  -  -
10 0-4   -  -  -  -  J  -
11 0-4   -  -  K  K  -  -
12 0-4   -  -  -  L  L  -
13 0-6   M  M  M  -  M  M
14 0-6   N  -  N  N  -  -
15 0-6   O  -  -  -  -  O
16 0-6   -  -  -  -  P  -
17 0-6   -  Q  -  Q  Q  -
18 0-6   -  R  R  R  R   
19 0-6   S  -  -  S  S   
20 1-2   -  -  T  -      
21 1-3   U  U  -  U      
22 3-4   -  -  V         
23 4-6   -  X  -         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FMNDX PSLRV SXSSX HDMZX QLPIO P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   B  -  -  -  B  -
03 1-0   -  C  C  -  C  C
04 1-0   -  D  D  D  D  D
05 2-0   -  E  -  E  E  -
06 2-0   -  F  -  -  -  -
07 2-0   G  -  -  G  -  G
08 0-3   H  H  -  -  H  H
09 0-3   -  -  I  -  -  I
10 0-3   -  J  J  J  -  J
11 0-3   K  -  -  -  K  -
12 0-6   -  -  -  L  L  -
13 0-6   -  M  M  M  M  M
14 0-6   N  N  -  N  -  -
15 0-6   -  -  -  O  -  O
16 0-6   P  P  -  -  P  -
17 0-6   Q  -  Q  -  Q  Q
18 1-4   -  -  -  -  -   
19 1-6   -  -  -  S  -   
20 2-3   -  -  T  -      
21 2-3   -  -  U  U      
22 2-3   -  V  V         
23 2-3   W  X  X         
24 2-4   X  -            
25 2-4   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

IPLII ATTUT NGRWQ SQFUQ KPMSP Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  B  B  -  -  B
03 1-0   -  -  -  C  -  C
04 1-0   -  -  D  D  D  -
05 1-0   E  -  E  -  E  E
06 1-0   -  F  F  F  -  F
07 1-0   -  G  G  G  G  -
08 2-0   -  -  -  -  H  -
09 0-3   I  -  -  I  I  -
10 0-3   -  J  J  -  -  J
11 0-3   -  -  K  -  K  -
12 0-3   -  -  L  -  L  -
13 0-3   M  -  -  -  -  -
14 0-3   N  N  -  N  N  N
15 0-3   O  -  O  O  O  O
16 0-3   -  -  -  -  -  -
17 0-3   -  Q  Q  -  Q  -
18 0-4   -  -  -  -  R   
19 0-4   -  S  -  -  -   
20 0-5   T  T  T  T      
21 0-6   -  -  -  U      
22 1-3   V  -  V         
23 1-3   W  X  -         
24 1-3   -  Y            
25 1-4   Y  -            
26 2-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

CPGPN DWOSQ KZHLR YQFCW OPFNY O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   -  B  B  -  -  B
03 2-0   C  C  -  C  -  -
04 0-4   D  D  D  -  D  -
05 0-4   E  -  E  -  E  E
06 0-4   F  F  -  F  F  F
07 0-4   -  -  -  G  G  G
08 0-4   -  -  H  -  H  H
09 0-4   -  I  I  I  -  -
10 0-5   -  -  -  -  J  -
11 0-5   K  -  -  K  K  -
12 0-5   -  L  -  -  -  L
13 0-5   -  -  -  M  M  M
14 0-6   N  -  N  N  -  -
15 0-6   O  -  O  O  -  O
16 0-6   P  -  -  P  -  -
17 0-6   Q  Q  -  Q  Q  Q
18 1-3   -  -  R  -  -   
19 2-3   S  -  -  -  -   
20 2-4   -  -  -  T      
21 2-5   -  -  U  -      
22 3-5   V  V  -         
23 4-6   -  -  X         
24 5-6   -  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

KXUIS JJOIC HATUA SGTQU JQMST Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 2-0   -  -  -  B  B  -
03 2-0   C  -  -  -  -  -
04 2-0   D  D  D  D  D  -
05 2-0   -  -  -  E  -  -
06 0-3   F  F  F  -  -  F
07 0-3   G  G  G  -  G  -
08 0-3   H  -  -  H  -  H
09 0-6   I  I  I  -  I  -
10 0-6   -  J  -  -  J  J
11 0-6   K  -  -  -  K  K
12 0-6   -  -  L  L  -  -
13 0-6   -  -  M  M  -  M
14 0-6   N  N  N  N  -  N
15 0-6   -  -  O  -  -  -
16 1-3   P  P  -  -  P  -
17 1-5   Q  -  -  -  -  -
18 2-5   R  -  R  R  -   
19 2-6   -  S  S  -  S   
20 3-4   -  -  T  T      
21 3-5   -  -  U  U      
22 3-5   -  V  -         
23 3-5   -  -  -         
24 3-6   X  -            
25 3-6   -  -            
26 3-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UTNLT LLAGE CLMMW LAWNT SMLNA Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   -  B  -  B  -  B
03 2-0   C  -  -  -  -  C
04 2-0   -  D  -  -  -  -
05 2-0   E  -  -  -  E  -
06 2-0   -  F  F  -  F  -
07 0-3   G  -  -  G  -  -
08 0-3   -  H  H  H  H  -
09 0-3   I  I  I  -  -  I
10 0-3   -  -  J  J  J  -
11 0-3   -  -  -  -  -  K
12 0-4   L  L  L  -  L  -
13 0-4   -  M  -  M  M  M
14 0-4   -  -  N  -  N  -
15 0-4   -  -  -  O  -  O
16 0-4   P  -  -  -  P  P
17 0-4   -  Q  -  Q  -  Q
18 0-4   R  R  -  R  R   
19 0-4   S  -  -  -  S   
20 1-2   -  T  -  -      
21 1-6   U  U  U  -      
22 2-3   V  V  V         
23 3-4   W  -  X         
24 3-4   X  Y            
25 3-4   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

WAVIN VJSNA SIGEF WSOXS ATIYA N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 2-0   B  B  B  -  B  B
03 2-0   C  C  -  -  C  -
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  E  E  -
06 0-3   -  F  -  -  F  -
07 0-3   G  G  G  G  -  G
08 0-3   -  H  -  H  H  H
09 0-3   I  -  -  -  -  I
10 0-3   -  J  -  J  -  -
11 0-4   -  -  K  -  K  K
12 0-4   -  L  -  -  L  L
13 0-4   -  M  -  M  -  -
14 0-4   N  N  N  -  -  -
15 0-4   -  O  -  -  -  O
16 0-4   P  P  P  -  P  P
17 0-4   Q  -  Q  -  Q  -
18 1-3   R  -  -  -  -   
19 1-4   S  -  -  S  S   
20 1-6   -  -  -  T      
21 2-3   -  -  U  U      
22 2-3   V  V  V         
23 2-3   -  -  -         
24 2-3   X  -            
25 2-4   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

ANOOC CLZWA TTUAM NYNTE QKKQO R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  B  B  -  -  -
03 1-0   -  -  -  C  C  C
04 1-0   -  D  -  -  -  -
05 1-0   -  -  E  E  E  -
06 1-0   -  F  F  -  -  F
07 2-0   G  -  -  G  G  G
08 2-0   -  H  -  H  -  -
09 2-0   I  -  I  -  I  -
10 2-0   -  J  J  J  -  J
11 2-0   -  -  K  -  K  K
12 0-3   L  -  -  L  L  -
13 0-3   M  -  -  -  M  -
14 0-3   N  N  N  -  -  N
15 0-3   -  -  O  -  -  -
16 0-3   P  P  P  -  -  P
17 0-3   -  -  -  Q  -  -
18 1-2   R  R  R  -  -   
19 1-2   -  -  -  S  S   
20 1-2   -  -  T  -      
21 1-2   U  -  -  -      
22 1-3   V  V  V         
23 2-5   W  X  X         
24 2-5   -  Y            
25 2-6   -  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VURLI LSUUU NANBU INLUU UZLRZ I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  A
02 2-0   B  -  B  B  B  -
03 2-0   -  C  C  C  C  C
04 2-0   D  D  -  -  -  -
05 0-3   -  E  -  E  -  E
06 0-3   -  -  F  -  F  -
07 0-3   G  G  -  -  G  -
08 0-3   -  H  -  H  -  H
09 0-3   I  -  I  -  I  I
10 0-3   -  J  J  -  -  J
11 0-3   K  -  -  K  K  K
12 0-3   -  L  -  L  L  L
13 0-3   -  -  M  M  M  -
14 0-5   N  N  N  -  -  -
15 0-5   O  -  -  O  O  O
16 0-5   P  -  -  -  -  -
17 0-5   Q  Q  Q  Q  Q  -
18 0-5   -  -  -  -  -   
19 0-5   -  S  S  S  S   
20 0-5   -  T  T  T      
21 0-6   U  U  -  -      
22 2-4   V  -  V         
23 2-5   W  -  -         
24 3-5   X  -            
25 3-5   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XNFTG YSOMO YPMPR SNHZW PDXNR C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 2-0   B  -  B  B  B  B
03 2-0   C  -  C  C  C  C
04 2-0   D  D  D  -  D  D
05 2-0   -  -  E  -  -  E
06 0-3   F  -  -  F  -  -
07 0-3   G  -  -  -  -  -
08 0-3   -  H  H  H  H  -
09 0-3   -  -  I  -  I  -
10 0-3   J  -  J  J  J  -
11 0-3   -  K  K  -  -  K
12 0-6   -  L  L  -  L  L
13 0-6   -  -  -  -  -  -
14 0-6   -  N  -  N  -  -
15 0-6   O  -  -  -  O  -
16 0-6   -  P  -  P  P  P
17 0-6   -  -  Q  -  -  Q
18 1-2   -  -  -  R  -   
19 1-4   S  S  S  S  -   
20 1-6   T  -  -  -      
21 2-3   U  U  -  U      
22 3-6   V  V  -         
23 3-6   W  -  -         
24 3-6   -  -            
25 3-6   -  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MTZQJ WUINN ZWOOU AFWZL OPTMI O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 2-0   -  -  B  B  -  -
03 2-0   -  C  -  -  -  C
04 2-0   -  D  D  -  -  -
05 2-0   E  E  -  -  E  -
06 2-0   F  F  -  F  F  F
07 2-0   -  G  G  G  G  G
08 2-0   -  -  H  -  -  H
09 0-3   -  I  -  I  I  I
10 0-3   -  -  -  -  -  J
11 0-4   K  K  K  -  K  -
12 0-4   L  L  L  L  L  -
13 0-4   -  -  M  M  M  M
14 0-4   N  -  N  -  -  N
15 0-4   O  -  -  O  O  -
16 0-4   P  P  -  P  -  -
17 0-4   Q  Q  -  -  -  Q
18 0-4   R  -  R  R  -   
19 0-4   S  -  -  S  S   
20 0-5   -  T  -  -      
21 0-6   -  -  U  -      
22 1-6   V  -  -         
23 2-3   -  X  -         
24 2-4   X  -            
25 2-4   Y  -            
26 2-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

SNXQD TPENP PPDQX XRWAO SYQRN B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  -  -  B  -
03 1-0   -  -  C  C  C  C
04 1-0   D  D  D  -  -  D
05 2-0   E  E  -  -  -  -
06 2-0   -  F  -  -  F  -
07 2-0   G  -  -  G  -  -
08 2-0   H  -  -  -  -  H
09 2-0   I  -  -  -  I  -
10 2-0   -  -  J  J  -  J
11 2-0   -  K  K  K  K  K
12 2-0   -  L  L  -  L  L
13 0-3   -  M  M  M  M  M
14 0-3   N  N  -  -  N  -
15 0-3   -  O  O  O  -  -
16 0-3   -  -  P  -  P  P
17 0-3   Q  Q  -  Q  -  Q
18 0-6   -  -  R  R  -   
19 0-6   S  S  -  -  S   
20 1-3   -  -  T  -      
21 1-6   -  -  -  U      
22 2-3   V  V  V         
23 2-3   -  X  -         
24 2-3   X  -            
25 2-3   -  -            
26 2-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QRWFT ERQYD ISSKV SQIJS KTWQA S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  A
02 2-0   B  -  -  -  -  -
03 2-0   -  -  -  C  -  C
04 2-0   -  -  -  D  D  D
05 0-3   E  -  -  -  E  -
06 0-5   F  -  F  -  -  F
07 0-5   -  G  -  -  G  -
08 0-5   H  H  -  -  H  -
09 0-5   I  I  I  I  I  I
10 0-5   J  -  J  J  -  J
11 0-5   K  K  K  -  -  K
12 0-6   -  -  -  -  -  L
13 0-6   -  -  M  M  M  -
14 0-6   N  N  -  -  -  -
15 0-6   O  O  O  -  O  -
16 1-3   P  P  -  -  -  -
17 1-6   Q  -  Q  -  Q  Q
18 2-5   R  -  -  -  R   
19 2-6   -  S  -  S  -   
20 2-6   -  -  T  T      
21 2-6   U  U  U  U      
22 2-6   -  -  V         
23 3-5   -  X  -         
24 3-6   X  -            
25 3-6   -  Z            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SABMI JAPWM SIVZH WVRUP NLMMA R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 2-0   -  -  B  -  -  -
03 0-3   -  C  -  C  C  -
04 0-3   D  D  -  D  -  D
05 0-3   -  E  E  -  E  E
06 0-3   F  -  F  -  -  -
07 0-3   G  -  G  G  -  -
08 0-3   -  -  -  H  H  H
09 0-3   I  I  I  -  -  I
10 0-4   J  -  -  J  J  J
11 0-4   -  K  -  -  K  K
12 0-4   -  -  L  -  L  -
13 0-5   M  -  M  -  M  -
14 0-5   N  -  -  -  -  N
15 0-5   -  -  O  O  O  O
16 0-5   P  -  P  P  P  -
17 0-5   -  Q  Q  Q  -  Q
18 0-5   R  -  -  -  -   
19 0-5   S  S  S  S  S   
20 0-5   T  -  -  T      
21 0-5   U  U  -  -      
22 0-5   -  -  -         
23 0-5   -  -  -         
24 0-5   X  Y            
25 0-6   -  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PSNZT HGRAB VOPBL ZNPWO PKSSN K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 2-0   -  -  B  B  B  B
03 2-0   C  -  C  C  C  C
04 2-0   -  -  -  -  D  -
05 0-3   -  E  -  -  E  E
06 0-3   F  F  F  F  F  -
07 0-3   G  G  -  G  G  -
08 0-3   -  H  H  -  -  -
09 0-3   I  -  -  I  -  -
10 0-3   -  J  -  J  -  J
11 0-3   -  K  K  -  -  -
12 0-3   -  L  -  -  L  L
13 0-4   -  M  M  -  -  M
14 0-4   N  N  N  N  N  N
15 0-4   -  -  -  -  O  O
16 0-4   P  -  P  -  -  P
17 0-6   -  -  -  -  -  -
18 0-6   R  -  -  -  R   
19 0-6   S  S  S  -  -   
20 1-6   -  -  T  T      
21 2-3   -  U  U  U      
22 2-3   V  -  -         
23 2-3   W  -  X         
24 2-3   X  Y            
25 2-4   Y  Z            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OJSGM RSUKW NWMCG VNXQG VNZQJ R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  -
02 2-0   B  -  -  B  B  -
03 2-0   C  C  C  C  -  C
04 2-0   D  D  D  D  -  -
05 2-0   -  E  -  -  -  E
06 0-3   -  -  -  -  F  -
07 0-3   -  -  G  -  -  -
08 0-3   -  -  H  H  H  H
09 0-3   I  -  I  -  -  I
10 0-3   J  -  -  -  J  J
11 0-3   -  K  K  -  -  -
12 0-4   L  -  L  L  -  -
13 0-4   M  M  -  -  M  -
14 0-4   -  -  N  N  N  N
15 0-4   O  -  O  -  O  -
16 0-4   P  -  P  -  -  P
17 0-4   -  Q  Q  Q  Q  -
18 0-4   -  R  R  -  -   
19 0-4   -  -  -  -  S   
20 0-4   T  T  -  -      
21 0-4   -  -  -  U      
22 0-4   V  V  V         
23 0-5   -  X  -         
24 1-6   X  Y            
25 2-3   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

ITOPT IAHZU SNTPH IUIHA VOOOI T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  -  B  -  -  -
03 1-0   -  -  C  C  C  C
04 2-0   -  D  D  D  -  D
05 0-3   E  E  -  -  -  -
06 0-3   F  -  -  F  F  F
07 0-3   G  G  -  G  G  -
08 0-5   -  -  -  H  H  -
09 0-5   I  I  -  -  -  -
10 0-5   -  J  -  -  J  J
11 0-5   -  K  K  -  K  -
12 0-6   -  -  L  L  L  L
13 0-6   -  M  M  -  -  M
14 0-6   N  -  -  -  -  N
15 0-6   O  -  -  O  -  -
16 0-6   P  -  P  -  P  P
17 0-6   Q  -  Q  Q  Q  Q
18 1-2   -  R  -  -  -   
19 1-2   -  -  S  S  -   
20 1-3   T  T  -  -      
21 1-3   U  -  -  U      
22 1-3   V  -  V         
23 1-3   -  X  -         
24 2-4   X  -            
25 2-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OQPTP ARZUQ TMUNJ QRQMX TMOQO N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   -  -  -  -  -  B
03 1-0   C  C  C  -  C  -
04 1-0   D  -  -  -  -  D
05 1-0   -  E  -  -  -  E
06 2-0   F  F  F  -  F  -
07 2-0   G  G  -  -  G  G
08 2-0   H  -  -  H  H  -
09 2-0   I  -  I  I  I  -
10 2-0   -  -  J  J  J  -
11 0-3   -  K  K  K  -  K
12 0-3   -  L  L  L  L  L
13 0-3   M  -  -  -  -  M
14 0-3   N  -  -  -  N  N
15 0-3   O  O  O  -  -  O
16 1-2   -  P  P  P  P  -
17 1-2   -  Q  -  -  Q  -
18 1-2   R  R  -  -  R   
19 1-2   S  -  -  S  -   
20 1-5   T  -  T  T      
21 1-5   -  -  U  -      
22 1-6   V  -  V         
23 1-6   -  -  X         
24 2-3   X  Y            
25 3-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QTAUP DVHVQ GOGVO SVVVV KVJOV A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  -
02 2-0   -  -  -  -  -  B
03 2-0   -  C  C  C  -  -
04 2-0   D  D  -  D  -  -
05 2-0   E  E  -  E  E  -
06 2-0   F  F  F  -  F  F
07 0-3   -  -  -  G  -  G
08 0-3   -  -  H  -  H  -
09 0-3   I  -  I  -  -  -
10 0-3   J  -  J  J  -  -
11 0-4   -  K  -  -  -  -
12 0-4   L  -  L  L  L  L
13 0-4   -  M  -  -  -  M
14 0-5   -  -  -  N  N  N
15 0-5   O  O  -  -  O  -
16 0-5   -  -  -  -  -  P
17 0-5   Q  -  -  Q  -  Q
18 0-5   -  -  R  -  R   
19 0-6   -  S  S  -  -   
20 1-6   -  T  T  -      
21 2-4   U  U  -  U      
22 2-4   -  -  V         
23 2-4   -  -  X         
24 2-4   X  -            
25 3-5   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QKJJT RXZUO QNUWK ZKFPN KVQMZ W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   B  -  -  -  B  B
03 1-0   -  -  C  C  C  -
04 1-0   -  D  -  D  -  -
05 1-0   -  -  E  E  -  -
06 1-0   F  -  -  -  F  F
07 2-0   G  G  -  -  G  -
08 2-0   -  -  -  -  -  H
09 2-0   -  -  I  -  I  -
10 2-0   J  J  J  J  -  J
11 0-5   -  K  K  -  K  K
12 0-5   L  -  L  -  -  -
13 0-5   M  -  -  -  -  M
14 0-5   -  -  -  -  N  -
15 0-5   -  -  -  O  -  -
16 1-4   -  P  -  P  P  -
17 1-5   -  Q  Q  Q  Q  Q
18 2-3   R  -  R  R  -   
19 2-4   -  -  S  S  -   
20 2-4   -  T  -  -      
21 2-4   U  -  U  U      
22 2-5   -  -  V         
23 2-5   W  X  -         
24 2-5   X  Y            
25 2-5   -  Z            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

AMAFP AFKVU ZVNOT ZRUPC VTHVP F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
