EFFECTIVE PERIOD:
29-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  -
02 2-0   -  -  -  -  -  B
03 2-0   -  C  C  C  -  -
04 2-0   D  D  -  D  -  -
05 2-0   E  E  -  E  E  -
06 2-0   F  F  F  -  F  F
07 0-3   -  -  -  G  -  G
08 0-3   -  -  H  -  H  -
09 0-3   I  -  I  -  -  -
10 0-3   J  -  J  J  -  -
11 0-4   -  K  -  -  -  -
12 0-4   L  -  L  L  L  L
13 0-4   -  M  -  -  -  M
14 0-5   -  -  -  N  N  N
15 0-5   O  O  -  -  O  -
16 0-5   -  -  -  -  -  P
17 0-5   Q  -  -  Q  -  Q
18 0-5   -  -  R  -  R   
19 0-6   -  S  S  -  -   
20 1-6   -  T  T  -      
21 2-4   U  U  -  U      
22 2-4   -  -  V         
23 2-4   -  -  X         
24 2-4   X  -            
25 3-5   Y  Z            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QKJJT RXZUO QNUWK ZKFPN KVQMZ W
-------------------------------
