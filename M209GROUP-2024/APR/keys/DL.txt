EFFECTIVE PERIOD:
28-APR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   -  -  -  -  -  B
03 1-0   C  C  C  -  C  -
04 1-0   D  -  -  -  -  D
05 1-0   -  E  -  -  -  E
06 2-0   F  F  F  -  F  -
07 2-0   G  G  -  -  G  G
08 2-0   H  -  -  H  H  -
09 2-0   I  -  I  I  I  -
10 2-0   -  -  J  J  J  -
11 0-3   -  K  K  K  -  K
12 0-3   -  L  L  L  L  L
13 0-3   M  -  -  -  -  M
14 0-3   N  -  -  -  N  N
15 0-3   O  O  O  -  -  O
16 1-2   -  P  P  P  P  -
17 1-2   -  Q  -  -  Q  -
18 1-2   R  R  -  -  R   
19 1-2   S  -  -  S  -   
20 1-5   T  -  T  T      
21 1-5   -  -  U  -      
22 1-6   V  -  V         
23 1-6   -  -  X         
24 2-3   X  Y            
25 3-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QTAUP DVHVQ GOGVO SVVVV KVJOV A
-------------------------------
