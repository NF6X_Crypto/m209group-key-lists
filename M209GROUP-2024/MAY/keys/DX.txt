EFFECTIVE PERIOD:
10-MAY-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   B  -  -  -  B  B
03 1-0   -  C  -  C  -  C
04 0-3   -  D  D  D  D  -
05 0-3   -  E  -  E  E  E
06 0-3   -  -  -  -  -  F
07 0-3   G  -  -  -  G  -
08 0-3   -  -  H  -  H  -
09 0-4   -  I  -  -  I  -
10 0-4   J  J  J  -  J  -
11 0-4   K  K  K  K  -  K
12 0-4   -  L  -  L  -  L
13 0-6   M  -  M  -  -  M
14 0-6   N  -  -  -  -  -
15 0-6   -  O  -  O  O  -
16 0-6   -  P  -  P  -  -
17 0-6   -  -  Q  -  Q  Q
18 1-3   R  -  -  -  R   
19 1-4   S  -  S  -  -   
20 1-5   -  T  -  T      
21 2-5   -  U  U  U      
22 3-5   V  V  -         
23 3-6   -  -  X         
24 3-6   X  -            
25 3-6   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KKTVZ MRWVS KVVRC CCWJW VGMTV M
-------------------------------
