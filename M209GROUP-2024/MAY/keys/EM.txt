EFFECTIVE PERIOD:
25-MAY-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 1-0   B  B  B  B  B  -
03 1-0   -  C  -  -  C  C
04 1-0   -  -  D  -  D  -
05 1-0   E  E  -  -  E  E
06 1-0   -  -  -  -  -  F
07 2-0   G  -  -  G  -  -
08 2-0   H  H  H  -  H  H
09 2-0   I  I  I  I  -  -
10 2-0   -  J  J  J  J  -
11 2-0   K  -  -  K  -  -
12 2-0   L  -  -  -  -  L
13 0-4   M  M  -  M  M  M
14 0-4   N  -  N  N  -  -
15 0-4   O  O  O  O  -  O
16 0-4   -  -  -  P  P  P
17 0-6   -  -  Q  -  Q  Q
18 1-4   R  R  R  -  -   
19 1-6   -  -  S  -  -   
20 2-3   -  T  -  T      
21 2-4   -  -  -  -      
22 2-4   V  V  -         
23 2-4   -  -  -         
24 2-4   X  -            
25 2-5   -  Z            
26 2-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ROSRF PKRHE SWWMD ZZSAA NSSAS U
-------------------------------
