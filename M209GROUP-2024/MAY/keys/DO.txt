EFFECTIVE PERIOD:
01-MAY-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  A
02 2-0   B  B  -  -  B  -
03 2-0   C  C  C  -  C  -
04 0-3   D  D  D  D  -  -
05 0-3   E  -  -  E  -  -
06 0-3   F  -  -  -  F  F
07 0-4   G  -  G  -  G  G
08 0-4   -  H  H  H  H  H
09 0-4   -  I  -  -  I  I
10 0-4   -  J  J  -  -  -
11 0-4   -  -  -  K  -  -
12 0-4   -  -  -  -  L  -
13 0-5   M  M  -  M  M  M
14 0-5   -  -  -  N  N  N
15 0-5   -  O  -  -  -  O
16 0-5   P  -  P  P  P  P
17 0-5   Q  Q  Q  -  -  -
18 1-6   R  -  R  R  -   
19 2-4   S  S  S  -  S   
20 2-6   -  T  -  T      
21 3-4   -  U  U  U      
22 3-5   -  -  -         
23 3-5   W  -  X         
24 3-5   -  Y            
25 3-5   Y  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

XTUSU VEXHU UEVAQ MWIEA QULQL M
-------------------------------
