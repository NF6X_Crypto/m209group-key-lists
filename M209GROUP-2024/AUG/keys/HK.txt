EFFECTIVE PERIOD:
09-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  B  -  -  B  B
03 1-0   -  -  -  -  -  -
04 1-0   -  -  D  D  -  D
05 1-0   -  E  E  E  E  -
06 1-0   F  F  F  -  -  -
07 2-0   G  G  G  -  -  -
08 2-0   H  -  H  -  -  H
09 0-3   I  -  -  I  I  I
10 0-3   -  -  J  -  -  -
11 0-3   -  K  K  -  K  -
12 0-3   L  -  -  L  L  -
13 0-5   M  -  M  M  -  -
14 0-5   N  N  N  -  -  N
15 0-5   -  O  O  O  O  O
16 1-2   P  -  -  P  -  -
17 1-3   -  -  Q  -  Q  Q
18 2-5   R  R  R  R  R   
19 2-5   -  -  -  S  S   
20 2-6   -  T  -  T      
21 3-5   -  -  U  -      
22 3-5   V  V  -         
23 3-5   W  -  -         
24 3-5   X  -            
25 4-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AIIUM PUAUT DMWJM RZVSW MIWTA P
-------------------------------
