EFFECTIVE PERIOD:
25-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  -  -  -  -
03 2-0   C  C  -  C  -  -
04 2-0   -  -  D  -  D  -
05 2-0   E  E  -  -  -  E
06 2-0   F  -  F  F  F  F
07 0-5   -  -  -  G  G  -
08 0-5   -  H  H  H  H  -
09 0-5   I  -  I  I  I  I
10 0-5   J  J  J  -  -  J
11 0-5   -  -  -  K  K  K
12 0-5   L  L  -  L  -  -
13 0-6   M  -  -  -  M  -
14 0-6   N  N  N  N  N  -
15 0-6   -  O  -  O  -  -
16 1-2   -  -  -  -  P  P
17 1-2   Q  -  Q  -  -  Q
18 1-2   -  R  R  -  -   
19 1-3   S  -  S  S  S   
20 1-5   -  -  -  T      
21 2-3   U  U  -  -      
22 2-6   V  -  V         
23 2-6   -  X  X         
24 2-6   X  -            
25 2-6   -  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ROIQK QUPXC QPTXI AVKVI XHUWT L
-------------------------------
