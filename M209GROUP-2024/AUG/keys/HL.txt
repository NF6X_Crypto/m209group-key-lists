EFFECTIVE PERIOD:
10-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  B  -  B  -
03 2-0   C  -  -  -  C  -
04 2-0   D  D  D  D  D  -
05 2-0   -  E  -  -  -  -
06 2-0   F  F  F  F  F  F
07 0-3   G  G  G  G  G  G
08 0-3   H  H  -  -  H  H
09 0-3   I  -  I  I  -  -
10 0-3   -  -  -  J  J  J
11 0-3   K  -  -  -  -  -
12 0-4   -  L  L  L  -  -
13 0-4   M  -  M  -  M  M
14 0-4   -  N  -  -  -  N
15 0-4   O  -  O  O  O  -
16 0-4   -  -  P  P  -  P
17 0-4   -  Q  Q  -  -  Q
18 1-3   R  R  -  R  -   
19 1-4   -  S  -  S  S   
20 1-6   T  -  T  -      
21 2-3   -  U  U  -      
22 2-3   V  -  -         
23 2-3   W  X  -         
24 2-3   X  Y            
25 2-4   -  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ZLAYN JAPNQ ALUPV XUUAF KPRMY P
-------------------------------
