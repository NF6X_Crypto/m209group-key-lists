EFFECTIVE PERIOD:
23-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  A
02 1-0   B  B  B  -  -  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  D  D  D  -
05 1-0   -  E  E  -  -  E
06 1-0   F  F  -  F  F  F
07 1-0   G  G  G  G  -  -
08 2-0   H  -  -  H  H  -
09 0-3   I  I  I  -  -  -
10 0-3   -  -  -  J  -  J
11 0-3   K  K  K  K  -  K
12 0-3   -  L  -  -  L  L
13 0-3   M  M  -  -  -  M
14 0-3   -  -  N  -  -  -
15 0-3   -  -  O  O  O  O
16 0-3   -  P  -  -  P  P
17 0-3   -  Q  Q  Q  Q  -
18 0-3   -  R  -  -  R   
19 0-3   -  -  S  S  -   
20 0-3   T  T  -  T      
21 0-4   U  -  U  -      
22 0-4   V  -  -         
23 0-4   W  -  X         
24 0-4   X  -            
25 0-5   -  Z            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OOGON WMYHL BOZVZ HZJYO LJDAO F
-------------------------------
