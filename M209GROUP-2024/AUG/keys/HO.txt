EFFECTIVE PERIOD:
13-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  B  -  -  -  B
03 1-0   -  -  C  C  -  C
04 1-0   -  D  -  D  D  -
05 1-0   E  E  -  E  -  E
06 2-0   F  F  -  -  -  -
07 2-0   G  G  G  -  G  G
08 2-0   -  -  H  -  -  -
09 2-0   I  I  I  I  -  I
10 2-0   -  -  -  -  J  J
11 2-0   K  -  K  -  -  K
12 0-6   L  L  L  -  L  -
13 0-6   M  M  M  -  -  M
14 0-6   N  -  N  -  -  -
15 0-6   O  -  -  O  O  -
16 0-6   -  -  -  P  P  -
17 0-6   -  -  Q  Q  Q  -
18 1-2   R  -  R  -  -   
19 1-2   S  S  S  -  S   
20 1-2   -  -  -  -      
21 1-2   -  U  -  U      
22 1-6   -  -  V         
23 2-4   W  -  X         
24 2-5   X  Y            
25 2-6   -  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

KZSNO CMLTV MAOJN ZAABA BLUUM S
-------------------------------
