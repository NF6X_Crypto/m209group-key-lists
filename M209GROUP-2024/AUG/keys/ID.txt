EFFECTIVE PERIOD:
28-AUG-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ID
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  B  B  -  -  -
03 1-0   -  C  -  -  -  C
04 1-0   D  D  D  -  D  -
05 1-0   -  -  -  E  E  -
06 1-0   -  -  F  -  F  F
07 1-0   G  -  G  -  -  -
08 2-0   H  H  -  H  -  -
09 2-0   -  I  I  -  I  -
10 2-0   J  J  -  J  -  -
11 2-0   -  K  -  -  -  K
12 2-0   -  L  -  -  L  L
13 0-4   -  -  -  M  -  -
14 0-4   -  N  N  N  N  N
15 0-4   O  -  -  -  -  O
16 0-4   P  P  P  -  -  -
17 0-4   Q  Q  Q  Q  -  Q
18 0-6   R  R  -  -  R   
19 0-6   S  -  -  -  -   
20 1-2   -  T  T  T      
21 1-2   U  -  -  U      
22 1-2   -  -  V         
23 1-2   W  X  -         
24 1-5   X  -            
25 2-4   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VASVJ REVYV ZLVRR AIPAK VTPDN T
-------------------------------
