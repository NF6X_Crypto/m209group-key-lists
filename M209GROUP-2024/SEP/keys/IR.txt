EFFECTIVE PERIOD:
11-SEP-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   B  B  -  B  -  B
03 1-0   C  -  C  -  C  -
04 1-0   D  D  -  D  D  -
05 1-0   -  -  E  E  E  -
06 2-0   F  -  -  F  -  -
07 0-3   -  G  -  -  G  G
08 0-3   H  H  -  H  H  H
09 0-3   -  -  -  -  I  I
10 0-3   -  J  J  -  J  J
11 0-3   K  -  K  K  -  K
12 0-3   L  L  L  L  L  -
13 0-3   -  M  -  -  -  -
14 0-4   -  -  N  N  -  -
15 0-5   -  O  O  -  O  O
16 0-5   -  P  P  P  -  -
17 0-5   Q  -  -  -  -  Q
18 0-6   R  R  R  R  -   
19 0-6   S  S  S  -  -   
20 0-6   -  T  T  -      
21 0-6   -  U  -  U      
22 1-3   V  -  V         
23 1-3   W  X  -         
24 1-3   -  -            
25 1-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

USPPQ MVNSZ GNWPA JSKJP MUPQR Q
-------------------------------
