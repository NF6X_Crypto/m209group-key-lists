EFFECTIVE PERIOD:
03-SEP-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  B  B  -  B  -
03 1-0   -  -  -  -  C  -
04 1-0   -  D  -  -  -  D
05 1-0   -  E  E  -  -  -
06 1-0   -  -  -  F  F  F
07 1-0   G  G  -  -  -  G
08 1-0   -  H  -  -  -  H
09 2-0   I  I  -  -  -  I
10 0-3   -  -  J  -  -  -
11 0-3   K  -  K  K  K  -
12 0-4   -  L  L  L  -  -
13 0-5   -  M  -  -  -  -
14 0-5   N  -  N  -  N  N
15 0-5   -  O  -  O  O  -
16 0-5   P  -  P  P  P  -
17 0-6   Q  -  Q  Q  Q  -
18 0-6   R  -  R  R  -   
19 0-6   -  S  S  S  -   
20 0-6   -  T  T  T      
21 0-6   -  -  -  -      
22 0-6   -  V  -         
23 0-6   -  X  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 2-3   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

MHQRJ JELJO YESNO KZJGN NPGJW F
-------------------------------
