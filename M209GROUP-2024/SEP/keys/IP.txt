EFFECTIVE PERIOD:
09-SEP-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  B  -  -  -
03 2-0   C  -  C  C  -  C
04 2-0   -  D  D  -  D  -
05 2-0   -  -  -  -  E  -
06 0-4   F  -  -  F  -  -
07 0-4   G  -  G  G  -  G
08 0-4   H  -  H  H  -  -
09 0-4   I  I  -  I  I  I
10 0-4   J  -  -  -  J  -
11 0-4   -  -  -  -  K  K
12 0-4   -  -  -  -  L  -
13 0-4   M  M  M  -  M  M
14 0-5   N  N  N  -  -  N
15 0-6   O  O  O  -  O  -
16 0-6   -  -  -  P  -  P
17 0-6   Q  Q  Q  Q  -  -
18 0-6   -  R  -  -  -   
19 0-6   -  S  -  S  -   
20 1-2   T  T  T  -      
21 1-5   -  U  U  U      
22 2-6   V  -  -         
23 3-4   -  X  X         
24 4-6   X  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QFNYU NUKVW DTFOR SRXER OZMIZ U
-------------------------------
