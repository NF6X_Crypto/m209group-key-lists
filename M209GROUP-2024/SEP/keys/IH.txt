EFFECTIVE PERIOD:
01-SEP-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  -  B  -  -  -
03 2-0   -  C  -  C  C  C
04 2-0   -  D  D  -  D  -
05 2-0   -  -  -  E  E  E
06 2-0   -  -  F  F  F  F
07 2-0   G  G  -  -  G  G
08 2-0   H  -  H  H  -  H
09 0-3   -  -  I  -  -  -
10 0-3   -  -  -  -  J  -
11 0-3   K  K  -  K  K  K
12 0-3   L  L  L  L  -  -
13 0-3   -  -  M  -  M  -
14 0-5   N  -  -  -  -  N
15 0-5   O  -  O  O  -  O
16 0-5   -  P  P  -  -  -
17 0-5   -  Q  -  Q  Q  Q
18 1-3   -  R  R  R  R   
19 1-5   S  S  -  S  -   
20 1-6   -  T  -  -      
21 2-3   U  -  -  -      
22 2-3   -  V  -         
23 2-3   W  X  -         
24 2-3   X  -            
25 2-5   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

BXSQT UIPPG FUKYS LAAUA IWWCU U
-------------------------------
