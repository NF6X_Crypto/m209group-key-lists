EFFECTIVE PERIOD:
23-MAR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  A
02 1-0   -  -  B  B  B  -
03 1-0   C  -  C  -  -  C
04 1-0   D  D  D  D  -  D
05 1-0   -  E  E  E  E  E
06 2-0   -  -  F  -  -  -
07 0-3   -  -  -  -  G  -
08 0-3   -  H  H  -  H  H
09 0-3   -  -  -  -  -  I
10 0-3   J  J  -  J  J  -
11 0-3   -  -  K  -  -  -
12 0-3   -  -  -  -  L  -
13 0-4   M  M  M  -  -  M
14 0-4   -  N  N  -  -  -
15 0-4   -  O  O  O  -  -
16 0-4   P  P  -  P  -  P
17 0-4   Q  -  -  -  Q  -
18 0-4   R  -  -  R  R   
19 0-4   -  S  -  S  S   
20 0-5   T  -  -  T      
21 0-6   U  U  U  U      
22 1-3   V  V  -         
23 1-4   -  -  X         
24 1-4   X  Y            
25 1-4   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SQSJJ TSVRA NSTWT LZJKD QZJXV C
-------------------------------
