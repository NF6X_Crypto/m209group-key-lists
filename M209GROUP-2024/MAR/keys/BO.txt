EFFECTIVE PERIOD:
10-MAR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  -  -
02 2-0   -  -  -  -  B  -
03 0-3   C  -  -  C  -  C
04 0-4   -  D  D  D  -  -
05 0-4   -  E  E  -  E  -
06 0-4   -  F  F  -  F  F
07 0-4   -  G  G  G  G  -
08 0-4   H  -  H  -  -  -
09 0-6   I  -  -  I  I  I
10 0-6   J  J  -  J  -  -
11 0-6   K  -  K  K  K  -
12 0-6   L  L  -  L  -  L
13 0-6   -  -  M  M  M  M
14 0-6   -  -  N  -  N  N
15 0-6   O  O  -  O  O  -
16 1-2   -  -  -  -  -  P
17 2-3   Q  -  Q  Q  -  -
18 2-3   R  R  -  -  -   
19 2-3   -  S  S  S  -   
20 2-4   -  -  -  -      
21 2-4   U  -  U  -      
22 2-4   V  V  -         
23 2-4   -  -  -         
24 2-5   X  -            
25 3-5   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TLUVQ OKNAL ZJTHN ANTUS AMVPZ Q
-------------------------------
