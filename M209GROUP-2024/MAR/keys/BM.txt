EFFECTIVE PERIOD:
08-MAR-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  B  B  B  -
03 2-0   C  -  C  C  -  -
04 2-0   D  -  D  -  -  -
05 2-0   -  -  -  E  -  -
06 2-0   F  F  -  F  -  F
07 2-0   -  G  -  G  -  -
08 0-3   -  H  -  -  H  H
09 0-3   -  -  I  I  I  I
10 0-3   -  J  J  -  J  J
11 0-3   -  K  -  K  K  -
12 0-4   L  -  L  -  L  L
13 0-4   M  -  M  M  -  M
14 0-4   -  -  -  -  N  -
15 0-4   -  O  O  -  O  -
16 0-4   P  -  P  -  P  -
17 0-4   -  Q  Q  -  -  -
18 0-4   R  R  R  -  R   
19 0-4   -  S  -  S  S   
20 0-4   T  T  T  -      
21 0-5   -  U  -  U      
22 1-3   V  -  -         
23 1-6   -  -  -         
24 2-3   X  Y            
25 2-4   -  -            
26 2-4   Z               
27 2-4                   
-------------------------------
26 LETTER CHECK

WGROR IABPS PRQUP NNSPT IXJEG O
-------------------------------
