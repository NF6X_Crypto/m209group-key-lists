EFFECTIVE PERIOD:
13-JUN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 2-0   B  B  B  -  B  B
03 0-3   C  C  C  -  C  -
04 0-4   -  D  D  -  D  D
05 0-4   E  -  E  E  -  -
06 0-4   -  F  F  F  -  F
07 0-4   G  -  G  -  G  G
08 0-5   H  H  -  H  H  H
09 0-5   I  I  -  I  I  I
10 0-5   -  -  -  -  -  -
11 0-5   -  -  -  K  -  -
12 0-6   -  -  -  L  -  -
13 0-6   -  M  M  -  -  -
14 0-6   -  -  -  -  N  -
15 0-6   -  O  -  -  -  -
16 0-6   P  -  P  P  P  P
17 0-6   Q  Q  -  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   S  -  -  -  S   
20 0-6   -  T  T  -      
21 0-6   -  -  -  U      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-3   X  -            
25 1-4   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

DCIIM TQVUM RJRWZ VNHDI FFOSS V
-------------------------------
