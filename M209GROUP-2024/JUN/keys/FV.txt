EFFECTIVE PERIOD:
29-JUN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  A
02 2-0   B  -  B  -  -  -
03 0-3   -  C  C  -  C  C
04 0-3   -  D  -  -  D  -
05 0-3   -  E  E  -  -  E
06 0-3   F  -  -  F  -  F
07 0-4   G  -  G  -  G  -
08 0-4   H  H  H  H  -  H
09 0-4   -  -  -  I  I  -
10 0-4   J  J  J  -  -  J
11 0-4   K  K  K  -  K  K
12 0-6   L  -  L  L  L  L
13 0-6   -  M  -  M  -  M
14 0-6   -  -  N  -  -  N
15 0-6   -  O  O  O  O  -
16 1-2   -  -  P  -  -  -
17 1-3   Q  Q  -  -  Q  -
18 1-3   R  -  -  -  R   
19 1-5   S  S  -  S  S   
20 2-3   T  -  T  T      
21 2-3   -  U  -  U      
22 2-4   V  -  V         
23 3-6   W  -  -         
24 3-6   -  Y            
25 3-6   -  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ASAMI AWSTO GVSFA FYYRR AWDVR Q
-------------------------------
