EFFECTIVE PERIOD:
09-JUN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  -
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  -  -  -
04 0-3   -  D  -  -  D  -
05 0-4   -  E  -  -  E  E
06 0-4   F  -  -  F  F  F
07 0-4   -  G  -  -  -  G
08 0-4   -  -  H  H  H  -
09 0-4   -  -  -  -  -  -
10 0-4   J  J  J  -  J  J
11 0-6   -  K  -  -  K  -
12 0-6   L  L  -  L  L  L
13 0-6   -  -  -  M  M  -
14 0-6   N  -  N  -  -  N
15 0-6   -  -  O  O  O  O
16 0-6   P  P  P  -  -  -
17 0-6   Q  Q  -  -  -  Q
18 1-3   R  -  R  -  -   
19 1-6   -  -  S  S  S   
20 2-3   -  T  -  T      
21 2-4   -  -  U  U      
22 3-4   V  -  -         
23 4-5   W  -  X         
24 4-6   -  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TUTMP EYTUT MRSON CYFME ZVHXH P
-------------------------------
