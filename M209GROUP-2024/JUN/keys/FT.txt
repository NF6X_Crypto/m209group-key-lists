EFFECTIVE PERIOD:
27-JUN-2024 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 0-3   B  -  B  -  -  B
03 0-3   -  C  C  -  C  C
04 0-3   -  -  D  D  -  -
05 0-3   E  E  -  -  -  E
06 0-3   F  -  -  F  F  F
07 0-4   G  -  G  G  G  G
08 0-4   -  -  -  -  H  -
09 0-4   I  I  I  -  I  -
10 0-4   J  J  J  -  -  -
11 0-4   -  K  -  K  -  -
12 0-4   -  L  L  L  -  L
13 0-4   -  -  -  -  M  -
14 0-5   N  N  -  -  N  N
15 0-5   O  O  O  -  O  O
16 0-5   -  P  P  -  -  -
17 0-5   Q  Q  -  -  -  -
18 0-5   R  -  R  R  R   
19 0-5   -  S  -  S  S   
20 0-6   T  -  -  -      
21 0-6   -  U  -  U      
22 1-2   -  -  V         
23 1-6   -  -  -         
24 3-4   -  Y            
25 3-4   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ATRKN IRKSN INUSQ TRCLR VSIZN T
-------------------------------
