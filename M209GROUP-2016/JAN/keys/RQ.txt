EFFECTIVE PERIOD:
30-JAN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  -
02 2-0   B  B  B  -  -  -
03 2-0   -  -  -  C  -  C
04 2-0   D  -  -  D  D  D
05 0-3   -  E  E  E  -  -
06 0-3   -  -  F  -  -  F
07 0-3   G  -  -  G  -  G
08 0-3   -  H  H  -  H  H
09 0-4   I  -  I  I  -  I
10 0-4   J  J  -  -  -  -
11 0-4   K  K  K  -  K  -
12 0-4   L  -  L  L  L  -
13 0-6   M  M  -  M  -  M
14 0-6   -  -  -  -  -  -
15 0-6   O  O  O  O  O  -
16 0-6   -  P  P  P  P  -
17 0-6   Q  -  Q  -  Q  -
18 1-5   R  R  -  -  -   
19 2-3   -  S  S  S  S   
20 2-5   -  -  T  T      
21 3-4   -  -  -  U      
22 4-6   V  V  -         
23 4-6   -  X  X         
24 4-6   -  Y            
25 4-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RLVVI JORRW QRWPR AZONM TTDRR L
-------------------------------
