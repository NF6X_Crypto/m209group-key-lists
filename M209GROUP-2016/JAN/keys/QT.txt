EFFECTIVE PERIOD:
07-JAN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 2-0   -  B  B  -  B  -
03 2-0   C  C  -  -  C  C
04 2-0   -  -  -  -  D  D
05 2-0   E  -  -  E  -  -
06 2-0   -  -  -  F  F  F
07 0-3   G  G  G  G  G  -
08 0-3   H  H  H  H  -  -
09 0-3   I  I  -  I  -  -
10 0-3   J  J  -  -  -  J
11 0-3   K  K  -  K  K  -
12 0-3   -  -  L  -  L  L
13 0-3   -  M  -  -  -  M
14 0-3   -  N  N  N  -  N
15 0-3   -  O  O  -  -  O
16 0-4   P  P  -  P  -  -
17 0-5   Q  -  Q  -  -  -
18 0-6   R  -  -  R  R   
19 0-6   S  -  S  S  -   
20 0-6   -  T  T  -      
21 0-6   U  U  -  -      
22 2-3   V  -  V         
23 2-3   -  X  X         
24 2-3   -  -            
25 2-6   Y  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UZGUF VWZRU HUNND DTVQR OXCRU K
-------------------------------
