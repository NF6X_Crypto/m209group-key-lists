EFFECTIVE PERIOD:
04-JAN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  -  -  B  -  -
03 1-0   C  -  C  -  C  C
04 1-0   -  D  D  D  D  -
05 0-4   E  E  E  -  -  E
06 0-4   F  F  F  -  F  F
07 0-4   G  G  -  -  -  -
08 0-4   H  -  -  H  H  H
09 0-4   -  I  -  I  I  I
10 0-4   -  J  -  J  -  J
11 0-5   -  K  K  K  K  -
12 0-5   -  -  -  -  -  L
13 0-5   M  M  -  -  -  -
14 0-5   -  -  N  -  N  N
15 0-5   -  O  O  O  O  -
16 1-2   -  -  P  -  -  -
17 1-3   -  -  -  Q  -  -
18 1-3   -  -  -  -  R   
19 1-4   S  S  -  S  S   
20 1-5   -  T  T  T      
21 1-5   -  U  -  U      
22 1-5   -  V  V         
23 1-5   W  X  X         
24 2-3   X  -            
25 2-6   Y  -            
26 3-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FEUSL CWAVV RMVTF UAVZL FMJVE W
-------------------------------
