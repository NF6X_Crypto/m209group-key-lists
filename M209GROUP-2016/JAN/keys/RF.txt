EFFECTIVE PERIOD:
19-JAN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  -
02 0-3   B  B  -  B  -  -
03 0-3   C  -  -  -  C  C
04 0-3   -  D  D  D  -  D
05 0-3   E  E  -  E  E  -
06 0-3   -  F  -  -  F  F
07 0-3   -  -  G  -  G  -
08 0-3   H  H  -  -  H  -
09 0-5   -  -  -  I  -  I
10 0-5   J  -  J  J  -  J
11 0-5   K  K  -  -  K  K
12 0-5   -  -  -  L  -  L
13 0-5   M  -  M  -  -  -
14 0-6   -  N  N  N  -  N
15 0-6   -  O  O  O  -  -
16 0-6   P  -  P  -  P  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  R  -  R  -   
19 0-6   -  S  S  -  -   
20 1-3   -  T  -  T      
21 2-4   -  -  U  -      
22 2-6   V  -  -         
23 3-5   -  X  -         
24 3-5   -  -            
25 3-5   Y  -            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VUMVM UIMMJ ZZTSM TVITL JDMZU O
-------------------------------
