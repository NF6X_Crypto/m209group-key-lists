EFFECTIVE PERIOD:
28-JAN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  C  C  -
04 1-0   -  -  -  D  D  D
05 1-0   E  -  -  -  -  E
06 1-0   -  F  -  -  -  F
07 1-0   -  G  G  -  -  -
08 1-0   H  -  H  H  -  H
09 2-0   -  I  -  I  -  I
10 2-0   -  J  -  J  -  -
11 2-0   K  K  -  -  K  -
12 2-0   -  -  L  L  -  L
13 0-3   M  M  -  -  M  M
14 0-3   N  -  N  N  -  -
15 0-3   -  O  -  -  O  -
16 0-3   P  P  -  -  P  -
17 0-4   Q  -  -  Q  -  Q
18 0-5   -  R  -  -  R   
19 0-6   -  S  S  S  S   
20 0-6   -  T  T  T      
21 0-6   -  -  U  U      
22 1-6   V  V  V         
23 1-6   -  X  -         
24 1-6   X  -            
25 2-3   -  -            
26 2-6   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

VWGGI KWMAA WOVFG OLLRL IVCZK O
-------------------------------
