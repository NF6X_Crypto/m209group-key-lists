EFFECTIVE PERIOD:
12-JUN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  -  -
02 2-0   -  -  -  B  -  B
03 0-3   C  C  -  C  C  C
04 0-3   -  D  -  D  -  -
05 0-3   -  -  E  E  -  E
06 0-3   F  -  F  F  -  F
07 0-3   -  G  G  -  G  -
08 0-3   H  -  H  -  H  -
09 0-4   -  I  -  I  -  -
10 0-4   -  -  -  -  J  J
11 0-4   K  K  -  K  -  K
12 0-4   -  -  L  -  L  -
13 0-4   M  M  -  -  M  M
14 0-4   N  -  N  -  -  -
15 0-5   -  O  -  O  -  -
16 0-5   P  -  P  -  -  P
17 0-5   -  -  Q  -  Q  -
18 1-4   R  R  R  R  R   
19 2-3   -  -  -  S  S   
20 2-4   -  T  T  -      
21 2-6   U  U  U  -      
22 3-5   V  V  -         
23 4-5   W  -  X         
24 4-5   X  Y            
25 4-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UHVLH UATCM QQNQA OKAAU HMBXI T
-------------------------------
