EFFECTIVE PERIOD:
10-JUN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 2-0   -  B  B  -  B  -
03 0-3   -  -  -  C  -  -
04 0-3   -  -  D  D  -  -
05 0-3   E  -  -  -  -  -
06 0-3   -  -  -  F  -  F
07 0-3   G  G  G  G  G  G
08 0-3   H  -  -  H  H  H
09 0-3   I  -  -  -  -  -
10 0-4   -  J  -  J  J  J
11 0-4   K  K  K  -  -  -
12 0-4   L  L  L  L  -  -
13 0-4   -  M  -  -  M  -
14 0-4   N  -  -  -  -  -
15 0-4   O  O  -  O  O  O
16 0-4   P  P  P  -  P  P
17 0-5   Q  -  Q  -  Q  Q
18 1-4   -  -  -  R  R   
19 1-5   S  S  S  S  S   
20 1-6   -  -  -  -      
21 2-3   -  -  U  U      
22 2-5   V  V  -         
23 3-4   -  X  X         
24 3-4   X  Y            
25 3-4   -  -            
26 3-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

GIURH YQSSZ CXRMQ QVIOC ZRITF T
-------------------------------
