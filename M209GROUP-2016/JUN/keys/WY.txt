EFFECTIVE PERIOD:
16-JUN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 0-3   B  -  B  -  B  B
03 0-3   -  -  -  C  -  -
04 0-3   -  D  D  D  D  D
05 0-3   E  -  E  E  E  -
06 0-3   F  F  -  F  -  -
07 0-3   G  -  G  G  G  -
08 0-4   -  H  -  -  -  -
09 0-4   I  -  -  I  -  I
10 0-4   J  -  -  J  J  -
11 0-4   -  -  K  K  K  -
12 0-4   L  L  -  -  -  L
13 0-4   -  -  -  M  M  M
14 0-4   -  N  N  N  N  N
15 0-4   -  O  -  -  -  O
16 0-4   -  -  P  -  -  P
17 0-4   Q  -  Q  Q  -  -
18 0-6   R  R  R  -  R   
19 0-6   -  -  -  S  S   
20 0-6   T  -  -  -      
21 0-6   -  -  U  -      
22 0-6   V  V  -         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   Y  Z            
26 1-3   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

RTPPZ HLIJI ISLQS TILTT PSUGA A
-------------------------------
