EFFECTIVE PERIOD:
30-JUN-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   -  -  -  -  -  -
03 2-0   -  -  -  C  -  -
04 2-0   -  D  D  D  -  D
05 2-0   -  E  E  E  E  -
06 2-0   F  -  -  -  -  -
07 0-3   G  G  -  G  G  G
08 0-3   H  H  -  -  H  H
09 0-3   -  -  I  -  I  -
10 0-3   -  J  J  -  J  J
11 0-3   K  -  K  K  K  K
12 0-3   L  L  -  -  -  -
13 0-4   M  -  -  -  -  M
14 0-6   N  N  -  N  N  -
15 0-6   -  -  O  O  O  O
16 0-6   P  -  P  -  -  P
17 0-6   Q  Q  Q  -  -  -
18 1-3   -  -  -  -  R   
19 1-4   -  -  S  S  S   
20 2-3   -  T  T  -      
21 2-3   U  -  -  -      
22 2-3   -  -  V         
23 2-3   -  -  -         
24 2-6   -  -            
25 3-4   -  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

HPAMF ZPGMD RPEUY PISUU TIUEZ M
-------------------------------
