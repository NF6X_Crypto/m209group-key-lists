EFFECTIVE PERIOD:
31-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   B  -  B  -  -  -
03 2-0   -  C  C  -  -  C
04 2-0   -  -  -  D  -  D
05 2-0   -  -  E  -  -  -
06 0-3   -  F  F  F  F  F
07 0-3   G  G  -  G  G  -
08 0-3   H  -  -  -  -  -
09 0-3   -  -  -  -  I  I
10 0-3   -  J  J  -  J  -
11 0-3   -  -  -  K  -  K
12 0-4   -  L  L  L  L  -
13 0-4   M  M  -  -  -  -
14 0-4   -  N  -  -  -  N
15 0-4   -  O  O  O  -  -
16 1-4   P  -  P  -  P  P
17 1-5   Q  Q  -  -  Q  -
18 1-6   -  -  R  R  R   
19 2-3   S  S  -  S  S   
20 2-4   T  T  -  T      
21 2-4   -  U  U  -      
22 2-4   -  V  V         
23 2-4   W  X  -         
24 3-4   X  -            
25 3-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PLYFU USAUO LUVKU UKPCU TINTS S
-------------------------------
