EFFECTIVE PERIOD:
12-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  A  A
02 2-0   -  B  -  B  B  B
03 2-0   -  C  C  C  -  -
04 2-0   D  D  D  D  D  D
05 2-0   -  -  -  -  E  E
06 2-0   F  F  F  F  -  -
07 0-4   -  -  G  -  -  G
08 0-4   -  H  -  H  -  H
09 0-4   -  -  I  -  -  -
10 0-4   J  -  J  J  -  J
11 0-4   K  -  K  K  -  -
12 0-5   -  L  L  L  L  -
13 0-5   -  M  M  -  M  -
14 0-6   -  N  -  N  N  -
15 0-6   O  -  -  -  -  O
16 0-6   P  P  -  P  P  -
17 0-6   Q  Q  Q  -  -  -
18 0-6   -  R  R  -  R   
19 0-6   S  S  S  -  -   
20 1-6   T  -  -  -      
21 2-4   -  U  -  -      
22 2-5   -  -  V         
23 3-5   W  X  -         
24 4-6   -  -            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

EUKSV AKKVO QUTTX AYZRN OORKX O
-------------------------------
