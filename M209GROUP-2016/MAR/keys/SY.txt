EFFECTIVE PERIOD:
04-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  -
02 2-0   -  B  B  B  -  B
03 2-0   C  -  C  -  -  C
04 2-0   -  -  -  -  -  D
05 2-0   E  E  -  -  -  E
06 0-3   -  F  -  -  F  -
07 0-3   -  -  -  -  G  G
08 0-3   -  H  H  -  -  H
09 0-3   -  -  I  I  I  -
10 0-3   J  J  -  J  J  J
11 0-3   K  K  K  K  -  K
12 0-3   L  -  -  L  -  -
13 0-4   -  -  M  -  M  -
14 0-5   N  -  -  N  -  -
15 0-6   -  O  -  O  -  O
16 0-6   -  P  -  -  P  P
17 0-6   Q  Q  Q  -  Q  -
18 0-6   R  -  R  -  R   
19 0-6   -  S  S  -  -   
20 1-2   -  -  -  T      
21 2-6   U  U  U  U      
22 2-6   V  -  V         
23 2-6   -  X  X         
24 2-6   -  -            
25 3-5   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PNLIZ UMKAS TSSNM JSUUB VPUDD S
-------------------------------
