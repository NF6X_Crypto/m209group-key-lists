EFFECTIVE PERIOD:
23-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  -  -  -  -  -
03 1-0   C  -  C  -  -  C
04 1-0   D  -  -  -  D  -
05 1-0   -  E  -  E  -  -
06 2-0   F  F  F  F  -  -
07 0-3   G  G  G  G  G  -
08 0-3   -  -  H  -  H  H
09 0-4   I  I  -  I  -  -
10 0-5   -  J  -  -  -  -
11 0-5   -  K  K  K  K  K
12 0-5   -  L  -  -  L  L
13 0-5   M  -  M  M  M  M
14 0-5   -  N  N  -  -  N
15 0-5   O  -  O  -  O  -
16 0-5   -  -  -  -  P  P
17 0-5   -  Q  -  Q  Q  Q
18 0-5   R  R  -  -  -   
19 0-6   -  -  -  S  -   
20 0-6   T  -  T  T      
21 0-6   U  -  -  U      
22 0-6   V  V  -         
23 0-6   -  X  X         
24 1-3   -  Y            
25 1-6   -  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UTNQS VQCCM ARDIX PKRPY UCUYO I
-------------------------------
