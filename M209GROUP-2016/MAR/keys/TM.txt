EFFECTIVE PERIOD:
18-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  -  B  -  B
03 1-0   C  -  C  C  C  C
04 1-0   D  -  -  -  -  -
05 2-0   E  E  -  E  -  E
06 2-0   -  F  -  -  F  -
07 2-0   -  G  G  -  -  G
08 2-0   -  H  H  -  -  -
09 0-4   -  -  -  -  -  I
10 0-4   J  J  -  -  J  J
11 0-4   K  -  -  K  K  K
12 0-5   L  -  -  -  -  L
13 0-5   -  -  M  M  -  -
14 0-5   -  N  N  N  N  -
15 0-5   O  O  O  O  -  -
16 0-5   -  -  P  P  -  -
17 0-5   Q  -  Q  Q  Q  Q
18 1-3   R  R  -  -  R   
19 1-4   S  S  -  S  S   
20 1-5   -  -  T  -      
21 2-4   U  -  U  U      
22 2-4   V  V  -         
23 2-4   -  -  X         
24 2-4   -  -            
25 2-5   Y  Z            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

PPWUO NWRRP VOHWG QLSQJ PWPKW A
-------------------------------
