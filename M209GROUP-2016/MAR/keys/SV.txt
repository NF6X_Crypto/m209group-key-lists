EFFECTIVE PERIOD:
01-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  -
02 1-0   -  -  -  B  -  B
03 1-0   -  C  C  -  C  -
04 1-0   D  D  -  -  -  -
05 2-0   E  -  E  E  E  -
06 2-0   -  F  -  -  -  F
07 2-0   -  -  -  -  -  -
08 0-3   -  H  -  -  H  H
09 0-3   I  -  -  I  -  -
10 0-3   -  -  -  J  J  J
11 0-3   -  K  K  -  K  -
12 0-6   -  -  L  -  -  L
13 0-6   M  -  M  M  M  -
14 0-6   N  N  -  N  -  N
15 0-6   O  O  O  O  O  O
16 0-6   -  P  -  P  -  -
17 0-6   Q  Q  -  -  -  Q
18 1-2   R  R  R  R  -   
19 1-2   S  -  -  S  -   
20 1-2   T  T  T  -      
21 1-2   U  U  -  -      
22 1-3   -  V  V         
23 1-4   W  -  -         
24 1-5   X  Y            
25 2-6   Y  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

JAPWX PVOXP QZOJA KOCRK OVITU V
-------------------------------
