EFFECTIVE PERIOD:
26-MAR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   -  -  B  -  B  B
03 1-0   C  C  C  C  -  -
04 1-0   -  -  -  D  D  D
05 1-0   -  E  -  E  E  E
06 2-0   -  -  F  -  -  F
07 2-0   G  G  -  -  -  -
08 2-0   -  H  H  -  H  -
09 2-0   I  -  -  I  -  -
10 2-0   J  -  -  J  J  -
11 2-0   K  K  K  -  K  K
12 0-6   -  -  -  L  -  -
13 0-6   -  M  M  M  M  M
14 0-6   -  N  N  -  -  N
15 0-6   O  -  -  -  -  O
16 0-6   -  -  P  P  -  -
17 0-6   -  Q  -  -  Q  Q
18 1-3   R  -  R  R  -   
19 1-3   -  S  -  S  -   
20 1-4   -  -  -  T      
21 1-6   -  U  U  -      
22 1-6   V  -  -         
23 1-6   W  X  -         
24 1-6   -  Y            
25 2-3   Y  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

PAALU BZNRU KUPTZ KZAJS UZDUJ U
-------------------------------
