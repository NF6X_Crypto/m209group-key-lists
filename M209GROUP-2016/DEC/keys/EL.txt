EFFECTIVE PERIOD:
28-DEC-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  B  B  B  B  B
03 2-0   -  -  -  -  C  C
04 2-0   -  D  -  D  -  -
05 0-3   E  E  E  E  E  E
06 0-3   F  -  F  -  F  -
07 0-3   -  G  G  G  -  -
08 0-3   H  -  -  -  -  -
09 0-3   I  I  -  I  -  I
10 0-3   -  -  -  J  J  J
11 0-4   K  K  -  K  K  K
12 0-5   L  -  L  L  -  -
13 0-5   M  M  M  -  M  M
14 0-5   -  -  -  -  -  N
15 0-5   O  -  O  O  O  O
16 0-5   -  P  P  -  P  -
17 0-5   Q  Q  -  -  -  Q
18 0-5   -  R  -  R  -   
19 0-5   -  S  S  -  -   
20 0-5   -  -  T  -      
21 0-5   -  -  U  -      
22 0-5   -  V  -         
23 0-5   -  -  X         
24 1-2   X  Y            
25 1-3   -  -            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AYMQQ JHKQX SLROK OKRSI ONVUG K
-------------------------------
