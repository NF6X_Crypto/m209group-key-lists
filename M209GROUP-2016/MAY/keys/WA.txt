EFFECTIVE PERIOD:
23-MAY-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  -
02 2-0   -  -  -  -  B  B
03 2-0   C  C  C  -  C  C
04 0-3   -  D  -  D  -  D
05 0-3   -  E  -  E  E  -
06 0-3   F  -  F  -  -  F
07 0-3   -  -  -  G  -  G
08 0-3   -  -  -  H  H  -
09 0-5   I  I  -  I  I  I
10 0-5   J  J  J  -  -  -
11 0-5   -  -  K  K  K  -
12 0-5   -  L  L  -  -  L
13 0-5   -  -  -  M  M  M
14 0-5   -  -  N  -  N  -
15 0-5   -  O  O  -  O  O
16 1-2   P  -  -  -  -  P
17 1-4   -  -  Q  Q  -  -
18 2-3   -  -  -  R  R   
19 2-3   S  -  S  S  -   
20 2-4   -  -  -  T      
21 2-5   U  -  -  U      
22 2-5   V  V  -         
23 2-5   -  X  X         
24 2-5   X  -            
25 2-6   Y  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

TUMVD MPNVW UMWNL APCMT AWTII V
-------------------------------
