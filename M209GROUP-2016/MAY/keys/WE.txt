EFFECTIVE PERIOD:
27-MAY-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: WE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   -  -  -  B  -  -
03 1-0   -  -  -  -  C  C
04 1-0   D  -  -  -  -  -
05 1-0   E  E  E  E  -  E
06 2-0   -  F  F  F  -  F
07 2-0   G  G  G  G  G  G
08 2-0   H  H  H  H  -  -
09 2-0   -  I  -  I  I  I
10 2-0   J  -  J  -  -  -
11 2-0   K  -  -  K  -  K
12 2-0   -  L  L  L  -  -
13 0-3   -  M  -  M  M  -
14 0-3   N  -  N  N  N  -
15 0-4   -  O  -  -  O  -
16 0-4   P  P  -  -  P  -
17 0-4   -  Q  -  Q  Q  Q
18 0-4   -  -  -  -  -   
19 0-5   -  S  -  -  -   
20 1-3   -  T  -  -      
21 1-4   U  U  U  -      
22 2-4   V  -  V         
23 2-4   W  X  X         
24 2-4   -  -            
25 2-4   Y  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

ZSNBS USKPS KHAJS ZVZSU QSSQJ Q
-------------------------------
