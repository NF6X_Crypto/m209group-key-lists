EFFECTIVE PERIOD:
08-APR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  B  -  B  -  -
03 1-0   -  -  C  -  C  C
04 1-0   -  -  D  -  -  -
05 1-0   E  E  E  E  -  -
06 1-0   F  F  F  F  F  -
07 1-0   -  G  -  -  G  G
08 1-0   H  -  -  -  -  -
09 2-0   -  -  -  -  -  I
10 2-0   J  J  -  J  J  -
11 2-0   K  K  -  K  K  -
12 2-0   -  -  -  L  L  L
13 2-0   -  -  M  M  -  M
14 2-0   -  N  N  -  -  N
15 2-0   O  O  -  O  O  -
16 2-0   -  -  P  P  P  -
17 2-0   -  -  Q  Q  Q  -
18 2-0   R  R  R  -  -   
19 2-0   -  S  S  -  -   
20 2-0   -  -  -  -      
21 0-3   U  U  U  U      
22 0-3   -  -  -         
23 0-4   W  X  -         
24 1-2   X  Y            
25 1-3   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SBOSS NMNWY BVQFF ZVNMC YWEFP Z
-------------------------------
