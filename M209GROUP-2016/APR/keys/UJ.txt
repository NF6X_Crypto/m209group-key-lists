EFFECTIVE PERIOD:
10-APR-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  B  B  -  -
03 1-0   -  -  -  -  C  -
04 2-0   D  -  D  -  D  -
05 2-0   E  -  E  -  -  E
06 2-0   F  F  -  -  -  -
07 2-0   G  -  -  G  -  G
08 2-0   -  H  -  H  -  -
09 2-0   I  I  I  -  I  I
10 2-0   -  -  J  J  J  -
11 0-3   -  -  -  -  K  K
12 0-3   L  L  L  L  -  L
13 0-3   -  M  -  M  M  M
14 0-3   -  N  N  N  -  -
15 0-3   -  -  -  O  O  -
16 0-3   -  P  -  -  -  P
17 0-3   -  Q  Q  Q  -  -
18 0-3   R  R  -  -  -   
19 0-3   -  S  -  -  -   
20 0-3   T  T  T  T      
21 0-3   -  U  -  -      
22 0-3   V  -  V         
23 0-3   W  X  -         
24 0-4   X  -            
25 0-6   -  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PKNVM HXGYB WSZNQ GMFXY CONVH B
-------------------------------
