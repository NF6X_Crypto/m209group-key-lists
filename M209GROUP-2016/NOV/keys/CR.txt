EFFECTIVE PERIOD:
12-NOV-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  -
02 1-0   B  -  -  -  -  -
03 0-3   -  C  -  -  -  -
04 0-3   D  -  -  D  D  D
05 0-3   E  E  E  E  -  E
06 0-3   -  -  F  -  F  F
07 0-5   -  G  -  -  G  -
08 0-5   H  H  -  H  -  -
09 0-5   -  I  -  -  -  I
10 0-5   -  -  -  J  J  -
11 0-5   -  -  K  K  K  -
12 0-6   L  -  L  L  -  -
13 0-6   -  M  M  -  M  -
14 0-6   -  N  -  -  N  N
15 0-6   -  -  O  -  O  O
16 0-6   -  -  -  P  P  P
17 0-6   Q  Q  -  Q  Q  Q
18 1-3   -  R  R  -  R   
19 1-3   S  -  S  S  -   
20 1-4   -  T  T  T      
21 1-6   -  -  U  U      
22 2-3   -  -  V         
23 3-5   W  X  -         
24 3-5   X  -            
25 3-5   Y  Z            
26 3-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZUKWY XYOOF NVMLU VAKQB YXARR K
-------------------------------
