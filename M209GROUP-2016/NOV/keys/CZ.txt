EFFECTIVE PERIOD:
20-NOV-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: CZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  -  -  -  B  -
03 1-0   C  -  C  -  C  -
04 1-0   D  -  D  D  -  -
05 1-0   -  E  -  E  -  E
06 0-3   -  -  -  F  -  F
07 0-3   G  G  G  -  G  G
08 0-3   -  H  -  -  -  -
09 0-4   I  I  -  I  I  I
10 0-4   J  -  J  -  J  -
11 0-4   K  K  -  -  -  -
12 0-4   L  L  -  -  L  L
13 0-4   -  -  -  M  -  M
14 0-5   -  -  N  N  -  -
15 0-5   O  -  -  -  O  -
16 0-5   -  P  -  -  -  -
17 0-5   Q  Q  -  Q  Q  -
18 1-2   -  R  R  -  -   
19 1-4   S  -  S  -  -   
20 2-5   -  -  T  -      
21 2-5   U  -  U  U      
22 2-6   -  -  -         
23 3-4   -  -  -         
24 3-5   -  -            
25 3-5   Y  Z            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

TPDRN UHIQW VHOUW DNXDV GPXHK M
-------------------------------
