EFFECTIVE PERIOD:
22-NOV-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 2-0   B  B  B  B  -  -
03 2-0   C  C  -  C  C  -
04 0-3   D  -  D  -  D  D
05 0-3   -  E  E  -  -  E
06 0-3   F  -  -  -  F  -
07 0-3   -  -  -  G  G  G
08 0-3   H  -  -  -  H  -
09 0-4   I  I  -  I  -  I
10 0-4   J  -  J  -  -  J
11 0-5   -  -  K  -  K  -
12 0-5   -  -  -  L  -  -
13 0-5   M  -  M  M  -  -
14 0-5   N  N  -  -  N  N
15 0-5   -  -  -  -  -  -
16 0-5   -  -  -  P  P  P
17 0-5   -  Q  Q  -  Q  -
18 0-5   -  -  -  R  -   
19 0-5   -  -  S  -  -   
20 1-2   -  T  T  -      
21 1-6   U  -  -  -      
22 2-4   V  V  V         
23 3-4   W  X  X         
24 3-5   -  -            
25 3-5   -  -            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

DZQNM AMFWM RZFYV UCDPP PQKPV Z
-------------------------------
