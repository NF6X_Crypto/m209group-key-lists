EFFECTIVE PERIOD:
21-NOV-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 0-3   -  -  -  -  -  B
03 0-3   -  C  -  C  C  C
04 0-3   -  D  D  -  -  -
05 0-3   E  -  E  -  E  -
06 0-3   -  -  -  F  -  -
07 0-3   G  -  G  -  -  G
08 0-3   -  H  H  H  H  H
09 0-4   -  I  I  -  -  I
10 0-4   J  -  J  -  -  J
11 0-4   K  -  -  -  -  -
12 0-4   L  L  L  L  L  -
13 0-5   M  M  M  M  M  -
14 0-5   N  -  -  N  N  -
15 0-5   -  O  O  O  -  O
16 0-5   -  -  -  -  P  P
17 0-5   Q  Q  Q  -  -  Q
18 0-5   -  R  -  R  R   
19 0-6   S  S  -  S  -   
20 1-4   T  -  T  -      
21 1-6   -  U  -  -      
22 2-3   -  V  V         
23 3-5   W  -  X         
24 3-5   -  Y            
25 3-5   Y  -            
26 3-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QZQOP HZDUP NFKNT OTOLS RAUTO N
-------------------------------
