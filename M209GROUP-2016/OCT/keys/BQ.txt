EFFECTIVE PERIOD:
16-OCT-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 1-0   -  B  B  -  B  -
03 0-3   C  C  -  C  C  C
04 0-3   -  D  -  D  -  D
05 0-3   -  -  E  -  E  -
06 0-3   -  F  F  -  F  F
07 0-4   -  -  G  G  G  G
08 0-4   -  -  -  -  H  -
09 0-4   I  I  -  -  -  -
10 0-4   -  J  J  -  -  J
11 0-4   -  -  -  -  -  K
12 0-5   L  -  -  -  L  -
13 0-5   M  M  -  M  -  M
14 0-5   -  N  -  -  -  N
15 0-5   -  O  O  O  O  -
16 0-5   P  -  -  -  -  -
17 0-5   Q  Q  -  Q  -  -
18 0-5   R  -  R  R  R   
19 0-6   -  S  S  S  S   
20 1-3   T  -  -  T      
21 1-6   U  U  U  -      
22 2-6   V  V  V         
23 3-4   -  X  -         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

WIPNS RVYUT VRTQR VICZV CJOOI R
-------------------------------
