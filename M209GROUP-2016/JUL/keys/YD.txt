EFFECTIVE PERIOD:
17-JUL-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: YD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 1-0   B  -  -  -  B  B
03 1-0   C  C  -  -  C  C
04 1-0   -  -  -  -  -  D
05 2-0   E  E  E  E  -  E
06 2-0   -  F  F  F  -  -
07 2-0   -  -  -  G  G  -
08 2-0   -  -  H  -  -  -
09 2-0   -  -  I  I  -  I
10 0-3   J  -  J  J  -  J
11 0-3   -  -  K  -  -  -
12 0-4   -  L  L  -  L  L
13 0-6   M  M  M  M  M  M
14 0-6   N  -  -  -  -  -
15 0-6   -  -  -  O  -  O
16 0-6   P  P  -  P  -  -
17 0-6   Q  -  -  -  Q  -
18 0-6   -  -  -  R  R   
19 0-6   S  S  S  -  -   
20 0-6   -  -  -  -      
21 0-6   U  U  U  U      
22 0-6   -  V  -         
23 0-6   -  -  -         
24 1-2   X  -            
25 1-3   -  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

HXMGR JHVTN PWFME AREQP IQKMT M
-------------------------------
