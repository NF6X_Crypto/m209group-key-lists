EFFECTIVE PERIOD:
09-JUL-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  -
02 2-0   B  -  B  -  -  -
03 2-0   C  -  C  -  -  -
04 2-0   -  D  D  D  D  -
05 2-0   E  -  -  E  -  E
06 2-0   F  F  F  -  -  F
07 0-3   G  -  -  -  -  G
08 0-3   H  H  H  -  H  H
09 0-3   -  I  I  I  -  -
10 0-3   -  -  J  J  -  J
11 0-3   K  K  -  -  K  K
12 0-3   -  -  L  -  L  -
13 0-6   M  M  -  -  M  -
14 0-6   -  N  -  -  -  -
15 0-6   O  -  -  O  O  O
16 0-6   -  -  -  P  P  P
17 0-6   Q  -  -  Q  -  -
18 1-3   -  -  -  R  -   
19 1-5   -  -  S  -  -   
20 2-3   T  T  -  -      
21 2-4   -  U  -  -      
22 2-5   V  -  V         
23 2-6   -  -  X         
24 2-6   -  Y            
25 2-6   -  -            
26 2-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

LCACN ZOZKU IATTS KUTVV BALOM C
-------------------------------
