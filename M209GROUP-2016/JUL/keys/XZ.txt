EFFECTIVE PERIOD:
13-JUL-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   B  -  -  -  B  -
03 1-0   -  C  C  C  C  -
04 1-0   -  -  -  D  -  -
05 2-0   E  E  E  -  -  E
06 2-0   -  F  -  -  F  F
07 2-0   -  G  G  G  -  -
08 2-0   H  -  H  H  -  H
09 2-0   I  -  I  -  -  -
10 2-0   J  J  -  -  J  -
11 2-0   K  -  -  -  K  -
12 0-3   -  L  L  L  L  L
13 0-3   M  -  M  -  M  M
14 0-3   N  -  -  N  -  -
15 0-3   O  O  -  -  -  O
16 1-2   P  -  -  P  -  -
17 1-6   -  -  Q  Q  Q  -
18 2-3   R  R  R  R  R   
19 2-3   S  S  -  -  S   
20 2-3   -  T  T  T      
21 2-3   -  -  -  -      
22 3-4   -  -  V         
23 3-5   W  X  X         
24 3-6   X  -            
25 3-6   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SGJZW ONLAN NVTNO RUVTK TTUFL T
-------------------------------
