EFFECTIVE PERIOD:
05-JUL-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: XR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 1-0   B  -  B  B  -  B
03 1-0   C  C  -  -  -  -
04 2-0   -  D  -  -  -  D
05 2-0   -  E  E  -  -  -
06 2-0   F  F  F  -  -  -
07 0-3   G  -  -  G  -  -
08 0-3   -  -  -  -  H  H
09 0-3   I  -  -  I  I  -
10 0-3   J  J  J  -  J  -
11 0-3   -  K  -  -  K  K
12 0-3   -  L  -  L  L  L
13 0-5   -  M  M  -  -  M
14 0-5   -  N  -  N  -  -
15 0-5   -  -  -  O  -  -
16 1-3   P  -  P  P  P  P
17 1-5   Q  -  -  Q  Q  Q
18 1-5   R  -  -  -  R   
19 1-5   -  S  -  S  -   
20 1-5   T  -  T  -      
21 2-3   U  U  U  U      
22 2-5   V  V  -         
23 2-6   -  X  X         
24 4-6   -  -            
25 5-6   Y  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QMTAU QAMQO TTHNQ XTQMX OFQQQ U
-------------------------------
