EFFECTIVE PERIOD:
30-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   B  B  B  -  B  -
03 1-0   -  C  C  -  -  C
04 1-0   -  -  -  D  D  -
05 1-0   E  -  -  E  -  E
06 2-0   -  F  -  -  -  -
07 2-0   G  G  G  -  -  -
08 2-0   -  H  H  H  H  -
09 2-0   -  -  -  -  I  I
10 2-0   -  J  J  -  J  J
11 0-3   K  K  K  K  K  -
12 0-3   L  L  L  -  L  -
13 0-3   -  M  M  -  M  -
14 0-3   N  N  -  N  -  N
15 0-3   O  O  O  -  -  -
16 0-3   -  -  P  P  -  P
17 0-3   -  -  Q  -  Q  Q
18 0-4   R  R  R  R  R   
19 0-6   S  S  -  S  -   
20 1-2   -  -  -  T      
21 1-6   U  -  -  U      
22 2-3   -  V  -         
23 2-3   -  -  X         
24 2-3   -  -            
25 2-3   -  -            
26 3-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

KNSAR KELCP ZZZLZ JVXKN UXAMZ X
-------------------------------
