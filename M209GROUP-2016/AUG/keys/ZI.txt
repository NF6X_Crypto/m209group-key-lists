EFFECTIVE PERIOD:
17-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 1-0   -  B  B  B  -  -
03 2-0   -  C  -  C  -  -
04 2-0   D  -  -  D  D  -
05 2-0   E  -  -  -  E  -
06 2-0   F  -  -  F  F  -
07 0-3   -  G  -  -  G  G
08 0-3   -  -  H  H  -  H
09 0-3   I  I  -  I  I  -
10 0-3   -  -  J  -  J  J
11 0-3   -  K  -  K  -  K
12 0-4   L  -  L  -  -  L
13 0-4   -  -  M  M  -  M
14 0-4   N  -  N  -  -  -
15 0-4   O  O  -  -  O  O
16 0-4   -  P  P  -  P  P
17 0-4   -  -  Q  Q  Q  Q
18 0-4   R  R  -  R  R   
19 0-4   -  -  S  -  -   
20 0-4   -  -  -  -      
21 0-6   U  U  U  -      
22 1-2   V  -  V         
23 1-6   -  -  X         
24 2-3   X  Y            
25 3-4   Y  -            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

YINNN WKHMY KVRIQ DQYCA ZYNKE Z
-------------------------------
