EFFECTIVE PERIOD:
19-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   B  B  -  -  B  -
03 2-0   -  -  -  -  -  C
04 0-4   D  D  D  -  D  D
05 0-4   E  -  E  E  -  E
06 0-4   -  -  -  -  -  -
07 0-4   -  G  G  G  -  -
08 0-5   -  H  -  H  H  -
09 0-5   -  -  -  -  -  -
10 0-5   -  J  J  -  J  -
11 0-5   K  K  -  K  K  K
12 0-5   -  -  -  -  -  L
13 0-6   M  -  M  M  -  M
14 0-6   -  -  N  -  N  -
15 0-6   -  -  -  O  O  O
16 1-2   P  -  -  P  P  P
17 1-2   Q  -  Q  Q  -  Q
18 1-2   R  R  R  R  R   
19 1-2   S  -  S  -  -   
20 1-3   T  T  T  T      
21 1-4   -  U  -  -      
22 2-6   -  V  -         
23 2-6   W  -  -         
24 2-6   -  Y            
25 2-6   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

SNXXX VVVGA QWMGM QGUUO KAOXN L
-------------------------------
