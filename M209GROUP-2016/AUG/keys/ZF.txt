EFFECTIVE PERIOD:
14-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   B  -  -  -  B  -
03 2-0   C  C  C  C  -  C
04 2-0   -  -  -  -  -  D
05 2-0   -  E  E  E  E  -
06 2-0   F  F  -  F  -  F
07 2-0   -  -  -  -  G  G
08 2-0   H  -  -  -  -  -
09 0-3   -  -  I  -  I  -
10 0-4   -  J  -  J  J  -
11 0-5   K  K  K  -  K  K
12 0-5   L  -  L  L  -  L
13 0-5   M  M  -  M  -  -
14 0-5   N  N  N  -  N  -
15 0-5   O  O  O  O  O  -
16 0-5   -  P  P  P  P  P
17 0-5   -  Q  -  Q  -  Q
18 0-5   -  R  -  R  R   
19 0-5   -  -  S  -  -   
20 0-5   T  -  T  T      
21 0-6   U  -  U  -      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 1-4   -  -            
25 1-6   Y  Z            
26 2-5   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

OMUTP JTKSS LETPN HVPUZ KXSAN X
-------------------------------
