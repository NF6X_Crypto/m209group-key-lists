EFFECTIVE PERIOD:
28-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  A
02 2-0   B  -  B  B  -  -
03 2-0   -  -  -  -  C  C
04 2-0   -  -  D  -  -  D
05 2-0   E  -  E  -  -  -
06 2-0   -  -  F  -  -  F
07 2-0   -  G  G  -  -  -
08 0-5   H  -  -  H  H  H
09 0-5   -  -  -  -  -  I
10 0-5   -  J  J  J  J  -
11 0-5   -  -  -  -  K  -
12 0-5   L  L  -  -  -  L
13 0-6   -  -  M  M  M  -
14 0-6   -  -  N  N  N  N
15 0-6   O  O  -  O  O  -
16 0-6   -  P  P  -  -  -
17 0-6   -  Q  Q  Q  -  Q
18 0-6   R  R  -  R  -   
19 0-6   -  -  -  S  -   
20 1-6   -  -  T  T      
21 2-4   U  U  U  -      
22 2-5   V  V  -         
23 3-4   W  X  X         
24 5-6   X  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JATAB CVVSV TKLAM SBRTA ZLTNL K
-------------------------------
