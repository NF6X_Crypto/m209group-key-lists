EFFECTIVE PERIOD:
27-AUG-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ZS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  -  -
02 2-0   B  B  -  B  -  B
03 2-0   -  C  C  -  C  C
04 2-0   -  -  D  D  D  D
05 2-0   E  -  -  -  -  -
06 0-3   -  F  -  F  F  F
07 0-3   G  -  -  -  G  -
08 0-3   -  -  H  H  -  H
09 0-3   -  I  -  I  I  -
10 0-3   -  J  J  J  J  -
11 0-3   -  -  -  K  -  -
12 0-5   L  L  -  -  -  L
13 0-5   M  M  M  -  -  M
14 0-5   N  N  -  -  -  N
15 0-6   O  -  O  O  O  -
16 0-6   -  -  -  P  -  -
17 0-6   Q  -  -  -  Q  -
18 1-4   R  -  R  -  R   
19 2-3   S  S  S  -  S   
20 2-4   T  T  -  T      
21 3-5   -  U  -  -      
22 4-6   V  -  -         
23 4-6   -  -  X         
24 5-6   X  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZRKOO UPLJX VOXUO ARSKX PZVJX N
-------------------------------
