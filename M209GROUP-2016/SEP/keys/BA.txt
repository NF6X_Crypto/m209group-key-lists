EFFECTIVE PERIOD:
30-SEP-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  A
02 2-0   -  -  B  -  B  -
03 2-0   -  C  -  -  -  C
04 2-0   D  -  D  -  D  -
05 2-0   -  -  E  E  E  -
06 2-0   -  -  F  F  F  -
07 2-0   -  -  G  -  -  G
08 2-0   H  -  -  -  -  H
09 2-0   I  I  -  I  -  -
10 2-0   J  J  J  J  J  -
11 0-3   K  -  K  K  -  K
12 0-3   -  L  L  L  -  -
13 0-4   M  M  M  -  M  M
14 0-5   N  -  -  N  N  -
15 0-5   -  -  O  -  -  -
16 0-5   P  P  -  -  P  -
17 0-5   Q  Q  Q  -  Q  Q
18 0-5   -  -  R  R  R   
19 0-5   -  S  -  S  S   
20 0-5   -  T  -  T      
21 0-5   U  -  -  U      
22 0-5   -  V  -         
23 0-5   W  X  -         
24 0-5   X  -            
25 0-5   -  Z            
26 1-6   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

BAXCY XOYLO NPLAE CCOBO APNXP L
-------------------------------
