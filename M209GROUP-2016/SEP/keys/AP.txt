EFFECTIVE PERIOD:
19-SEP-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  -
02 2-0   -  -  B  -  B  B
03 2-0   C  -  -  -  -  C
04 2-0   -  -  D  D  D  D
05 2-0   E  E  E  -  -  -
06 2-0   F  F  F  -  F  -
07 2-0   G  G  -  G  -  G
08 2-0   H  -  -  -  H  -
09 0-3   -  I  -  I  -  I
10 0-4   J  -  J  J  -  -
11 0-4   -  K  -  -  -  K
12 0-4   -  -  -  -  L  -
13 0-4   -  -  -  -  M  M
14 0-5   N  N  -  -  N  N
15 0-5   O  O  -  O  -  -
16 0-5   -  -  P  -  P  -
17 0-5   Q  Q  Q  -  -  -
18 0-5   R  -  -  R  R   
19 0-6   S  -  S  S  -   
20 1-2   T  T  T  T      
21 2-5   -  U  U  -      
22 2-5   V  V  -         
23 2-5   -  X  -         
24 2-5   X  Y            
25 3-6   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

VKAEK UZUPZ QLFOB AMUGW NSZOM W
-------------------------------
