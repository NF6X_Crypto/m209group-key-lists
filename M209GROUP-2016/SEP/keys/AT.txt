EFFECTIVE PERIOD:
23-SEP-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 0-3   B  -  B  B  B  -
03 0-3   -  C  C  C  C  -
04 0-3   -  D  D  D  -  -
05 0-4   -  E  E  -  -  -
06 0-5   F  F  -  F  F  -
07 0-5   G  -  -  -  -  G
08 0-5   -  -  -  H  -  -
09 0-5   -  -  I  -  -  I
10 0-5   -  -  -  -  J  J
11 0-5   K  K  -  -  -  -
12 0-5   -  -  L  L  L  L
13 0-5   -  M  M  -  M  M
14 0-5   -  N  N  N  -  -
15 0-6   O  -  -  -  O  O
16 0-6   -  P  -  P  P  P
17 0-6   Q  Q  -  Q  Q  Q
18 0-6   R  R  R  -  -   
19 0-6   -  S  -  S  S   
20 0-6   T  T  -  -      
21 0-6   U  -  -  U      
22 2-3   -  -  -         
23 2-4   W  X  -         
24 3-6   -  -            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QXNOA WMUCO SHHYG MOMOT NQZYW C
-------------------------------
