EFFECTIVE PERIOD:
21-SEP-2016 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: AR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 2-0   B  -  B  B  -  -
03 0-3   C  C  -  -  C  C
04 0-3   -  D  D  D  D  -
05 0-3   E  -  -  E  E  -
06 0-3   F  -  F  F  F  -
07 0-3   -  G  -  -  -  -
08 0-3   H  H  -  -  -  H
09 0-3   -  I  -  I  -  -
10 0-3   -  -  -  -  -  J
11 0-4   K  -  -  -  -  K
12 0-4   L  L  L  L  -  -
13 0-4   M  -  M  -  M  M
14 0-4   N  N  N  N  -  N
15 0-4   O  O  -  -  O  O
16 0-4   P  P  -  P  -  P
17 0-5   -  Q  Q  -  Q  Q
18 0-5   -  R  R  R  -   
19 0-5   -  -  -  -  S   
20 1-2   T  T  T  T      
21 2-4   U  -  U  -      
22 3-5   -  -  V         
23 3-5   -  X  X         
24 3-5   -  Y            
25 3-5   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QQDZO SMXPQ ZSQAK OCKDD POAOR D
-------------------------------
