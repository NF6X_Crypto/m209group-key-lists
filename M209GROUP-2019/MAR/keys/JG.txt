EFFECTIVE PERIOD:
09-MAR-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  -
02 2-0   B  B  -  -  B  -
03 2-0   -  C  -  C  C  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  E  E  -  E
06 0-3   -  -  F  F  -  F
07 0-3   G  G  -  -  G  G
08 0-3   -  -  -  -  -  -
09 0-3   -  I  I  -  -  I
10 0-3   J  -  -  J  -  J
11 0-3   -  -  -  -  K  -
12 0-5   L  L  L  -  L  -
13 0-5   M  M  M  -  M  M
14 0-5   -  -  -  N  -  -
15 0-5   O  -  -  O  -  -
16 0-5   -  -  -  -  -  -
17 0-5   -  Q  Q  -  Q  -
18 1-5   -  -  R  -  R   
19 1-6   S  -  S  -  -   
20 2-3   -  T  T  -      
21 2-5   U  U  -  U      
22 2-5   V  -  V         
23 2-5   W  -  X         
24 2-5   X  -            
25 3-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MMNJH AABKV UNONU KCLUU NHTMM N
-------------------------------
