EFFECTIVE PERIOD:
28-MAR-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 1-0   B  B  -  B  B  -
03 1-0   -  -  C  -  C  -
04 2-0   D  D  D  D  -  D
05 0-3   -  E  E  -  -  -
06 0-4   -  F  F  F  F  F
07 0-4   -  G  -  -  -  G
08 0-5   H  H  H  H  -  H
09 0-5   -  -  -  I  -  -
10 0-5   -  -  -  -  -  J
11 0-5   K  -  -  -  K  -
12 0-6   L  L  L  L  -  -
13 0-6   -  M  M  -  -  M
14 0-6   N  -  -  N  N  -
15 0-6   -  O  -  -  O  O
16 0-6   P  P  -  P  P  P
17 0-6   Q  -  Q  -  -  Q
18 0-6   -  R  -  -  R   
19 0-6   -  -  S  S  -   
20 0-6   T  -  T  T      
21 0-6   U  U  U  -      
22 0-6   -  -  -         
23 0-6   -  -  X         
24 1-4   -  -            
25 1-5   -  Z            
26 2-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GIRLV SXRFK AZWSW CRFLM DWATU H
-------------------------------
