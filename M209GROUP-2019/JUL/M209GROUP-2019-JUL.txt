SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JUL 2019

    01 JUL 2019  00:00-23:59 GMT:  USE KEY NQ
    02 JUL 2019  00:00-23:59 GMT:  USE KEY NR
    03 JUL 2019  00:00-23:59 GMT:  USE KEY NS
    04 JUL 2019  00:00-23:59 GMT:  USE KEY NT
    05 JUL 2019  00:00-23:59 GMT:  USE KEY NU
    06 JUL 2019  00:00-23:59 GMT:  USE KEY NV
    07 JUL 2019  00:00-23:59 GMT:  USE KEY NW
    08 JUL 2019  00:00-23:59 GMT:  USE KEY NX
    09 JUL 2019  00:00-23:59 GMT:  USE KEY NY
    10 JUL 2019  00:00-23:59 GMT:  USE KEY NZ
    11 JUL 2019  00:00-23:59 GMT:  USE KEY OA
    12 JUL 2019  00:00-23:59 GMT:  USE KEY OB
    13 JUL 2019  00:00-23:59 GMT:  USE KEY OC
    14 JUL 2019  00:00-23:59 GMT:  USE KEY OD
    15 JUL 2019  00:00-23:59 GMT:  USE KEY OE
    16 JUL 2019  00:00-23:59 GMT:  USE KEY OF
    17 JUL 2019  00:00-23:59 GMT:  USE KEY OG
    18 JUL 2019  00:00-23:59 GMT:  USE KEY OH
    19 JUL 2019  00:00-23:59 GMT:  USE KEY OI
    20 JUL 2019  00:00-23:59 GMT:  USE KEY OJ
    21 JUL 2019  00:00-23:59 GMT:  USE KEY OK
    22 JUL 2019  00:00-23:59 GMT:  USE KEY OL
    23 JUL 2019  00:00-23:59 GMT:  USE KEY OM
    24 JUL 2019  00:00-23:59 GMT:  USE KEY ON
    25 JUL 2019  00:00-23:59 GMT:  USE KEY OO
    26 JUL 2019  00:00-23:59 GMT:  USE KEY OP
    27 JUL 2019  00:00-23:59 GMT:  USE KEY OQ
    28 JUL 2019  00:00-23:59 GMT:  USE KEY OR
    29 JUL 2019  00:00-23:59 GMT:  USE KEY OS
    30 JUL 2019  00:00-23:59 GMT:  USE KEY OT
    31 JUL 2019  00:00-23:59 GMT:  USE KEY OU

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  B  B  B  B  -
03 1-0   C  -  C  -  C  -
04 1-0   D  -  -  D  D  -
05 0-4   -  -  -  E  E  -
06 0-4   -  -  F  -  F  F
07 0-5   -  -  -  -  G  G
08 0-5   H  -  H  -  -  H
09 0-5   I  I  I  I  -  I
10 0-5   -  -  -  J  J  -
11 0-5   -  -  -  -  -  K
12 0-6   -  L  -  -  L  L
13 0-6   M  M  M  -  M  -
14 0-6   N  -  -  N  -  N
15 0-6   -  -  O  -  O  O
16 1-5   -  P  P  -  -  -
17 1-6   Q  Q  Q  -  -  Q
18 1-6   R  R  R  R  -   
19 1-6   -  -  -  S  -   
20 1-6   -  -  -  T      
21 2-3   U  U  -  U      
22 3-4   -  -  V         
23 3-6   W  -  -         
24 3-6   X  -            
25 4-5   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

SAMUN OQORT OSWZW NZWVP SLORM D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 2-0   B  -  B  -  -  B
03 0-3   C  C  C  C  -  -
04 0-3   D  D  D  -  D  D
05 0-3   E  E  -  E  -  -
06 0-3   F  -  F  F  F  F
07 0-3   -  -  G  G  -  -
08 0-3   H  H  -  -  -  -
09 0-3   -  -  -  I  -  I
10 0-4   J  J  -  -  -  -
11 0-4   -  -  -  -  K  -
12 0-4   -  -  -  -  -  -
13 0-4   -  M  M  M  -  M
14 0-4   -  N  N  -  N  N
15 0-5   O  -  -  O  O  -
16 0-5   -  -  P  P  P  P
17 0-5   -  Q  Q  Q  Q  -
18 0-5   R  -  R  R  -   
19 0-6   S  -  -  S  -   
20 0-6   T  -  T  T      
21 0-6   -  U  U  -      
22 1-6   -  -  -         
23 3-5   -  X  -         
24 3-5   -  -            
25 3-5   Y  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QZAZW RRUNB PUPXJ WLGKE GNTVS V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  A
02 2-0   -  B  B  -  -  B
03 2-0   -  C  -  C  -  C
04 2-0   D  -  D  -  -  -
05 2-0   E  E  E  -  E  -
06 0-3   F  -  F  -  -  -
07 0-3   -  -  G  -  -  -
08 0-5   -  -  -  -  H  -
09 0-5   I  -  I  I  I  I
10 0-5   -  J  J  J  -  J
11 0-5   K  -  -  -  -  -
12 0-6   -  -  -  -  -  -
13 0-6   M  -  -  M  M  -
14 0-6   N  -  N  -  -  -
15 0-6   -  -  O  -  -  -
16 0-6   P  P  P  P  P  P
17 0-6   -  Q  -  Q  Q  Q
18 1-5   -  R  -  R  R   
19 2-5   -  S  -  -  S   
20 2-5   -  T  -  T      
21 2-5   U  -  U  U      
22 2-5   V  V  V         
23 2-6   -  X  -         
24 3-4   X  Y            
25 3-5   Y  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

GUPKR YSAWR ZPQPG AVAFG QBHVV V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  B  -  -  -  -
03 1-0   -  C  C  C  C  -
04 1-0   D  -  -  D  D  -
05 2-0   E  -  -  E  E  E
06 2-0   -  -  F  F  F  F
07 2-0   -  G  -  -  -  -
08 0-4   -  -  -  H  -  -
09 0-5   -  -  -  I  I  I
10 0-5   J  -  J  J  -  J
11 0-5   K  K  K  K  K  K
12 0-5   -  L  L  L  -  L
13 0-5   -  M  -  -  -  -
14 0-6   -  N  N  N  -  -
15 0-6   O  O  O  -  -  -
16 0-6   P  -  -  -  -  P
17 0-6   Q  -  Q  Q  Q  -
18 0-6   R  R  -  R  -   
19 0-6   S  -  -  -  S   
20 1-4   -  T  T  -      
21 1-5   U  -  -  -      
22 2-5   -  -  V         
23 2-6   -  -  -         
24 2-6   -  -            
25 2-6   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

URFOC AAGAF OTAQH QTQTC HOQIO P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  -  -  -  -
05 1-0   E  -  -  E  -  E
06 1-0   -  -  -  F  -  F
07 1-0   -  G  -  -  -  -
08 1-0   -  -  -  -  H  H
09 2-0   -  I  I  I  I  I
10 0-3   J  J  J  J  -  J
11 0-3   -  K  K  K  K  -
12 0-3   L  -  -  L  L  -
13 0-3   -  -  -  -  M  -
14 0-3   N  -  N  N  -  -
15 0-3   -  -  O  -  O  O
16 0-3   -  P  -  P  -  -
17 0-3   -  -  Q  -  -  -
18 0-3   -  -  R  -  R   
19 0-3   -  S  S  -  -   
20 0-4   -  -  T  -      
21 0-5   U  U  -  -      
22 0-5   V  V  V         
23 0-6   W  X  -         
24 1-3   -  -            
25 1-5   Y  Z            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ORZPO TYNMM WNOMO OFCPR ZDNKX D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 2-0   B  B  B  B  -  B
03 2-0   -  C  -  -  -  C
04 0-4   D  -  D  -  D  -
05 0-4   E  -  E  E  -  -
06 0-4   -  F  F  F  F  F
07 0-4   G  G  -  -  -  -
08 0-4   H  H  H  H  -  H
09 0-4   -  I  I  -  I  -
10 0-4   J  -  J  J  J  J
11 0-4   -  K  -  K  -  K
12 0-4   L  -  -  L  -  L
13 0-5   -  -  -  -  M  -
14 0-5   -  N  N  -  N  -
15 0-5   -  -  O  O  -  O
16 0-5   P  -  -  P  -  -
17 0-6   -  -  -  Q  -  -
18 0-6   R  -  -  -  R   
19 0-6   S  S  S  -  S   
20 0-6   -  -  -  -      
21 0-6   U  -  -  -      
22 1-2   V  V  -         
23 1-3   -  -  -         
24 2-5   -  -            
25 4-6   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KNTNU CIPMQ LINUT CYRWP TPCLV V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 2-0   B  -  -  B  -  -
03 2-0   C  -  C  -  C  -
04 2-0   -  D  -  D  D  D
05 0-3   E  -  -  -  -  E
06 0-3   -  F  -  -  -  -
07 0-3   -  -  G  -  -  G
08 0-3   -  -  H  -  H  H
09 0-3   -  I  I  I  I  -
10 0-4   J  -  -  J  -  -
11 0-5   -  K  K  K  -  -
12 0-5   L  L  L  -  -  L
13 0-6   -  M  M  -  M  M
14 0-6   -  -  N  N  -  N
15 0-6   -  O  -  -  O  O
16 0-6   P  P  P  P  P  -
17 0-6   Q  -  -  Q  Q  Q
18 0-6   -  R  R  -  -   
19 0-6   S  -  S  -  S   
20 0-6   -  -  -  T      
21 0-6   U  -  -  U      
22 0-6   -  V  V         
23 0-6   W  -  -         
24 2-3   -  Y            
25 2-5   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

LVRVW LLJLE XTJMQ ENITS WVPYE M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  -  B  -  B  B
03 1-0   -  -  C  C  C  -
04 1-0   -  -  D  D  D  -
05 1-0   E  -  E  E  -  -
06 1-0   -  F  -  -  -  -
07 0-3   G  -  G  G  -  -
08 0-3   H  H  H  H  H  -
09 0-3   I  -  I  I  I  I
10 0-3   -  J  J  J  J  -
11 0-3   -  -  -  -  K  -
12 0-3   -  L  -  -  -  L
13 0-4   M  -  M  -  -  -
14 0-4   -  -  N  -  -  N
15 0-4   O  O  O  O  O  O
16 0-4   P  P  -  P  -  P
17 0-6   -  Q  -  Q  Q  -
18 1-2   -  R  -  -  R   
19 1-4   -  -  -  -  -   
20 1-4   T  -  T  T      
21 1-4   U  U  -  U      
22 1-4   V  -  V         
23 1-6   -  -  -         
24 2-5   X  -            
25 2-6   -  Z            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

VMLOT PVMZA NZXPH USYAT FDMXT U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   -  -  -  -  -  B
03 1-0   -  C  C  C  C  C
04 1-0   -  D  D  -  -  -
05 1-0   E  -  E  E  -  E
06 1-0   F  F  -  -  -  -
07 0-3   -  G  G  -  G  G
08 0-3   -  -  -  H  H  -
09 0-3   I  -  -  I  I  -
10 0-3   -  -  -  -  -  J
11 0-3   -  K  K  -  K  K
12 0-5   L  -  L  -  L  -
13 0-5   -  M  -  M  -  -
14 0-5   N  N  N  -  N  N
15 0-5   O  -  O  -  O  -
16 0-6   P  -  P  P  P  -
17 0-6   Q  -  -  -  -  -
18 1-2   R  -  -  -  -   
19 1-3   -  S  S  S  S   
20 1-3   T  T  T  T      
21 1-3   -  -  -  U      
22 1-3   V  -  V         
23 1-4   -  -  -         
24 1-6   X  -            
25 3-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ASXKS KTHWQ PGJQP SAGFS ORSPT W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 1-0   B  B  B  -  -  -
03 1-0   C  C  -  -  -  C
04 1-0   D  D  -  D  D  -
05 1-0   E  -  E  E  -  E
06 1-0   -  F  -  F  -  F
07 0-3   G  G  -  -  G  G
08 0-3   H  H  -  -  H  -
09 0-3   I  -  I  -  I  -
10 0-3   -  -  J  -  J  J
11 0-3   K  K  -  K  K  -
12 0-3   -  L  L  L  -  L
13 0-6   -  M  -  M  M  M
14 0-6   N  -  -  N  N  -
15 0-6   -  -  -  -  -  O
16 0-6   -  -  P  P  P  -
17 0-6   Q  -  -  -  Q  Q
18 1-3   -  R  R  R  R   
19 1-6   S  -  S  -  -   
20 1-6   T  T  T  -      
21 1-6   -  -  -  -      
22 1-6   V  -  -         
23 2-3   -  X  X         
24 2-4   X  -            
25 3-6   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ATTMZ LZZMT KUUUV LNNAA BABNM K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  B  -  -
03 1-0   C  C  -  C  C  -
04 1-0   D  -  -  D  D  -
05 1-0   -  E  E  -  -  E
06 0-3   -  -  -  -  F  -
07 0-3   -  -  -  G  G  -
08 0-3   -  H  -  H  -  H
09 0-3   -  -  -  -  I  I
10 0-3   J  -  J  J  -  J
11 0-4   -  -  K  K  -  K
12 0-4   -  -  L  -  L  -
13 0-4   M  M  M  M  M  -
14 0-4   N  -  N  N  -  N
15 0-6   O  O  -  -  O  -
16 0-6   -  P  P  P  -  P
17 0-6   -  Q  Q  -  -  Q
18 1-3   R  -  -  -  R   
19 1-3   S  -  -  -  -   
20 1-3   -  T  -  T      
21 1-3   -  -  -  -      
22 1-4   V  V  V         
23 2-5   -  -  X         
24 3-5   X  Y            
25 3-5   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VJRAZ FRGSW XVGNW KMEGS WQVWO S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  A  -  -
02 0-3   -  -  -  B  -  -
03 0-3   C  C  C  -  C  -
04 0-3   -  D  D  D  D  D
05 0-4   E  E  -  E  -  -
06 0-4   F  -  -  F  F  F
07 0-4   -  G  G  -  -  G
08 0-4   -  H  -  -  H  -
09 0-4   -  -  -  I  -  -
10 0-5   J  J  -  -  -  -
11 0-5   -  -  -  -  K  -
12 0-5   -  -  L  L  -  L
13 0-5   M  -  M  M  -  M
14 0-5   N  -  N  N  N  -
15 0-5   O  O  -  O  O  -
16 1-3   P  -  P  -  -  P
17 2-3   Q  Q  Q  -  -  Q
18 2-6   R  R  -  R  R   
19 3-4   -  S  S  -  -   
20 3-4   -  -  -  -      
21 3-4   U  -  -  -      
22 3-4   -  V  -         
23 3-5   -  -  -         
24 3-5   X  -            
25 3-6   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TNAVB ULBAJ AMUAL MMLMC MZAUA V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  B  -  -  -  -
03 1-0   C  -  -  -  C  C
04 1-0   -  D  D  D  D  D
05 0-3   -  -  -  E  -  -
06 0-3   -  F  F  -  F  F
07 0-3   -  -  -  -  G  G
08 0-3   H  H  -  -  H  -
09 0-3   -  I  -  -  -  I
10 0-3   J  J  J  J  -  -
11 0-3   K  K  K  -  -  K
12 0-3   -  L  -  L  L  -
13 0-3   M  -  M  M  M  M
14 0-3   N  N  N  N  -  -
15 0-3   -  -  O  O  O  O
16 0-4   -  P  P  -  -  P
17 0-4   -  -  -  -  Q  Q
18 0-5   R  -  -  R  -   
19 0-5   -  -  -  -  S   
20 0-5   -  -  -  T      
21 0-6   U  -  U  U      
22 1-4   V  V  V         
23 1-5   -  X  -         
24 2-6   -  Y            
25 3-5   -  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WVXFC PIVVC NKFAG TACJT ZKWXW M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   -  B  B  B  -  -
03 1-0   C  -  -  -  -  C
04 1-0   -  D  -  -  D  D
05 2-0   -  E  E  E  E  E
06 0-3   -  -  F  F  -  -
07 0-3   G  -  -  -  G  -
08 0-3   H  H  H  H  H  H
09 0-3   I  I  I  -  -  -
10 0-3   J  J  -  J  J  J
11 0-4   -  -  -  K  K  -
12 0-4   L  L  -  -  -  -
13 0-4   -  -  M  M  -  M
14 0-4   N  N  N  -  -  -
15 0-4   -  -  O  O  O  O
16 0-4   -  -  -  P  -  P
17 0-4   Q  -  -  -  Q  Q
18 0-4   -  -  R  R  R   
19 0-5   -  -  S  -  S   
20 0-5   -  -  T  T      
21 0-6   U  U  -  U      
22 1-3   V  V  -         
23 1-4   W  -  -         
24 1-4   -  Y            
25 1-4   -  Z            
26 2-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

RNLOH ZSUKC UREPW KLQZS JLSSZ D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  -
02 2-0   -  -  B  B  B  B
03 0-3   C  -  C  -  -  C
04 0-3   -  D  -  -  -  D
05 0-3   E  -  -  -  E  E
06 0-3   F  -  -  F  F  F
07 0-3   -  G  G  -  G  -
08 0-3   H  H  -  H  -  H
09 0-4   -  -  I  I  I  -
10 0-4   J  -  J  J  J  -
11 0-4   K  K  -  -  K  -
12 0-4   L  L  -  L  -  L
13 0-4   M  -  -  M  -  -
14 0-4   -  -  N  N  -  N
15 0-4   O  O  O  -  O  -
16 0-4   -  P  -  -  P  P
17 0-4   -  Q  -  -  Q  -
18 0-4   R  R  R  -  -   
19 0-4   -  -  -  S  -   
20 0-4   T  -  -  -      
21 0-4   -  U  U  U      
22 0-5   V  V  V         
23 0-6   -  X  X         
24 0-6   X  Y            
25 0-6   -  -            
26 2-5   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

VYDGI FQIAM XMLCT AVQAF RQUIH G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 2-0   -  -  B  -  B  -
03 2-0   -  C  -  -  -  C
04 2-0   D  D  -  D  D  D
05 2-0   E  -  E  E  -  E
06 2-0   F  F  F  F  -  -
07 2-0   G  -  G  -  G  G
08 0-5   -  -  H  H  -  -
09 0-5   -  I  -  I  -  I
10 0-5   -  J  J  -  -  J
11 0-5   -  -  K  -  -  -
12 0-6   -  L  L  L  L  -
13 0-6   M  M  -  -  M  -
14 0-6   N  N  -  -  N  N
15 0-6   -  -  O  -  O  -
16 1-2   -  -  P  P  -  P
17 1-4   Q  Q  -  Q  Q  -
18 1-6   R  R  R  R  R   
19 1-6   S  -  S  S  S   
20 1-6   T  T  -  T      
21 2-5   -  -  -  -      
22 3-4   V  V  -         
23 4-6   -  -  X         
24 5-6   -  Y            
25 5-6   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

IPVAH ZQSIA TYCVW PWHZQ ZJXMW L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  B  B  -  B
03 1-0   -  C  C  -  -  -
04 1-0   -  D  -  -  D  -
05 1-0   E  E  -  E  -  E
06 1-0   F  -  F  F  F  -
07 1-0   -  -  -  -  G  G
08 1-0   H  H  -  H  -  -
09 1-0   -  -  -  I  I  -
10 1-0   -  J  -  J  J  -
11 1-0   K  K  K  -  K  K
12 1-0   L  L  -  -  -  L
13 0-3   M  -  -  M  M  -
14 0-3   N  N  N  N  -  -
15 0-3   -  O  O  O  -  -
16 0-3   -  -  P  -  -  P
17 0-3   Q  -  -  -  Q  -
18 0-3   -  R  R  R  R   
19 0-4   S  -  -  -  S   
20 0-6   T  T  T  -      
21 0-6   -  -  -  U      
22 0-6   V  -  V         
23 0-6   -  X  -         
24 1-3   -  -            
25 2-5   Y  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OAINT HVGOM BZIHC UPHUZ JOUUZ N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  -
02 0-3   B  -  B  -  -  B
03 0-3   -  C  -  -  C  C
04 0-3   -  D  -  -  -  D
05 0-3   E  E  E  -  E  -
06 0-4   F  -  F  F  -  F
07 0-4   -  -  -  G  -  G
08 0-4   -  H  H  H  H  -
09 0-4   -  I  -  -  -  -
10 0-4   -  -  J  J  J  -
11 0-6   -  K  -  K  -  -
12 0-6   L  -  L  -  -  L
13 0-6   -  M  -  -  -  M
14 0-6   N  N  N  -  -  -
15 0-6   -  O  -  -  O  -
16 1-5   P  -  -  -  -  P
17 2-3   -  Q  -  -  Q  -
18 2-5   R  -  R  R  R   
19 2-6   S  S  S  S  S   
20 2-6   -  T  T  T      
21 2-6   U  -  U  U      
22 3-4   -  -  -         
23 4-6   W  -  -         
24 4-6   -  Y            
25 4-6   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

LMQCK ARRUW VKOPT VKIVZ ZKOKH Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  -  -  B  B  B
03 1-0   -  C  C  -  C  -
04 1-0   D  -  D  D  -  D
05 1-0   -  E  E  E  E  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  -  -  -  -
08 0-3   H  -  -  -  H  -
09 0-3   -  -  -  I  -  -
10 0-3   J  -  -  -  -  J
11 0-3   K  K  -  K  -  -
12 0-4   -  L  L  -  -  L
13 0-5   -  -  -  M  -  M
14 0-5   -  N  -  N  N  N
15 0-5   O  -  -  -  O  -
16 1-3   -  -  P  -  P  P
17 1-5   -  Q  Q  Q  Q  -
18 1-5   R  R  R  R  -   
19 1-5   S  S  S  S  S   
20 1-5   -  -  T  T      
21 2-4   -  U  U  -      
22 2-5   V  -  -         
23 2-6   W  -  -         
24 3-4   -  Y            
25 4-5   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

FGZZV VXALU OCVOQ MCRKG KAOKM W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  B  B  -  -  -
03 1-0   C  -  C  -  -  C
04 1-0   -  -  -  D  D  -
05 2-0   E  E  -  E  -  E
06 2-0   F  F  -  -  -  -
07 2-0   -  -  -  -  G  -
08 0-4   -  H  -  -  -  -
09 0-4   I  I  I  -  -  -
10 0-4   J  -  -  J  J  -
11 0-4   -  K  -  -  K  K
12 0-4   -  L  -  L  L  L
13 0-4   M  M  M  -  -  -
14 0-4   N  N  -  -  N  -
15 0-6   O  -  -  O  -  O
16 0-6   -  -  P  P  -  P
17 0-6   Q  Q  -  Q  -  Q
18 1-2   R  -  R  R  -   
19 1-6   S  -  S  -  S   
20 2-3   T  -  T  T      
21 3-4   -  -  -  U      
22 3-4   -  V  -         
23 3-5   -  -  X         
24 4-6   X  -            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

IMXSX SLWTL DTSSA JJIWK SNPZZ U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  B  B  B  B  B
03 1-0   -  -  -  -  -  C
04 1-0   D  -  D  -  D  -
05 0-3   -  -  -  E  E  -
06 0-4   F  -  -  F  F  -
07 0-4   -  -  -  G  -  G
08 0-4   H  H  -  H  -  H
09 0-4   -  -  -  -  -  -
10 0-4   -  J  J  -  J  J
11 0-4   -  -  -  -  -  -
12 0-5   -  -  -  L  -  L
13 0-5   M  -  M  M  M  M
14 0-5   N  -  -  -  N  -
15 0-5   -  O  -  -  O  O
16 0-5   P  -  P  -  P  P
17 0-5   Q  Q  -  Q  -  -
18 1-4   -  R  R  R  R   
19 1-5   -  S  S  -  -   
20 1-5   -  -  -  -      
21 1-5   -  -  U  U      
22 1-5   V  V  V         
23 2-6   W  X  X         
24 3-4   X  -            
25 3-5   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QSPMW MQOUP LPRWS HSLLF UVPQL W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  B  B  B  -
03 1-0   C  C  C  C  C  -
04 1-0   -  D  -  -  D  -
05 1-0   E  -  E  E  E  E
06 1-0   F  -  -  F  -  -
07 1-0   -  -  -  G  -  G
08 1-0   H  H  -  H  H  -
09 1-0   -  I  I  I  -  I
10 1-0   J  J  J  -  -  J
11 0-4   K  -  -  K  K  K
12 0-4   -  L  -  -  -  -
13 0-4   -  -  -  M  -  -
14 0-4   -  N  N  -  N  N
15 0-5   O  O  O  -  -  O
16 0-5   -  -  -  -  P  -
17 0-5   Q  -  -  Q  -  -
18 0-5   -  -  R  R  -   
19 0-5   S  -  S  -  S   
20 0-5   T  T  -  -      
21 0-6   U  U  U  -      
22 1-5   -  -  V         
23 1-5   W  -  -         
24 1-5   X  -            
25 2-3   -  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HNJNA TBVNJ PQVUG ZTGVG PZBKC T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   B  B  B  B  B  B
03 1-0   -  C  -  C  C  C
04 1-0   -  D  D  -  -  -
05 1-0   E  -  E  E  -  E
06 1-0   -  F  F  F  F  -
07 1-0   -  G  G  -  G  -
08 2-0   H  H  H  -  H  -
09 2-0   I  -  -  -  I  -
10 2-0   J  -  J  J  -  -
11 2-0   K  K  -  K  K  -
12 2-0   -  L  -  -  -  L
13 2-0   M  M  -  M  M  -
14 2-0   N  N  -  N  -  -
15 2-0   -  -  O  O  -  O
16 2-0   -  -  -  P  P  P
17 2-0   Q  Q  -  -  -  Q
18 0-3   -  R  R  -  R   
19 0-4   S  -  S  S  -   
20 0-4   -  -  T  -      
21 0-4   -  -  U  U      
22 0-4   -  V  V         
23 0-4   -  -  -         
24 0-6   X  -            
25 0-6   Y  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FQPWE EJKQK NTQRR ILKZN HZSZX D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ON
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  -  A  A
02 2-0   B  -  B  -  -  B
03 2-0   C  C  C  -  -  C
04 2-0   -  -  -  -  D  D
05 2-0   E  E  -  E  -  -
06 2-0   -  F  -  -  F  -
07 0-5   G  G  G  -  G  -
08 0-5   H  -  -  H  H  -
09 0-5   I  -  -  I  -  I
10 0-5   J  -  -  J  -  -
11 0-5   -  K  -  K  K  -
12 0-6   -  L  L  L  -  -
13 0-6   M  -  M  -  M  M
14 0-6   N  N  -  -  N  N
15 0-6   O  O  O  -  -  -
16 1-4   -  P  -  -  -  P
17 1-6   Q  -  -  Q  Q  Q
18 2-5   -  R  R  R  -   
19 2-6   -  -  S  S  S   
20 2-6   T  -  T  T      
21 2-6   U  -  U  -      
22 2-6   -  V  V         
23 3-6   W  -  -         
24 4-5   X  Y            
25 4-6   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KUTTF UOAUT TICVH APPLO USTNA V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  A
02 2-0   -  B  B  B  -  B
03 2-0   C  -  C  -  -  -
04 2-0   D  -  -  -  -  D
05 2-0   -  E  -  -  E  -
06 0-4   F  F  -  -  F  F
07 0-4   G  -  G  G  -  -
08 0-4   -  -  -  -  H  H
09 0-4   -  I  -  -  -  -
10 0-4   -  J  J  J  J  J
11 0-4   -  -  -  K  -  K
12 0-4   L  L  L  -  L  -
13 0-5   M  -  -  M  -  -
14 0-6   -  -  -  -  N  -
15 0-6   O  O  O  -  -  O
16 0-6   -  -  P  P  -  -
17 0-6   -  -  Q  Q  -  -
18 0-6   R  R  -  -  R   
19 0-6   -  -  S  S  S   
20 1-4   -  -  -  -      
21 2-3   U  U  U  U      
22 2-6   V  -  V         
23 3-5   -  X  X         
24 4-6   X  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ABDTS CUNAN ZBSCL USNLU GVHAU A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  A
02 2-0   B  B  B  -  -  -
03 2-0   C  -  -  C  -  C
04 2-0   D  -  D  -  -  D
05 0-3   E  -  -  E  E  -
06 0-3   -  F  -  F  -  -
07 0-3   G  G  -  G  G  G
08 0-6   H  -  H  H  -  H
09 0-6   -  I  I  -  I  -
10 0-6   -  J  -  J  -  -
11 0-6   K  -  K  -  K  -
12 0-6   -  -  L  -  L  -
13 0-6   M  -  -  -  M  M
14 0-6   N  N  -  -  N  -
15 0-6   -  O  O  O  -  O
16 1-2   -  P  P  -  -  P
17 1-4   -  -  Q  -  Q  Q
18 2-3   R  -  R  -  -   
19 2-4   -  S  -  S  S   
20 2-4   -  T  -  T      
21 2-5   -  -  -  U      
22 2-6   -  V  V         
23 2-6   -  -  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

MRWFZ VQXJR MJNVS EOSKV RUMRA R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  A
02 2-0   B  -  B  B  B  -
03 2-0   C  -  C  -  C  -
04 2-0   -  -  -  -  D  -
05 2-0   -  E  E  -  -  E
06 2-0   F  -  F  F  -  -
07 2-0   G  -  -  -  -  G
08 2-0   -  -  -  -  H  H
09 2-0   I  I  I  I  I  I
10 0-3   J  J  J  J  J  -
11 0-4   K  -  K  -  -  -
12 0-4   -  L  -  L  -  -
13 0-4   M  M  M  M  -  M
14 0-4   N  N  N  -  N  N
15 0-4   -  -  O  -  -  O
16 0-5   P  P  -  -  -  -
17 0-5   Q  Q  -  Q  -  -
18 0-6   -  -  -  R  R   
19 0-6   S  S  -  S  S   
20 0-6   -  T  -  -      
21 0-6   U  U  U  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 1-5   -  Y            
25 2-6   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ILUIW RTVRT KVBIO VDJHS YQKLS J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   B  B  B  B  -  B
03 2-0   C  C  C  C  -  C
04 0-3   D  -  -  -  D  -
05 0-3   E  -  -  -  -  E
06 0-3   -  F  -  -  -  -
07 0-3   G  -  G  G  -  G
08 0-3   H  -  -  H  -  H
09 0-3   -  -  -  I  I  -
10 0-3   -  -  -  J  -  J
11 0-3   K  -  -  K  -  K
12 0-3   L  L  -  -  L  -
13 0-4   M  -  M  -  M  M
14 0-5   -  -  N  -  -  -
15 0-5   O  O  O  -  O  O
16 0-5   P  P  P  P  -  P
17 0-5   -  Q  Q  -  -  -
18 0-5   -  -  -  R  R   
19 0-5   S  -  -  -  S   
20 0-5   -  T  T  T      
21 0-6   -  U  -  -      
22 1-5   V  -  V         
23 1-6   -  -  -         
24 2-6   -  -            
25 3-5   -  Z            
26 3-5   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

ZVPXC EYLWC OXRGF EYGEO KFZPM Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   B  B  -  -  -  B
03 1-0   C  C  -  -  C  C
04 1-0   D  -  -  -  D  -
05 1-0   E  E  E  -  -  -
06 1-0   -  -  F  -  F  F
07 0-4   -  G  -  G  -  -
08 0-4   -  -  H  -  -  -
09 0-4   I  -  I  -  I  I
10 0-4   -  -  J  J  J  -
11 0-4   -  -  -  -  -  K
12 0-4   L  L  -  L  -  -
13 0-4   -  -  M  M  M  M
14 0-5   -  -  N  N  N  -
15 0-5   -  -  -  O  -  -
16 0-5   -  -  P  P  -  -
17 0-5   Q  Q  -  -  -  -
18 0-5   -  R  -  R  -   
19 0-5   S  -  S  S  S   
20 1-4   -  T  -  -      
21 1-4   -  -  -  U      
22 1-4   V  V  -         
23 1-4   -  X  X         
24 1-5   X  Y            
25 2-4   -  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

NZUSZ MSJUC SHNSL UHJMM TTTMN B
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  -  -  B  -  B
03 1-0   C  C  -  -  C  -
04 2-0   -  D  D  D  -  D
05 2-0   -  -  E  -  E  -
06 2-0   F  -  F  F  -  F
07 2-0   -  -  -  -  G  G
08 2-0   -  H  H  H  -  H
09 2-0   -  I  -  I  -  -
10 2-0   J  -  -  J  J  J
11 0-4   K  K  K  K  -  K
12 0-5   -  -  L  L  L  -
13 0-5   -  M  M  -  -  -
14 0-5   N  -  N  -  N  -
15 0-5   -  O  O  -  O  O
16 0-5   P  P  P  -  P  P
17 0-5   Q  -  -  -  -  -
18 0-5   R  R  -  R  -   
19 0-6   S  -  -  -  -   
20 1-4   -  T  T  -      
21 1-5   U  U  -  -      
22 2-3   V  -  V         
23 2-5   -  -  -         
24 2-5   X  -            
25 2-5   Y  -            
26 2-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZQQXN TQZHE PZSSX AHQOW HZOTF N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  B  -  -  B  B
03 1-0   -  -  C  -  C  C
04 1-0   D  D  D  D  -  D
05 1-0   E  -  E  E  -  E
06 1-0   F  -  -  -  -  -
07 0-3   G  -  -  G  G  -
08 0-3   H  -  -  H  -  H
09 0-3   -  I  I  -  -  -
10 0-3   J  -  J  -  -  -
11 0-3   -  -  K  -  K  -
12 0-3   -  L  -  L  L  -
13 0-3   -  M  M  -  -  M
14 0-3   -  N  N  -  N  -
15 0-4   -  O  O  O  O  -
16 0-4   P  P  -  P  -  -
17 0-4   -  -  Q  -  -  Q
18 0-4   R  -  R  R  -   
19 0-5   S  -  S  -  S   
20 0-6   T  T  -  -      
21 0-6   U  U  -  U      
22 1-3   -  -  V         
23 1-3   -  X  -         
24 1-3   -  -            
25 1-4   Y  -            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TLSXR XDETO EDRQW OOJSP WAMTT D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
