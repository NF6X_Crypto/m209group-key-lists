EFFECTIVE PERIOD:
04-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  B  -  -  -  -
03 1-0   -  C  C  C  C  -
04 1-0   D  -  -  D  D  -
05 2-0   E  -  -  E  E  E
06 2-0   -  -  F  F  F  F
07 2-0   -  G  -  -  -  -
08 0-4   -  -  -  H  -  -
09 0-5   -  -  -  I  I  I
10 0-5   J  -  J  J  -  J
11 0-5   K  K  K  K  K  K
12 0-5   -  L  L  L  -  L
13 0-5   -  M  -  -  -  -
14 0-6   -  N  N  N  -  -
15 0-6   O  O  O  -  -  -
16 0-6   P  -  -  -  -  P
17 0-6   Q  -  Q  Q  Q  -
18 0-6   R  R  -  R  -   
19 0-6   S  -  -  -  S   
20 1-4   -  T  T  -      
21 1-5   U  -  -  -      
22 2-5   -  -  V         
23 2-6   -  -  -         
24 2-6   -  -            
25 2-6   Y  Z            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

URFOC AAGAF OTAQH QTQTC HOQIO P
-------------------------------
