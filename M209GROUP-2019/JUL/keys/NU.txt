EFFECTIVE PERIOD:
05-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  A
02 1-0   -  B  B  B  B  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  -  -  -  -
05 1-0   E  -  -  E  -  E
06 1-0   -  -  -  F  -  F
07 1-0   -  G  -  -  -  -
08 1-0   -  -  -  -  H  H
09 2-0   -  I  I  I  I  I
10 0-3   J  J  J  J  -  J
11 0-3   -  K  K  K  K  -
12 0-3   L  -  -  L  L  -
13 0-3   -  -  -  -  M  -
14 0-3   N  -  N  N  -  -
15 0-3   -  -  O  -  O  O
16 0-3   -  P  -  P  -  -
17 0-3   -  -  Q  -  -  -
18 0-3   -  -  R  -  R   
19 0-3   -  S  S  -  -   
20 0-4   -  -  T  -      
21 0-5   U  U  -  -      
22 0-5   V  V  V         
23 0-6   W  X  -         
24 1-3   -  -            
25 1-5   Y  Z            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ORZPO TYNMM WNOMO OFCPR ZDNKX D
-------------------------------
