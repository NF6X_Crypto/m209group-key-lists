EFFECTIVE PERIOD:
22-JUL-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  A
02 1-0   B  B  B  B  B  -
03 1-0   C  C  C  C  C  -
04 1-0   -  D  -  -  D  -
05 1-0   E  -  E  E  E  E
06 1-0   F  -  -  F  -  -
07 1-0   -  -  -  G  -  G
08 1-0   H  H  -  H  H  -
09 1-0   -  I  I  I  -  I
10 1-0   J  J  J  -  -  J
11 0-4   K  -  -  K  K  K
12 0-4   -  L  -  -  -  -
13 0-4   -  -  -  M  -  -
14 0-4   -  N  N  -  N  N
15 0-5   O  O  O  -  -  O
16 0-5   -  -  -  -  P  -
17 0-5   Q  -  -  Q  -  -
18 0-5   -  -  R  R  -   
19 0-5   S  -  S  -  S   
20 0-5   T  T  -  -      
21 0-6   U  U  U  -      
22 1-5   -  -  V         
23 1-5   W  -  -         
24 1-5   X  -            
25 2-3   -  Z            
26 2-4   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HNJNA TBVNJ PQVUG ZTGVG PZBKC T
-------------------------------
