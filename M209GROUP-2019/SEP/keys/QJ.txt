EFFECTIVE PERIOD:
10-SEP-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: QJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 2-0   B  -  B  -  -  B
03 0-3   -  -  C  C  -  C
04 0-3   -  D  D  -  -  -
05 0-3   E  -  E  E  E  E
06 0-3   F  -  -  -  F  -
07 0-3   -  G  -  -  G  -
08 0-3   -  H  -  H  H  H
09 0-3   -  I  I  -  I  I
10 0-3   -  J  -  J  J  -
11 0-3   K  -  -  -  -  -
12 0-3   -  L  L  L  -  -
13 0-4   -  -  M  M  -  -
14 0-4   -  -  N  -  -  N
15 0-4   -  O  O  -  -  -
16 0-4   -  P  -  P  P  P
17 0-4   Q  Q  Q  Q  Q  -
18 0-4   R  -  -  R  R   
19 0-5   S  -  S  S  -   
20 0-6   -  -  -  -      
21 0-6   U  U  U  U      
22 1-5   V  -  V         
23 1-6   W  -  -         
24 3-4   X  Y            
25 3-4   -  -            
26 3-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UQDYL ZOPZN ZZQVT GNCUA NWROV P
-------------------------------
