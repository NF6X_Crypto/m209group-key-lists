EFFECTIVE PERIOD:
13-AUG-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  -  A  -
02 2-0   -  -  B  -  -  B
03 2-0   -  -  -  C  -  -
04 2-0   -  D  -  -  -  -
05 2-0   E  E  -  E  -  E
06 2-0   -  F  -  -  F  -
07 2-0   -  G  -  -  G  -
08 2-0   H  H  H  H  -  H
09 2-0   I  -  -  I  I  I
10 2-0   J  J  J  -  -  J
11 0-3   -  K  K  -  K  K
12 0-4   -  -  L  -  L  L
13 0-4   M  -  -  -  -  -
14 0-5   N  -  -  N  -  N
15 0-5   O  -  O  O  O  O
16 0-5   -  P  P  -  -  P
17 0-5   -  -  -  Q  Q  -
18 0-5   -  R  -  -  R   
19 0-5   S  -  -  -  S   
20 0-5   T  -  T  T      
21 0-6   U  -  U  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-4   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

MTEXI NIVAK AJZKW QZTXI RQPDI Q
-------------------------------
