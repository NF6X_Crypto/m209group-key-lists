EFFECTIVE PERIOD:
14-AUG-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  -
02 1-0   B  B  B  -  -  B
03 1-0   C  C  C  -  C  C
04 1-0   -  D  D  D  -  -
05 1-0   -  -  E  -  E  E
06 1-0   F  -  F  -  F  -
07 1-0   G  -  -  G  -  G
08 1-0   -  H  -  H  -  H
09 1-0   -  -  -  I  I  I
10 0-3   -  J  -  -  J  J
11 0-4   K  -  K  K  K  -
12 0-4   -  -  L  L  L  L
13 0-4   M  -  -  M  -  -
14 0-4   -  N  N  -  N  -
15 0-5   -  O  O  O  O  O
16 0-5   -  P  -  P  P  P
17 0-5   Q  -  -  Q  -  -
18 0-5   -  -  -  -  -   
19 0-5   S  S  S  -  -   
20 0-6   T  -  -  -      
21 0-6   -  U  -  U      
22 1-4   -  V  V         
23 1-4   W  X  -         
24 1-4   X  -            
25 2-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PQNWZ EAMVD HQYWE MZWJN JAJXJ L
-------------------------------
