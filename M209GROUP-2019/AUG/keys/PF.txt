EFFECTIVE PERIOD:
11-AUG-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  -  A
02 2-0   B  B  B  -  B  -
03 2-0   C  -  -  -  C  C
04 2-0   D  D  D  -  -  -
05 2-0   -  -  -  E  E  E
06 2-0   F  -  -  -  -  F
07 0-3   G  G  -  -  G  -
08 0-3   -  -  -  H  H  H
09 0-3   I  -  I  I  I  I
10 0-3   J  J  -  J  J  -
11 0-4   K  K  K  K  -  -
12 0-4   L  L  L  L  -  L
13 0-4   -  -  -  -  -  M
14 0-4   N  N  -  -  N  -
15 0-5   -  O  O  -  -  -
16 1-4   P  P  -  P  -  P
17 2-3   -  Q  Q  -  Q  -
18 2-5   -  R  R  R  R   
19 3-4   S  -  -  S  -   
20 3-4   -  T  T  -      
21 3-4   U  -  U  U      
22 3-4   -  V  -         
23 4-5   -  -  X         
24 4-5   X  Y            
25 4-5   -  -            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

IRNYI WURMA FLORO GPWSN YVKZA M
-------------------------------
