EFFECTIVE PERIOD:
15-AUG-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: PJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  B  B  -  B  -
03 1-0   C  C  -  -  C  -
04 1-0   D  -  D  -  -  D
05 1-0   -  -  -  -  E  -
06 1-0   -  -  -  F  -  F
07 1-0   -  G  G  G  -  G
08 1-0   -  -  H  H  -  -
09 1-0   -  -  -  -  I  I
10 1-0   J  J  -  -  J  -
11 1-0   -  K  -  K  K  -
12 2-0   -  L  -  -  -  L
13 0-3   -  -  M  M  -  M
14 0-3   N  N  N  -  -  -
15 0-3   O  O  -  O  -  O
16 0-3   -  -  -  -  -  P
17 0-3   Q  -  Q  Q  -  -
18 0-3   R  R  -  -  R   
19 0-3   S  S  S  -  S   
20 0-5   -  -  -  -      
21 0-5   -  -  -  U      
22 0-6   V  V  V         
23 0-6   W  X  X         
24 2-4   X  -            
25 2-6   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

KOPVF LORVL ISTYL ELNGE NFNIN R
-------------------------------
