EFFECTIVE PERIOD:
20-NOV-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   B  B  B  B  B  -
03 1-0   C  -  C  C  -  -
04 1-0   D  D  D  D  -  -
05 0-3   -  -  E  E  E  E
06 0-3   -  -  F  -  F  -
07 0-3   -  G  -  -  G  G
08 0-3   H  H  -  H  H  H
09 0-4   I  I  -  -  -  -
10 0-4   J  -  J  J  -  -
11 0-4   K  K  -  -  -  K
12 0-4   -  L  -  L  -  -
13 0-4   -  M  -  -  M  M
14 0-6   N  N  -  -  N  N
15 0-6   -  -  O  O  O  O
16 0-6   P  P  -  -  P  -
17 0-6   Q  Q  Q  -  -  Q
18 0-6   -  -  -  -  R   
19 0-6   S  -  -  -  -   
20 1-4   T  T  T  T      
21 1-4   -  U  U  U      
22 1-4   -  -  -         
23 1-4   -  X  X         
24 1-6   -  Y            
25 2-3   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

PPSVQ ANPKG OWUAV GJIPU VRBSO A
-------------------------------
