EFFECTIVE PERIOD:
23-NOV-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  -  B  B  B  B
03 1-0   C  -  -  -  -  -
04 1-0   D  -  -  -  -  -
05 1-0   E  E  E  E  -  E
06 1-0   -  -  -  -  F  F
07 1-0   -  G  G  -  -  -
08 0-4   H  -  H  H  -  H
09 0-4   I  I  I  -  I  -
10 0-4   J  -  -  J  -  -
11 0-4   -  -  -  -  K  K
12 0-4   -  L  L  -  L  L
13 0-5   -  M  M  M  M  M
14 0-6   -  -  N  -  -  N
15 0-6   O  -  -  O  -  -
16 0-6   P  P  P  P  P  P
17 0-6   -  Q  Q  Q  -  Q
18 1-5   R  -  -  -  -   
19 1-6   S  S  -  -  S   
20 2-4   -  T  -  -      
21 3-4   -  U  -  U      
22 3-5   -  V  -         
23 4-5   W  X  -         
24 4-6   X  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

ANZZO JJMYO VSQWS JOLAU TMLTL Z
-------------------------------
