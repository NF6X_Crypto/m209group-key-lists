EFFECTIVE PERIOD:
15-NOV-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 1-0   -  B  B  B  -  -
03 1-0   -  -  C  -  -  C
04 1-0   -  D  D  -  D  D
05 1-0   E  E  -  -  -  E
06 1-0   -  F  F  -  F  -
07 0-4   G  G  -  G  G  G
08 0-4   -  -  H  H  H  -
09 0-4   I  -  -  I  -  I
10 0-4   J  -  J  -  -  J
11 0-4   K  K  -  -  K  -
12 0-4   -  -  -  L  -  L
13 0-4   -  M  M  M  -  -
14 0-5   N  -  -  -  N  N
15 0-5   -  -  O  O  O  O
16 0-5   P  -  P  P  P  -
17 0-5   -  Q  -  Q  Q  Q
18 1-3   -  -  -  R  R   
19 1-3   S  -  -  -  -   
20 1-4   T  -  -  T      
21 1-4   U  -  -  -      
22 1-4   -  V  -         
23 1-4   -  X  X         
24 1-6   -  -            
25 2-3   Y  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

UETZZ ZGMGW QJDJG ANWGO UAISA T
-------------------------------
