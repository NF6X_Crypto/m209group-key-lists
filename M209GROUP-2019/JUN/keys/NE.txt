EFFECTIVE PERIOD:
19-JUN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 2-0   -  -  -  -  -  -
03 0-3   C  C  C  C  C  C
04 0-4   D  -  D  -  -  -
05 0-4   E  E  E  -  -  E
06 0-4   F  -  -  -  F  -
07 0-4   G  G  -  G  G  -
08 0-5   -  -  H  -  H  -
09 0-5   I  -  I  I  I  -
10 0-5   J  -  J  J  -  J
11 0-5   -  K  -  -  -  -
12 0-5   -  L  L  L  -  L
13 0-5   -  -  -  -  M  M
14 0-5   N  -  N  -  -  N
15 0-5   O  O  -  O  -  O
16 0-5   P  -  -  P  P  -
17 0-5   -  Q  Q  Q  -  Q
18 0-6   -  -  -  -  -   
19 0-6   S  -  -  -  S   
20 0-6   T  T  T  -      
21 0-6   -  U  -  U      
22 0-6   -  V  -         
23 0-6   -  -  -         
24 1-3   -  Y            
25 3-4   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ERNPY ALLQP PNCPO TTJOL YLAYH I
-------------------------------
