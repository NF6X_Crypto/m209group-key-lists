EFFECTIVE PERIOD:
17-JUN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  B  -  B  B
03 1-0   C  C  -  -  C  C
04 1-0   -  D  -  D  -  -
05 1-0   E  -  E  E  -  -
06 0-3   F  -  F  F  F  F
07 0-4   -  -  G  -  -  G
08 0-4   H  H  H  -  H  H
09 0-4   -  -  -  -  I  I
10 0-4   -  -  J  -  J  -
11 0-4   -  K  K  K  K  K
12 0-4   L  L  -  L  -  L
13 0-4   M  M  M  -  -  M
14 0-4   -  N  N  N  N  N
15 0-4   -  O  -  -  -  -
16 0-5   P  -  -  P  P  -
17 0-5   -  -  -  -  Q  -
18 0-6   -  -  -  -  -   
19 0-6   -  -  S  S  -   
20 0-6   T  -  T  -      
21 0-6   -  -  -  U      
22 0-6   -  V  -         
23 0-6   W  -  X         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-5   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

PQLQI EJGWR EAWZS RJMQR RDQJM I
-------------------------------
