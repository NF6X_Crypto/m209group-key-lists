EFFECTIVE PERIOD:
02-JUN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  B  -  -  -  B
03 1-0   -  -  -  C  C  C
04 1-0   D  -  D  -  -  -
05 1-0   -  -  E  E  E  -
06 1-0   -  -  -  -  F  -
07 1-0   G  -  -  -  G  G
08 2-0   -  -  -  H  H  H
09 2-0   I  I  -  I  I  I
10 2-0   -  J  J  J  J  J
11 2-0   K  K  -  K  -  -
12 2-0   -  -  -  -  -  L
13 0-5   M  -  -  -  M  -
14 0-5   -  N  -  -  N  -
15 0-5   -  O  O  O  -  -
16 1-2   -  P  P  P  -  -
17 1-4   Q  -  -  Q  Q  -
18 2-5   -  -  R  R  -   
19 2-5   S  -  S  -  -   
20 2-5   -  T  T  T      
21 2-5   U  U  -  U      
22 3-4   V  V  V         
23 3-5   -  -  -         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

JANMF ATPOA AVRNV LNVFW RZZMT Q
-------------------------------
