EFFECTIVE PERIOD:
12-JUN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   B  B  B  B  -  -
03 2-0   -  -  C  C  C  C
04 2-0   D  -  -  D  D  -
05 2-0   E  -  E  -  -  -
06 2-0   -  F  F  -  F  F
07 2-0   -  -  -  -  -  G
08 2-0   -  H  -  H  H  H
09 2-0   I  I  I  I  I  I
10 2-0   J  -  -  -  J  -
11 2-0   K  K  K  -  -  -
12 2-0   -  L  -  -  L  L
13 2-0   -  M  -  -  -  M
14 0-3   -  N  -  -  -  N
15 0-4   O  O  O  O  -  O
16 0-4   -  -  P  P  -  -
17 0-4   Q  -  Q  -  Q  Q
18 0-5   R  -  R  -  -   
19 0-5   S  -  S  S  -   
20 0-6   -  T  T  T      
21 0-6   -  -  -  U      
22 0-6   V  -  -         
23 0-6   -  -  -         
24 1-5   X  Y            
25 2-6   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QIMMG RMEGR JVYJI LUHSW ITMVV P
-------------------------------
