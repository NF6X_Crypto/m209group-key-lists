EFFECTIVE PERIOD:
21-APR-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  -  B  B  B  B
03 1-0   -  C  -  C  C  C
04 1-0   D  -  -  -  -  -
05 1-0   -  -  -  -  -  -
06 1-0   -  F  -  -  F  -
07 2-0   G  G  G  G  -  -
08 0-4   -  -  H  H  H  -
09 0-4   I  -  -  -  I  I
10 0-4   -  J  -  -  J  -
11 0-4   K  -  -  K  K  -
12 0-4   -  L  L  -  -  L
13 0-5   -  M  M  M  M  M
14 0-5   N  -  N  -  N  -
15 0-5   -  -  O  O  O  -
16 0-6   -  P  -  -  -  P
17 0-6   -  Q  -  -  -  Q
18 0-6   -  -  -  R  -   
19 0-6   -  S  S  S  S   
20 0-6   T  T  -  T      
21 0-6   -  U  -  -      
22 0-6   V  V  V         
23 0-6   -  -  -         
24 0-6   X  -            
25 0-6   Y  Z            
26 2-3   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

FPUDI RXUQY QNFFF SKLNU FPPRQ J
-------------------------------
