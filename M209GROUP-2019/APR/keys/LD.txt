EFFECTIVE PERIOD:
27-APR-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  A
02 2-0   -  -  B  -  -  B
03 2-0   -  -  -  C  C  -
04 2-0   D  -  -  -  D  -
05 2-0   -  E  -  E  E  E
06 2-0   -  -  -  F  -  F
07 0-3   -  -  -  G  -  G
08 0-3   H  -  H  -  -  -
09 0-3   -  -  I  -  -  I
10 0-4   J  -  J  J  J  -
11 0-4   K  -  K  -  -  -
12 0-4   -  L  -  L  L  -
13 0-4   -  M  M  -  M  M
14 0-6   -  N  -  -  -  -
15 0-6   -  -  O  -  -  O
16 0-6   P  -  P  -  P  -
17 0-6   Q  -  -  Q  -  -
18 1-3   R  R  -  -  -   
19 2-4   S  S  -  -  S   
20 2-6   -  -  T  T      
21 3-4   U  -  U  -      
22 3-4   -  V  V         
23 3-4   -  -  -         
24 3-4   X  Y            
25 3-5   Y  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CKPIW AJAPQ SAHPJ VZKOP PHRJW U
-------------------------------
