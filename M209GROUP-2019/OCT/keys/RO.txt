EFFECTIVE PERIOD:
11-OCT-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  -
02 1-0   B  B  B  B  B  B
03 1-0   -  -  C  C  C  -
04 1-0   -  D  D  -  D  D
05 1-0   -  -  -  -  -  -
06 2-0   -  F  F  -  F  -
07 2-0   -  G  -  G  G  -
08 2-0   -  -  -  -  H  H
09 2-0   I  I  -  -  -  -
10 2-0   J  J  -  -  -  J
11 0-5   K  K  K  -  -  -
12 0-5   L  -  L  -  -  L
13 0-6   M  -  M  M  -  M
14 0-6   -  -  -  N  N  -
15 0-6   -  O  O  -  O  O
16 0-6   -  P  P  -  P  -
17 0-6   Q  Q  Q  -  -  Q
18 1-2   -  R  -  R  R   
19 1-2   S  S  -  S  -   
20 1-2   -  -  T  T      
21 1-2   -  U  U  -      
22 1-6   -  -  -         
23 2-3   -  -  -         
24 2-4   -  Y            
25 2-5   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MYVVV BVCGV UQRFV GSLGX RAQTA V
-------------------------------
