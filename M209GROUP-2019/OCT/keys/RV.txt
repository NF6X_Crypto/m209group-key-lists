EFFECTIVE PERIOD:
18-OCT-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: RV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  -
02 1-0   B  -  B  B  B  B
03 2-0   -  C  -  -  -  -
04 0-3   -  D  -  D  D  D
05 0-3   E  -  E  -  -  -
06 0-3   -  -  -  F  F  -
07 0-3   -  G  G  -  -  -
08 0-3   -  -  H  H  -  H
09 0-4   I  I  I  I  I  I
10 0-4   J  J  -  -  -  J
11 0-4   K  K  K  -  K  K
12 0-4   -  -  -  L  L  L
13 0-4   -  -  -  M  -  M
14 0-4   -  N  -  N  -  -
15 0-4   -  O  O  O  -  O
16 0-4   P  P  P  P  P  -
17 0-4   Q  Q  -  -  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  -  -  -   
20 0-6   T  T  T  -      
21 0-6   -  -  U  -      
22 1-2   -  V  -         
23 1-6   -  -  X         
24 2-5   -  -            
25 3-4   Y  Z            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

VAAQJ CMKGM YKOCY IWYNK WVKRT P
-------------------------------
