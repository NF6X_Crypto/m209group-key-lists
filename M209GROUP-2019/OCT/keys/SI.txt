EFFECTIVE PERIOD:
31-OCT-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: SI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  -
02 0-3   -  -  -  -  B  -
03 0-4   -  -  -  -  C  C
04 0-4   D  D  D  -  -  D
05 0-4   E  E  E  E  -  E
06 0-4   -  F  -  F  F  F
07 0-4   G  G  -  -  -  G
08 0-4   -  -  H  -  H  -
09 0-4   I  I  I  -  -  -
10 0-4   -  -  -  J  J  -
11 0-4   -  K  K  -  K  K
12 0-4   L  L  L  L  L  -
13 0-5   M  M  M  -  M  M
14 0-5   -  -  -  N  -  -
15 0-5   -  -  -  O  -  O
16 0-5   P  -  P  P  P  P
17 0-5   -  -  -  Q  Q  Q
18 0-5   -  -  R  -  R   
19 0-5   -  S  -  -  -   
20 0-5   T  -  -  T      
21 0-6   U  U  U  -      
22 0-6   -  -  V         
23 0-6   -  -  X         
24 0-6   -  Y            
25 0-6   -  -            
26 1-2   Z               
27 1-6                   
-------------------------------
26 LETTER CHECK

PRQJA POKAP PHFYN KAQSP SHSZR S
-------------------------------
