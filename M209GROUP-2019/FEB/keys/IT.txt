EFFECTIVE PERIOD:
24-FEB-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: IT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  -
02 1-0   -  B  B  -  -  B
03 1-0   -  -  C  -  -  C
04 1-0   -  D  D  D  D  D
05 1-0   -  -  -  E  -  E
06 1-0   -  F  F  F  F  F
07 2-0   -  -  -  -  G  -
08 2-0   H  H  -  -  -  -
09 2-0   -  -  I  -  I  -
10 2-0   -  -  J  -  -  J
11 2-0   K  -  -  -  K  K
12 0-3   L  L  L  L  -  -
13 0-3   M  M  M  -  M  -
14 0-3   N  -  N  -  -  N
15 0-4   O  -  -  O  O  -
16 0-4   P  P  -  P  -  P
17 0-4   -  -  -  Q  -  Q
18 1-3   -  R  R  R  R   
19 1-3   -  -  -  S  S   
20 1-3   -  -  T  -      
21 1-3   U  U  -  -      
22 1-4   V  -  -         
23 1-5   -  X  X         
24 1-6   -  -            
25 2-3   -  -            
26 2-4   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

QIFNN RABJI MXTOM HJAXH HQXAR R
-------------------------------
