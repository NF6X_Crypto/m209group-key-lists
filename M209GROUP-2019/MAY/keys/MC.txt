EFFECTIVE PERIOD:
22-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   B  -  B  -  -  -
03 1-0   C  C  -  -  C  -
04 1-0   -  D  -  D  D  D
05 1-0   -  -  -  -  E  E
06 0-3   -  F  F  -  F  -
07 0-3   G  -  -  G  G  -
08 0-3   H  -  -  H  -  -
09 0-3   I  I  -  I  -  I
10 0-3   -  -  -  J  -  -
11 0-3   K  -  -  K  K  K
12 0-5   L  L  -  -  L  L
13 0-5   -  M  M  -  -  -
14 0-5   N  N  N  -  N  N
15 0-5   -  O  O  -  -  O
16 0-5   P  P  -  -  P  -
17 0-6   -  Q  Q  Q  Q  -
18 1-4   -  -  -  -  -   
19 1-5   -  -  S  S  -   
20 1-5   -  -  T  T      
21 1-5   -  -  U  U      
22 1-5   V  -  -         
23 1-6   W  X  X         
24 1-6   X  -            
25 2-6   Y  Z            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

ALKLP QTVMT UARSK MBOUS EUURA B
-------------------------------
