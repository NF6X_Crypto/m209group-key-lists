EFFECTIVE PERIOD:
28-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  -  A
02 2-0   -  B  B  -  -  B
03 0-3   -  C  -  -  C  -
04 0-3   D  -  -  D  D  -
05 0-3   E  -  E  -  -  E
06 0-3   F  -  F  F  -  -
07 0-3   G  G  -  G  G  -
08 0-3   -  H  -  H  H  H
09 0-4   -  -  -  -  I  I
10 0-5   J  -  -  -  -  J
11 0-5   K  K  -  -  -  -
12 0-5   -  L  -  L  L  L
13 0-6   -  M  M  -  -  M
14 0-6   -  N  N  N  N  -
15 0-6   O  O  O  -  -  -
16 0-6   -  -  P  P  -  P
17 0-6   Q  -  Q  Q  Q  -
18 0-6   -  R  -  R  -   
19 0-6   S  S  S  -  S   
20 0-6   T  -  T  T      
21 0-6   U  U  -  -      
22 0-6   V  V  V         
23 0-6   -  -  -         
24 1-2   -  Y            
25 2-5   -  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

OUWME YLUSE FZEDY YSDPQ BHXLA X
-------------------------------
