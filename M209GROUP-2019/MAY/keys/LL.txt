EFFECTIVE PERIOD:
05-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  A
02 2-0   -  B  -  B  B  B
03 2-0   -  C  -  -  C  C
04 2-0   D  -  D  D  -  D
05 2-0   E  -  -  E  -  -
06 2-0   F  F  F  F  F  F
07 2-0   G  G  -  G  G  G
08 0-3   -  H  H  -  -  -
09 0-3   -  I  -  -  I  I
10 0-3   J  -  J  J  -  J
11 0-3   K  K  -  -  -  -
12 0-4   L  L  -  -  L  -
13 0-5   M  -  M  M  -  M
14 0-6   N  -  -  -  N  -
15 0-6   -  -  O  -  -  -
16 0-6   -  P  P  P  -  -
17 0-6   Q  -  -  Q  Q  -
18 0-6   -  -  -  -  R   
19 0-6   S  S  -  -  -   
20 0-6   -  -  T  -      
21 0-6   U  -  -  -      
22 0-6   -  V  -         
23 0-6   W  X  X         
24 1-4   -  -            
25 2-3   -  -            
26 2-6   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

DORDK CGTST TBUZE TUOKX BMOBU T
-------------------------------
