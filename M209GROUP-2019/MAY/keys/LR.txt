EFFECTIVE PERIOD:
11-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 2-0   -  B  B  -  -  -
03 2-0   -  C  C  C  C  -
04 2-0   D  -  D  -  D  -
05 2-0   -  -  -  E  -  E
06 2-0   F  -  -  F  F  -
07 2-0   -  -  G  G  -  G
08 2-0   H  -  -  -  -  H
09 2-0   -  I  I  -  I  I
10 2-0   J  -  -  -  J  -
11 2-0   K  -  -  K  -  K
12 2-0   -  -  L  -  -  L
13 0-3   M  M  M  M  -  M
14 0-3   N  -  N  N  N  -
15 0-3   -  O  O  -  O  -
16 0-3   -  P  P  -  -  P
17 0-3   Q  Q  Q  Q  Q  -
18 0-3   -  -  -  R  R   
19 0-5   S  -  -  S  -   
20 0-5   -  T  T  T      
21 0-6   -  -  -  U      
22 0-6   V  V  -         
23 0-6   W  X  -         
24 2-3   X  -            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

XYZMA TMNQZ PPSZG FMHMR AMOTM W
-------------------------------
