EFFECTIVE PERIOD:
04-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  -  -  -  B  B
03 1-0   -  -  C  -  -  -
04 1-0   -  -  D  D  D  -
05 0-3   E  -  -  -  E  -
06 0-3   F  F  F  -  -  -
07 0-3   -  G  -  -  G  -
08 0-3   -  -  H  -  -  -
09 0-5   -  I  -  I  -  I
10 0-5   J  -  J  J  -  -
11 0-5   -  K  K  K  K  K
12 0-5   L  -  -  L  -  -
13 0-5   M  M  -  M  M  M
14 0-5   -  N  N  -  N  N
15 0-5   O  -  -  O  -  -
16 1-4   -  -  -  -  P  P
17 1-5   Q  -  Q  Q  Q  Q
18 2-3   R  -  -  R  -   
19 3-4   -  -  S  -  -   
20 3-4   -  T  T  T      
21 3-4   U  U  U  U      
22 3-5   V  V  -         
23 3-5   -  -  X         
24 3-5   -  Y            
25 3-5   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

OQVNQ ATORZ SMWML TGOQO ARHAO K
-------------------------------
