EFFECTIVE PERIOD:
24-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ME
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 2-0   -  -  -  -  B  -
03 2-0   -  C  -  -  C  C
04 2-0   D  -  D  D  -  D
05 2-0   -  E  -  -  E  -
06 0-3   F  F  -  F  F  F
07 0-3   G  G  G  -  -  -
08 0-3   -  H  -  -  H  H
09 0-3   I  -  -  I  I  -
10 0-3   -  J  J  J  J  -
11 0-6   K  K  -  -  -  K
12 0-6   L  -  L  -  L  -
13 0-6   M  M  -  -  -  -
14 0-6   -  N  -  -  N  -
15 0-6   -  -  -  -  -  O
16 0-6   -  P  P  P  -  P
17 0-6   Q  -  Q  Q  -  Q
18 1-2   -  R  -  -  R   
19 1-5   -  -  S  -  -   
20 1-6   -  -  T  T      
21 2-3   U  U  -  U      
22 3-6   V  V  -         
23 3-6   -  X  X         
24 3-6   -  -            
25 3-6   Y  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OIMPN VATIW UIORQ ITVGP FAPUT M
-------------------------------
