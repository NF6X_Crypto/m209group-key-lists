EFFECTIVE PERIOD:
30-MAY-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 2-0   B  B  -  B  B  B
03 0-3   C  C  -  C  -  -
04 0-3   D  -  D  -  -  D
05 0-3   -  -  -  -  -  E
06 0-3   -  F  F  F  -  -
07 0-3   -  G  G  G  -  -
08 0-3   H  -  -  -  H  H
09 0-3   -  I  I  -  -  -
10 0-4   J  J  J  J  J  J
11 0-4   K  K  K  -  K  K
12 0-4   L  -  L  -  L  L
13 0-4   -  M  M  -  M  -
14 0-4   N  -  -  -  -  N
15 0-6   -  O  O  -  O  O
16 0-6   P  -  -  -  P  -
17 0-6   Q  -  -  Q  -  Q
18 0-6   -  -  -  -  -   
19 0-6   -  -  -  S  S   
20 1-2   T  T  T  -      
21 1-3   -  U  -  U      
22 3-6   V  V  V         
23 4-5   W  X  -         
24 4-6   -  -            
25 4-6   -  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OUZKS BAEZJ PPDLS STMZL VULMK R
-------------------------------
