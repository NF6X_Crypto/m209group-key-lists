SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF DEC 2019

    01 DEC 2019  00:00-23:59 GMT:  USE KEY TN
    02 DEC 2019  00:00-23:59 GMT:  USE KEY TO
    03 DEC 2019  00:00-23:59 GMT:  USE KEY TP
    04 DEC 2019  00:00-23:59 GMT:  USE KEY TQ
    05 DEC 2019  00:00-23:59 GMT:  USE KEY TR
    06 DEC 2019  00:00-23:59 GMT:  USE KEY TS
    07 DEC 2019  00:00-23:59 GMT:  USE KEY TT
    08 DEC 2019  00:00-23:59 GMT:  USE KEY TU
    09 DEC 2019  00:00-23:59 GMT:  USE KEY TV
    10 DEC 2019  00:00-23:59 GMT:  USE KEY TW
    11 DEC 2019  00:00-23:59 GMT:  USE KEY TX
    12 DEC 2019  00:00-23:59 GMT:  USE KEY TY
    13 DEC 2019  00:00-23:59 GMT:  USE KEY TZ
    14 DEC 2019  00:00-23:59 GMT:  USE KEY UA
    15 DEC 2019  00:00-23:59 GMT:  USE KEY UB
    16 DEC 2019  00:00-23:59 GMT:  USE KEY UC
    17 DEC 2019  00:00-23:59 GMT:  USE KEY UD
    18 DEC 2019  00:00-23:59 GMT:  USE KEY UE
    19 DEC 2019  00:00-23:59 GMT:  USE KEY UF
    20 DEC 2019  00:00-23:59 GMT:  USE KEY UG
    21 DEC 2019  00:00-23:59 GMT:  USE KEY UH
    22 DEC 2019  00:00-23:59 GMT:  USE KEY UI
    23 DEC 2019  00:00-23:59 GMT:  USE KEY UJ
    24 DEC 2019  00:00-23:59 GMT:  USE KEY UK
    25 DEC 2019  00:00-23:59 GMT:  USE KEY UL
    26 DEC 2019  00:00-23:59 GMT:  USE KEY UM
    27 DEC 2019  00:00-23:59 GMT:  USE KEY UN
    28 DEC 2019  00:00-23:59 GMT:  USE KEY UO
    29 DEC 2019  00:00-23:59 GMT:  USE KEY UP
    30 DEC 2019  00:00-23:59 GMT:  USE KEY UQ
    31 DEC 2019  00:00-23:59 GMT:  USE KEY UR

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  -
02 1-0   B  B  -  -  B  B
03 1-0   -  -  -  -  -  -
04 1-0   -  -  D  D  D  D
05 2-0   -  -  E  E  E  -
06 2-0   -  F  -  -  F  -
07 2-0   G  G  -  G  G  G
08 2-0   H  H  -  -  -  H
09 0-3   I  -  I  I  -  -
10 0-3   J  J  J  -  -  -
11 0-3   -  -  -  K  -  -
12 0-3   -  L  -  -  L  -
13 0-3   M  M  -  M  -  M
14 0-5   -  N  N  N  -  N
15 0-6   -  O  -  O  -  O
16 0-6   -  P  -  -  P  -
17 0-6   -  -  Q  Q  Q  Q
18 0-6   R  R  -  -  -   
19 0-6   S  -  S  S  -   
20 1-3   T  -  -  -      
21 1-5   U  U  U  -      
22 2-3   -  V  V         
23 2-6   -  -  X         
24 2-6   -  -            
25 2-6   Y  Z            
26 2-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

RJPZQ MOOQN UKQSS AIPTW UJJAK O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 2-0   -  -  B  -  -  B
03 2-0   C  C  C  C  C  C
04 2-0   D  -  -  -  -  D
05 2-0   E  -  -  E  E  -
06 2-0   F  -  -  -  F  -
07 2-0   G  -  -  -  -  G
08 2-0   -  -  -  -  H  -
09 2-0   -  I  I  I  I  -
10 0-3   J  J  -  -  J  J
11 0-3   K  -  K  K  -  K
12 0-4   -  L  -  L  L  -
13 0-4   M  -  -  M  -  -
14 0-4   -  -  -  N  -  -
15 0-4   O  O  O  -  -  O
16 0-4   -  -  -  P  -  P
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  -  R  R  R   
19 0-6   S  S  S  -  -   
20 0-6   -  T  -  -      
21 0-6   -  U  -  -      
22 1-3   -  V  V         
23 1-5   W  X  X         
24 2-4   X  -            
25 2-4   Y  Z            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

WLMNY YKMVS UVEYC CJQDQ VQVCL Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  A  -
02 2-0   B  -  -  B  -  B
03 0-3   -  -  C  -  -  C
04 0-3   -  D  D  D  D  -
05 0-4   E  E  -  -  E  -
06 0-4   -  F  -  F  -  -
07 0-4   G  -  G  -  G  -
08 0-4   -  H  H  H  -  H
09 0-4   -  I  I  I  -  -
10 0-4   -  -  J  -  -  J
11 0-4   -  -  -  K  K  K
12 0-4   -  L  -  -  L  L
13 0-4   -  M  M  -  M  M
14 0-4   N  -  -  -  -  -
15 0-5   O  O  O  O  O  -
16 0-5   -  P  -  -  P  -
17 0-5   -  -  Q  -  -  -
18 0-5   -  R  R  -  R   
19 0-5   S  -  S  -  -   
20 0-5   -  -  -  T      
21 0-5   U  U  -  -      
22 0-6   V  -  V         
23 0-6   W  -  -         
24 1-3   -  -            
25 3-6   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MPOOM ELMOQ QJXDW CSSPZ IZNTE O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   B  B  -  -  B  B
03 1-0   C  C  -  -  -  -
04 0-5   D  D  D  D  D  -
05 0-5   -  -  -  E  -  E
06 0-5   F  -  -  -  F  -
07 0-5   -  -  -  G  -  G
08 0-5   H  -  -  -  H  H
09 0-5   I  -  I  I  -  -
10 0-6   -  J  -  J  -  -
11 0-6   -  K  K  -  K  -
12 0-6   -  -  -  L  L  L
13 0-6   -  M  M  M  -  -
14 0-6   -  -  N  -  -  N
15 0-6   O  O  -  -  -  -
16 1-2   -  -  P  -  -  P
17 1-3   Q  Q  Q  Q  -  -
18 1-4   -  R  R  R  R   
19 1-5   S  -  S  -  -   
20 1-5   T  -  -  -      
21 1-5   -  U  U  -      
22 1-5   -  -  -         
23 1-6   W  X  X         
24 1-6   -  -            
25 2-4   Y  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PTCUN MMTVS MANAU NLCAS NDKCJ A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  B  B  -  -  -
03 1-0   -  C  C  C  -  -
04 2-0   -  D  D  -  D  -
05 2-0   E  -  -  E  -  E
06 2-0   -  -  F  -  -  -
07 2-0   -  G  -  G  -  G
08 2-0   -  -  H  H  -  H
09 0-3   I  -  I  -  -  I
10 0-4   -  -  J  -  J  -
11 0-4   -  K  K  -  -  K
12 0-4   L  -  -  L  -  L
13 0-4   M  -  -  M  M  M
14 0-4   N  N  -  -  N  -
15 0-4   -  -  -  O  O  O
16 0-4   P  P  P  -  -  -
17 0-5   -  -  Q  -  Q  Q
18 0-6   -  -  -  R  R   
19 0-6   -  S  S  -  S   
20 0-6   T  -  -  -      
21 0-6   U  U  -  -      
22 1-2   -  V  -         
23 1-4   W  -  X         
24 1-4   -  -            
25 1-4   Y  Z            
26 2-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TMRCR QMOZO MAHWL UJNGN MWITO C
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  A  -  -  A  A
02 0-3   -  B  B  -  B  -
03 0-3   -  C  C  C  C  C
04 0-3   D  -  D  D  -  D
05 0-3   -  -  -  -  -  -
06 0-3   F  F  F  F  F  F
07 0-3   G  -  -  -  -  G
08 0-5   H  H  -  H  -  -
09 0-5   -  -  -  I  I  -
10 0-5   -  -  J  -  J  J
11 0-5   K  -  K  K  -  -
12 0-5   -  L  L  -  -  -
13 0-5   M  -  M  -  M  -
14 0-5   N  N  -  N  N  N
15 0-6   -  O  O  -  O  O
16 0-6   -  -  P  -  -  P
17 0-6   -  -  Q  Q  -  -
18 0-6   -  -  -  -  R   
19 0-6   -  -  -  S  -   
20 1-3   T  -  -  -      
21 2-4   -  -  U  U      
22 2-5   V  V  -         
23 3-6   W  X  X         
24 3-6   -  Y            
25 3-6   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

BVUZL JKTJA RNTAB LTKCM ZZAAT V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  A
02 2-0   -  B  B  B  -  B
03 2-0   -  C  C  -  C  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  E  E  -  E
06 0-3   -  F  F  -  -  -
07 0-3   G  G  -  -  G  G
08 0-3   -  H  H  -  H  -
09 0-3   I  -  -  I  -  -
10 0-3   J  -  -  -  -  J
11 0-3   -  K  K  -  -  -
12 0-6   -  L  -  L  -  L
13 0-6   -  M  -  M  M  -
14 0-6   N  -  N  N  N  N
15 0-6   -  -  O  -  -  O
16 0-6   -  -  -  -  -  -
17 0-6   -  -  -  -  Q  Q
18 1-5   R  R  -  R  R   
19 2-3   S  S  S  -  S   
20 2-3   -  -  -  T      
21 2-4   -  -  U  U      
22 2-6   -  -  V         
23 2-6   W  -  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KVBTS LMVUB LZTZU KSUSA VULZU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  -  A
02 2-0   -  -  -  B  B  -
03 2-0   -  C  C  C  C  C
04 0-5   D  D  -  D  -  D
05 0-5   E  E  -  -  E  E
06 0-5   -  F  F  -  -  -
07 0-5   G  G  G  -  G  G
08 0-5   H  -  -  -  -  -
09 0-6   I  I  -  -  I  -
10 0-6   -  J  J  J  J  -
11 0-6   K  -  K  K  -  K
12 0-6   L  -  L  L  L  -
13 0-6   M  M  M  -  -  M
14 0-6   N  N  N  N  -  -
15 0-6   O  O  -  -  -  -
16 1-4   -  P  P  P  -  P
17 2-3   -  -  Q  Q  Q  Q
18 2-4   -  -  -  -  R   
19 2-4   -  -  -  -  -   
20 2-4   T  T  T  -      
21 2-4   -  -  -  U      
22 2-5   V  -  -         
23 2-5   -  X  -         
24 2-5   X  Y            
25 2-5   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZNJGF ZTINA ANQUS MRAFA NBOVA T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  D  D  D  D
05 1-0   E  -  E  -  -  E
06 1-0   F  F  -  F  F  F
07 1-0   G  -  G  -  G  -
08 0-4   -  -  -  H  -  -
09 0-4   -  I  -  I  -  I
10 0-4   -  -  J  J  J  J
11 0-4   -  K  -  -  K  K
12 0-4   -  -  -  L  -  -
13 0-5   M  M  M  M  M  M
14 0-5   N  N  -  N  N  -
15 0-5   O  -  -  -  -  -
16 1-4   P  P  P  -  P  -
17 1-6   Q  Q  Q  -  -  Q
18 2-5   -  -  R  R  R   
19 2-6   -  S  S  -  -   
20 3-5   T  T  -  T      
21 4-5   U  U  -  -      
22 4-5   V  V  V         
23 4-5   W  -  X         
24 4-5   -  -            
25 5-6   -  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VANAV UVWLA EVQVA MXARG RSKVN U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  B  B  B  -  -
03 1-0   C  -  C  C  C  -
04 1-0   -  -  -  D  D  -
05 2-0   E  E  -  -  E  E
06 2-0   -  F  F  -  -  -
07 2-0   G  G  G  G  -  G
08 2-0   H  H  -  -  H  H
09 2-0   I  -  -  -  -  I
10 0-3   J  -  -  J  -  -
11 0-3   -  -  K  -  K  K
12 0-3   -  L  -  L  L  -
13 0-3   -  -  M  M  -  -
14 0-3   N  N  N  -  -  -
15 0-6   O  O  O  -  -  O
16 1-2   P  -  -  P  P  P
17 1-6   -  -  -  Q  -  -
18 2-3   R  R  -  R  R   
19 2-3   -  S  S  -  -   
20 2-3   T  -  T  T      
21 2-3   -  U  -  -      
22 3-5   -  -  -         
23 3-5   W  X  -         
24 3-6   -  Y            
25 3-6   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AMFKV QUKJJ UAUJE ASSSJ TFVOA S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  C  -  C
04 1-0   D  -  -  -  -  D
05 1-0   -  E  E  E  -  E
06 2-0   -  -  F  -  F  F
07 2-0   -  -  -  G  G  -
08 0-3   H  -  -  -  -  -
09 0-3   -  I  -  -  -  I
10 0-3   J  -  -  -  -  -
11 0-3   K  -  -  K  K  -
12 0-3   L  L  -  -  L  L
13 0-3   M  M  M  M  -  M
14 0-3   -  N  N  -  N  -
15 0-3   O  -  O  O  O  -
16 0-3   -  -  P  -  -  -
17 0-4   -  -  Q  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   S  -  -  S  S   
20 0-6   -  -  -  -      
21 0-6   -  U  -  U      
22 1-3   V  -  V         
23 1-3   -  X  X         
24 1-3   -  Y            
25 1-6   -  -            
26 2-5   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

NRTVM ARAQU NSQKS VZZOJ IKVAP S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 1-0   B  B  -  B  B  B
03 1-0   -  C  C  -  -  C
04 1-0   -  D  D  -  D  -
05 1-0   -  E  -  E  E  E
06 1-0   -  -  F  F  F  F
07 1-0   G  -  G  -  -  G
08 2-0   H  H  H  -  H  H
09 2-0   -  I  I  -  -  -
10 0-3   -  -  J  -  J  -
11 0-3   K  -  -  K  -  -
12 0-3   L  -  -  -  L  L
13 0-4   M  M  M  -  M  -
14 0-5   -  -  N  N  -  -
15 0-6   -  O  O  O  -  O
16 0-6   -  -  -  -  P  P
17 0-6   Q  Q  -  Q  -  Q
18 0-6   -  -  R  R  R   
19 0-6   -  S  -  -  -   
20 0-6   -  T  -  T      
21 0-6   U  -  -  U      
22 0-6   V  V  V         
23 0-6   -  X  -         
24 1-3   X  -            
25 1-6   -  Z            
26 2-3   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

IYEZT WTEAM QWZTO IEOYJ BRWAE P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  B  -  -  B
03 1-0   C  -  -  -  C  C
04 1-0   -  -  D  D  -  -
05 1-0   E  E  -  -  E  E
06 1-0   -  -  F  F  F  F
07 1-0   G  -  G  -  -  -
08 1-0   -  H  -  -  -  -
09 2-0   -  -  -  I  I  I
10 2-0   -  -  J  J  -  -
11 2-0   -  K  K  -  -  -
12 2-0   L  -  L  L  -  L
13 2-0   M  -  M  -  -  M
14 2-0   N  -  N  N  N  N
15 2-0   O  O  O  O  -  -
16 2-0   P  P  -  P  P  P
17 2-0   -  -  -  Q  Q  -
18 2-0   R  R  -  R  -   
19 2-0   -  -  -  -  S   
20 0-4   -  T  -  -      
21 0-5   U  U  U  -      
22 0-5   V  V  V         
23 0-6   -  X  -         
24 1-2   -  Y            
25 1-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VPPPE AURRX DMSNA XEKPC DQNOP Y
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  A  A  -
02 2-0   -  B  -  -  -  -
03 2-0   C  -  C  -  C  -
04 0-3   D  D  D  -  D  -
05 0-3   -  E  E  E  E  E
06 0-3   -  -  -  F  F  F
07 0-3   -  G  -  -  -  -
08 0-3   H  H  -  H  -  H
09 0-3   -  I  -  -  -  I
10 0-4   -  J  J  -  J  J
11 0-5   -  -  K  K  -  K
12 0-5   L  -  -  -  -  -
13 0-5   M  -  -  -  -  M
14 0-5   N  N  N  N  N  N
15 0-6   O  -  -  -  O  O
16 0-6   P  P  P  -  P  -
17 0-6   -  Q  -  -  -  Q
18 0-6   -  R  -  R  -   
19 0-6   S  -  S  S  S   
20 1-4   -  T  T  T      
21 2-4   -  U  U  -      
22 2-5   V  -  V         
23 3-6   -  -  X         
24 3-6   -  Y            
25 3-6   -  Z            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

QGYTO QWSPQ UPYSW OMSFW UOIVF K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  -  -  -  -  -
03 1-0   C  C  -  C  C  C
04 1-0   D  -  -  D  D  D
05 1-0   E  E  -  E  E  -
06 1-0   -  -  -  F  F  -
07 1-0   -  -  -  G  -  G
08 1-0   H  H  H  -  -  -
09 1-0   -  -  I  I  I  I
10 1-0   -  -  J  J  -  J
11 1-0   K  -  -  K  -  K
12 1-0   L  L  -  -  L  L
13 0-3   -  M  M  -  M  -
14 0-3   -  -  N  -  N  -
15 0-3   O  O  O  -  -  O
16 0-3   P  -  -  -  P  P
17 0-3   -  Q  -  -  Q  Q
18 0-3   R  R  -  R  R   
19 0-4   S  -  S  S  -   
20 0-5   T  -  T  -      
21 0-5   -  -  -  U      
22 0-5   -  V  -         
23 0-6   W  -  X         
24 1-3   X  Y            
25 2-5   Y  -            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

ZNSOT OOONX NTFUN QGKXM ETWNL S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  -  B  B  B  -
03 1-0   -  C  -  -  -  C
04 1-0   D  -  -  D  -  D
05 2-0   -  -  -  E  E  E
06 0-3   F  -  F  -  F  -
07 0-4   G  -  -  -  -  G
08 0-4   -  H  -  -  -  -
09 0-4   -  -  I  I  -  -
10 0-4   -  -  J  J  J  -
11 0-4   K  -  -  -  -  K
12 0-5   -  L  L  L  -  -
13 0-5   -  -  -  -  -  -
14 0-5   N  -  N  -  -  -
15 0-5   O  O  O  -  -  O
16 0-5   P  P  P  -  -  -
17 0-5   Q  -  Q  Q  Q  -
18 0-5   R  R  R  R  R   
19 0-5   S  -  S  -  S   
20 1-3   -  T  -  T      
21 1-4   -  U  -  U      
22 2-3   -  V  -         
23 4-5   -  X  X         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JIHIN WOUZP OEXUF CFXNC JIBCU A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  B  B  B  -  -
03 1-0   C  C  -  C  -  -
04 1-0   -  -  -  -  D  -
05 1-0   -  E  E  -  E  E
06 1-0   F  F  F  F  F  F
07 2-0   G  -  -  -  -  -
08 2-0   H  H  H  H  -  -
09 2-0   I  -  -  -  I  I
10 2-0   -  J  J  J  J  J
11 0-4   -  -  K  -  -  -
12 0-5   L  -  L  L  -  -
13 0-5   M  M  -  M  -  M
14 0-5   -  N  N  -  N  N
15 0-5   -  -  O  -  -  O
16 1-2   -  P  P  P  P  P
17 1-4   Q  -  -  -  -  Q
18 2-5   R  R  R  R  R   
19 2-5   -  -  -  S  -   
20 2-5   T  T  T  -      
21 2-5   U  U  U  -      
22 3-4   -  -  -         
23 3-5   W  X  -         
24 4-5   -  -            
25 4-5   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GRVPV VUJRF MUYYG LAVUK QPIZP N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 2-0   -  -  -  -  -  B
03 2-0   C  -  C  C  -  C
04 2-0   -  D  -  -  D  -
05 2-0   E  -  -  E  -  E
06 2-0   -  -  F  F  F  F
07 0-4   -  -  G  -  G  G
08 0-4   H  H  -  H  -  H
09 0-4   -  I  I  I  I  I
10 0-4   J  J  -  -  -  -
11 0-4   -  K  K  K  -  -
12 0-4   L  -  -  -  L  -
13 0-4   M  -  -  M  M  -
14 0-5   N  N  N  -  -  -
15 0-6   O  -  -  -  O  -
16 0-6   -  P  P  P  P  P
17 0-6   Q  Q  Q  -  Q  -
18 0-6   -  -  R  -  R   
19 0-6   -  -  -  S  S   
20 0-6   -  T  T  -      
21 0-6   -  U  U  U      
22 0-6   -  -  -         
23 0-6   W  -  -         
24 1-3   -  -            
25 2-3   -  Z            
26 2-4   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

LJHLC RQNRR ISLUY MLIIP JOMDA N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   -  -  -  B  -  -
03 2-0   C  C  -  -  C  C
04 2-0   -  D  D  -  -  D
05 0-3   E  E  E  -  E  E
06 0-3   F  -  F  F  F  F
07 0-3   G  G  G  -  -  -
08 0-3   H  -  H  -  H  -
09 0-3   I  -  -  -  -  -
10 0-3   -  -  J  J  J  -
11 0-3   K  -  K  K  K  K
12 0-3   -  L  -  -  -  -
13 0-4   M  M  -  M  M  -
14 0-4   N  N  N  -  -  -
15 0-6   O  O  -  O  -  -
16 0-6   -  -  -  P  -  -
17 0-6   -  -  Q  -  Q  Q
18 0-6   -  R  -  -  R   
19 0-6   -  -  -  S  S   
20 1-4   T  T  T  T      
21 1-5   -  U  U  U      
22 2-4   V  V  -         
23 2-6   W  X  X         
24 3-6   X  -            
25 3-6   Y  -            
26 3-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

YADTC FAAJX WJKTT TOYFR MFEAS H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 2-0   -  B  B  -  -  -
03 0-3   -  C  -  -  -  C
04 0-3   D  -  D  -  D  D
05 0-3   E  -  E  -  E  -
06 0-3   -  -  F  F  -  -
07 0-3   -  G  G  -  G  -
08 0-3   -  H  -  H  H  -
09 0-3   I  -  I  -  I  -
10 0-3   J  J  J  J  J  -
11 0-3   K  K  K  -  K  K
12 0-3   L  L  L  L  -  L
13 0-3   -  M  -  -  -  M
14 0-4   -  -  -  N  -  N
15 0-4   -  O  -  O  O  O
16 0-4   -  P  -  -  -  -
17 0-4   Q  Q  Q  Q  Q  Q
18 0-4   -  R  -  R  -   
19 0-4   -  S  S  S  -   
20 0-5   T  T  -  T      
21 0-6   U  -  -  U      
22 0-6   -  -  -         
23 0-6   -  -  -         
24 1-5   X  -            
25 3-4   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

FOMTO XLHLL TPDMW PVHZS ZTMMC I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   B  -  B  B  B  -
03 1-0   C  C  -  C  -  C
04 2-0   -  D  D  -  D  -
05 2-0   E  -  -  -  -  -
06 2-0   -  F  F  F  -  F
07 2-0   -  -  G  -  -  -
08 2-0   H  -  H  H  H  -
09 0-3   I  I  -  I  -  I
10 0-5   J  J  -  J  -  -
11 0-5   -  K  K  K  K  K
12 0-5   L  L  L  -  -  -
13 0-5   -  M  -  -  -  -
14 0-5   -  -  N  -  N  N
15 0-5   -  O  -  O  -  -
16 1-2   P  -  P  P  -  -
17 1-2   -  Q  -  -  Q  Q
18 1-2   -  R  -  R  -   
19 1-2   -  S  -  -  S   
20 1-5   T  -  T  -      
21 2-3   U  U  U  -      
22 2-3   -  -  V         
23 2-3   W  -  X         
24 2-4   -  -            
25 3-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

UZXOQ RQUMN KVITO UQHRS UWQQK P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  A  A
02 2-0   -  B  -  B  -  B
03 2-0   C  -  -  C  C  -
04 0-3   D  D  -  D  D  -
05 0-3   E  -  -  E  -  E
06 0-3   -  F  -  -  F  -
07 0-3   G  G  G  G  -  -
08 0-3   -  H  -  H  -  -
09 0-3   I  I  I  -  I  -
10 0-3   -  J  J  -  J  J
11 0-4   K  -  K  K  K  -
12 0-4   -  L  -  L  L  L
13 0-4   M  -  -  -  M  M
14 0-4   N  -  N  -  -  -
15 0-6   O  O  O  -  O  -
16 0-6   -  -  -  -  -  P
17 0-6   -  Q  -  Q  -  -
18 0-6   R  R  R  -  -   
19 0-6   -  -  -  S  -   
20 1-3   -  -  T  -      
21 2-4   -  U  U  -      
22 2-5   V  -  V         
23 3-6   -  -  X         
24 3-6   -  Y            
25 3-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QSPER JVRSV SPJHF GAFWA VAOLR T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  -
02 2-0   -  -  B  B  -  B
03 2-0   C  C  -  C  -  C
04 2-0   -  -  D  D  D  -
05 0-3   E  -  -  -  E  E
06 0-3   F  -  F  -  F  F
07 0-3   G  G  G  G  G  -
08 0-3   -  H  -  -  H  H
09 0-3   I  I  -  -  -  -
10 0-6   -  J  -  J  J  -
11 0-6   -  -  -  K  -  K
12 0-6   -  L  L  L  -  L
13 0-6   -  M  M  M  -  M
14 0-6   N  N  N  -  -  -
15 0-6   -  -  -  -  O  -
16 1-2   P  -  P  -  -  -
17 1-5   Q  Q  -  Q  Q  -
18 2-3   -  R  -  R  R   
19 2-4   S  S  S  -  -   
20 2-5   -  -  -  T      
21 2-5   U  U  U  U      
22 2-6   V  V  V         
23 2-6   W  -  X         
24 2-6   X  -            
25 2-6   Y  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

UMZOM MOUWT NAPUO MJVTU MIUTO V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   B  -  B  -  B  -
03 1-0   C  C  C  -  -  -
04 1-0   D  D  D  D  D  -
05 1-0   -  E  -  -  E  -
06 1-0   -  -  -  -  F  F
07 1-0   G  -  G  G  -  -
08 1-0   H  -  H  H  H  -
09 1-0   I  I  -  I  -  -
10 1-0   -  -  J  -  -  -
11 2-0   -  -  K  K  -  -
12 2-0   L  L  L  -  -  L
13 2-0   M  M  M  -  M  M
14 2-0   -  -  N  N  -  N
15 0-4   -  O  -  O  -  O
16 0-4   -  -  -  -  P  -
17 0-4   -  Q  -  Q  -  Q
18 0-4   -  R  -  R  R   
19 0-4   S  S  -  -  S   
20 0-4   -  -  -  -      
21 0-4   U  U  U  -      
22 0-4   V  -  V         
23 0-5   -  -  -         
24 0-5   -  -            
25 0-6   -  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

FMNPQ RSEAD KNWRO PISKY EBXAA P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  A
02 2-0   B  -  -  -  -  -
03 2-0   -  C  -  -  C  -
04 2-0   -  -  D  D  -  D
05 2-0   E  E  -  -  -  E
06 2-0   -  -  F  F  -  -
07 2-0   G  -  G  -  -  G
08 2-0   -  H  -  H  -  -
09 0-3   -  I  I  -  I  -
10 0-3   J  -  J  J  J  J
11 0-3   -  K  -  -  K  -
12 0-3   L  L  L  -  -  L
13 0-5   M  M  M  -  M  M
14 0-5   -  -  -  N  N  -
15 0-5   -  -  O  O  -  -
16 0-5   P  P  -  P  -  P
17 0-5   -  -  -  Q  -  -
18 0-5   R  -  R  -  R   
19 0-5   -  S  S  -  -   
20 0-5   T  T  T  -      
21 0-5   -  U  U  U      
22 0-6   V  -  -         
23 0-6   -  X  -         
24 1-4   -  -            
25 1-6   Y  -            
26 2-3   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

CZOAP PYIRC MFCPA PSLZP LVRKJ J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  -  B  -  B
03 1-0   -  -  -  -  C  -
04 1-0   -  D  D  D  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  -  F  F  -  F
07 2-0   -  G  -  G  G  -
08 2-0   H  -  H  -  -  -
09 2-0   I  -  I  -  I  I
10 2-0   -  J  -  J  -  J
11 2-0   -  K  K  K  -  K
12 2-0   -  -  L  L  -  -
13 0-4   M  -  M  -  -  M
14 0-4   -  -  N  N  N  -
15 0-6   -  O  O  -  -  -
16 1-2   P  -  -  -  P  -
17 1-4   -  -  -  Q  Q  Q
18 1-4   R  R  R  -  -   
19 1-4   S  S  -  -  S   
20 1-4   -  -  T  T      
21 2-6   -  -  U  U      
22 3-4   V  -  V         
23 3-6   W  -  -         
24 4-5   -  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XLRRT ARSNY NAABS URNSU TKLSC U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  C  -  -  C
04 1-0   D  D  D  D  D  D
05 1-0   E  E  -  E  E  E
06 1-0   -  F  -  F  -  F
07 1-0   G  -  G  -  G  -
08 1-0   H  -  -  H  H  -
09 2-0   I  I  I  -  -  I
10 2-0   J  -  J  J  J  J
11 2-0   -  -  -  -  -  -
12 2-0   L  -  L  -  -  -
13 0-3   -  -  -  M  M  M
14 0-3   -  -  N  -  N  -
15 0-3   -  O  -  -  O  -
16 0-3   -  P  -  P  -  -
17 0-4   Q  -  Q  Q  Q  Q
18 0-4   -  R  -  -  -   
19 0-4   S  -  S  -  -   
20 0-4   T  T  T  -      
21 0-6   -  U  -  -      
22 1-4   V  -  -         
23 1-4   W  -  -         
24 2-3   X  Y            
25 2-6   Y  -            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QQCZO LWMJW CHSYV RSKTW KMKIJ M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  -  -
02 2-0   -  -  -  -  B  -
03 2-0   C  C  -  C  -  -
04 2-0   D  -  D  -  D  -
05 2-0   E  -  -  E  E  -
06 2-0   -  F  F  F  -  F
07 2-0   G  -  -  -  -  -
08 2-0   -  -  -  H  -  H
09 2-0   -  I  I  -  I  I
10 2-0   J  -  J  -  -  J
11 2-0   -  -  K  -  K  K
12 2-0   -  L  -  -  L  -
13 0-3   M  -  M  -  -  -
14 0-4   N  N  N  N  -  N
15 0-4   -  -  O  O  -  -
16 0-4   P  -  -  P  P  -
17 0-4   -  -  Q  -  -  Q
18 0-4   R  R  -  R  -   
19 0-6   -  -  S  S  S   
20 0-6   T  -  -  T      
21 0-6   -  U  -  -      
22 0-6   -  V  V         
23 0-6   -  X  X         
24 0-6   -  Y            
25 0-6   Y  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

JGGZB GYSMT MSITN IZHCO IZSOT Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  A
02 2-0   -  B  -  B  -  -
03 2-0   -  C  -  C  -  C
04 2-0   -  -  -  D  D  D
05 2-0   E  E  -  E  -  -
06 2-0   -  -  -  -  F  F
07 2-0   G  -  G  G  G  G
08 2-0   -  -  -  -  -  -
09 2-0   -  -  -  I  -  -
10 2-0   J  J  -  -  -  J
11 0-3   K  K  K  K  K  K
12 0-3   -  -  -  L  L  -
13 0-3   M  -  -  M  -  M
14 0-3   -  N  -  -  N  N
15 0-4   -  O  O  -  O  O
16 0-5   P  P  P  -  -  -
17 0-5   -  -  -  -  Q  -
18 0-5   -  R  -  -  R   
19 0-5   -  -  S  -  S   
20 0-5   -  -  T  T      
21 0-6   -  U  U  -      
22 1-3   -  -  V         
23 1-4   W  -  X         
24 2-5   X  Y            
25 2-5   Y  -            
26 2-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

WSOTA NXQGU ILPVI PGDIL VWIBY X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   B  B  B  B  -  -
03 2-0   -  -  C  C  -  -
04 0-3   -  D  D  D  D  -
05 0-3   E  -  -  -  E  E
06 0-3   F  F  -  F  -  F
07 0-3   -  -  -  G  -  G
08 0-3   H  -  H  -  H  -
09 0-3   -  -  I  I  I  -
10 0-3   J  -  J  -  -  -
11 0-3   K  -  -  -  -  K
12 0-5   -  -  L  -  -  L
13 0-5   -  M  -  -  M  -
14 0-5   N  -  -  N  -  N
15 0-5   -  O  -  O  O  O
16 0-6   P  P  P  P  P  -
17 0-6   Q  -  -  Q  -  -
18 0-6   -  -  R  R  -   
19 0-6   -  -  S  -  S   
20 1-2   T  T  T  -      
21 1-6   -  U  -  U      
22 3-4   V  V  -         
23 3-5   -  -  X         
24 3-5   X  Y            
25 3-5   -  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JSMOZ ORPEO CYASC NNSVM QVJLJ O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 1-0   -  B  B  -  -  B
03 1-0   -  C  C  -  -  C
04 1-0   D  -  -  D  D  -
05 2-0   -  -  E  -  E  -
06 2-0   -  -  F  -  F  F
07 2-0   -  -  G  -  -  -
08 2-0   -  H  H  -  -  -
09 0-3   -  -  -  I  I  -
10 0-5   J  J  -  J  -  J
11 0-5   K  -  -  K  K  K
12 0-5   -  -  -  L  -  L
13 0-5   -  -  M  -  M  M
14 0-5   N  -  -  N  -  -
15 0-6   O  O  -  O  -  O
16 0-6   P  -  P  P  -  P
17 0-6   -  -  Q  Q  -  -
18 1-3   -  -  R  R  R   
19 1-3   S  S  -  S  S   
20 1-6   -  T  -  -      
21 1-6   U  -  U  -      
22 1-6   -  V  -         
23 1-6   -  X  X         
24 2-3   -  -            
25 2-5   Y  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

TOMMR UHPSQ JXWLM ESMTI WNMAK A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
