EFFECTIVE PERIOD:
13-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  A
02 1-0   -  B  B  -  -  B
03 1-0   C  -  -  -  C  C
04 1-0   -  -  D  D  -  -
05 1-0   E  E  -  -  E  E
06 1-0   -  -  F  F  F  F
07 1-0   G  -  G  -  -  -
08 1-0   -  H  -  -  -  -
09 2-0   -  -  -  I  I  I
10 2-0   -  -  J  J  -  -
11 2-0   -  K  K  -  -  -
12 2-0   L  -  L  L  -  L
13 2-0   M  -  M  -  -  M
14 2-0   N  -  N  N  N  N
15 2-0   O  O  O  O  -  -
16 2-0   P  P  -  P  P  P
17 2-0   -  -  -  Q  Q  -
18 2-0   R  R  -  R  -   
19 2-0   -  -  -  -  S   
20 0-4   -  T  -  -      
21 0-5   U  U  U  -      
22 0-5   V  V  V         
23 0-6   -  X  -         
24 1-2   -  Y            
25 1-5   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

VPPPE AURRX DMSNA XEKPC DQNOP Y
-------------------------------
