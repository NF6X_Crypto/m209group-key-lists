EFFECTIVE PERIOD:
16-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   B  -  B  B  B  -
03 1-0   -  C  -  -  -  C
04 1-0   D  -  -  D  -  D
05 2-0   -  -  -  E  E  E
06 0-3   F  -  F  -  F  -
07 0-4   G  -  -  -  -  G
08 0-4   -  H  -  -  -  -
09 0-4   -  -  I  I  -  -
10 0-4   -  -  J  J  J  -
11 0-4   K  -  -  -  -  K
12 0-5   -  L  L  L  -  -
13 0-5   -  -  -  -  -  -
14 0-5   N  -  N  -  -  -
15 0-5   O  O  O  -  -  O
16 0-5   P  P  P  -  -  -
17 0-5   Q  -  Q  Q  Q  -
18 0-5   R  R  R  R  R   
19 0-5   S  -  S  -  S   
20 1-3   -  T  -  T      
21 1-4   -  U  -  U      
22 2-3   -  V  -         
23 4-5   -  X  X         
24 4-5   X  -            
25 4-5   -  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JIHIN WOUZP OEXUF CFXNC JIBCU A
-------------------------------
