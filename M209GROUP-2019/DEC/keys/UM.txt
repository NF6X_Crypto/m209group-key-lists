EFFECTIVE PERIOD:
26-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  -  B  -  B
03 1-0   -  -  -  -  C  -
04 1-0   -  D  D  D  D  D
05 1-0   -  E  -  -  E  -
06 1-0   F  -  F  F  -  F
07 2-0   -  G  -  G  G  -
08 2-0   H  -  H  -  -  -
09 2-0   I  -  I  -  I  I
10 2-0   -  J  -  J  -  J
11 2-0   -  K  K  K  -  K
12 2-0   -  -  L  L  -  -
13 0-4   M  -  M  -  -  M
14 0-4   -  -  N  N  N  -
15 0-6   -  O  O  -  -  -
16 1-2   P  -  -  -  P  -
17 1-4   -  -  -  Q  Q  Q
18 1-4   R  R  R  -  -   
19 1-4   S  S  -  -  S   
20 1-4   -  -  T  T      
21 2-6   -  -  U  U      
22 3-4   V  -  V         
23 3-6   W  -  -         
24 4-5   -  Y            
25 4-6   Y  -            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

XLRRT ARSNY NAABS URNSU TKLSC U
-------------------------------
