EFFECTIVE PERIOD:
27-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: UN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   -  B  -  -  -  -
03 1-0   C  C  C  -  -  C
04 1-0   D  D  D  D  D  D
05 1-0   E  E  -  E  E  E
06 1-0   -  F  -  F  -  F
07 1-0   G  -  G  -  G  -
08 1-0   H  -  -  H  H  -
09 2-0   I  I  I  -  -  I
10 2-0   J  -  J  J  J  J
11 2-0   -  -  -  -  -  -
12 2-0   L  -  L  -  -  -
13 0-3   -  -  -  M  M  M
14 0-3   -  -  N  -  N  -
15 0-3   -  O  -  -  O  -
16 0-3   -  P  -  P  -  -
17 0-4   Q  -  Q  Q  Q  Q
18 0-4   -  R  -  -  -   
19 0-4   S  -  S  -  -   
20 0-4   T  T  T  -      
21 0-6   -  U  -  -      
22 1-4   V  -  -         
23 1-4   W  -  -         
24 2-3   X  Y            
25 2-6   Y  -            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

QQCZO LWMJW CHSYV RSKTW KMKIJ M
-------------------------------
