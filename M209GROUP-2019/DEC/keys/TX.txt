EFFECTIVE PERIOD:
11-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  B  B  -  -  -
03 1-0   C  -  -  C  -  C
04 1-0   D  -  -  -  -  D
05 1-0   -  E  E  E  -  E
06 2-0   -  -  F  -  F  F
07 2-0   -  -  -  G  G  -
08 0-3   H  -  -  -  -  -
09 0-3   -  I  -  -  -  I
10 0-3   J  -  -  -  -  -
11 0-3   K  -  -  K  K  -
12 0-3   L  L  -  -  L  L
13 0-3   M  M  M  M  -  M
14 0-3   -  N  N  -  N  -
15 0-3   O  -  O  O  O  -
16 0-3   -  -  P  -  -  -
17 0-4   -  -  Q  Q  -  Q
18 0-6   -  R  R  R  -   
19 0-6   S  -  -  S  S   
20 0-6   -  -  -  -      
21 0-6   -  U  -  U      
22 1-3   V  -  V         
23 1-3   -  X  X         
24 1-3   -  Y            
25 1-6   -  -            
26 2-5   Z               
27 2-6                   
-------------------------------
26 LETTER CHECK

NRTVM ARAQU NSQKS VZZOJ IKVAP S
-------------------------------
