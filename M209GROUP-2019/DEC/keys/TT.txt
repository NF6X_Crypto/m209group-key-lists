EFFECTIVE PERIOD:
07-DEC-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: TT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  -  A
02 2-0   -  B  B  B  -  B
03 2-0   -  C  C  -  C  -
04 2-0   D  D  D  D  -  -
05 2-0   E  -  E  E  -  E
06 0-3   -  F  F  -  -  -
07 0-3   G  G  -  -  G  G
08 0-3   -  H  H  -  H  -
09 0-3   I  -  -  I  -  -
10 0-3   J  -  -  -  -  J
11 0-3   -  K  K  -  -  -
12 0-6   -  L  -  L  -  L
13 0-6   -  M  -  M  M  -
14 0-6   N  -  N  N  N  N
15 0-6   -  -  O  -  -  O
16 0-6   -  -  -  -  -  -
17 0-6   -  -  -  -  Q  Q
18 1-5   R  R  -  R  R   
19 2-3   S  S  S  -  S   
20 2-3   -  -  -  T      
21 2-4   -  -  U  U      
22 2-6   -  -  V         
23 2-6   W  -  -         
24 2-6   -  -            
25 2-6   Y  -            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KVBTS LMVUB LZTZU KSUSA VULZU M
-------------------------------
