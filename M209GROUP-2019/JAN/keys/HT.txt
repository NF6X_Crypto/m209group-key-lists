EFFECTIVE PERIOD:
29-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 2-0   -  B  -  B  -  -
03 2-0   -  C  C  -  -  -
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  E  -  -
06 2-0   F  F  -  -  F  F
07 2-0   -  -  G  G  -  G
08 2-0   -  H  H  -  H  -
09 0-3   -  -  -  -  I  I
10 0-3   J  -  J  J  -  J
11 0-3   K  -  K  -  -  -
12 0-3   L  L  -  L  L  L
13 0-3   -  -  M  -  M  -
14 0-3   -  N  -  N  N  -
15 0-3   O  O  -  O  O  O
16 0-3   -  P  -  P  -  P
17 0-3   Q  Q  Q  Q  -  Q
18 0-3   R  -  -  -  -   
19 0-5   S  -  -  S  -   
20 0-5   -  T  T  -      
21 0-5   U  U  -  U      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 0-6   X  Y            
25 0-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MQORD LUKHG OZUZJ JAJKP RJADK I
-------------------------------
