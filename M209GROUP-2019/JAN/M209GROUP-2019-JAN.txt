SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF JAN 2019

    01 JAN 2019  00:00-23:59 GMT:  USE KEY GR
    02 JAN 2019  00:00-23:59 GMT:  USE KEY GS
    03 JAN 2019  00:00-23:59 GMT:  USE KEY GT
    04 JAN 2019  00:00-23:59 GMT:  USE KEY GU
    05 JAN 2019  00:00-23:59 GMT:  USE KEY GV
    06 JAN 2019  00:00-23:59 GMT:  USE KEY GW
    07 JAN 2019  00:00-23:59 GMT:  USE KEY GX
    08 JAN 2019  00:00-23:59 GMT:  USE KEY GY
    09 JAN 2019  00:00-23:59 GMT:  USE KEY GZ
    10 JAN 2019  00:00-23:59 GMT:  USE KEY HA
    11 JAN 2019  00:00-23:59 GMT:  USE KEY HB
    12 JAN 2019  00:00-23:59 GMT:  USE KEY HC
    13 JAN 2019  00:00-23:59 GMT:  USE KEY HD
    14 JAN 2019  00:00-23:59 GMT:  USE KEY HE
    15 JAN 2019  00:00-23:59 GMT:  USE KEY HF
    16 JAN 2019  00:00-23:59 GMT:  USE KEY HG
    17 JAN 2019  00:00-23:59 GMT:  USE KEY HH
    18 JAN 2019  00:00-23:59 GMT:  USE KEY HI
    19 JAN 2019  00:00-23:59 GMT:  USE KEY HJ
    20 JAN 2019  00:00-23:59 GMT:  USE KEY HK
    21 JAN 2019  00:00-23:59 GMT:  USE KEY HL
    22 JAN 2019  00:00-23:59 GMT:  USE KEY HM
    23 JAN 2019  00:00-23:59 GMT:  USE KEY HN
    24 JAN 2019  00:00-23:59 GMT:  USE KEY HO
    25 JAN 2019  00:00-23:59 GMT:  USE KEY HP
    26 JAN 2019  00:00-23:59 GMT:  USE KEY HQ
    27 JAN 2019  00:00-23:59 GMT:  USE KEY HR
    28 JAN 2019  00:00-23:59 GMT:  USE KEY HS
    29 JAN 2019  00:00-23:59 GMT:  USE KEY HT
    30 JAN 2019  00:00-23:59 GMT:  USE KEY HU
    31 JAN 2019  00:00-23:59 GMT:  USE KEY HV

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  A
02 2-0   -  B  -  B  B  -
03 2-0   -  -  C  C  -  C
04 2-0   -  -  -  D  D  D
05 2-0   -  E  E  E  -  -
06 2-0   F  F  -  F  -  -
07 0-3   -  -  -  -  G  G
08 0-3   H  H  -  -  -  H
09 0-3   -  I  I  I  I  I
10 0-3   -  -  -  -  -  J
11 0-4   K  -  K  -  -  -
12 0-5   L  L  L  L  -  L
13 0-5   -  -  -  M  M  M
14 0-5   -  -  N  -  -  -
15 0-5   O  -  O  -  O  O
16 0-5   -  -  -  P  P  -
17 0-5   -  Q  Q  Q  -  -
18 0-5   R  R  -  -  R   
19 0-6   -  S  S  -  -   
20 0-6   -  -  -  T      
21 0-6   U  -  U  U      
22 2-3   -  V  V         
23 2-5   W  -  -         
24 2-5   X  -            
25 2-5   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

HTOWV GJTSP LMPFU NVBSN RLQPK M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   -  -  -  -  -  B
03 1-0   -  -  C  C  C  -
04 1-0   D  D  D  D  -  -
05 1-0   -  -  E  E  -  E
06 1-0   F  -  -  -  F  F
07 1-0   -  G  -  -  -  -
08 1-0   -  H  H  H  -  H
09 1-0   -  I  I  I  I  I
10 1-0   -  -  J  J  -  -
11 0-3   -  K  -  K  K  -
12 0-3   L  L  -  -  L  L
13 0-3   M  M  -  -  M  -
14 0-4   -  N  N  N  -  -
15 0-6   O  -  O  O  O  -
16 0-6   -  -  -  -  -  P
17 0-6   -  -  Q  -  Q  Q
18 0-6   R  -  R  -  R   
19 0-6   S  -  S  S  S   
20 0-6   T  T  T  T      
21 0-6   -  U  -  -      
22 0-6   V  V  -         
23 0-6   -  X  -         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-3   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

EQKON PACMJ LLPFZ KLFQF ABXAE V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  A  -  -
02 0-3   -  -  B  B  -  B
03 0-3   -  C  -  -  C  C
04 0-3   -  D  D  D  -  D
05 0-3   E  E  -  E  E  E
06 0-3   -  -  F  -  -  F
07 0-4   -  -  G  G  G  G
08 0-4   -  H  H  H  -  -
09 0-4   -  I  -  -  I  -
10 0-4   J  J  -  -  -  -
11 0-4   K  K  K  K  K  -
12 0-4   L  -  -  L  -  L
13 0-5   -  M  -  -  M  -
14 0-5   -  N  N  -  N  -
15 0-5   O  -  O  -  -  -
16 0-5   P  -  P  -  P  -
17 0-5   Q  -  -  Q  -  Q
18 0-5   -  -  -  -  -   
19 0-5   S  -  -  -  S   
20 1-5   -  T  T  T      
21 2-3   U  U  U  U      
22 2-6   -  V  -         
23 3-4   W  X  X         
24 4-5   -  Y            
25 4-5   Y  -            
26 4-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

IUTAS CIAMT AJTSN TZHNM ANAUI T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   -  -  B  -  -  B
03 2-0   C  -  -  C  C  -
04 2-0   D  D  -  -  D  -
05 2-0   E  -  E  -  -  E
06 2-0   -  -  -  F  -  F
07 0-3   -  -  -  -  G  G
08 0-3   -  -  -  H  H  -
09 0-6   I  -  I  I  I  I
10 0-6   J  J  J  J  -  -
11 0-6   K  -  -  K  -  -
12 0-6   -  L  L  -  -  L
13 0-6   M  -  -  M  M  M
14 0-6   N  N  N  N  -  -
15 0-6   O  -  O  -  O  -
16 1-3   P  -  P  -  P  -
17 1-3   -  Q  Q  -  Q  -
18 1-3   R  -  -  R  -   
19 1-4   -  S  S  -  S   
20 1-6   T  T  -  -      
21 2-3   U  -  U  U      
22 2-3   V  V  V         
23 2-3   -  X  X         
24 2-3   -  Y            
25 2-6   Y  -            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

NUAKQ TFYZT AZINT OCBUV QNMNA V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 1-0   -  -  B  B  -  B
03 1-0   -  -  C  -  C  C
04 1-0   D  D  -  D  -  D
05 2-0   E  E  E  E  -  -
06 2-0   F  -  F  F  F  -
07 2-0   -  G  -  -  G  -
08 2-0   H  -  H  -  H  -
09 2-0   -  I  I  -  I  I
10 2-0   -  -  -  J  J  J
11 0-4   K  -  K  K  K  K
12 0-5   L  L  -  -  -  -
13 0-5   -  M  -  M  -  -
14 0-5   N  -  N  -  -  N
15 0-5   -  -  -  -  O  -
16 0-5   P  -  P  -  -  -
17 0-5   -  Q  Q  -  -  Q
18 0-5   -  R  -  R  R   
19 0-5   -  -  -  S  S   
20 0-5   T  -  T  -      
21 0-5   -  U  U  -      
22 0-5   -  V  -         
23 0-5   W  X  -         
24 0-6   X  -            
25 0-6   Y  -            
26 1-6   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

IZGVE AWXZH QABMY ZTVSG DFWJE P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 1-0   B  B  B  B  -  -
03 1-0   -  C  -  C  C  C
04 1-0   D  -  D  D  D  -
05 1-0   E  E  E  -  -  -
06 1-0   F  -  -  F  -  -
07 1-0   G  G  -  G  G  -
08 2-0   H  H  H  -  -  -
09 2-0   -  -  I  -  -  I
10 2-0   -  -  J  -  -  J
11 2-0   -  -  -  K  -  -
12 0-5   -  -  -  -  -  L
13 0-5   M  -  -  M  M  -
14 0-5   -  N  N  N  -  -
15 0-5   -  O  O  O  O  O
16 0-5   P  -  -  P  P  -
17 0-6   -  -  Q  -  -  Q
18 0-6   R  -  R  -  R   
19 0-6   S  -  -  S  -   
20 1-4   -  T  -  -      
21 1-6   U  U  -  -      
22 1-6   V  -  -         
23 1-6   W  X  X         
24 1-6   -  -            
25 2-3   Y  -            
26 2-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

RPLTO RAPLQ IIRFT VPRVB GIHWO L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  B  -  -  B  -
03 1-0   -  -  -  C  C  -
04 2-0   D  D  -  D  D  D
05 0-4   -  -  E  E  -  E
06 0-5   F  F  -  F  F  -
07 0-5   G  -  G  -  G  G
08 0-5   H  -  H  H  H  H
09 0-5   I  -  I  -  I  -
10 0-5   J  J  J  -  J  J
11 0-5   K  K  -  -  K  -
12 0-5   -  -  L  -  -  -
13 0-5   -  M  M  M  -  -
14 0-5   -  -  N  -  -  N
15 0-5   -  O  -  O  -  O
16 0-5   -  -  P  P  -  P
17 0-5   Q  Q  -  -  -  Q
18 0-6   R  R  R  -  R   
19 0-6   -  -  -  -  -   
20 0-6   -  -  -  T      
21 0-6   -  -  -  U      
22 0-6   V  V  -         
23 0-6   -  -  X         
24 1-3   -  Y            
25 1-6   Y  -            
26 2-3   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

DEHKJ HXKAU XWHUA MASSF NOJLI P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  B  B  -  -  -
03 1-0   -  -  C  C  C  C
04 1-0   D  -  D  -  -  D
05 1-0   -  E  E  E  -  -
06 0-4   -  -  -  -  F  -
07 0-4   G  G  -  G  G  -
08 0-4   H  H  H  H  H  H
09 0-4   I  I  -  -  -  I
10 0-4   -  -  -  -  J  J
11 0-5   -  -  K  -  -  K
12 0-5   -  L  -  L  L  L
13 0-5   M  -  M  -  M  -
14 0-5   N  N  -  N  -  -
15 0-5   O  O  -  O  O  -
16 1-2   -  -  P  P  -  P
17 1-4   -  -  Q  Q  Q  Q
18 2-5   R  -  -  R  -   
19 2-5   S  S  S  -  S   
20 2-6   -  -  -  -      
21 3-6   -  -  U  U      
22 4-5   -  -  -         
23 4-5   -  X  -         
24 4-5   -  Y            
25 4-5   Y  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

PUQZN UDNNM PKKTV VNVMU TZVTZ T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  -  A  A
02 0-3   B  B  -  B  B  -
03 0-3   -  C  -  C  -  -
04 0-3   -  D  -  -  -  D
05 0-4   E  E  E  E  -  E
06 0-4   -  F  F  F  F  F
07 0-4   -  -  G  G  -  -
08 0-4   H  -  -  -  -  -
09 0-5   -  I  -  -  -  -
10 0-5   J  J  -  J  J  -
11 0-5   K  K  -  K  -  K
12 0-5   L  L  L  L  L  L
13 0-5   M  -  -  M  M  M
14 0-5   -  N  N  -  N  N
15 0-5   O  O  -  O  -  -
16 1-3   -  P  P  -  -  -
17 1-3   Q  Q  Q  -  Q  Q
18 1-4   -  -  R  R  R   
19 1-6   S  -  -  -  -   
20 2-3   T  -  T  T      
21 3-4   U  -  -  -      
22 3-4   V  -  V         
23 3-5   -  -  X         
24 3-5   X  Y            
25 3-5   -  Z            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

AQAOO VWIWU JIETS TMMKU WAETN W
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  A  A  A  -  -
02 0-3   B  B  -  -  B  B
03 0-3   -  C  C  -  C  C
04 0-3   D  -  -  D  D  -
05 0-5   E  -  E  E  E  E
06 0-5   -  -  -  -  -  F
07 0-5   G  G  G  G  G  G
08 0-5   -  H  -  H  -  -
09 0-5   I  -  -  -  I  I
10 0-5   -  -  -  J  J  J
11 0-5   K  -  -  -  -  K
12 0-6   L  L  L  -  -  -
13 0-6   -  M  M  M  -  -
14 0-6   N  -  -  N  N  N
15 0-6   -  O  -  -  -  O
16 1-2   P  -  P  P  -  -
17 1-3   Q  Q  -  -  Q  -
18 2-3   -  R  R  -  R   
19 2-3   S  S  -  -  -   
20 2-6   -  T  T  -      
21 3-4   -  -  U  U      
22 3-5   V  -  V         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-5   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

LCWLT PVMTV UVAKV JZVEB GAODO A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  -  B  -  B  B
03 1-0   -  -  -  -  -  -
04 1-0   -  D  D  D  -  D
05 2-0   -  -  -  -  E  E
06 2-0   F  -  -  F  -  -
07 2-0   -  -  G  G  -  -
08 2-0   -  -  -  H  H  H
09 2-0   I  I  I  -  I  -
10 0-4   -  J  -  -  -  J
11 0-4   K  -  K  K  K  -
12 0-4   -  -  L  -  L  -
13 0-4   -  M  -  -  M  -
14 0-4   -  N  N  -  -  -
15 0-5   -  -  -  O  -  -
16 0-5   -  -  P  -  P  P
17 0-5   Q  Q  -  Q  Q  -
18 1-2   R  -  R  -  -   
19 1-2   -  S  -  S  -   
20 1-2   -  T  T  T      
21 1-2   U  -  -  U      
22 1-3   -  -  -         
23 1-3   W  X  X         
24 2-4   -  -            
25 3-5   Y  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

HOXZW VKRQP KRUDQ KVZHZ RLAWP K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   B  -  -  -  -  B
03 1-0   C  C  C  -  -  C
04 2-0   D  D  D  -  D  D
05 2-0   -  E  -  E  E  E
06 2-0   F  -  F  -  F  -
07 2-0   -  -  -  G  -  G
08 0-4   -  H  -  -  -  -
09 0-4   I  -  I  I  -  I
10 0-4   -  -  -  J  -  -
11 0-4   -  -  K  -  K  -
12 0-4   L  -  -  -  -  L
13 0-5   M  M  M  -  M  -
14 0-5   N  N  N  N  N  N
15 0-5   -  -  O  O  O  -
16 1-2   -  P  P  -  -  -
17 1-5   Q  Q  -  -  Q  Q
18 1-6   -  -  -  R  -   
19 2-4   S  S  -  S  S   
20 3-6   -  T  -  T      
21 4-5   U  U  U  U      
22 4-5   V  -  V         
23 4-5   W  -  -         
24 4-5   X  Y            
25 5-6   -  Z            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

AAWQF ZXWTK KMQAK TFJXF MKWXV E
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  A  -  A
02 2-0   B  B  B  B  B  B
03 2-0   C  -  -  -  C  C
04 2-0   -  -  D  -  D  -
05 2-0   -  E  E  E  E  -
06 2-0   -  -  -  -  -  F
07 2-0   -  -  -  G  G  G
08 2-0   -  -  H  -  -  H
09 2-0   -  I  -  I  I  -
10 0-3   J  -  -  J  J  J
11 0-3   -  -  -  K  -  K
12 0-4   L  -  -  -  L  L
13 0-5   -  -  M  -  M  -
14 0-5   N  -  N  -  -  -
15 0-5   -  O  O  -  O  O
16 0-5   P  -  P  -  -  -
17 0-5   -  Q  Q  -  -  -
18 0-5   R  R  -  R  -   
19 0-6   -  S  -  S  S   
20 0-6   -  T  -  T      
21 0-6   U  U  U  U      
22 0-6   -  V  -         
23 0-6   W  -  X         
24 1-3   -  Y            
25 2-5   Y  -            
26 3-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

ZQNTP LJAHA NTXLQ JNRHQ JGMDK S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 2-0   B  B  B  -  B  -
03 0-3   C  C  C  C  -  C
04 0-3   -  D  -  -  D  D
05 0-3   -  E  -  E  -  -
06 0-3   -  -  F  F  F  F
07 0-4   G  G  -  G  -  -
08 0-4   -  -  -  -  H  -
09 0-4   I  -  I  -  -  -
10 0-4   J  J  J  -  J  J
11 0-4   -  -  K  K  -  K
12 0-4   -  -  L  -  -  L
13 0-4   M  M  M  M  -  -
14 0-4   N  -  -  N  N  -
15 0-4   O  -  -  -  O  O
16 0-4   P  -  P  -  -  -
17 0-4   Q  Q  -  -  -  -
18 0-4   -  -  R  R  -   
19 0-5   -  -  -  S  S   
20 0-5   T  -  -  -      
21 0-5   U  U  -  U      
22 0-5   -  V  V         
23 0-5   W  X  -         
24 0-5   -  -            
25 0-6   -  -            
26 1-3   Z               
27 1-6                   
-------------------------------
26 LETTER CHECK

PPLFH OMKXH DXIVM XBKGS HYWDD G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   B  -  -  B  -  -
03 2-0   -  C  C  -  C  -
04 0-3   -  -  D  D  D  D
05 0-4   E  -  -  -  E  E
06 0-4   F  F  F  F  F  F
07 0-4   -  -  G  -  -  -
08 0-4   -  -  H  H  -  H
09 0-4   I  -  -  I  I  I
10 0-4   J  -  -  J  J  J
11 0-4   K  -  -  K  -  -
12 0-4   L  L  L  -  -  -
13 0-4   -  M  M  -  -  -
14 0-4   N  N  N  N  -  N
15 0-4   -  -  -  O  O  O
16 0-4   -  P  -  P  P  P
17 0-5   Q  Q  -  -  -  -
18 0-5   R  R  R  R  R   
19 0-6   -  -  S  -  S   
20 0-6   -  T  -  T      
21 0-6   -  U  U  -      
22 0-6   V  -  -         
23 0-6   -  -  X         
24 2-3   -  -            
25 2-5   -  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

BRRYL XHSZB ZMYHS JASMN HJAUR D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  A  A
02 1-0   -  -  -  -  B  -
03 1-0   -  -  C  -  C  -
04 1-0   -  -  -  -  -  -
05 2-0   E  -  -  -  -  E
06 2-0   -  -  -  F  -  F
07 2-0   G  G  -  G  G  G
08 2-0   -  -  -  -  -  -
09 0-3   -  -  I  I  I  -
10 0-3   J  J  J  J  -  J
11 0-3   K  K  -  K  -  K
12 0-3   -  -  L  -  L  L
13 0-3   M  -  -  M  -  -
14 0-3   -  N  -  N  N  N
15 0-6   -  -  -  -  O  -
16 0-6   P  -  P  P  -  -
17 0-6   -  -  -  Q  Q  -
18 1-2   R  R  -  R  -   
19 1-4   -  S  S  -  -   
20 1-6   T  T  T  -      
21 1-6   -  U  U  U      
22 1-6   V  -  V         
23 1-6   -  -  X         
24 2-3   X  -            
25 2-4   Y  Z            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

PKSQQ PWOTS OOOHC LIURK TXCJQ K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  A
02 0-3   B  -  B  B  B  -
03 0-3   C  C  -  C  C  -
04 0-3   D  D  -  D  -  D
05 0-4   E  -  E  E  E  E
06 0-4   -  F  F  F  -  -
07 0-4   G  G  G  G  G  -
08 0-4   H  -  H  -  H  H
09 0-4   -  -  I  I  -  I
10 0-4   J  -  -  -  J  J
11 0-4   -  -  K  -  -  -
12 0-4   -  L  -  -  L  L
13 0-5   -  M  -  -  -  -
14 0-5   N  N  N  N  -  -
15 0-5   -  -  -  O  O  -
16 0-5   P  P  P  -  -  -
17 0-5   -  -  Q  -  Q  Q
18 0-6   -  R  -  R  -   
19 0-6   -  S  -  -  -   
20 1-2   T  -  -  -      
21 2-6   U  -  U  U      
22 3-4   -  V  -         
23 3-4   -  -  X         
24 3-4   -  Y            
25 3-4   Y  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

ORPQC TDNLQ YUTOA ZNYMG IZVVL H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  -  A
02 1-0   -  -  B  B  -  B
03 1-0   C  -  -  -  -  C
04 0-3   -  -  D  D  D  -
05 0-3   E  E  -  E  E  E
06 0-3   F  F  F  F  -  -
07 0-3   G  G  -  -  G  -
08 0-3   H  H  H  -  H  H
09 0-3   -  -  -  I  -  -
10 0-3   -  -  -  -  -  J
11 0-3   -  K  -  -  K  -
12 0-3   -  -  -  L  L  L
13 0-4   M  M  M  M  -  -
14 0-5   -  -  -  N  -  N
15 0-6   O  -  O  -  O  -
16 0-6   -  -  P  P  P  P
17 0-6   Q  -  Q  -  -  -
18 0-6   -  R  -  -  R   
19 0-6   S  S  S  S  S   
20 0-6   -  T  -  -      
21 0-6   -  -  -  -      
22 0-6   V  V  V         
23 0-6   -  -  -         
24 0-6   X  Y            
25 0-6   Y  -            
26 1-2   -               
27 2-5                   
-------------------------------
26 LETTER CHECK

CYIAC VGNXP AZCNN OPOQB LCWFU Z
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  A
02 2-0   -  B  -  -  -  -
03 2-0   C  -  -  C  C  -
04 0-3   -  -  D  -  -  -
05 0-3   -  E  E  -  -  -
06 0-3   -  -  F  F  -  F
07 0-3   -  -  -  G  -  G
08 0-3   -  -  -  H  H  H
09 0-3   I  I  -  -  I  -
10 0-6   J  -  J  -  -  J
11 0-6   -  -  K  K  -  -
12 0-6   -  -  L  L  -  L
13 0-6   -  -  M  M  M  M
14 0-6   -  N  -  -  -  -
15 0-6   O  O  -  -  O  O
16 1-2   P  P  P  P  P  -
17 2-4   Q  Q  -  Q  Q  Q
18 2-5   R  R  -  -  R   
19 2-5   S  -  S  S  S   
20 2-5   -  -  -  -      
21 2-6   U  -  U  -      
22 2-6   V  V  V         
23 2-6   W  X  -         
24 2-6   X  -            
25 3-5   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

MUZNQ MRAUB MMSST UQZKA NTLPU U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   B  -  -  -  -  B
03 1-0   -  C  C  -  -  -
04 1-0   D  D  -  -  D  D
05 2-0   E  -  -  -  -  -
06 2-0   -  F  -  F  F  F
07 2-0   -  G  G  -  G  -
08 2-0   -  H  H  -  -  H
09 2-0   I  I  I  I  I  -
10 0-5   -  -  -  J  -  -
11 0-6   -  K  -  K  K  -
12 0-6   -  L  -  -  -  L
13 0-6   M  -  -  M  -  M
14 0-6   N  -  N  N  -  N
15 0-6   -  -  -  -  -  -
16 1-3   -  -  P  -  P  P
17 1-5   Q  -  -  Q  Q  Q
18 1-5   R  -  R  -  -   
19 1-5   -  S  S  S  S   
20 1-6   -  -  -  T      
21 1-6   U  U  U  U      
22 1-6   V  -  V         
23 1-6   W  -  -         
24 2-5   X  -            
25 2-6   Y  Z            
26 3-4   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

CSTJN VYPVM SOTLA LNJWY JHQVO M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  -
02 2-0   B  B  B  -  -  -
03 2-0   C  C  C  -  -  C
04 2-0   -  -  D  -  D  -
05 2-0   -  -  E  -  E  E
06 0-3   -  F  -  F  -  -
07 0-3   G  -  -  -  G  -
08 0-3   -  -  -  -  H  H
09 0-3   I  I  -  I  -  -
10 0-3   J  J  -  -  -  J
11 0-4   -  K  K  -  K  K
12 0-5   -  -  L  L  -  -
13 0-5   M  M  M  M  -  -
14 0-5   -  -  N  N  -  N
15 0-5   -  -  O  -  -  O
16 0-5   P  P  -  -  -  P
17 0-5   -  Q  Q  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   S  -  -  S  -   
20 0-6   -  -  -  T      
21 0-6   U  -  -  -      
22 2-3   V  V  V         
23 2-5   -  -  -         
24 2-5   X  -            
25 2-5   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

NPIMG QQJQH GZUOV INHII UTMTP K
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  A  -
02 1-0   B  -  B  -  -  -
03 1-0   C  -  C  C  C  C
04 1-0   -  D  -  D  D  D
05 2-0   E  E  -  E  E  E
06 2-0   -  -  -  -  -  F
07 2-0   -  G  G  -  -  G
08 0-3   H  -  H  -  -  H
09 0-3   -  I  -  -  -  -
10 0-3   -  -  J  J  J  J
11 0-3   K  -  -  K  K  K
12 0-3   L  -  -  L  L  -
13 0-3   -  -  M  -  -  -
14 0-4   N  N  N  -  -  -
15 0-5   -  O  O  O  -  O
16 0-5   P  P  -  P  -  -
17 0-5   Q  -  Q  -  Q  -
18 0-5   -  -  R  R  R   
19 0-5   -  -  S  -  S   
20 0-5   -  T  T  -      
21 0-5   U  U  -  U      
22 0-5   V  V  -         
23 0-5   -  X  X         
24 1-2   -  -            
25 1-3   Y  -            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

ZQAII AZOPN QSXHE JIVQW AGMIA O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   B  -  -  -  B  -
03 1-0   C  C  C  -  -  -
04 1-0   -  D  -  -  -  D
05 1-0   E  E  -  E  -  -
06 2-0   F  F  F  F  -  F
07 0-4   G  -  G  -  -  G
08 0-4   -  -  H  H  H  -
09 0-4   -  I  -  -  -  -
10 0-4   J  J  -  -  J  J
11 0-5   K  -  -  K  K  K
12 0-5   -  -  L  -  L  -
13 0-5   -  M  M  M  -  M
14 0-5   N  N  N  -  N  N
15 0-5   -  O  -  O  O  -
16 1-4   -  -  P  -  -  P
17 1-5   -  Q  Q  Q  Q  Q
18 1-5   -  -  -  R  R   
19 1-5   -  -  -  S  -   
20 1-5   T  -  -  T      
21 2-4   -  U  -  U      
22 2-5   -  -  V         
23 2-5   W  -  X         
24 2-6   X  -            
25 3-6   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

VZVOO UVSYT NENVJ WVUMV RYOMW V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 1-0   B  -  -  B  B  -
03 1-0   -  C  C  C  C  -
04 2-0   -  -  D  D  D  D
05 2-0   E  E  E  -  -  -
06 2-0   F  F  F  -  -  F
07 2-0   -  -  G  G  -  -
08 0-3   -  -  -  H  -  -
09 0-3   I  I  -  I  -  I
10 0-3   J  J  J  J  J  -
11 0-3   K  K  -  -  K  -
12 0-3   -  -  -  L  L  L
13 0-3   M  -  M  M  M  -
14 0-4   -  N  N  -  N  N
15 0-4   O  -  O  -  -  O
16 0-4   P  P  -  P  P  P
17 0-4   -  Q  Q  -  -  Q
18 1-3   R  -  -  R  R   
19 1-5   S  -  -  -  -   
20 2-4   -  -  -  T      
21 2-4   U  U  U  -      
22 2-4   V  V  -         
23 2-4   W  X  -         
24 2-5   -  -            
25 2-5   -  -            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

WTPUD OPAJD WQURH AZMMW UUIOW H
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  A  -
02 2-0   B  -  B  -  B  B
03 2-0   -  C  -  -  -  -
04 2-0   D  -  -  -  -  D
05 2-0   E  E  -  -  E  -
06 2-0   -  -  F  F  F  -
07 0-3   -  -  G  G  -  G
08 0-4   -  H  -  H  H  -
09 0-4   -  -  I  -  I  I
10 0-5   J  J  -  J  -  J
11 0-5   K  -  -  K  K  -
12 0-5   -  -  -  -  L  L
13 0-5   -  -  M  M  -  M
14 0-5   -  N  N  N  N  -
15 0-5   O  O  O  -  O  -
16 0-5   P  -  -  -  -  P
17 0-5   -  -  Q  Q  -  -
18 0-6   -  R  R  -  -   
19 0-6   S  -  -  -  -   
20 0-6   T  T  -  -      
21 0-6   U  U  U  -      
22 1-4   V  V  -         
23 2-5   -  -  -         
24 2-5   X  Y            
25 2-5   -  Z            
26 2-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

WJQTF PKJSU PCMVE AUZZT KPUAJ R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  -
02 1-0   -  -  -  -  B  -
03 2-0   -  -  -  C  C  C
04 2-0   D  -  -  D  D  -
05 2-0   E  E  E  -  -  E
06 2-0   -  -  -  -  F  -
07 2-0   -  -  G  G  -  G
08 2-0   H  H  -  H  H  H
09 0-4   I  I  I  I  -  -
10 0-4   J  -  -  -  -  J
11 0-4   K  -  -  -  -  K
12 0-4   L  -  L  L  L  -
13 0-4   -  M  -  -  -  -
14 0-4   -  -  -  -  N  N
15 0-4   O  -  O  O  -  O
16 1-2   -  P  -  P  P  -
17 1-2   -  -  Q  Q  Q  -
18 1-2   -  R  R  R  -   
19 1-2   S  S  S  -  S   
20 1-3   T  T  -  T      
21 1-4   U  -  -  -      
22 1-5   -  -  -         
23 1-6   W  -  X         
24 1-6   -  Y            
25 2-4   -  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

EMKAA AZTZY CAJEM ZNMSR UMKUM T
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  -  -  -  -  -
03 1-0   -  -  -  C  -  C
04 1-0   D  -  -  -  D  D
05 1-0   E  -  E  -  E  -
06 0-3   -  F  -  F  -  -
07 0-3   G  G  -  -  -  -
08 0-3   -  -  H  H  -  H
09 0-3   I  I  I  I  -  I
10 0-3   J  J  J  J  -  J
11 0-4   -  -  -  -  -  K
12 0-4   -  L  L  -  L  -
13 0-4   M  -  -  -  M  -
14 0-4   N  -  N  -  -  N
15 0-4   O  O  O  O  O  -
16 0-4   -  -  P  P  -  -
17 0-5   -  -  -  -  Q  Q
18 1-4   -  -  R  -  R   
19 1-5   S  S  S  -  S   
20 2-3   -  T  T  T      
21 3-4   U  U  -  -      
22 3-4   -  -  -         
23 3-4   -  -  X         
24 3-4   X  Y            
25 3-5   -  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

OOSUL UOOGT BUDZZ INZSU LLEZA U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  -  B  B  -  B
03 1-0   C  C  -  C  -  -
04 1-0   -  D  -  D  D  D
05 1-0   E  E  -  E  E  -
06 1-0   -  -  -  -  -  F
07 0-3   -  G  -  -  G  -
08 0-3   -  H  H  H  H  H
09 0-3   -  -  I  -  -  -
10 0-3   -  -  J  -  -  J
11 0-4   K  K  -  K  K  -
12 0-4   L  -  -  L  L  -
13 0-4   M  M  -  -  -  M
14 0-4   N  N  N  N  -  N
15 0-5   -  -  O  -  -  -
16 0-5   P  -  P  -  -  -
17 0-5   -  Q  Q  -  Q  Q
18 0-5   -  R  R  R  R   
19 0-6   S  -  -  S  S   
20 1-2   -  -  T  -      
21 1-5   U  U  -  U      
22 1-5   -  -  V         
23 1-5   W  -  X         
24 1-5   -  -            
25 3-4   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VKHRF UUPLV HZPQL PHGPT HKQRV P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  A
02 2-0   -  B  -  B  -  -
03 2-0   -  C  C  -  -  -
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  E  -  -
06 2-0   F  F  -  -  F  F
07 2-0   -  -  G  G  -  G
08 2-0   -  H  H  -  H  -
09 0-3   -  -  -  -  I  I
10 0-3   J  -  J  J  -  J
11 0-3   K  -  K  -  -  -
12 0-3   L  L  -  L  L  L
13 0-3   -  -  M  -  M  -
14 0-3   -  N  -  N  N  -
15 0-3   O  O  -  O  O  O
16 0-3   -  P  -  P  -  P
17 0-3   Q  Q  Q  Q  -  Q
18 0-3   R  -  -  -  -   
19 0-5   S  -  -  S  -   
20 0-5   -  T  T  -      
21 0-5   U  U  -  U      
22 0-5   -  V  -         
23 0-5   -  -  -         
24 0-6   X  Y            
25 0-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MQORD LUKHG OZUZJ JAJKP RJADK I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  -  -
02 2-0   -  -  B  B  B  -
03 2-0   -  C  C  -  -  C
04 2-0   D  -  -  D  -  D
05 2-0   -  E  E  E  -  -
06 0-3   -  -  -  -  F  F
07 0-3   -  -  G  -  G  -
08 0-3   -  -  H  -  H  H
09 0-3   I  I  I  -  I  -
10 0-3   J  J  J  -  J  J
11 0-3   -  K  -  K  -  -
12 0-3   L  -  -  L  -  -
13 0-6   M  -  -  -  M  -
14 0-6   -  N  -  -  -  N
15 0-6   O  -  O  O  O  O
16 1-3   P  P  -  -  -  P
17 1-5   -  -  -  -  -  Q
18 1-6   R  R  -  R  R   
19 1-6   S  -  S  -  S   
20 2-3   T  -  T  T      
21 2-6   U  -  -  U      
22 2-6   -  V  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 4-5   -  -            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

DVETM VVNOA NRUNV NVMIV TQNNR G
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-JAN-2019 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  A
02 1-0   -  -  -  B  B  B
03 1-0   -  -  C  C  C  -
04 1-0   D  -  -  -  -  -
05 1-0   -  -  -  -  -  -
06 1-0   -  -  -  F  F  F
07 1-0   G  G  -  -  G  G
08 2-0   H  -  -  H  -  H
09 2-0   I  I  I  I  I  I
10 2-0   -  J  -  J  -  J
11 2-0   -  K  K  K  -  -
12 2-0   -  -  L  L  L  L
13 0-3   M  -  -  -  M  -
14 0-3   N  N  N  -  -  -
15 0-3   O  -  O  -  O  -
16 0-4   P  P  -  P  P  P
17 0-5   Q  Q  -  -  -  -
18 0-5   R  R  R  -  R   
19 0-5   -  S  S  S  -   
20 0-5   -  -  -  T      
21 0-6   U  U  U  -      
22 1-5   -  V  -         
23 1-5   -  X  X         
24 1-5   X  -            
25 2-3   -  Z            
26 2-5   Z               
27 3-4                   
-------------------------------
26 LETTER CHECK

TZQPR PPMVN WBFIR AKSQW SKITW P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
