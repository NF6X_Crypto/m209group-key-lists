EFFECTIVE PERIOD:
11-OCT-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  -
02 1-0   -  -  -  -  B  -
03 2-0   -  C  -  C  -  -
04 2-0   -  D  D  -  -  -
05 2-0   E  E  -  -  E  -
06 0-3   -  -  F  F  -  -
07 0-3   G  G  G  -  G  G
08 0-3   H  -  H  H  H  -
09 0-3   I  -  -  -  -  I
10 0-3   -  J  J  J  -  J
11 0-3   K  -  K  -  -  K
12 0-5   L  L  -  L  L  L
13 0-6   -  -  M  M  -  -
14 0-6   -  -  N  N  -  N
15 0-6   O  -  -  -  O  O
16 0-6   P  -  -  -  P  P
17 0-6   -  -  Q  -  -  Q
18 0-6   -  R  -  -  R   
19 0-6   S  -  -  S  -   
20 1-3   T  T  T  -      
21 1-5   U  U  U  -      
22 2-3   -  V  -         
23 2-6   -  X  X         
24 2-6   X  Y            
25 2-6   -  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

XLZAQ SYOLR ZMCVK ATYPM WPAVZ F
-------------------------------
