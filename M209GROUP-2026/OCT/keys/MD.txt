EFFECTIVE PERIOD:
17-OCT-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MD
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  -  -  B  -  -
03 1-0   C  C  -  -  -  C
04 1-0   -  -  -  -  D  -
05 2-0   -  E  -  -  E  -
06 2-0   F  -  F  -  F  -
07 2-0   G  G  -  -  -  -
08 2-0   -  -  H  H  -  -
09 2-0   I  I  I  I  I  I
10 0-3   J  J  -  J  J  -
11 0-3   K  K  K  K  K  K
12 0-3   L  -  L  -  -  L
13 0-3   -  M  M  M  M  -
14 0-3   N  -  N  N  N  N
15 0-4   O  -  O  -  -  O
16 0-4   -  P  -  P  -  -
17 0-4   Q  Q  Q  Q  -  -
18 1-3   -  -  R  -  R   
19 1-4   -  -  -  -  S   
20 2-3   T  T  -  T      
21 2-3   -  -  -  -      
22 2-3   -  -  V         
23 2-3   W  -  X         
24 2-5   -  Y            
25 2-5   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PAMPT KDMNR UMJSZ RMARU XASQT M
-------------------------------
