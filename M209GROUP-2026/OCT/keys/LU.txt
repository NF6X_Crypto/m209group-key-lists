EFFECTIVE PERIOD:
08-OCT-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  A
02 1-0   B  B  B  B  -  B
03 1-0   -  -  C  C  C  C
04 0-3   D  D  -  D  -  -
05 0-3   E  E  -  -  -  -
06 0-3   -  -  -  -  -  -
07 0-3   -  -  -  G  G  G
08 0-3   H  -  H  H  H  -
09 0-5   -  I  I  -  -  I
10 0-5   -  J  J  J  J  -
11 0-5   K  -  K  -  K  K
12 0-5   -  L  L  -  -  L
13 0-5   -  -  -  M  -  -
14 0-5   N  N  -  N  N  -
15 0-5   O  O  -  O  -  O
16 0-6   P  -  P  P  -  -
17 0-6   -  -  -  -  Q  -
18 0-6   R  R  R  -  R   
19 0-6   S  S  -  S  -   
20 1-4   -  T  -  -      
21 1-6   U  -  U  -      
22 2-5   V  V  V         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-5   -  Z            
26 3-5   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

KGWFP QQPAG QLPFQ VAWAK KABAV A
-------------------------------
