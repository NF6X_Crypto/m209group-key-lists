EFFECTIVE PERIOD:
22-OCT-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  A  -
02 1-0   B  B  B  B  B  B
03 2-0   C  -  -  -  C  -
04 0-3   -  -  D  -  -  -
05 0-3   -  E  E  -  E  -
06 0-3   -  -  F  -  -  F
07 0-3   -  G  -  G  -  -
08 0-3   H  -  -  H  -  -
09 0-3   I  -  -  -  -  I
10 0-3   J  J  -  -  -  J
11 0-3   -  -  K  -  K  -
12 0-3   L  L  L  L  -  L
13 0-3   M  -  -  M  -  M
14 0-4   N  N  N  -  N  N
15 0-4   -  O  -  O  O  O
16 0-4   -  P  -  P  -  P
17 0-4   Q  Q  -  Q  Q  Q
18 0-4   -  -  R  -  -   
19 0-4   S  -  -  -  S   
20 0-4   -  T  T  -      
21 0-4   -  U  -  U      
22 0-5   -  -  V         
23 0-6   -  -  -         
24 1-4   X  Y            
25 1-5   -  Z            
26 3-4   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

UGOPV ENCZD ZOHOK WLAGF ZXHYP M
-------------------------------
