EFFECTIVE PERIOD:
29-OCT-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  -  -  -  -
02 0-3   B  -  B  -  -  -
03 0-3   -  -  -  C  C  C
04 0-3   -  -  -  -  D  -
05 0-3   E  -  -  -  E  E
06 0-3   -  F  -  F  -  F
07 0-4   -  -  -  G  G  -
08 0-4   -  -  -  H  -  -
09 0-4   I  -  I  -  I  I
10 0-4   -  J  -  J  -  -
11 0-4   K  -  -  K  -  K
12 0-4   -  L  L  -  -  L
13 0-5   M  -  -  -  M  M
14 0-5   N  N  N  N  -  -
15 0-5   -  O  O  O  O  -
16 1-2   P  -  -  -  P  P
17 1-3   -  Q  -  Q  -  -
18 1-5   R  -  R  R  -   
19 2-5   -  S  S  -  S   
20 3-4   T  -  T  -      
21 3-5   -  -  U  U      
22 3-5   V  V  V         
23 4-5   -  X  -         
24 4-5   X  Y            
25 4-5   Y  Z            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

NZPLA ULUWE MZNZS OTAUO MLTAM M
-------------------------------
