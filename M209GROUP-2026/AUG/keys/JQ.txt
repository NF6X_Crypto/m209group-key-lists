EFFECTIVE PERIOD:
13-AUG-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: JQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  -  A  -
02 2-0   -  -  B  -  -  B
03 2-0   C  C  -  -  C  -
04 2-0   -  -  -  D  -  D
05 2-0   -  E  -  E  E  E
06 2-0   F  -  -  -  F  F
07 2-0   G  G  -  G  -  -
08 0-3   -  H  -  H  -  -
09 0-3   I  I  I  I  -  -
10 0-3   J  J  J  -  -  -
11 0-4   -  -  -  K  -  -
12 0-4   L  -  L  -  -  -
13 0-5   M  -  M  -  M  M
14 0-5   -  -  N  -  N  N
15 0-5   O  O  -  -  -  O
16 0-5   -  -  -  P  P  -
17 0-5   -  Q  -  Q  -  -
18 1-6   R  -  R  R  R   
19 2-5   S  -  S  -  -   
20 2-5   T  T  -  T      
21 2-5   -  -  U  -      
22 2-5   -  -  -         
23 2-6   W  -  X         
24 2-6   X  Y            
25 3-4   -  -            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

RJUGT UJINR RTQXZ QERRV URFOD X
-------------------------------
