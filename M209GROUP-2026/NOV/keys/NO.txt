EFFECTIVE PERIOD:
23-NOV-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: NO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  A  -
02 2-0   B  -  B  B  -  B
03 2-0   C  -  C  -  C  -
04 2-0   D  -  -  -  D  -
05 2-0   -  E  -  -  E  E
06 2-0   -  -  -  F  F  F
07 2-0   -  -  G  -  -  -
08 2-0   H  -  -  -  -  H
09 2-0   I  -  -  I  -  -
10 0-3   -  J  -  J  -  J
11 0-4   K  K  K  K  -  -
12 0-4   L  -  -  -  -  L
13 0-5   -  -  M  M  M  -
14 0-5   -  N  N  -  -  -
15 0-5   -  O  -  O  -  O
16 0-5   -  P  -  -  P  P
17 0-5   -  Q  Q  Q  Q  -
18 0-6   R  R  -  -  R   
19 0-6   -  -  S  -  -   
20 0-6   T  T  T  -      
21 0-6   -  U  U  U      
22 2-5   V  -  -         
23 2-5   W  X  X         
24 2-5   X  -            
25 3-4   -  Z            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PVPMR XTCZB WPQJJ KHDJE SUINQ Y
-------------------------------
