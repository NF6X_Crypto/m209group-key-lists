EFFECTIVE PERIOD:
06-NOV-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: MX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 0-3   B  B  B  -  -  B
03 0-4   -  -  C  C  -  -
04 0-4   -  D  D  -  D  -
05 0-4   -  -  -  E  E  -
06 0-4   F  F  F  -  -  F
07 0-4   -  G  -  -  G  -
08 0-4   -  H  H  H  H  -
09 0-4   -  -  -  I  -  I
10 0-4   -  J  J  -  -  J
11 0-4   K  K  -  -  -  -
12 0-4   L  -  -  -  -  L
13 0-4   -  M  -  M  M  M
14 0-4   N  N  -  N  -  -
15 0-5   O  O  O  O  O  O
16 0-5   -  -  -  P  P  P
17 0-5   Q  -  Q  -  Q  -
18 0-5   -  R  -  -  R   
19 0-5   S  S  S  -  S   
20 0-5   -  T  T  -      
21 0-6   -  U  -  -      
22 0-6   -  -  V         
23 0-6   W  -  -         
24 2-3   X  -            
25 2-6   -  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

OARXM NKMHM CXOOJ NTPFG JTYVA N
-------------------------------
