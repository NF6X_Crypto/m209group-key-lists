EFFECTIVE PERIOD:
28-FEB-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  A  A
02 2-0   B  -  -  -  -  B
03 2-0   C  -  C  -  -  -
04 0-3   -  D  -  D  -  D
05 0-3   -  E  E  E  E  E
06 0-3   F  F  F  -  F  -
07 0-3   G  G  -  -  -  -
08 0-3   H  -  -  -  H  -
09 0-3   I  -  I  I  -  I
10 0-3   -  -  -  -  J  -
11 0-3   K  K  K  -  -  K
12 0-4   -  -  L  L  -  -
13 0-4   -  M  M  M  -  M
14 0-4   -  N  N  -  N  -
15 0-4   -  -  -  O  O  -
16 0-4   -  -  -  P  P  -
17 0-4   -  Q  Q  -  -  -
18 0-4   R  R  -  -  R   
19 0-4   S  S  S  S  -   
20 0-4   T  -  -  T      
21 0-4   -  U  U  -      
22 2-3   -  -  V         
23 2-5   -  X  -         
24 3-4   -  Y            
25 3-4   Y  Z            
26 3-4   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

WZSAF LRWPE EFYPO PESMB VANZR B
-------------------------------
