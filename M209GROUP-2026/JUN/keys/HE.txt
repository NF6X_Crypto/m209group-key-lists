EFFECTIVE PERIOD:
10-JUN-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   B  B  B  B  -  B
03 2-0   C  C  -  -  -  C
04 2-0   -  D  -  D  D  -
05 0-3   -  -  -  -  -  -
06 0-3   F  F  F  F  -  -
07 0-4   G  -  G  G  -  G
08 0-4   -  H  H  -  H  -
09 0-4   I  I  -  -  I  -
10 0-5   J  -  J  -  J  J
11 0-5   -  -  K  -  K  K
12 0-5   -  -  -  -  -  -
13 0-5   -  -  M  M  M  M
14 0-5   N  -  N  -  N  -
15 0-5   -  O  -  O  -  -
16 1-4   P  -  -  P  P  P
17 2-4   Q  -  -  Q  -  -
18 2-4   R  -  R  R  -   
19 2-4   -  -  S  S  S   
20 2-4   -  T  T  -      
21 2-5   -  U  -  -      
22 3-4   -  V  V         
23 3-4   -  X  X         
24 3-4   -  Y            
25 3-5   Y  -            
26 3-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

UIULW UUPQR LOQLQ WXPPR OTHWL R
-------------------------------
