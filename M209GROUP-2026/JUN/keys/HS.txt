EFFECTIVE PERIOD:
24-JUN-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  -  A
02 1-0   B  B  -  B  -  -
03 0-3   C  -  -  -  -  -
04 0-3   -  -  D  D  -  -
05 0-3   -  -  E  -  E  E
06 0-3   -  F  F  F  -  -
07 0-3   -  G  -  G  -  -
08 0-3   H  -  -  H  -  -
09 0-3   -  -  -  -  I  I
10 0-3   -  J  J  J  J  -
11 0-3   K  K  K  -  K  K
12 0-4   L  -  L  L  -  -
13 0-4   M  -  M  M  M  -
14 0-4   N  N  -  N  N  -
15 0-4   -  -  -  -  O  O
16 0-4   -  -  P  -  -  P
17 0-5   Q  -  Q  -  -  Q
18 0-5   R  R  -  R  -   
19 0-5   S  -  S  -  S   
20 0-5   -  T  -  T      
21 0-5   U  U  U  -      
22 0-5   -  V  V         
23 0-6   W  X  -         
24 1-2   -  -            
25 1-4   -  -            
26 3-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

HQUUA UBAKG GAQNQ ROJQJ XAAEE J
-------------------------------
