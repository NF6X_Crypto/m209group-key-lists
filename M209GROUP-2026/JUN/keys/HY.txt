EFFECTIVE PERIOD:
30-JUN-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: HY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  A
02 2-0   -  -  B  B  -  B
03 2-0   -  -  C  -  -  -
04 2-0   -  -  -  -  D  D
05 2-0   -  -  -  -  -  E
06 0-3   -  F  -  F  F  -
07 0-4   G  G  G  G  G  -
08 0-4   H  H  H  H  H  -
09 0-4   I  I  -  I  I  -
10 0-4   -  -  -  J  -  J
11 0-4   -  -  -  -  K  K
12 0-4   L  -  -  -  L  -
13 0-4   M  -  -  -  M  -
14 0-4   -  N  -  -  N  -
15 0-4   -  -  O  O  -  O
16 0-5   P  P  -  P  P  P
17 0-6   Q  -  Q  -  -  Q
18 0-6   R  R  R  R  R   
19 0-6   -  S  S  -  -   
20 0-6   T  T  -  T      
21 0-6   -  U  -  -      
22 1-2   -  V  V         
23 1-5   W  -  -         
24 2-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

MJPSR XPYJF VQABN NZZUI BKLVX K
-------------------------------
