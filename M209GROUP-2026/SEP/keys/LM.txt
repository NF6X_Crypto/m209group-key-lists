EFFECTIVE PERIOD:
30-SEP-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: LM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  -  A
02 0-3   B  B  B  -  -  B
03 0-3   -  C  -  C  -  C
04 0-3   D  -  -  -  D  -
05 0-3   -  E  -  E  -  E
06 0-4   F  -  F  -  F  -
07 0-4   G  G  -  G  G  G
08 0-4   -  -  H  H  -  -
09 0-4   I  I  I  -  I  -
10 0-4   -  -  -  J  J  J
11 0-5   -  -  -  K  K  K
12 0-5   -  -  -  -  -  -
13 0-5   M  M  M  M  M  -
14 0-5   -  N  -  N  -  N
15 0-5   O  -  O  O  -  O
16 0-5   P  P  P  -  -  P
17 0-6   -  Q  -  -  Q  -
18 1-4   -  -  -  -  R   
19 1-6   S  -  S  S  S   
20 2-4   T  T  T  T      
21 3-5   -  U  -  U      
22 3-6   -  -  -         
23 4-5   W  X  -         
24 4-5   -  -            
25 4-5   -  -            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

OYSEE UYVOD GACVM SVVYM OOUMU U
-------------------------------
