EFFECTIVE PERIOD:
02-SEP-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: KK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 1-0   B  B  -  -  B  -
03 1-0   -  C  C  C  C  C
04 1-0   D  -  -  D  -  D
05 1-0   -  -  E  -  E  -
06 2-0   F  F  -  F  -  -
07 2-0   G  -  -  -  -  G
08 2-0   H  -  H  H  H  H
09 0-4   I  I  I  -  -  I
10 0-4   -  -  -  -  J  -
11 0-4   -  -  -  K  -  K
12 0-4   L  -  L  L  -  -
13 0-4   M  M  M  -  -  M
14 0-5   -  -  N  -  N  -
15 0-5   O  -  -  -  O  O
16 0-5   P  P  P  P  -  P
17 0-5   -  Q  -  Q  Q  -
18 1-2   R  -  R  R  R   
19 1-2   S  S  -  S  -   
20 1-2   -  T  T  T      
21 1-2   -  -  -  -      
22 1-6   -  V  V         
23 1-6   -  -  X         
24 2-4   X  Y            
25 3-6   Y  -            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

MHVWR VMRLA RNALT IURLW GIRPO X
-------------------------------
