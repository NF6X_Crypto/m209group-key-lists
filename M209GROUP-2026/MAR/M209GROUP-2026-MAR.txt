SECRET    SECRET    SECRET    SECRET    SECRET

NET INDICATOR M209GROUP

KEY LIST FOR MONTH OF MAR 2026

    01 MAR 2026  00:00-23:59 GMT:  USE KEY DH
    02 MAR 2026  00:00-23:59 GMT:  USE KEY DI
    03 MAR 2026  00:00-23:59 GMT:  USE KEY DJ
    04 MAR 2026  00:00-23:59 GMT:  USE KEY DK
    05 MAR 2026  00:00-23:59 GMT:  USE KEY DL
    06 MAR 2026  00:00-23:59 GMT:  USE KEY DM
    07 MAR 2026  00:00-23:59 GMT:  USE KEY DN
    08 MAR 2026  00:00-23:59 GMT:  USE KEY DO
    09 MAR 2026  00:00-23:59 GMT:  USE KEY DP
    10 MAR 2026  00:00-23:59 GMT:  USE KEY DQ
    11 MAR 2026  00:00-23:59 GMT:  USE KEY DR
    12 MAR 2026  00:00-23:59 GMT:  USE KEY DS
    13 MAR 2026  00:00-23:59 GMT:  USE KEY DT
    14 MAR 2026  00:00-23:59 GMT:  USE KEY DU
    15 MAR 2026  00:00-23:59 GMT:  USE KEY DV
    16 MAR 2026  00:00-23:59 GMT:  USE KEY DW
    17 MAR 2026  00:00-23:59 GMT:  USE KEY DX
    18 MAR 2026  00:00-23:59 GMT:  USE KEY DY
    19 MAR 2026  00:00-23:59 GMT:  USE KEY DZ
    20 MAR 2026  00:00-23:59 GMT:  USE KEY EA
    21 MAR 2026  00:00-23:59 GMT:  USE KEY EB
    22 MAR 2026  00:00-23:59 GMT:  USE KEY EC
    23 MAR 2026  00:00-23:59 GMT:  USE KEY ED
    24 MAR 2026  00:00-23:59 GMT:  USE KEY EE
    25 MAR 2026  00:00-23:59 GMT:  USE KEY EF
    26 MAR 2026  00:00-23:59 GMT:  USE KEY EG
    27 MAR 2026  00:00-23:59 GMT:  USE KEY EH
    28 MAR 2026  00:00-23:59 GMT:  USE KEY EI
    29 MAR 2026  00:00-23:59 GMT:  USE KEY EJ
    30 MAR 2026  00:00-23:59 GMT:  USE KEY EK
    31 MAR 2026  00:00-23:59 GMT:  USE KEY EL

SECRET    SECRET    SECRET    SECRET    SECRET



SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
01-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   -  B  B  B  B  B
03 2-0   C  C  -  -  -  -
04 0-3   D  -  D  D  -  D
05 0-3   -  -  E  -  -  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  -  -  -  -
08 0-3   -  H  -  H  H  H
09 0-4   I  I  -  I  I  I
10 0-4   -  -  J  J  J  J
11 0-4   -  -  K  K  K  -
12 0-4   -  L  -  -  L  -
13 0-5   -  -  M  M  -  -
14 0-5   -  -  -  N  -  -
15 0-5   O  -  -  -  O  O
16 0-5   P  P  -  -  -  P
17 0-5   Q  Q  Q  -  -  -
18 1-6   R  R  R  -  R   
19 2-3   S  S  S  S  S   
20 2-6   -  -  -  T      
21 3-5   U  U  -  -      
22 4-5   V  -  V         
23 4-5   -  X  X         
24 4-5   X  -            
25 4-5   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QPFUN KVPNP PHPLK NVVUA GLAZZ N
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
02-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  A  A
02 2-0   -  B  -  B  -  B
03 2-0   C  -  C  -  -  C
04 2-0   D  D  D  D  -  D
05 2-0   E  -  -  -  -  -
06 2-0   -  F  F  F  F  -
07 2-0   -  -  G  G  G  -
08 2-0   -  H  -  -  -  -
09 2-0   I  -  I  -  I  -
10 2-0   J  J  -  -  -  J
11 2-0   -  K  K  -  K  K
12 0-3   L  -  -  L  -  -
13 0-3   M  -  M  -  -  M
14 0-3   N  N  -  N  N  -
15 0-3   O  O  -  O  -  -
16 0-3   -  P  -  P  P  -
17 0-4   -  Q  Q  -  -  Q
18 0-4   R  -  R  -  R   
19 0-4   -  -  S  S  S   
20 0-4   -  T  -  -      
21 0-4   U  U  U  -      
22 0-4   -  -  -         
23 0-4   W  -  X         
24 0-5   -  Y            
25 0-6   Y  -            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

MQVMH QUJDZ OZWIT NSCRD XLNLU O
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
03-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   B  B  B  B  B  -
03 2-0   C  C  C  C  C  -
04 2-0   D  -  -  -  -  D
05 0-3   -  -  -  -  -  E
06 0-3   F  F  F  F  -  F
07 0-4   G  -  G  -  G  G
08 0-4   H  H  H  -  -  -
09 0-4   I  -  I  -  -  I
10 0-4   -  -  J  J  J  J
11 0-5   K  -  -  K  K  K
12 0-5   L  L  -  -  -  -
13 0-5   -  -  -  -  M  -
14 0-5   N  -  N  -  -  -
15 0-5   -  -  O  -  O  O
16 0-5   -  -  -  P  -  -
17 0-5   -  Q  -  Q  Q  -
18 0-5   R  -  R  R  -   
19 0-6   -  S  S  -  S   
20 1-6   -  -  -  T      
21 2-4   -  U  U  -      
22 2-6   V  V  -         
23 3-4   -  -  X         
24 3-5   -  -            
25 3-5   Y  Z            
26 3-5   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

JOGRS TGYZP YOOHS JORZM LCSGU F
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
04-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  -  -
02 1-0   B  B  -  -  -  B
03 1-0   C  -  C  C  C  -
04 1-0   -  D  -  -  -  -
05 1-0   -  -  -  E  E  E
06 1-0   F  F  F  -  F  -
07 1-0   G  -  G  G  G  G
08 2-0   -  -  -  -  -  -
09 2-0   -  I  -  -  I  I
10 0-3   J  -  -  -  J  J
11 0-3   -  K  K  K  -  -
12 0-3   -  -  L  -  -  -
13 0-3   -  -  -  -  -  M
14 0-3   -  -  N  N  N  N
15 0-3   O  -  O  O  O  O
16 0-3   -  P  P  P  P  -
17 0-3   Q  -  -  -  -  Q
18 0-3   -  R  R  R  -   
19 0-4   S  -  S  -  S   
20 0-6   T  T  -  T      
21 0-6   -  -  -  U      
22 0-6   -  V  -         
23 0-6   W  X  X         
24 1-6   X  Y            
25 2-4   -  -            
26 2-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

IWRRW OFNRO JPVPE RZIZK VSCRL V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
05-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  A  -  -  -
02 0-3   B  -  -  -  B  B
03 0-3   C  -  C  -  C  C
04 0-3   D  D  -  D  -  D
05 0-3   -  -  E  -  E  E
06 0-3   F  -  -  F  F  -
07 0-4   -  G  -  -  -  -
08 0-4   H  -  H  H  -  -
09 0-4   -  -  I  I  -  I
10 0-4   -  -  -  J  -  -
11 0-4   K  -  -  K  K  -
12 0-4   -  -  L  -  L  -
13 0-6   M  M  M  -  -  M
14 0-6   -  -  -  N  -  N
15 0-6   O  O  O  O  O  O
16 0-6   -  P  -  P  -  -
17 0-6   Q  -  -  Q  Q  Q
18 1-2   R  R  R  R  -   
19 2-3   S  -  -  -  S   
20 3-4   T  T  -  T      
21 3-6   U  -  U  -      
22 3-6   -  V  V         
23 4-6   -  X  X         
24 4-6   -  Y            
25 4-6   -  -            
26 4-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

CUTUA NLVUU UMVBU LLUVS LUUUU U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
06-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  A  A  -
02 2-0   -  B  B  -  B  -
03 2-0   -  -  -  C  C  C
04 2-0   D  D  -  -  -  D
05 2-0   -  -  -  E  -  E
06 2-0   -  -  -  -  -  F
07 2-0   -  G  -  G  -  -
08 0-3   -  H  H  H  -  H
09 0-3   I  I  I  -  -  -
10 0-3   -  -  -  -  J  -
11 0-3   -  -  K  -  K  -
12 0-4   -  L  -  L  L  L
13 0-4   M  -  -  -  M  -
14 0-4   N  N  -  N  N  -
15 0-4   O  -  -  -  O  O
16 0-6   P  P  P  P  -  -
17 0-6   Q  -  -  Q  Q  Q
18 1-2   -  R  R  -  -   
19 1-2   -  S  S  -  S   
20 1-5   T  T  T  T      
21 1-6   -  U  U  -      
22 2-4   -  V  V         
23 2-4   W  -  -         
24 2-4   X  -            
25 2-4   -  -            
26 3-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

DWGSA TVYJN LAQIR IITWO OESGT R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
07-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DN
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  -  A  -  -
02 2-0   B  -  B  -  B  -
03 2-0   -  C  C  -  -  C
04 2-0   D  D  -  -  D  D
05 2-0   E  -  E  E  E  E
06 2-0   -  F  -  F  -  -
07 2-0   -  -  G  -  G  G
08 2-0   -  H  H  H  H  H
09 2-0   -  I  I  I  -  -
10 2-0   J  -  -  -  J  -
11 0-3   -  -  K  -  -  K
12 0-4   L  -  L  -  L  -
13 0-4   M  M  M  -  M  M
14 0-4   N  -  -  -  N  -
15 0-4   -  O  -  O  -  O
16 0-4   P  -  -  -  P  -
17 0-4   -  -  -  Q  -  -
18 0-6   -  -  -  R  R   
19 0-6   -  S  S  -  -   
20 0-6   -  -  T  T      
21 0-6   -  U  -  U      
22 0-6   V  -  -         
23 0-6   -  X  X         
24 0-6   -  -            
25 0-6   Y  -            
26 1-5   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

TBPZZ HMGQQ IKCYA BRQKT CKBRA R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
08-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DO
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   A  -  -  A  -  A
02 0-3   -  B  B  -  B  B
03 0-3   -  C  -  C  -  -
04 0-3   D  D  D  D  -  -
05 0-3   -  E  -  E  E  E
06 0-3   -  F  F  F  -  F
07 0-5   -  G  G  -  G  -
08 0-5   -  -  -  -  -  -
09 0-5   I  -  I  I  I  -
10 0-5   -  -  J  -  J  -
11 0-5   -  K  -  -  K  K
12 0-6   L  -  -  -  -  -
13 0-6   M  M  M  -  M  -
14 0-6   N  N  -  -  -  N
15 0-6   O  -  -  O  O  O
16 0-6   P  P  P  -  P  P
17 0-6   Q  Q  -  Q  Q  Q
18 1-4   -  R  -  R  -   
19 1-6   S  S  -  S  S   
20 2-5   T  -  T  -      
21 3-5   U  -  U  U      
22 3-5   V  -  V         
23 3-5   -  -  -         
24 3-5   X  Y            
25 3-6   -  -            
26 4-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

JNNUU UUZUM ZUUNN LSUSM TJAUU U
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
09-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  A  A
02 2-0   -  -  -  -  -  -
03 2-0   -  -  -  -  -  -
04 0-3   -  D  -  -  -  -
05 0-4   E  -  E  -  -  E
06 0-4   F  F  F  -  -  F
07 0-4   -  -  G  G  G  G
08 0-4   H  -  H  -  H  H
09 0-4   -  -  -  -  I  I
10 0-4   J  J  J  -  J  -
11 0-4   -  K  K  K  -  -
12 0-4   L  L  -  L  -  L
13 0-4   -  M  -  M  -  M
14 0-5   N  N  -  N  -  -
15 0-5   O  -  O  O  O  -
16 0-5   -  -  P  -  P  -
17 0-5   -  -  -  Q  -  Q
18 0-5   -  R  -  R  -   
19 0-5   S  S  S  -  S   
20 0-5   T  T  -  -      
21 0-6   U  -  -  -      
22 0-6   V  V  V         
23 0-6   W  X  X         
24 0-6   -  -            
25 0-6   Y  Z            
26 2-3   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

IQQLU OGQLL EJFFQ YMIGQ OMUYK L
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
10-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DQ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  -  -  -
02 1-0   -  -  -  B  B  B
03 1-0   C  -  -  C  C  C
04 1-0   -  D  -  -  -  -
05 1-0   E  E  E  E  -  -
06 1-0   -  -  F  F  F  -
07 2-0   G  G  -  -  G  G
08 2-0   -  H  H  H  -  H
09 2-0   I  -  I  I  I  I
10 2-0   J  -  -  -  -  J
11 2-0   K  -  K  -  -  K
12 0-4   -  L  L  L  -  -
13 0-4   -  -  M  -  M  M
14 0-4   N  -  -  N  N  -
15 0-4   O  O  -  -  -  -
16 0-5   -  -  P  -  P  P
17 0-5   Q  Q  Q  Q  Q  Q
18 1-4   -  -  -  -  R   
19 1-5   S  S  -  -  S   
20 2-3   T  -  -  -      
21 2-4   U  -  -  -      
22 2-4   -  V  V         
23 2-4   W  X  -         
24 2-4   X  Y            
25 2-5   Y  Z            
26 2-6   -               
27 3-5                   
-------------------------------
26 LETTER CHECK

LTPJA MFWUW UIZUU UOYMI UMRFL A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
11-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DR
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  -  -  A
02 1-0   -  -  B  -  B  -
03 1-0   C  -  C  -  C  C
04 1-0   -  D  -  D  D  -
05 1-0   E  E  -  E  -  -
06 1-0   -  F  -  F  -  F
07 1-0   G  G  G  G  G  -
08 0-3   -  -  -  -  -  H
09 0-3   -  I  I  -  -  -
10 0-3   J  -  J  -  -  -
11 0-3   -  K  -  K  K  -
12 0-5   L  -  -  -  -  -
13 0-5   M  M  -  M  M  M
14 0-5   N  -  N  -  N  N
15 0-6   O  O  O  O  -  -
16 0-6   P  -  -  -  -  P
17 0-6   -  -  Q  Q  Q  -
18 1-2   -  R  -  -  R   
19 1-2   -  S  -  -  S   
20 1-6   T  -  T  T      
21 1-6   -  -  -  -      
22 1-6   -  -  V         
23 1-6   -  X  X         
24 2-4   X  -            
25 2-5   -  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

UJNQO MJNUQ JWNNP OJOJQ VHQUZ S
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
12-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DS
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  A  -  -  -
02 2-0   B  B  -  B  -  B
03 2-0   -  -  -  -  C  -
04 2-0   -  D  D  D  D  D
05 2-0   E  E  E  -  E  -
06 2-0   -  F  F  -  -  F
07 2-0   -  G  G  -  G  -
08 2-0   -  -  -  H  -  -
09 0-3   -  -  -  I  -  I
10 0-3   J  J  J  J  J  -
11 0-3   K  -  K  -  -  -
12 0-3   -  L  L  L  L  L
13 0-3   -  -  M  -  -  M
14 0-3   N  N  -  -  N  N
15 0-3   O  -  -  -  -  -
16 0-3   -  -  -  -  -  -
17 0-3   Q  Q  -  -  Q  -
18 0-3   -  R  R  R  -   
19 0-4   S  -  -  S  -   
20 0-4   T  -  -  T      
21 0-4   -  -  U  U      
22 0-4   V  V  V         
23 0-4   W  X  -         
24 0-5   X  Y            
25 0-6   Y  -            
26 1-4   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

BDJKM HHYZJ SIQJS UWOGM ZNQZK D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
13-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DT
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  B  -  B  B  B
03 2-0   C  C  C  -  C  C
04 2-0   -  -  -  D  -  -
05 2-0   E  -  E  -  E  E
06 2-0   F  F  -  F  -  -
07 2-0   G  G  -  -  -  -
08 2-0   -  -  -  -  H  H
09 0-3   I  I  -  -  -  I
10 0-4   J  J  J  -  J  -
11 0-4   -  -  K  -  K  -
12 0-4   L  -  -  -  -  -
13 0-4   M  M  -  M  -  -
14 0-4   N  -  N  N  -  N
15 0-4   O  O  O  -  O  O
16 0-4   -  -  P  -  -  P
17 0-5   -  Q  Q  Q  Q  -
18 0-6   R  -  -  R  R   
19 0-6   -  S  S  S  S   
20 0-6   -  -  T  T      
21 0-6   -  -  -  -      
22 0-6   -  -  V         
23 0-6   -  -  -         
24 0-6   X  -            
25 0-6   -  Z            
26 1-2   -               
27 1-5                   
-------------------------------
26 LETTER CHECK

OHKKV RJQMJ POQJP MJKBK SOTKN A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
14-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  A  A  -  A
02 1-0   B  B  -  B  B  -
03 1-0   -  C  -  -  C  -
04 0-4   D  D  -  -  -  -
05 0-4   E  -  E  -  E  -
06 0-4   F  -  -  -  F  -
07 0-4   G  G  -  -  G  G
08 0-4   -  H  H  H  -  H
09 0-4   -  -  I  -  -  I
10 0-5   -  -  J  -  -  J
11 0-5   K  K  -  -  -  -
12 0-5   L  L  L  L  -  L
13 0-5   -  -  -  M  -  -
14 0-5   -  -  N  N  N  N
15 0-5   -  O  -  -  -  O
16 0-5   P  -  P  P  P  P
17 0-6   -  -  -  Q  Q  -
18 1-5   -  R  -  -  R   
19 1-6   S  -  S  -  -   
20 2-4   T  T  -  T      
21 3-4   -  -  U  -      
22 3-6   -  -  V         
23 4-5   -  -  X         
24 4-5   X  -            
25 4-5   -  -            
26 4-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

SPNTA RLPGV ZZQQU HTHFB PATNX V
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
15-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DV
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  -  -  -
02 1-0   -  -  -  B  -  -
03 1-0   C  -  C  -  C  C
04 1-0   D  D  -  -  D  -
05 1-0   -  -  -  E  -  E
06 1-0   F  -  -  F  -  -
07 1-0   G  G  -  G  -  -
08 2-0   H  -  H  H  H  H
09 2-0   I  I  -  -  I  I
10 2-0   -  J  J  -  J  J
11 2-0   -  K  K  K  -  -
12 2-0   -  -  -  L  L  L
13 2-0   M  M  -  M  -  -
14 2-0   -  -  -  -  -  N
15 2-0   -  O  -  -  -  -
16 2-0   -  -  -  -  -  P
17 0-3   Q  -  Q  -  Q  -
18 0-4   -  R  R  R  R   
19 0-5   -  S  S  S  S   
20 0-5   T  T  -  T      
21 0-5   U  U  U  U      
22 1-2   -  -  V         
23 1-2   W  -  X         
24 1-2   -  -            
25 1-5   Y  Z            
26 4-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

SMZOU ASRCN SFAMW GQYMA PNBTE Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
16-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DW
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  A
02 1-0   -  -  -  B  B  B
03 1-0   -  C  C  -  -  -
04 1-0   -  D  D  -  D  -
05 2-0   E  -  -  -  E  E
06 2-0   F  F  -  -  -  -
07 0-3   G  G  -  G  G  G
08 0-3   H  -  -  H  -  H
09 0-3   -  I  -  -  -  -
10 0-3   J  J  J  J  J  J
11 0-3   K  K  K  K  -  K
12 0-3   L  -  -  -  L  -
13 0-3   M  -  -  M  M  -
14 0-3   -  N  N  N  N  N
15 0-3   O  -  O  -  -  -
16 0-3   -  -  P  P  P  -
17 0-3   -  Q  Q  -  -  Q
18 0-5   -  -  -  R  R   
19 0-6   -  S  S  S  S   
20 0-6   -  -  -  T      
21 0-6   U  U  -  -      
22 0-6   V  V  -         
23 0-6   W  -  -         
24 1-2   X  Y            
25 1-6   -  -            
26 2-4   Z               
27 3-6                   
-------------------------------
26 LETTER CHECK

TNQSE SPPLE SPMVL GOPGQ AJGXV X
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
17-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DX
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  -  -
02 2-0   B  -  -  B  -  B
03 2-0   C  C  C  C  C  -
04 0-4   D  D  D  -  -  D
05 0-4   -  E  -  -  E  E
06 0-4   F  F  -  F  -  F
07 0-4   G  -  -  G  G  -
08 0-4   -  -  -  -  -  H
09 0-4   -  I  I  I  -  -
10 0-4   J  -  -  J  -  -
11 0-4   K  -  K  K  K  -
12 0-4   L  -  -  -  -  L
13 0-5   M  -  -  -  M  -
14 0-6   -  -  N  -  -  -
15 0-6   O  O  O  -  O  O
16 0-6   -  -  P  -  -  P
17 0-6   -  Q  -  Q  Q  Q
18 0-6   -  -  -  -  R   
19 0-6   -  S  -  -  S   
20 0-6   T  T  T  -      
21 0-6   -  U  U  U      
22 0-6   -  -  V         
23 0-6   W  X  X         
24 0-6   -  Y            
25 0-6   -  Z            
26 1-3   -               
27 2-3                   
-------------------------------
26 LETTER CHECK

EOEAZ QRCZE AVOEA MEJXM CMNNC A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
18-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  A
02 1-0   -  -  -  B  -  B
03 1-0   C  C  C  -  C  C
04 1-0   D  D  -  D  -  -
05 2-0   E  E  E  -  E  -
06 2-0   F  -  -  F  F  -
07 2-0   -  G  G  -  -  G
08 2-0   -  H  H  H  H  H
09 2-0   -  -  I  -  -  -
10 2-0   -  J  J  J  -  -
11 2-0   K  -  K  K  -  K
12 2-0   -  -  -  L  L  -
13 0-3   M  -  -  -  -  -
14 0-4   N  -  -  -  N  N
15 0-4   O  -  O  O  -  -
16 0-4   -  P  P  -  -  -
17 0-4   -  -  Q  Q  Q  -
18 0-5   -  -  R  -  -   
19 0-5   S  S  -  S  -   
20 1-4   T  -  -  -      
21 1-5   U  -  -  U      
22 2-4   V  -  -         
23 2-4   -  X  X         
24 2-4   -  -            
25 2-4   -  -            
26 3-5   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

EOOIY INAVL MLOVY UQOTO TIZNK Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
19-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  A  A  A
02 2-0   -  B  B  B  -  -
03 2-0   C  C  -  C  C  -
04 2-0   D  D  D  -  D  -
05 0-3   -  E  E  -  E  -
06 0-3   -  -  F  F  -  -
07 0-5   G  -  G  -  -  G
08 0-5   H  H  H  -  -  -
09 0-5   I  -  I  I  I  I
10 0-5   -  -  -  -  -  -
11 0-5   -  K  -  K  K  K
12 0-6   L  -  -  -  -  -
13 0-6   -  M  -  M  -  M
14 0-6   N  N  -  N  N  -
15 0-6   -  -  O  -  -  O
16 0-6   -  P  -  -  -  P
17 0-6   Q  -  -  Q  Q  Q
18 1-4   -  -  R  -  -   
19 2-5   S  -  -  -  S   
20 2-5   T  -  T  T      
21 2-5   U  U  -  U      
22 2-5   -  -  -         
23 2-6   -  X  -         
24 3-4   X  -            
25 3-6   Y  Z            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

LPSAN TSURD RPPTP SONUL ZYLPA D
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
20-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EA
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  -  A
02 1-0   -  B  -  -  B  -
03 1-0   C  C  C  -  -  -
04 1-0   -  D  -  D  -  -
05 1-0   E  -  -  -  -  E
06 1-0   F  F  F  F  F  F
07 0-3   G  G  G  G  -  G
08 0-4   H  H  H  H  -  -
09 0-5   -  I  -  I  I  I
10 0-5   J  -  J  -  J  J
11 0-5   -  K  K  -  K  -
12 0-5   L  L  L  L  L  -
13 0-5   M  M  M  -  M  -
14 0-5   -  N  -  N  -  -
15 0-5   O  -  -  -  -  O
16 0-5   -  -  -  -  P  P
17 0-6   Q  Q  Q  -  Q  Q
18 0-6   -  -  R  -  -   
19 0-6   -  -  -  -  -   
20 0-6   -  -  T  T      
21 0-6   U  U  -  -      
22 1-5   V  -  -         
23 1-5   W  -  -         
24 1-5   X  -            
25 1-6   -  Z            
26 2-4   -               
27 2-6                   
-------------------------------
26 LETTER CHECK

KTBAS XRRJM BQJYC SAVTN UISYS J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
21-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EB
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  -  -  A  A  -
02 1-0   B  B  B  -  B  B
03 1-0   C  C  C  -  C  -
04 1-0   -  -  -  -  -  -
05 1-0   E  E  E  E  E  E
06 1-0   -  F  -  -  -  -
07 1-0   G  -  -  G  G  G
08 1-0   H  -  -  H  H  H
09 1-0   I  I  I  -  I  I
10 1-0   -  -  J  J  -  J
11 1-0   K  -  -  -  -  -
12 0-4   L  L  -  L  L  -
13 0-5   -  -  -  M  M  M
14 0-5   N  -  N  -  -  -
15 0-5   -  -  O  -  O  O
16 0-5   P  P  P  P  P  P
17 0-5   Q  Q  Q  -  -  -
18 0-6   R  R  -  R  -   
19 0-6   -  S  S  -  -   
20 0-6   T  -  -  -      
21 0-6   -  U  -  -      
22 0-6   -  V  -         
23 0-6   W  X  X         
24 1-6   -  Y            
25 2-3   Y  Z            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UTUIZ KAMOU HJTUO APLOZ HTZHO J
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
22-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  A  A  A  -
02 1-0   -  B  -  B  -  -
03 1-0   C  C  -  C  C  -
04 1-0   D  -  D  -  D  -
05 2-0   -  E  E  E  -  -
06 0-3   F  -  -  -  -  F
07 0-3   -  -  -  -  G  G
08 0-3   -  H  -  -  H  -
09 0-3   I  -  -  I  I  I
10 0-3   -  J  J  J  J  J
11 0-3   K  -  -  K  -  -
12 0-3   -  L  L  L  L  L
13 0-3   -  -  M  -  -  M
14 0-4   N  N  -  N  -  N
15 0-4   O  O  O  -  -  O
16 0-4   P  -  -  P  P  P
17 0-4   Q  -  Q  -  -  -
18 0-4   -  -  -  -  R   
19 0-4   -  S  S  -  -   
20 0-5   -  -  -  T      
21 0-6   -  -  -  -      
22 1-2   -  V  V         
23 1-4   W  X  -         
24 2-5   X  -            
25 3-4   Y  -            
26 3-4   -               
27 3-4                   
-------------------------------
26 LETTER CHECK

IXASE LBQTO TQDZS EILNZ NRVBY I
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
23-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ED
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  -  A  A
02 2-0   -  -  -  B  B  B
03 2-0   -  C  C  C  C  -
04 2-0   -  D  D  -  -  D
05 2-0   E  E  -  E  E  -
06 2-0   F  -  -  F  -  F
07 0-4   G  -  G  -  G  G
08 0-4   -  -  H  -  -  -
09 0-4   -  -  -  -  I  I
10 0-4   J  J  -  -  -  J
11 0-5   K  -  -  K  -  K
12 0-6   -  L  -  L  -  L
13 0-6   -  M  -  M  M  M
14 0-6   N  -  N  N  N  -
15 0-6   O  O  O  -  O  -
16 1-3   P  -  -  -  -  -
17 2-5   Q  -  Q  -  Q  -
18 2-6   R  -  R  R  -   
19 3-4   -  S  S  S  S   
20 3-4   T  -  T  -      
21 3-5   -  -  U  U      
22 4-5   V  V  -         
23 4-5   W  -  X         
24 4-6   -  -            
25 4-6   Y  Z            
26 4-6   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

ZUOHL PNWTH ZTQZV QQMIQ WPVLQ Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
24-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EE
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  A  A  -
02 0-3   B  -  B  B  -  B
03 0-3   -  C  -  -  C  C
04 0-3   -  -  D  -  D  -
05 0-3   -  -  -  -  E  E
06 0-3   -  -  -  -  F  -
07 0-3   G  -  G  G  G  G
08 0-3   -  -  H  -  H  H
09 0-4   I  -  I  -  -  -
10 0-4   J  J  -  -  -  J
11 0-4   -  K  -  -  -  -
12 0-4   -  L  -  L  -  L
13 0-4   M  -  M  M  -  M
14 0-5   N  -  -  N  N  -
15 0-6   O  -  O  -  O  -
16 0-6   P  -  P  P  P  P
17 0-6   -  Q  Q  Q  -  -
18 0-6   R  R  R  -  R   
19 0-6   -  S  S  S  -   
20 0-6   -  T  -  T      
21 0-6   U  -  -  U      
22 0-6   -  -  V         
23 0-6   -  X  -         
24 0-6   -  Y            
25 0-6   -  -            
26 2-4   Z               
27 2-5                   
-------------------------------
26 LETTER CHECK

GXVOO UHFZR IVCUM HRHRL HHZCN A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
25-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EF
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  A  A
02 1-0   -  -  B  -  B  B
03 1-0   C  -  C  -  -  C
04 1-0   -  -  -  D  -  D
05 1-0   -  E  E  -  E  E
06 2-0   -  -  F  -  F  -
07 2-0   -  G  G  G  -  G
08 0-4   -  H  -  H  -  H
09 0-4   -  -  I  -  -  -
10 0-4   J  -  -  J  J  -
11 0-4   -  K  -  -  -  -
12 0-4   L  -  L  L  -  L
13 0-4   M  M  -  M  M  -
14 0-4   N  N  -  -  N  N
15 0-4   O  O  -  O  -  -
16 0-5   -  -  -  -  P  -
17 0-5   Q  -  -  Q  Q  Q
18 0-5   R  R  R  -  R   
19 0-6   -  -  -  S  S   
20 1-2   T  T  T  T      
21 1-5   U  -  -  -      
22 2-6   -  V  V         
23 3-4   W  X  X         
24 4-5   X  -            
25 4-5   Y  -            
26 4-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QPWEW QSASQ CLQPC QMLRZ YUGAR A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
26-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  A  -
02 1-0   B  -  -  B  B  B
03 1-0   -  -  C  -  C  -
04 2-0   D  -  D  D  D  D
05 2-0   E  E  E  -  E  E
06 2-0   -  -  F  F  -  -
07 2-0   -  -  -  -  -  -
08 2-0   -  H  H  -  -  -
09 0-3   -  -  -  -  -  -
10 0-3   -  -  J  -  J  J
11 0-3   K  K  K  K  -  K
12 0-3   L  -  L  L  -  L
13 0-3   M  M  M  M  -  M
14 0-3   N  N  N  -  -  -
15 0-5   -  O  O  O  -  O
16 0-6   -  -  -  -  P  -
17 0-6   Q  Q  -  Q  Q  Q
18 0-6   R  -  -  R  R   
19 0-6   -  -  -  S  -   
20 0-6   T  -  -  -      
21 0-6   -  U  U  U      
22 0-6   V  -  V         
23 0-6   -  -  -         
24 0-6   -  -            
25 0-6   -  Z            
26 1-5   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

VTTZP CUFSC RRLFK OJSOQ GZJAQ R
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
27-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  -  A
02 1-0   -  B  -  B  B  -
03 1-0   -  C  C  C  -  C
04 1-0   D  -  D  D  -  -
05 1-0   -  -  E  -  E  -
06 1-0   -  F  -  F  F  F
07 0-3   G  G  G  G  G  -
08 0-3   -  -  H  -  -  -
09 0-3   I  -  -  -  -  I
10 0-3   J  J  -  J  J  -
11 0-3   K  K  -  -  K  K
12 0-3   -  -  L  L  L  -
13 0-3   -  -  M  M  -  -
14 0-6   -  N  -  -  N  -
15 0-6   -  -  O  O  -  O
16 0-6   P  P  -  -  -  P
17 0-6   Q  -  -  -  -  Q
18 1-2   R  R  R  R  R   
19 1-3   -  S  -  -  -   
20 1-3   -  -  -  -      
21 1-3   -  U  U  -      
22 1-3   V  V  -         
23 1-6   -  X  X         
24 1-6   X  -            
25 3-6   Y  Z            
26 4-5   -               
27 4-6                   
-------------------------------
26 LETTER CHECK

TUOAT ITTMA CCUNW IMABN OASIT A
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
28-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EI
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  -  A  -
02 1-0   -  B  -  B  B  B
03 1-0   C  -  -  C  C  -
04 1-0   -  D  -  -  -  -
05 2-0   E  E  -  E  E  -
06 0-3   F  -  F  F  F  F
07 0-3   G  G  G  G  G  G
08 0-3   -  -  -  -  H  H
09 0-3   -  -  I  I  I  I
10 0-3   J  -  -  J  -  -
11 0-3   -  K  K  -  -  -
12 0-5   L  L  -  L  -  L
13 0-5   -  M  M  M  -  -
14 0-5   -  -  N  N  -  N
15 0-5   -  O  O  -  -  -
16 0-5   P  -  -  -  P  -
17 0-5   -  -  Q  -  -  -
18 0-5   -  R  R  R  R   
19 0-5   S  S  S  -  S   
20 0-6   T  -  T  T      
21 0-6   U  -  U  -      
22 1-3   -  -  -         
23 1-6   W  X  X         
24 3-5   -  Y            
25 3-5   -  Z            
26 3-5   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QNZSX PRXPS SBPTP AWHNZ RBQSJ P
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
29-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EJ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  -  -  -  A
02 1-0   B  -  B  -  -  -
03 2-0   C  -  -  -  C  C
04 0-3   D  -  -  D  D  D
05 0-3   -  -  -  E  E  -
06 0-3   F  F  -  -  -  F
07 0-3   G  -  G  -  -  G
08 0-3   H  -  H  -  H  -
09 0-3   I  -  I  -  -  -
10 0-3   -  J  J  J  -  -
11 0-3   -  K  -  -  K  -
12 0-3   -  L  -  L  L  -
13 0-3   -  M  -  -  -  -
14 0-3   -  N  -  N  -  N
15 0-4   O  -  -  O  O  -
16 0-5   -  -  P  P  -  -
17 0-5   -  Q  Q  Q  Q  Q
18 0-5   -  -  R  -  -   
19 0-6   S  -  -  S  -   
20 0-6   -  T  T  -      
21 0-6   U  -  U  U      
22 0-6   V  -  V         
23 0-6   W  -  -         
24 1-4   X  Y            
25 1-5   -  Z            
26 3-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

EBOZN IWVQH LUOLD EQPUT JAHJN Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
30-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  A  -  A  A
02 2-0   B  B  B  B  -  -
03 2-0   -  -  -  -  -  -
04 2-0   D  D  D  -  D  -
05 2-0   -  E  -  E  E  E
06 0-4   -  -  -  F  F  F
07 0-4   -  -  G  G  -  G
08 0-4   -  -  H  H  H  -
09 0-4   I  I  -  -  -  I
10 0-4   J  J  J  J  J  -
11 0-4   -  K  -  K  K  K
12 0-5   L  L  -  -  -  L
13 0-6   M  -  M  M  -  -
14 0-6   -  N  -  N  -  N
15 0-6   O  O  O  -  -  O
16 1-3   -  -  P  -  P  -
17 2-3   -  -  -  -  -  -
18 2-4   -  -  R  R  R   
19 3-5   S  S  -  S  -   
20 3-5   -  -  -  -      
21 3-5   -  -  U  U      
22 3-5   V  -  V         
23 4-6   -  X  -         
24 5-6   -  Y            
25 5-6   Y  -            
26 5-6   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UUFIU HRNXH NOUVO ZNOTS PUSJT Q
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET

SECRET    SECRET    SECRET    SECRET    SECRET



EFFECTIVE PERIOD:
31-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  A  -  A
02 1-0   -  B  B  -  -  -
03 1-0   C  C  -  -  C  -
04 1-0   D  D  D  D  -  D
05 1-0   -  -  E  E  E  -
06 1-0   F  -  -  -  -  -
07 2-0   G  G  -  G  G  G
08 2-0   H  -  H  -  H  H
09 2-0   -  -  -  -  I  I
10 2-0   J  -  -  -  J  J
11 0-3   -  -  K  -  K  -
12 0-5   L  -  L  L  -  -
13 0-5   -  M  -  -  -  M
14 0-5   N  N  -  N  -  N
15 0-5   -  -  O  -  -  -
16 0-5   P  P  P  P  P  P
17 0-5   Q  Q  Q  -  -  Q
18 0-6   -  R  R  R  R   
19 0-6   -  S  -  -  -   
20 1-2   T  -  T  T      
21 1-6   U  -  -  -      
22 2-5   -  V  -         
23 2-5   W  X  -         
24 2-5   X  -            
25 2-5   -  -            
26 3-4   -               
27 3-6                   
-------------------------------
26 LETTER CHECK

HSOOY LPTPA PSPUA OUYRD MCLSU M
-------------------------------



SECRET    SECRET    SECRET    SECRET    SECRET
