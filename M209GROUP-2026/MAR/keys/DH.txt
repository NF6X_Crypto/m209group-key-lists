EFFECTIVE PERIOD:
01-MAR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: DH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  -  -  -  -  -
02 2-0   -  B  B  B  B  B
03 2-0   C  C  -  -  -  -
04 0-3   D  -  D  D  -  D
05 0-3   -  -  E  -  -  -
06 0-3   -  F  -  -  -  F
07 0-3   G  G  -  -  -  -
08 0-3   -  H  -  H  H  H
09 0-4   I  I  -  I  I  I
10 0-4   -  -  J  J  J  J
11 0-4   -  -  K  K  K  -
12 0-4   -  L  -  -  L  -
13 0-5   -  -  M  M  -  -
14 0-5   -  -  -  N  -  -
15 0-5   O  -  -  -  O  O
16 0-5   P  P  -  -  -  P
17 0-5   Q  Q  Q  -  -  -
18 1-6   R  R  R  -  R   
19 2-3   S  S  S  S  S   
20 2-6   -  -  -  T      
21 3-5   U  U  -  -      
22 4-5   V  -  V         
23 4-5   -  X  X         
24 4-5   X  -            
25 4-5   -  Z            
26 4-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

QPFUN KVPNP PHPLK NVVUA GLAZZ N
-------------------------------
