EFFECTIVE PERIOD:
15-DEC-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OK
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   -  A  -  A  -  A
02 2-0   B  B  -  -  -  -
03 2-0   -  C  C  C  C  C
04 2-0   -  -  D  D  -  D
05 2-0   E  -  E  -  -  E
06 2-0   -  -  F  -  F  -
07 0-3   G  -  -  -  -  G
08 0-5   H  H  H  H  H  -
09 0-5   -  I  -  I  -  I
10 0-5   -  J  J  J  -  -
11 0-5   -  K  K  K  K  -
12 0-5   L  -  -  L  L  -
13 0-5   -  M  -  M  -  -
14 0-5   -  N  N  -  -  N
15 0-6   -  -  -  -  -  O
16 0-6   P  P  P  -  P  -
17 0-6   Q  Q  Q  Q  -  -
18 0-6   -  -  -  R  R   
19 0-6   S  -  -  -  S   
20 1-2   T  -  T  T      
21 1-3   U  U  -  -      
22 2-6   V  V  V         
23 4-5   -  X  X         
24 5-6   -  -            
25 5-6   -  Z            
26 5-6   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

PIINU DVXIM ZQKZB VAEUU SQCUP N
-------------------------------
