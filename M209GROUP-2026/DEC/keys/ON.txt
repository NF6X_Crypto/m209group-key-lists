EFFECTIVE PERIOD:
18-DEC-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: ON
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  -  A  A  A
02 2-0   -  -  B  B  -  B
03 2-0   -  C  C  -  C  C
04 0-4   D  D  -  D  -  -
05 0-4   -  -  -  -  E  -
06 0-4   F  -  F  F  F  F
07 0-4   -  -  -  G  -  G
08 0-4   -  H  H  H  -  -
09 0-4   I  -  I  -  I  I
10 0-4   J  J  J  -  -  -
11 0-4   -  -  K  -  K  K
12 0-5   L  L  -  -  -  -
13 0-5   M  -  -  -  M  -
14 0-5   N  -  N  N  -  -
15 0-5   -  -  -  -  O  -
16 0-5   P  P  -  P  -  -
17 0-5   -  Q  Q  Q  -  Q
18 0-5   -  -  R  -  -   
19 0-5   -  S  S  S  S   
20 0-5   T  -  T  -      
21 0-5   U  -  -  -      
22 0-5   V  V  -         
23 0-5   -  -  X         
24 0-5   X  Y            
25 0-6   Y  -            
26 1-2   Z               
27 1-3                   
-------------------------------
26 LETTER CHECK

CYDVM BKRPN WEWRM JWBRB RZZCK I
-------------------------------
