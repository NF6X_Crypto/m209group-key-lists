EFFECTIVE PERIOD:
30-DEC-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OZ
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  A  A  A  A  A
02 2-0   -  -  B  -  -  -
03 2-0   -  C  -  -  -  -
04 2-0   -  -  D  D  D  -
05 2-0   -  E  E  -  -  E
06 0-3   F  F  F  -  F  -
07 0-3   G  G  G  G  -  -
08 0-3   H  H  -  -  -  -
09 0-3   I  I  -  I  I  I
10 0-3   -  -  J  -  -  -
11 0-6   K  -  -  K  -  K
12 0-6   -  -  -  -  -  -
13 0-6   -  M  -  -  -  M
14 0-6   N  N  -  N  N  N
15 0-6   -  O  O  O  O  O
16 0-6   P  P  -  P  -  -
17 0-6   -  Q  Q  Q  -  -
18 1-2   -  -  -  R  R   
19 2-3   -  -  S  -  S   
20 2-3   T  T  T  -      
21 2-3   U  -  -  -      
22 2-3   -  -  -         
23 2-4   W  X  X         
24 2-5   -  -            
25 3-6   Y  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

USVUL SMLNM RABTJ ATVLA ILDMA N
-------------------------------
