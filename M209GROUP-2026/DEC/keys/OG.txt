EFFECTIVE PERIOD:
11-DEC-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: OG
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  A  A  -  -  -
02 1-0   -  B  -  B  -  B
03 1-0   -  -  -  -  C  C
04 1-0   -  -  -  -  D  -
05 1-0   -  -  E  -  E  -
06 1-0   F  -  F  F  -  F
07 1-0   G  -  -  -  -  G
08 1-0   H  H  H  H  H  H
09 1-0   I  -  I  -  -  -
10 2-0   -  J  J  -  -  J
11 0-3   -  -  -  K  K  -
12 0-3   -  L  -  L  L  -
13 0-3   -  M  -  M  M  -
14 0-3   N  -  N  N  -  -
15 0-3   -  -  -  O  O  -
16 0-3   -  P  P  P  -  -
17 0-4   Q  -  -  -  -  Q
18 0-5   R  -  -  R  R   
19 0-5   -  S  -  S  S   
20 0-5   -  -  -  -      
21 0-5   U  -  U  U      
22 0-5   V  -  -         
23 0-6   W  X  X         
24 1-3   X  Y            
25 2-5   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

OTRHC KTWOL ZSJBK NMQYX JAJMK P
-------------------------------
