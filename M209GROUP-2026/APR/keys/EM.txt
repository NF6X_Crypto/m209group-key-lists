EFFECTIVE PERIOD:
01-APR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EM
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 0-3   -  -  A  -  -  A
02 0-3   B  -  -  -  B  -
03 0-3   -  C  -  -  C  C
04 0-5   -  -  D  -  D  -
05 0-5   -  -  -  E  -  E
06 0-5   F  -  -  F  -  -
07 0-5   G  -  G  G  -  G
08 0-5   H  H  H  -  -  H
09 0-6   I  I  -  -  I  -
10 0-6   -  -  J  -  -  J
11 0-6   K  -  -  K  K  K
12 0-6   -  L  -  -  -  L
13 0-6   M  M  M  M  M  M
14 0-6   N  -  N  N  N  -
15 0-6   -  -  O  -  O  -
16 1-2   -  -  -  P  -  -
17 1-3   Q  -  -  -  -  Q
18 1-3   R  R  -  R  R   
19 1-3   -  -  -  -  S   
20 1-6   T  T  T  T      
21 2-3   -  U  U  -      
22 3-4   V  V  -         
23 3-5   -  -  X         
24 3-5   X  Y            
25 3-5   -  -            
26 3-5   -               
27 5-6                   
-------------------------------
26 LETTER CHECK

UAVME CAAEA RREVJ AAVWV JPSVV J
-------------------------------
