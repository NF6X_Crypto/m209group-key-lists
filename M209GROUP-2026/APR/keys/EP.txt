EFFECTIVE PERIOD:
04-APR-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: EP
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   -  -  A  A  -  A
02 2-0   B  B  -  B  B  -
03 2-0   -  C  -  -  C  C
04 0-3   D  -  -  D  D  D
05 0-3   E  -  E  E  E  E
06 0-3   F  F  F  -  -  -
07 0-3   G  G  G  G  G  G
08 0-3   H  H  H  H  -  -
09 0-3   -  I  -  -  I  -
10 0-3   -  J  -  J  J  -
11 0-6   -  K  K  -  K  K
12 0-6   L  -  L  -  -  -
13 0-6   -  M  M  -  M  -
14 0-6   -  N  N  N  -  N
15 0-6   -  O  -  -  -  O
16 1-4   P  P  -  -  -  -
17 2-3   Q  Q  -  Q  Q  Q
18 2-4   R  R  -  -  R   
19 2-5   -  -  S  -  -   
20 2-5   -  -  -  T      
21 2-6   U  -  -  U      
22 2-6   V  -  -         
23 2-6   -  X  -         
24 2-6   -  -            
25 3-5   -  -            
26 3-6   Z               
27 4-5                   
-------------------------------
26 LETTER CHECK

ZNLQM NOMMP XNNSN MVANK ZYMYS N
-------------------------------
