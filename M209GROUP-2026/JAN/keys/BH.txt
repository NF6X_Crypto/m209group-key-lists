EFFECTIVE PERIOD:
08-JAN-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BH
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  A  A  A
02 2-0   B  B  -  -  -  B
03 2-0   -  C  C  -  -  -
04 2-0   D  D  D  D  -  D
05 0-4   -  -  -  E  E  -
06 0-4   -  -  F  -  -  -
07 0-4   G  G  -  G  -  G
08 0-4   -  H  -  H  H  H
09 0-4   I  -  -  -  I  I
10 0-4   J  -  J  -  J  J
11 0-4   K  -  K  K  K  K
12 0-6   L  -  L  L  L  -
13 0-6   -  -  -  M  -  -
14 0-6   -  N  -  N  -  -
15 0-6   O  O  O  O  O  -
16 1-2   P  P  P  -  -  P
17 1-3   -  Q  -  -  -  -
18 1-5   -  R  -  R  R   
19 2-3   -  -  -  -  -   
20 2-3   -  -  -  -      
21 2-4   U  U  U  -      
22 2-4   -  -  V         
23 2-4   -  X  -         
24 2-4   -  Y            
25 2-6   Y  -            
26 3-6   Z               
27 4-6                   
-------------------------------
26 LETTER CHECK

AVVMZ UMJTV JNWZT VSTMR RRULS T
-------------------------------
