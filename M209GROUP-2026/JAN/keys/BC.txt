EFFECTIVE PERIOD:
03-JAN-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: BC
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  -  -  A  -
02 2-0   B  -  B  -  -  B
03 2-0   -  C  C  C  C  C
04 2-0   -  -  D  D  -  D
05 2-0   E  E  -  E  -  E
06 0-3   F  F  -  F  -  F
07 0-3   G  -  -  G  G  -
08 0-3   -  -  -  H  -  -
09 0-5   -  -  -  -  -  I
10 0-5   J  J  J  J  -  -
11 0-6   K  K  K  -  K  -
12 0-6   -  -  -  -  L  -
13 0-6   -  -  -  -  M  M
14 0-6   N  -  -  -  N  -
15 0-6   -  -  -  -  O  -
16 1-4   P  -  P  P  -  P
17 2-3   Q  Q  -  -  Q  -
18 2-3   R  -  -  R  R   
19 2-3   S  S  S  -  -   
20 2-3   -  -  T  -      
21 2-4   U  U  U  -      
22 2-4   V  V  V         
23 2-5   -  X  X         
24 2-5   -  Y            
25 3-6   -  Z            
26 4-5   Z               
27 5-6                   
-------------------------------
26 LETTER CHECK

GGAIM VVQZW XYRSI VOAIS VRLZI S
-------------------------------
