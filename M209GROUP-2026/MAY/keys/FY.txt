EFFECTIVE PERIOD:
09-MAY-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: FY
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 2-0   A  A  A  -  A  A
02 2-0   B  B  B  B  B  B
03 2-0   C  -  C  C  C  -
04 0-3   -  D  -  -  -  -
05 0-3   E  E  -  E  E  -
06 0-3   F  F  -  -  F  -
07 0-3   -  G  -  -  -  -
08 0-3   -  -  -  H  -  H
09 0-3   I  -  I  -  -  -
10 0-4   -  J  -  -  J  -
11 0-4   K  -  -  K  -  -
12 0-4   L  L  L  -  L  L
13 0-6   M  M  -  M  -  M
14 0-6   N  -  -  -  -  N
15 0-6   -  O  -  O  O  O
16 1-5   P  -  P  P  -  P
17 2-4   -  -  -  -  -  -
18 2-4   -  R  -  -  R   
19 2-5   S  S  S  S  S   
20 2-5   -  -  T  T      
21 2-6   U  U  U  -      
22 2-6   V  V  -         
23 2-6   -  -  X         
24 2-6   X  -            
25 3-4   -  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

QHUUO PXALL QXUJO LQMOR OVQQP X
-------------------------------
