EFFECTIVE PERIOD:
31-MAY-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GU
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  -  A  -
02 1-0   -  B  B  -  B  B
03 1-0   -  -  C  -  C  C
04 1-0   -  D  -  D  -  -
05 0-3   E  E  E  -  -  E
06 0-3   F  F  F  -  F  -
07 0-3   -  G  G  -  G  G
08 0-3   -  H  -  -  H  -
09 0-3   -  I  I  I  I  -
10 0-4   J  -  -  J  J  -
11 0-4   K  K  -  -  -  K
12 0-4   L  L  L  L  -  L
13 0-4   -  M  M  M  M  M
14 0-4   -  -  N  N  N  N
15 0-4   O  -  -  O  -  -
16 0-5   P  -  -  P  -  -
17 0-5   Q  -  Q  -  -  Q
18 1-3   R  R  R  -  -   
19 1-3   S  -  S  S  S   
20 1-3   T  -  -  -      
21 1-3   -  U  -  U      
22 1-4   -  -  V         
23 2-5   -  -  -         
24 3-5   -  -            
25 3-5   Y  -            
26 3-6   -               
27 4-5                   
-------------------------------
26 LETTER CHECK

XVVYQ LJFWP FQUGP UUNUB VAYUH J
-------------------------------
