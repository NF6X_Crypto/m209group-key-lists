EFFECTIVE PERIOD:
22-MAY-2026 00:00 THROUGH 23:59 GMT

NET INDICATOR:      M209GROUP
KEY LIST INDICATOR: GL
-------------------------------
NR LUGS  1  2  3  4  5  6
-------------------------------
01 1-0   A  -  -  A  -  A
02 1-0   B  -  B  -  -  -
03 2-0   -  C  C  -  -  C
04 2-0   -  D  -  -  D  D
05 2-0   E  E  E  E  -  E
06 2-0   F  -  F  -  F  F
07 2-0   -  G  -  G  G  G
08 2-0   -  -  H  H  H  H
09 2-0   -  -  I  I  -  -
10 0-3   J  -  -  -  J  -
11 0-3   -  -  K  K  -  -
12 0-3   -  L  -  L  L  -
13 0-3   -  -  -  -  -  M
14 0-3   -  N  N  -  -  -
15 0-3   O  O  O  O  -  -
16 0-5   P  P  -  P  P  -
17 0-5   Q  Q  -  -  Q  Q
18 0-5   -  -  -  R  -   
19 0-5   -  -  S  -  S   
20 1-3   -  -  T  T      
21 1-4   U  U  -  -      
22 2-5   -  V  V         
23 2-5   -  X  -         
24 2-5   X  -            
25 2-5   -  Z            
26 2-6   Z               
27 3-5                   
-------------------------------
26 LETTER CHECK

AWMAI STRWB PTLRX ADYHK KZLRY P
-------------------------------
